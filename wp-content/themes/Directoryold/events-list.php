<?php
/*
Template Name: Event Listing
*/
 	get_header(); 
	$tmpdata = get_option('templatic_settings');
	global $wpdb,$posts,$htmlvar_name,$wp_query;

	//do_action('after_directory_header');
	/**********************************************************/
	//Content for displaying map on event listing page
	?>
	<div id="category-widget" class="category-widget columns">
		<?php dynamic_sidebar('after_event_directory_header'); ?>
	</div>
	<?php
	/**********************************************************/

	/*do action for display the breadcrumb in between header and container. */
	//do_action('directory_before_container_breadcrumb'); 
	
	do_action( 'before_content' );
?>
<!--  CONTENT AREA START -->
<div id="content" class="contentarea large-9 small-12 columns <?php directory_class();?>">
	
	<?php 
	do_action( 'open_content' );
	do_action('directory_inside_container_breadcrumb'); /*do action for display the breadcrumb  inside the container. */ 	
	?>
     
    <div class="view_type_wrap">
		<?php
		/* Hooks for category title */
		do_action('directory_before_categories_title'); /* html variables are coming from this action in this plugin */ ?>

		<h1 class="loop-title"><?php the_title(); ?></h1>

		<?php do_action('directory_after_categories_title'); 
		/* Hooks for category title end */

		/* Hooks for category description */
		do_action('directory_before_categories_description'); 

		if ( category_description() ) : /* Show an optional category description */ ?>
		  <div class="archive-meta"><?php echo category_description(); ?></div>
		<?php endif; 

		do_action('directory_after_categories_description');
		/* Hooks for category description */
		
		do_action('directory_before_subcategory');
	 
		//do_action('directory_subcategory');
		/********************************************************************************************/
		//Content for displaying event categories for filter list
		/*************************************************/
		function replace_wps_a_tag($output) {
			$pattern = '<a href="(.+)">';
			$output = preg_replace($pattern, 'input type="checkbox" class="sub-category-item checkbox" name="listing_sub_category[]"' , $output);
			return $output;
		}
		add_action('wp_list_categories','replace_wps_a_tag');
		/*************************************************/
   		do_action('tevolution_category_query');
		$featured_catlist_list =  wp_list_categories('title_li=&echo=0&taxonomy=event-categories&show_count=0&hide_empty=1&pad_counts=0&show_option_none=&orderby=name&order=ASC');
		if(is_plugin_active('Tevolution-LocationManager/location-manager.php'))
		{
			remove_filter( 'terms_clauses','locationwise_change_category_query',10,3 );
		}

		if(!strstr(@$featured_catlist_list,'No categories') && !empty($featured_catlist_list))
		{
			echo '<div id="sub_listing_categories">';
			echo '<ul>';
				echo $featured_catlist_list;
			echo '</ul>';
			echo '</div>';
		}
		?>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			//console.log(jQuery(this).attr("class").split(' ')[1].split('-')[2]);
			jQuery('.sub-category-item').each(function( index ) {
		  		//console.log( index + ": " + jQuery(this).parent().attr("class").split(' ')[1].split('-')[2] );
		  		jQuery(this).val(jQuery(this).parent().attr("class").split(' ')[1].split('-')[2]);
			});
			jQuery('.cat-item').click(function(){
				//console.log(jQuery(this).attr("class").split(' ')[1].split('-')[2]);
				searchMulticheckbox('sub-category','Category');
			});
		});
		</script>	
		<?php
		/********************************************************************************************/

		/* Loads the sidebar-before-content. */
		if(function_exists('supreme_sidebar_before_content'))
	   apply_filters('tmpl_before-content',supreme_sidebar_before_content() ); 
		   
		do_action('directory_after_subcategory');
	 ?>
    </div>
     <!--Start loop taxonomy page-->
   
	<?php do_action('directory_before_loop_taxonomy');?>
     
    <!--Start loop taxonomy page-->
    <section id="loop_listing_taxonomy" class="search_result_listing <?php if(isset($tmpdata['default_page_view']) && $tmpdata['default_page_view']=="gridview"){echo 'grid';}else{echo 'list';}?>" <?php if( is_plugin_active('Tevolution-Directory/directory.php') && isset($tmpdata['default_page_view']) && $tmpdata['default_page_view']=="mapview"){ echo 'style="display: none;"';}?>>
    <?php 
/********************************************************************************************************/
global $wp;
$paged = ((int)get_query_var('paged')) ? (int)get_query_var('paged') : 1;

$term = $_REQUEST['p1g_search'];
if(!empty($_REQUEST['p1g_search_type'])){
	if($_REQUEST['p1g_search_type']=='places') {
		$qry = "SELECT DISTINCT id, CONCAT(post_title,', ',(SELECT meta_value FROM wp_postmeta WHERE post_id=id AND meta_key='_location_town'),', ',(SELECT meta_value FROM wp_postmeta WHERE post_id=id AND meta_key='_location_state')) as title ,guid as url FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value = '".$term."') OR (meta_key='_location_state' AND meta_value = '".$term."') OR post_title = '".$term."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id) 
			UNION DISTINCT
			SELECT DISTINCT id, CONCAT(post_title,', ',(SELECT meta_value FROM wp_postmeta WHERE post_id=id AND meta_key='_location_town'),', ',(SELECT meta_value FROM wp_postmeta WHERE post_id=id AND meta_key='_location_state')) as title ,guid as url FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value like '%".$term."%') OR (meta_key='_location_state' AND meta_value like '%".$term."%') OR post_title like '%".$term."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)";
	}

	$new_arg = array(
		'post_type' => 'event',
		'post__in' => $wpdb->get_col($qry),
		'paged' => $paged

	);
	$new_query = array_merge( $new_arg, (array)$wp->query_vars );
	$test = query_posts($new_query);
	//echo "<pre>";print_r($wp_query);echo "</pre>";die;
} else {
	$test = query_posts("post_type='event'&post_status='publish'&posts_per_page=-1");
}//echo "<pre>";print_r($test);echo "</pre>";
/********************************************************************************************************/
 if (have_posts()) : 
			while (have_posts()) : the_post(); 

				$post_id = get_the_ID();
				$tmpdata = get_option('templatic_settings');
				?>
				<?php do_action('directory_before_post_loop');?>
				 
				<article class="post  <?php templ_post_class();?>" >  
					<?php 
					/* Hook to display before image */	
					do_action('directory_before_category_page_image');
						
					/* Hook to Display Listing Image  */
					do_action('directory_category_page_image');
					 
					/* Hook to Display After Image  */						 
					do_action('directory_after_category_page_image'); 
					   
					/* Before Entry Div  */	
					do_action('directory_before_post_entry');?>
					
					<!-- Entry Start -->
					<div class="entry"> 
					   
						<?php  /* do action for before the post title.*/ 
						do_action('directory_before_post_title');         ?>
					   <div class="listing-wrapper">
						<!-- Entry title start -->
						<div class="entry-title">
					   
						<?php do_action('templ_post_title');                /* do action for display the single post title */?>
					   
						</div>
						
						<?php do_action('directory_after_post_title');          /* do action for after the post title.*/?>
					   
						<!-- Entry title end -->
						
						<!-- Entry details start -->
						<div class="entry-details">
						
						<?php  /* Hook to get Entry details - Like address,phone number or any static field  */  
						//do_action('listing_post_info'); 
						/***********************************************************************/
						//Content for displaying address
						$address=get_post_meta($post_id,'_location_address',true);
						$time=get_post_meta($post_id,'_event_start_date',true)."  -  ".get_post_meta($post_id,'_event_end_date',true); 
						echo '<p class="address">'.$address.'</p>';
						echo '<p class="time">'.$time.'</p>';
						/***********************************************************************/
						?>
						
						</div>
						<!-- Entry details end -->
					   </div>
						<!--Start Post Content -->
						<?php /* Hook for before post content . */ 
					   
						do_action('directory_before_post_content'); 
						
						/* Hook to display post content . */ 
						//do_action('templ_taxonomy_content');
						/***********************************************************************/
						//Content for displaying description about event
						if(@$tmpdata['listing_hide_excerpt']=='' || !in_array($post_type,@$tmpdata['listing_hide_excerpt'])){
							if(function_exists('supreme_prefix')){
								$theme_settings = get_option(supreme_prefix()."_theme_settings");
							}else{
								$theme_settings = get_option("supreme_theme_settings");
							}
							if($theme_settings['supreme_archive_display_excerpt']){
								echo '<div class="entry-summary123">';
								if(!function_exists('tevolution_excerpt_length')){	
									if($theme_settings['templatic_excerpt_length']){
										$length = $theme_settings['templatic_excerpt_length'];
									}
									if(function_exists('print_excerpt')){
										echo print_excerpt($length);
									}else{
										the_excerpt();
									}
								}else{
									the_excerpt();
								}
								echo '</div>';
							}else{ 
								echo '<div class="entry-content">';
								the_content(); 
								echo '</div>';
							}
						}
						/***********************************************************************/
					   
						/* Hook for after the post content. */
						do_action('directory_after_post_content'); 
						?>
						<!-- End Post Content -->
					   <?php 
					    /* Hook for before listing categories     */
						do_action('directory_before_taxonomies');
					    
						/* Display listing categories     */
					    do_action('templ_the_taxonomies'); 

						/* Hook to display the listing comments, add to favorite and pinpoint   */						
						//do_action('directory_after_taxonomies');
						/***********************************************************************/
						//Content for displaying favourite
						echo '<div class="rev_pin">';
						echo '<ul>';
						
						$comment_count= count(get_comments(array('post_id' => $post_id,	'status'=> 'approve')));
						$review=($comment_count <=1 )? __('review','templatic'):__('reviews','templatic');
						$review=apply_filters('tev_review_text',$review);

						if(current_theme_supports('tevolution_my_favourites') ):?>
							<li class="favourite"><?php tevolution_favourite_html();?></li>

						<?php endif;

						if(get_option('default_comment_status')=='open' || $post->comment_status =='open'){
							?>
							<li class="review"> <?php echo '<a href="'.get_permalink($post_id).'#comments">'.$comment_count.' '.$review.'</a>';?></li>
							<?php
						}
						if( !empty($address) && isset($templatic_settings['category_googlemap_widget']) &&  @$templatic_settings['category_googlemap_widget']!='yes' && @$templatic_settings['pippoint_oncategory'] ==1 && !is_author() && !$is_related && !is_home()):?> 
				          	<li class='pinpoint'><a id="pinpoint_<?php echo $post_id;?>" class="ping" href="#map_canvas"><?php _e('Pinpoint','templatic');?></a></li>
						<?php endif;

						echo '</ul>';
						echo '</div>';
						/***********************************************************************/
						?>
					</div>
					<!-- Entry End -->
					<?php do_action('directory_after_post_entry');?>
				</article>
			<?php do_action('directory_after_post_loop');
          	endwhile;
			wp_reset_query(); 
		else:?>
          	<p class='nodata_msg'><?php _e( 'Apologies, but no results were found for the requested archive.', DIR_DOMAIN ); ?></p>              
        <?php endif; 
        
        /* pagination start */
		if($wp_query->max_num_pages !=1):?>
		<div id="listpagi">
			<div class="pagination pagination-position">
				<?php if(function_exists('pagenavi_plugin')) { pagenavi_plugin(); } ?>
			</div>
		</div>
		<?php endif; /* pagination end */ ?>
        
    </section>
    <?php 
	do_action('directory_after_loop_taxonomy');
	 
	if(function_exists('supreme_sidebar_after_content'))
		apply_filters('tmpl_after-content',supreme_sidebar_after_content());  /* after-content-sidebar - use remove filter to don't display it */
		
	do_action( 'close_content' );
	?>
      <!--End loop taxonomy page -->
</div>
<!--  CONTENT AREA END -->

<!--taxonomy  sidebar -->
<?php
do_action( 'after_content' );
$taxonomy_name = 'event-categories';
/*echo "<script> 
	jQuery(document).ready(function(){
		 jQuery('.form_cat_left').closest('.ver-list-filter').css('display','none');
	});
</script>";	*/
?>
<aside id="sidebar-primary" class="sidebar large-3 small-12 columns">
	<?php 
		dynamic_sidebar('listingcategory_listing_sidebar'); 
		dynamic_sidebar('sidebar-event'); 
	?>
</aside>
<!--end taxonomy sidebar -->
<?php get_footer(); ?>