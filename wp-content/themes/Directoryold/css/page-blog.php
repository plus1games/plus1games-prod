<?php
/**
 * Template Name: Blog
 */
die('34566778');
get_header(); ?>
<div class="center">
<div class="content alignleft">
  <h1 class="sp-title"><?php the_title(); ?></h1>
  <?php
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  $wp_query->query('post_type=post&showposts=-1');
  ?>
 <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <header>
        <h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <div class="post-meta">
          <div class="fleft">Posted in: <?php the_category(', ') ?> &nbsp;|&nbsp; <time datetime="<?php the_time('Y-m-d\TH:i'); ?>"><?php the_time('F j, Y'); ?> at <?php the_time() ?></time> , by <?php the_author_posts_link() ?></div>
        </div><!--.post-meta-->
          
      </header>
      <a href="<?php the_permalink() ?>"><figure class="featured-thumbnail"><?php the_post_thumbnail(); ?></figure></a>
      <div class="post-content">
        <div class="excerpt"><?php $excerpt = get_the_excerpt(); echo my_string_limit_words($excerpt,50);?></div>
        <a href="<?php the_permalink() ?>" class="button">Read more</a>
      </div>
	  <div class="postcommnt"><?php //comments_popup_link('No comments', '1 comment', '% comments', 'comments-link', 'Comments are closed'); ?></div>
    </article>
    
  <?php endwhile; ?>
  
  <?php if ( $wp_query->max_num_pages > 1 ) : ?>
    <nav class="oldernewer">
      <div class="older">
        <?php next_posts_link('&laquo; Older Entries') ?>
      </div><!--.older-->
      <div class="newer">
        <?php previous_posts_link('Newer Entries &raquo;') ?>
      </div><!--.newer-->
    </nav><!--.oldernewer-->
  <?php endif; ?>
  
  <?php $wp_query = null; $wp_query = $temp;?>

</div><!--#content-->
<div class="sidebar alignleft"><?php get_sidebar(); ?></div>
</div>
<?php get_footer(); ?>