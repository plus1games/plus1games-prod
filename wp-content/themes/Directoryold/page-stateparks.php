<?php
/**
 * Template Name: State Parks
 */
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
get_header();
global $wpdb,$post;

if(($_REQUEST['action'])=='totalparks')
	{
		
		echo "<div style='width:68%;float:left;margin:20px;'>";
		$totalstate= $_REQUEST['state'];
			echo "<h3>Total Parks in State: ".$totalstate."</h3>";
			$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
				JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
			WHERE $wpdb->posts.`post_type` = 'listing' 
				AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$totalstate."') AND wp_term_relationships.`term_taxonomy_id`='392'
			ORDER BY $wpdb->posts.`ID`";

			$loop = $wpdb->get_results( $args );
			if(!empty($loop)){
				echo "<table border='1' style='width:auto;'><tr><th style='text-align:left;'>Cities</th><th>Number of Locations</th></tr>";
				$citygroup =array();
				foreach($loop as $v){
					$arrcity = get_post_meta($v->ID ,'city');
					//$citygroup[$arrcity[0]][] = $v->ID;
					$citygroup[] = $arrcity[0];
				}
				$citycount = array_count_values($citygroup);
				ksort($citycount);
				foreach($citycount as $key=>$val){
					$content_url = content_url();
					echo "<tr><td><a href='".site_url()."/?post_type=listing&custom_map=1&relation=OR&s=".$key."&cityname=".$key."&statename=".$totalstate."' style='text-decoration:none;font-size:15px;'>".$key."</a></td><td>".$val."</td></tr>";
				}
				echo "</table>";
			}
			else{
				echo "No park found in this state.";
			}
		echo "</div>";
	}
if(($_REQUEST['action'])=='indoorpark'){
	$totalstate= $_REQUEST['state'];
	$indoor_postids = $wpdb->get_results( "SELECT DISTINCT post_id FROM wp_em_location_fields WHERE field_indoor='Indoor' ",ARRAY_A );
	$post_ids=array();

	foreach($indoor_postids as $a){
		$post_ids[]=$a["post_id"];
	}
	echo "<div style='width:68%;float:left;margin:20px;'>";
		echo "<h3>Indoor Parks in State: ".$totalstate."</h3>";
		$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
			JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
		WHERE $wpdb->posts.`post_type` = 'listing' 
			AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$totalstate."') AND wp_term_relationships.`term_taxonomy_id`='392'
		ORDER BY $wpdb->posts.`ID`";

		$loop = $wpdb->get_results( $args );
		
			$total_postid=array();
			foreach($loop as $tp){
				$total_postid[]=	$tp->ID;
			} 
			$result=array_intersect($post_ids,$total_postid);
			if(!empty($result)){
			echo "<table border='1' style='width:auto;'><tr><th style='text-align:left;'>Cities</th><th>Number of Locations</th></tr>";
			$citygroup =array();
			foreach($result as $v){
				$arrcity = get_post_meta($v ,'city');
				//$citygroup[$arrcity[0]][] = $v->ID;
				$citygroup[] = $arrcity[0];
			}
			$citycount = array_count_values($citygroup);
			ksort($citycount);
			foreach($citycount as $key=>$val){
				$content_url = content_url();
				echo "<tr><td><a href='".site_url()."/?post_type=listing&type=indoorpark&custom_map=1&relation=OR&s=".$key."&cityname=".$key."&statename=".$totalstate."' style='text-decoration:none;font-size:15px;'>".$key."</a></td><td>".$val."</td></tr>";
			}
			echo "</table>";
		}
		else{
			echo "No Indoor park found in this state.";
		}
	echo "</div>";	
}

if(($_REQUEST['action'])=='outdoorpark'){
	$totalstate= $_REQUEST['state'];
	$indoor_postids = $wpdb->get_results( "SELECT DISTINCT post_id FROM wp_em_location_fields WHERE field_indoor='Outdoor' ",ARRAY_A );
	$post_ids=array();

	foreach($indoor_postids as $a){
		$post_ids[]=$a["post_id"];
	}
	echo "<div style='width:68%;float:left;margin:20px;'>";
		echo "<h3>Outdoor Parks in State: ".$totalstate."</h3>";
		$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
			JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
		WHERE $wpdb->posts.`post_type` = 'listing' 
			AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$totalstate."') AND wp_term_relationships.`term_taxonomy_id`='392'
		ORDER BY $wpdb->posts.`ID`";

		$loop = $wpdb->get_results( $args );
		$total_postid=array();
		foreach($loop as $tp){
			$total_postid[]=	$tp->ID;
		} 
		$result=array_intersect($post_ids,$total_postid);
		if(!empty($result)){
			echo "<table border='1' style='width:auto;float: left;'><tr><th style='text-align:left;'>Cities</th><th>Number of Locations</th></tr>";
			$citygroup =array();
			foreach($result as $v){
				$arrcity = get_post_meta($v ,'city');
				//$citygroup[$arrcity[0]][] = $v->ID;
				$citygroup[] = $arrcity[0];
			}
			$citycount = array_count_values($citygroup);
			ksort($citycount);
			foreach($citycount as $key=>$val){
				$content_url = content_url();
				echo "<tr><td><a href='".site_url()."/?post_type=listing&type=outdoorpark&custom_map=1&relation=OR&s=".$key."&cityname=".$key."&statename=".$totalstate."' style='text-decoration:none;font-size:15px;'>".$key."</a></td><td>".$val."</td></tr>";
			}
			echo "</table>";
		}
			else{
				echo "No Outdoor park found in this state.";
			}
	echo "</div>";	
}
if(($_REQUEST['action'])=='petfriendly')
	{
		echo "<div style='width:68%;float:left;margin:20px;'>";
			$totalstate= $_REQUEST['state'];
			echo "<h3>Pet Friendly Parks in State: ".$totalstate."</h3>";
			$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
				JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
			WHERE $wpdb->posts.`post_type` = 'listing' 
				AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$totalstate."') AND wp_term_relationships.`term_taxonomy_id` IN(404)
			ORDER BY $wpdb->posts.`ID`";

			$loop = $wpdb->get_results( $args );
			if(!empty($loop)){
				echo "<table border='1' style='width:auto;'><tr><th style='text-align:left;'>Cities</th><th>Number of Locations</th></tr>";
				$citygroup =array();
				foreach($loop as $v){
					$arrcity = get_post_meta($v->ID ,'city');
					//$citygroup[$arrcity[0]][] = $v->ID;
					$citygroup[] = $arrcity[0];
				}
				$citycount = array_count_values($citygroup);
				ksort($citycount);
				foreach($citycount as $key=>$val){
					$content_url = content_url();
					echo "<tr><td><a href='".site_url()."/?post_type=listing&type=parksportcat&custom_map=1&term_id=357&relation=OR&s=".$key."&cityname=".$key."&statename=".$totalstate."' style='text-decoration:none;font-size:15px;'>".$key."</a></td><td>".$val."</td></tr>";
				}			
				echo "</table>";
			}
			else{
				echo "No Pet Friendly park found in this state.";
			}
		echo "</div>";		
	}
	if(($_REQUEST['action'])=='water')
	{
		echo "<div style='width:68%;float:left;margin:20px;'>";
			$totalstate= $_REQUEST['state'];
			echo "<h3>Water Parks in State: ".$totalstate."</h3>";
			$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
				JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
			WHERE $wpdb->posts.`post_type` = 'listing' 
				AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$totalstate."') AND wp_term_relationships.`term_taxonomy_id` IN(417,647)
			ORDER BY $wpdb->posts.`ID`";

			$loop = $wpdb->get_results( $args );
			if(!empty($loop)){
				echo "<table border='1' style='width:auto;'><tr><th style='text-align:left;'>Cities</th><th>Number of Locations</th></tr>";
				$citygroup =array();
				foreach($loop as $v){
					$arrcity = get_post_meta($v->ID ,'city');
					//$citygroup[$arrcity[0]][] = $v->ID;
					$citygroup[] = $arrcity[0];
				}
				$citycount = array_count_values($citygroup);
				ksort($citycount);
				foreach($citycount as $key=>$val){
					$content_url = content_url();
					echo "<tr><td><a href='".site_url()."/?post_type=listing&type=water&custom_map=1&term_id=370,600&relation=OR&s=".$key."&cityname=".$key."&statename=".$totalstate."' style='text-decoration:none;font-size:15px;'>".$key."</a></td><td>".$val."</td></tr>";
				}			
				echo "</table>";
			}
			else{
				echo "No Water park found in this state.";
			}
		echo "</div>";		
	}
if(($_REQUEST['action'])=='dogpark'){
	$dogparkstate =$_REQUEST['state'];
	echo "<div style='width:68%;float:left;margin:20px;'>";
	$sql_dogparkcities="SELECT DISTINCT(location_town) FROM wp_em_locations WHERE location_state='".$dogparkstate."' ORDER BY location_town";
	$result_dogparkcities=$wpdb->get_results($sql_dogparkcities);
	echo "<h3>State : ".$dogparkstate."</h3>";

	echo "<table border='1' style='width:auto;'><tr><th>Cities</th><th>Number of Outdoor Location</th></tr>";
		foreach($result_dogparkcities as $rc){
			$sqllocationid="SELECT DISTINCT(location_id) as id FROM wp_em_locations WHERE location_town='".$rc->location_town."' AND location_state='".$dogparkstate."'";
			$result_locationid=$wpdb->get_results($sqllocationid);
			$dogparkcount=0;
			foreach($result_locationid as $rl)	
			{
				$sqlfield="SELECT location_id  FROM wp_em_location_fields WHERE location_id='".$rl->id."' AND field_category='77'";
				$resultfield=$wpdb->get_results($sqlfield);
				if(!empty($resultfield)){
					$dogparkcount++;
				}
				
			}
			$content_url = content_url();
			echo "<tr><td><a href='".get_template_directory_uri()."/cityparks.php?city=".$rc->location_town."&action=dogparkname&state=".$dogparkstate."' style='text-decoration:none;font-size:15px;' >".$rc->location_town."</a></td><td>".$dogparkcount."</td></tr>";
		}
		echo "</table></div>";
}
echo "<div>";
get_sidebar();
echo "</div>";
get_footer();
?>