<?php
/**
 * Template Name: Pet Friendly Parks

$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path); */
//global $supreme;
$theme_options = get_option($supreme->prefix.'_theme_settings');
get_header();
global $wpdb,$post,$wp_query;
	$arrstate = Array ( '1' => 'AK'  , '2' => 'AL' , '3' => 'AR', '4' => 'AZ' , '5' => 'CA' , '6' => 'CO' , '7' => 'CT' , '8' => 'DC' ,'9' => 'DE' , '10' => 'FL', '11' => 'GA', '12' => 'HI', '13' => 'IA', '14' => 'ID', '15' => 'IL', '16' => 'IN', '17' => 'KS', '18' => 'KY', '19' => 'LA', '20' => 'MA', '21' => 'MD', '22' => 'ME', '23' => 'MI', '24' => 'MN', '25' => 'MO', '26' => 'MS', '27' => 'MT', '28' => 'NC', '29' => 'ND', '30' => 'NE', '31' => 'NH', '32' => 'NJ', '33' => 'NM', '34' => 'NV', '35' => 'NY', '36' => 'OH', '37' => 'OK', '38' => 'ON', '39' => 'OR', '40' => 'PA', '41' => 'RI', '42' => 'SC', '43' => 'SD', '44' => 'TN', '45' => 'TX', '46' => 'UT', '47' => 'VA', '48' => 'VT', '49' => 'WA', '50' => 'WI', '51' => 'WV', '52' =>'WY' );
	$arrpid = array();
	foreach($arrstate as $v){
		
		$args = "SELECT DISTINCT($wpdb->posts.`ID`) FROM $wpdb->posts 
			JOIN  $wpdb->postmeta AS PM1 ON ($wpdb->posts.ID = PM1.post_id) JOIN  wp_term_relationships  ON ($wpdb->posts.ID = wp_term_relationships.`object_id`)
		WHERE $wpdb->posts.`post_type` = 'listing' 
			AND $wpdb->posts.`post_status` = 'publish' AND (PM1.meta_key = 'state' AND PM1.meta_value='".$v."') AND wp_term_relationships.`term_taxonomy_id` IN(404)
		ORDER BY $wpdb->posts.`ID`";

		$loop = $wpdb->get_results( $args );
		//$arrpid[] = $v .','.count($loop);	
		$arrpid[]="['".$v."',".count($loop)."]";
	}
  $total_data=implode(',',$arrpid);
?>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load('visualization', '1', { 'packages': ['geochart'] });
	//Total parks
    google.setOnLoadCallback(drawMap);
    function drawMap() {
        var data = google.visualization.arrayToDataTable([
    ['Country', 'Number of Locations'],
			<?php echo $total_data; ?>
    /*['Alabama', 0],*/
    ]);	
        var options = {};
        options['region'] = 'US';
        options['resolution'] = 'provinces';
        options['colors'] = ['#C9DEED', '#87BBD9', '#4798C5', '#0078B2', '#00589E'];
		options['datalessRegionColor']=['#C1C1C1']
		options['legend'] = {'position': 'top'};
        var container = document.getElementById('map_canvas');
        var geochart = new google.visualization.GeoChart(container);
		google.visualization.events.addListener(geochart, 'select', function() {
        var selection = geochart.getSelection()[0];
        var label = data.getValue(selection.row, 0);
		var action = 'petfriendly';
		jQuery('#totalcontainer').text('Loading...');
		//alert (label);
        jQuery.ajax({
			   url    :'<?php echo get_template_directory_uri();?>/custom-ajax/parks-ajax.php',
			   data   :'state='+label+'&action='+action,
				   dataType:'html',
			   success:function(x){
					jQuery('#totalcontainer').html(x);
			   }
			 });
		});
        geochart.draw(data, options);
    };

   
  </script>
	<div id ='geomapleft' class='' style='width:68%;float:left;margin:20px;'>
		<div style=''>
			<h2 style="" >USA Total Pet Friendly Parks Map</h2>
			<div id='map_canvas' style='width: 98%; height: auto;float:left;max-width:550px;'></div>
			<div id='totalcontainer' style='clear:both;overflow:auto;'></div>
		</div><br/>
		
	</div>
	
	<?php
	echo "<div>";
	get_sidebar();
	echo "</div>";
	get_footer();
	?>