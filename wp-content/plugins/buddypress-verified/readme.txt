=== BuddyVerified ===
Contributors: modemlooper
Tags: buddypress, members, profile, spam, moderation
Requires at least: WordPress 3.6 and BuddyPress 2.0
Tested up to: 4.5.3
Stable tag: 2.3.1

Allows admins to specify verified accounts. Adds a badge to verified user avatars.

== Description ==

Allows admins to specify verified accounts. Adds a badge to verified user avatars.


== Installation ==

1. Upload `buddyverified` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
5. Then go to wp-admin > Users > Extended Profile >> and choose various settings.

== Screenshots ==

1. Admin user verify options
2. Admin listing verified
3. User profile verified

== Notes ==
License.txt - contains the licensing details for this component.

BuddyPress doesn't have an Extended Profile page for the super admin. You should create a new administrator account for your front end profile. (https://buddypress.trac.wordpress.org/ticket/5365)

== Changelog ==
= 2.3.1 =
fixed missing non verified avatars * props Brajesh

= 2.3 =
Update to CSS for WP yearly themes.
Added option to add custom CSS for badge placement. Settings -> BuddyPress, clicl option tab.

= 2.2 =
removed tooltip as is caused to many conflicts. verfied text no in member header

= 2.1 =

All new plugin
