<?php
/**
 * Plugin Name: Directory - Listing Statistics
 * Plugin URI: http://templatic.com/docs/listing-statistics/
 * Description: Allows content authors to see various analytic stats about their listings right from their user dashboard
 * Version: 1.0.1
 * Author: Templatic
 * Author URI: http://templatic.com
 */
ob_start();
@define( 'TEVOLUTION_STATISTCS_DOMAIN', 'tevolution_statistics');
define( 'TEVOLUTION_STATISTCS_VERSION', '1.0.1' );
define('TEVOLUTION_STATISTCS_SLUG','Tevolution-Statistics/statistics.php');
// Plugin Folder URL
define( 'TEVOLUTION_STATISTCS_URL', plugin_dir_url( __FILE__ ) );
// Plugin Folder Path
define( 'TEVOLUTION_STATISTCS_DIR', plugin_dir_path( __FILE__ ) );
// Plugin Root File
define( 'TEVOLUTION_STATISTCS_FILE', __FILE__ );

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
ob_start();
$locale = get_locale();
global $wpdb;
if(file_exists(TEVOLUTION_STATISTCS_DIR.'languages/'.$locale.'.mo'))
{
	load_textdomain(TEVOLUTION_STATISTCS_DOMAIN,TEVOLUTION_STATISTCS_DIR.'languages/'.$locale.'.mo');
}
if(strstr($_SERVER['REQUEST_URI'],'plugins.php')){
	require_once('wp-updates-plugin.php');
	new WPTevolutionStatisticsUpdates( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
}

/*
 * Plugin Deactivation hook
 */
register_deactivation_hook(__FILE__,'statistics_deactivation_hook');
function statistics_deactivation_hook(){

	 delete_option('statistics_redirect_activation');
}

/*
 * Plugin Activation hook
 */
register_activation_hook(__FILE__,'statistics_plugin_activate');
if(!function_exists('statistics_plugin_activate')){
	function statistics_plugin_activate(){	
		global $wpdb;
		update_option('statistics_redirect_activation','Active');
	}
}
add_action('admin_init', 'statistics_plugin_redirect');

/*
Name : statistics_plugin_redirect
Description : Redirect plugin to monetize listing
*/
function statistics_plugin_redirect()
{
	if (get_option('statistics_redirect_activation') == 'Active' && is_plugin_active('Tevolution/templatic.php'))
	{
		update_option('statistics_redirect_activation', 'Deactive');
		wp_redirect(site_url().'/wp-admin/admin.php?page=monetization&activate=tevolution_statistics');
	}
}

if((isset($_REQUEST['activate']) && $_REQUEST['activate'] == 'tevolution_statistics'))
{
	add_action('admin_notices','tevolution_statistics_admin_notices');	//action to show notice after plugin activation.
}

/*
Name : tevolution_statistics_admin_notices
Description : message while activate the plugin
*/
function tevolution_statistics_admin_notices()
{
	if(isset($_REQUEST['activate']) && $_REQUEST['activate'] == 'tevolution_statistics')
	{
		echo '<div class="updated"><p>' . __('Tevolution - Statistics plugin has been activated and option to disable this is added below. Listing owners will be able to view listing statistics on their dashboard now.  Refer to this',TEVOLUTION_STATISTCS_DOMAIN).' <b><a href="http://templatic.com/docs/tevolution/">'.__('plugin documentation',TEVOLUTION_STATISTCS_DOMAIN).'</a></b> '.__('for more detail.',TEVOLUTION_STATISTCS_DOMAIN). '</p></div>';
	}
}
if(is_plugin_active('Tevolution/templatic.php'))
{
	include(TEVOLUTION_STATISTCS_DIR.'statistics_settings.php');	
}else{
	add_action('admin_notices','statistics_admin_notices');
}

/*
Name : statistics_admin_notices
Description : show notice while tevolution plugin is not activated.
*/
function statistics_admin_notices(){
	echo '<div class="error"><p>' . sprintf(__('You have not activated the base plugin %s. Please activate it to use Tevolution-Statistics plugin.',TEVOLUTION_STATISTCS_DOMAIN),'<b>Tevolution</b>'). '</p></div>';
}

/*
 * Function Name: statistics_css_on_init
 * Return: include statistics css
 */
add_action( 'wp_head', 'include_statistics_css' );
add_action( 'admin_head', 'include_statistics_css' );   
function include_statistics_css() {
   /* Register our stylesheet. */
   wp_register_style( 'statistcs_css', TEVOLUTION_STATISTCS_URL.'css/statistics-style.css' );
   wp_enqueue_style( 'statistcs_css' );
}

/*
 * Function Name: statistics_update_login
 * Return: update statistics_update_login plugin version after templatic member login
 */
add_action('wp_ajax_statistics','statistics_update_login');
function statistics_update_login()
{
	check_ajax_referer( 'statistics', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}
?>