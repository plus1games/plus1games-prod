Templatic Plugin - Directory - Listing Statistics(version 1.0.1)  
=============================================================================

25th April 2015 (Version 1.0.1)
-----------------------------------------------------------------------------
Fix - Show statistics field goes blank while change in order of price package.

Add - Added the functionality to show count of search page visit on author dashboard page.

Fix - Home page counter was not working.

Fix - WordPress 4.2 & Security related changes.
 