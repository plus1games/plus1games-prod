<?php
/*
 * Function Name: add_stats_option
 * Description: html to show statistics on dashboard page in price package.
 */
add_action('add_new_row_pricepackages','add_stats_option',20);
function add_stats_option($id='')
{
	?>
	<!-- added the html for user can see statistics on user dashboard page -->
	<tr>
		<th valign="top">
			<label for="show_statistics" class="form-textfield-label"><?php _e('Allow listing statistics on author profile page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
		</th>
		<td>
			<input type="checkbox" size="10" value="1" <?php checked( @get_post_meta($id,'show_statistics',true), true ); ?> id="show_statistics" name="show_statistics"><label for="show_statistics">&nbsp;<?php echo __('Yes',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
			<p class="description"><?php _e('Users who purchase this package will be able to see analytics like how many times the listing was visited, how many times an inquiry for a listing was sent,etc. ',TEVOLUTION_STATISTCS_DOMAIN);?></p>
		</td>
	</tr>
	<?php
}

/*
 * Function Name: save_stats_option
 * Description: save the option in price package whether to show statistics on dashboard page with particular price package.
 */
add_action('save_post_monetization_package','save_stats_option');
function save_stats_option($post_id)
{
	if(is_admin() && @$_REQUEST['action'] != 'price_package_order')
	{
		update_post_meta($post_id,'show_statistics',$_POST['show_statistics']);
	}
}

/*
 * Function Name: increase_post_stats_count
 * Description: increase the count how many time the particular post detail page has been visited 
 */
add_action('wp_head','increase_post_stats_count');
function increase_post_stats_count()
{
	global $post;
	$package_select = get_post_meta($post->ID,'package_select',true);
	if(is_tax() || is_tag() || is_category() || is_archive())
	{
		$_SESSION['statistics_detail_page_link'] = 'statistics_category_page'; //set session for visit for detail page from category page
	}
	if(is_front_page() || is_home())
	{
		$_SESSION['statistics_detail_page_link'] = 'statistics_front_page'; //set session for visit for detail page from home page
	}
	if(is_search())
	{
		$_SESSION['statistics_detail_page_link'] = 'statistics_search_page'; //set session for visit for detail page from home page
	}
	if(get_post_meta($package_select,'show_statistics',true))
	{
		if(is_single())
		{
			$single_post_count = get_post_meta($post->ID,'single_post_count',true);
			update_post_meta($post->ID,'single_post_count',@$single_post_count+1); 												// update the over all count for the particular post.
			$today_single_post_count = @get_post_meta($post->ID,date('Y-m-d').'_single_post_count',true);
			update_post_meta($post->ID,date('Y-m-d').'_single_post_count',@$today_single_post_count+1);							// update the over all count for the particular post for particular date.
			$month_single_post_count = @get_post_meta($post->ID,Date('Y').'_'.date('n').'_single_post_count',true);
			update_post_meta($post->ID,Date('Y').'_'.date('n').'_single_post_count',@$month_single_post_count+1);				// update the over all count for the particular post for particular month.
			
			if(isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_front_page')
			{
				$front_post_count = get_post_meta($post->ID,'front_post_count',true);
				update_post_meta($post->ID,'front_post_count',@$front_post_count+1);											// update the over all count for the particular post when we visit from home page.
				$today_front_page_post_count = @get_post_meta($post->ID,date('Y-m-d').'_front_page_post_count',true);
				update_post_meta($post->ID,date('Y-m-d').'_front_page_post_count',@$today_front_page_post_count+1);				// update the over all count for the particular post when we visit from home page for particular date.
				$month_front_page_post_count = @get_post_meta($post->ID,Date('Y').'_'.date('n').'_front_page_post_count',true);
				update_post_meta($post->ID,Date('Y').'_'.date('n').'_front_page_post_count',@$month_front_page_post_count+1);	// update the over all count for the particular post when we visit from home page for particular month.
				unset($_SESSION['statistics_detail_page_link']);
			}
			if(isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_category_page')
			{
				$category_post_count = get_post_meta($post->ID,'category_post_count',true);
				update_post_meta($post->ID,'category_post_count',@$category_post_count+1);										// update the over all count for the particular post when we visit from category page.
				$today_category_post_count = @get_post_meta($post->ID,date('Y-m-d').'_category_post_count',true);
				update_post_meta($post->ID,date('Y-m-d').'_category_post_count',@$today_category_post_count+1);					// update the over all count for the particular post when we visit from category page for particular date.
				$month_category_post_count = @get_post_meta($post->ID,Date('Y').'_'.date('n').'_category_post_count',true);
				update_post_meta($post->ID,Date('Y').'_'.date('n').'_category_post_count',@$month_category_post_count+1);		// update the over all count for the particular post when we visit from home page for particular month.
				unset($_SESSION['statistics_detail_page_link']);
			}
			if(isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_search_page')
			{
				$search_post_count = get_post_meta($post->ID,'search_post_count',true);
				update_post_meta($post->ID,'search_post_count',@$category_post_count+1);										// update the over all count for the particular post when we visit from category page.
				$today_search_post_count = @get_post_meta($post->ID,date('Y-m-d').'_search_post_count',true);
				update_post_meta($post->ID,date('Y-m-d').'_search_post_count',@$today_search_post_count+1);					// update the over all count for the particular post when we visit from category page for particular date.
				$month_search_post_count = @get_post_meta($post->ID,Date('Y').'_'.date('n').'_search_post_count',true);
				update_post_meta($post->ID,Date('Y').'_'.date('n').'_search_post_count',@$month_search_post_count+1);		// update the over all count for the particular post when we visit from home page for particular month.
				unset($_SESSION['statistics_detail_page_link']);
			}
		}
	}
	if((!is_tax() && !is_tag() && !is_category() && !is_archive() ) && isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_category_page')
	{
		unset($_SESSION['statistics_detail_page_link']);	// unset session for increment for visit of detail page other than category page.
	}
	if(!is_search()  && isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_search_page')
	{
		unset($_SESSION['statistics_detail_page_link']);	// unset session for increment for visit of detail page other than category page.
	}
	if( (!is_home() && !is_front_page()) && isset($_SESSION['statistics_detail_page_link']) && $_SESSION['statistics_detail_page_link'] == 'statistics_front_page')
	{
		//unset($_SESSION['statistics_detail_page_link']);	// unset session for increment for visit of detail page other than home page.
	}
	
}

/*
 * Function Name: showdetail_page_statistics
 * Description: Show statistics on author page.
 */
add_action('templ_show_edit_renew_delete_link','showdetail_page_statistics',20);
function showdetail_page_statistics()
{
	global $post,$current_user;
	$package_select = get_post_meta($post->ID,'package_select',true);
	$post_author_id=$post->post_author;
	if(get_post_meta($package_select,'show_statistics',true)  && (is_author() || is_admin()) && ((is_user_logged_in() && $current_user->ID==$post_author_id) || is_admin()))
	{
		if(!is_admin())
		{
	?>
		<a id="show_statistics_div_<?php echo $post->ID; ?>" class="button secondary_btn tiny_btn" onclick="return show_statistics(<?php echo $post->ID; ?>)" class="show_statistics_div"><?php _e('Statistics',TEVOLUTION_STATISTCS_DOMAIN); ?></a>
	<?php } ?>
		<div class="tevolution-statistic" id="statistics_<?php echo $post->ID; ?>" <?php if(!is_admin()){ ?>style="display:none;"<?php } ?>>  
			
		  
			<div id="statistics_results" class="clearfix">
				<div class="stats-res-left horizontal">
					<h3><?php _e('Today',TEVOLUTION_STATISTCS_DOMAIN);?></h3>
					<ul>
						<li>
							<label><?php _e('Total',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,'viewed_count_daily',true))?get_post_meta($post->ID,'viewed_count_daily',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Front Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_front_page_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_front_page_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Category Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_category_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_category_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Search Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_search_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_search_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Send Inquiry',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_send_enquiry_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_send_enquiry_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Send to friend',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_send_friend_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_send_friend_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Favorites',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span><?php echo (get_post_meta($post->ID,date('Y-m-d').'_add_to_fav_post_count',true))?get_post_meta($post->ID,date('Y-m-d').'_add_to_fav_post_count',true):0; ?> <?php _e('People',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
					</ul>
				</div>
				
				<div class="stats-res-right horizontal">
					<div id="statistics_tabs">
				
				<div id="current_year" class="clearfix">
					<div class="ms-months current_year_<?php echo $post->ID; ?> clearfix" >
						<div class="months_links <?php echo $post->ID; ?>">
						<?php
							for($m = 1;$m <= 12; $m++){
								$month =  date_i18n("M", mktime(0, 0, 0, $m, 1));
								$current_month = Date('M');
								if($m < get_the_time('n') && Date('Y') <= get_the_time('Y')  )
								{
									continue;
								}
								if($m > Date('n'))
								{
									break;
								}
								if($current_month == $month)
								{
									$class = "class='active'";
								}
								else
								{
									$class = '';
								}
								echo "<a href='javascript:void(0);' id=".$m.'_'.$post->ID." onclick='return show_month_stats($m,$post->ID);' $class>$month</a>";
							} 
						?>
						</div>
					</div>
					<div class="ms-years">
						<?php $year_diff = abs(get_the_time('Y')-Date('Y'));
						if($year_diff >= 1)
						{?>
						<select name="statistics_year" onchange='return show_year_stats(this.value,<?php echo $post->ID; ?>);' id="statistics_year_<?php echo $post->ID; ?>">
						<?php
							for($m = 0;$m <= $year_diff; $m++){
								$nextyear  = mktime(0, 0, 0, date("m"),   date("d"),   get_the_time("Y")+$m);
								$year = date_i18n("Y",$nextyear);
								if(Date('Y') == $year)
								{
									$selected = 'selected="selected"';
								}
								echo "<option value='$year' ".$selected.">$year</option>";
							}
						?>
						</select>
						<?php } ?>
					</div>
					
				</div>
			</div>
					<h3 id="month_show_statistics_<?php echo $post->ID; ?>"><?php  echo get_the_time('F').' '.Date("Y").' ';_e('Statistics',TEVOLUTION_STATISTCS_DOMAIN); ?></h3>
					<ul>
						<li>
							<label><?php _e('Total',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="total_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_single_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_single_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Front Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="front_page_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_front_page_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_front_page_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Category Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="category_page_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_category_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_category_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('From Search Page',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="search_page_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_search_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_search_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Send Inquiry',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="send_enquiry_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_send_enquiry_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_send_enquiry_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Send to friend',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="send_friend_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_send_friend_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_send_friend_post_count',true):0; ?> <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						<li>
							<label><?php _e('Favorites',TEVOLUTION_STATISTCS_DOMAIN); ?></label>
							<span id="add_to_fav_post_count_<?php echo $post->ID; ?>"><?php echo (get_post_meta($post->ID,Date('Y').'_'.date('n').'_add_to_fav_post_count',true))?get_post_meta($post->ID,Date('Y').'_'.date('n').'_add_to_fav_post_count',true):0; ?> <?php _e('People',TEVOLUTION_STATISTCS_DOMAIN); ?></span>
						</li>
						
					</ul>
				</div>    
			</div>
		</div> 
<?php }
}

/*
 * Function Name: statistics_meta_box
 * Description: add meta box to show statistics while edit post page.
 */
add_action('add_meta_boxes','statistics_meta_box',12);
if(!function_exists('statistics_meta_box'))
{
	function statistics_meta_box()
	{
		$post_types = get_post_types();
		
		foreach( $post_types as $post_type )
		{
			if( $post_type != "page" && $post_type != "attachment" && $post_type != "revision" && $post_type != "nav_menu_item" && $post_type != "admanager")
			{
				/* Add Metabox for post type */
				add_meta_box('statistics_meta_box_settings', __('Statistics',TEVOLUTION_STATISTCS_DOMAIN), 'statistics_meta_box_settings', $post_type, 'normal', 'high');
			}
		}
	}
}

/*
 * Function Name: statistics_meta_box_settings
 * Description: fetch statistics on edit post page.
 */
function statistics_meta_box_settings()
{
	showdetail_page_statistics();	// function to show statistics on edit post admin side.
}

/*
 * Function Name: tevolution_settings_send_friend
 * Return: send friend to email count increment 
 */
add_action('wp_ajax_tevolution_settings_send_friend','tevolution_settings_send_friend');
add_action('wp_ajax_nopriv_tevolution_settings_send_friend','tevolution_settings_send_friend');
function tevolution_settings_send_friend()
{
	global $post;
	$package_select = get_post_meta($_REQUEST['post_id'],'package_select',true);
	if(get_post_meta($package_select,'show_statistics',true))
	{
		if(isset($_REQUEST['to_name_friend']) && $_REQUEST['to_name_friend'] != '' && isset($_REQUEST['to_friend_email']) && $_REQUEST['to_friend_email'] != '' && isset($_REQUEST['yourname']) && $_REQUEST['yourname'] != '' && isset($_REQUEST['youremail']) && $_REQUEST['youremail'] != ''  && isset($_REQUEST['frnd_subject']) && $_REQUEST['frnd_subject'] != ''  && isset($_REQUEST['frnd_comments']) && $_REQUEST['frnd_comments'] != '' )
		{
			$send_friend_post_count = get_post_meta($_REQUEST['post_id'],'send_friend_post_count',true);
			update_post_meta($_REQUEST['post_id'],'send_friend_post_count',@$send_friend_post_count+1);									// update the over all count for the particular post when send to friend mail is fired.
			$today_send_friend_post_count = @get_post_meta($_REQUEST['post_id'],date('Y-m-d').'_send_friend_post_count',true);
			update_post_meta($_REQUEST['post_id'],date('Y-m-d').'_send_friend_post_count',@$today_send_friend_post_count+1);			// update the current date count for the particular post when send to friend mail is fired.
			$month_send_friend_post_count = @get_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_send_friend_post_count',true);
			update_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_send_friend_post_count',@$month_send_friend_post_count+1);	// update the current month count for the particular post when send to friend mail is fired.
		}
	}
	exit;
}

/*
 * Function Name: tevolution_settings_send_enquiry
 * Return: send enquiry email count increment 
 */
add_action('wp_ajax_tevolution_settings_send_enquiry','tevolution_settings_send_enquiry');
add_action('wp_ajax_nopriv_tevolution_settings_send_enquiry','tevolution_settings_send_enquiry');
function tevolution_settings_send_enquiry()
{
	global $post;
	$package_select = get_post_meta($_REQUEST['listing_id'],'package_select',true);
	if(get_post_meta($package_select,'show_statistics',true))
	{
		if(isset($_REQUEST['full_name']) && $_REQUEST['full_name'] != '' && isset($_REQUEST['your_iemail']) && $_REQUEST['your_iemail'] != '' && isset($_REQUEST['contact_number']) && $_REQUEST['contact_number'] != '' && isset($_REQUEST['inq_subject']) && $_REQUEST['inq_subject'] != ''  && isset($_REQUEST['inq_msg']) && $_REQUEST['inq_msg'] != '' )
		{
			$send_enquiry_post_count = get_post_meta($_REQUEST['listing_id'],'send_enquiry_post_count',true);
			update_post_meta($_REQUEST['listing_id'],'send_enquiry_post_count',@$send_enquiry_post_count+1);								// update the over all count for the particular post when send enquiry mail is fired.
			$today_send_enquiry_post_count = @get_post_meta($_REQUEST['listing_id'],date('Y-m-d').'_send_enquiry_post_count',true);
			update_post_meta($_REQUEST['listing_id'],date('Y-m-d').'_send_enquiry_post_count',@$today_send_enquiry_post_count+1);			// update the current date count for the particular post when send enquiry mail is fired.
			$month_send_enquiry_post_count = @get_post_meta($_REQUEST['listing_id'],Date('Y').'_'.date('n').'_send_enquiry_post_count',true);
			update_post_meta($_REQUEST['listing_id'],Date('Y').'_'.date('n').'_send_enquiry_post_count',@$month_send_enquiry_post_count+1);	// update the current month count for the particular post when send enquiry mail is fired.
		}
	}
	exit;
}

/*
 * Function Name: tevolution_settings_send_enquiry
 * Return: increament the count of add to favorite
 */
add_action('wp_ajax_tevolution_settings_add_to_fav','tevolution_settings_add_to_fav');
function tevolution_settings_add_to_fav()
{
	global $post;
	$package_select = get_post_meta($_REQUEST['post_id'],'package_select',true);
	if(get_post_meta($package_select,'show_statistics',true))
	{
		$add_to_fav_post_count = get_post_meta($_REQUEST['post_id'],'add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],'add_to_fav_post_count',@$add_to_fav_post_count+1);									// update the over all count for the particular post when click on add to favorite.
		$today_add_to_fav_post_count = @get_post_meta($_REQUEST['post_id'],date('Y-m-d').'_add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],date('Y-m-d').'_add_to_fav_post_count',@$today_add_to_fav_post_count+1);				// update the current date count for the particular post when click on add to favorite.
		$month_add_to_fav_post_count = @get_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_add_to_fav_post_count',@$month_add_to_fav_post_count+1);	// update the current month count for the particular post when click on add to favorite.
	}
	exit;
}

/*
 * Function Name: tevolution_settings_remove_from_fav
 * Return: decrement the count of add to favorite
 */
add_action('wp_ajax_tevolution_settings_remove_from_fav','tevolution_settings_remove_from_fav');
function tevolution_settings_remove_from_fav()
{
	global $post;
	$package_select = get_post_meta($_REQUEST['post_id'],'package_select',true);
	if(get_post_meta($package_select,'show_statistics',true))
	{
		$add_to_fav_post_count = get_post_meta($_REQUEST['post_id'],'add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],'add_to_fav_post_count',@$add_to_fav_post_count-1);									// update the over all count for the particular post when click remove from favorite.
		$today_add_to_fav_post_count = @get_post_meta($_REQUEST['post_id'],date('Y-m-d').'_add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],date('Y-m-d').'_add_to_fav_post_count',@$today_add_to_fav_post_count-1);				// update the current date count for the particular post when click remove from favorite.
		$month_add_to_fav_post_count = @get_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_add_to_fav_post_count',true);
		update_post_meta($_REQUEST['post_id'],Date('Y').'_'.date('n').'_add_to_fav_post_count',@$month_add_to_fav_post_count-1);	// update the current month count for the particular post when click remove from favorite.
	}
	exit;
}

/*
 * Function Name: tevolution_show_month_stats
 * Return: ajax to show views on user dashboard
 */
add_action('wp_ajax_tevolution_show_month_stats','tevolution_show_month_stats');
function tevolution_show_month_stats()
{
	global $post;
	$month_single_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_single_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_single_post_count',true):0;
	$month_front_page_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_front_page_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_front_page_post_count',true):0;
	$month_category_page_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_category_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_category_post_count',true):0;
	$month_send_enquiry_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_send_enquiry_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_send_enquiry_post_count',true):0;
	$month_send_friend_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_send_friend_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_send_friend_post_count',true):0;
	$month_add_to_fav_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_add_to_fav_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_add_to_fav_post_count',true):0;
	$month_search_page_post_count = (get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_search_post_count',true))?get_post_meta($_REQUEST['post_id'],$_REQUEST['year'].'_'.$_REQUEST['month'].'_search_post_count',true):0;
	echo $month_single_post_count."_".$month_front_page_post_count."_".$month_category_page_post_count."_".$month_send_enquiry_post_count."_".$month_send_friend_post_count."_".$month_add_to_fav_post_count."_".$month_search_page_post_count;
	exit;
}

/*
 * Function Name: tevolution_show_month_stats
 * Return: ajax to show views on user dashboard
 */
add_action('wp_ajax_tevolution_show_month','tevolution_show_month');
function tevolution_show_month()
{
	global $post;
	$pid = $_REQUEST['post_id'];
	$result = '';
	$current_month = '';
	$result = '<div class="months_links '.$_REQUEST['post_id'].'">';
		
			for($m = 1;$m <= 12; $m++){
				$month =  date_i18n("M", mktime(0, 0, 0, $m, 1));
				$current_month = Date('M');
				if($m < get_the_time('n',$pid) && $_REQUEST['year'] <= get_the_time('Y',$pid)  )
				{
					continue;
				}
				if($m > Date('n') && $_REQUEST['year'] == Date('Y')  )
				{
					break;
				}
				if($m == get_the_time('n',$pid) && $_REQUEST['year'] == get_the_time('Y',$pid) && $m > $_REQUEST['month'])
				{
					$class = "class='active'";
				}
				elseif($m == $_REQUEST['month'])
				{
					$class = "class='active'";
				}
				else
				{
					$class = '';
				}
				$result .= "<a href='javascript:void(0);' id=".$m.'_'.$_REQUEST['post_id']." onclick='return show_month_stats($m,$pid);' $class>$month</a>";
			}
			
		$result .='</div>';
		echo $result;
	exit;
}

/*
 * Function Name: save_stats
 * Return: script to save the count option from send inquiry,send to friend and favorite.
 */
add_action('wp_footer','save_stats',20);
add_action('admin_footer','save_stats',20);
function save_stats()
{
	global $post;
	?>
	<script type="text/javascript">
		//jquery to count while send to firend mail is fired.
		jQuery(document).ready(function(){
			//global vars
			var send_to_frnd = jQuery("#send_to_frnd");
			
			//On Submitting send to friend form
			send_to_frnd.submit(function(){
					var send_to_frnd_data = send_to_frnd.serialize();				
					jQuery.ajax({
						url:ajaxUrl,
						type:'POST',
						data:'action=tevolution_settings_send_friend&' + send_to_frnd_data,
						success:function(results) {
							
						}
					});
				});

			//jquery to count while send enquiry mail is fired.
			var enquiry1frm = jQuery("#inquiry_frm");
			//On Submitting send enquiry form
			enquiry1frm.submit(function(){
					var inquiry_data = enquiry1frm.serialize();					
					jQuery.ajax({
						url:ajaxUrl,
						type:'POST',
						data:'action=tevolution_settings_send_enquiry&' + inquiry_data,
						success:function(results) {
							
						}
					});
				});
				
				//On Submitting add to favorite form
				jQuery('.addtofav').live('click',function(){
					jQuery.ajax({
						url:ajaxUrl,
						type:'POST',
						data:'action=tevolution_settings_add_to_fav&post_id=<?php echo $post->ID; ?>',
						success:function(results) {
							
						}
					});
				});
				
				//On Submitting remove from favorite form
				jQuery('.removefromfav').live('click',function(){
					jQuery.ajax({
						url:ajaxUrl,
						type:'POST',
						data:'action=tevolution_settings_remove_from_fav&post_id=<?php echo $post->ID; ?>',
						success:function(results) {
							
						}
					});
				});
			});
			
			//function to toggle statistics html on user dasboard.
			function show_statistics(pid)
			{
				jQuery("#statistics_"+pid).toggle();
			}
			var months = [];
			 months[1] = '<?php _e('January',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[2] = '<?php _e('February',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[3] = '<?php _e('March',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[4] = '<?php _e('April',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[5] = '<?php _e('May',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[6] = '<?php _e('June',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[7] = '<?php _e('July',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[8] = '<?php _e('August',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[9] = '<?php _e('September',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[10] = '<?php _e('October',TEVOLUTION_STATISTCS_DOMAIN);?>';
			 months[11] = '<?php _e('November',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			 months[12] = '<?php _e('December',TEVOLUTION_STATISTCS_DOMAIN); ?>';
			//function to show statistics month wise.
			function show_month_stats(month,pid)
			{
				jQuery( "."+pid+" a" ).each(function() {
					jQuery( "."+pid+" a" ).removeClass( "active" );
					
				});
				jQuery("#"+month+"_"+pid ).addClass( "active" );
				var year = '';
				if(document.getElementById('statistics_year_'+pid))
				{
					year = jQuery('#statistics_year_'+pid).val()
				}else
				{
					var d = new Date();
					year = d.getFullYear(); 
				}
				jQuery.ajax({
					url:ajaxUrl,
					type:'POST',
					data:'action=tevolution_show_month_stats&month='+month+"&year="+year+"&post_id="+pid,
					success:function(results) {
						var result = results.split("_");
						
						
						jQuery("#month_show_statistics_"+pid).html(months[month]+" "+year+" "+'<?php _e('Statistics',TEVOLUTION_STATISTCS_DOMAIN); ?>');
						jQuery("#total_post_count_"+pid).html(result[0]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#front_page_post_count_"+pid).html(result[1]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#category_page_post_count_"+pid).html(result[2]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#send_enquiry_post_count_"+pid).html(result[3]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#send_friend_post_count_"+pid).html(result[4]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#add_to_fav_post_count_"+pid).html(result[5]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
						jQuery("#search_page_post_count_"+pid).html(result[6]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
					}
				});
				return true;
			}
			//function to show statistics year wise.
			function show_year_stats(year,pid)
			{
				var active_id=jQuery("."+pid+" a.active").attr('id');
				var month = active_id.split("_");
				month = month[0];
				jQuery( "."+pid+" a" ).each(function() {
					jQuery( "."+pid+" a" ).removeClass( "active" );
					
				});
				
				jQuery.ajax({
					url:ajaxUrl,
					type:'POST',
					data:'action=tevolution_show_month&month='+month+"&year="+year+"&post_id="+pid,
					success:function(results) {
						jQuery('.current_year_'+pid).html(results);
						
						if(jQuery("."+pid+" a.active").length > 0)
						{
							var current_active_id=jQuery("."+pid+" a.active").attr('id');
							var current_month = current_active_id.split("_");
							current_month = current_month[0];
						}
						else
						{
							var d = new Date();
							var current_month = d.getMonth()+1; 
							jQuery("#"+current_month+"_"+pid ).addClass( "active" );
						}
						
						jQuery.ajax({
							url:ajaxUrl,
							type:'POST',
							data:'action=tevolution_show_month_stats&month='+current_month+"&year="+year+"&post_id="+pid,
							success:function(results) {
								var result = results.split("_");
								jQuery("#month_show_statistics_"+pid).html(months[current_month]+" "+jQuery('#statistics_year_'+pid).val()+" "+'<?php _e('Statistics',TEVOLUTION_STATISTCS_DOMAIN); ?>');
								jQuery("#total_post_count_"+pid).html(result[0]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#front_page_post_count_"+pid).html(result[1]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#category_page_post_count_"+pid).html(result[2]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#send_enquiry_post_count_"+pid).html(result[3]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#send_friend_post_count_"+pid).html(result[4]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#add_to_fav_post_count_"+pid).html(result[5]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
								jQuery("#search_page_post_count_"+pid).html(result[6]+" <?php _e('Visits',TEVOLUTION_STATISTCS_DOMAIN); ?>");
							}
						});
					}
				});
				return true;
			}
		</script>
	<?php
}
?>