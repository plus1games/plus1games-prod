<?php
/*
Plugin Name: Social Networks Widget
Description: Adds a widget which lets you link to social media presences
Author: Frankie Roberto
Author URI: http://www.frankieroberto.com
Version: 0.2
*/

class SocialNetworksWidget extends WP_Widget {

	function SocialNetworksWidget() {
		$widget_ops = array('classname' => 'social_networks_widget', 'description' => __('Link to your social networks.'));
		$this->WP_Widget('social_networks', __('Social Networks'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$title_link = strip_tags($instance['title_link']);		
    if (!empty( $title_link) ) { $title_page = get_post($title_link); }
		
		$networks['Twitter'] = $instance['twitter'];
		$networks['Technorati'] = $instance['technorati'];
		$networks['Facebook'] = $instance['facebook'];
		$networks['LinkedIn'] = $instance['linkedin'];
		$networks['Flickr'] = $instance['flickr'];
		$networks['YouTube'] = $instance['youtube'];
		$networks['StumbleUpon'] = $instance['stumbleupon'];
		$networks['Delicious'] = $instance['delicious'];
		$networks['Skype'] = $instance['skype'];
		$networks['Apple'] = $instance['apple'];
		$networks['Digg'] = $instance['digg'];
		$networks['Google'] = $instance['google'];
		$networks['Reddit'] = $instance['reddit'];
		$networks['Rss'] = $instance['rss'];
		$networks['Email'] = $instance['email'];
		$networks['Dopplr'] = $instance['dopplr'];
		$networks['Ember'] = $instance['ember'];
		

		$display = $instance['display'];
		

		echo $before_widget;
		echo $before_title;
		if (!empty( $title_link) ) { echo "<a href=\"" . get_permalink($title_page->ID) . "\">"; } 
		  if (empty( $title) ) { echo $title_page->post_title;}
		  else { echo($title); }
		if (!empty( $title_link) ) { echo "</a>"; } 
		echo $after_title;
		?>
		
			<ul class="social-networks">
				
					<?php foreach(array("Delicious", "Facebook", "Flickr", "Twitter", "Technorati", "StumbleUpon", "YouTube", "Skype", "Apple", "Digg", "Google", "Reddit", "Rss", "Email", "LinkedIn", "Dopplr", "Ember") as $network) : ?>
			    <?php if (!empty($networks[$network])) : ?><li><a style="background-image:url(<?php bloginfo('siteurl'); ?>/wp-content/plugins/social-networks-widget/images/<?php echo strtolower($network);?>.png); background-repeat:no-repeat; width:46px; height:46px; display:block;" rel="external" target="_blank" title="<?php echo strtolower($network);?>" href="<?= $networks[$network] ?>"><?php if (($display == "both") or ($display =="icons")) { ?>
			    <?php } if (($display == "labels") or ($display == "both")) {?> <?php echo $network; ?><?php } ?></a></li><?php endif; ?>
					<?php endforeach; ?>
			      
      </ul>
      
		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['title_link'] = $new_instance['title_link'];
		$instance['twitter'] = $new_instance['twitter'];
		$instance['technorati'] = $new_instance['technorati'];
		$instance['facebook'] = $new_instance['facebook'];
		$instance['stumbleupon'] = $new_instance['stumbleupon'];
		$instance['linkedin'] = $new_instance['linkedin'];
		$instance['flickr'] = $new_instance['flickr'];
		$instance['youtube'] = $new_instance['youtube'];
		$instance['delicious'] = $new_instance['delicious'];
		$instance['skype'] = $new_instance['skype'];
		$instance['apple'] = $new_instance['apple'];
		$instance['digg'] = $new_instance['digg'];
		$instance['google'] = $new_instance['google'];
		$instance['reddit'] = $new_instance['reddit'];
		$instance['rss'] = $new_instance['rss'];
		$instance['email'] = $new_instance['email'];
		$instance['dopplr'] = $new_instance['dopplr'];
		$instance['ember'] = $new_instance['ember'];

		$instance['display'] = $new_instance['display'];

		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '', 'title_link' => '' ) );
		$title = strip_tags($instance['title']);
		$title_link = strip_tags($instance['title_link']);		
		$twitter = $instance['twitter'];
		$technorati = $instance['technorati'];		
		$facebook = $instance['facebook'];
		$stumbleupon = $instance['stumbleupon'];		
		$linkedin = $instance['linkedin'];		
		$youtube = $instance['youtube'];
		$flickr = $instance['flickr'];
		$delicious = $instance['delicious'];
		$skype = $instance['skype'];
		$apple = $instance['apple'];
		$digg = $instance['digg'];
		$google = $instance['google'];
		$reddit = $instance['reddit'];
		$rss = $instance['rss'];
		$email = $instance['email'];
		$dopplr = $instance['dopplr'];
		$ember = $instance['ember'];
		
		$display = $instance['display'];		


		$text = format_to_edit($instance['text']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

    <p><label for="<?php echo $this->get_field_id('title_link'); ?>"><?php _e('Title link'); ?></label>     
    <?php wp_dropdown_pages(array('selected' => $title_link, 'name' => $this->get_field_name('title_link'), 'show_option_none' => __('None'), 'sort_column'=> 'menu_order, post_title'));?>
    </p>

		<p><label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e('Twitter URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo esc_attr($twitter); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('technorati'); ?>"><?php _e('Technorati URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('technorati'); ?>" name="<?php echo $this->get_field_name('technorati'); ?>" type="text" value="<?php echo esc_attr($technorati); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e('Facebook URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="text" value="<?php echo esc_attr($facebook); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('stumbleupon'); ?>"><?php _e('StumbleUpon URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('stumbleupon'); ?>" name="<?php echo $this->get_field_name('stumbleupon'); ?>" type="text" value="<?php echo esc_attr($stumbleupon); ?>" /></p>
    
		<p><label for="<?php echo $this->get_field_id('linkedin'); ?>"><?php _e('LinkedIn URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('linkedin'); ?>" name="<?php echo $this->get_field_name('linkedin'); ?>" type="text" value="<?php echo esc_attr($linkedin); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('flickr'); ?>"><?php _e('Flickr URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('flickr'); ?>" name="<?php echo $this->get_field_name('flickr'); ?>" type="text" value="<?php echo esc_attr($flickr); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('delicious'); ?>"><?php _e('Delicious URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('delicious'); ?>" name="<?php echo $this->get_field_name('delicious'); ?>" type="text" value="<?php echo esc_attr($delicious); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('youtube'); ?>"><?php _e('YouTube URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" type="text" value="<?php echo esc_attr($youtube); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('skype'); ?>"><?php _e('Skype URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('skype'); ?>" name="<?php echo $this->get_field_name('skype'); ?>" type="text" value="<?php echo esc_attr($skype); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('apple'); ?>"><?php _e('Apple URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('apple'); ?>" name="<?php echo $this->get_field_name('apple'); ?>" type="text" value="<?php echo esc_attr($apple); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('digg'); ?>"><?php _e('Digg URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('digg'); ?>" name="<?php echo $this->get_field_name('digg'); ?>" type="text" value="<?php echo esc_attr($digg); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('google'); ?>"><?php _e('Google URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('google'); ?>" name="<?php echo $this->get_field_name('google'); ?>" type="text" value="<?php echo esc_attr($google); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('reddit'); ?>"><?php _e('Reddit URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('reddit'); ?>" name="<?php echo $this->get_field_name('reddit'); ?>" type="text" value="<?php echo esc_attr($reddit); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('rss'); ?>"><?php _e('Rss URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('rss'); ?>" name="<?php echo $this->get_field_name('rss'); ?>" type="text" value="<?php echo esc_attr($rss); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Dopplr URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('dopplr'); ?>" name="<?php echo $this->get_field_name('dopplr'); ?>" type="text" value="<?php echo esc_attr($dopplr); ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('ember'); ?>"><?php _e('Ember URL:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('ember'); ?>" name="<?php echo $this->get_field_name('ember'); ?>" type="text" value="<?php echo esc_attr($ember); ?>" /></p>
		


		<p>Display:</p>
		<label for="<?php echo $this->get_field_id('icons'); ?>"><input type="radio" name="<?php echo $this->get_field_name('display'); ?>" value="icons" id="<?php echo $this->get_field_id('icons'); ?>" <?php checked($display, "icons"); ?>></input>  Icons</label>
		<label for="<?php echo $this->get_field_id('labels'); ?>"><input type="radio" name="<?php echo $this->get_field_name('display'); ?>" value="labels" id="<?php echo $this->get_field_id('labels'); ?>" <?php checked($display, "labels"); ?>></input> Labels</label>
		<label for="<?php echo $this->get_field_id('both'); ?>"><input type="radio" name="<?php echo $this->get_field_name('display'); ?>" value="both" id="<?php echo $this->get_field_id('both'); ?>" <?php checked($display, "both"); ?>></input> Both</label>

    
<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("SocialNetworksWidget");'));


?>