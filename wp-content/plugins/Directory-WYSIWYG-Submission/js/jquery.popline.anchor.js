/*
*	TypeWatch 2.2
*
*	Examples/Docs: github.com/dennyferra/TypeWatch
*	
*  Copyright(c) 2013 
*	Denny Ferrassoli - dennyferra.com
*   Charles Christolini
*  
*  Dual licensed under the MIT and GPL licenses:
*  http://www.opensource.org/licenses/mit-license.php
*  http://www.gnu.org/licenses/gpl.html
*/

(function(jQuery) {
	
	jQuery.ajaxSetup({
			async: true
		});
	jQuery.fn.typeWatch = function(o) {		
		// The default input types that are supported
		var _supportedInputTypes =
			['TEXT', 'TEXTAREA', 'PASSWORD', 'TEL', 'SEARCH', 'URL', 'EMAIL', 'DATETIME', 'DATE', 'MONTH', 'WEEK', 'TIME', 'DATETIME-LOCAL', 'NUMBER', 'RANGE','SPAN'];
		// Options
		var options = jQuery.extend({
			wait: 750,
			callback: function() { },
			highlight: true,
			captureLength: 2,
			inputTypes: _supportedInputTypes
		}, o);
		function checkElement(timer, override) {
			if(jQuery(timer.el).is("input")){
				var value = jQuery(timer.el).val();
			}else{
				var value = jQuery(timer.el).html();
			}
			// Fire if text >= options.captureLength AND text != saved text OR if override AND text >= options.captureLength			
			if ((value.length >= options.captureLength && value.toUpperCase() != timer.text)
				|| (override && value.length >= options.captureLength))
			{
				timer.text = value.toUpperCase();
				timer.cb.call(timer.el, value);
			}
		};
		function watchElement(elem) {			
			if(jQuery(elem).is("input")){
				var elementType = elem.type.toUpperCase();
			}else{
				var elementType = jQuery(elem).prop('tagName');
			}
			if (jQuery.inArray(elementType, options.inputTypes) >= 0) {
				// Allocate timer element
				var timer = {
					timer: null,
					text: jQuery(elem).val().toUpperCase(),
					cb: options.callback,
					el: elem,
					wait: options.wait
				};
				// Set focus action (highlight)
				if (options.highlight) {
					jQuery(elem).focus(
						function() {
							this.select();
						});
				}
				// Key watcher / clear and reset the timer
				var startWatch = function(evt) {
					var timerWait = timer.wait;
					var overrideBool = false;
					if(jQuery(this).is("input")){
						var evtElementType = this.type.toUpperCase();
					}else{
						var evtElementType = jQuery(this).prop('tagName');
					}					
					// If enter key is pressed and not a TEXTAREA and matched inputTypes
					/*if (typeof evt.keyCode != 'undefined' && evt.keyCode == 13 && evtElementType != 'TEXTAREA' && jQuery.inArray(evtElementType, options.inputTypes) >= 0) {
						timerWait = 1;
						overrideBool = true;
					}*/
					var timerCallbackFx = function() {
						checkElement(timer, overrideBool)
					}
					// Clear timer					
					clearTimeout(timer.timer);
					timer.timer = setTimeout(timerCallbackFx, timerWait);
				};
				jQuery(elem).on('keydown paste cut input', startWatch);
			}
		};
		// Watch Each Element
		return this.each(function() {
			watchElement(this);
		});
	};
})(jQuery);
// JavaScript Document

jQuery('input.oembed_video_text').live('keyup', function (e) {	
	var id=jQuery(this).attr('id');
	//searchInput = jQuery('input.oembed_video_text');
	searchInput = jQuery('input#'+id);
	searchInput.typeWatch({
		callback: function(){			
			if(e.keyCode!='13' && e.keyCode!='86'){				
				var value= jQuery('input.oembed_video_text').attr('value');
				var data_class= jQuery('input.oembed_video_text').attr('data-class');
				jQuery("input[name='"+data_class+"']").val(value);
				jQuery.ajax({
					url:ajaxUrl,
					type:'POST',
					async: true,
					data:'action=convert_oembed_video&video='+value,
					success:function(results){				
						jQuery("."+data_class).html(results);
						jQuery('span.oembed_video_span').remove();
						return false; 
					}
				});
			}
			return false;
			
		},
		wait: 300,
		highlight: false,
		captureLength: 0
	});
});

jQuery('.frontend_link').live( 'click focus', function(e) {
	var $_this = jQuery(this);
 	var href= jQuery(this).attr('href');
 	var id= jQuery(this).attr('id'); 	
 	jQuery('.anchor_btn').remove();
 	jQuery('.frontend_anchor_'+id).remove();
 	jQuery(this).after('<span class="frontend_anchor_'+id+' anchor_btn popline popline_btn frontend_edit_anchor"><i class="fa fa-link"></i><input data-class="'+id+'" class="frontend_edit_text textfield" value="'+href+'" type="text" placeholder="http://" style="display: inline;"></span>');
 	e.stopPropagation();
	return false;
});

/* frontend link related contenteditable text will not remove */
jQuery('a.frontend_link').live("keypress", function(e) {    
    if(e.keyCode==8 || e.keyCode==46){    	
    	e.preventDefault();
    }
});

/*inserted anchor text change in anchor href on keyup event */
jQuery('input.frontend_edit_text').live("keyup", function(e) {
    //if (e.keyCode == 13) {
    	var value= jQuery(this).attr('value');
    	var data_class= jQuery(this).attr('data-class');            
        jQuery("a#"+data_class).attr("href", value);        
        //jQuery('.anchor_btn').remove();
        //return false; 
    //}
});
/*inserted anchor text change in anchor href on keypress event */
jQuery('input.frontend_edit_text').live("keypress", function(e) {
    if (e.keyCode == 13) {
    	var value= jQuery(this).attr('value');
    	var data_class= jQuery(this).attr('data-class');            
        jQuery("a#"+data_class).attr("href", value);        
        jQuery('.anchor_btn').remove();
        return false; 
    }
});


/* 
 * Function Name: frontend_edit_value
 * Return: display selected value or option in display block
 */
function frontend_edit_value(th,type,id){
	var value='';
	if(type=="radio"){
		value= jQuery('#'+th.id).attr('data-value');
		jQuery('#'+id).html(value);
		jQuery('.frontend_edit_input').hide();
	}
	if(type=='select'){		
		value=jQuery("#"+th.id +" option:selected").html();		
		jQuery('#'+id).html(value);
		jQuery('.frontend_edit_input').hide();
	}

	if(type=="multicheckbox"){		 
		jQuery("input[name='"+th.name+"']").each( function () {			
			if (jQuery(this).attr('checked'))
			{
				value+=jQuery(this).attr('data-value')+', ';
			}
		});
		jQuery('#'+id).html(value.slice(0, value.lastIndexOf(",")));
	}

}


jQuery('.frontend_text').keypress(function(e){	
	if (e.keyCode == 13) {		
		e.preventDefault();
	}
});
/* Contenteditable  only allowed pain text */
var contenteditable = document.querySelectorAll("[contenteditable]");
for(var x=0; x<contenteditable.length; x++)
{	
	contenteditable[x].addEventListener("paste", function(e) {
		var text = e.clipboardData.getData("text/plain");
		if(text!="")
		{
			e.preventDefault();		
			document.execCommand("insertHTML", false, text);
		}
	});
}
/*assign tabindex event on frontend class */
jQuery(function() {
	var tabindex = 1;
	jQuery('#content').find('[class*=frontend]').filter(function() {
	    var $input = jQuery(this);	    
	    $input.attr("tabindex", tabindex);
	    tabindex++;
	});	
});


function frontend_check_validation(e,validation,name,error_msg){	
	
	var number_keycode = ["96", "97", "98","99","100","101","102","103","104","105"];
	var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{1,4})$/;
	var value=(jQuery('.'+name).attr('href')!=undefined)  ? jQuery('.'+name).attr('href')  :jQuery('.'+name).text();	

	if(validation=='phone_no'){		
		if(jQuery('.'+name).text().length > 15 && (e.keyCode!= 8 && e.keyCode!= 46)){
			e.preventDefault();
		}		
		if((e.which > 57 || e.which < 48 ) && !inArray(e.which,number_keycode) && (e.keyCode!= 8 && e.keyCode!= 9 && e.keyCode!= 86 && e.keyCode!= 36 && e.keyCode!= 46 && e.keyCode!= 37 && e.keyCode!= 38 && e.keyCode!= 39 && e.keyCode!= 40 && e.which!=43 && e.which!=40 && e.which!=41 && e.which!=173 && e.which!=189 ) && fronteditor_disableCtrlKeyCombination(e)){
			e.preventDefault();
		}
	}
	if(validation=='digit'){
		if((e.which > 57 || e.which < 48 ) && !inArray(e.which,number_keycode) && (e.keyCode!= 8 && e.keyCode!= 9 && e.keyCode!= 86 && e.keyCode!= 36 && e.keyCode!= 46 && e.keyCode!= 37 && e.keyCode!= 38 && e.keyCode!= 39 && e.keyCode!= 40 ) && fronteditor_disableCtrlKeyCombination(e)){
			e.preventDefault();
		}
	}
	
	if(validation=='email'){		
		if(jQuery.trim(value)==''){
			jQuery('#error_'+name).remove();
		    jQuery('.'+name).after('<span id="error_'+name+'" class="message_error2 frontend_error">'+error_msg+'</span>');
		}else if (!reg.test(jQuery.trim(value))) {		    
		    jQuery('#error_'+name).remove();
		    jQuery('.'+name).after('<span id="error_'+name+'" class="message_error2 frontend_error">'+email_valid_address+'</span>');
		}else{
		 	jQuery('#error_'+name).remove();
		}
	}
	if(validation=='require' || validation=='phone_no' || validation=='digit'){		
		if(jQuery.trim(value)==''){
			jQuery('.error_'+name).remove();
		    jQuery('.'+name).after('<span id="error_'+name+'" class="message_error2 frontend_error error_'+name+'">'+error_msg+'</span>');		
		}else{			
		 	jQuery('.error_'+name).remove();
		}
	}
	if (e.keyCode == 13) {e.preventDefault();}	
}


/* Hide custom fields on out side click like multicheckbox, select box, radio button and anchor link */

jQuery(document).click(function(event) {
	var id = event.target.id;
	var classname = event.target.className;
	if(classname !='frontend_edit_text textfield'){
		jQuery('.frontend_edit_anchor').hide();
	}

	if(classname !='frontend_edit_text oembed_video_text textfield' && classname!="frontend_oembed_video button"){		
		jQuery('.oembed_video_span').hide();
	}

	result = id.search("frontend_");	
	
	if((classname!="hr_input_radio" && classname!='form_row frontend_edit_input' && result=="-1" && classname!='')){
		jQuery('.frontend_edit_input').hide();
	}
	result = classname.search("frontend_");
	if(id=="" && result=='-1'){
		jQuery(".frontend_edit_input_checkbox").hide();
	}
	if(classname==''){
		//jQuery('.editblock div.templatic-editor').removeClass('show_editor');
	}
});


/*javascript array check function  */
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

/* Disable Ctril + key  */
function fronteditor_disableCtrlKeyCombination(e)
{
	//list all CTRL + key combinations you want to disable
	var forbiddenKeys = new Array('a', 'c', 'x', 'v', 'j');
	var key;
	var isCtrl;

	if(window.event)
	{
		key = window.event.keyCode;     //IE
		if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
	}
	else
	{
		key = e.which;     //firefox
		if(e.ctrlKey)
			isCtrl = true;
		else
			isCtrl = false;
	}

	//if ctrl is pressed check if other key is in forbidenKeys array
	if(isCtrl)
	{
		for(i=0; i<forbiddenKeys .length; i++)
		{
			//case-insensitive comparation
			if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
			{				
				return false;
			}
		}
	}
	return true;
}

jQuery('.editblock div.templatic-element').live( 'click focus touch', function(e) {
	var index = jQuery(this).parent().attr("class");
	var data = index.split(' ');
	jQuery('.'+data[0]+' div.templatic-editor').addClass('show_editor');
});

jQuery('div').not("div.templatic-element").bind('click', function(e) {
	var index = jQuery(this).attr("class");	
	var flg=0;
	if(jQuery.trim(index)!='' &&  jQuery.trim(index).length>0){
		var data = index.split(' ');
		if(!inArray('editblock',data)){
			flg=1;		
		}
	}
    
	if(flg==1 && window.getSelection()==''){
		jQuery('.editblock div.templatic-editor').removeClass('show_editor');
	}
});

jQuery( "#frontend_address" ).focusout(function() {
	jQuery( ".address_yellow_panel" ).hide();	
});
jQuery( "#frontend_address" ).click(function() {
	jQuery( ".address_yellow_panel" ).show();	
});


jQuery(document).ready(function(){
	searchInput = jQuery('#frontend_address');
	searchInput.typeWatch({
		callback: function(){
			geocode();
		},
		wait: 1000,
		highlight: false,
		captureLength: 0
	});
});

