<?php
/*
 * frontend edit submit form work as default submit page first select catgeory then after select price package
 */

function frontend_edit_submit_form_shortcode($atts){
	extract( shortcode_atts( array (
			'post_type'   =>'post',				
			), $atts ) 
		);	
	ob_start();	
	remove_filter( 'the_content', 'wpautop' , 12);

	global $wpdb,$post,$current_user,$all_cat_id,$monetization,$validation_info;
	/* set the submit post type on submit form page */
	if(get_post_meta($post->ID,'submit_post_type',true)=="" || $post_type!=get_post_meta($post->ID,'submit_post_type',true)){
		update_post_meta($post->ID,'submit_post_type',$post_type);	
	}
	
	if(get_post_meta($post->ID,'is_frontend_submit_form',true)=="" || '1'!=get_post_meta($post->ID,'is_frontend_submit_form',true)){
		update_post_meta($post->ID,'is_frontend_submit_form',1);		
		update_post_meta($post->ID,'is_tevolution_submit_form',1);	
	}
	
	
	/*check internet explore version and redirect original submit form its less then 10 version */
	$browser_details=getBrowser();	
	if(($browser_details['name']=='Internet Explorer' &&  version_compare( $browser_details['version'], 10, '<')) || (function_exists('tmpl_wp_is_mobile') && tmpl_wp_is_mobile())){
		$args=array(	
		'post_type' => 'page',
		'post_per_page'=> -1,
		'post_status' => 'publish',							
		'meta_query' => array(
							array(
								'key' => 'is_tevolution_submit_form',
								'value' => '1',
								'compare' => '='
								),												
							array(
								'key' => 'submit_post_type',
								'value' =>  $post_type,
								'compare' => '='
								)
							)
		);
		remove_all_actions('posts_where');
		$the_query  = new WP_Query( $args );
		if( $the_query->have_posts()):
			while ( $the_query->have_posts() ) : $the_query->the_post();						
				if(@$post->ID != "" && !get_post_meta($post->ID,'is_frontend_submit_form',true)):
					$page_id=$post->ID;
					if(is_plugin_active('sitepress-multilingual-cms/sitepress.php') && function_exists('icl_object_id')){
						$page_id = icl_object_id( $post->ID, 'page', false, ICL_LANGUAGE_CODE );					
					}
				endif;	
			endwhile;
		endif;	

		wp_redirect(get_permalink($post_id));
		exit;
	}

	do_action('submit_form_before_content');

	$tmpdata = get_option('templatic_settings');

	/*
	 * fetch ip address for security reason
	 */
	$ip = (function_exists('templ_fetch_ip'))?templ_fetch_ip():'';
	if($ip != ""){ 
		echo '<div class="error_msg">';
		_e(IP_BLOCK,FE_DOMAIN);
		echo '</div>';
		return ob_get_clean();
	}
	$custom_post_type=get_option('templatic_custom_post');		
	$post_type_label=strtolower($custom_post_type[$post_type]['labels']['singular_name']);
	?>
	<div class="submit-progress-steps columns">
		<ul>
			<?php if(is_user_logged_in()):?>
				<li><span class="active"><?php _e('1. Enter mandatory fields',FE_DOMAIN);?></span></li>
			<?php else:?>
				<li><span class="active"><?php _e('1. Login/Register',FE_DOMAIN);?></span></li>
		 	<?php endif;?>
			<li><span><?php echo sprintf(__('2. Add your %s',FE_DOMAIN),$post_type_label);?></span></li>
			<li><span><?php echo _e('3. Pay & Publish',FE_DOMAIN);?></span></li>
		</ul>
	</div>
	<?php
	
	/* Display invalid captch message  */
	$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));
	$taxonomy = $taxonomies[0];

	$edit_id =(isset($_REQUEST['pid']) && $_REQUEST['pid']!='')?$_REQUEST['pid']: '';
	/*Fetch category id */
	if(isset($edit_id) && $edit_id !=''){
		global $monetization,$cat_array;		
		$get_category = wp_get_post_terms($edit_id,$taxonomy);
		$all_cat_price=0;
		foreach($get_category as $_get_category)
		{
			$cat_array[] = $_get_category->term_id;
			if($_get_category->term_price!=''){
				$all_cat_price+=$_get_category->term_price;
			}
		}
		$all_cat_id = implode(',',$cat_array);		
	}
	
	/*Form validation information */
	$form_fields=front_end_fetch_submit_page_form_fields($taxonomy);	
	$validation_info = array();
	foreach($form_fields as $key=>$val)
	{
		if($key!="category")
			continue;
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			continue;
		}	
		$str = ''; $fval = '';
		$field_val = $key.'_val';
		$val['title']=(isset($val['title']))? $val['title'] :'';		
		$validation_info[] = array(
							'title'	       => $val['title'],
							'name'	       => $key,
							'espan'	       => $key.'_error',
							'type'	       => $val['type'],
							'text'	       => $val['text'],
							'is_require'	  => @$val['is_require'],
							'validation_type'=> $val['validation_type']
					);
	}	

	if(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit']==1){
    	$_SESSION['custom_fields']['total_price']=get_post_meta($edit_id,'total_price',true);
    	if($all_cat_price!=0){
    		$_SESSION['custom_fields']['all_cat_price']=$all_cat_price;
    	}
    	$package_select=get_post(get_post_meta($edit_id,'package_select',true));
    	$feature_amount=get_post_meta($package_select->ID,'feature_amount',true);
    	$feature_cat_amount=get_post_meta($package_select->ID,'feature_cat_amount',true);
    	if(get_post_meta($edit_id,'featured_h',true)){
    		$_SESSION['custom_fields']['featured_h']=$feature_amount;
    	}else{
    		unset($_SESSION['custom_fields']['featured_h']);
    	}
    	if(get_post_meta($edit_id,'featured_c',true)){
    		$_SESSION['custom_fields']['featured_c']=$feature_cat_amount;
    	}else{
    		unset($_SESSION['custom_fields']['featured_c']);
    	}
    }
	global $submit_form_validation_id,$cat_array;
	$submit_form_validation_id = "submit_form";
	?>
    <script type="text/javascript">
	var currency = '<?php echo get_option('currency_symbol'); ?>';
	var position = '<?php echo get_option('currency_pos'); ?>';
	var pkg_price = parseFloat(0);
	var edit = 1;
	</script>
    <?php
	do_action('action_before_html');
	echo '<form name="submit_form" id="submit_form" class="form_front_style frontend_edit_submit_form" action="'.$form_action_url.'" method="post" enctype="multipart/form-data">';
	
	echo '<div class="accordion" id="post-listing" >';
	/* price package will be display after activate monetization.*/
	
	global $monetization;
	$user_have_pkg = $monetization->templ_get_packagetype($current_user->ID,$post_type); /* User selected package type*/
	$user_last_postid = $monetization->templ_get_packagetype_last_postid($current_user->ID,$post_type); /* User last post id*/
	$user_have_days = $monetization->templ_days_for_packagetype($current_user->ID,$post_type); /* return alive days(numbers) of last selected package  */
	$is_user_have_alivedays = $monetization->is_user_have_alivedays($current_user->ID,$post_type); /* return user have an alive days or not true/false */

	if($current_user->ID)// check user wise post per  Subscription limit number post post 
	{
		$package_id=get_user_meta($current_user->ID,$post_type.'_package_select',true);// get the user selected price package id
		$user_limit_post=get_user_meta($current_user->ID,$post_type.'_list_of_post',true); //get the user wise limit post count on price package select
		$package_limit_post=get_post_meta($package_id,'limit_no_post',true);// get the price package limit number of post
		$user_have_pkg = get_post_meta($package_id,'package_type',true); 
	}
	
	do_action('action_before_price_package',$post_type,$post->ID);/*hook before showing price package*/
				
	/* if You have successfully activated monetization then this function will be included for listing prices */
	if(class_exists('monetization') && function_exists('is_price_package') && is_price_package($current_user->ID,$post_type,$post->ID) > 0)
	{			
		global $monetization;
		/*while edit a listing do not show packages*/
		if((isset($edit_id) && $edit_id !='' && (isset($_REQUEST['renew'])) || ($_REQUEST['backandedit'] && $_REQUEST['backandedit'] == 1)) || (isset($edit_id) && $edit_id=='') )
		{
			/*fetch the price package*/			
			$user_have_pkg = $monetization->tmpl_fetch_price_package($current_user->ID,$post_type,$post->ID);
			echo "<input type='hidden' id='is_user_select_subscription_pkg' name='is_user_select_subscription_pkg' value='1' >";
		}
	}	
	do_action('action_after_price_package',$post_type,$post->ID);/*hook after showing price package*/		

	echo '<input type="hidden" name="total_price" id="total_price" value="'.get_post_meta($user_last_postid,'total_price',true).'" />';
	
	/* Display select category and location country, zone and city dropdown */
	echo ' <div id="step-post" class="accordion-navigation step-wrapper step-post">';
	?>
        <a class="step-heading active" href="#"><span>2</span><span><?php _e('Enter Details',DOMAIN); ?></span><span><i class="fa fa-caret-down"></i><i class="fa fa-caret-right"></i></span></a>
        <div id="post" class="step-post content <?php echo $active; ?> clearfix">
    <?php
	echo "<h3>".__('Select Category',FE_DOMAIN)."</h3>";
	if($tmpdata['templatic-category_custom_fields'] == 'Yes' ){
		$default_custom_metaboxes = get_post_fields_templ_plugin($post_type,'custom_fields','post');/*custom fields for all category.*/
		
		if(array_key_exists('post_coupons',$default_custom_metaboxes))
			unset($default_custom_metaboxes['post_coupons']);
		
		echo '<div class="cont_box" id= "submit_category_box">';	
		$cat_array=$all_cat_id_array = explode(",",$all_cat_id);
		
		display_custom_category_field_plugin($default_custom_metaboxes,'custom_fields','post',$post_type);//displaty  post category html.
		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit'){			
			display_custom_category_name($default_custom_metaboxes,$all_cat_id_array,$taxonomy);//display selected category name when come for edit .
		}
		echo '</div>';			
	}else{
		$custom_metaboxes = array();
		/* Fetch Heading type custom fields */
		$heading_type = fetch_heading_per_post_type($post_type);					
		if(count($heading_type) > 0)
		{
			foreach($heading_type as $_heading_type){
				$custom_metaboxes[$_heading_type] = get_post_custom_fields_templ_plugin($post_type,$all_cat_id,$taxonomy,$_heading_type);//custom fields for custom post type..
			}
		}else{
			$custom_metaboxes[] = get_post_custom_fields_templ_plugin($post_type,$all_cat_id,$taxonomy,'');//custom fields for custom post type..
		}
		foreach ($custom_metaboxes as $key => $value) {		
			foreach($value as $k=>$v){
				$default_custom_metaboxes[$k]=$v;
			}
			
		}
		echo '<div class="cont_box" id= "submit_category_box">';	
		$cat_array=$all_cat_id_array = explode(",",$all_cat_id);		
		display_custom_category_field_plugin($default_custom_metaboxes,'custom_fields','post',$post_type);//displaty  post category html.
		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit'){
			display_custom_category_name($default_custom_metaboxes,$all_cat_id_array,$taxonomy);//display selected category name when come for edit .
		}
		echo '</div>';
	}
	
	if(array_key_exists('post_coupons',$default_custom_metaboxes))
			unset($default_custom_metaboxes['post_coupons']);
			
	foreach ($default_custom_metaboxes as $key => $value) {
		if($key=='post_city_id' && is_plugin_active('Tevolution-LocationManager/location-manager.php')){
			echo "<h3>".__('Select Location',FE_DOMAIN)."</h3>";
		}		
		do_action('tevolution_custom_fieldtype',$key,$value,$post_type);		
	}

			$coupons = get_posts(array('post_type'=>'coupon_code','post_status'=>'publish')); // show only if coupon available			
			if($coupons)
			{
				if(!isset($_REQUEST['action']) && isset($_REQUEST['action']) !='edit'){
					$coupon_code = '';
					if(@$_REQUEST['backandedit']) { $coupon_code = $_SESSION['custom_fields']['add_coupon']; }else if(isset($edit_id) && $edit_id !=''){ $coupon_code = get_post_meta($edit_id,'add_coupon',true); }else{ $coupon_code = ''; } /* coupon code when click ok GBE*/
					if(function_exists('templ_get_coupon_fields')){
						templ_get_coupon_fields($coupon_code); /* fetch coupon code */
					}
				}
			}
			global $monetization;
			if(class_exists('monetization')){
				if((isset($edit_id) && $edit_id !='' && (!isset($_REQUEST['renew']))) || $is_user_select_subscription_pkg == 1 || (function_exists('is_price_package') && is_price_package($current_user->ID,$post_type,$post->ID) <= 0))
				{
					if(get_post_meta($edit_id,'package_select',true)){
						$packg_id = get_post_meta($edit_id,'package_select',true);
					}
					else{
						$packg_id = get_user_meta($current_user->ID,$post_type.'_package_select',true);
					}
					$monetization->tmpl_fetch_price_package_featured_option($current_user->ID,$post_type,$post->ID,$packg_id,$is_user_select_subscription_pkg);
				}
				else
				{
				?>
					<div style="display:none;" id="show_featured_option">
						<input type="checkbox" value="" id="featured_h" name="featured_h">
						<input type="checkbox" value="" id="featured_c" name="featured_c">
					</div>
				<?php
				}
			}
		/* for how many category can select */
		do_action('action_after_custom_fields',$custom_metaboxes,'custom_fields',$post_type,$_SESSION['package_select']);	
		
		if(is_user_logged_in()){
			templ_captcha_integrate('submit'); /* Display recaptcha in submit form */	
		}
		tevolution_show_term_and_condition(); // show terms and conditions check box
	  ?>
    <input type="hidden" name="frontend_edit_submit_cat" class="frontend_edit_submit_cat" value="1">	
	<input type="hidden" name="cur_post_type" id="cur_post_type" value="<?php echo $post_type; ?>"  />
    <input type="hidden" name="submit_post_type" id="submit_post_type" value="<?php echo $post_type; ?>"  />
	<input type="hidden" name="cur_post_taxonomy" id="cur_post_taxonomy" value="<?php echo $taxonomy; ?>"  />
	<input type="hidden" name="cur_post_id" value="<?php echo $post_id; ?>"  />
	<?php if(isset($edit_id) && $edit_id !=''): ?>
		<input type="hidden" name="pid" id="pid" value="<?php echo $edit_id; ?>"  />
	<?php endif; ?> 
	<?php if(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit']==1):?>
		<input type="hidden" name="backandedit" value="1" />
	<?php endif;?>
	<?php if(isset($_REQUEST['renew']) && $_REQUEST['renew'] !=''): ?>
		<input type="hidden" name="renew" id="renew" value="<?php echo $_REQUEST['renew']; ?>"  />
	<?php endif; 
	global $submit_button;
	if(!isset($submit_button)){ $submit_button = ''; }	
		$submit_label = get_option('templatic_custom_post');
		
		$PostTypeObject = get_post_type_object($post_type);
		$_PostTypeName = $PostTypeObject->labels->name;
		if($current_user->ID=='') {  
			echo '<span class="message_error2 common_error_not_login"></span>';
			echo '<input type="button" id="continue_submit_from" name="continue_submit_from" value="'.__('Continue',DOMAIN).'" '.$submit_button.'/>&nbsp;&nbsp;';
		}
		if($current_user->ID!='') {
		?>
		<input type="submit" name="preview" value="<?php  echo sprintf(__('Add your %s details >>',FE_DOMAIN),$post_type); echo ucfirst($submit_label[get_post_meta($post_id,'submit_post_type',true)]['label']);?>" class="frontend_submit_button normal_button main_btn" <?php echo $submit_button; ?>/>    
	
		<p><?php echo sprintf(__("Manage your %s in next page >>",FE_DOMAIN),$post_type);?></p>
        <span id="frontend_edit_process" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></span>
        <?php } ?>
		<input type="hidden" value="<?php echo @$alive_days;?>" id="alive_days" name="alive_days" >
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'):?>
			<input type="hidden" name="frontend_action" value="<?php echo $_REQUEST['action']?>" />
		<?php endif;?>
		<?php if(isset($_REQUEST['renew']) && $_REQUEST['renew']=='1'):?>
			<input type="hidden" name="frontend_action" value="renew" />
		<?php endif;
		
		if(is_user_logged_in()){
		?>
		<span class="error message_error2" id="frontend_submit_error"></span>
        <span class="message_error2" id="common_error"></span>
	<?php
		}
	echo '</div>';
	echo '</div>';
		/* Finish submit custom fields detail structure*/
		do_action('before_login_register_form',$post_type,$post->ID);
		if($current_user->ID=='') {  
		?>
		 <div id="step-auth" class="accordion-navigation step-wrapper step-auth">
			<a class="step-heading active" href="#"><span id="span_user_login">3</span><span><?php _e('Login / Register',DOMAIN); ?></span><span><i class="fa fa-caret-down"></i><i class="fa fa-caret-right"></i></span></a>
			<div id="auth" class="step-auth content clearfix">
			<?php
				/*display the login and register form while user submit a form without logged in.*/
				$_SESSION['redirect_to']=get_permalink();
				do_action('templ_fecth_login_onsubmit');
				do_action('templ_fetch_registration_onsubmit');
				if($current_user->ID=='') {  
				templ_captcha_integrate('submit'); /* Display recaptcha in submit form */	
				?>
				<input type="submit" name="preview" value="<?php  echo sprintf(__('Add your %s details >>',FE_DOMAIN),$post_type); echo ucfirst($submit_label[get_post_meta($post_id,'submit_post_type',true)]['label']);?>" class="frontend_submit_button normal_button main_btn" <?php echo $submit_button; ?>/>    
			
				<p><?php echo sprintf(__("Manage your %s in next page >>",FE_DOMAIN),$post_type);?></p>
                <span id="frontend_edit_process" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></span>
                <span class="error message_error2" id="frontend_submit_error"></span>
                <span class="message_error2" id="common_error"></span>
				<?php } ?>
			</div>
		 </div>
		 <?php
		}
		do_action('before_payment_option_form',$post_type,$post->ID);
		
		echo '</div>';
		echo '</form>';		
	
	if(file_exists(TEMPLATIC_FRONTEND_DIR.'submition_validation.php'))
		include_once(TEMPLATIC_FRONTEND_DIR.'submition_validation.php');
	
	if(file_exists(TEMPL_REGISTRATION_FOLDER_PATH . 'registration_js.php'))
		include_once(TEMPL_REGISTRATION_FOLDER_PATH . 'registration_js.php');

	/* END OF BLOCK IP CONDITION */
	do_action('submit_form_after_content');
	if(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit'] == 1)
	{
		add_action('wp_footer','save_cat_price',99);
	}
	return ob_get_clean();
}

/* Set the final category price in javascript variable */
function save_cat_price()
{
	?>
    <script>		
		var final_cat_price = <?php echo $_SESSION['custom_fields']['all_cat_price']; ?>;				
	</script>
    <?php
}


if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
	/*
	 * return custom field insert character count limit validation 
	 */
	add_action('wp_footer','frontend_edit_fieldsmonetization_limit_count');
	function frontend_edit_fieldsmonetization_limit_count(){
		if(is_single() && isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			global $post;
			$package_select=get_post_meta($post->ID,'package_select',true);
			$custom_fields= get_frontend_single_post_fields(get_post_type());
			$post_title_limit=get_post_meta($package_select,'post_title_character_limit',true);
			$post_content_limit=get_post_meta($package_select,'post_content_character_limit',true);
			$post_excerpt_limit=get_post_meta($package_select,'post_excerpt_character_limit',true);
			$post_images_limit=get_post_meta($package_select,'max_image',true);
			?>
			<script type="text/javascript">
			<?php if($post_title_limit!=''):?>
			//jQuery('.frontend-entry-title').keypress(function(e){ frontend_check_charcount('frontend-entry-title', <?php echo $post_title_limit;?>, e,'text'); });
			<?php endif;?>
			<?php if($post_content_limit!=''):?>
			jQuery('.frontend-entry-content').keypress(function(e){ frontend_check_charcount('frontend-entry-content', <?php echo $post_content_limit;?>, e,'texteditor'); });    	
			<?php endif;?>
			<?php if($post_excerpt_limit!=''):?>
			jQuery('.frontend-entry-excerpt').keypress(function(e){ frontend_check_charcount('frontend-entry-excerpt', <?php echo $post_excerpt_limit;?>, e,'textarea'); });    	
			<?php endif;
			foreach ($custom_fields as $key => $value) {
				if($key!="category" && $key!="post_title" && $key!="post_excerpt" && $key!="post_excerpt" && $key!="post_images"){
					$count_limit = get_post_meta($package_select,$value['htmlvar_name'].'_character_limit',true);
					if(($value['type']=='text' || $value['type']=='texteditor' || $value['type']=='textarea') && $count_limit!=""){					
						?>
						jQuery('.frontend_<?php echo $value["htmlvar_name"];?>').keypress(function(e){ frontend_check_charcount('frontend_<?php echo $value["htmlvar_name"];?>', <?php echo $count_limit;?>, e,'<?php echo $value["type"];?>'); });
						<?php
					}
				}
			}
			
			/* return display remaining character limit after edit custom fields on frontend edit detail page */
			?>
			function frontend_check_charcount(content_id, max, e,type)
			{       	var textlen =jQuery('.'+content_id).text();
				var remaining_count=0;
				if(e.which != 8 && jQuery.trim(textlen).length > max && (e.keyCode!= 8 && e.keyCode!= 46))
				{            
					e.preventDefault();
				}
				if(type!="text"){        	
					var str = jQuery('.'+content_id).text();
					jQuery('span.span_'+content_id).remove();
					remaining_count=parseInt(max) - parseInt(jQuery.trim(str).length);        	
					jQuery('.'+content_id).before('<span class="span_'+content_id+' clearfix">'+remaining_count+' <?php echo CHARACTERS_LEFT;?></span>');
				}
			}    
		</script>
		<?php
		}
	}
}
?>