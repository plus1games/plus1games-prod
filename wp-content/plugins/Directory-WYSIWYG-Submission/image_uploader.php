<?php
/*
 * This file use for upload image on frontend edit detail page
 */
global $post,$htmlvar_name;
$package_select=get_post_meta($post->ID,'package_select',true);
$post_images_limit=get_post_meta($package_select,'max_image',true);
$tmpdata = get_option('templatic_settings');
$templatic_image_size =  @$tmpdata['templatic_image_size'];
if(!$templatic_image_size){ $templatic_image_size = '50'; }
?>
<script type="text/javascript">
/*Upload single uploader  */
var temp = 1;
var $uc = jQuery.noConflict();
jQuery(function()
{
	var btnUpload=jQuery('#uploadimage');	
	var status=jQuery('#forntend_status');
	var $image_gallery_ids = jQuery('#fontend_image_gallery');
	var $images_gallery = jQuery('#frontend_images_gallery_container ul.frontend_images_gallery');
	var counter=0;	
	new AjaxUpload(btnUpload, {
	name: 'frontend_uploadfile[]',
	action: frontend_plg_url+'uploadfile.php',
	<?php if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php') && $post_images_limit!=''):?>
	data:{images_count:<?php echo $post_images_limit?>,total_count:counter},
	<?php endif;?>
	onSubmit: function(file, ext)
	{
		var file_extension = file.search('.php');
		var file_extension_js = file.search('.js');
		if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext)) || file_extension != -1 || file_extension_js != -1){
		 // extension is not allowed 
		status.text("<?php _e('Only JPG, PNG or GIF files are allowed',FE_DOMAIN); ?>");
		return false;
		}status.text("<?php _e('Uploading...',FE_DOMAIN); ?>");
	},
	
	onComplete: function(file, response)
	{
		
		/*Image size validation*/		
		if(response == 'LIMIT'){
			status.text("<?php _e('Your image size must be less then',FE_DOMAIN); echo " ".$templatic_image_size." "; _e('kilobytes',FE_DOMAIN); ?>");
			return false;
		}
		
		// Start Limit Code
		if(response > 10 )
		{
			status.text("<?php _e('You can upload maximum 10 images',FE_DOMAIN); ?>");
			return false;
		}
		<?php if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php') && $post_images_limit!=''):?>
		if(response > <?php echo $post_images_limit;?> )
		{			
			status.text("<?php _e('You can upload maximum',FE_DOMAIN);echo " ".$post_images_limit." "; _e('images',FE_DOMAIN); ?>");
			return false;
		}
		/* Directory theme wysiwyg plugin issue with field monetization plugin */
		var count=0;
		if(jQuery('#fontend_image_gallery').val() != ''){
			jQuery('#frontend_images_gallery_container ul.frontend_images_gallery li').each(function(){
				count = count + 1;
			});
		}
		
		
		var limit = response.split(",").length-1;
		if(<?php echo $post_images_limit;?> >= 2)
		{
			cnt = count+limit;
		}
		else
		{
			cnt = count;
		}
		
		console.log(count+'=='+limit+'=='+cnt+'====='+'<?php echo $post_images_limit;?>');
		
		if(parseFloat(cnt) > <?php echo $post_images_limit;?> )
		{
			status.text("<?php _e('You can upload maximum',FE_DOMAIN);echo " ".$post_images_limit." "; _e('images',FE_DOMAIN); ?>") ;
			return false;
		}
		<?php endif;?>
		// End Limit Code		
		var spl = response.split(",");
		//On completion clear the status
		status.text('');
		attachment_name=$image_gallery_ids.val();
		var img_name1 = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+spl[0];
		
		jQuery('#frontend_images_gallery_container ul li').each(function(){
		
			if(jQuery(this).attr('data-attachment_id') == 'sample_image.jpg'){
				jQuery(this).remove();
				jQuery('ul.frontend_edit_images_ul').html('<li class="image" data-attachment_id="' + spl[0] + '"><img src="' + img_name1 + '"/></li>');
			}
			
		});
		
		console.log(spl);

		for(var i =0;i<spl.length;i++){

			if((spl.length-1) == i)
			  {
			  }
			else
			  {  	
				var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+spl[i];
				var id_name=spl[i].split("."); 				
				if(jQuery('ul.frontend_edit_images_ul').find( "li" ).length==0){
			  		jQuery('ul.frontend_edit_images_ul').append('<li class="image" data-attachment_id="' + spl[i] + '"><img src="' + img_name + '"/></li>');
			  	}

				$images_gallery.append('\
						<li class="image" data-attachment_id="' + spl[i] + '" data-attachment_src="'+img_name+'">\
							<img src="' + img_name + '" height="120" width="120"/>\
							<span>\
								<a href="#" class="delete" title="'+delete_image+'"><i class="fa fa-times-circle redcross"></i></a>\
							</span>\
						</li>');
				attachment_name = attachment_name ? attachment_name + "," + spl[i] : spl[i];				
				$image_gallery_ids.val( attachment_name );
				
			  }
		}
	}
	
});


	$images_gallery.sortable({
		items: 'li.image',
		//items: ($this.children('li').length>1)? 'li.image' :'',
		cursor: 'move',
		scrollSensitivity:40,
		forcePlaceholderSize: true,
		forceHelperSize: false,
		helper: 'clone',
		opacity: 0.65,
		placeholder: 'wc-metabox-sortable-placeholder',
		start:function(event,ui){
			ui.item.css('background-color','#f6f6f6');
		},
		stop:function(event,ui){
			ui.item.removeAttr('style');
		},
		update: function(event, ui) {
			var attachment_ids = '';
			var j=0;
			jQuery('#frontend_images_gallery_container ul li.image').css('cursor','default').each(function() {
				var attachment_id = jQuery(this).attr( 'data-attachment_id' );
				var attachment_src = jQuery(this).attr( 'data-attachment_src' );
				attachment_ids = attachment_ids + attachment_id + ',';				
				if(attachment_src!="" && attachment_src!='undefined' ){
					var img_name = attachment_src;
				}else{
					var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+attachment_id;
				}
				if(j==0){
					jQuery('ul.frontend_edit_images_ul li').remove();
			  		jQuery('ul.frontend_edit_images_ul').append('<li class="image"><img src="' + img_name + '"/></li>');
			  	}
				j++;
			});
			$image_gallery_ids.val( attachment_ids );
		}
	});

	jQuery('#frontend_images_gallery_container, .frontend_images_gallery').on( 'click', 'a.delete', function() {
		var attachment_ids = '';
		var j=0;		
		jQuery(this).closest('li.image').remove();
		if(jQuery('#frontend_images_gallery_container ul').find( "li" ).length==0){
			jQuery('ul.frontend_edit_images_ul li').remove();
		}
		jQuery('#frontend_images_gallery_container ul li.image').css('cursor','default').each(function() {
			var attachment_id = jQuery(this).attr( 'data-attachment_id' );
			var attachment_src = jQuery(this).attr( 'data-attachment_src' );
			attachment_ids = attachment_ids + attachment_id + ',';
			if(attachment_src!="" && attachment_src!='undefined' ){
				var img_name = attachment_src;
			}else{
				var img_name = '<?php echo bloginfo("template_url")."/images/tmp/"; ?>'+attachment_id;
			}			
			if(j==0){				
				jQuery('ul.frontend_edit_images_ul li').remove();
		  		jQuery('ul.frontend_edit_images_ul').append('<li class="image"><img src="' + img_name + '"/></li>');
		  	}

			j++;
		});		
		$image_gallery_ids.val( attachment_ids );		
		var delete_id=jQuery(this).attr('id');
		if(delete_id!='' && delete_id!='undefined'){			
			jQuery.ajax({
				url:"<?php echo esc_js( get_bloginfo( 'wpurl' ) . '/wp-admin/admin-ajax.php' ); ?>",
				type:'POST',
				data:'action=delete_gallery_image&image_id=' + delete_id,
				success:function(results) {					
				}
			});
		}
		return false;
	} );
});

jQuery(document).ready(function()
{
<?php 
if(!empty($htmlvar_name)){
	foreach($htmlvar_name as $key=>$val){
		if($val['type']=='upload'){
		?>
		
			jQuery(document).ready(function(){
				if(jQuery('#fronted_upload_<?php echo $key;?>').attr('data-src')!=""){                           
					jQuery('#fronted_upload_<?php echo $key;?>').after('<a href="#" id="delete_upload_<?php echo $key;?>" class="delete_upload_file" data-id="fronted_upload_<?php echo $key;?>" data-class="frontend_<?php echo $key;?>"><i class="fa fa-times-circle redcross"></i></a>');
					jQuery('#fronted_upload_<?php echo $key;?>').after('<div class="fronted_upload_<?php echo $key; ?> fileUploadThumbnails"><div><img src="'+jQuery('#fronted_upload_<?php echo $key;?>').attr('data-src')+'"   width="150" height="150"/></div></div>');
				}
											
			  var thumb = jQuery('img#thumb');        					  
			  new AjaxUpload_image('fronted_upload_<?php echo $key;?>', {
				action: frontend_plg_url+'uploadfile.php',
				appendId: 'fronted_upload_<?php echo $key;?>', // id where to append image uploader	
				name: 'file',
				onSubmit: function(file, extension) {
				  jQuery('div.preview').addClass('loading');
				},
				onComplete: function(file, response) {
				  thumb.load(function(){
					jQuery('div.preview').removeClass('loading');
					thumb.unbind();
				  });						
				  thumb.attr('src', response);
					jQuery('.frontend_<?php echo $key;?>').html(response);
					jQuery('#delete_upload_<?php echo $key;?>').remove();
					jQuery('.fronted_upload_<?php echo $key; ?>').remove();
					jQuery('#fronted_upload_<?php echo $key;?>').after('<a href="#" id="delete_upload_<?php echo $key;?>" class="delete_upload_file" data-id="fronted_upload_<?php echo $key;?>" data-class="frontend_<?php echo $key;?>"><i class="fa fa-times-circle redcross"></i></a>');
					jQuery('#fronted_upload_<?php echo $key;?>').after('<div class="fronted_upload_<?php echo $key; ?> fileUploadThumbnails"><div><img src="'+response+'"  width="150" height="150"/></div></div>');
				}
			  });
			  
			});			
		<?php
		}
	}
		
}
?>

	jQuery('body.frontend_editor').on( 'click', 'a.delete_upload_file', function() {
		var clas= jQuery(this).attr('data-class');		
		var data_id= jQuery(this).attr('data-id');		
		var id= jQuery(this).attr('id');		
		jQuery('.'+clas).html('');
		jQuery(this).remove();
		jQuery('.'+data_id).html('');
		return false;
	});
});


// Edit oembed video edit bok
jQuery('.frontend_oembed_video').on( 'click', function() {
	var $_this = jQuery(this); 	
 	var id= jQuery(this).attr('id');
 	var value= jQuery('.'+id).html();
 	jQuery('.oembed_video_span').remove(); 	
 	var value=jQuery("input[name='"+id+"']").val();
 	jQuery(this).after("<span class='oembed_video_span '><i class='fa fa-youtube-play'></i><input id='oembed_"+id+"' data-class='"+id+"' class='frontend_edit_text oembed_video_text textfield' value='"+value+"' type='text'  style='display: inline;'></span>"); 	
});

</script>