<?php
/*
Plugin Name: Directory - WYSIWYG Submission
Plugin URI: http://templatic.com/docs/wysiwyg-submission/
Description: WYSIWYG Submission plugin will allow your website visitors to add new content in a totally new way. To generate a submit page for this plugin go to the <a href="edit.php?post_type=page">Pages</a> section and open the WYSIWYG Submission page.
Author: Templatic
Author URI: http://templatic.com
Version: 1.2.4
*/
define('FRONTEND_EDITOR_VERSION','1.2.4');
@define('FRONTEND_EDITOR_PLUGIN_NAME','Directory - WYSIWYG-Submission Plugin');
define('FRONTEND_EDITOR_SLUG','Directory-WYSIWYG-Submission/directory_WYSIWYG_submission.php');

define( 'TEMPLATIC_FRONTEND_URL', plugin_dir_url( __FILE__ ) );
// Plugin Folder Path
define( 'TEMPLATIC_FRONTEND_DIR', plugin_dir_path( __FILE__ ) );
if (!defined('FE_DOMAIN')) 	
	define('FE_DOMAIN','templatic-frontend');

$locale = get_locale();
load_textdomain( FE_DOMAIN, plugin_dir_path( __FILE__ ).'languages/'.$locale.'.mo' );
@define('CHARACTERS_LEFT',__('characters left',FE_DOMAIN));



include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
/* for auto updates */
if(strstr($_SERVER['REQUEST_URI'],'plugins.php') || strstr($_SERVER['REQUEST_URI'],'update.php') || strstr($_SERVER['REQUEST_URI'],'update-core.php') ){
	require_once('wp-updates-plugin.php');
	new WPUpdatesFrontendEditorUpdater( 'http://templatic.com/updates/api/index.php', plugin_basename(__FILE__) );
}
/* plugin activation hook */

include(dirname(__FILE__)."/frontend_editor_functions.php");
include(dirname(__FILE__)."/single_page_customfields_functions.php");

register_activation_hook(__FILE__, 'frontend_editor_activate_plugin');
register_deactivation_hook(__FILE__, 'frontend_editor_deactivate_plugin');
add_action('admin_init','frontend_editor_activate',20);


/*
 * remove all action for display success post submited information
 */
add_action('init','frontend_edit_remove_actions_filters',0);
function frontend_edit_remove_actions_filters(){
	
	if((isset($_REQUEST['page']) && $_REQUEST['page']=='success') || (isset($_REQUEST['ptype']) && $_REQUEST['ptype']=='return')){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){			
			remove_action('tevolution_submition_success_post_content','tevolution_submition_success_post_submited_content',10);			
			remove_action('payfast_submit_post_details','successfull_return_payfast_post_details',10);
			add_action('tevolution_submition_success_msg','frontend_submition_successful_added_tabs',0);	
			add_action('tevolution_submition_success_msg','frontend_submition_success_msg',11);
			
			add_action('paypal_successfull_return_content','frontend_submition_success_msg',11);
			add_action('after_successfull_return_paypal_process','frontend_submition_successful_added_tabs',0);	

		}

	}

	/* paypal Pro Payment gateway*/
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='paypal_pro_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('paypalpro_successfull_return_content','successfull_return_paypalpro_post_details',10);
		}
	}
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='2co_success'){
		remove_action('2co_submit_post_details','successfull_return_2co_post_details',10);
		add_action('2co_successfull_return_content','frontend_submition_successful_added_tabs',0);	
		add_action('2co_successfull_return_content','frontend_submition_success_msg',11);	
	}
	/* Authorrize dot net */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='authorizedotnet_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('authorizedotnet_submit_post_details','successfull_return_authorizedotnet_post_details',10);
			add_action('authorizedotnet_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('authorizedotnet_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* Ebs Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='ebs_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('ebs_submit_post_details','successfull_return_ebs_post_details',10);
			add_action('ebs_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('ebs_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* PSIgate Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='psigate_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('psigate_submit_post_details','successfull_return_psigate_post_details',10);
			add_action('psigate_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('psigate_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* eway Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='eway_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('eway_submit_post_details','successfull_return_eway_post_details',10);
			add_action('eway_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('eway_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* worldpay Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='worldpay_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('worldpay_submit_post_details','successfull_return_worldpay_post_details',10);
			add_action('worldpay_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('worldpay_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* stripe Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='stripe_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('strip_submit_post_details','successfull_return_strip_post_details',10);
			add_action('strip_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('strip_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}	
	/* Briantree Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='braintree_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){			
			remove_action('braintree_submit_post_details','successfull_return_braintree_post_details');
			add_action('braintree_successfull_return_content','frontend_submition_successful_added_tabs',0);	
			add_action('braintree_successfull_return_content','frontend_submition_success_msg',11);	
		}
	}
	/* inspire commerce Payment gateway */
	if(isset($_REQUEST['page']) && $_REQUEST['page']=='inspire_commerce_success'){
		$is_frontend_edit_form=get_post_meta($_REQUEST['pid'],'is_frontend_edit_form',true);		
		if($is_frontend_edit_form){
			remove_action('inspire_commerce_submit_post_details','successfull_return_inspire_commerce_post_details');
			add_action('inspire_commerce_successfull_return_content','frontend_submition_success_msg',11);
			add_action('inspire_commerce_successfull_return_content','frontend_submition_successful_added_tabs',0);	
		}
	}
}

/*
 * Display frontend submit processing tab
 */
function frontend_submition_successful_added_tabs(){
	$pid=$_REQUEST['pid'];
	$post_type=get_post_type($pid);
	$custom_post_type=get_option('templatic_custom_post');		
	$post_type_label=strtolower($custom_post_type[$post_type]['labels']['singular_name']);
	?>
	<div class="submit-progress-steps columns clearfix">
		<ul>
			<li><span><?php _e('1. Enter mandatory fields',FE_DOMAIN);?></span></li>
			<li><span><?php echo sprintf(__('2. Add your %s',FE_DOMAIN),$post_type_label);?></span></li>
			<li><span class="active"><?php echo _e('3. Pay & Publish',FE_DOMAIN);?></span></li>
		</ul>
	</div>
	<?php
}

/*
 * Return: display preview listing link and author dashboard link on success page
 */
function frontend_submition_success_msg(){
	global $wpdb,$current_user;	
	$post_id=@$_REQUEST['pid'];
	$permalink=get_permalink($post_id);
	if(false===strpos($permalink,'?')){
	    $url_glue = '?';
	}else{
		$url_glue = '&';	
	}
	$paidamount = get_post_meta($_REQUEST['pid'],'paid_amount',true);	
	echo '<div class="submited_info">';
	echo '<h3>'.__('Your Package Details',FE_DOMAIN).'</h3>';
	echo "<ul class='list'>";
	if(get_post_meta($_REQUEST['pid'],'package_select',true))
	{
			$package_name = get_post(get_post_meta($_REQUEST['pid'],'package_select',true));
			if (function_exists('icl_register_string')) {									
				$package_name->post_title = icl_t('tevolution-price', 'package-name'.$package_name->ID,$package_name->post_title);
			}
			$package_type = get_post_meta($package_name->ID,'package_type',true);
			if($package_type  ==2){
				$pkg_type = __('Pay per subscription',FE_DOMAIN); 
			}else{ 
				$pkg_type = __('Pay per post',FE_DOMAIN); 
			} ?>
			<li><p class="submit_info_label"><?php _e('Package Title',FE_DOMAIN);?>: </p> <p class="submit_info_detail"> <?php echo $package_name->post_title;?></p></li>
			<li><p class="submit_info_label"><?php _e('Package Type',FE_DOMAIN);?>: </p> <p class="submit_info_detail"> <?php echo $pkg_type;?></p></li>
		 
<?php
	}
	if(get_post_meta($_REQUEST['pid'],'alive_days',true))
	{
		 echo "<li><p class='submit_info_label'>"; _e('Validity',FE_DOMAIN); echo ": </p> <p class='submit_info_detail'> ".get_post_meta($_REQUEST['pid'],'alive_days',true).' '; _e('Days',FE_DOMAIN); echo "</p></li>";
	}
	if(get_user_meta($suc_post->post_author,'list_of_post',true))
	{
		 echo "<li><p class='submit_info_label'>"; _e('Number of Posts',FE_DOMAIN).": </p> <p class='submit_info_detail'> ".get_user_meta($suc_post->post_author,'list_of_post',true)."</p></li>";
	}
	if(get_post_meta(get_post_meta($_REQUEST['pid'],'package_select',true),'recurring',true))
	{
		 echo "<li><p class='submit_info_label'>"; _e('Recurring Charges',FE_DOMAIN).": </p> <p class='submit_info_detail'> ".fetch_currency_with_position(get_post_meta($_REQUEST['pid'],'paid_amount',true))."</p></li>";
	}
	if($paidamount > 0){
		fetch_payment_description($_REQUEST['pid']);
	}
	echo "</ul>";
	echo "</div>";
	?>
	<a href="<?php echo $permalink.$url_glue.'action=edit&front_edit=1';?>"  class="frontend_edit_button button button-primary"><?php _e('Edit Listing',FE_DOMAIN);?></a>
	<a href="<?php echo get_permalink($post_id);?>"  class="frontend_edit_button button button-primary"><?php _e('Preview Listing',FE_DOMAIN);?></a>
	<a href="<?php echo get_author_posts_url($current_user->ID);?>"  class="frontend_edit_button button button-primary"><?php _e('My Dashboard',FE_DOMAIN); ?></a>
	<?php
}


/*
 * add frontend edit submit page shortcode action call
 */
add_action('init','frontend_edit_submit_shortcode');
function frontend_edit_submit_shortcode()
{
	include(dirname(__FILE__)."/frontend_edit_submit_code.php");
	add_shortcode('wysiwyg_submission_form', 'frontend_edit_submit_form_shortcode');	

	if(is_plugin_active('Listing-Vouchers/coupons.php')):				
		include_once(TEMPLATIC_FRONTEND_DIR.'coupon_uploader.php');
	endif;	
	
}


add_action( 'wp_enqueue_scripts', 'templatic_frontend_enqueue_scripts' );
add_action( 'wp_head', 'templatic_inline_script' );


/*
 * Return: assign javascript inline variable define
 */
function templatic_inline_script(){

 	?>
 	<script type="text/javascript">
 		var upload_title= "<?php echo __( 'Add images gallery', FE_DOMAIN ); ?>";
 		var add_upload_title= "<?php echo __( 'Add to gallery', FE_DOMAIN ); ?>";
 		var delete_image= "<?php echo __( 'Delete image', FE_DOMAIN ); ?>";
 		var delete_i= "<i class='fa fa-times-circle redcross'></i>";
 		var frontend_plg_url= "<?php echo TEMPLATIC_FRONTEND_URL  ?>";
 		var email_valid_address= "<?php echo __('Please provide a valid email address',FE_DOMAIN);  ?>";
 	</script>
 	<?php
	if(is_single() && (isset($_REQUEST['action']) && $_REQUEST['action']=='edit') ){
		remove_action('event_user_attend','event_user_attend');	
	}
}

/*
 * Return: wp enqueue script for inline editor tools like link, blockquote, text decoration, list , justify, blockformat and addition anchor script
 * Also inclide image uploader script, datepicker script and datapicker localize script
 */
function templatic_frontend_enqueue_scripts(){
	global $wp_version,$post,$wp_locale;
	$is_frontend_submit_form = get_post_meta( @$post->ID, 'is_frontend_submit_form', true );
	if(is_single() && (isset($_REQUEST['action']) && $_REQUEST['action']=='edit') || $is_frontend_submit_form==1 || (isset($_REQUEST['page']) && strpos($_REQUEST['page'],'success') !== false)
		|| (isset($_REQUEST['pmethod']))){		
		wp_enqueue_style( 'templatic-editor-css' , TEMPLATIC_FRONTEND_URL.'css/templatic_editor.css', false, $wp_version, 'screen' );		
		wp_enqueue_style( 'popline-page' , TEMPLATIC_FRONTEND_URL.'css/page.css', false, $wp_version, 'screen' );
	}
	
	if(is_single() && (isset($_REQUEST['action']) && $_REQUEST['action']=='edit')){
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'jquery-templatic_editor', TEMPLATIC_FRONTEND_URL.'js/templatic_editor.min.js' , array( 'jquery' ), $wp_version, true );
		wp_enqueue_script( 'jquery-popline-anchor', TEMPLATIC_FRONTEND_URL.'js/jquery.popline.anchor.js' , array( 'jquery' ), $wp_version, true );
		wp_enqueue_script( 'jquery-ajaxupload', TEMPLATIC_FRONTEND_URL.'js/ajaxupload.3.5.js' , array( 'jquery' ), $wp_version, true );
		wp_enqueue_script( 'jquery-image-uploader', TEMPLATIC_FRONTEND_URL.'js/jquery.uploader.js' , array( 'jquery' ), $wp_version, true );		
		
		wp_enqueue_script('jquery-ui-datepicker');
		 //localize our js
		$aryArgs = array(
			'monthNames'        => strip_array_indices( $wp_locale->month ),
			'monthNamesShort'   => strip_array_indices( $wp_locale->month_abbrev ),
			'monthStatus'       => __( 'Show a different month', FE_DOMAIN ),
			'dayNames'          => strip_array_indices( $wp_locale->weekday ),
			'dayNamesShort'     => strip_array_indices( $wp_locale->weekday_abbrev ),
			'dayNamesMin'       => strip_array_indices( $wp_locale->weekday_initial ),
			// is Right to left language? default is false
			'isRTL'             => @$wp_locale->is_rtl,
		);	
		// Pass the array to the enqueued JS
		wp_localize_script( 'jquery-ui-datepicker', 'objectL11tmpl', $aryArgs );
	}

}

/*
 * forntend editor tool display script and save frontend edit data using jquery Ajax
 */
add_action('wp_footer','templatic_frontend_editor_footer',100);
function templatic_frontend_editor_footer(){
	global $post,$htmlvar_name;
	if(is_single() && isset($_REQUEST['action'])){
		$htmlvar_name =	get_frontend_single_post_fields(get_post_type());	
		//check user not logged in then redirect in login page on  frontend edit time	
		if(!is_user_logged_in()){
			wp_redirect(get_tevolution_login_permalink());
			exit;
		}

		if(function_exists('tevolution_get_post_type'))
			$custom_post_type = tevolution_get_post_type();
		$post_typ=get_post_type();
		$post_id=get_the_ID();
		$post_author=$post->post_author;
		$payable_amount=get_post_meta($post_id,'payable_amount',true);

		if(isset($_REQUEST['front_edi']) && $_REQUEST['front_edi']==1){
			$payable_amount='0';
		}		
		?>	
		<script type="text/javascript">
		jQuery(function(){
		  jQuery('.editblock').editable({
                            inlineMode: false,
                            buttons: ['heading_title','bold', 'italic','insertOrderedList','insertUnorderedList','blockquote','createLink','unLink','undo' ]
		  							})
		
		});
		jQuery( '#frontend-edit-save, #frontendedit_paynow,#frontendedit_paymentnow, #frontend-go-back-link, #priview-listing-link' ).on( 'click.autosave-local', function() {				
			var submit_id=	jQuery(this).attr('id');			
			var payable_amount='<?php echo $payable_amount?>';			
			var data;		
			data = {
					action: 'templatic_frontend_edit',					
					postimages: jQuery('#fontend_image_gallery').val(),
					ID: <?php echo $post_id?>,
					post_id: <?php echo $post_id?>,
					post_type: '<?php echo $post_typ?>',
					post_author: '<?php echo $post_author?>',
					post_title: jQuery('.frontend-entry-title').html(),				
					content: jQuery('.frontend-entry-content div.templatic-element').html(),
					excerpt: jQuery('.frontend-entry-excerpt').html(),
					prev_page: jQuery('#prev_page').val(),
					<?php if(isset($_REQUEST['front_edit']) && $_REQUEST['front_edit']==1):?>
					front_edit: 1,
					<?php endif;?>
			<?php
			if(!empty($htmlvar_name)){
				foreach($htmlvar_name as $key=>$val){
					if($key!="category" && $key!="post_title" && $key!="post_excerpt" && $key!="post_excerpt" && $key!="post_images"){
						if($val['type']=='select' ){
							echo $key;?>: jQuery('select.frontend_<?php echo $key;?>').val(),									
					<?php }elseif($val['type']=='radio'){
							echo $key;?>: jQuery('input[name=frontend_<?php echo $key?>]:checked').val(),
							<?php if($key=="event_type"):?>
							recurrence_occurs:jQuery('select#recurrence-occurs').val(),
							recurrence_per:jQuery('#recurrence-per').val(),
							recurrence_bydays:jQuery('input[name^=recurrence_bydays]:checked').map(function(n){if(this.checked){return  this.value;};}).get(),
							monthly_recurrence_byweekno:jQuery('#monthly-modifier').val(),
							recurrence_byday:jQuery('#recurrence-weekday').val(),
							recurrence_days:jQuery('#end_days').val(),
							<?php endif;?>
					<?php }elseif($val['type']=='multicheckbox'){
							echo $key;?>: (jQuery('.frontend_<?php echo $key;?>:checked').map(function(n){if(this.checked){return  this.value;};}).get()!='') ? jQuery('.frontend_<?php echo $key;?>:checked').map(function(n){if(this.checked){return  this.value;};}).get() : '',
					<?php }elseif($val['type']=='oembed_video'){
							echo $key;?>: jQuery("input[name='frontend_edit_<?php echo $key?>']").val(),
					<?php }elseif($val['type']=='coupon_uploader'){
							echo $key;?>: jQuery("input[name='frontend_edit_<?php echo $key?>']").val(),
					<?php }elseif($val['type']=='texteditor'){
							echo $key;?>: jQuery('.frontend_<?php echo $key?> div.templatic-element').html(),
					<?php }elseif($val['type']=='date'){
						echo $key;?>: jQuery('#frontend_datepicker_<?php echo $key?>').val(),
					<?php }else{
							echo $key; ?>: (jQuery('a.frontend_<?php echo $key?>').attr('href')!=undefined) ? jQuery('a.frontend_<?php echo $key?>').attr('href') :jQuery('.frontend_<?php echo $key?>').html(),
					<?php
						}
						do_action('frontend_edit_save_data_json',$key,$val);
					}

					if($val['type']=='geo_map'){?>
						zooming_factor: jQuery('#zooming_factor').val(),
						geo_latitude: jQuery('#geo_latitude').val(),
						geo_longitude: jQuery('#geo_longitude').val(),
						map_view: jQuery('#map_view').val(),
					<?php
					}
				}
					
			}
			?>
			};			
			jQuery("#frontend_edit_post_process").show();
			jQuery.ajax({
				url:ajaxUrl,
				type:'POST',
				async: true,
				data:data,
				success:function(results){
					jQuery("#frontend_edit_post_process").hide();					
					if(submit_id=='frontend-edit-save'){
						jQuery("#frontend_edit_post_save").show();
						setTimeout(function() { jQuery("#frontend_edit_post_save").hide(); }, 3000);
					}
					if(submit_id=='frontendedit_paymentnow'){												
						jQuery( "#frontendedit_payment_form_redirect" ).submit();
					}
					if(payable_amount <= 0 && submit_id=='frontendedit_paynow'){
						jQuery( "#frontendedit_payment_form" ).submit();					
					}
					/*redirect preview listing page or go back and edit frontend submit form */
					if(submit_id=='frontend-go-back-link' || submit_id== 'priview-listing-link' || data.front_edit == 1){
						var href= jQuery('#'+submit_id).attr('data-url');
						
						if(data.front_edit == 1){
							href=results.redirect_url;
						}
						if(submit_id=='priview-listing-link'){
							
							window.open(href, '_blank').focus();
							
  							/*win.focus(); */
  							return false;  							
						}
						else{
							window.location=href;
						}
					}

				}
			});

			return true;
		});
		/* Display multicheckbox, select, radio, date adn textarea input element editable */
		<?php
		if(!empty($htmlvar_name)){			
			foreach($htmlvar_name as $key=>$val){
				if($val['type']=='textarea'){?>						
					jQuery('.frontend_<?php echo $key;?>').attr("data-placeholder","<?php _e('Type something',FE_DOMAIN);?>");
					if(jQuery('.frontend_<?php echo $key;?>').html()==''){
						jQuery('.frontend_<?php echo $key;?>').addClass('frontend_textarea f-placeholder');
					}
					jQuery('.frontend_<?php echo $key;?>').on('change click focus', function () {								
						if(jQuery('.frontend_<?php echo $key;?>').html()==''){
							jQuery('.frontend_<?php echo $key;?>').addClass('frontend_textarea f-placeholder');
						}else{
							jQuery('.frontend_<?php echo $key;?>').removeClass('frontend_textarea f-placeholder');
						}
					});
					jQuery('.frontend_<?php echo $key;?>').on('focusout', function () {
						if(jQuery('.frontend_<?php echo $key;?>').html()==''){
							jQuery('.frontend_<?php echo $key;?>').addClass('frontend_textarea f-placeholder');
						}
					});
				<?php	
				}
				if($val['type']=='date'){?>
				jQuery(function(){
					var pickerOpts = {						
						showOn: "both",
						dateFormat: 'yy-mm-dd',							
						buttonText: '<i class="fa fa-calendar"></i>',
						monthNames: objectL11tmpl.monthNames,
						monthNamesShort: objectL11tmpl.monthNamesShort,
						dayNames: objectL11tmpl.dayNames,
						dayNamesShort: objectL11tmpl.dayNamesShort,
						dayNamesMin: objectL11tmpl.dayNamesMin,
						isRTL: objectL11tmpl.isRTL,
					};	
					jQuery('#frontend_datepicker_<?php echo $key;?>').datepicker(pickerOpts);
				});
				jQuery('#frontend_date_<?php echo $key;?>').html("<input type='text' size='12' placeholder='<?php echo date('Y-m-d'); ?>' id='frontend_datepicker_<?php echo $key;?>' name='frontend_<?php echo $key;?>' class='frontend_datepicker frontend_<?php echo $key;?>' value='<?php echo get_post_meta($post_id,$key,true)?>' />");
				jQuery('#frontend_date_<?php echo $key;?>').addClass('frontend_editor_datepicker');
				<?php
				}
				
				if($val['type']=='multicheckbox'){
					$option_values = explode(",",$val['option_values']);				
					$option_title = explode(",",$val['option_title']);
					$input_checkbox='';
					if(!empty($option_values)){
						$input_checkbox.="<div class='form_row frontend_edit_input_checkbox' id='frontend_ul_".$key."'><ul  class='hr_input_multicheckbox'>";
						for($i=0;$i<count($option_values);$i++){
							$input_checkbox.="<li><label for='".$key."-".trim($option_values[$i])."' class='frontend_edit_checkbox'><input type='checkbox' onClick='frontend_edit_value(this,&quot;multicheckbox&quot;,&quot;frontend_multicheckbox_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."[]' value='".trim($option_values[$i])."' class='frontend_".$key."' > ".trim($option_title[$i])."</label></li>";
						}
						$input_checkbox.='<div>';
					}

					?>
					jQuery('#frontend_multicheckbox_<?php echo $key;?>').addClass('frontend_multicheckbox');
					jQuery('#frontend_multicheckbox_<?php echo $key;?>').on( 'click focus', function(e) {							
						jQuery('#frontend_ul_<?php echo $key;?>').remove();
						jQuery(this).after("<?php echo $input_checkbox;?>");
						var val=jQuery(this).html().split(",");						 	
						for(var i=0;i<val.length;i++){						 		
							jQuery("input[name='frontend_<?php echo $key;?>[]'][data-value='"+val[i].trim()+"']").prop("checked", true);
						}
						e.stopPropagation();
					});
					<?php

				}elseif($val['type']=='radio'){
					$option_values = explode(",",$val['option_values']);				
					$option_title = explode(",",$val['option_title']);
					$input_radio='';
					if(!empty($option_values)){
						$input_radio.="<div class='form_row frontend_edit_input' id='frontend_ul_".$key."'><ul class='hr_input_radio'>";
						for($i=0;$i<count($option_values);$i++){
							if($key=='gender'){
							if(trim($option_values[$i])=='male'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='male' class='genderimg' src='".get_template_directory_uri()."/images_new/Male.png' style='cursor:pointer;'/><center>Male</center></label></li>";
							}
							else if(trim($option_values[$i])=='female'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='female' class='genderimg' src='".get_template_directory_uri()."/images_new/Female.png' style='cursor:pointer;'/><center>Female</center></label></li>";
							}
							else if(trim($option_values[$i])=='coed'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='coed' class='genderimg' src='".get_template_directory_uri()."/images_new/CoEd.png' style='cursor:pointer;'/><center>Co Ed</center></label></li>";
							}
							}
							else if($key=='level'){
							 if(trim($option_values[$i])=='recreation'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='recreation' class='levelimg' src='".get_template_directory_uri()."/images_new/Recreation.png' style='cursor:pointer;'/><center>Recreation</center></label></li>";
							}
							else if(trim($option_values[$i])=='beginner'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='beginner' class='levelimg' src='".get_template_directory_uri()."/images_new/Beginner.png' style='cursor:pointer;'/><center>Beginner</center></label></li>";
							}
							else if(trim($option_values[$i])=='intermediate'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='intermediate' class='levelimg' src='".get_template_directory_uri()."/images_new/Intermediate.png' style='cursor:pointer;'/><center>Intermediate</center></label></li>";
							}
							else if(trim($option_values[$i])=='advance'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='advance' class='levelimg' src='".get_template_directory_uri()."/images_new/Advance.png' style='cursor:pointer;'/><center>Advance</center></label></li>";
							}
							else if(trim($option_values[$i])=='competition'){
							$input_radio.="<li style='width:auto;'><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' style='display:none;'> <label for='".$key."-".trim($option_values[$i])."'><img rel ='competition' class='levelimg' src='".get_template_directory_uri()."/images_new/Competition.png' style='cursor:pointer;'/><center>Competition</center></label></li>";
							}}
							else{
								$input_radio.="<li><input type='radio' onClick='frontend_edit_value(this,&quot;radio&quot;,&quot;frontend_radio_$key&quot;)' id='".$key."-".trim($option_values[$i])."' data-value='".trim($option_title[$i])."' name='frontend_".$key."' value='".trim($option_values[$i])."' class='frontend_".$key."' > <label for='".$key."-".trim($option_values[$i])."'>".trim($option_title[$i])."</label></li>";
							}
						}
						$input_radio.='</div>';
					}

					?>
					jQuery('#frontend_radio_<?php echo $key;?>').addClass('frontend_radio');
					jQuery('#frontend_radio_<?php echo $key;?>').on( 'click focus', function() {								
						var val=jQuery(this).html();
						jQuery('#frontend_ul_<?php echo $key;?>').remove();
						jQuery(this).after("<?php echo $input_radio;?>");
						jQuery("input[name='frontend_<?php echo $key;?>'][data-value='"+val+"']").prop("checked", true);

					});	
					<?php
				}elseif($val['type']=='select'){					
					$option_values = explode(",",$val['option_values']);
					$option_title = explode(",",$val['option_title']);					
					$input_select='';
					if(!empty($option_values)){
						$input_select.="<div class='form_row frontend_edit_input' id='frontend_ul_".$key."'><select onChange='frontend_edit_value(this,&quot;select&quot;,&quot;frontend_select_$key&quot;)' id='frontend_".$key."' name='frontend_".$key."' class='frontend_".$key."'>";
						for($i=0;$i<count($option_values);$i++){														
							$input_select.="<option value='".trim($option_values[$i])."' >".trim($option_title[$i])."</option>";	
						}
						$input_select.='</select</div>';
					}

					?>
					jQuery('#frontend_select_<?php echo $key;?>').addClass('frontend_select');
					jQuery('#frontend_select_<?php echo $key;?>').on( 'click focus', function() {
						var val=jQuery(this).html();								
						jQuery('#frontend_ul_<?php echo $key;?>').remove();						 	
						jQuery(this).after("<?php echo $input_select;?>");
						jQuery("select.frontend_<?php echo $key;?>").val(jQuery('#'+this.id).html());							 	
						jQuery("select.frontend_<?php echo $key;?>").find("option").filter(function(){							 	
						  return ( (jQuery(this).html() ==jQuery.trim(val)) )
						}).prop('selected', true);
					});
					<?php
				}
			}				
		}
		?>
		jQuery(function(){	
			/* Contenteditable false for select, multichekbox and radio custom fields type */
			jQuery(".frontend_select,.frontend_multicheckbox,.frontend_radio").each(function(){
				jQuery(this).attr('contenteditable','false');
			});
		});
		
		</script>
		<?php
		if(!is_page()){
			include_once(TEMPLATIC_FRONTEND_DIR.'image_uploader.php');			
		}
	}

}


/*
 * Return: sava inline editing post data update/draft,save after click on save as draf and publish button
 */
add_action('wp_ajax_templatic_frontend_edit','templatic_frontend_edit_save_data');
function templatic_frontend_edit_save_data(){
	
	if(isset($_REQUEST) && !empty($_REQUEST)){
		global $wpdb;
		$payable_amount=get_post_meta($_REQUEST['post_id'],'payable_amount',true);
		if($payable_amount <= 0){	
			//$edit_post['post_status']=fetch_posts_default_status();
		}else{
			//$edit_post['post_status']=fetch_posts_default_paid_status();
		}
		$post_id=$_REQUEST['post_id'];
		$post_title=trim(str_replace('<br>', '',$_REQUEST['post_title']));
		$post_content=trim($_REQUEST['content']);
		$post_images=$_REQUEST['postimages'];
		$post_excerpt=trim($_REQUEST['excerpt']);
		$post_author=$_REQUEST['post_author'];
		
		$edit_post['ID']=$_REQUEST['post_id'];
		$edit_post['post_title']=trim(str_replace('<br>', '',$_REQUEST['post_title']));
		$edit_post['post_content']=trim($_REQUEST['content']);
		$edit_post['post_type']=$_REQUEST['post_type'];
		$edit_post['post_name']=wp_unique_post_slug(sanitize_title($_REQUEST['post_title']),$edit_post['ID'],'publish',$edit_post['post_type'],0);		

		if(isset($_REQUEST['excerpt']) && $_REQUEST['excerpt']!=""){
			$edit_post['post_excerpt']=trim($_REQUEST['excerpt']);
		}
		
		wp_update_post( $edit_post );		

		if(isset($_REQUEST['postimages']) && $_REQUEST['postimages']!=""){
			
			/* delets the sample image when other images are uploaded */
			$wpdb->delete( $wpdb->posts , array('post_parent'=>$post_id, 'post_title' => 'sample_image') );

			$uploaddir = TEMPLATEPATH."/images/tmp/";
			$dirinfo = wp_upload_dir();
			$path = $dirinfo['path'];
			$url = $dirinfo['url'];
			$subdir = $dirinfo['subdir'];
			$basedir = $dirinfo['basedir'];
			$baseurl = $dirinfo['baseurl'];
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$post_images=explode(',',$_REQUEST['postimages']);
			$menu_order=0;
			foreach ($post_images as $image) {
				if($image!=""){
					$upload_img_path=$uploaddir._wp_relative_upload_path( $image );
					$wp_filetype = wp_check_filetype(basename($image), null );
					$dest=$path.'/'._wp_relative_upload_path( $image);
					if(file_exists($upload_img_path)){
						copy($upload_img_path, $dest);						
						unlink($upload_img_path);
						$attachment = array('guid' => $url.'/'._wp_relative_upload_path( $image ),
										'post_mime_type' => $wp_filetype['type'],
										'post_title' => preg_replace('/\.[^.]+$/', '', basename($image)),
										'post_content' => '',
										'post_status' => 'inherit',
										'menu_order' => $menu_order++,
									);						
						$img_attachment=substr($subdir.'/'.$image,1);
						
						$attach_id = wp_insert_attachment( $attachment, $img_attachment, $post_id );
						$upload_img_path=$path.'/'._wp_relative_upload_path( $image );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $upload_img_path );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}
				}//finish foreach loop
				
			}//finish the foreach loop
		}

		if($post_id)
		{
			$j = 1;
			$post_images=explode(',',$_REQUEST['postimages']);
			foreach($post_images as $arrVal)
			{
				$expName = array_slice(explode(".",$arrVal),0,1);				
				$wpdb->query('update '.$wpdb->posts.' set  menu_order = "'.$j++.'" where post_name = "'.$expName[0].'"  and post_parent = "'.$post_id.'"');				
			}
		}

		do_action('frontend_edit_save_data',$_REQUEST,$post_id); // add adction for use frontend edit save extra data

		/*
		 * save the custom fields value
		 * except ajax action, post id, post type, post title, post content, post excerpt, categort and post images
		 */
		foreach($_REQUEST as $key=>$val){			
			if($key!='action' && $key!='post_id' && $key!='post_type' && $key!='post_author' && $key!='post_title' && $key!='content' && $key!='excerpt' && $key!='category' && $key!='postimages'){
				if(is_array($val)){
					$allowed_multicheckbox=apply_filters('tmpl_frontend_multicheckbox',array('recurrence_bydays'));
					if(in_array(trim($key),$allowed_multicheckbox)){
						update_post_meta($post_id, trim($key) ,implode(',',$val));
					}else{
						update_post_meta($post_id, trim($key) ,$val);
					}
					
				}else
					update_post_meta($post_id, trim($key) , trim($val));
			}
			do_action('frontend_edit_ajax_save_data',$key,$val,$post_id); // Add action for save extra custom fields
		}

		if(isset($_REQUEST['front_edit']) && $_REQUEST['front_edit']==1){
			$user_id = get_current_user_id();
			$author_url = get_author_posts_url($user_id );
			if(isset($author_url) && !empty($author_url) && (strtolower($author_url) == strtolower($_REQUEST['prev_page']))) 
				$send_data['redirect_url']=$author_url;
			else	
				$send_data['redirect_url']=get_permalink($post_id);
		}else{
			$send_data['redirect_url']='';
		}
		wp_send_json($send_data);
	}
	exit;
}


/*
 * Convert oembed video from wordpress supported video site url such as youtube,Animoto,Instagram, Vimeo, Vine adn etc
 */
add_action('wp_ajax_convert_oembed_video','templatic_convert_oembed_video');
function templatic_convert_oembed_video(){	
	if(isset($_REQUEST['video']) && $_REQUEST['video']!=""){		
		$embed_code = wp_oembed_get( $_REQUEST['video']);
		if($embed_code!=""){
			echo $embed_code;
		}else{
			echo stripcslashes($_REQUEST['video']);
		}
	}
	exit;
}



/*
 * fetch post type wise custom fields on frontend edit detail page
 */
function get_frontend_single_post_fields($post_types){
 	global $wpdb,$post;
	remove_all_actions('posts_where');
	$cur_lang_code=(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))? ICL_LANGUAGE_CODE :'';
	add_filter('posts_join', 'custom_field_posts_where_filter');
	if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){	
		$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$post_types.'',
				'value' => array($post_types,'all'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),			
		),		
		 
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value_num',
		'meta_value_num'=>'sort_order',
		'order' => 'ASC'
		);		
		if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
			$package_select=$_REQUEST['package_select'];
			$field_monetiz_custom_fields=get_post_meta($package_select,'custom_fields',true);
		}
	}else{
		$args=
			array( 
			'post_type' => 'custom_fields',
			'posts_per_page' => -1	,
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'post_type_'.$post_types.'',
					'value' => array($post_types,'all'),
					'compare' => 'IN',
					'type'=> 'text'
				),
				array(
					'key' => 'show_on_page',
					'value' =>  array('user_side','both_side'),
					'compare' => 'IN',
					'type'=> 'text'
				),			
			),		
			 
			'meta_key' => 'sort_order',
			'orderby' => 'meta_value_num',
			'meta_value_num'=>'sort_order',
			'order' => 'ASC'
			);
	}
	
	$post_query = get_transient( '_tevolution_query_frontend'.trim($post_types).$cur_lang_code);
	if ( false === $post_query && get_option('tevolution_cache_disable')==1 ) {
		$post_query = new WP_Query($args);
		set_transient( '_tevolution_query_frontend'.trim($post_types).$cur_lang_code, $post_query, 12 * HOUR_IN_SECONDS );
	}elseif(get_option('tevolution_cache_disable')==''){
		$post_query = new WP_Query($args);
	}
	$return_arr = array();
	if($post_query){
		while ($post_query->have_posts()) : $post_query->the_post();
			if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){				
				if(!empty($field_monetiz_custom_fields) && !in_array($post->ID, $field_monetiz_custom_fields) ){
					continue;
				}
			}
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}
			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      =>  $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"option_title" => get_post_meta($post->ID,"option_title",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
					"field_require_desc" =>get_post_meta($post->ID,"field_require_desc",true),
					"heading_type" => get_post_meta($post->ID,"heading_type",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
		endwhile;
		wp_reset_query();
		wp_reset_postdata();
	}	
	remove_filter('posts_join', 'custom_field_posts_where_filter');
	return $return_arr;
}

/*
 * subscriber user also edit his/her own drft on frontend
 */
add_action( 'after_setup_theme', 'frontend_edit_enable_view_drafts',99);
function frontend_edit_enable_view_drafts() {

	if(!is_admin() || (defined( 'DOING_AJAX' ) && DOING_AJAX) ){		
		$role = get_role( 'subscriber' ); 
		$role->add_cap( 'read_private_posts' ); 
		$role->add_cap( 'edit_posts' );
	}else{
		$role = get_role( 'subscriber' ); 
		$role->remove_cap( 'read_private_posts' ); 
		$role->remove_cap( 'edit_posts' );
	}	
}

/*This hook call on plugin activation for insert WYSIWYG plugin related setting and submit page */
function frontend_editor_activate_plugin(){
	update_option('fontend_custom_fields_insert','none');
	frontend_editor_activate();
}

/*
 * set default data custom fields data on frontend editor plugin activation
 */
function frontend_editor_activate(){
	global $wpdb,$pagenow;
	if(isset($_POST['reset_custom_fields']) && (isset($_POST['custom_reset']) && $_POST['custom_reset']==1))
	{
		update_option('fontend_custom_fields_insert','none');
	}	

	if((isset($_REQUEST['page']) && (($_REQUEST['page']=='custom_fields' || $_REQUEST['page']=='templatic_system_menu')) || $pagenow=='themes.php' || $pagenow=='plugins.php' ) && get_option('fontend_custom_fields_insert')!='inserted') 
	{
		update_option('fontend_custom_fields_insert','inserted');
		
		/* Post Title */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_title' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','Click to edit your title');
		}
	
		/* Post Content */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_content' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"<h3>How to add your content in this area?</h3>
	
	You can select this content to edit it and add your own then format it to make it look more readable. You can use <b>bold </b>or <i>Italic </i> to highlight important parts of your content.  What's great is you can also use <a href='http://templatic.com'>hyperlinks</a> in this content.
	
	You can also add both ordered and unordered lists in this field:<br/>
	<b>Ordered list sample:</b>
	<ol>
		<li>A new way to add your listing and easily edit it from the front-end</li>
		<li>The spell-checker in this editor makes sure your content in it does not contain any spelling errors</li>
	</ol>
	<b>Unordered list sample:</b>
	<ul>
		<li>See exactly how your listing will look as you are adding it.</li>
		<li>This on-page editing takes user experience to a different level</li>
	</ul>");
		}
		/* Post excerpt */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'post_excerpt' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.');
		}
	
		/* Address custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'address' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','481 Broadway New York, NY 10013');
		}
	
		/* Address map view custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'map_view' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','Road Map');
		}
		/* proprty_feature custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'proprty_feature' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.');
		}
		/* Listing logo custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'listing_logo' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			$dirinfo = wp_upload_dir();
			$source_img= TEMPLATIC_FRONTEND_DIR."/img/250x250.jpg";
			copy($source_img, $dirinfo['path']."/250x250.jpg");
			update_post_meta($post_content->ID, 'default_value',$dirinfo['url'].'/250x250.jpg');
		}
	
		/* listing timing custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'listing_timing' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','10.00-18.00 week days - Sunday closed');
		}
		/* Phone custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'phone' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','(260) 207-4478');
			update_post_meta($post_content->ID, 'is_require','1');
			update_post_meta($post_content->ID, 'validation_type','phone_no');
		}
		/* email custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'email' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','info@gmail.com');
			update_post_meta($post_content->ID, 'show_on_detail','1');
			update_post_meta($post_content->ID, 'field_require_desc','Please provide your email address');
		}
		/* website custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'website' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','http://www.templatic.com');
		}
		/* twitter custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'twitter' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','http://www.twitter.com/templatic');
		}
		/* facebook custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'facebook' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','https://www.facebook.com/templatic');
		}
		/* google_plus custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'google_plus' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','https://www.plus.google.com/templatic');
		}
		/* video custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'video' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','https://www.youtube.com/watch?v=8wxOVn99FTE');
			update_post_meta($post_content->ID, 'ctype','oembed_video');		
			$my_post = array('ID'           => $post_content->ID,
							 'post_content' => 'Takes a original URL and automatically to fetch the embed HTML for it using oEmbed	'
							);
			wp_update_post( $my_post );
		}
	
	
		/*Events plugin related custom fields */
		/* Start date custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'st_date' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',date("Y-m-d"));
		}
		/* end Date custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'end_date' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',date("Y-m-d"));
		}
		/* start time custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'st_time' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','09:00');
		}
		/* end time custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'end_time' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','20:00');
		}
		/* registration fees custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'reg_fees' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','$50');
		}
		/* registration fees custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'reg_fees' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','$50');
		}
	
		/* organizer name custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_name' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','Castor Event Organizers');
		}
		/* organizer email custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_email' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','info@event.com');
		}
		/* organizer address custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_address' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','5 Buckingham Dr Street, paris, NX, USA - 21478');
		}
		/* organizer contact custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_contact' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','01-025-98745871');
		}
		/* organizer website custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_website' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','http://event.com');
		}
		/* organizer mobile custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_mobile' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value','0897456123071');
		}
		/* organizer description custom fields */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'organizer_desc' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"<strong>What we do?</strong><br /><p>An event is normally a large gathering of people, who have come to a particular place at a particular time for a particular reason. Having said that, there's very little that's normal about an event. In our experience, each one is different and their variety is enormous. And that's as it should be: an event is something special. Aone - off. We plan these occasions in meticulous details, manage them from the ground, dismantle them when they are over and assess the result.</p><br /> <strong>How we do it?</strong><br /> <p>Events can be used to communicate key message, faster community relations, motivate work forces or raise funds. One of the first things we ask our clients is, what they want to achieve from their event. This is the cornerstone of the whole operation for us, our starting point and most importantly, it's the way success can be measured.</p>");
		}
		
		/* Property related custom fields */
		/* Property tag custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'property_tag' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"Foreclosure");
		}
		/* Property type custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'property_type' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"Sale");
		}
		/* price custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'price' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"10000");
		}
		/* bedrooms custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'bedrooms' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"2");
		}
		/* bathrooms custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'bathrooms' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"1");
		}
		/* area custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'area' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"1000");
		}
		/* mls_no custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'mls_no' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"100256");
		}
		/* additional_features custom field */
		$post_content = $wpdb->get_row("SELECT post_title,ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'additional_features' and $wpdb->posts.post_type = 'custom_fields'");
		if(count($post_content) != 0)
		{
			update_post_meta($post_content->ID, 'default_value',"<ul class='col3-ul-list columns'><li>Pound</li><li>fireplace, Vinyl, Tile/Brick</li><li>lake view, pool</li><li>Parking: 3+ Cars parking</li><li>Famous Places</li><li>Lot Dimensions: 135x196</li><li>gas heat</li><li>wine cellar</li><li>Excercies Equipments</li><li>gym</li><li>Swimming Pool: Yes</li><li>Sideyard(s)</li><li>Energy Efficient Appliances</li><li>Famous buildings, Places</li><li>Cable TV Wired</li><li>Central Air: Yes</li><li>Pool Type: Abvegrndpool</li><li>School District: Lake Forest</li><li>Water: Public</li><li>Kitdoublesin</li><li>Eleccooking</li><li>Oven-Self Cleaning</li><li>Electric Service: 200-300ampel</li><li>Cooling: Central A/C</li></ul>");
		}
	
		/*Finish events plugin related custom fields */
	
		 /*Set the Submit listing page */
		 if(function_exists('tevolution_get_post_type'))
			 $custom_post_type = tevolution_get_post_type();
		 $frontend_supported_post_type=apply_filters('frontend_supported_post_type',array('listing','property','event'));

		 if(!empty($custom_post_type)){
		 foreach($custom_post_type as $post_type){
		 	if(in_array($post_type,$frontend_supported_post_type)){
				 $post_content = $wpdb->get_row("SELECT post_title FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'wysiwyg-submission-".$post_type."' and $wpdb->posts.post_type = 'page'");
				 if(count($post_content) == 0)
				 {
					$my_post = array(
						 'post_title' => 'WYSIWYG Submission '.ucfirst($post_type),
						 'post_content' => "[wysiwyg_submission_form post_type='".$post_type."']",
						 'post_status' => 'publish',
						 'comment_status' => 'closed',
						 'post_author' => 1,
						 'post_name' => 'wysiwyg-submission-'.$post_type,
						 'post_type' => "page",
						);		
					$post_id = wp_insert_post( $my_post );
					if(is_plugin_active('sitepress-multilingual-cms/sitepress.php')){
						global $sitepress;
						$current_lang_code= ICL_LANGUAGE_CODE;
						$default_language = $sitepress->get_default_language();	
						/* Insert wpml  icl_translations table*/
						$sitepress->set_element_language_details($post_id, $el_type='post_page',$post_id, $current_lang_code, $default_language );
						if(function_exists('wpml_insert_templ_post'))
							wpml_insert_templ_post($post_id,'page'); /* insert post in language */
					}
					
					update_post_meta($post_id, '_wp_page_template','default' );
					update_post_meta($post_id, 'submit_post_type',$post-type );
					update_post_meta($post_id, 'is_frontend_submit_form','1' );
					update_post_meta($post_id, 'is_tevolution_submit_form','1' );
				 }
			}
		 }
		}
	}

}

/* Remove default plugin auto update notification strip in plugin section */
add_action('admin_init','frontend_editor_autoupdate_remove_action');
function frontend_editor_autoupdate_remove_action(){
	remove_action( 'after_plugin_row_Directory-WYSIWYG-Submission/directory_WYSIWYG_submission.php', 'wp_plugin_update_row' ,10, 2 );
}

/*
 * Return: update Frontend editor plugin version after templatic member login
 */
add_action('wp_ajax_frontend_editor','frontend_editor_update_login');
function frontend_editor_update_login()
{
	check_ajax_referer( 'frontend_editor', '_ajax_nonce' );
	$plugin_dir = rtrim( plugin_dir_path(__FILE__), '/' );	
	require_once( $plugin_dir .  '/templatic_login.php' );	
	exit;
}


/* Frontend editor deactivate hook for remove custom fields default value */
function frontend_editor_deactivate_plugin(){
	$args=array('post_type'      => 'custom_fields','posts_per_page' => -1	);
	
	$post_meta_info = new WP_Query($args);	
	if($post_meta_info->have_posts()){
		while ($post_meta_info->have_posts()) : $post_meta_info->the_post();
			/*Remove default value */
			update_post_meta(get_the_ID(), 'default_value',"");
		endwhile;		
	}
}