<?php
$tmpdata = get_option('templatic_settings');
$templatic_image_size =  @$tmpdata['templatic_image_size'];
if(!$templatic_image_size){ $templatic_image_size = '50'; }

$num_of_coupons = ($tmpdata['numberof_coupons'] == '' || $tmpdata['numberof_coupons'] == 0)? 10 : $tmpdata['numberof_coupons'];

?>
<script type="text/javascript">
var temp = 1;
var html_var = '<?php echo $val['htmlvar_name']; ?>';
var numof_coupons_can_upload = parseInt(<?php echo $num_of_coupons;?>);
var $uc = jQuery.noConflict();
var cpnArr = new Array();

jQuery.noConflict();
jQuery(document).ready(function($){
	jQuery("#couponlist").sortable({
		 connectWith: '#deleteArea',
		 'start': function (event, ui) {
			   /*jQuery Ui-Sortable Overlay Offset Fix*/
			   if ($.browser.webkit) {
				  wscrolltop = $(window).scrollTop();
			   }
		 },
		 'sort': function (event, ui) {
			   /*jQuery Ui-Sortable Overlay Offset Fix*/
			   if ($.browser.webkit) {
				  ui.helper.css({ 'top': ui.position.top + wscrolltop + 'px' });
			   }
		 },
		 update: function(event, ui){
			 /*Run this code whenever an item is dragged and dropped out of this list*/
			 var order = $(this).sortable('serialize');						
			 jQuery.ajax({
				 url: '<?php echo plugin_dir_url( __FILE__ ); ?>processImage.php',
				 type: 'POST',
				 data: order,				 
				 success:function(result){					 	
						document.getElementById('cpnarr').value = result;
					}
			 });			 
	 	}
	 });		 
	
});
function listing_voucher_delete_image(name,img_name,pid)
{
	var li_id=name;	
	var image_arr=document.getElementById('cpnarr').value;
	jQuery.ajax({
		 url: '<?php echo plugin_dir_url( __FILE__ ); ?>processImage.php',
		 type: 'POST',
		 data: 'name='+img_name+'&coupon_arr='+image_arr+'&pid='+pid,				 
		 success:function(result){			 
				document.getElementById('cpnarr').value = result;
				jQuery('#'+li_id).remove();	
				jQuery('.'+li_id).remove();				
		}				 
	 });	
}
</script>
<style type="text/css">
img_table {
	margin-top: 20px;
	}
li >img {
	cursor: move;
	}
#couponlist { width:700px; }
#couponlist div { float:left; }
#couponlist p >img {
	cursor: move;
	}
#couponlist div p { position:relative; padding: 0; }
#couponlist div p span {
	position:absolute;
	top:-6px;
	right:-6px;
	}
.uploadfilebutton{ position:absolute;font-size:30px; cursor:pointer; z-index:2147483583; top:-10px; left:0; opacity:0; }
</style>

<?php $button_text = apply_filters('tmpl_image_uploader_text',__("Upload Coupon", DOMAIN)); ?>
	 <div class="clearfix image_gallery_description">
     <p class="add_tevolution_images hide-if-no-js">
		<!-- Multi coupon uploader button -->
		<div class="<?php echo $name; ?>-sm-preview"></div>
			<div id="fancy-contact-form">
			<div class="dz-default dz-message" ><span  id="fancy-<?php echo $name; ?>"><span><i class="fa fa-folder"></i> <?php _e("Upload Coupon", DOMAIN); ?></span></div><span id="cpnstatus" class="message_error2 clearfix"></span></span></div>
			<input type="hidden" name="submitted" value="1">
       		<p class="max-upload-size">
			<?php
				echo sprintf( __( 'Maximum upload file size: %d%s.',DOMAIN ), esc_html($templatic_image_size),'KB' );
			?>
            </p>
		</div>
		<script>
			var thumb_src = '<?php echo get_template_directory_uri();?>/images/coupons/';
			jQuery(document).ready(function(){
				var settings = {
					url: '<?php echo plugin_dir_url( __FILE__ ); ?>uploadCoupon.php',
					dragDrop:true,
					fileName: "uploadcoupon[]",
					allowedTypes:"jpeg,jpg,png,gif,pdf",	
					returnType:"json",
					multiple:true,
					showAbort:false,
					showProgress:true,
					onSuccess:function(files,data,xhr)
					{
						/*check the category type*/
						<?php if($tmpdata['templatic-category_type'] ==  'select') { ?>			
							var selcted_cat = jQuery('#submit_form select[name="category[]"] option:selected').val();
						<?php 
						}
						else
						{
						?>
							var selcted_cat = jQuery('#submit_form input[name="category[]"]:checked').length;
						<?php
						}
						?>
						var status=$uc('#cpnstatus');
						status.text('');
						
						<?php
						
						if(!is_admin() && is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
						?>
						if(selcted_cat <= 0 || selcted_cat == ''){
							status.text("<?php _e('Please select category first.',DOMAIN); ?>");
							jQuery('.ajax-file-upload-statusbar').hide();
							return false;
						}
						
						<?php } ?>
						
						jQuery('#post_coupons_error').html('');
						
						$uc('#cfiles .success').each(function(){
								counter = parseFloat(this.id) + 1;
						});
						jQuery('.ajax-file-upload-statusbar').css('display','none');
						data = data+'';
						/*coupon size validation*/
						 if(data == 'LIMIT'){
								status.text("<?php _e('Your image size must be less then',CPN_DOMAIN); echo " ".$templatic_image_size." "; _e('kilobytes',CPN_DOMAIN); ?>");
								return false;
							 }
						
						
						 var counter = 0;
						 $uc('#couponlist div').each(function(){
								counter = counter + 1;
						 });
						limit = (data.split(",").length + counter);
						<?php
							if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php')){
								if(isset($_REQUEST['post']) && !empty($_REQUEST['post']))
									$package_id = get_post_meta($_REQUEST['post'],'package_select',true);
								else	
									$package_id = $_SESSION['package_select'];
								?>
									numof_coupons_can_upload = '<?php echo get_post_meta($package_id,'coupons_allowed',true); ?>';
								<?php
							}
						?>
						if(parseFloat(limit) > parseFloat(numof_coupons_can_upload))
						  {
								status.text("<?php _e('You can upload maximum ',CPN_DOMAIN);?>"+numof_coupons_can_upload+"<?php _e(' coupons',CPN_DOMAIN); ?>");
								return false;
						  }
						/* End Limit Code	*/
						var id_name = data.split('.');
						var img_name = '';		
						var img_name = '<?php echo bloginfo('template_url')."/images/coupons/"; ?>'+id_name[0]+"."+id_name[1];
						
						var FileExt = img_name.substr(img_name.lastIndexOf('.')+1);  
						if(FileExt != 'pdf')	
							var img_name = '<?php echo bloginfo('template_url')."/images/coupons/"; ?>'+id_name[0]+"."+id_name[1];
						else
							var img_name = '<?php echo plugin_dir_url( __FILE__ ); ?>images/pdfthumb.png';  /* if file is pdf then show default thumbnail */
							
						
						
						$uc('<div id=i_'+data+' class='+id_name[0]+'>').appendTo('#couponlist').html('<p id="'+id_name[0]+'"><img width="60px" height="60px" src="'+img_name+'" name="'+data+'" /><span><i onClick="listing_voucher_delete_image(\''+id_name[0]+'\',\''+data+'\');" class="fa fa-times-circle redcross"></i></span></p>');
						
						
						var cpnArr_i = 0;
						$uc('#files #couponlist p img').each(function(){
							cpnArr[cpnArr_i] = this.name;
							cpnArr_i++;
						});
						document.getElementById('cpnarr').value = cpnArr;
					},
					showDelete:true,
					deleteCallback: function(data,pd)
					{
					for(var i=0;i<data.length;i++)
					{
						jQuery.post("delete.php",{op:"delete",name:data[i]},
						function(resp, textStatus, jqXHR)
						{
							/*Show Message  */
							jQuery("#status").append("<div>File Deleted</div>");      
						});
					 }      
					pd.statusbar.hide(); /*You choice to hide/not.*/

				}
				}
				var uploadObj = jQuery("#fancy-"+'<?php echo $name; ?>').uploadFile(settings);
			});
		</script>
	 </p>
  
<table width="70%" align="center" border="0" class="img_table">
<tr>
    <td>
       <?php if(isset($_REQUEST['pid'])){			
			 $thumb_img_arr = bdw_get_images_plugin($_REQUEST['pid'],'large');		
			  if($thumb_img_arr):
                foreach ($thumb_img_arr as $val) :
					 $tmpimg = explode("/",$val['file']);
					 $name = end($tmpimg);
					 if($name!="")
						 $image_name.=$name.",";
				endforeach;	   
			  endif;
	   }
	   
	   if(isset($_REQUEST['cpnarr']) && $_REQUEST['cpnarr'] != '' &&  !$_REQUEST['pid'] )
        {
			$image_upload = explode(",",$_REQUEST['cpnarr']);
            foreach($image_upload as $image_id=>$val)
            {
				if($val !='')
				$tmp =explode("/",$val);
				$name = end($tmp);
				if($val!="")
					$image_name.=$name.",";				
			}
		}
	   ?>
        <input name="cpnarr" id="cpnarr" value="<?php echo @$image_name;?>" type="hidden"/>
        <table>
        	<tr>
            <td id="files">
            	<div id="couponlist">
                
               
        <?php
        $i = 0;
		if(isset($_REQUEST['cpnarr']) && $_REQUEST['cpnarr'] != '' &&  !$_REQUEST['pid'] )
        {
			$image_upload = explode(",",$_REQUEST['cpnarr']);
			
            foreach($image_upload as $image_id=>$val)
            {
				
                $thumb_src = get_template_directory_uri().'/images/coupons/'.$val;	
				$src = TEMPLATEPATH.'/images/coupons/'.$val;
				if($val !='')
					$tmpd = explode("/",$val);
					$name = end($tmpd);
					$img_name= explode('.',$name);
				if(file_exists($src) && $val!=""):
           ?>
                    <div id="i_<?php  echo $name; ?>" class="<?php echo $img_name[0]?>">
                    	<p id='<?php echo $img_name[0]?>'>
                        <img src="<?php echo $thumb_src; ?>" height = "60px" width = "60px" name="<?php echo $name; ?>" alt="" />                       
                        <span>
                        	<img align="right" id="cross" onclick="listing_voucher_delete_image('<?php echo $img_name[0]; ?>','<?php echo $name; ?>','');" src="<?php echo plugin_dir_url( __FILE__ ); ?>images/cross.png" alt="delete" class="redcross" />
                        </span>   
                         </p>               
                    </div>
           <?php
		   		endif;
            }
        }
       ?>
       <?php
       if(isset($_REQUEST['pid']) && !$_REQUEST['backandedit']) :
             $coupons = get_post_meta($_REQUEST['pid'],'coupon_ids',true);
		
			if($coupons!=""){
				$coupons_arr = explode(',',$coupons);
 
				foreach($coupons_arr as $key=>$cpns)
				{
					$coupons = get_post($cpns);
					$thumb_cpn_arr[$key]['id'] = $coupons->ID;
					$thumb_cpn_arr[$key]['file'] = $coupons->guid;
				}
			}
			
            $i = 0;
            if($thumb_cpn_arr):
                foreach ($thumb_cpn_arr as $val) :
				$tmpimg = explode("/",$val['file']);
                $name = end($tmpimg );
				$img_name= explode('.',$name);
               /*$thumb_src = get_template_directory_uri().'/thumb.php?src='.$val['file']; */
			   if($name!=""):

           ?>                    
                    <div id="i_<?php  echo $name; ?>" class="<?php echo $img_name[0]?>">
                    	<p id='<?php echo $img_name[0]?>'>
                        <img src="<?php echo $val['file']; ?>" height = "60px" width = "60px" name="<?php echo $name; ?>" alt="" />                       
                        <span>
                        	<img align="right" id="cross<?php echo $i; ?>" onclick="listing_voucher_delete_image('<?php echo $img_name[0]; ?>','<?php echo $name; ?>','<?php echo $val['id']; ?>');" src="<?php echo plugin_dir_url( __FILE__ ); ?>images/cross.png" alt="delete" class="redcross" />
                        </span>   
                         </p>               
                    </div>
            <?php
					endif;
                 endforeach;
            endif;
        endif;
       ?>
        <?php 
            if(!empty($_SESSION["file_info"]) && isset($_REQUEST['backandedit']) && isset($_REQUEST['pid']) ):
                global $upload_folder_path;
                $i = 0;				
                foreach($_SESSION["file_info"] as $image_id=>$val):
                    $final_src = TEMPLATEPATH.'/images/tmp/'.$val;
					$src = get_bloginfo('template_directory').'/images/tmp/'.$val;
					$name = end(explode("/",$val));
					$img_name= explode('.',$name);					
                    if($val):
                    if(file_exists($final_src)):
					
        ?>
        			<div id="i_<?php  echo $name; ?>" class="<?php echo $img_name[0]?>">
                    	<p id='<?php echo $img_name[0]?>'>
                        <img src="<?php echo $src; ?>" height = "60px" width = "60px" name="<?php echo $name; ?>" alt="" />                       
                        <span>
                        	<img align="right" id="cross<?php echo $i; ?>" onclick="listing_voucher_delete_image('<?php echo $img_name[0]; ?>','<?php echo $name; ?>','');" src="<?php echo plugin_dir_url( __FILE__ ); ?>images/cross.png" alt="delete" class="redcross" />
                        </span>   
                         </p>               
                    </div>
                      
                   <?php else: ?>
                   <?php
                  		$thumb_img_arr = bdw_get_images_plugin($_REQUEST['pid'],'large');
                        foreach($thumb_img_arr as $value):
                            $name = end(explode("/",$value['file']));
							$img_name= explode('.',$name);							
                            if($name == $val):	
							
                   ?>
                   		<div id="i_<?php  echo $name; ?>" class="<?php echo $img_name[0]?>">
                            <p id='<?php echo $img_name[0]?>'>
                            <img src="<?php echo $value['file']; ?>" height = "60px" width = "60px" name="<?php echo $name; ?>" alt="" />                       
                            <span>
                                <img align="right" id="cross<?php echo $i; ?>" onclick="listing_voucher_delete_image('<?php echo $img_name[0]; ?>','<?php echo $name; ?>','<?php echo $value['id']; ?>');" src="<?php echo plugin_dir_url( __FILE__ ); ?>images/cross.png" alt="delete" class="redcross" />
                            </span>   
                             </p>               
                        </div>
                        
                    <?php								
                            endif;
                       endforeach;
                    ?>
             
             <?php 
                    endif;
                    endif;
                    $i++;
                endforeach; 
            endif;
             ?>
              </div>
	</td>
   </tr>
  </table>
</td>
</tr>
</table>