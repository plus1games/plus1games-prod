<?php
/*
*
* All voucher upload , download , print functions are here 
*
*/
/*  apply css styles for plugin */
function process_styles_scripts()
{
	wp_enqueue_style( 'coupon-style',  TEVOLUTION_COUPON_URL.'css/coupon-style.css' );
}

/*  adds the script to footer */	
function print_script()
{
	?>
<script>

	/* create image view in new tab for print */	
	function makepage(src)
	{
		return "<html>\n" +
		"<head>\n" +
		"<title>Templatic Voucher</title>\n" +
		"<script>\n" +
		"function step1() {\n" +
		" setTimeout('step2()', 10);\n" +
		"}\n" +
		"function step2() {\n" +
		" window.print();\n" +
		" window.close();\n" +
		"}\n" +
		"</scr" + "ipt>\n" +
		"</head>\n" +
		"<body onLoad='step1()'>\n" +
		"<img src='" + src + "'/>\n" +
		"</body>\n" +
		"</html>\n";
	}

	/*  Prints the image from new tab */	
	function printme(evt,key)
	{
		
	if (!evt) 
		{
			/* Old IE*/
			evt = window.event;
		} 
		
	var image = evt.target;

	if (!image) 
		{
			/* Old IE*/
			image = window.event.srcElement;
		}

		tmp_src = image.src;
		coupon_src=document.getElementById('tmpl_img_'+key);

		var imagObject = new Image();
		imagObject = coupon_src;
		src = imagObject.src;

		link = "";
		var pw = window.open(link, "_new");
		pw.document.open();
		pw.document.write(makepage(src));
		pw.document.close();

	}

</script>
<?php
}



/*  add an option to custom fields for coupons */
function coupon_custom_field($post_id)
{
	?>
   <option value="coupon_uploader" <?php if( @get_post_meta($post_id,"ctype",true) == 'coupon_uploader'){ echo 'selected="selected"';}?>>
		<?php echo _e('Voucher uploader',CPN_DOMAIN);?>
   </option>
   <?php
}

/* generates the coupons uploader on submit form */
function coupon_custom_field_on_submitform($key,$val,$post_type)
{
	if($val['ctype'] == 'coupon_uploader') 
	{	
		/* get id of voucher uploader */
		$coupon_post_id = get_post_id_by_meta_key_and_value('ctype','coupon_uploader');
		$coupon_desc = get_post($coupon_post_id);
		/* get description of voucher uploader */
		$admin_desc = $coupon_desc-> post_content;
		add_action('wp_footer','callback_on_footer_fn');
		echo '<div class="upload_box ">';
			include (TEVOLUTION_COUPON_FOLDER_PATH."coupon_uploader.php"); 		
		echo '</div>';
		?>
		<span class="message_note"><?php echo $admin_desc;?></span>
		<span class="message_error2" id="post_coupons_error"></span>
		<span class="safari_error" id="csafari_error"></span>
		<?php 
	} 
}

/* Process for uploading coupons */
function coupons_process($last_postid)
{

if(isset($_POST['cpnarr']) && $_POST['cpnarr']!=""){	
	$_SESSION['file_info_coupon'] = explode(",",$_POST['cpnarr']);
}
	if(isset($_SESSION["file_info_coupon"]) && $_SESSION['file_info_coupon']!="")
		{
			$cpn_menu_order = 0;
			$cpns_ids = array();
			$coupons_session = array_values(array_filter($_SESSION['file_info_coupon']));
			
			foreach($coupons_session as $cpn_id=>$value)
			{
				$cpnsrc = TEMPLATEPATH."/images/coupons/".$value;
				if(file_exists($cpnsrc) && $value != '')
				{
					$ext = @end(explode('.', $value));
					$cpn_menu_order++;
					$cpn_dest_path = get_coupons_destination_path().'/'.$value;
					if($ext != 'pdf')
					{
						$cpn_post_img = tmpl_move_voucher_file($cpnsrc,$cpn_dest_path);
						$cpn_post_img['post_status'] = 'attachment';
						$cpn_post_img['post_parent'] = $last_postid;
						$cpn_post_img['post_type'] = 'attachment';
						$cpn_post_img['post_mime_type'] = 'image/jpeg';
				    }
				    else
				    {
						$cpn_post_img = tmpl_move_voucher_file($cpnsrc,$cpn_dest_path);
						$cpn_post_img['post_status'] = 'attachment';
						$cpn_post_img['post_parent'] = $last_postid;
						$cpn_post_img['post_type'] = 'attachment';
						$cpn_post_img['post_mime_type'] = 'application/pdf';
					 }
					$cpn_post_img['menu_order'] = $cpn_menu_order;
					$cpn_dirinfo = wp_upload_dir();		
					$cpn_path = $cpn_dirinfo['path'];
					$cpn_url = $cpn_dirinfo['url'];					
					
					$cpn_basedir = $cpn_dirinfo['basedir'];
					$cpn_baseurl = $cpn_dirinfo['baseurl'];	
					$destination_path = $cpn_basedir."/coupons";

					if(!is_dir($destination_path))
						{
							if (!mkdir($destination_path, 0777, true)) 
					 			{
									die(__('Failed to create folders...',CPN_DOMAIN));
	 				 			}
						}
			
					$cpn_subdir = '/coupons';
					$cpn_wp_filetype = wp_check_filetype(basename($value), null );
					$cpn_attachment = array(
						 'guid' => $cpn_baseurl.$cpn_subdir."/"._wp_relative_upload_path( $value ),
						 'post_mime_type' => $cpn_wp_filetype['type'],
						 'post_title' => preg_replace('/\.[^.]+$/', '', basename($value)),
						 'post_content' => '',
						 'post_status' => 'inherit',
						 'post_type' => 'attachment-coupon',
						 'menu_order' => $cpn_menu_order
					  );	
					  
					  $cpn_img_attachment = substr($cpn_subdir."/".$value,1);
					  $cpn_attach_id = wp_insert_attachment( $cpn_attachment, $cpn_img_attachment);
					  $cpns_ids[] = $cpn_attach_id;
					  require_once(ABSPATH . 'wp-admin/includes/image.php');					 
					  $cpn_upload_img_path=$cpn_basedir.$scpn_ubdir."/"._wp_relative_upload_path( $value);
					  $cpn_attach_data = wp_generate_attachment_metadata( $cpn_attach_id, $cpn_upload_img_path );					
					  wp_update_attachment_metadata($cpn_attach_id, $cpn_attach_data );		
			}
		}
		unset($_SESSION["file_info_coupon"]);
		
	/* Save coupons from admin */
	if(is_admin() && $_REQUEST['hidden_post_status'] !='draft')
	{
		/* if there is already coupon then add with that */
		  $already_cpns = get_post_meta($last_postid,'coupon_ids',true);
			if(isset($already_cpns) && $already_cpns != '')
			{
				$cpns_extra = explode(',',$already_cpns);
				
				$old_cpn_ids = array();
				foreach($cpns_extra as $old_cpns){
						$old_cpn_ids[] = $old_cpns;
				}
			$cpns_ids = wp_parse_args($cpns_ids, array_unique($old_cpn_ids));	
			}
			else
				{
					
				}
		
	}
		$cpns = implode(',', $cpns_ids);
		$cpns;
		update_post_meta($last_postid,'coupon_ids',$cpns);	
				
	}
	/*remove action while adding coupons was adding twice from backend*/
	remove_action('save_post','coupons_process',10,1);
}



/* Create tab for coupons in single listing view */
function show_coupons_tab()
{
	global $htmlvar_name;
	$coupons = get_post_meta(get_the_ID(),'coupon_ids',true);
	/*check whether coupon code post exist or not*/
	if($coupons!=""):
		$coupons_arr = explode(',',$coupons);
		foreach($coupons_arr as $key=>$cpns)
		{
			if(FALSE !== get_post_status( $cpns ))
			{
				$found_coupons = true;
			}
		}
	endif;
	if(!empty($coupons) && $found_coupons && ($tmpl_flds_varname['post_coupons'] || $htmlvar_name['field_label']['post_coupons'])):
	?>
          <li class="tab-title" role="presentational"><a href="#listing_coupons"><?php echo _e('Vouchers',CPN_DOMAIN);?></a></li>
   <?php 
   endif; 
}

/* Shows available coupons and create link for download and print coupons */
function coupons_listing()
{
	$coupons = get_post_meta(get_the_ID(),'coupon_ids',true);
		
	if($coupons!=""):
	$coupons_arr = explode(',',$coupons);


?>
	
	<section id="listing_coupons" class="content" aria-hidden="false" role="tabpanel">
		<?php echo apply_filters('tmpl_voucher_title',''); ?>
	<ul>
	
<?php 
	 	 
	foreach($coupons_arr as $key=>$cpns)
	{
		$coupons = get_post($cpns);
		
		if($coupons)
		{
	
		$url = wp_get_attachment_url( $cpns );
		$cpn_name = basename($url); 
				
		$file = TEVOLUTION_COUPON_MAIN_DIR.$cpn_name;
		$path_parts = pathinfo($file);
    	$ext = strtolower($path_parts["extension"]);
    	$img_url = $url;
		
			if($ext == 'pdf')
				{
					$img_url = plugin_dir_url( __FILE__ ).'/images/pdfthumb.png';
				}
			else 
				{
					$img_thumbnail = wp_get_attachment_image_src( $cpns );
				}
			$idname = explode('.',$coupons->post_title);	
			echo '<li class="cpn" id="'.$idname[0].'">';
			echo '<div class="cpn_img">';	
			if(strstr($_SERVER['REQUEST_URI'],'/wp-admin/')){			
				echo '<img id="tmpl_img_'.$key.'" src="'.$img_url.'" height="78" width="78" />';
			}else{
				echo '<img id="tmpl_img_'.$key.'" src="'.$img_url.'" height="188" width="268" />';
			}
			if(is_admin())
				echo '<span class="close"><i class="dashicons dashicons-no" onclick="listing_voucher_delete_image(\''.$idname[0].'\',\''.$cpn_name.'\','.$cpns.');"></i></span>';
			
			
			echo '</div>';
			/* if admin side then not show these buttons */
			if(!is_admin()){ 
			echo '<div class="cpn_optopn">';
			if($ext != 'pdf')
			{
				echo '<a class="button pr" onClick="printme(event,'.$key.')" href="#"><i class="fa fa-print"></i> ';
				_e('Print Voucher',CPN_DOMAIN);
				echo '</a>';
			} 
			echo '<a class="button dw" href="?dcoupon='.$cpn_name.'"><i class="fa fa-download"></i> ';
			_e('Download Voucher',CPN_DOMAIN);
			echo '</a><br/>';
			echo '</div>';
			}
			echo '</li>';
			}
		        
    }           
?>
	</ul>	
</section>

<?php endif;	

} 

/* Destination folder path for coupons for upload if folder is not exist then creates the folder */
function get_coupons_destination_path()
{	
   $wp_upload_dir = wp_upload_dir();

	$path = $wp_upload_dir['basedir'];
	$url = $wp_upload_dir['baseurl'];
	$destination_path = $path."/coupons";

		if(!is_dir($destination_path))
			{
				if (!mkdir($destination_path, 0777, true)) 
					 {
						die(__('Failed to create folders...',CPN_DOMAIN));
	 				 }
			}
		  
	  return $destination_path;
}

/* For download any files */
function downloadFile( $fullPath ){ 

  /* Must be fresh start */
  if( headers_sent() ) 
    die('Headers Sent'); 

  /* Required for some browsers */
  if(ini_get('zlib.output_compression')) 
    ini_set('zlib.output_compression', 'Off'); 

  /* File Exists? */
  if( file_exists($fullPath) ){ 
    
    /* Parse Info / Get Extension */
    $fsize = filesize($fullPath); 
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]); 
    
    /* Determine Content Type */
    switch ($ext) { 
      case "pdf": $ctype="application/pdf"; break; 
      case "exe": $ctype="application/octet-stream"; break; 
      case "zip": $ctype="application/zip"; break; 
      case "doc": $ctype="application/msword"; break; 
      case "xls": $ctype="application/vnd.ms-excel"; break; 
      case "ppt": $ctype="application/vnd.ms-powerpoint"; break; 
      case "gif": $ctype="image/gif"; break; 
      case "png": $ctype="image/png"; break; 
      case "jpeg": 
      case "jpg": $ctype="image/jpg"; break; 
      default: $ctype="application/force-download"; 
    } 

    header("Pragma: public"); /* required */
    header("Expires: 0"); 
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
    header("Cache-Control: private",false); /* required for certain browsers */
    header("Content-Type: $ctype"); 
    header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" ); 
    header("Content-Transfer-Encoding: binary"); 
    header("Content-Length: ".$fsize); 
    ob_clean(); 
    flush(); 
    readfile( $fullPath );
    exit; 

  } else 
    die('File Not Found'); 

}

/* Creates coupons tab in preview */
function show_coupons_tab_preview()
{
	if(isset($_POST['cpnarr']) && $_POST['cpnarr']!=""){	
			$_SESSION['file_info_coupon'] = explode(",",$_POST['cpnarr']);
				$_SESSION["templ_file_info_coupon"] = explode(",",$_POST['cpnarr']);
		}

	if($_SESSION["file_info_coupon"])
	{
		?> 
          <li class="tab-title" role="presentational"><a href="#listing_coupons"><?php echo _e('Vouchers',CPN_DOMAIN);?></a></li>         
      <?php 
	}
} 

/* Preview the uploaded coupons */
function show_coupon_preview_with_title()
{

echo '<h2 class="cpn_ttl">';
_e('Vouchers',CPN_DOMAIN);
echo '</h2>';
/* check if coupons are set or not */
if(isset($_POST['cpnarr']) && $_POST['cpnarr']!=""){	
	$_SESSION['file_info_coupon'] = explode(",",$_POST['cpnarr']);
	$_SESSION["templ_file_info_coupon"] = explode(",",$_POST['cpnarr']);
}
	
	if(isset($_SESSION["file_info_coupon"]))
	{
		echo '<section id="listing_coupons" class="content" aria-hidden="false" role="tabpanel">';
		echo '<ul>';
		$arrange_coupons = array_values(array_filter($_SESSION["file_info_coupon"]));
			foreach($arrange_coupons as $cpn_id=>$cpn)
				{
					$path_parts = pathinfo(TEVOLUTION_COUPON_TEMP_DIR.$cpn);
					$ext = strtolower($path_parts["extension"]);
					$cpn_src =  get_template_directory_uri().'/images/coupons/'.$cpn;
					
					if($ext == 'pdf')
						{
							$cpn_src = plugin_dir_url( __FILE__ ).'/images/pdfthumb.png';
						}
					
		 			
		 				echo '<li class="cpn">';
    					echo '<div class="cpn_img">';		
						echo '<img id="tmpl_img_'.$cpn_id.'" src="'.$cpn_src.'" height="188" width="268" />';
						
						echo '</div>';
		
						echo '<div class="cpn_optopn">';
						if($ext != 'pdf')
						{
							echo '<a class="button pr" href="javascript:void(0)"><i class="fa fa-print"></i> ';
							_e('Print Voucher',CPN_DOMAIN);
							echo '</a>';
					   } 
						echo '<a class="button dw" href="javascript:void(0)"><i class="fa fa-download"></i> ';
						_e('Download Voucher',CPN_DOMAIN);
						echo '</a><br/>';
						echo '</div>';
						echo '</li>';
				}
		echo '</ul>';	
		echo '</section>';	
	}			
}

/* Preview the uploaded coupons */
function show_coupon_preview()
{

/* check if coupons are set or not */
if(isset($_POST['cpnarr']) && $_POST['cpnarr']!=""){	
	$_SESSION['file_info_coupon'] = explode(",",$_POST['cpnarr']);
	$_SESSION["templ_file_info_coupon"] = explode(",",$_POST['cpnarr']);
}
	
	if(isset($_SESSION["file_info_coupon"]))
	{
		echo '<section id="listing_coupons" class="content" aria-hidden="false" role="tabpanel">';
		echo apply_filters('tmpl_voucher_title','');
		echo '<ul>';
		$arrange_coupons = array_values(array_filter($_SESSION["file_info_coupon"]));
			foreach($arrange_coupons as $cpn_id=>$cpn)
				{
					$path_parts = pathinfo(TEVOLUTION_COUPON_TEMP_DIR.$cpn);
					$ext = strtolower($path_parts["extension"]);
					$cpn_src =  get_template_directory_uri().'/images/coupons/'.$cpn;
					
					if($ext == 'pdf')
						{
							$cpn_src = plugin_dir_url( __FILE__ ).'/images/pdfthumb.png';
						}
					
		 			
		 				echo '<li class="cpn">';
    					echo '<div class="cpn_img">';		
						echo '<img id="tmpl_img_'.$cpn_id.'" src="'.$cpn_src.'" height="188" width="268" />';
						
						echo '</div>';
		
						echo '<div class="cpn_optopn">';
						if($ext != 'pdf')
						{
							echo '<a class="button pr"  href="javascript:void(0)"><i class="fa fa-print"></i> ';
							_e('Print Voucher',CPN_DOMAIN);
							echo '</a>';
					   } 
						echo '<a class="button dw" href="javascript:void(0)"><i class="fa fa-download"></i> ';
						_e('Download Voucher',CPN_DOMAIN);
						echo '</a><br/>';
						echo '</div>';
						echo '</li>';
				}
		echo '</ul>';	
		echo '</section>';	
	}			
}

/*
Name : numberof_coupons_allowed_submitform
Description : Creates a new row for upload number of allowed coupons at admin side in General Settings
*/
function numberof_coupons_allowed_submitform()
{
	if(!is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
	{
	$tmpdata = get_option('templatic_settings');
	$num_of_coupons = ($tmpdata['numberof_coupons'] == '')?'10':$tmpdata['numberof_coupons'];
	?>
	<tr>
		<th><label id="numberof_coupons_general_setting"><?php _e('Number of vouchers allowed',CPN_DOMAIN); ?></label></th>
		<td>
			<label for="numberof_coupons">
				<input type="text" name="numberof_coupons" size="4" value="<?php echo $num_of_coupons; ?>" id="numberof_coupons" />
			</label>
			<p class="description"><?php _e('Enter the number of vouchers (coupons) visitors are allowed to upload during the submission process. </br><b>Note:</b> This option is not applicable when the Fields Monetization add-on is installed. When using that add-on you can set the coupon limit for each individual price package. ',CPN_DOMAIN); ?></p>
		</td>
	</tr>
	<?php
	} 		 
}
/*
Name : numberof_coupons_allowed_inpackages
Description : Creates a new row for number of coupons at admin side in packages
*/
function numberof_coupons_allowed_inpackages($id)
{
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(is_plugin_active('Tevolution-FieldsMonetization/fields_monetization.php'))
	{
	?>

		<tr>
			<th valign="top">
				<label class="form-textfield-label" for="coupons_allowed" id="coupons_allowed"><?php _e('Number of vouchers allowed',CPN_DOMAIN); ?> </label>
			</th>
			<td>
				<input type="text" value="<?php if(isset($id) && $id != '' && get_post_meta($id, 'coupons_allowed', true) != "") { echo get_post_meta($id, 'coupons_allowed', true); }?>" id="coupons_allowed" size="4" name="coupons_allowed">
				<br><p class="description"><?php echo __('Enter the number of vouchers (coupons) visitors are allowed to upload during the submission process.',CPN_DOMAIN); ?></p>
			</td>
		</tr>	
	
	<?php
	}
}


/* 
Name : save_custom_fields
description : Save custom fields as per price package selection.
*/
function save_numberof_coupons_allowed()
{
	global $last_postid;
	$num_of_coupon = ($_REQUEST['coupons_allowed'] != '')?$_REQUEST['coupons_allowed']:'10';
	update_post_meta($last_postid, 'coupons_allowed', $num_of_coupon);
}

/* 
Name : label_for_coupon
description : Create label for coupons in Custom Fields in packages
*/
function label_for_coupon($type)
{
	if($type == 'coupon_uploader')
		{
			$type_text = __('Coupon Upload',CPN_DOMAIN);
		}		
	return $type_text;		
}

/* 
Name : destroy_coupons_from_tmp
description : Destroys the session for uploaded coupons and removes coupons from teperory folder after successfull submit form
*/
function destroy_coupons_from_tmp()
{
	if( !empty( $_SESSION["file_info_coupon"] ) ){
		foreach( $_SESSION["file_info_coupon"] as $image_id=>$val ){
			if(file_exists(TEMPLATEPATH."/images/coupons/".$val) && $val!='')
				unlink(TEMPLATEPATH."/images/coupons/".$val);
		}
	}
	unset($_SESSION['file_info_coupon']);
	unset($_SESSION['file_info_coupon']);
	unset($_SESSION['templ_file_info_coupon']);
}



/*
Name : coupons_admin_notices
Description : return message tevolution and tevolution directory plugin is activated or not.
*/
function coupons_admin_notices()
{
	if(!is_plugin_active('Tevolution/templatic.php'))
	{
		echo '<div class="error"><p>' . sprintf(__('You have not activated the base plugin %s. Please activate it to use Tevolution - Coupons plugin.',CPN_DOMAIN),'<b>Tevolution</b>'). '</p></div>';
	}
}


/*
Name : validate_coupon_custom_field
Description : Field validation.
*/
function validate_coupon_custom_field($js_code)
{
	$coupon_post_id = get_post_id_by_meta_key_and_value('ctype','coupon_uploader');
	
	$is_required = get_post_meta($coupon_post_id,'is_require',true);
	
	$msg = get_post_meta($coupon_post_id,'field_require_desc',true);
	
	

	if(isset($is_required) && $is_required == 1)
	{
		$js_code .= 'if(jQuery("#cpnarr").val() == "")
				{
					jQuery("#coupon_status").html("");
					jQuery("#post_coupons_error").addClass("message_error2");
					if("'.$msg.'" == "")
					{
					    
						jQuery("#post_coupons_error").html("'.__('Please upload vouchers..',CPN_DOMAIN).'");
						return false;
					}
					else
					{
						
						jQuery("#post_coupons_error").html("'.$msg.'");
						return false;	
					}
				}';
	}		
	return $js_code;
}


/*
Name : get_post_id_by_meta_key_and_value
Description : gets the post id from meta-key and value.
*/
function get_post_id_by_meta_key_and_value($key, $value) {
	global $wpdb;
	$meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='$key' AND meta_value='$value'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
		$meta = $meta[0];
	}		
	if (is_object($meta)) {
		return $meta->post_id;
	}
	else {
		return false;
	}
}

function coupons_listing_in_mobile_device()
{

	global $htmlvar_name,$tmpl_flds_varname;
	
	$coupons = get_post_meta(get_the_ID(),'coupon_ids',true);
	
	/*check whether coupon code post exist or not*/
	if($coupons!=""):
		$coupons_arr = explode(',',$coupons);
		foreach($coupons_arr as $key=>$cpns)
		{
			if(FALSE !== get_post_status( $cpns ))
			{
				$found_coupons = true;
			}
		}
	endif;

	if(!empty($coupons) && $found_coupons && ($tmpl_flds_varname['post_coupons'] || $htmlvar_name['field_label']['post_coupons'])):
	
	$coupons_arr = explode(',',$coupons);

?>
<dd class="tmpl-accordion-navigation"><a href="#listing_coupons"><?php echo apply_filters('tmpl_voucher_title',__('Vouchers',CPN_DOMAIN)); ?></a>
		<div id="listing_coupons" class="content">
	<?php 
		
			
		foreach($coupons_arr as $key=>$cpns)
		{
			$coupons = get_post($cpns);
			
			if($coupons)
			{
		
			$url = wp_get_attachment_url( $cpns );
			$cpn_name = basename($url); 
					
			$file = TEVOLUTION_COUPON_MAIN_DIR.$cpn_name;
			$path_parts = pathinfo($file);
			$ext = strtolower($path_parts["extension"]);
			$img_url = $url;
			
				if($ext == 'pdf')
					{
						$img_url = plugin_dir_url( __FILE__ ).'/images/pdfthumb.png';
					}
				else 
					{
						$img_thumbnail = wp_get_attachment_image_src( $cpns );
					}
				$idname = explode('.',$coupons->post_title);	
				echo '<div class="cpn" id="'.$idname[0].'">';
				echo '<div class="cpn_img">';	
				if(strstr($_SERVER['REQUEST_URI'],'/wp-admin/')){			
					echo '<img id="tmpl_img_'.$key.'" src="'.$img_url.'" height="78" width="78" />';
				}else{
					echo '<img id="tmpl_img_'.$key.'" src="'.$img_url.'" height="188" width="268" />';
				}
				if(is_admin())
					echo '<span class="close"><i class="dashicons dashicons-no" onclick="listing_voucher_delete_image(\''.$idname[0].'\',\''.$cpn_name.'\','.$cpns.');"></i></span>';
				
				
				echo '</div>';
				/* if admin side then not show these buttons */
				if(!is_admin()){ 
				echo '<div class="cpn_optopn">';
				if($ext != 'pdf')
				{
					echo '<a class="button pr" onClick="printme(event,'.$key.')" href="#"><i class="fa fa-print"></i> ';
					_e('Print Voucher',CPN_DOMAIN);
					echo '</a>';
				} 
				echo '<a class="button dw" href="?dcoupon='.$cpn_name.'"><i class="fa fa-download"></i> ';
				_e('Download Voucher',CPN_DOMAIN);
				echo '</a><br/>';
				echo '</div>';
				}
				echo '</div>';
				}
					
		}
	?>
	</div>	
</dd>	
	<?php
	endif;

}

/* 
	Move the uploaded image 
*/
if(!function_exists('tmpl_move_voucher_file')){
	function tmpl_move_voucher_file($src,$dest){
		copy($src, $dest);
		
		$dest = explode('/',$dest);
		$img_name = $dest[count($dest)-1];
		$img_name_arr = explode('.',$img_name);
		$my_post = array();
		$my_post['post_title'] = $img_name_arr[0];
		$my_post['guid'] = get_bloginfo('url')."/files/".get_voucher_destination_path_plugin().$img_name;
		return $my_post;
	}
}

/* 
	Return the Image Final path
*/
if(!function_exists('get_voucher_destination_path_plugin')){
	function get_voucher_destination_path_plugin(){
		$today = getdate();
		if ($today['month'] == "January"){
		  $today['month'] = "01";
		}
		elseif ($today['month'] == "February"){
		  $today['month'] = "02";
		}
		elseif  ($today['month'] == "March"){
		  $today['month'] = "03";
		}
		elseif  ($today['month'] == "April"){
		  $today['month'] = "04";
		}
		elseif  ($today['month'] == "May"){
		  $today['month'] = "05";
		}
		elseif  ($today['month'] == "June"){
		  $today['month'] = "06";
		}
		elseif  ($today['month'] == "July"){
		  $today['month'] = "07";
		}
		elseif  ($today['month'] == "August"){
		  $today['month'] = "08";
		}
		elseif  ($today['month'] == "September"){
		  $today['month'] = "09";
		}
		elseif  ($today['month'] == "October"){
		  $today['month'] = "10";
		}
		elseif  ($today['month'] == "November"){
		  $today['month'] = "11";
		}
		elseif  ($today['month'] == "December"){
		  $today['month'] = "12";
		}
		global $upload_folder_path;
		$tmppath = $upload_folder_path;
		global $blog_id;
		if($blog_id)
		{
			return $user_path = $today['year']."/".$today['month']."/";
		}else
		{
			return $user_path = get_option( 'siteurl' ) ."/$tmppath".$today['year']."/".$today['month']."/";
		}
	}
}
/* end of file */
?>