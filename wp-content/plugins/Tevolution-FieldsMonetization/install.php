<?php
if(is_admin() && isset($_REQUEST['page']) && $_REQUEST['page'] == 'monetization')
{
	include_once(FIELDS_MONETIZATION_PLUGIN_DIR.'/templatic_custom_fields_monetization.php');
}
if(!is_active_custom_fields_addons('Tevolution/templatic.php') || (isset($_REQUEST['activate']) && $_REQUEST['activate'] == 'fields_monetization'))
{
	add_action('admin_notices','fields_monetization_admin_notices');
}

/*
Name : is_active_custom_fields_addons
Description : return each add-ons is activated or not
*/
function is_active_custom_fields_addons($key)
{
	$act_key = get_option($key);
	if ($act_key != '')
	{
		return true;
	}
}
/*
Name : fields_monetization_admin_notices
Description : return message tevolution plugin is activated or not.
*/
function fields_monetization_admin_notices()
{
	if(isset($_REQUEST['activate']) && $_REQUEST['activate'] == 'fields_monetization')
	{
		echo '<div class="updated"><p>' . sprintf(__('You have activated Tevolution - Fields Monetization plugin, this will allow you to limit custon fields, categories, number of images users can upload, etc in different price packages. We recommend you to go through the detailed <b><a href="http://templatic.com/docs/tevolution-fields-monetization/">documentation guide</a></b> of this plugin.',TFM_DOMAIN),'<b>Tevolution</b>'). '</p></div>';
	}
	if(!is_plugin_active('Tevolution/templatic.php'))
	{
		echo '<div class="error"><p>' . sprintf(__('You have not activated the base plugin %s. Please activate it to use Tevolution - Fields Monetization plugin.',TFM_DOMAIN),'<b>Tevolution</b>'). '</p></div>';
	}
}
/*
Name: custom_fields_add_pkg_js
desc : return the script for fetching price packages
*/
function custom_fields_add_pkg_js(){
	global $wp_query,$pagenow,$post;
	
	// If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object	
	if($post)
		$is_tevolution_submit_form = get_post_meta( $post->ID, 'is_tevolution_submit_form', TRUE );
		$is_frontend_submit_form = get_post_meta( $post->ID, 'is_frontend_submit_form', TRUE );
	if((is_page() &&  ($is_tevolution_submit_form==1 || $is_frontend_submit_form==1)) ||(is_admin() && $pagenow=='post.php') && is_plugin_active('Tevolution/templatic.php')){ 
		include(dirname(__FILE__).'/shortcodes/price_package_js.php');
	}
}
add_action('admin_head','custom_fields_add_pkg_js');
add_action('wp_head','custom_fields_add_pkg_js');
?>