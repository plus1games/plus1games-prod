<?php
/*
Name: show_custom_fields_monetization
description : show html for field monetization in price package.
*/
add_action('fields_monetization','show_custom_fields_monetization',$id);
function show_custom_fields_monetization($id = '')
{
?>
<!-- added the html for user can select masimum categroy -->
	<tr>
		<th valign="top">
			<label for="category_can_select" class="form-textfield-label"><?php _e('Allow user to select number of categories',TFM_DOMAIN); ?></label>
		</th>
		<td>
			<input type="text" size="10" value="<?php echo get_post_meta($id, 'category_can_select', true); ?>" id="category_can_select" name="category_can_select">
			<br/>
			<p class="description"><?php _e('Enter maximum number of categories that you want your users to select when they submit a listing from frontend.',TFM_DOMAIN); ?></p>
		</td>
	</tr>
	<!-- ends the html for user can select masimum categroy -->
	<tr>
		<th colspan="2"><div class="tevo_sub_title"><?php _e('Settings for custom fields',TFM_DOMAIN); ?></div>
		<p class="tevolurion_desc"><?php _e('These settings are must if you want to charge the users for custom fields wise',TFM_DOMAIN); ?>.</p></th>
	</tr>
	
	<tr>
		<th valign="top">
			<label for="billing_period" class="form-textfield-label"><?php _e('Custom Fields',TFM_DOMAIN); ?></label>
		</th>
		<td >
         <span id='processing' style='display:none;'><i class='fa fa-circle-o-notch fa-spin'></i></span>
			<span id="custom_fields">
           
			<?php
				
				if(get_post_meta($id,'custom_fields', true) || ( isset($_REQUEST['action']) && $_REQUEST['action'] =='edit'))
				{
					do_action('show_custom_fields_monetization_edit');
				}else
				{
					?><div style="color:red;"><?php _e('Please select any of the above category.',TFM_DOMAIN);?></div><?php
				}
			?>
			</span>
			<p class="description"><?php _e('Check the above box if you want the price package to show selected custom fields.',TFM_DOMAIN); ?>
			<br/>
			<b><?php _e('Note:',TFM_DOMAIN) ?></b>
			<?php _e('Text box below will allow user to fill the textbox, textarea and texteditor to maximum character count.',TFM_DOMAIN); ?>
			</p>
		</td>
	</tr>
	<tr>
		<th valign="top">
			<label for="billing_period" class="form-textfield-label"><?php _e('Allow number of images to upload',TFM_DOMAIN); ?></label>
		</th>
		<td>
			<input type="text" size="10" value="<?php echo get_post_meta($id, 'max_image', true); ?>" id="max_image" name="max_image">
			<br/>
			<p class="description"><?php _e('Enter the maximum number of images can upload if not user can upload maximum "Ten" images by default.',TFM_DOMAIN); ?></p>
		</td>
	</tr>
	
	<?php do_action('fields_monetization_new_row',$id); ?>
	<tr>
		<th colspan="2"><div class="tevo_sub_title"><?php _e('Price Settings',TFM_DOMAIN); ?></div></th>
	</tr>
	
<?php
}
/*
Name: show_cust_fileds
description : fetch custom fields for price package backend.
*/
add_action('admin_footer','show_cust_fileds');
function show_cust_fileds()
{
	if(is_plugin_active('wpml-translation-management/plugin.php')){
		$site_url = get_bloginfo( 'wpurl' )."/wp-admin/admin-ajax.php?lang=".ICL_LANGUAGE_CODE ;	
	}else{
		$site_url = get_bloginfo( 'wpurl' )."/wp-admin/admin-ajax.php" ;
	}
?>
<script>
var ajaxUrl = "<?php echo esc_js( $site_url); ?>";
jQuery('#field_category').click(function(){
	var id = '';
	var sep = ",";
	var chk_arr =  document.getElementsByName("category[]");
	var chklength = chk_arr.length;             
	
	for(k=0;k< chklength;k++)
	{
		if(chk_arr[k].checked)
		{
			if(k == (parseInt(chklength) - 1))
			{
				sep = '';
			}
			id += chk_arr[k].value;
		}
	}
	var form_data = jQuery('#monetization').serialize();
	var package_id = '';
	var package_url = '';
	if(jQuery('#package_id'))
	{
		package_id = jQuery('#package_id').val();
		package_url = '&package_id='+package_id;
	}
	var custom_fields = null;
	document.getElementById('processing').style.display = '';
	custom_fields = jQuery.ajax({
		url:ajaxUrl,
		type:'POST',
		data:'action=show_custom_fields_monetization1&termid=' + id+'&form_data='+form_data+package_url,
		beforeSend : function(){
			if(custom_fields != null){
				custom_fields.abort();
			}
        },
		success:function(results) {
			document.getElementById('processing').style.display = 'none';
			if(results && results != 0 && results.search("custom_field_not_found") <=0)
			{
				jQuery('#custom_fields').html(results);
				jQuery('#custom_fields').css('color', '');
			}
			else
			{
				if(id == '')
				{
					jQuery('#custom_fields').html('Please select category from above.');
					jQuery('#custom_fields').css('color', 'red');
				}
				else if(results.search("custom_field_not_found") > 0)
				{
					var edit_url_text = "<?php _e('custom fields',TFM_DOMAIN); ?>";
					var edit_url = "<?php echo '<a target=\"_blank\" href='.admin_url("admin.php?page=custom_setup&ctab=custom_fields").'>' ?>";
						edit_url += edit_url_text;
						edit_url += "<?php echo '</a>'; ?>";
					jQuery('#custom_fields').html("<?php _e('Selected category does not have any custom fields.Please create it from ',TFM_DOMAIN); ?>"+edit_url);
					jQuery('#custom_fields').css('color', 'red');
				}
			}
		}
	});
});

/*
* validation for number of category selection for particular post type.
*/
jQuery('#monetization').submit(function(){
	var validate = true;
	var validation_message =  "<?php echo __("Please select a number of categories greater than or equal to the number of allowed categories per submission or decrease the number of allowed categories in the field below",TFM_DOMAIN); ?>";
	var taxonomy = [];
		var previous_tax = '';
		
		jQuery( "#field_category #category_checklist li" ).each(function( index ) {
		
			if(jQuery(this).attr('id') != '' && jQuery(this).attr('id') != 'undefined' &&  jQuery(this).attr('id'))
			{
				var current_id = jQuery(this).attr('id').split('-');
				if(previous_tax != current_id[0]){
					taxonomy.push(current_id[0]);
					previous_tax = current_id[0];
				}
				
			}
		});	

		
	for(var i = 0; i < taxonomy.length; i++){
		
		if(jQuery("#category_can_select").val() > 0 )
		{
			if(jQuery("#category_can_select").val() > jQuery("."+taxonomy[i]+" input[name^=category]:checked").length)
			{
				validate = false;
			}
		}else
		{
			validate = true;
		}
		
    }	
		
	if(!validate)
	{
		jQuery('#validate_message').html('');
		jQuery('#field_category').after('<div id="validate_message" class="message_error2">'+validation_message+'</div>');
		return false;
	}
	return true;
});
jQuery( document ).ready(function() {
	var validate = true;
	var validation_message = "<?php echo __("Please select a number of categories greater than or equal to the number of allowed categories per submission or decrease the number of allowed categories in the field below",TFM_DOMAIN); ?>";
	
		var taxonomy = [];
		var previous_tax = '';
		
		jQuery( "#field_category #category_checklist li" ).each(function( index ) {
		
			if(jQuery(this).attr('id') != '' && jQuery(this).attr('id') != 'undefined' &&  jQuery(this).attr('id'))
			{
				var current_id = jQuery(this).attr('id').split('-');
				if(previous_tax != current_id[0]){
					taxonomy.push(current_id[0]);
					previous_tax = current_id[0];
				}
				
			}
		});	

		
	for(var i = 0; i < taxonomy.length; i++){
		
		if(jQuery("#category_can_select").val() > 0 )
		{
			if(jQuery("#category_can_select").val() > jQuery("."+taxonomy[i]+" input[name^=category]:checked").length)
			{
				validate = false;
			}
		}else
		{
			validate = true;
		}
		
    }	
			
		
	if(!validate)
	{
		jQuery('#validate_message').html('');
		jQuery('#field_category').after('<div id="validate_message" class="message_error2">'+validation_message+'</div>');
		return false;
	}
	return true;
});
</script>
<?php
}
?>