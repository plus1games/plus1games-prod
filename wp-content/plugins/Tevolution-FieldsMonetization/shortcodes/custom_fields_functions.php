<?php
/*
 * Function Name: fetch_custom_fields_packages_front_end
 * Return: display the submit form from front end side
 */
function fetch_custom_fields_packages_front_end($pkg_id='',$div_id,$post_type,$taxonomy_slug,$post_cat='')
{
	global $wpdb,$post;		
	$post_categories = explode(',',$post_cat);
	if($div_id != 'ajax_packages_checkbox'){ $class ='form_row_pkg clearfix'; }
	$package_ids = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'monetization_package' AND post_status = 'publish'");
		if($div_id !='all_packages'){ /* this query will execute only for category wise packages */
		$pargs = array('post_type' => 'monetization_package','posts_per_page' => -1	,'post_status' => array('publish'),
				  'meta_query' => array('relation' => 'AND',array('key' => 'package_post_type','value' => $post_type,'compare' => 'LIKE','type'=> 'text'),array('key' => 'package_status','value' =>  '1','compare' => '=')),
				  'tax_query' => array( array('taxonomy' => $taxonomy_slug,'field' => 'id','terms' => $post_categories,'include_children'=>false,'operator'  => 'IN') ),
			'orderby' => 'menu_order',
			'order' => 'ASC'
			);
		}else{
		$pargs = array('post_type' => 'monetization_package','posts_per_page' => -1	,'post_status' => array('publish'),
				  'meta_query' => array('relation' => 'AND',array('key' => 'package_post_type','value' =>$post_type,'compare' => 'LIKE','type'=> 'text'),array('key' => 'package_status','value' =>  '1','compare' => '=')),
			'orderby' => 'menu_order',
			'order' => 'ASC'
			);
		}
		wp_reset_query();
		$package_query = null;
		$package_query = new WP_Query($pargs); 	
		if( $package_query->have_posts())
		{
			$i=0;
			while($package_query->have_posts())
			{ 
				$package_query->the_post();
				$disply_price_package=apply_filters('tevolution_price_package_loop_frontend','1',$post,$post_type);
				if($disply_price_package==''){
					continue;	
				}
				if($i==0){
					$package_id=get_the_ID();
				}
				$i++;
				
			}
		}
		
		if($div_id =='all_packages'){
			if(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit'] !=''){
			$cat_price = $_SESSION['custom_fields']['all_cat_price'];
			}else{ $cat_price =''; }
			if(isset($_REQUEST['category']) && $_REQUEST['category'] != ""){
				$cats_of =  count($_REQUEST['category']); 
			}
			else
			{ $cats_of = "";}
		 ?>
		<input type="hidden" name="all_cat" id="all_cat" value="0"/>
		<?php 
			$tmpdata = get_option('templatic_settings');
			if(isset($tmpdata['templatic-category_type']) == 'select'):
		?>	
		<input type="hidden" name="all_cat_price" id="all_cat_price" value="<?php if(isset($_REQUEST['category']) && $_REQUEST['category'] !=""){ if(is_array($_REQUEST['category']) && $cats_of >0){ $cat = explode(",",$_REQUEST['category'][0]); echo $cat[1]; }else{ echo $_REQUEST['category'];  }  }else{ if(isset($cat_price) && $cat_price !=''){ echo $cat_price; }else{ echo "0"; } }  ?>"/>
		<?php else: ?>
		<input type="hidden" name="all_cat_price" id="all_cat_price" value="<?php if(isset($_REQUEST['category']) && $_REQUEST['category'] !=""){ if(is_array($_REQUEST['category']) && $cats_of >1){ $cat = implode(",",$_REQUEST['category']); echo $cat[2]; }else{ echo $_REQUEST['category'];  }  }else{ if(isset($cat_price) && $cat_price !=''){ echo $cat_price; }else{ echo "0"; } }  ?>"/>
		<?php endif; ?>
		
		<?php } ?>

		<div id="<?php echo $div_id; ?>" class="<?php echo $class; ?>">
		<?php
		if( $package_query->have_posts() && (!isset($_REQUEST['action']) && @$_REQUEST['action'] !='edit'))
		{
			if($div_id =='all_packages'){ ?>
				<div class="sec_title"><h3 id="package_data"><?php _e('Select a Package',TFM_DOMAIN); ?></h3></div>
		<?php }
			if(isset($_SESSION['package_select']) && $_SESSION['package_select'] != '')
			{
				$selected_pkg = $_SESSION['package_select'];
			}
			elseif(!isset($selected_pkg) && $selected_pkg == '')
			{
				$selected_pkg = $package_id;
			}
			if($pkg_id !=''){
				$selected_pkg = $pkg_id;
			}			
		
		while($package_query->have_posts())
		{ 
			$package_query->the_post();
			
			$disply_price_package=apply_filters('tevolution_price_package_loop_frontend','1',$post,$post_type);
			if($disply_price_package==''){
				continue;	
			}
			
			$package_type = get_post_meta($post->ID,'package_type',true);
			$package_post_type = get_post_meta($post->ID,'package_post_type',true);
			$package_categories = get_post_meta($post->ID,'category',true);
			$show_package = get_post_meta($post->ID,'show_package',true);
			$package_amount = get_post_meta($post->ID,'package_amount',true);
			$recurring = get_post_meta($post_id,'recurring',true);
			if($package_type == 2 && $recurring == 1){
				$package_validity = get_post_meta($post->ID,'billing_num',true);
				$package_validity_per =get_post_meta($post->ID,'billing_per',true);
			}else{
				$package_validity = get_post_meta($post->ID,'validity',true);
				$package_validity_per = get_post_meta($post->ID,'validity_per',true);
			}
			$package_status = get_post_meta($post->ID,'package_status',true);
			$recurring = get_post_meta($post->ID,'recurring',true);
			$billing_num = get_post_meta($post->ID,'billing_num',true);
			$billing_per = get_post_meta($post->ID,'billing_per',true);
			$billing_cycle = get_post_meta($post->ID,'billing_cycle',true);
			$is_featured = get_post_meta($post->ID,'is_featured',true);
			$feature_amount_home = get_post_meta($post->ID,'feature_amount',true);
			$feature_cat_amount = get_post_meta($post->ID,'feature_cat_amount',true);  
			$featured_h = get_post_meta($post->ID,'home_featured_type',true); 
			$featured_c = get_post_meta($post->ID,'featured_type',true);
			$package_is_recurring = get_post_meta($post->ID,'recurring',true);
			$package_billing_num = get_post_meta($post->ID,'billing_num',true);
			$package_billing_per =get_post_meta($post->ID,'billing_per',true);
			$package_billing_cycle =get_post_meta($post->ID,'billing_cycle',true);
			
				if(isset($category_id)){ $catid = $category_id; }else{ $catid =''; }
				if(isset($cat_array) && $cat_array != "")
				{
					$catid = $cat_array;
				}
				else
				{
					if(isset($_REQUEST['category'])){
					$catid = $_REQUEST['category'];
					}else{ $catid =''; }
				}
				$post_title = $post->post_title; //price package title.
				$post_content = $post->post_content; // price package description. */
				
				if(function_exists('icl_register_string')){
					icl_register_string(TFM_DOMAIN, 'package_title_'.$post->ID,$post_title);
					$post_title = icl_t(TFM_DOMAIN, 'package_title_'.$post->ID,$post_title);
					icl_register_string(TFM_DOMAIN, 'package_description_'.$post->ID,$post_content);
					$post_content = icl_t(TFM_DOMAIN, 'package_description_'.$post->ID,$post_content);
				}	
				
				$package_type_title = '<b>'.__('Package type',TFM_DOMAIN)."</b>: ";  
				if($package_type  ==2){ 
					$package_type_title .= __('Pay per subscription',TFM_DOMAIN); 
				}
				else{ 
					$package_type_title .= __('Pay per post',TFM_DOMAIN); 
				} // price package type.
				$no_of_post = '';
				if($package_type  == 2 && get_post_meta($post->ID,'limit_no_post',true) !=''){
					$no_of_post = '<b>'.__('Number of Posts',TFM_DOMAIN)."</b>: ".get_post_meta($post->ID,'limit_no_post',true); // limit to submit no. of post with particular price package.
				 }
				 $package_cost = fetch_currency_with_position($package_amount); // price package cost
				 $price_package_validity = '<b>'.__('Validity',TFM_DOMAIN)."</b>: ". $package_validity; // price package validity
				 $package_val_period = ''; // price package day.
				   if($package_validity_per == 'D')
					{
						$package_val_period = __('Days',TFM_DOMAIN);
					}
					elseif($package_validity_per == 'M')
					{
						$package_val_period = __('Months',TFM_DOMAIN);
					}
					else
					{
						$package_val_period = __('Years',TFM_DOMAIN);
					} 
					$package_recurring = ''; // is price package recurring.
					if($package_is_recurring=='1')
					{
						$package_recurring .= '<p class=""><span><b>'.__('Recurring Charges',TFM_DOMAIN).'</b>:&nbsp;</span><span>'.$package_billing_num ."&nbsp;";
						if($package_billing_per == 'D')
						{
							$package_recurring .= __('Days',TFM_DOMAIN);
						}
						elseif($package_billing_per == 'M')
						{
							$package_recurring .= __('Months',TFM_DOMAIN);
						}
						else
						{
							$package_recurring .= __('Years',TFM_DOMAIN);
						}
						$package_recurring .= "&nbsp;".$package_billing_cycle.__(' Cycle',TFM_DOMAIN). "</span>";
					}
				?>
				<div class="package" >
					<label ><input type="radio" onclick="monetize_show_featuredprice(<?php echo $post->ID; ?>);" id="price_select_<?php echo $post->ID; ?>" name="package_select" value="<?php echo $post->ID; ?>" <?php if($selected_pkg == $post->ID) {?>  checked="checked" <?php  } ?>>
						 <h3><?php echo $post_title; ?></h3>
						 <p><?php echo $post_content; ?></p>
						 <p><?php echo $package_type_title; ?></p>
						 <?php	echo $no_of_post; ?>
						 <p class="cost"><span class="price"><?php echo '<b>';_e('Charges',TFM_DOMAIN);echo ':</b>';echo $package_cost; ?></span><span><?php echo ' '.$price_package_validity; ?>
						 <?php  echo $package_val_period; ?></span></p>
						 <?php	echo $package_recurring; ?>
					</label>
				</div>
	<?php } 
	} ?>    		 
	</div>        
<?php
}
 /*
Name : show_comment_rating_single
Description : show rating on single page after page gets loaded.
*/
add_action('wp_footer','monetize_show_comment_rating_single',20);
if(!function_exists('monetize_show_comment_rating_single'))
{
	function monetize_show_comment_rating_single()
	{
		global $post;
		remove_all_actions('posts_where');
		$post_type = get_post_meta($post->ID,'submit_post_type',true);
		if((get_post_meta($post->ID,'is_tevolution_submit_form',true) || get_post_meta($post->ID,'is_frontend_submit_form',true) ) && (strstr($post->post_content,'custom_fields_submit_form') || strstr($post->post_content,'frontend_edit_submit_form') ))
		{
			$pargs = array('post_type' => 'monetization_package','posts_per_page' => -1	,'post_status' => array('publish'),
					  'meta_query' => array('relation' => 'AND',array('key' => 'package_post_type','value' =>$post_type,'compare' => 'LIKE','type'=> 'text'),array('key' => 'package_status','value' =>  '1','compare' => '=')),
				'orderby' => 'menu_order',
				'order' => 'ASC'
				);
			wp_reset_query();
			$package_query = null;
			$package_query = new WP_Query($pargs); 	
			if( $package_query->have_posts() && (!isset($_REQUEST['action']) && @$_REQUEST['action'] !='edit') && (!isset($_REQUEST['backandedit']) && @$_REQUEST['backandedit'] !=1))
			{
				while($package_query->have_posts())
				{ 
					$package_query->the_post();					
					
					$disply_price_package=apply_filters('tevolution_price_package_loop_frontend','1',$post,$post_type);
					if($disply_price_package==''){
						continue;	
					}
			?>
				<script>
				jQuery(window).load(function () {
					if(document.getElementById('submit_coupon_code') && document.getElementById('total_price').value > 0)
					{
						document.getElementById('submit_coupon_code').style.display='';
					}
					else
					{
						if(document.getElementById('submit_coupon_code')){
							document.getElementById('submit_coupon_code').style.display='none';
						}
					}
					monetize_show_featuredprice(<?php echo get_the_ID(); ?>);
				});
				</script>
			<?php
					break;
				}
			}
		}
	}
}
/*
Name: show_category_as_price_package
desc : add script category list as per selection of price package at front end.
*/
add_action('wp_footer','show_category_as_price_package',20);
function show_category_as_price_package()
{
	if(is_plugin_active('wpml-translation-management/plugin.php')){
		$site_url = get_bloginfo( 'wpurl' )."/wp-admin/admin-ajax.php?lang=".ICL_LANGUAGE_CODE ;	
	}else{
		$site_url = get_bloginfo( 'wpurl' )."/wp-admin/admin-ajax.php" ;
	}
?>
<script>
var ajaxUrl = "<?php echo esc_js( $site_url ); ?>";
function  show_monetize_category(package_id){
	var id = '';
	var sep = ",";
	var package_id = '';
	var package_url = '';
	if(document.getElementById('package_id'))
	{
		package_id = jQuery('#package_id').val();
		package_url = '&package_id='+package_id;
	}else
	{
		package_id = jQuery('[name="package_select"]:checked').val();
		package_url = '&package_id='+package_id;
	}
	var form_data = jQuery('#submit_form').serialize();
	jQuery.ajax({
		url:ajaxUrl,
		type:'POST',
		data:'action=show_custom_fields_category&form_data='+form_data+package_url,
		success:function(results) {
			if(results && results != 0)
			{
				if(jQuery('.cont_box')){
				jQuery('.cont_box').html(results);
				jQuery('.cont_box').css('color', '');
				}
				jQuery('.cont_box').css('display', 'block');
			}
			else
			{
				jQuery('.cont_box').html("<?php _e('Sorry no category found for the selected price package.',TFM_DOMAIN); ?>");
				jQuery('.cont_box').css('color', 'red');
				if(jQuery('.cont_box')){
				jQuery('.cont_box').css('display', 'none');
				}
			}
		}
	});
}
</script>
<?php
}
/*
Name: show_custom_fields_category
desc : return category list as per selection of price package at front end.
*/
add_action('wp_ajax_nopriv_show_custom_fields_category','show_custom_fields_category',20);
add_action('wp_ajax_show_custom_fields_category','show_custom_fields_category',20);
function show_custom_fields_category()
{
	global $wpdb,$post;
	$submit_post_type = get_post_meta($_REQUEST['cur_post_id'],'submit_post_type',true);
	remove_all_actions('posts_where');
		$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$submit_post_type.'',
				'value' => array($submit_post_type,'all'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			)
		),		
		 
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
	$post_query = null;
	$post_query = new WP_Query($args);	
	$post_meta_info = $post_query;
	$return_arr = array();
	if($post_meta_info){
		while ($post_meta_info->have_posts()) : $post_meta_info->the_post();
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}
			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      =>  $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
					"heading_type" => get_post_meta($post->ID,"heading_type",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
		endwhile;
	}
	custom_fields_display_custom_category_field_plugin($return_arr,'custom_fields',$submit_post_type,$submit_post_type);exit;
}
/*
Name: monetize_show_custom_fields_category
desc : return custom fields as per category.
*/
function monetize_show_custom_fields_category($submit_post_type='')
{
	global $wpdb,$post;
	
	remove_all_actions('posts_where');
		$args=
		array( 
		'post_type' => 'custom_fields',
		'posts_per_page' => -1	,
		'post_status' => array('publish'),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'post_type_'.$submit_post_type.'',
				'value' => array($submit_post_type,'all'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			array(
				'key' => 'show_on_page',
				'value' =>  array('user_side','both_side'),
				'compare' => 'IN',
				'type'=> 'text'
			),
			
			array(
				'key' => 'is_active',
				'value' =>  '1',
				'compare' => '='
			)
		),
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'id',
				'terms' => 1,
				'operator'  => 'IN'
			)
			
		 ),
		 
		'meta_key' => 'sort_order',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
	$post_query = null;
	$post_query = new WP_Query($args);	
	$post_meta_info = $post_query;
	$return_arr = array();
	if($post_meta_info){
		while ($post_meta_info->have_posts()) : $post_meta_info->the_post();
			if(get_post_meta($post->ID,"ctype",true)){
				$options = explode(',',get_post_meta($post->ID,"option_values",true));
			}
			$custom_fields = array(
					"name"		=> get_post_meta($post->ID,"htmlvar_name",true),
					"label" 	=> $post->post_title,
					"htmlvar_name" 	=> get_post_meta($post->ID,"htmlvar_name",true),
					"default" 	=> get_post_meta($post->ID,"default_value",true),
					"type" 		=> get_post_meta($post->ID,"ctype",true),
					"desc"      =>  $post->post_content,
					"option_values" => get_post_meta($post->ID,"option_values",true),
					"is_require"  => get_post_meta($post->ID,"is_require",true),
					"is_active"  => get_post_meta($post->ID,"is_active",true),
					"show_on_listing"  => get_post_meta($post->ID,"show_on_listing",true),
					"show_on_detail"  => get_post_meta($post->ID,"show_on_detail",true),
					"validation_type"  => get_post_meta($post->ID,"validation_type",true),
					"style_class"  => get_post_meta($post->ID,"style_class",true),
					"extra_parameter"  => get_post_meta($post->ID,"extra_parameter",true),
					"show_in_email" =>get_post_meta($post->ID,"show_in_email",true),
					"heading_type" => get_post_meta($post->ID,"heading_type",true),
					);
			if($options)
			{
				$custom_fields["options"]=$options;
			}
			$return_arr[get_post_meta($post->ID,"htmlvar_name",true)] = $custom_fields;
		endwhile;
	}
	custom_fields_display_custom_category_field_plugin($return_arr,'custom_fields',$submit_post_type,$submit_post_type);
}
/* 
Name :display_custom_category_field_plugin
description : Returns category custom fields html
*/
function custom_fields_display_custom_category_field_plugin($custom_metaboxes,$session_variable,$post_type,$cpost_type='post'){
	$PostTypeObject = get_post_type_object($post_type);
	$PostTypeName = $PostTypeObject->labels->name;
	$_PostTypeName = $PostTypeObject->labels->name . ' ' . __('Information',TFM_DOMAIN);
	
	foreach($custom_metaboxes as $key=>$val) {
		$name = $val['name'];
		$site_title = $val['label'];
		$type = $val['ctype'];
		$htmlvar_name = $val['htmlvar_name'];
		$admin_desc = $val['desc'];
		$option_values = $val['option_values'];
		$default_value = $val['default'];
		$style_class = $val['style_class'];
		$extra_parameter = $val['extra_parameter'];
		if(!$extra_parameter){ $extra_parameter ='';}
		/* Is required CHECK BOF */
		$is_required = '';
		$input_type = '';
		if($val['is_require'] == '1'){
			$is_required = '<span class="required">*</span>';
			$is_required_msg = '<span id="'.$name.'_error" class="message_error2"></span>';
		} else {
			$is_required = '';
			$is_required_msg = '';
		}
		/* Is required CHECK EOF */
		if(@$_REQUEST['pid'])
		{
			$post_info = get_post($_REQUEST['pid']);
			if($name == 'post_title') {
				$value = $post_info->post_title;
			}else {
				$value = get_post_meta($_REQUEST['pid'], $name,true);
			}
			
		}else if(@$_SESSION[$session_variable] && @$_REQUEST['backandedit'])
		{
			$value = @$_SESSION[$session_variable][$name];
		}else{
			$value='';
		}
	   
	if($type=='post_categories')
	{ /* fetch categories on action */
		$site_title=str_replace('Post Category',ucfirst($PostTypeName).' Category',$site_title);?>
	<div class="form_row clearfix">
	  
			<label><?php echo $site_title.$is_required; ?></label>
             <div class="category_label"><?php require_once (FIELDS_MONETIZATION_PLUGIN_DIR.'shortcodes/category.php');?></div>
			 <?php echo $is_required_msg;?>
             <?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div>
						<?php endif;?>
     </div>    
    <?php }
	}
}
/*
Name: show_character_count_custom_field
desc : validation for character limit.
*/
add_action('show_character_count','show_character_count_custom_field',10,3);
function show_character_count_custom_field($name,$type,$pkg_id)
{
	if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && ((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') ))
	{
		$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
		if(get_post_meta($package_select, $name.'_character_limit',true))
		{
			$package_field_name = get_post_meta($package_select, $name.'_character_limit',true);
		}
	}else
	{
		$package_field_name = get_post_meta($pkg_id,$name.'_character_limit',true);
	}
	if($package_field_name )
	{
	?>
		<script>
			var maxLen_<?php echo $name;?> = '<?php  echo $package_field_name; ?>';
			jQuery(document).ready( function(evt){
					jQuery('#<?php echo $name;?>').attr('maxlength',maxLen_<?php echo $name;?>);
					jQuery('#charsLeft_<?php echo $name;?>').html((maxLen_<?php echo $name;?> - jQuery('#<?php echo $name;?>').val().length) + '<?php _e(' characters left',TFM_DOMAIN); ?>');
					return true;
			});
			jQuery('#<?php echo $name;?>').keyup( function(evt){
				
					jQuery('#charsLeft_<?php echo $name;?>').html((maxLen_<?php echo $name;?> - jQuery('#<?php echo $name;?>').val().length) + '<?php _e(' characters left',TFM_DOMAIN); ?>');
					return true;
			});
		</script>
	<?php
	}
}
/* 
Name :display_custom_post_field_plugin
description : Returns all custom fields html
*/
function monetize_display_custom_post_field_plugin($custom_metaboxes,$session_variable,$post_type){
	$tmpdata = get_option('templatic_settings');
	
	if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
	{
		global $sitepress, $wpdb, $wp_post_types, $wp_taxonomies,$sitepress;
		$custom_fields['custom_fields']='custom_fields';
		$wp_post_types = array_merge($wp_post_types,$custom_fields);
	}
	foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
	  {
		 $i = 0;
		$activ = fetch_active_heading($heading);
		if($activ):
			$PostTypeObject = get_post_type_object($post_type);
			$PostTypeName = $PostTypeObject->labels->name;
			$_PostTypeName = $PostTypeObject->labels->name . ' ' . __('Information',TFM_DOMAIN);
			if($heading == '[#taxonomy_name#]' && $_custom_metaboxes)
			{
				
				if(function_exists('icl_register_string')){
					icl_register_string(TFM_DOMAIN,$_PostTypeName,$_PostTypeName);
				}
				if(function_exists('icl_t')){
					$PostTypeName1 = icl_t(TFM_DOMAIN,$_PostTypeName,$_PostTypeName);
				}else{
					$PostTypeName1 = __($_PostTypeName,TFM_DOMAIN); 
				}
			?>	
            	<div class="sec_title"><h3><?php echo ucfirst($PostTypeName1); ?></h3></div>
			<?php
            }
		endif;	
		
		foreach($_custom_metaboxes as $key=>$val) {
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['ctype'];
			$htmlvar_name = $val['htmlvar_name'];			
			if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
			{
				$post_id = icl_object_id($val['id'], 'custom_fields', true,$sitepress->get_default_language());
			}
			else
			{
				$post_id = $val['id'];
			}
			//set the post category , post title, post content, post image and post expert replace as per post type
			if($htmlvar_name=="category")
			{
				$site_title=str_replace('Post Category',ucfirst($PostTypeName).' Category',$site_title);
			}
			if($htmlvar_name=="post_title")
			{
				$site_title=str_replace('Post Title',ucfirst($PostTypeName).' Title',$site_title);
			}
			if($htmlvar_name=="post_content")
			{
				$site_title=str_replace('Post Content',ucfirst($PostTypeName).' Content',$site_title);
			}
			if($htmlvar_name=="post_excerpt")
			{
				$site_title=str_replace('Post Excerpt',ucfirst($PostTypeName).' Excerpt',$site_title);
			}
			if($htmlvar_name=="post_images")
			{
				$site_title=str_replace('Post Images',ucfirst($PostTypeName).' Images',$site_title);
			}
			//finish post type wise replace post category, post title, post content, post expert, post images
			$admin_desc = $val['desc'];
			$option_values = $val['option_values'];
			$default_value = $val['default'];
			$style_class = $val['style_class'];
			$extra_parameter = $val['extra_parameter'];
			if(!$extra_parameter){ $extra_parameter ='';}
			/* Is required CHECK BOF */
			$is_required = '';
			$input_type = '';
			if($val['validation_type'] != ''){
				if($val['is_require'] == '1'){
				$is_required = '<span class="required">*</span>';
				}
				
				$is_required_msg = '<span id="'.$name.'_error" class="message_error2"></span>';
			} else {
				$is_required = '';
				$is_required_msg = '';
			}
			/* Is required CHECK EOF */
			$value = "";
			if(@$_REQUEST['pid'])
			{
				$post_info = get_post($_REQUEST['pid']);
				if($name == 'post_title') {
					$value = $post_info->post_title;
				}
				elseif($name == 'post_content') {
					$value = $post_info->post_content;
				}
				elseif($name == 'post_excerpt'){
					$value = $post_info->post_excerpt;
				}
				else {
					$value = get_post_meta($_REQUEST['pid'], $name,true);
				}
			
			}
			if(@$_SESSION[$session_variable] && @$_REQUEST['backandedit'])
			{
				$value = @$_SESSION[$session_variable][$name];
			}
			if(isset($_SESSION['package_select']) && $_SESSION['package_select'] != '')
			{
				if(is_array(get_post_meta($_SESSION['package_select'],'custom_fields',true)) && !in_array($post_id,get_post_meta($_SESSION['package_select'],'custom_fields',true)) && $name != 'post_title' && $name != 'post_content' && $name != 'post_excerpt' && $name != 'post_images')
				{
					continue;
				}
				
			}
			if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != '')
			{
				$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
				if(!in_array($post_id,get_post_meta($package_select,'custom_fields',true)) && $name != 'post_title' && $name != 'post_content' && $name != 'post_excerpt' && $name != 'post_images')
				{
					continue;
				}
	
			}
			if($_custom_metaboxes && $i == 0 && $heading !='[#taxonomy_name#]' ){
					echo "<div class='sec_title'><h3>".$heading."</h3>";
					if($_custom_metaboxes['basic_inf']['desc']!=""){echo '<p>'.$_custom_metaboxes['basic_inf']['desc'].'</p>';}
					echo "</div>";
					$i++;
				}
			$value = apply_filters('SelectBoxSelectedOptions',$value,$name);
			if(get_post_meta($_SESSION['package_select'],$name.'_character_limit',true))
			{
				$maxlength = 'maxlength='.get_post_meta($_SESSION['package_select'],$name.'_character_limit',true);
			}
		?>
		<div class="form_row clearfix <?php echo $style_class;?>">
		   <?php if($type=='text'){?>
		   <label><?php echo $site_title.$is_required; ?></label>
		   <?php if($name == 'geo_latitude' || $name == 'geo_longitude') {
				$extra_script = 'onblur="changeMap();"';
				
			} else {
				$extra_script = '';
				
			}
			
			
			?>
             <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
			 <input name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php if(isset($value)){ echo stripslashes($value); } else { echo @$val['default']; } ?>" type="text" class="textfield <?php echo $style_class;?>" <?php echo $extra_parameter; ?> <?php echo $maxlength; ?> <?php echo $extra_script;?> />
			 <?php
				$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
				if(get_post_meta($_SESSION['package_select'],$name.'_character_limit',true) ||  get_post_meta($package_select, $name.'_character_limit', true))
				{ ?>
					<span id="charsLeft_<?php echo $name;?>" ><?php 
						if( @$_SESSION['package_select'])
						{
							echo get_post_meta($_SESSION['package_select'],$name.'_character_limit',true);
						}
						else
						{
							echo get_post_meta($package_select,$name.'_character_limit',true);
						}	
						?></span>
			  <?php } ?>
              <?php echo $is_required_msg;?>
			  
			 	<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
             <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}else if($type=='oembed_video'){ ?>
				<label><?php echo $site_title.$is_required; ?></label>				
				<?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
				<input name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php if(isset($value) && $value!=''){ echo stripslashes($value); } ?>" type="text" class="textfield <?php echo $style_class;?>" <?php echo $extra_parameter; ?> <?php echo $extra_script;?> placeholder="<?php echo @$val['default']; ?>"/>
				<?php echo $is_required_msg;
				if($admin_desc!=""):?>
					<div class="description"><?php echo $admin_desc; ?></div>
				<?php endif;
				do_action('tmpl_custom_fields_'.$name.'_after');
			}elseif($type=='date'){
				//jquery data picker			
			?>     
				<script type="text/javascript">
					jQuery(function(){
						var pickerOpts = {						
							showOn: "both",
							dateFormat: 'yy-mm-dd',
							//buttonImage: "<?php echo TEMPL_PLUGIN_URL;?>css/datepicker/images/cal.png",
							buttonText: '<i class="fa fa-calendar"></i>',
							buttonImageOnly: false,
							onChangeMonthYear: function(year, month, inst) {
							  	jQuery("#<?php echo $name;?>").blur();
						     },
						     onSelect: function(dateText, inst) {
							   //jQuery("#<?php echo $name;?>").focusin();
							     jQuery("#<?php echo $name;?>").blur();
						     }
						};	
						jQuery("#<?php echo $name;?>").datepicker(pickerOpts);
					});
				</script>
				<label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
				<input type="text" name="<?php echo $name;?>" id="<?php echo $name;?>" class="textfield <?php echo $style_class;?>" value="<?php echo esc_attr(stripslashes($value)); ?>" size="25" <?php echo 	$extra_parameter;?> />
				 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>	          
			<?php
			}
			elseif($type=='multicheckbox')
			{ ?>
			 <label><?php echo $site_title.$is_required; ?></label>
			<?php
				$options = $val['option_values'];				
				$option_titles = $val['option_title'];				
				if(strstr($value,','))
				{							
					update_post_meta($_REQUEST['pid'],$htmlvar_name,explode(',',$value));
					$value=get_post_meta($_REQUEST['pid'],$htmlvar_name,true);
				}
				if(!isset($_REQUEST['pid']) && !isset($_REQUEST['backandedit']))
				{
					$default_value = explode(",",$val['default']);
				}
	
				if($options)
				{  
					$chkcounter = 0;
					echo '<div class="form_cat_left">';
					do_action('tmpl_custom_fields_'.$name.'_before');
					$option_values_arr = explode(',',$options);
					$option_titles_arr = explode(',',$option_titles);
					for($i=0;$i<count($option_values_arr);$i++)
					{
						$chkcounter++;
						$seled='';
						if(isset($_REQUEST['pid']) || isset($_REQUEST['backandedit']))
						  {
							$default_value = $value;
						  }
						if($default_value !=''){
						if(in_array($option_values_arr[$i],$default_value)){ 
						$seled='checked="checked"';} }	
						$option_titles_arr[$i] = (!empty($option_titles_arr[$i])) ? $option_titles_arr[$i] : $option_values_arr[$i];
						echo '
						<div class="form_cat">
							<label>
								<input name="'.$key.'[]"  id="'.$key.'_'.$chkcounter.'" type="checkbox" value="'.$option_values_arr[$i].'" '.$seled.'  '.$extra_parameter.' /> '.$option_titles_arr[$i].'
							</label>
						</div>';
					}
					echo '</div>';
					?>
                     <?php echo $is_required_msg;?>
					<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
					<?php
					do_action('tmpl_custom_fields_'.$name.'_after');
				}
			}		
			elseif($type=='texteditor'){	?>
				<label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
				<?php
					$media_pro = false;
					include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
					if(is_plugin_active("Media-Pro/media-pro.php") && $htmlvar_name=="post_content")
					{
						$media_pro = true;
					}
					// default settings
					$settings =   array(
						'wpautop' => true, // use wpautop?
						'media_buttons' => $media_pro, // show insert/upload button(s)
						'textarea_name' => $name, // set the textarea name to something different, square brackets [] can be used here
						'textarea_rows' => '10', // rows="..."
						'tabindex' => '',
						'maxlemgth' => get_post_meta($_SESSION['package_select'],$name.'_character_limit',true),
						'editor_css' => '<style>.wp-editor-wrap{width:640px;margin-left:0px;}</style>', // intended for extra styles for both visual and HTML editors buttons, needs to include the <style> tags, can use "scoped".
						'editor_class' => '', // add extra class(es) to the editor textarea
						'teeny' => false, // output the minimal editor config used in Press This
						'dfw' => false, // replace the default full screen with DFW (supported on the front-end in WordPress 3.4)
						'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
						'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
					);				
					if(isset($value) && $value != '') 
					{  $content=$value; }
					else{$content= $val['default']; } 				
					wp_editor( stripslashes($content), $name, $settings);
					
					$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
					if(get_post_meta($_SESSION['package_select'],$name.'_character_limit',true) ||  get_post_meta($package_select, $name.'_character_limit', true))
					{ ?>
						<span id="charNum"></span>
						<span id="charsLeft_<?php echo $name;?>" ><?php 
						if( @$_SESSION['package_select'])
						{
							echo get_post_meta($_SESSION['package_select'],$name.'_character_limit',true);
						}
						else
						{
							echo get_post_meta($package_select,$name.'_character_limit',true);
						}
						?></span>
				  <?php }
				?>
                <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}elseif($type=='textarea'){ 
			?>
                <label><?php echo $site_title.$is_required; ?></label>
                <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
                <textarea name="<?php echo $name;?>" id="<?php echo $name;?>" class="<?php if($style_class != '') { echo $style_class;}?> textarea" <?php echo $extra_parameter;?> <?php echo $maxlength; ?>><?php if(isset($value))echo stripslashes($value);?></textarea>
               	 <?php echo $is_required_msg;?>
                <?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
				<?php
				$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
				if(get_post_meta($_SESSION['package_select'],$name.'_character_limit',true) ||  get_post_meta($package_select, $name.'_character_limit', true))
				{ ?>
					<span id="charsLeft_<?php echo $name;?>" ><?php 
						if( @$_SESSION['package_select'])
						{
							echo get_post_meta($_SESSION['package_select'],$name.'_character_limit',true);
						}
						else
						{
							echo get_post_meta($package_select,$name.'_character_limit',true);
						} ?></span>
			  <?php } ?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}elseif($type=='radio'){
			?>
			<?php if($name != 'position_filled' || @$_REQUEST['pid']): ?>
			 <label class="r_lbl"><?php echo $site_title.$is_required; ?></label>
            <?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
			<?php
				$options = $val['option_values'];
				$option_title = $val['option_title'];
				if($options)
				{  $chkcounter = 0;
					echo '<div class="form_cat_left">';
					$option_values_arr = explode(',',$options);
					$option_titles_arr = explode(',',$option_title);
			
					if($option_title ==''){  $option_titles_arr = $option_values_arr;  }
					echo '<div class="form_cat_left">';
					echo '<ul class="hr_input_radio">';
					for($i=0;$i<count($option_values_arr);$i++)
					{
						$chkcounter++;
						$seled='';
						
						if($default_value == $option_values_arr[$i]){ $seled='checked="checked"';}
						if (isset($value) && trim($value) == trim($option_values_arr[$i])){ $seled='checked="checked"';}
						$event_type = array("Regular event", "Recurring event");
						
						if($key == 'event_type'):
							if (trim(@$value) == trim($event_type[$i])){ $seled="checked=checked";}
							echo '<li>
									<label class="r_lbl">
										<input name="'.$key.'"  id="'.$key.'_'.$chkcounter.'" type="radio" value="'.$event_type[$i].'" '.$seled.'  '.$extra_parameter.' /> '.$option_titles_arr[$i].'
									</label>
								</li>';
						else:
							echo '<li>
									<label class="r_lbl">
										<input name="'.$key.'"  id="'.$key.'_'.$chkcounter.'" type="radio" value="'.$option_values_arr[$i].'" '.$seled.'  '.$extra_parameter.' /> '.$option_titles_arr[$i].'
									</label>
								</li>';
						endif;
					}
					echo '</ul></div></div>';
				}
				?>
                 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
				<?php
			 do_action('tmpl_custom_fields_'.$name.'_after');
			 endif;	
			}elseif($type=='select'){
			?>
			 <label><?php echo $site_title.$is_required; ?></label>
				<?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
                <select name="<?php echo $name;?>" id="<?php echo $name;?>" class="textfield textfield_x <?php echo $style_class;?>" <?php echo $extra_parameter;?>>
				<option value=""><?php _e("Please Select",TFM_DOMAIN);?></option>
				<?php if($option_values){
				//$option_values_arr = explode(',',$option_values);
				$option_title = ($val['option_title']) ? $val['option_title'] : $val['option_values'];
				$option_values_arr = apply_filters('SelectBoxOptions',explode(',',$option_values),$name);
				$option_title_arr = apply_filters('SelectBoxTitles',explode(',',$option_title),$name);
				for($i=0;$i<count($option_values_arr);$i++)
				{
				?>
				<option value="<?php echo $option_values_arr[$i]; ?>" <?php if($value==$option_values_arr[$i]){ echo 'selected="selected"';} else if($default_value==$option_values_arr[$i]){ echo 'selected="selected"';}?>><?php echo $option_title_arr[$i]; ?></option>
				<?php	
				}
				?>
				<?php }?>
			   
				</select>
                 <?php echo $is_required_msg;?>
				<?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
                <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
			<?php
			}
			elseif(!isset($_REQUEST['action']) && $type=='post_categories' && $tmpdata['templatic-category_custom_fields'] == 'No')
				{
				/* fetch categories on action */ ?>
				<div class="form_row clearfix">
				  
						<label><?php echo $site_title.$is_required; ?></label>
						 <div class="category_label"><?php require_once (TEMPL_MONETIZE_FOLDER_PATH.'templatic-custom_fields/category.php');?></div>
						 <?php echo $is_required_msg;?>
						 <?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div>
						<?php endif;?>			
				 </div>    
				<?php }
			else if($type=='upload'){ ?>
			<label><?php echo $site_title.$is_required; ?></label>
				<?php do_action('tmpl_custom_fields_'.$name.'_before'); ?>
                    
                    <input type="file" value="<?php echo $_SESSION['upload_file']; ?>" name="<?php echo $name; ?>" class="fileupload" id="<?php echo $name; ?>" />
                    
                    <?php do_action('tmpl_custom_fields_'.$name.'_after'); ?>
                    
                    <?php if($_REQUEST['pid']): ?>
                    	<p class="resumback"><a href="<?php echo get_post_meta($_REQUEST['pid'],$name, $single = true); ?>"><?php echo basename(get_post_meta($_REQUEST['pid'],$name, $single = true)); ?></a></p>
                    <?php elseif($_SESSION['upload_file'] && @$_REQUEST['backandedit']): 
					 $upload_file=strtolower(substr(strrchr($_SESSION['upload_file'][$name],'.'),1));					 
					 if($upload_file=='jpg' || $upload_file=='jpeg' || $upload_file=='gif' || $upload_file=='png' || $upload_file=='jpg' ):
				?>
                    	<p class="resumback"><img src="<?php echo $_SESSION['upload_file'][$name]; ?>" /></p>
                    	<?php else:?>
                    	<p class="resumback"><a href="<?php echo $_SESSION['upload_file'][$name]; ?>"><?php echo basename($_SESSION['upload_file'][$name]); ?></a></p>
                         <?php endif;?>
                    <?php endif; ?>
                    
                    <?php echo $is_required_msg;?>
                    <?php if($admin_desc!=""):?><div class="description"><?php echo $admin_desc; ?></div><?php endif;?>
			<?php 
			}else{ //
				do_action('tevolution_custom_fieldtype',$key,$val,$post_type);
			}
			
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			
			if($type == 'image_uploader' && !is_plugin_active("Media-Pro/media-pro.php")) {
					$package_select = '';
					if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != '')
					{
						$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
					}
					if((isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && ((isset($_REQUEST['action']) && $_REQUEST['action']=='edit')) && get_post_meta($package_select, 'max_image', true) >= 1) ||  get_post_meta($_SESSION['package_select'],'max_image',true) >= 1)
						{?>
						<label><?php echo $site_title.$is_required ?></label>
					
                    <?php include (FIELDS_MONETIZATION_PLUGIN_DIR."shortcodes/image_uploader.php");
						if(isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && ((isset($_REQUEST['action']) && $_REQUEST['action']=='edit')))
						{
							$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
							if(get_post_meta($package_select, 'max_image', true))
							{
								$no_image_upload = get_post_meta($package_select,'max_image',true);
								if($no_image_upload == 1)
									$image_msg = __(' image',TFM_DOMAIN);
								else
									$image_msg = __(' images',TFM_DOMAIN);
								echo '<span class="message_note">'.__('You can upload maximum ',TFM_DOMAIN).$no_image_upload.$image_msg.'</span>';
							}
						}
						if(get_post_meta($_SESSION['package_select'],'max_image',true) )
						{
							$no_image_upload = get_post_meta($_SESSION['package_select'],'max_image',true);
							if($no_image_upload == 1)
								$image_msg = __(' image',TFM_DOMAIN);
							else
								$image_msg = __(' images',TFM_DOMAIN);
							echo '<span class="message_note">'.__('You can upload maximum ',TFM_DOMAIN).$no_image_upload.$image_msg.'</span>';
						}?>
						
                    <span class="message_note"><?php echo $admin_desc;?></span>
                    <span class="message_error2" id="post_images_error"></span>
			<?php 	}
				}?> 
               
			<?php if($type=='geo_map') { ?>
				<?php include_once(TEMPL_MONETIZE_FOLDER_PATH."templatic-custom_fields/location_add_map.php"); ?>
                    
                    <?php if($admin_desc):?>
                    	<span class="message_note"><?php echo $admin_desc;?></span>
                    <?php else:?>
                    	<span class="message_note"><?php echo $GET_MAP_MSG;?></span>
                    <?php endif; ?>
			<?php } ?>
			
			<?php 
					do_action('show_new_custom_field',$type,$site_title,$is_required);
				?>
               
			<div class="clearfix"></div>
			</div>    
		<?php
		}
	}
	if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
	{
		global $sitepress, $wpdb, $wp_post_types, $wp_taxonomies,$sitepress;
		foreach( $wp_post_types as $k=>$val ) {
			if ( $k == 'custom_fields' ) {
				unset( $wp_post_types[$k] );
			}
		}
	}
}
add_action('tmpl_textarea_cutom_fields_settings','tmpl_text_cutom_fields_settings',10,5);
add_action('tmpl_text_cutom_fields_settings','tmpl_text_cutom_fields_settings',10,5);
function tmpl_text_cutom_fields_settings($custom_metaboxes,$session_variable,$post_type,$pkg_id,$name){
	$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
	$result = '';
	if(get_post_meta($pkg_id,$name.'_character_limit',true))
	{
		$maxlength = 'maxlength='.get_post_meta($pkg_id,$name.'_character_limit',true);
	}
	if(get_post_meta($pkg_id,$name.'_character_limit',true) ||  get_post_meta($package_select, $name.'_character_limit', true))
	{ 
		$result .= '<span id="charsLeft_'.$name.'" >';
			if( @$pkg_id)
			{
				$result .= get_post_meta($pkg_id,$name.'_character_limit',true);
			}
			else
			{
				$result .= get_post_meta($package_select,$name.'_character_limit',true);
			}	
		$result .= '</span>';
   } 
   $_SESSION['package_select'] = $pkg_id;
   echo $result;
}

/*
* validation for custom fields of character limit.
*/
add_action('tmpl_cutom_fields_settings','charCountLimitValidation',10,5);
function charCountLimitValidation($custom_metaboxes,$session_variable,$post_type,$pkg_id,$name){
	$tmpdata = get_option('templatic_settings');
	foreach($custom_metaboxes as $heading=>$_custom_metaboxes)
	{		 
		foreach($_custom_metaboxes as $key=>$val) {
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['ctype'];
			$htmlvar_name = $val['htmlvar_name'];			
			$post_id = $val['id'];
			
			//finish post type wise replace post category, post title, post content, post expert, post images
			$admin_desc = $val['desc'];
			$option_values = $val['option_values'];
			$default_value = $val['default'];
			$style_class = $val['style_class'];
			$extra_parameter = $val['extra_parameter'];
			if(!$extra_parameter){ $extra_parameter ='';}
			/* Is required CHECK BOF */
			$is_required = '';
			$input_type = '';
			
			/* Is required CHECK EOF */
			$value = "";
		
			if($type=='texteditor'){
				
				if(get_post_meta($pkg_id,$name.'_character_limit',true))
				{
					
				}
			}if($type=='text'){
				
				do_action('show_character_count',$name,$type,$pkg_id);
				
			}if($type=='texteditor'){
				
				do_action('show_character_count',$name,$type,$pkg_id);
			}
			if($type=='textarea'){
				
				do_action('show_character_count',$name,$type,$pkg_id);
			}
		}
	}
}
/*
* validation for character limit for tinymce visual editor.
*/
add_action('tmpl_texteditor_cutom_fields_settings','monetize_submit_form');
function monetize_submit_form()
{
	add_filter( 'tiny_mce_before_init', 'character_limit_tiny_mce_before_init',100,2 );
}
function character_limit_tiny_mce_before_init( $initArray ,$editor_id)
{
	wp_reset_query();
	wp_reset_postdata();
	global $limit_custom_fields,$all_cat_id,$post;
	$post_id = $_REQUEST['submit_page_id'];
	$post_type =get_post_meta($post_id,'submit_post_type',true);
	$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type,'public'   => true, '_builtin' => true ));
	$taxonomy = $taxonomies[0];
	$custom_metaboxes1 = array();
	/* Fetch Heading type custom fields */
	$heading_type = fetch_heading_per_post_type($post_type);
	if(count($heading_type) > 0)
	{
		foreach($heading_type as $_heading_type){
			$custom_metaboxes1[$_heading_type] = get_post_custom_fields_templ_plugin($post_type,$all_cat_id,$taxonomy,$_heading_type);//custom fields for custom post type..
		}
	}else{
		$custom_metaboxes1[] = get_post_custom_fields_templ_plugin($post_type,$all_cat_id,$taxonomy,'');//custom fields for custom post type..
	}

	foreach($custom_metaboxes1 as $heading=>$_custom_metaboxes)
	{		 
		foreach($_custom_metaboxes as $key=>$val) {
			$name = $val['name'];
			$site_title = $val['label'];
			$type = $val['ctype'];
			$htmlvar_name = $val['htmlvar_name'];			
			$post_id = $val['id'];
			
			//finish post type wise replace post category, post title, post content, post expert, post images
			$admin_desc = $val['desc'];
			$option_values = $val['option_values'];
			$default_value = $val['default'];
			$style_class = $val['style_class'];
			$extra_parameter = $val['extra_parameter'];
			if(!$extra_parameter){ $extra_parameter ='';}
			/* Is required CHECK BOF */
			$is_required = '';
			$input_type = '';
			
			/* Is required CHECK EOF */
			$value = "";
		
			if($type=='texteditor'){
				
				if(get_post_meta($_SESSION['package_select'],$name.'_character_limit',true) || (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && ((isset($_REQUEST['action']) && $_REQUEST['action']=='edit') )) )
				{
					$package_select = get_post_meta($_REQUEST['pid'], 'package_select', true);
					if(get_post_meta($package_select, $name.'_character_limit', true))
					{?>
						<script>
							var content_id = '<?php echo $name; ?>';
							var maxlength_content_id = "<?php echo get_post_meta($package_select,$name.'_character_limit',true); ?>";
						</script>
						<?php
						}
						else
						{
						?>
						<script>
							var content_id = '<?php echo $name; ?>';
							var maxlength_content_id = "<?php echo get_post_meta($_SESSION['package_select'],$name.'_character_limit',true); ?>";
						</script>
				<?php
						}
				 $initArray['setup'] = <<<JS
[function(ed) { var content_limit = '';
    ed.onKeyUp.add(function(ed, e) {
        if(tinyMCE.activeEditor.id != '') {
			var maxlength_content_id = jQuery('#'+tinyMCE.activeEditor.id).attr('maxlength');
            var content = tinyMCE.get(tinyMCE.activeEditor.id).getContent().replace(/<[^>]+>/g, "");
            var max = maxlength_content_id;
            var len = content.length;
            if (len > max) {
				 var charCount = 0;
				tinyMCE.get(tinyMCE.activeEditor.id).setContent(content_limit);
			  jQuery('#charsLeft_'+tinyMCE.activeEditor.id).html(charCount + ' characters left');
            } else {
			 content_limit = tinyMCE.get(tinyMCE.activeEditor.id).getContent().replace(/<[^>]+>/g, "");
             var charCount = max - len;
             jQuery('#charsLeft_'+tinyMCE.activeEditor.id).html(charCount + ' characters left');
            }
         }
    });
}][0]
JS;
    
				}
			}
		}
	}
	return $initArray;
}
/*
Name: templ_field_monetization_fetch_registration_onsubmit
Description : Show login box on registration page.
*/
function templ_field_monetization_fetch_registration_onsubmit()
{
	if($_SESSION['custom_fields']['login_type'])
	{
		$user_login_or_not = $_SESSION['custom_fields']['login_type'];
	}
	if((isset($_SESSION['user_email']) && $_SESSION['user_email']!='') || (isset($_REQUEST['backandedit']) && $_REQUEST['backandedit'] == 1))
	{
		$user_login_or_not = 'new_user';
	}
  ?>
	<div id="login_user_meta" <?php if($user_login_or_not=='new_user'){ echo 'style="display:block;"';}else{ echo 'style="display:none;"'; }?> >
	 <input type="hidden" name="user_email_already_exist" id="user_email_already_exist" value="<?php if($_SESSION['custom_fields']['user_email_already_exist']) { echo "1"; } ?>" />
	   <input type="hidden" name="user_fname_already_exist" id="user_fname_already_exist" value="<?php if($_SESSION['custom_fields']['user_fname_already_exist']) { echo "1"; } ?>" />
	   <input type="hidden" name="login_type" id="login_type" value="<?php echo $_SESSION['custom_fields']['login_type']; ?>" />
	   <input type="hidden" name="reg_redirect_link" value="<?php echo apply_filters('tevolution_register_redirect_to',@$_SERVER['HTTP_REFERER']);?>" />
	    <?php
			$user_meta_array = user_fields_array();
			display_usermeta_fields($user_meta_array);/* fetch registration form */
			include_once(TT_REGISTRATION_FOLDER_PATH . 'registration_validation.php');
		?>
	</div>
<?php
}
/*
 * add action for add calender css and javascript file inside html head tag
 */ 
add_action ('wp_head', 'add_remove_header_js');
function add_remove_header_js()
{
	global $post;
	$is_submit=get_post_meta( @$post->ID,'is_tevolution_submit_form',true);
	if($is_submit==1)
	{
		remove_action ('wp_head', 'header_css_javascript');
		add_action ('wp_head', 'monetize_header_css_javascript',20);
	}
}
/*
Name: monetize_add_userrole_option_js
Description : hide and show registration box on submit form page.
*/
function monetize_add_userrole_option_js(){ ?>
	<script type="text/javascript">
	function set_login_registration_frm(val)
	{
		if(val=='existing_user')
		{
			document.getElementById('login_user_meta').style.display = 'none';
			document.getElementById('login_user_frm_id').style.display = '';
			document.getElementById('login_type').value = val;
			document.getElementById('monetize_preview').style.display = 'none';
		}else  //new_user
		{
			document.getElementById('login_user_meta').style.display = 'block';
			document.getElementById('login_user_frm_id').style.display = 'none';
			document.getElementById('login_type').value = val;
			document.getElementById('monetize_preview').style.display = 'block';
		}
	}
	</script>
<?php }
/*
 * Function Name:monetize_header_css_javascript
 * Front side add css and javascript file in side html head tag 
 */
 
function monetize_header_css_javascript()  {  
	
	global $current_user, $wp_locale,$post;
	if($current_user->ID == '')
	{ 
		if(function_exists('monetize_add_userrole_option_js'))
		{
			monetize_add_userrole_option_js();
		}
	}	
	
	
	$is_submit=get_post_meta( @$post->ID,'is_tevolution_submit_form',true);
	//wp_enqueue_script('jquery_ui_core',TEMPL_PLUGIN_URL.'js/jquery.ui.core.js');	
	$register_page_id=get_option('tevolution_register');
	$login_page_id=get_option('tevolution_login');
	$profile_page_id=get_option('tevolution_profile');
	
	if(is_admin() || ($is_submit==1 || $register_page_id==$post->ID || $login_page_id==$post->ID || $profile_page_id==$post->ID)){
		wp_enqueue_script('jquery-ui-datepicker');	
		 //localize our js
		$aryArgs = array(
			'monthNames'        => strip_array_indices( $wp_locale->month ),
			'monthNamesShort'   => strip_array_indices( $wp_locale->month_abbrev ),
			'monthStatus'       => __( 'Show a different month', TFM_DOMAIN ),
			'dayNames'          => strip_array_indices( $wp_locale->weekday ),
			'dayNamesShort'     => strip_array_indices( $wp_locale->weekday_abbrev ),
			'dayNamesMin'       => strip_array_indices( $wp_locale->weekday_initial ),
			// is Right to left language? default is false
			'isRTL'             => @$wp_locale->is_rtl,
		);
	 
		// Pass the array to the enqueued JS
		wp_localize_script( 'jquery-ui-datepicker', 'objectL10n', $aryArgs );
	
	}
	
}
if(!function_exists('monetize_fetch_package_feature_details'))
{
	function monetize_fetch_package_feature_details($edit_id='',$png_id='',$all_cat_id)
	{
				
		/* set feature price when Go back and edit */
		
		if(isset($edit_id) && $edit_id !=''){
			$price_select =  get_post_meta($edit_id,'package_select',true); /* selected package */
			$is_featured = get_post_meta($price_select,'is_featured',true); // package is featured or not 
			if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
				$author_can_moderate_comment = get_post_meta($pkg_id,'can_author_mederate',true);
			}
			if($is_featured ==1){
				$featured_h = get_post_meta($price_select,'feature_amount',true); //
				$featured_c = get_post_meta($price_select,'feature_cat_amount',true); //
			}
		}elseif(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit'] !='' && isset($_SESSION['custom_fields']['package_select'])){
			$pkg_id = $_SESSION['custom_fields']['package_select'];
			$is_featured = get_post_meta($pkg_id,'is_featured',true);
			if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
				$author_can_moderate_comment = get_post_meta($pkg_id,'can_author_mederate',true);
			}
			if($is_featured ==1){
				
				$featured_h = get_post_meta($pkg_id,'feature_amount',true); //
				$featured_c = get_post_meta($pkg_id,'feature_cat_amount',true); //
				if(!$featured_h){ $featured_h =0; }
				if(!$featured_c){ $featured_c =0; }
			}	
		}
		elseif(isset($_REQUEST['backandedit']) && $_REQUEST['backandedit'] !=''){
			$pkg_id = $_SESSION['package_select'];
			$is_featured = get_post_meta($pkg_id,'is_featured',true);
			if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
				$author_can_moderate_comment = get_post_meta($pkg_id,'can_author_mederate',true);
			}
			if($is_featured ==1){
				
				$featured_h = get_post_meta($pkg_id,'feature_amount',true); //
				$featured_c = get_post_meta($pkg_id,'feature_cat_amount',true); //
				if(!$featured_h){ $featured_h =0; }
				if(!$featured_c){ $featured_c =0; }
			}	
		}
		else{
			$featured_h =0;
			$featured_c =0;
			if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
				$author_can_moderate_comment = 0;
			}
		}	?>
			<!-- FETCH FEATURED POST PRICES IN FRONT END -->
            <?php wp_reset_query(); global $post,$current_user,$monetization; 		
			
		    $post_type = (get_post_meta($post->ID,'submit_post_type',true)!="")? get_post_meta($post->ID,'submit_post_type',true):get_post_meta($post->ID,'template_post_type',true);
			$PostTypeObject = get_post_type_object($post_type);
			$_PostTypeName = $PostTypeObject->labels->name;
			$featured_type= (isset($_SESSION['custom_fields']['featured_type']) && $_SESSION['custom_fields']['featured_type']!='')? $_SESSION['custom_fields']['featured_type']: 'none';			
			?>
			<div class="form_row clearfix" id="is_featured" <?php if($is_featured == '1'){ }else{ ?>style="display:none; } <?php }?>">
				<label><?php echo sprintf(__('Would you like to make this %s featured?',TFM_DOMAIN) ,$_PostTypeName);?> </label>
				<div class="feature_label">
				
					<label><input type="checkbox" name="featured_h" id="featured_h" value="<?php echo $featured_h; ?>" onclick="featured_list(this.id)" <?php if(@$_SESSION['custom_fields']['featured_h'] !="" || @$_SESSION['featured_h'] !=""){ echo "checked=checked"; } ?>/><?php _e('Yes &sbquo; feature this listing on homepage.',TFM_DOMAIN); ?> <span id="ftrhome"><?php if(isset($featured_h) && $featured_h !=""){ echo "(".fetch_currency_with_position($featured_h).")"; }else{ echo "(". __('Free',T_DOMAIN) .")"; } ?></span></label>
					
					<label><input type="checkbox" name="featured_c" id="featured_c" value="<?php echo $featured_c; ?>" onclick="featured_list(this.id)" <?php if(@$_SESSION['custom_fields']['featured_c'] !="" || @$_SESSION['featured_c'] !=""){ echo "checked=checked"; } ?>/><?php _e('Yes &sbquo; feature this listing on category pages.',TFM_DOMAIN); ?><span id="ftrcat"><?php if(isset($featured_c) && $featured_c !=""){ echo "(".fetch_currency_with_position($featured_c).")"; }else{ echo "(". __('Free',T_DOMAIN).")"; } ?></span></label>
					
					
					
					<input type="hidden" name="featured_type" id="featured_type" value="<?php echo $featured_type;?>"/>
					
					
				</div>
				<?php				
					$msg_note = sprintf(__("An additional amount will be charged to make this %s featured. You have the option to feature your %s on home page or category page or both.",TFM_DOMAIN),$_PostTypeName,$_PostTypeName);
					if(function_exists('icl_register_string')){
						icl_register_string(TFM_DOMAIN,$msg_note,$msg_note);
					}
					
					if(function_exists('icl_t')){
						$msg_note1 = icl_t(TFM_DOMAIN,$msg_note,$msg_note);
					}else{
						$msg_note1 = __($msg_note,TFM_DOMAIN); 
					}
				?>
				<span class="message_note"><?php _e($msg_note1,TFM_DOMAIN);?></span>
				
			</div>
			
			<div class="form_row clearfix feature_label" id="moderate_comment" <?php if($author_can_moderate_comment == 1){ }else{ ?>style="display:none; <?php }?>">
			<label><?php echo sprintf(__('Would you like to make this %s comment be moderate?',TFM_DOMAIN) ,$_PostTypeName);?> </label>
			<?php
						if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php')){
							
							if($pkg_id)
							{
								$comment_mederation_amount = get_post_meta($pkg_id,'comment_mederation_amount',true);
							}
							if(!$comment_mederation_amount){ $comment_mederation_amount =0; }
							{
							?>
								<label><input type="checkbox" name="author_can_moderate_comment" id="author_can_moderate_comment" value="<?php echo $comment_mederation_amount; ?>" onclick="featured_list(this.id)" <?php if(@$_SESSION['custom_fields']['author_can_moderate_comment'] !=""){ echo "checked=checked"; } ?>/><?php echo ' ';_e(MODERATE_COMMENT,TFM_DOMAIN); ?><span id="ftrcomnt"><?php if(isset($author_can_moderate_comment) && $author_can_moderate_comment !=""){ echo "(".fetch_currency_with_position($comment_mederation_amount).")"; }else{ echo "(".fetch_currency_with_position('0').")"; } ?></span></label>
								<input type="hidden" name="author_moderate" id="author_moderate" value="0"/>
							<?php	
							}
						}
					?>
					<?php
						$msg_note = sprintf(__("An additional amount will be charged to make this %s comment moderateable.",TFM_DOMAIN),$_PostTypeName);
						if(function_exists('icl_register_string')){
							icl_register_string(TFM_DOMAIN,$msg_note,$msg_note);
						}
						
						if(function_exists('icl_t')){
							$msg_note1 = icl_t(TFM_DOMAIN,$msg_note,$msg_note);
						}else{
							$msg_note1 = __($msg_note,TFM_DOMAIN); 
						}
					?>
					<span class="message_note"><?php echo $msg_note1;?></span>
			</div>
			<!-- END - FETCH FEATURED POST PRICE -->
			
			<!-- FETCH THE TOTAL AMOUNT TO BE PAID IN FRONT END -->
			<?php if(isset($_SESSION['total_price']) && $_SESSION['total_price'] > 0)
			{?>
			<div id="price_package_price_list" <?php if(($_SESSION['total_price'] <= 0  || $_SESSION['total_price'] == '')){?> style="display:none;" <?php } ?> class="form_row clearfix">
				<label><?php _e('Total price as per your selection.',TFM_DOMAIN);?> </label>
				<div class="form_cat"  >
				<?php if(isset($category_id)){ $catid = $category_id; }else{ $catid =''; }
					if(isset($cat_array) && $cat_array != "")
					{
						$catid = $cat_array;
					}
					else
					{
						 $catid =''; 
					} 
					if(!isset($total_price))
					{
						$total_price = 0;
					}
					/* variables to set category pricing */
					if(isset($_SESSION['custom_fields'])){ /* selection when go back and edit */
						$_SESSION['custom_fields']['featured_type'] = $_SESSION['featured_type'];
						$_SESSION['custom_fields']['featured_h'] = $_SESSION['featured_h'];
						$_SESSION['custom_fields']['featured_c'] = $_SESSION['featured_c'];
						$_SESSION['custom_fields']['total_price'] = $_SESSION['total_price'];
						$_SESSION['custom_fields']['all_cat_price']=$_SESSION['all_cat_price'];
						
						$cat_price = $_SESSION['custom_fields']['all_cat_price'];
						$price_select = $_SESSION['package_select'];
						$packprice = get_post_meta($_SESSION['package_select'],'package_amount',true);
						if(isset($_SESSION['custom_fields']['total_price'])){
							$total_price = $_SESSION['custom_fields']['total_price'];
						}
						$featured_type = $_SESSION['custom_fields']['featured_type'];
						
						if($featured_type =='both'){
							$fprice = floatval($_SESSION['custom_fields']['featured_h']) + floatval($_SESSION['custom_fields']['featured_c']) ;
						}elseif($featured_type == 'c'){
							$fprice = floatval(floatval(@$_SESSION['custom_fields']['featured_c'])) ;
						}elseif($featured_type == 'h'){
							$fprice = floatval($_SESSION['custom_fields']['featured_h']) ;
						}
						else{
							$fprice =0;
						}						
						if($_SESSION['custom_fields']['author_can_moderate_comment'] >0 && $_SESSION['custom_fields']['author_moderate'] == 1){
							$fprice += floatval($_SESSION['custom_fields']['author_can_moderate_comment']);
						}
					}
					elseif(isset($_SESSION['all_cat_price']) || isset($_SESSION['package_select'])){ /* selection when go back and edit */
						$cat_price = $_SESSION['all_cat_price'];
						$price_select = $_SESSION['package_select'];
						$packprice = get_post_meta($_SESSION['package_select'],'package_amount',true);
						if(isset($_SESSION['total_price'])){
							$total_price = $_SESSION['total_price'];
						}
						$featured_type = $_SESSION['featured_type'];
						
						if($featured_type =='both'){
							$fprice = floatval($_SESSION['featured_h']) + floatval($_SESSION['custom_fields']['featured_c']) ;
						}elseif($featured_type = 'c'){
							$fprice = floatval(floatval(@$_SESSION['featured_c'])) ;
						}elseif($featured_type = 'h'){
							$fprice = floatval($_SESSION['featured_h']) ;
						}
						else{
							$fprice =0;
						}
						if($_SESSION['custom_fields']['author_can_moderate_comment'] >0 && $_SESSION['custom_fields']['author_moderate'] == 1){
							$fprice += floatval($_SESSION['author_can_moderate_comment']);
						}
					}
					elseif(isset($_REQUEST['category'])){
						$cat_price = $monetization->templ_fetch_category_price($_REQUEST['category']);
						$total_price = $monetization->templ_fetch_category_price($_REQUEST['category']);
					}elseif(isset($edit_id) && $edit_id !='' && $_REQUEST['category'] ==''){
				
						$cat_price = $monetization->templ_total_selected_cats_price($all_cat_id);
						$packprice = get_post_meta($price_select,'package_amount',true);/* package amount */
						
						$featured_type = get_post_meta($edit_id,'featured_type',true);
						$featured_h_price = get_post_meta($price_select,'feature_amount',true);
						$featured_c_price = get_post_meta($price_select,'feature_cat_amount',true);
						$total_price = get_post_meta($edit_id,'total_price',true);
						/* featured prices when comes for edit */
						if($featured_type =='both'){
							$fprice = floatval($featured_h_price + $featured_c_price) ;
						}elseif($featured_type == 'c'){
							$fprice = floatval($featured_c_price) ;
						}elseif($featured_type == 'h'){
							$fprice = floatval($featured_h_price) ;
						}else{
							$fprice =0;
						}
						if(get_post_meta($edit_id,'author_can_moderate_comment',true)){
							$fprice += get_post_meta($edit_id,'author_can_moderate_comment',true);
						}
					}else{
						$cat_price = 0;
						$price_select = '';
						$packprice = 0;
					}
					
					$currency = get_option('currency_symbol');
					$position = get_option('currency_pos');
					global  $wpdb;
					?><span id="before_cat_price_id" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;';}?></span>
					<span id="cat_price" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>>
					<?php if($catid != "") { $catprice = $wpdb->get_row("select * from $wpdb->term_taxonomy tt,  $wpdb->terms t where tt.term_taxonomy_id = '".$catid."' and tt.term_id = t.term_id"); if($catprice->term_price !=""){ echo $catprice->term_price; }else{ echo "0"; } }else{ if($cat_price !="") { echo $cat_price; }else{ echo '0'; } } ?></span>
					<span id="cat_price_id" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?>		</span>		
					
					<span id="pakg_add" <?php if($_SESSION['all_cat_price'] <= 0  || $_SESSION['all_cat_price'] == ''){?> style="display:none;" <?php } ?>><?php echo '+';?> 	</span>	
					
					<span id="before_pkg_price_id" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;'; } ?></span>
					<span id="pkg_price" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if(isset($price_select) && $price_select !=""){ echo $packprice; } else{ echo "0";}?></span>
					<span id="pkg_price_id" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?>	</span>	
					
					<span id="pakg_price_add" <?php if($packprice <= 0 || $fprice <= 0){?> style="display:none;" <?php } ?>><?php echo '+'; ?> </span>	
					
					<span id="before_feture_price_id" <?php if($fprice <= 0){?> style="display:none;" <?php } ?> ><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;'; } ?></span>
					<span id="feture_price"  <?php if($fprice <= 0){?> style="display:none;" <?php } ?>><?php if($fprice !=""){ echo $fprice ; }else{ echo "0"; }?></span>
					<span id="feture_price_id" <?php if($fprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?></span>	
					
					<span id="cat_price_total_price" <?php if(($fprice <= 0 || $fprice == '') && ($cat_price <= 0 || $cat_price == '') || ($fprice <= 0 || $fprice == '') && ($packprice <= 0 || $packprice == '') || ($packprice <= 0 || $packprice == '') && ($cat_price <= 0 || $cat_price == '')){?> style="display:none;" <?php } ?>><?php echo "<span id='result_price_equ'>=</span>"; ?>
					<?php if($position == '1'){ echo '<span id="currency_before_result_price">'.$currency.'</span>'; }else if($position == '2'){ echo '<span id="currency_before_space_result_price">'.$currency.'&nbsp;</span>'; } ?>
					<span id="result_price"><?php if($total_price != ""){ echo $total_price; }else if($catid != ""){  echo $catprice->term_price; }else{ echo "0";} ?></span>
					<?php if($position == '3'){ echo '<span id="currency_after_result_price">'.$currency.'</span>'; }else if($position != 1 && $position != 2 && $position !=3){ echo '<span id="currency_after_space_result_price">&nbsp;'.$currency."</span>"; } ?></span></span>
					
					<input type="hidden" name="total_price" id="total_price" value="<?php if($total_price != ""){ echo $total_price; }else if($catid != ""){  echo $catprice->term_price; }else{ echo "0";} ?>"/>
				</div>
				<span class="message_note"> </span>
				
				<label><?php echo '<span class="message_note">';_e("Final amount including discount will be displayed on preview page.",TFM_DOMAIN);echo "</span>";?></label>
			<!-- END - FETCH TOTAL PRICE -->
			</div>
			<?php
			}
			else{ ?>
			<div id="price_package_price_list" <?php if(($_SESSION['custom_fields']['total_price'] <= 0  || $_SESSION['custom_fields']['total_price'] == '')){?> style="display:none;" <?php } ?> class="form_row clearfix">
				<label><?php _e('Total price as per your selection.',TFM_DOMAIN);?> </label>
				<div class="form_cat"  >
				<?php
					global $monetization;
					if(isset($category_id)){ $catid = $category_id; }else{ $catid =''; }
					if(isset($cat_array) && $cat_array != "")
					{
						$catid = $cat_array;
					}
					else
					{
						 $catid =''; 
					} 
					if(!isset($total_price))
					{
						$total_price = 0;
					}
					/* variables to set category pricing */
					if(isset($_SESSION['custom_fields'])){ /* selection when go back and edit */
						$cat_price = $_SESSION['custom_fields']['all_cat_price'];
						$price_select = $_SESSION['custom_fields']['package_select'];
						$packprice = get_post_meta($_SESSION['custom_fields']['package_select'],'package_amount',true);
						if(isset($_SESSION['custom_fields']['total_price'])){
							$total_price = $_SESSION['custom_fields']['total_price'];
						}
						$featured_type = $_SESSION['custom_fields']['featured_type'];
						
						if($featured_type =='both'){
							$fprice = floatval($_SESSION['custom_fields']['featured_h']) + floatval($_SESSION['custom_fields']['featured_c']) ;
						}elseif($featured_type = 'c'){
							$fprice = floatval(floatval(@$_SESSION['custom_fields']['featured_c'])) ;
						}elseif($featured_type = 'h'){
							$fprice = floatval($_SESSION['custom_fields']['featured_h']) ;
						}
						else{
							$fprice =0;
						}
						if($_SESSION['custom_fields']['author_can_moderate_comment'] >0 && $_SESSION['custom_fields']['author_moderate'] == 1){
							$fprice += floatval($_SESSION['custom_fields']['author_can_moderate_comment']);
						}
					}
					elseif(isset($_SESSION['all_cat_price']) || isset($_SESSION['package_select'])){ /* selection when go back and edit */
						$cat_price = $_SESSION['all_cat_price'];
						$price_select = $_SESSION['package_select'];
						$packprice = get_post_meta($_SESSION['package_select'],'package_amount',true);
						if(isset($_SESSION['total_price'])){
							$total_price = $_SESSION['total_price'];
						}
						$featured_type = $_SESSION['featured_type'];
						
						if($featured_type =='both'){
							$fprice = floatval($_SESSION['featured_h']) + floatval($_SESSION['custom_fields']['featured_c']) ;
						}elseif($featured_type = 'c'){
							$fprice = floatval(floatval(@$_SESSION['featured_c'])) ;
						}elseif($featured_type = 'h'){
							$fprice = floatval($_SESSION['featured_h']) ;
						}
						else{
							$fprice =0;
						}
						if($_SESSION['custom_fields']['author_can_moderate_comment'] >0 && $_SESSION['custom_fields']['author_moderate'] == 1){
							$fprice += floatval($_SESSION['author_can_moderate_comment']);
						}
					}
					elseif(isset($_REQUEST['category'])){
						$cat_price = $monetization->templ_fetch_category_price($_REQUEST['category']);
						$total_price = $monetization->templ_fetch_category_price($_REQUEST['category']);
					}elseif(isset($edit_id) && $edit_id !='' && $_REQUEST['category'] ==''){
				
						$cat_price = $monetization->templ_total_selected_cats_price($all_cat_id);
						$packprice = get_post_meta($price_select,'package_amount',true);/* package amount */
						
						$featured_type = get_post_meta($edit_id,'featured_type',true);
						$featured_h_price = get_post_meta($price_select,'feature_amount',true);
						$featured_c_price = get_post_meta($price_select,'feature_cat_amount',true);
						$total_price = get_post_meta($edit_id,'total_price',true);
						/* featured prices when comes for edit */
						if($featured_type =='both'){
							$fprice = floatval($featured_h_price + $featured_c_price) ;
						}elseif($featured_type = 'c'){
							$fprice = floatval($featured_c_price) ;
						}elseif($featured_type = 'h'){
							$fprice = floatval($featured_h_price) ;
						}else{
							$fprice =0;
						}
						if(get_post_meta($edit_id,'author_can_moderate_comment',true)){
							$fprice += get_post_meta($edit_id,'author_can_moderate_comment',true);
						}
					}else{
						$cat_price = 0;
						$price_select = '';
						$packprice = 0;
					}
					
					$currency = get_option('currency_symbol');
					$position = get_option('currency_pos');
					global  $wpdb;
					
					?><span id="before_cat_price_id" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;';}?></span>
					<span id="cat_price" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>>
					<?php if($catid != "") { $catprice = $wpdb->get_row("select * from $wpdb->term_taxonomy tt,  $wpdb->terms t where tt.term_taxonomy_id = '".$catid."' and tt.term_id = t.term_id"); if($catprice->term_price !=""){ echo $catprice->term_price; }else{ echo "0"; } }else{ if($cat_price !="") { echo $cat_price; }else{ echo '0'; } } ?></span>
					<span id="cat_price_id" <?php if($cat_price <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?>		</span>		
					
					<span id="pakg_add" <?php if($_SESSION['custom_fields']['all_cat_price'] <= 0  || $_SESSION['custom_fields']['all_cat_price'] == ''){?> style="display:none;" <?php } ?>><?php echo '+';?> 	</span>	
					
					<span id="before_pkg_price_id" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;'; } ?></span>
					<span id="pkg_price" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if(isset($price_select) && $price_select !=""){ echo $packprice; } else{ echo "0";}?></span>
					<span id="pkg_price_id" <?php if($packprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?>	</span>	
					
					<span id="pakg_price_add" <?php if($packprice <= 0 || $fprice <= 0){?> style="display:none;" <?php } ?>><?php echo '+'; ?> </span>	
					
					<span id="before_feture_price_id" <?php if($fprice <= 0){?> style="display:none;" <?php } ?> ><?php if($position == '1'){ echo $currency; }else if($position == '2'){ echo $currency.'&nbsp;'; } ?></span>
					<span id="feture_price"  <?php if($fprice <= 0){?> style="display:none;" <?php } ?>><?php if($fprice !=""){ echo $fprice ; }else{ echo "0"; }?></span>
					<span id="feture_price_id" <?php if($fprice <= 0){?> style="display:none;" <?php } ?>><?php if($position == '3'){ echo $currency; }else if($position != 1 && $position != 2 && $position !=3){ echo '&nbsp;'.$currency; } ?></span>	
					
					<span id="cat_price_total_price" <?php if(($fprice <= 0 || $fprice == '') && ($cat_price <= 0 || $cat_price == '') || ($fprice <= 0 || $fprice == '') && ($packprice <= 0 || $packprice == '') || ($packprice <= 0 || $packprice == '') && ($cat_price <= 0 || $cat_price == '')){?> style="display:none;" <?php } ?>><?php echo "<span id='result_price_equ'>=</span>"; ?>
					<?php if($position == '1'){ echo '<span id="currency_before_result_price">'.$currency.'</span>'; }else if($position == '2'){ echo '<span id="currency_before_space_result_price">'.$currency.'&nbsp;</span>'; } ?>
					<span id="result_price"><?php if($total_price != ""){ echo $total_price; }else if($catid != ""){  echo $catprice->term_price; }else{ echo "0";} ?></span>
					<?php if($position == '3'){ echo '<span id="currency_after_result_price">'.$currency.'</span>'; }else if($position != 1 && $position != 2 && $position !=3){ echo '<span id="currency_after_space_result_price">&nbsp;'.$currency."</span>"; } ?></span></span>
					
					<input type="hidden" name="total_price" id="total_price" value="<?php if($total_price != ""){ echo $total_price; }else if($catid != ""){  echo $catprice->term_price; }else{ echo "0";} ?>"/>
				</div>
				<span class="message_note"> </span>
				
				<label><?php echo '<span class="message_note">';_e("Final amount including discount will be displayed on preview page.",TFM_DOMAIN);echo "</span>";?></label>
			<!-- END - FETCH TOTAL PRICE -->
			</div>
	<?php
		}
		?>
        <span id="process2" style="display:none;"><i class='fa fa-circle-o-notch fa-spin'></i></span>
        <?php
	}
}
/*
Name : monetize_templ_total_price
Args : taxonomy name
Desc : return the total price of selected categories
*/
function monetize_templ_total_price($taxonomy){
	$args = array('hierarchical' => true ,'hide_empty' => 0, 'orderby' => 'term_group');
	$terms = get_terms($taxonomy, $args);
	$total_price=0;
	foreach($terms as $term){
		if(isset($_SESSION['package_select']) && $_SESSION['package_select'] != '' )
		{
			$monetize_term = explode(',',get_post_meta($_SESSION['package_select'],'category',true));
		}
		if((isset($_REQUEST['package_id']) && $_REQUEST['package_id'] != ''))
		{
			$monetize_term = explode(',',get_post_meta($_REQUEST['package_id'],'category',true));
		}
		if(is_active_addons('monetization') && $cp !='0') { $dispay_price = " (".fetch_currency_with_position($cp).")"; }else{ $dispay_price = ''; } /* cat price works only with monetization */ 
		
		if(((isset($_REQUEST['package_select']) && $_REQUEST['package_select'] != '') || (isset($_SESSION['package_select']) && $_SESSION['package_select'] != '') )  ||  ((isset($_REQUEST['sub_package_select']) && $_REQUEST['sub_package_select'] != '')  ) )
		{
			if(in_array($term->term_id,$monetize_term))
			{
				$total_price += $term->term_price;
			}
		}
		
	}
	return $total_price;
}

/**
 * Output an unordered list of checkbox <input> elements labelled
 * with term names. Taxonomy independent version of wp_category_checklist().
 *
 * @since 3.0.0
 *
 * @param int $post_id
 * @param array $args
 
Display the categories check box like wordpress - wp-admin/includes/meta-boxes.php
 */
function tev_fm_wp_terms_checklist($post_id = 0, $args = array()) {
	global  $cat_array,$current_user,$monetization;
 	$defaults = array(
		'descendants_and_self' => 0,
		'selected_cats' => false,
		'popular_cats' => false,
		'walker' => null,
		'taxonomy' => 'category',
		'checked_ontop' => true
	);
	if(isset($_REQUEST['backandedit']) != '' || (isset($_REQUEST['pid']) && $_REQUEST['pid']!="") ){
		$place_cat_arr = $cat_array;
		$post_id = $_REQUEST['pid'];
	}
	else
	{
		if(!empty($cat_array)){
			for($i=0; $i < count($cat_array); $i++){
				$place_cat_arr[] = @$cat_array[$i]->term_taxonomy_id;
			}
		}
	}
	$args = apply_filters( 'wp_terms_checklist_args', $args, $post_id );
	$template_post_type = get_post_meta($post->ID,'submit_post_type',true);
	extract( wp_parse_args($args, $defaults), EXTR_SKIP );

	if ( empty($walker) || !is_a($walker, 'Walker') )
		$walker = new Tev_Fm_Walker_Category_Checklist;

	$descendants_and_self = (int) $descendants_and_self;

	$args = array('taxonomy' => $taxonomy);

	$tax = get_taxonomy($taxonomy);
	$args['disabled'] = !current_user_can($tax->cap->assign_terms);

	if ( is_array( $selected_cats ) )
		$args['selected_cats'] = $selected_cats;
	elseif ( $post_id )
		$args['selected_cats'] = wp_get_object_terms($post_id, $taxonomy, array_merge($args, array('fields' => 'ids')));
	else
		$args['selected_cats'] = array();

	if ( is_array( $popular_cats ) )
		$args['popular_cats'] = $popular_cats;
	else
		$args['popular_cats'] = get_terms( $taxonomy, array( 'get' => 'all', 'fields' => 'ids', 'orderby' => 'count', 'order' => 'DESC', 'hierarchical' => false ) );

	if ( $descendants_and_self ) {
		$categories = (array) get_terms($taxonomy, array( 'child_of' => $descendants_and_self, 'hierarchical' => 0, 'hide_empty' => 0 ) );
		$self = get_term( $descendants_and_self, $taxonomy );
		array_unshift( $categories, $self );
	} else {
		$categories = (array) get_terms($taxonomy, array('get' => 'all'));
	}
	/* Check price paclkage type*/
	$user_have_pkg = $monetization->templ_get_packagetype($current_user->ID,$post_type); 
	//if user choose pay per subscription then set the package id and package_select request
	if($user_have_pkg==2){
		$_REQUEST['package_select']=$_REQUEST['package_id']= $package_id=get_user_meta($current_user->ID,'package_selected',true);
	}
	

	if ( $checked_ontop ) {
		// Post process $categories rather than adding an exclude to the get_terms() query to keep the query the same across all posts (for any query cache)
		$checked_categories = array();
		$keys = array_keys( $categories );
		$c=0;
		foreach( $keys as $k ) {
			if( $categories[$k]->term_id !='' && is_array($place_cat_arr)){
			if ( in_array( $categories[$k]->term_id, $place_cat_arr ) ) {
				$checked_categories[] = $categories[$k];
				unset( $categories[$k] );
			}
			}
		}
		// Put checked cats on top
		echo call_user_func_array(array(&$walker, 'walk'), array($checked_categories, 0, $args));
	}
	// Then the rest of them

	echo call_user_func_array(array(&$walker, 'walk'), array($categories, 0, $args));
	if(empty($categories) && empty($checked_categories)){

			echo '<span style="font-size:12px; color:red;">'.sprintf(__('You have not created any category for %s post type.So, this listing will be submited as uncategorized.',DOMAIN),$template_post_type).'</span>';
	}
}

/**
 * Walker to output an unordered list of category checkbox <input> elements.
 *
 * @see Walker
 * @see wp_category_checklist()
 * @see wp_terms_checklist()
 * @since 2.5.1
 */
class Tev_Fm_Walker_Category_Checklist extends Walker {
	var $tree_type = 'category';
	var $db_fields = array ('parent' => 'parent', 'id' => 'term_id'); //TODO: decouple this

	/**
	 * Starts the list before the elements are added.
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of category. Used for tab indentation.
	 * @param array  $args   An array of arguments. @see wp_terms_checklist()
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent<ul class='children'>\n";
	}

	/**
	 * Ends the list of after the elements are added.
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of category. Used for tab indentation.
	 * @param array  $args   An array of arguments. @see wp_terms_checklist()
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	/**
	 * Start the element output.
	 * @param string $output   Passed by reference. Used to append additional content.
	 * @param object $category The current term object.
	 * @param int    $depth    Depth of the term in reference to parents. Default 0.
	 * @param array  $args     An array of arguments. @see wp_terms_checklist()
	 * @param int    $id       ID of the current term.
	 */
	function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
		
		extract($args);
		global $cat_array;
		if ( empty($taxonomy) )
			$taxonomy = 'category';

		if ( $taxonomy == 'category' )
			$name = 'post_category';
		else
			$name = 'tax_input['.$taxonomy.']';
		
			if((isset($_REQUEST['package_id']) && $_REQUEST['package_id'] != ''))
			{
				$monetize_term = explode(',',get_post_meta($_REQUEST['package_id'],'category',true));
			}elseif(isset($_SESSION['package_select']) && $_SESSION['package_select'] != '' )
			{
				$monetize_term = explode(',',get_post_meta($_SESSION['package_select'],'category',true));
			}
			global $sitepress;
		
			if(((isset($_REQUEST['package_select']) && $_REQUEST['package_select'] != '') || (isset($_SESSION['package_select']) && $_SESSION['package_select'] != '') )  ||  ((isset($_REQUEST['sub_package_select']) && $_REQUEST['sub_package_select'] != '')  ) )
			{
				if(!empty($cat_array)){
					$selected_cats=$cat_array;
				}	
				if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
				{
					if(in_array(icl_object_id($category->term_id, $taxonomy, true, $sitepress->get_default_language()),$monetize_term))
					{ 
						if($category->term_price !=''){$cprice = "&nbsp;(".fetch_currency_with_position($category->term_price).")"; }else{ $cprice =''; }
					//	$class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';
						$output .= "\n<li id='{$taxonomy}-{$category->term_id}'>" . '<label class="selectit"><input value="' . $category->term_id . ','.$category->term_price.'" type="checkbox" name="category[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' . checked( in_array( $category->term_id, $selected_cats ), true, false ) .    ' onclick="monetize_fetch_packages('.$category->term_price.',this.form)"/> ' . esc_html( apply_filters('the_category', $category->name )) . $cprice.'</label>';
					}
				}
				else
				{
					if(in_array($category->term_id,$monetize_term))
					{
						if($category->term_price !=''){$cprice = "&nbsp;(".fetch_currency_with_position($category->term_price).")"; }else{ $cprice =''; }
						//	$class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';
							$output .= "\n<li id='{$taxonomy}-{$category->term_id}'>" . '<label class="selectit"><input value="' . $category->term_id . ','.$category->term_price.'" type="checkbox" name="category[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' . checked( in_array( $category->term_id, $selected_cats ), true, false ) .    ' onclick="monetize_fetch_packages('.$category->term_price.',this.form)"/> ' . esc_html( apply_filters('the_category', $category->name )) . $cprice.'</label>';
					}
				}
				
			}else
			{
				global $current_user,$post;
				$package_id=get_user_meta($current_user->ID,get_post_meta($post->ID,'submit_post_type',true).'_package_select',true);// get the user selected price package id
				if((isset($package_id) && $package_id != ''))
				{
					$monetize_term = explode(',',get_post_meta($package_id,'category',true));
				}
				if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
				{	
					if(in_array(icl_object_id($category->term_id, $taxonomy, true, $sitepress->get_default_language()),$monetize_term))
					{
						if($category->term_price !=''){$cprice = "&nbsp;(".fetch_currency_with_position($category->term_price).")"; }else{ $cprice =''; }
						//	$class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';
							$output .= "\n<li id='{$taxonomy}-{$category->term_id}'>" . '<label class="selectit"><input value="' . $category->term_id . ','.$category->term_price.'" type="checkbox" name="category[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' . checked( in_array( $category->term_id, $selected_cats ), true, false ) .    ' onclick="monetize_fetch_packages('.$category->term_price.',this.form)"/> ' . esc_html( apply_filters('the_category', $category->name )) . $cprice.'</label>';
					}
				}
				else
				{
					if(in_array($category->term_id,$monetize_term))
					{
						if($category->term_price !=''){$cprice = "&nbsp;(".fetch_currency_with_position($category->term_price).")"; }else{ $cprice =''; }
						//	$class = in_array( $category->term_id, $popular_cats ) ? ' class="popular-category"' : '';
							$output .= "\n<li id='{$taxonomy}-{$category->term_id}'>" . '<label class="selectit"><input value="' . $category->term_id . ','.$category->term_price.'" type="checkbox" name="category[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' . checked( in_array( $category->term_id, $selected_cats ), true, false ) .    ' onclick="monetize_fetch_packages('.$category->term_price.',this.form)"/> ' . esc_html( apply_filters('the_category', $category->name )) . $cprice.'</label>';
					}
				}
			}
	}

	/**
	 * Ends the element output, if needed.
	 * @param string $output   Passed by reference. Used to append additional content.
	 * @param object $category The current term object.
	 * @param int    $depth    Depth of the term in reference to parents. Default 0.
	 * @param array  $args     An array of arguments. @see wp_terms_checklist()
	 */
	function end_el( &$output, $category, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
}
/*
Name : update_post_before_submit
Args : post id
Desc : delete the extra custom fileds while renew.
*/
add_action('update_post_before_submit','delete_extra_custom_fileds',$last_postid);
function delete_extra_custom_fileds($last_postid)
{ 	
	global $wpdb;
	$_SESSION['custom_fields']['package_select'];
	$new_package_select = get_post_meta($_SESSION['custom_fields']['package_select'],'custom_fields',true);/*fetch the new price package selected custom feidls array*/
	$package_select = get_post_meta($last_postid,'package_select',true);
	$old_package_select = get_post_meta($package_select,'custom_fields',true);/*fetch the old custom feidls array*/
	$result = array_diff($old_package_select,$new_package_select);/*differene between old and new custom fields*/

	/*following loop is to delete the custom fields*/
	foreach($result as $key=>$var)
	{
	 	$custom_fields = get_post_meta($var,'htmlvar_name',true);
		$custom_fields_value =  get_post_meta($last_postid,$custom_fields,true);
		delete_post_meta($last_postid,$custom_fields,$custom_fields_value);
	}
}

/* Get selected price package values to submit form */
add_action('wp_ajax_tmpl_ajax_price','tmpl_ajax_price');
add_action('wp_ajax_nopriv_tmpl_ajax_price','tmpl_ajax_price');

/* Get selected price package values to submit form. Prevously this code was in - Tevolution-FieldsMonetization\shortcodes\ajax_price.php */

function tmpl_ajax_price()
{
	require(ABSPATH . "wp-load.php");
	if(isset($_REQUEST['pkid'])){
	$packid = $_REQUEST['pkid']; }else{
		$pxid = 1;
	}
	$pckid = $_REQUEST['pckid'];
	$post_type = $_REQUEST['post_type'];
	$taxonomy = $_REQUEST['taxonomy'];
	$all_cat_id = str_replace('|',',',$_REQUEST['pckid']);
	global  $price_db_table_name,$wpdb ;
		if($packid != "")
		{
		$pricesql = $wpdb->get_row("select * from $wpdb->posts where ID='".$packid."'"); 
		$homelist = get_post_meta($packid,'feature_amount',true);
		if(!$homelist){ $homelist =0; }
		$catlist =  get_post_meta($packid,'feature_cat_amount',true);
		if(!$catlist){ $catlist =0; }
		$bothlist = $catlist + $homelist;
		$packprice = get_post_meta($packid,'package_amount',true);
		$is_featured = get_post_meta($packid,'is_featured',true);
		$alive_days = get_post_meta($packid,'validity',true);
		//pass the number of category can select maximmum  category while submit form
		$category_can_select = get_post_meta($packid,'category_can_select',true);
		$none = 0;
		
		if(is_plugin_active('thoughtful-comments/fv-thoughtful-comments.php'))
		{
			$comment_mederation_amount = get_post_meta($packid,'comment_mederation_amount',true);
			if(!$comment_mederation_amount){ $comment_mederation_amount = 0; }
			$can_author_mederate = get_post_meta($packid,'can_author_mederate',true);
			$priceof = array($homelist,$catlist,$bothlist,$none,$packprice,$is_featured,$alive_days,$can_author_mederate,$comment_mederation_amount,$category_can_select);
			$rawrsize = sizeof($priceof);
		}
		else
		{
			$priceof = array($homelist,$catlist,$bothlist,$none,$packprice,$is_featured,$alive_days,$category_can_select);
			$rawrsize = sizeof($priceof);
		}
		$returnstring = "";
		
		//go through the array, using a unique identifier to mark the start of each new record
		for($i=0;$i<$rawrsize;$i++)
		{
			
			$returnstring .= $priceof[$i];
			$returnstring .= '###RAWR###';
		}
		
		echo $returnstring;exit;
		}
}

/*
	If we pass the category 
*/
function templ_get_custom_categoryid($category_id){
		foreach($category_id as $_category_arr)
		{
			$category[] = explode(",",$_category_arr);
		}
		if(isset($category))
		foreach($category as $_category){
			$arr_category[] = $_category[0];
			$arr_category_price[] = $_category[1];
		}
		return $cat_array = $arr_category;	
}
/*
* hide custom fields while its not selected in selected price package.
*/
add_filter('tevolution_custom_fields_loop_frontend','tevolution_custom_fields_loop_frontend',10,6);
function tevolution_custom_fields_loop_frontend($return_value, $custom_fields_id,$post,$post_type,$pkg_id,$name)
{
	/*condition to check whether custom fields are selected in particular price package or not*/
	if(is_array(get_post_meta($pkg_id,'custom_fields',true)) && !in_array($custom_fields_id,get_post_meta($pkg_id,'custom_fields',true)) && $name != 'post_title' && $name != 'post_content' && $name != 'post_excerpt' && $name != 'post_images')
	{ 
		return;
	}
	return $return_value;
}
/*added action to hidden field to check category selection*/
add_action('action_after_custom_fields','action_after_custom_fields',10,4);
function action_after_custom_fields($custom_metaboxes,$custom_fields,$post_type,$pakg_id)
{   
	if(isset($_REQUEST['action_edit']) && $_REQUEST['action_edit'] == 'edit')
	{
	
		$pakg_id = get_post_meta($_REQUEST['pid'],'package_select',true);
	
	}
	echo "<input type='hidden' id='category_can_select' name='category_can_select' value='".get_post_meta($pakg_id,'category_can_select',true)."'>";
}
/* call image uploader file from monetization plugin */
add_filter('include_image_upload_script','include_image_upload_script');
function include_image_upload_script()
{
	return FIELDS_MONETIZATION_PLUGIN_DIR."shortcodes/image_uploader.php";	
}
?>
