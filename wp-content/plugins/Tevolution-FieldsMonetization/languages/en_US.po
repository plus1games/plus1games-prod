msgid ""
msgstr ""
"Project-Id-Version: field monetization\n"
"POT-Creation-Date: 2015-04-14 10:12+0530\n"
"PO-Revision-Date: 2015-04-14 10:12+0530\n"
"Last-Translator: rishi <rcptemplatic@gmail.com>\n"
"Language-Team: templatic <upendra@templatic.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.9\n"
"X-Poedit-Basepath: D:\\wamp\\www\\wordpress\\wp-content\\plugins\\Tevolution-"
"FieldsMonetization\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: D:\\wamp\\www\\wordpress\\wp-content\\plugins"
"\\Tevolution-FieldsMonetization\n"

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:231
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:261
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:377
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:407
msgid "Text"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:235
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:285
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:381
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:431
msgid "Text Editor"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:239
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:289
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:385
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:435
msgid "Textarea"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:243
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:389
msgid "Image Uploader"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:252
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:325
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:398
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:467
msgid "Max char limit"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:265
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:411
msgid "Multi image uploader"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:269
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:415
msgid "Date Picker"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:273
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:419
msgid "Multi Checkbox"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:277
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:423
msgid "Radio"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:281
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:427
msgid "Select"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:293
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:301
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:439
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:447
msgid "File uploader"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:297
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:443
msgid "Geo Map"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:305
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:451
msgid "Multi City"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:313
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/fields_monetization.php:455
msgid "Map View"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/install.php:31
msgid ""
"You have activated Tevolution - Fields Monetization plugin, this will allow "
"you to limit custon fields, categories, number of images users can upload, "
"etc in different price packages. We recommend you to go through the detailed "
"<b><a href=\"http://templatic.com/docs/tevolution-fields-monetization/"
"\">documentation guide</a></b> of this plugin."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/install.php:35
#, php-format
msgid ""
"You have not activated the base plugin %s. Please activate it to use "
"Tevolution - Fields Monetization plugin."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/category.php:50
msgid "Select All"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/category.php:93
msgid "Select Category"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:74
msgid "Select a Package"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:146
msgid "Package type"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:148
msgid "Pay per subscription"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:151
msgid "Pay per post"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:155
msgid "Number of Posts"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:158
msgid "Validity"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:162
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:178
msgid "Days"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:166
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:182
msgid "Months"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:170
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:186
msgid "Years"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:175
msgid "Recurring Charges"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:188
msgid " Cycle"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:197
msgid "Charges"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:307
msgid "Sorry no category found for the selected price package."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:489
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:600
msgid "Information"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:568
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:573
msgid " characters left"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:975
msgid "Please Select"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1055
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1065
msgid " image"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1057
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1067
msgid " images"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1058
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1068
msgid "You can upload maximum "
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1378
msgid "Show a different month"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1453
#, php-format
msgid "Would you like to make this %s featured?"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1456
msgid "Yes &sbquo; feature this listing on homepage."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1456
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1458
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/price_package_js.php:166
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/price_package_js.php:175
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/price_package_js.php:325
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/price_package_js.php:327
msgid "Free"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1458
msgid "Yes &sbquo; feature this listing on category pages."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1467
#, php-format
msgid ""
"An additional amount will be charged to make this %s featured. You have the "
"option to feature your %s on home page or category page or both."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1483
#, php-format
msgid "Would you like to make this %s comment be moderate?"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1501
#, php-format
msgid ""
"An additional amount will be charged to make this %s comment moderateable."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1520
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1655
msgid "Total price as per your selection."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1648
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1780
msgid "Final amount including discount will be displayed on preview page."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/custom_fields_functions.php:1917
#, php-format
msgid ""
"You have not created any category for %s post type.So, this listing will be "
"submited as uncategorized."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:79
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:85
msgid "Upload Image"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:89
msgid "Maximum upload file size: "
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:125
msgid "Please select category first."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:136
msgid "Your image size must be less then"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:136
msgid "kilobytes"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:178
msgid "You can upload maximum"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/image_uploader.php:178
msgid "images"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:31
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:34
#, php-format
msgid "%s"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:36
msgid "You cannot select more than "
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:37
msgid " categories with this package."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:148
msgid "Please provide your email address"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:154
msgid "Please provide valid email address"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/shortcodes/submition_validation.php:359
msgid "Oops!, looks like you forgot to enter a value in some compulsory field"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:13
msgid "Allow user to select number of categories"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:18
msgid ""
"Enter maximum number of categories that you want your users to select when "
"they submit a listing from frontend."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:23
msgid "Settings for custom fields"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:24
msgid ""
"These settings are must if you want to charge the users for custom fields "
"wise"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:29
msgid "Custom Fields"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:42
msgid "Please select any of the above category."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:46
msgid ""
"Check the above box if you want the price package to show selected custom "
"fields."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:48
msgid "Note:"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:49
msgid ""
"Text box below will allow user to fill the textbox, textarea and texteditor "
"to maximum character count."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:55
msgid "Allow number of images to upload"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:60
msgid ""
"Enter the maximum number of images can upload if not user can upload maximum "
"\"Ten\" images by default."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:66
msgid "Price Settings"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:138
msgid "custom fields"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:142
msgid ""
"Selected category does not have any custom fields.Please create it from "
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:155
#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_custom_fields_monetization.php:198
msgid ""
"Please select a number of categories greater than or equal to the number of "
"allowed categories per submission or decrease the number of allowed "
"categories in the field below"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:36
msgid ""
"Invalid UserName or password. are you using templatic member username and "
"password?"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:59
msgid ""
"We don't find Tevolution - Fields Monetization Plugin active in your "
"templatic account, you will not be able to update without a license"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:64
msgid ""
"Invalid UserName or password. Please enter templatic member's username and "
"password."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:84
msgid ""
"Templatic Login , enter your templatic credentials to take the updates of "
"Tevolution-FieldsMonetization."
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:88
msgid "User Name"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:92
msgid "Password"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:97
msgid "Cancel"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:105
msgid "Tevolution-FieldsMonetization Plugin"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/templatic_login.php:105
msgid "Update Now"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/wp-updates-plugin.php:44
msgid ""
"There is a new version of Tevolution-FieldsMonetization plugin available "
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/wp-updates-plugin.php:49
msgid " <a href=\""
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/wp-updates-plugin.php:92
msgid ""
"An Unexpected HTTP Error occurred during the API request.</p> <p><a href=\"?"
"\" onclick=\"document.location.reload(); return false;\">Try again</a>"
msgstr ""

#: D:\wamp\www\wordpress\wp-content\plugins\Tevolution-FieldsMonetization/wp-updates-plugin.php:96
msgid "An unknown error occurred"
msgstr ""
