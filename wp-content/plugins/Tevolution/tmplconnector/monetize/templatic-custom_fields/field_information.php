<?php 
$pid = "";
if($_REQUEST['data']== 'categoryid'){
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once( $parse_uri[0] . 'wp-load.php' );
	global $wpdb;
	$selectedcatid = explode(',',$_REQUEST['term_id']);
	$pid= $_REQUEST['post_id'];
}
elseif(strpos($_SERVER['REQUEST_URI'] ,'wp-admin') !==false && !$_REQUEST['submit_post_url'] == '/add-places/'  && !$_REQUEST['data']== 'categoryid'){
	$pid= $_GET['post'];
	$post_terms_data = wp_get_object_terms( $pid, 'listingcategory');
	$selectedcatid = array();
	foreach($post_terms_data as $term){
		$selectedcatid[] = $term->term_id;
	}
}
else {
	$pid= $_REQUEST['pid'];
	$selectedcatid = explode(',',$_REQUEST['category_id']);
	array_pop($selectedcatid);
}
$children = get_term_children( 345,'listingcategory' );
array_push($children,'345');
$result = array_intersect($selectedcatid, $children);

if(!empty($result) || strpos($_SERVER['REQUEST_URI'] ,'add-places') !==false || (is_admin() && !$_REQUEST['submit_post_url'] == '/add-sport-groups/' && !$_REQUEST['submit_post_url'] == '/add-sporting-goods/')){
$fielddata = $wpdb->get_results("select * from wp_em_location_fields where post_id ='".$pid."'");
?>
<div class="form_row clearfix custom_fileds field_information" style="width:100%; overflow:scroll;margin-top:20px;">
	<button id="addrow" type="button" name="addrow" value="Add Fields" >Add Field</button>
	<table  id="locationfields" border="1" width="100%">
		<tr height="26" style="font-weight:bold;background:#FFFBCC">
			<th>Action&nbsp;&nbsp;&nbsp;</th> 
			<th>Field Category</th>
			<th>Field Name</th>
			<th>Indoor/Outdoor</th>
			<th>Public-Private</th>
			<th>Description</th>
			<th>Size</th>
			<th>Type</th>
		</tr>
		<?php if($pid == "" || empty($fielddata) ){ ?>
		<tr>
			<td > <a class="field_edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/edit_icon.jpg" title="edit"></a><a  href="" class="field_remove"><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/delete_icon.jpg" title="delete"></a>
			<a  href="" class="field_duplicate"><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/copy_icon.jpg" title="duplicate"></a>
			</td>
				<?php	
					$args5=array(
					'orderby'    => 'name',
					'taxonomy'   => 'listingcategory',
					'order'      => 'ASC',
					'parent'     => '345',
					'show_count' => 0,
					'hide_empty' => 0,
					'pad_counts' => true,					
					); 
				$categories=get_categories($args5);
				?>
			<input type="hidden" name="field_id[]" value="" >
			<td>
				<!-- <input type="text" name="field_category[]" value=""> -->		
				<select name="field_category[]" style="width:200px;">
				<option value="">Select Category</option>
				<?php foreach($categories as $category){			
					if(in_array($category->term_id,$selectedcatid)){
						echo '<option value="'.$category->term_id.'">'.$category->name.'</option>';
					}
				}  ?>
				</select>
			</td>
			<td><input type="text" name="field_name[]" value="" ></td>
			<td><!-- <input type="text" name="field_indoor[]" value=""> -->
				<select name="field_indoor[]">
				<option value="Indoor">Indoor</option>
				<option value="Outdoor">Outdoor</option>
				</select>
			</td>
			<td>
				<select name="field_public[]" >
				<option value="Public">Public</option>
				<option value="Private">Private</option>
				<option value="Semi-Private">Semi-Private</option>
				<option value="Resort">Resort</option>
				<option value="Military">Military</option>
				</select>
			</td>
			<td><input type="text" name="field_desc[]" value="" ></td>	
			<td><input type="text" name="field_size[]" value="" ></td>	
			<td><input type="text" name="field_type[]" value="" ></td>	
		</tr>
		<?php } 
		else{ 
			foreach( $fielddata as $fd ){?>
				<tr>
					<td > <a class="field_edit" href="" ><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/edit_icon.jpg" title="edit"></a><a  href="" class="field_remove"><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/delete_icon.jpg" title="delete"></a>
					<a  href="" class="field_duplicate"><img src="<?php  echo content_url(); ?>/plugins/Tevolution/images/copy_icon.jpg" title="duplicate"></a>
					</td>
						<?php	
							$args5=array(
							'orderby'    => 'name',
							'taxonomy'   => 'listingcategory',
							'order'      => 'ASC',
							'parent'     => '345',
							'show_count' => 0,
							'hide_empty' => 0,
							'pad_counts' => true,					
							); 
						$categories=get_categories($args5);
						?>
					<input type="hidden" name="field_id[]" value="<?php echo $fd->field_id; ?>" >
					<td>
						<!-- <input type="text" name="field_category[]" value=""> -->		
						<select name="field_category[]" style="width:200px;" disabled>
						<option value="">Select Category</option>
						<?php foreach($categories as $category){	
								if(in_array($category->term_id,$selectedcatid)){?>		
									<option value="<?php echo $category->term_id; ?>" <?php echo ($fd->field_category == trim($category->term_id)) ? "selected='selected'"  : '';  ?>><?php echo $category->name; ?></option>
						<?php   }
							}  ?>
						</select>
					</td>
					<td><input type="text" name="field_name[]" value="<?php echo $fd->field_name; ?>" readonly="readonly" ></td>
					<td><!-- <input type="text" name="field_indoor[]" value=""> -->
						<select name="field_indoor[]" disabled>
						<option value="Indoor" <?php echo ($fd->field_indoor == 'Indoor') ? "selected='selected' " : ''; ?>>Indoor</option>
						<option value="Outdoor" <?php echo ($fd->field_indoor == 'Outdoor') ? "selected='selected' " : ''; ?>>Outdoor</option>
						</select>
					</td>
					<td>
						<select name="field_public[]" disabled>
						<option value="Public" <?php echo ($fd->field_public == 'Public') ? "selected='selected' " : ''; ?>>Public</option>
						<option value="Private" <?php echo ($fd->field_public == 'Private') ? "selected='selected' " : ''; ?>>Private</option>
						<option value="Semi-Private" <?php echo ($fd->field_public == 'Semi-Private') ? "selected='selected' " : ''; ?>>Semi-Private</option>
						<option value="Resort" <?php echo ($fd->field_public == 'Resort') ? "selected='selected' " : ''; ?>>Resort</option>
						<option value="Military" <?php echo ($fd->field_public == 'Military') ? "selected='selected' " : ''; ?>>Military</option>
						</select>
					</td>
					<td><input type="text" name="field_desc[]" value="<?php echo $fd->field_desc; ?>" readonly="readonly"></td>	
					<td><input type="text" name="field_size[]" value="<?php echo $fd->field_size; ?>" readonly="readonly"></td>	
					<td><input type="text" name="field_type[]" value="<?php echo $fd->field_type; ?>" readonly="readonly"></td>	
				</tr>
	<?php	}
		 } ?>
	</table>
</div>
<?php } ?>
			<script>	
	jQuery(document).ready(function(){
		jQuery('tr.row-test_field').hide();
		jQuery('.form_row.test_field').hide();
		var rowCount = jQuery('#locationfields tr').length;
		if(rowCount==2){
			jQuery('.field_remove').hide();
		}
		jQuery("#addrow").click(function(){
			var rowCount = jQuery('#locationfields tr').length;
			jQuery('.field_remove').show();
			var clonedRow = jQuery('#locationfields tbody>tr:last').clone(true);	
			clonedRow.each(function () {
				jQuery(this).find('input[name^=field_id]').val('').attr('name','field_id[]').attr("readonly", false);
				        jQuery(this).find('input[name^=field_name]').val('').attr('name','field_name[]').attr("readonly", false);
						jQuery(this).find('select[name^=field_category]').val('').attr('name','field_category[]').attr("readonly", false).find('option[value=""]').attr('selected', true).change();

						jQuery(this).find('select[name^=field_indoor]').val('Indoor').attr('name','field_indoor[]').attr("readonly", false).find('option[value="Indoor"]').attr('selected', true).change();
						jQuery(this).find('select[name^=field_public]').val('Public').attr('name','field_public[]').attr("readonly", false).find('option[value="Public"]').attr('selected', true).change();
						jQuery(this).find('input[name^=field_size]').val('').attr('name','field_size[]').attr("readonly", false);
						jQuery(this).find('input[name^=field_type]').val('').attr('name','field_type[]').attr("readonly", false);
						jQuery(this).find('input[name^=field_desc]').val('').attr('name','field_desc[]').attr("readonly", false);  
						
			});
			clonedRow.insertAfter('#locationfields tbody>tr:last'); 

		});
		jQuery("a.field_remove").click(function() {		
			
		    var ansconf =  confirm("Are you sure you want to delete?");
			if(ansconf){
				jQuery(this).closest("tr").remove();
				var rowCount = jQuery('#locationfields tr').length;
				if(rowCount == 2){
					jQuery('.field_remove').hide();
				}
				return false; 
			}
			else{
				return false; 
			}
		});
		jQuery("a.field_edit").click(function(){
			jQuery(this).parent().parent().find("td input").removeAttr("readonly");
			jQuery(this).parent().parent().find("td select").removeAttr("disabled");
			return false;
		});
		jQuery("a.field_duplicate").click(function(){
			jQuery('.field_remove').show();
			  var sel1 = jQuery(this).parent().parent().find('select[name^=field_cat]').val();
			   var sel2 = jQuery(this).parent().parent().find('select[name^=field_ind]').val();
				var sel3 = jQuery(this).parent().parent().find('select[name^=field_pub]').val();
			     var duplicate_row = jQuery(this).parent().parent().clone(true); 
				    duplicate_row.each(function () {
						jQuery(this).find('input[name^=field_name]').attr('name','field_name[]').attr("readonly", false);
						 jQuery(this).find('select[name^=field_category]').attr('name','field_category[]').attr("readonly", false);
						  jQuery(this).find('select[name^=field_indoor]').attr('name','field_indoor[]').attr("readonly", false);
						   jQuery(this).find('select[name^=field_public]').attr('name','field_public[]').attr("readonly", false);
						   jQuery(this).find('input[name^=field_size]').attr('name','field_size[]').attr("readonly", false);
						    jQuery(this).find('input[name^=field_type]').attr('name','field_type[]').attr("readonly", false);
						     jQuery(this).find('input[name^=field_desc]').attr('name','field_desc[]').attr("readonly", false);						  
					});
			       if(duplicate_row.insertAfter('#locationfields tbody>tr:last')){ 
			      jQuery('#locationfields tbody>tr:last').find('select[name^=field_cat]').val(sel1);
			     jQuery('#locationfields tbody>tr:last').find('select[name^=field_ind]').val(sel2);
			    jQuery('#locationfields tbody>tr:last').find('select[name^=field_pub]').val(sel3);
			}
		return false;
	   });
		
});
</script>