<?php 

if(isset($_REQUEST['page']) && $_REQUEST['page'] =='paynow'){ 
{ 
	$submit_booking_nonce = ($_REQUEST['submit_booking_nonce']) ? $_REQUEST['submit_booking_nonce'] : "";
	
	if(wp_verify_nonce($submit_booking_nonce, 'booking_submit_nonce')){
	
	//contion for captcha inserted properly or not.
			$tmpdata = get_option('templatic_settings');
			if(isset($tmpdata['user_verification_page']) && $tmpdata['user_verification_page'] != "")
			{
				$display = $tmpdata['user_verification_page'];
			}
			else
			{
				$display = "";	
			}
			$id = $_REQUEST['booking_page_id'];
			$permalink = get_permalink( $id );
			if( is_plugin_active('wp-recaptcha/wp-recaptcha.php') && $tmpdata['recaptcha'] == 'recaptcha' && in_array('submit',$display)){
					require_once( ABSPATH.'wp-content/plugins/wp-recaptcha/recaptchalib.php');
					$a = get_option("recaptcha_options");
					$privatekey = $a['private_key'];
									$resp = recaptcha_check_answer ($privatekey,
											getenv("REMOTE_ADDR"),
											$_POST["recaptcha_challenge_field"],
											$_POST["recaptcha_response_field"]);
														
					if (!$resp->is_valid ) {
						if($_REQUEST['captcha_room_house_booking_id'] != '')
						 {
							wp_safe_redirect($permalink.'?id='.$_REQUEST['captcha_room_house_booking_id'].'&ecptcha=captch');
						 }
						 else
						 {
							wp_safe_redirect($permalink.'/?ecptcha=captch');	 
						 }
						exit;
					} 
				}
			if(file_exists(ABSPATH.'wp-content/plugins/are-you-a-human/areyouahuman.php') && is_plugin_active('are-you-a-human/areyouahuman.php') && $tmpdata['recaptcha'] == 'playthru'  && in_array('submit',$display))
			{
				require_once( ABSPATH.'wp-content/plugins/are-you-a-human/areyouahuman.php');
				require_once(ABSPATH.'wp-content/plugins/are-you-a-human/includes/ayah.php');
				$ayah = new AYAH();
				$score = $ayah->scoreResult();
				if(!$score)
				{
					if($_REQUEST['captcha_room_house_booking_id'] != '')
					 {
						wp_safe_redirect($permalink.'?id='.$_REQUEST['captcha_room_house_booking_id'].'&invalid=playthru');
					 }
					 else
					 {
						wp_safe_redirect($permalink.'/?invalid=playthru');	 
					 }
					exit;
				}
			}
		
		//Initialize booking variables Start
		
		//fetch_posts_default_paid_status
		
		$post_title = $_REQUEST['booking_first_name'].' '.$_REQUEST['booking_last_name'];
		$post_name = $_REQUEST['booking_first_name'].'_'.$_REQUEST['booking_last_name'];
		$post_content = __("User Booking Request");
		$booking_date = date('Y-m-d');
		$checkin_date = $_REQUEST['checkindate'];
		$checkout_date = $_REQUEST['checkoutdate'];
		$paymentmethod = $_REQUEST['paymentmethod'];
		$booking_post_type = $_REQUEST['booking_post_type'];
		$services = explode(',',$_REQUEST['booking_service_ids']);
		//$services = $_REQUEST['booking_service_ids'];
		if(isset($paymentmethod) && $paymentmethod=='prebanktransfer'){
			if(function_exists('fetch_posts_default_status')){
				$post_default_status = fetch_posts_default_status();
			}else{
				$post_default_status = 'publish';
			}
		}else{
			if(function_exists('fetch_posts_default_paid_status')){
				$post_default_status = fetch_posts_default_paid_status();
			}else{
				$post_default_status = 'draft';
			}
		}
		
		
		
		//Get property/room details
		$property_room_id = $_REQUEST['property_id'];
		if($property_room_id){
			$property_room_details = get_post_custom($property_room_id);
		}
		$total_amount = $_REQUEST['total_amount'];
		//Initialize booking variables Finish
		
		//Insert booking details Start	
		$my_post = array(
			 'post_title' => $post_title,
			 'post_content' => $post_content,
			 'post_status' => $post_default_status,
			 'post_author' => 1,
			 'post_name' => $post_name,
			 'post_type' => CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,
			);
		$booking_post_id = wp_insert_post( $my_post );	
		if(is_plugin_active('wpml-translation-management/plugin.php')){
			if(function_exists('wpml_insert_templ_post'))
				wpml_insert_templ_post($booking_post_id,$my_post['post_type']); /* insert post in language */
		}
		global $wpdb,$wp_query;
		
		//ADD HOUSE/ROOM CUSTOM FIELD DATA IN DATABASE START
		if(function_exists('get_post_custom_fields_templ_booking_plugin')){
			$GetCustomFields = get_post_custom_fields_templ_booking_plugin($booking_post_type,'','','');
		}
		update_post_meta($booking_post_id,'cost_subtotal',$_REQUEST['hidden_cost_subtotal']);
		foreach($GetCustomFields as $key=>$val){
			if($val['type'] == 'text' OR $val['type'] == 'select' OR $val['type'] == 'checkbox' OR $val['type'] == 'textarea' OR $val['type'] == 'radio'  OR $val['type'] == 'upload' OR $val['type'] == 'date' OR $val['type'] == 'multicheckbox' OR $val['type'] == 'geo_map' OR $val['type'] == 'texteditor'){ // Normal Type Things...
				$var = $val["name"];
				if( get_post_meta( $booking_post_id, $val["name"], true ) == "" ){
					add_post_meta($booking_post_id, $val["name"], $_POST[$var], true );
				}elseif($_POST[$var] != get_post_meta($booking_post_id, $val["name"], true)){
					update_post_meta($booking_post_id, $val["name"], $_POST[$var]);
				}elseif($_POST[$var] == ""){
					delete_post_meta($booking_post_id, $val["name"], get_post_meta($booking_post_id, $val["name"], true));
				}
			}
		}
		//ADD HOUSE/ROOM CUSTOM FIELD DATA IN DATABASE FINISH
		
		//ADD BOOKING CUSTOM FIELD DATA IN DATABASE START
		if(function_exists('get_post_custom_fields_templ_booking_plugin')){
			$default_custom_metaboxes = get_post_custom_fields_templ_booking_plugin(CUSTOM_POST_TYPE_TEMPLATIC_BOOKING,'','','');
		}
	
		foreach($default_custom_metaboxes as $key=>$val){
			if($val['type'] == 'text' OR $val['type'] == 'select' OR $val['type'] == 'checkbox' OR $val['type'] == 'textarea' OR $val['type'] == 'radio'  OR $val['type'] == 'upload' OR $val['type'] == 'date' OR $val['type'] == 'multicheckbox' OR $val['type'] == 'geo_map' OR $val['type'] == 'texteditor'){ // Normal Type Things...
				$var = $val["name"];
				if( get_post_meta( $booking_post_id, $val["name"], true ) == "" ){
					add_post_meta($booking_post_id, $val["name"], $_POST[$var], true );
				}elseif($_POST[$var] != get_post_meta($booking_post_id, $val["name"], true)){
					update_post_meta($booking_post_id, $val["name"], $_POST[$var]);
				}elseif($_POST[$var] == ""){
					delete_post_meta($booking_post_id, $val["name"], get_post_meta($booking_post_id, $val["name"], true));
				}
			}
		}
		$sep = ",";
		$cat_name = "";
		for($k=0;$k<count($services);$k++){
			if($k == (count($services)-1))
				$sep = "";
			
			$cat_name .= get_term($services[$k],'services')->name.$sep;
			
		}
		$category_name=explode(',',$cat_name);
		wp_set_object_terms($booking_post_id,$category_name, 'services');
		//ADD BOOKING CUSTOM FIELD DATA IN DATABASE FINISH
		//echo $total_amount;exit;
		$room_price = explode( get_option('currency_symbol'), $total_amount );
		$booking_service_prices = 0;
		$tax_price = 0;
		$discount_amt=0;
		$options = get_option('templatic_settings');
		$tax_type = $options['templatic_booking_system_settings']['templatic_booking_general_settings']['tax_type'];
		$days_between = ceil(abs(strtotime($checkout_date) - strtotime($checkin_date)) / 86400);	
		if(isset($_REQUEST['booking_service_prices']) && $_REQUEST['booking_service_prices']!= '')
		{
			$terms = get_the_terms( $booking_post_id, 'services' );
			foreach($terms as $skey => $svalue){
				$booking_service_prices += ($_REQUEST['hidden_no_of_rooms']*$days_between * $svalue->term_price);
			}
		}
		if(isset($_REQUEST['tax_price']) && $_REQUEST['tax_price'] != '')
			$tax_price = $_REQUEST['tax_price'];

		$room_price = ($room_price[1] - ($tax_price + $booking_service_prices));
		
		if(isset($_REQUEST['booking_coupon_id']) && $_REQUEST['booking_coupon_id'] != '')
		{
			
			$booking_coupon_id = $_REQUEST['booking_coupon_id'];
			$discount_type = get_post_meta($booking_coupon_id,'coupondisc',true);
			$discount_amount = get_post_meta($booking_coupon_id,'couponamt',true);
			$start_date = strtotime(get_post_meta($booking_coupon_id,'startdate',true));
			$end_date 	= strtotime(get_post_meta($booking_coupon_id,'enddate',true));
			$todays_date = strtotime(date("Y-m-d"));
			$b4_discount_totl_price = $_REQUEST['before_disc_amt'];
			if ($start_date <= $todays_date && $end_date >= $todays_date)
			{
				if($discount_type=='per')
				{
					$rem_discount_amount = (100 - $discount_amount);
					$grossprice = ($room_price/($rem_discount_amount/100));
					$discount_amt = abs(($b4_discount_totl_price*$discount_amount)/100);
				}
				elseif($discount_type=='amt')
				{
					$discount_amt = $discount_amount;
				}
			}
			if($discount_amt>0){
				$room_price = abs($room_price + $discount_amt);
			}else{
				$room_price = abs($room_price);
			}
		}
		$total_price = str_replace(get_option('currency_symbol'),'',$total_amount);
		$hidden_total = $_REQUEST['hidden_total'];
		
		//ADD DEFAULT STATIC CUSTOM FIELD DATA IN DATABASE START
		$post_meta = array(
					'booking_service_prices'	=> $booking_service_prices,
					'discount_amt'				=> $discount_amt,
					'tax_price'					=> $tax_price,
					'tax_type'					=> $tax_type,
					'booking_post_type' 		=> $booking_post_type,
					'room_price'				=> $room_price,
					'booking_date' 				=> $booking_date,
					'checkin_date' 				=> $checkin_date,
					'checkout_date' 			=> $checkout_date,
					'property_room_id' 			=> $property_room_id,
					'total_amount' 				=> $total_amount,
					'status' 					=> 'Pending',
					'paymentmethod' 			=> $paymentmethod,
					'total_price'				=> $total_price,
					'before_discount'			=> $b4_discount_totl_price,
					'room_house_total'			=> $hidden_total,
					'submit_booking_nonce'		=> $submit_booking_nonce
				);
		foreach($post_meta as $key=> $_post_meta){
			add_post_meta($booking_post_id, $key, $_post_meta);
		}
		//ADD DEFAULT STATIC CUSTOM FIELD DATA IN DATABASE FINISH

		
		// find the term in terms table and get id. i.e for doctor 
		// look in term taxonomy table to find term_taxonomy_id. i.e. term_id is 168 for doctor though its taxonomy is services and its term_taxonomy_id is 169
		// then run the sql query to assign particular taxonomy to post.
		
		
		//Insert booking details End
		
		// Insert availability details in availability check table START
			$checkout_previous_date = date('Y-m-d', strtotime($checkout_date .' -1 day'));
			$booking_availability_table = $wpdb->prefix."booking_availability_check";
			$sql_insert_availability = $wpdb->query("INSERT INTO $booking_availability_table SET property_id=$property_room_id, booking_post_id=$booking_post_id, from_date='$checkin_date', to_date='$checkout_previous_date', status='Pending'");
		// Insert availability details in availability check table FINISH
		
		
		$information_details = "";
		if($default_custom_metaboxes){
			$information_details.='<ul>';
			foreach($default_custom_metaboxes as $key=>$val){//echo "<pre>";print_r($val);echo "</pre>";
				if($key=='post_title' && $val['show_in_email'])
				{
					$information_details.= '<li><label>'.$val['label'].' :</label>'.$post_title.'</li>';
				}
				if($key=='post_content' && $val['show_in_email'] && $post_content!='')
				{
					$information_details.= '<li><label>'.$val['label'].' :</label>'.$post_content.'</li>';
				}
				if($val['type'] == 'multicheckbox' && get_post_meta($booking_post_id,$val['htmlvar_name'],true) !='' && $val['show_in_email']=='1')
				{
					$information_details.='<li><label>'.$val['label'].' :</label> '.  implode(",",get_post_meta($booking_post_id,$val['htmlvar_name'],true)).'</li>';
				}else{
					if($val['show_in_email']=='1' && get_post_meta($booking_post_id,$val['htmlvar_name'],true)!="")
					{
						$information_details.= '<li><label>'.$val['label'].' :</label>'.get_post_meta($booking_post_id,$val['htmlvar_name'],true).'</li>';
					}
				}
				
			}
			$type = get_post_type(get_post_meta($booking_post_id,'property_room_id',true));
			$post_type_object = get_post_type_object($type);
			$label_room_house = ucfirst($post_type_object->labels->name.' '.__("Type",BKG_DOMAIN));
			$information_details.= "<li><label>{$label_room_house}:</label> ".get_the_title(get_post_meta($booking_post_id,'property_room_id',true)).'</li>';
			$information_details.='</ul>';
		}
		
		///////EMAIL START//////
		$fromEmail = get_site_emailId_plugin();
		$fromEmailName = get_site_emailName_plugin();
		$toEmail = get_post_meta($booking_post_id,'booking_email',true);
		$toEmailName = $post_title;
		$store_name = get_option('blogname');
		$templatic_settings = get_option('templatic_settings');
		//echo "<pre>";print_r($templatic_settings);echo "</pre>";exit;
		$email_content = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_msg'];
		$email_subject = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_subject'];
		if(!$email_subject)
		{
			$email_subject = __('Booking- Email Confirmation');	
		}
		if(!$email_content)
		{
			$email_content  = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Thank you, your booking request is received successfully. We will contact you soon for the confirmation.',BKG_DOMAIN).'</p>';
            $email_content .= '<p>'.__('In case you have any queries, please email us at [CONTACT_MAIL]. We will be glad to assist you.',BKG_DOMAIN).'</p>';
			$email_content .= '<p>'.__('If you want to cancel your booking request then click on link given below.',BKG_DOMAIN).'<br />[CANCEL_URL]</p>';
			$email_content .= '<p>[BOOKING_DETAIL]</p>';
			$email_content .= '<p>'.__('Thank you for staying at our Hotel.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
		}
		$page_name = "booking-cancel";
		$page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."'");
		$page_name_id = get_option('booking_system_cancel');
		$cancel="<a href='".get_option('home')."/?page_id=".$page_name_id."&booking_id=".$booking_post_id."&submit_booking_nonce=".$submit_booking_nonce."'>".get_option('home')."/?page_id=".$page_name_id."&booking_id=".$booking_post_id."&submit_booking_nonce=".$submit_booking_nonce."</a>";
		
		$email_info = $property_room_details['home_contact_mail'][0];//Fetch email id from house's email meta information   //fetch_hotel_info('contact_hotel_mail');
		$hotel_email_info = ($email_info) ? $email_info : @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_email'];
		$booking_detail = templatic_booking_mail_success_text_display($booking_post_id);
		$booking_detail .= $information_details;
		
		$BookingPostType = get_post_meta($booking_post_id,'booking_post_type',true);
			
		if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
			$hotel_name = get_the_title($property_room_id);
			$addresses = get_post_meta($property_room_id,'address',true);
			$address_exclude = explode(',',$addresses);
			$home_street = $address_exclude[0];
			$home_state = $address_exclude[1];
			$home_country = $address_exclude[2];
		}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
			$hotel_name = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
			$home_street = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
			$home_state = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
			$home_country = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
		}
		
		
		$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
		if($home_country!=""){
			$regard_content .= ', '.$home_country;
		}
		$regard_content .=  '<br />'.@$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_phone'];
		$serach_array = array('[USER_NAME]', '[CONTACT_MAIL]', '[CANCEL_URL]', '[BOOKING_DETAIL]', '[ADMIN_NAME]');
		$replace_array = array($toEmailName, '<i>'.$hotel_email_info.'</i>', $cancel, $booking_detail, $regard_content);
		$mail_content = str_replace($serach_array,$replace_array,stripslashes($email_content));
		templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
		$admin_subject = __('A booking request has been submitted',BKG_DOMAIN);
		$booking_email = __('Hi ',BKG_DOMAIN).$fromEmailName.',<br /><br />'. __('A booking request has been submitted by',BKG_DOMAIN) .$toEmailName.'. '.__('More details are as below.',BKG_DOMAIN).'<br /><br />'.$booking_detail; 
		templ_send_email($toEmail,$toEmailName,$fromEmail,$fromEmailName,$admin_subject,stripslashes($booking_email),$extra='');//To admin email
	//////EMAIL END////////
	
		global $wpdb,$last_postid,$payable_amount,$transection_db_table_name,$trans_id;
		$transection_db_table_name = $wpdb->prefix . "transactions";
		$last_postid = $booking_post_id;
		$payable_amount = str_replace(get_option('currency_symbol'),'',$total_amount);
		$payment_amt = str_replace(get_option('currency_symbol'),'',$total_amount);
		$paymentmethod = $_REQUEST['paymentmethod'];
		if($paymentmethod !="" && $last_postid !=""){
			$post_author  = $wpdb->get_row("select * from $wpdb->posts where ID = '".$last_postid."'") ;
			
			$booking_user_details = unserialize($booking_user_details[0]);
			$post_title  = $post_author->post_title ;
			$post_author  = $post_author->post_author ;
			$user_fname = $post_title;
			$user_email = get_post_meta($last_postid,'booking_email',true);
			$user_billing_name = $post_title;
			$billing_Address = get_post_meta($last_postid,'booking_street',true).'<br/>'.get_post_meta($last_postid,'booking_city_state',true).', '.get_post_meta($last_postid,'booking_country',true);
			
			if($wpdb->query("SHOW COLUMNS FROM $transection_db_table_name where Field='prop_id'")){}
			else{
				$sql="ALTER table $transection_db_table_name add prop_id bigint(20)";
				$wpdb->query($sql);
			}
			
			 $transaction_insert = 'INSERT INTO '.$transection_db_table_name.' set 
				post_id="'.$last_postid.'",
				user_id = "'.$post_author.'",
				post_title ="'.$post_title.'",
				payment_method="'.$paymentmethod.'",
				payable_amt="'.$payable_amount.'",
				payment_date="'.date("Y-m-d H:i:s").'",
				paypal_transection_id="",
				status="0",
				user_name="'.$user_fname.'",
				pay_email="'.$user_email.'",
				billing_name="'.$user_billing_name.'",
				billing_add="'.$billing_Address.'",prop_id="'.$property_room_id.'"';
				global $trans_id;
				$wpdb->query($transaction_insert);
				$trans_id = mysql_insert_id();
		}
		
		//return mysql_insert_id();
		global $payable_amount;
		$payable_amount = $payment_amt;
		if(is_active_addons('monetization') && $total_amount != '' && $_REQUEST['paymentmethod']){
			if(function_exists('payment_menthod_response_url')){
				payment_menthod_response_url($_REQUEST['paymentmethod'],$last_postid,'','',$payment_amt);
			}
		}else{
			$suburl = "&pid=$last_postid";
			if(is_plugin_active('sitepress-multilingual-cms/sitepress.php'))
			{
				global $sitepress;
				if($sitepress->get_current_language())
				{
					wp_redirect(get_option( 'siteurl' ).'/'.$sitepress->get_current_language().'/?page=success&'.$suburl);
				}
			}
			else
			{
				wp_redirect(get_option('home').'/?page=success&'.$suburl);
			}
		}
		//wp_redirect(get_option( 'home' ).'/?page=templatic_success_booking');exit;
	}else{
		add_action('wp_footer','booking_script_error');
	}
	function booking_script_error(){
		echo '<script type="text/javascript">jQuery("#booking_common_error").html("You are not allowed to submit this form.");</script>';
	}
 }
}
	
?>
