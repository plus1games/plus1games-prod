<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");
$my_post_type = $_REQUEST['post_id'];
if(is_plugin_active('wpml-translation-management/plugin.php'))
{
	global $sitepress;
	$sitepress->switch_lang($_REQUEST['language']);
}
if($_REQUEST['post_id']){
	$post_type = get_post_type($_REQUEST['post_id']);
	$total_no_of_rooms = get_post_meta($_REQUEST['post_id'],'no_of_rooms',true);
	$total_adults = get_post_meta($_REQUEST['post_id'],'home_capacity',true);
	
	$total_guest_title = 'Total Guests';
	if(function_exists('icl_register_string')){
		icl_register_string(BKG_DOMAIN,$total_guest_title,$total_guest_title);
	}
	
	if(function_exists('icl_t')){
		$total_guest_title1 = icl_t(BKG_DOMAIN,$total_guest_title,$total_guest_title);
	}else{
		$total_guest_title1 = __($total_guest_title,BKG_DOMAIN); 
	}
	
	$adults_title = 'Adults not defined';
	if(function_exists('icl_register_string')){
		icl_register_string(BKG_DOMAIN,$adults_title,$adults_title);
	}
	
	if(function_exists('icl_t')){
		$adults_title1 = icl_t(BKG_DOMAIN,$adults_title,$adults_title);
	}else{
		$adults_title1 = __($adults_title,BKG_DOMAIN); 
	}
	
	$no_of_rooms_title = 'Number of rooms';
	if(function_exists('icl_register_string')){
		icl_register_string(BKG_DOMAIN,$no_of_rooms_title,$no_of_rooms_title);
	}
	
	if(function_exists('icl_t')){
		$no_of_rooms_title1 = icl_t(BKG_DOMAIN,$no_of_rooms_title,$no_of_rooms_title);
	}else{
		$no_of_rooms_title1 = __($no_of_rooms_title,BKG_DOMAIN); 
	}
	
	$no_room_availabel_title = 'No room available';
	if(function_exists('icl_register_string')){
		icl_register_string(BKG_DOMAIN,$no_room_availabel_title,$no_room_availabel_title);
	}
	
	if(function_exists('icl_t')){
		$no_room_availabel_title1 = icl_t(BKG_DOMAIN,$no_room_availabel_title,$no_room_availabel_title);
	}else{
		$no_room_availabel_title1 = __($no_room_availabel_title,BKG_DOMAIN); 
	}
	
	$html = '<div id="adults_rooms">';
	$html.='			<div class="form_row clearfix">
					<select name="home_capacity" id="home_capacity">
						<option value="">'.$total_guest_title1.'</option>';
						if(($total_adults) && $total_adults>0){
							for($k=1;$k<=$total_adults;$k++){
								$html.='<option value="'.$k.'">'.$k.'</option>';
							}
						}else{
							$html.='<option value="">'.$adults_title1.'</option>';
						}
	$html.='					
					</select>
				</div>';
			if($post_type=="room"){
				$html.= '			<div class="form_row clearfix" id="rooms_widget">
								<select name="no_of_rooms" id="no_of_rooms">
									<option value="">'.$no_of_rooms_title1.'</option>';
									if(($total_no_of_rooms) && $total_no_of_rooms>0){
										for($i=1;$i<=$total_no_of_rooms;$i++){
											$html.='<option value="'.$i.'">'.$i.'</option>';
										}
									}else{
										$html.='<option value="">'.$no_room_availabel_title1.'</option>';
									}
				$html.='		</select></div>';
			}
	
			$html.='</div>';
			
	echo $html;		
}

?>