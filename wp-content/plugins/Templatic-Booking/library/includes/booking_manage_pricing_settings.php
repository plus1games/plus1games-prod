<?php
global $wpdb,$wp_query;
$templatic_settings = get_option( "templatic_settings" );
//echo "<pre>";
//print_r($templatic_settings);
$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
$max_adults_allowed = explode(",",$GetPostMeta);

?>
<div class="templatic_settings">
	<form class="form_style" action="" method="post">
		<input type="hidden" style="height:40px;width:80px;" name="price_for_property_id" id="price_for_property_id" value=""/>
		<p class="description"><?php _e("Here you can manage prices/person for a day,week, and month . They will be calculated in booking section.",BKG_DOMAIN);?></p>
		<table style="width:100%">
			<tbody>
				
				<?php if(isset($_REQUEST['del_success']) && $_REQUEST['del_success']!="" && $_REQUEST['del_success']==1){?>
						<tr>
							<td colspan="2" align="left" style="font-weight:bold;color:green"><?php echo __(DEL_SEASONAL_SUCCESS_MESSAGE,BKG_DOMAIN);?></td>
						</tr>
				<?php }
					  if((isset($_REQUEST['ins_success']) && $_REQUEST['ins_success']!="") && (isset($_REQUEST['seasonal_flag']) && $_REQUEST['seasonal_flag']==1)){?>
						<tr>
							<td colspan="2" align="left" style="font-weight:bold;color:green"><?php echo __(ADD_SEASONAL_SUCCESS_MESSAGE,BKG_DOMAIN);?></td>
						</tr>
				<?php }
					  if((isset($_REQUEST['update_success']) && $_REQUEST['update_success']!="") && (isset($_REQUEST['seasonal_flag']) && $_REQUEST['seasonal_flag']==1)){?>
						<tr>
							<td colspan="2" align="left" style="font-weight:bold;color:green"><?php echo __(UPDATED_SEASONAL_SUCCESS_MESSAGE,BKG_DOMAIN);?></td>
						</tr>
				<?php }?>
				<tr>
					<td colspan="2" style="padding:0 10px;">
						<div id="seasonal_prices_div" >
							<span class="heading_seasonal"><?php echo '<b>';_e("Manage Seasonal Prices",BKG_DOMAIN);echo '</b>';?>
								<a href="<?php echo admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices&action=add_seasonal_price';?>" style="font-size:14px;" class="add-new-h2"><?php _e("Add new seasonal price",BKG_DOMAIN);?></a>
							</span>
							<table style="width:35%" class="widefat post">
								<thead>
									<tr>
										<th style="width:40px"><label for="id" class="form-textfield-label"><?php _e("Title",BKG_DOMAIN);?></label></th>
										<th><label for="seasonal_date" class="form-textfield-label"><?php _e("Seasonal Date",BKG_DOMAIN);?></label></th>
										<th style="width:50px"><label for="status" class="form-textfield-label"><?php _e("House/Room",BKG_DOMAIN);?></label></th>
										<th style="width:50px"><label for="status" class="form-textfield-label"><?php _e("Status",BKG_DOMAIN);?></label></th>
										<th style="width:70px"><label for="view_details" class="form-textfield-label"><?php _e("Actions",BKG_DOMAIN);?></label></th>
									</tr>
								</thead>
								<tbody>
									<?php 
										global $wpdb;
										$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
										$sql_seasonal_data = $wpdb->get_results("select * from $booking_seasonal_price_table");
										if(count($sql_seasonal_data)>0){
											$cnt=1;
											foreach($sql_seasonal_data as $seasonal_price_data){
												$prices_per_persons = unserialize($seasonal_price_data->prices_per_persons);
												$prices_per_day = $prices_per_persons['price_per_day'];
												$prices_per_week = $prices_per_persons['price_per_week'];
												$prices_per_month = $prices_per_persons['price_per_month'];
									?>
												<tr>
													<td><?php echo $seasonal_price_data->seasonal_title; ?></td>
													<td>
														<?php 
															_e(date_i18n(get_option('date_format'),strtotime($seasonal_price_data->from_date))." <b>To</b> ".date_i18n(get_option('date_format'),strtotime($seasonal_price_data->to_date)),BKG_DOMAIN); 
														?>
													</td>
													<td><?php echo  get_the_title($seasonal_price_data->set_price_for); ?></td>
													<td><?php echo (strtolower($seasonal_price_data->status) == strtolower('Enable')) ? __("Enable",BKG_DOMAIN) : __("Disable",BKG_DOMAIN); ?></td>
													<td>
														<a href="javascript:void(0);" name="view_details" id="view_details" onclick="view_seasonal_details(<?php echo $cnt; ?>)">
															<img src="<?php echo TEMPLATIC_BOOKING_URL.'/images/details.png';?>" title="View Details" alt="View Details"/>
														</a>
														| <a href="<?php echo admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices&action=add_seasonal_price&edit_id='.$seasonal_price_data->seasonal_prices_id;?>" name="edit_seasonal" id="edit_seasonal">
															<img src="<?php echo TEMPLATIC_BOOKING_URL.'/images/edit.png';?>" title="Edit Seasonal Price" alt="Edit Seasonal Price"/>
														  </a>
														| <a href="javascript:void(0);" onclick="delete_seasonal_price(<?php echo $seasonal_price_data->seasonal_prices_id;?>)" name="delete_seasonal" id="delete_seasonal">
															<img src="<?php echo TEMPLATIC_BOOKING_URL.'/images/reject.png';?>" title="Delete Seasonal Price" alt="Delete Seasonal Price"/>
														  </a>
													</td>
												</tr>
												<tr id="view_seasonal_tr_detail_<?php echo $cnt;?>">
													<td colspan="5" style="padding: 0px; border: medium none;">
														<div id="view_seasonal_in_detail_<?php echo $cnt;?>" style="display:none;padding: 5px 0;">
															<table width="100%" style="border:1px solid #DBD9D9">
																<tr>
																	<th width="20%"><?php _e("From Date:",BKG_DOMAIN);?></th>
																	<td width="80%"><?php _e(date_i18n(get_option('date_format'),strtotime($seasonal_price_data->from_date))); ?></td>
																</tr>
																<tr>
																	<th width="20%"><?php _e("To Date:",BKG_DOMAIN);?></th>
																	<td width="80%"><?php _e(date_i18n(get_option('date_format'),strtotime($seasonal_price_data->to_date))); ?></td>
																</tr>
																<tr>
																	<th width="20%"><?php _e("House/Room:",BKG_DOMAIN);?></th>
																	<td width="80%"><?php echo get_the_title($seasonal_price_data->set_price_for); ?></td>
																</tr>
																<tr>
																	<th width="20%"><?php _e("Status:",BKG_DOMAIN);?></th>
																	<td width="80%"><?php echo (strtolower($seasonal_price_data->status) == strtolower('enable')) ? __("Enable",BKG_DOMAIN) : __("Disable",BKG_DOMAIN); ?></td>
																</tr>
																<tr>
																	<th width="20%" style="border-bottom: 0px none;"><?php _e("Price(s)/Person(s):",BKG_DOMAIN);?></th>
																	<td width="80%" style="padding:0;">
																		<table cellpadding="6" cellspacing="0" style=";border: 1px solid #EEEEEE;border-bottom:0;">
																			<tr>
																				<td>&nbsp;</td>
																				<?php 
																					if(count($max_adults_allowed)>0){
																						for($i=1;$i<=count($max_adults_allowed);$i++){
																							echo "<td style='width:70px;max-width:70px'><b>$i</b></td>";
																						}
																					}
																				?>
																			</tr>
																			<tr>
																				<td>
																					<?php echo '<b><i>';_e("Prices/Day",BKG_DOMAIN);echo '</i></b>';?>
																				</td>
																				<?php 
																					if(count($max_adults_allowed)>0){
																						for($i=0;$i<count($max_adults_allowed);$i++){
																							$price_day = $prices_per_day[$i];
																							echo "<td style='width:70px;max-width:70px'>$price_day</td>";
																						}
																					}
																				?>
																			</tr>
																			<tr>
																				<td>
																					<?php echo '<b><i>';_e("Prices/Week",BKG_DOMAIN);echo '</i></b>';?>
																				</td>
																				<?php 
																					if(count($max_adults_allowed)>0){
																						for($i=0;$i<count($max_adults_allowed);$i++){
																							$price_week = $prices_per_week[$i];
																							echo "<td style='width:70px;max-width:70px'>$price_week</td>";
																						}
																					}
																				?>
																			</tr>
																			<tr style="border-bottom: 0px none;">
																				<td>
																					<?php echo '<b><i>';_e("Prices/Month",BKG_DOMAIN);echo '</i></b>';?>
																				</td>
																				<?php 
																					if(count($max_adults_allowed)>0){
																						for($i=0;$i<count($max_adults_allowed);$i++){
																							$price_month = $prices_per_month[$i];
																							echo "<td style='width:70px;max-width:70px'>$price_month</td>";
																						}
																					}
																				?>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
									<?php 	$cnt++;}
										}else{?>
											<tr>
												<td colspan="4"><?php _e("No seasonal prices created.",BKG_DOMAIN);?></td>
											</tr>
									<?php 
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			</tbody>	
		</table>
		
	</form>
</div>