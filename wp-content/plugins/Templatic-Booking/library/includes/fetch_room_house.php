<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");
$my_post_type = $_REQUEST['post_type'];
if(is_plugin_active('wpml-translation-management/plugin.php'))
{
	global $sitepress;
	$sitepress->switch_lang($_REQUEST['language']);
}
$templatic_settings = get_option( "templatic_settings" );
$args=array(
	'post_type'  => $my_post_type,	
	'posts_per_page' => -1,
	'post_status' => array('publish'),						
	'order' => 'ASC'	
);
	$posts_properties = new WP_Query($args);
	
	if($posts_properties){?>
		<th><label for="property_id"><?php _e("Select Room/House",BKG_DOMAIN);?></label></th>
		<td>
			<select name="property_id" id="property_id" onchange="load_property_posts(this.value);" > <?php //if($booking_post_type == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){echo "selected=selected";}?>
				<option value=""><?php _e("Please Select",BKG_DOMAIN);?></option>
				
<?php	
				while ($posts_properties->have_posts()): $posts_properties->the_post();$the_id = get_the_id();?>
					<option value="<?php echo $the_id;?>" <?php if($property_id == $the_id){echo "selected=selected";}?>><?php echo the_title();?></option>
		<?php   endwhile; wp_reset_query();
			echo '</select>';
		echo '</td>';
	}else{
		echo '<th><label for="property_id">';
				_e('Select Room/House',BKG_DOMAIN);
		echo '</label></th>
			<td>';
		echo '<select name="property_id" id="property_id"> 
				<option value="">';	
					_e('No room/house available',BKG_DOMAIN);
		  echo '</option>
			  </select>';
		echo "</td>";	
	}




?>