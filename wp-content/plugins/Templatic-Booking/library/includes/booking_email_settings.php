<?php
global $wpdb;
	$templatic_settings = get_option( "templatic_settings" );
	$booking_request_sent_subject = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_subject'];
	$booking_request_sent_msg = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_request_sent_msg'];
	$booking_confirm_success_sub = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_sub'];
	$booking_confirm_success_msg = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_msg'];
	$booking_reject_success_sub = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub'];
	$booking_reject_success_msg = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg'];
	$booking_cancel_success_sub = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_sub'];
	$booking_cancel_success_msg = @$templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_cancel_success_msg'];
?>
<div class="templatic_settings">
	<form class="form_style" action="" method="post">
		<table style="width:100%">
			<tbody>
				<tr>
					<td colspan="2"><h3><?php _e("Email Settings",BKG_DOMAIN);?></h3></td>
				</tr>
				<tr>
					<td colspan="2">
						<table style="width:60%" class="widefat post">
							<thead>
								<tr>
									<th><label for="email_type" class="form-textfield-label"><?php _e("Email Type",BKG_DOMAIN);?></label></th>
									<th><label for="email_sub" class="form-textfield-label"><?php _e("Email Subject",BKG_DOMAIN);?></label></th>
									<th><label for="email_desc" class="form-textfield-label"><?php _e("Email Description",BKG_DOMAIN);?></label></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<label class="form-textfield-label">
											<?php _e("Booking Request Sent Notification Email",BKG_DOMAIN); 
												echo '<br/><i><small>';_e('(This is the acknowledgement message that the user will receive once they submit the request.)',BKG_DOMAIN);echo "</small></i>";?>
											</label>
									</td>
									<td><textarea name="booking_request_sent_subject" style="width:350px; height:130px;"><?php if($booking_request_sent_subject!=""){echo stripslashes($booking_request_sent_subject);}else{echo __(stripslashes(BOOKING_REQUEST_SENT_SUBJECT),BKG_DOMAIN);}?></textarea></td>
									<td>
										<textarea name="booking_request_sent_msg" style="width:350px; height:130px;"><?php if($booking_request_sent_msg!=""){echo stripslashes($booking_request_sent_msg);}else{echo __(stripslashes(BOOKING_REQUEST_SENT_MSG),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
								<tr>
									<td>
										<label class="form-textfield-label">
											<?php _e("Booking Request Confirmed successfully",BKG_DOMAIN);  
											echo '<br/><i><small>';_e('(Specify the message which the user will receive after you accept his/her booking request.)',BKG_DOMAIN); echo "</small></i>";?>
										</label>
									</td>
									<td><textarea name="booking_confirm_success_sub" style="width:350px; height:130px;"><?php if($booking_confirm_success_sub!=""){echo stripslashes($booking_confirm_success_sub);}else{echo __(stripslashes(BOOKING_REQUEST_CONFIRM_SUCCESS_SUBJECT),BKG_DOMAIN);}?></textarea></td>
									<td>
										<textarea name="booking_confirm_success_msg" style="width:350px; height:130px;"><?php if($booking_confirm_success_msg!=""){echo stripslashes($booking_confirm_success_msg);}else{echo __(stripslashes(BOOKING_REQUEST_CONFIRM_SUCCESS_MSG),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
								<tr>
									<td>
										<label class="form-textfield-label">
											<?php _e("Booking Request Rejected",BKG_DOMAIN); 
												echo '<br/><i><small>';_e('(Specify the message which the user will receive if you cancel his/her booking request.)',BKG_DOMAIN);echo "</small></i>";?>
										</label>
									</td>
									<td><textarea name="booking_reject_success_sub" style="width:350px; height:130px;"><?php if($booking_reject_success_sub!=""){echo stripslashes($booking_reject_success_sub);}else{echo __(stripslashes(BOOKING_REQUEST_REJECT_SUBJECT),BKG_DOMAIN);}?></textarea></td>
									<td>
										<textarea name="booking_reject_success_msg" style="width:350px; height:130px;"><?php if($booking_reject_success_msg!=""){echo stripslashes($booking_reject_success_msg);}else{echo __(stripslashes(BOOKING_REQUEST_REJECT_MSG),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
								<tr>
									<td>
										<label class="form-textfield-label">
											<?php _e("Booking Request Cancelled",BKG_DOMAIN); 
											echo '<br/><i><small>';_e('(Specify the message which the user has cancel his/her booking request.)',BKG_DOMAIN);echo "</small></i>";?>
										</label>
									</td>
									<td><textarea name="booking_cancel_success_sub" style="width:350px; height:130px;"><?php if($booking_cancel_success_sub!=""){echo stripslashes($booking_cancel_success_sub);}else{echo stripslashes(BOOKING_CANCEL_MSG_MAIL);}?></textarea></td>
									<td>
										<textarea name="booking_cancel_success_msg" style="width:350px; height:130px;"><?php if($booking_cancel_success_msg!=""){echo stripslashes($booking_cancel_success_msg);}else{echo __(stripslashes(BOOK_PROCESS_CANCEL_MAIL),BKG_DOMAIN);}?></textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<?php 
							$legend_display = '<h3>'.__('Legends',BKG_DOMAIN).' : </h3>';
							$legend_display .= '<p style="line-height:30px;width:100%;"><label style="float:left;width:180px;">[USER_NAME]</label> : '.__('Name of the recipient.',BKG_DOMAIN).'<br />
												<label style="float:left;width:180px;">[CONTACT_MAIL]</label> : '.__('Email of the sender',BKG_DOMAIN).'<br />
												<label style="float:left;width:180px;">[CANCEL_URL]</label> : '.__('Cancel booking URL',BKG_DOMAIN).'<br />
												<label style="float:left;width:180px;">[BOOKING_DETAIL]</label> : '.__('Details of booking',BKG_DOMAIN).'<br />
												<label style="float:left;width:180px;">[ADMIN_NAME]</label> : '.__('Name of Administrator',BKG_DOMAIN).'<br />';
							echo $legend_display;
						?>
					</td>
				</tr>
			</tbody>	
		</table>
		<p class="submit" style="clear: both;">
		  <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save All Settings',BKG_DOMAIN);?>">
		  <input type="hidden" name="emails_submit" value="y">
		</p>
	</form>
</div>