<?php
global $wpdb;
$templatic_settings = get_option( "templatic_settings" );
$hotel_name = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
$contact_email = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_email'];
$hotel_street = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
$hotel_state = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
$hotel_country = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
$contact_phone = @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_phone'];
?>
<div class="templatic_settings">
	<form class="form_style" action="" method="post">
		<p class="description"><?php _e("Hotel information specified here is applicable to Rooms only",BKG_DOMAIN);?></p>
		<table class="form-table">
			<tbody>
				<tr>
					<td colspan="2"><h3><?php _e("Hotel Information",BKG_DOMAIN);?></h3></td>
				</tr>
				<tr>
					<th><label><?php _e("Hotel Name",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="hotel_name_id" class="input_wrap">
								<input type="text" name="hotel_name" class="input_select" id="hotel_name" value="<?php echo $hotel_name;?>"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Contact Email",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="contact_email_id" class="input_wrap">
								<input type="text" name="contact_email" class="input_select" id="contact_email" value="<?php echo $contact_email;?>"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Hotel Street",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="hotel_street_id" class="input_wrap">
								<input type="text" name="hotel_street" class="input_select" id="hotel_street" value="<?php echo $hotel_street;?>"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Hotel State",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="hotel_state_id" class="input_wrap">
								<input type="text" name="hotel_state" class="input_select" id="hotel_state" value="<?php echo $hotel_state;?>"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Hotel Country",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="hotel_country_id" class="input_wrap">
								<input type="text" name="hotel_country" class="input_select" id="hotel_country" value="<?php echo $hotel_country;?>"/>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th><label><?php _e("Contact Phone",BKG_DOMAIN);?></label></th>
					<td>
						<div class="element">
							<div id="contat_phone_id" class="input_wrap">
								<input type="text" name="contact_phone" class="input_select" id="contact_phone" value="<?php echo $contact_phone;?>"/>
							</div>
						</div>
					</td>
				</tr>
			</tbody>	
		</table>
		<p class="submit" style="clear: both;">
		  <input type="submit" name="Submit" class="button-primary" value="<?php _e('Save All Settings',BKG_DOMAIN);?>">
		  <input type="hidden" name="hotel_information_settings" value="y">
		</p>
	</form>
</div>