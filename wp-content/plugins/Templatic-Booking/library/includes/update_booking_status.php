<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");
global $wpdb;
$PostId = $_REQUEST['post_id'];
$UpdatedStatus = $_REQUEST['status'];
$PostStatus = get_post_status($PostId);
$Flag = 0;
if($PostStatus == "auto-draft"){
	$Flag = 1;
}else{
	update_post_meta($PostId,'status',$UpdatedStatus);
	//Update the status of booking availability table START.
		$booking_availability_table = $wpdb->prefix."booking_availability_check";
	 	$sql_availability_updates = $wpdb->query("UPDATE $booking_availability_table SET status='$UpdatedStatus' where booking_post_id=$PostId");
	//Update the status of booking availability table START.
	
	$post_data = get_post($PostId);
	$fromEmail = get_site_emailId_plugin();
	$fromEmailName = get_site_emailName_plugin();
	$toEmail = get_post_meta($PostId,'booking_email',true);
	$toEmailName = $post_data->post_title;
	$store_name = get_option('blogname');
	
	$page_name = "booking-cancel";
	$page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."'");
	$page_name_id = get_option('booking_system_cancel');
	if(strtolower($UpdatedStatus) == strtolower("Approved")){
		///////EMAIL START//////
		
			$templatic_settings = get_option('templatic_settings');
			$email_content = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_msg'];
			$email_subject = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_confirm_success_sub'];
			if(!$email_subject)
			{
				$email_subject = __('Booking-Confirmed',BKG_DOMAIN);	
			}
			if(!$email_content)
			{
				$email_content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking is confirmed. We hope you enjoy the stay at our Hotel.',BKG_DOMAIN).'</p>
								  <p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL]. '.__('We will be glad to assist you.',BKG_DOMAIN).'</p>
								  <p>'.__('If you want to cancel your booking request then click on link given below.',BKG_DOMAIN).'</p>
								  <p>[CANCEL_URL]<br />'.__('Thank you for staying at our Hotel.',BKG_DOMAIN).'</p>
								  <p><br />[ADMIN_NAME]</p>';
			}
			$cancel="<a href='".get_option('home')."/?page_id=$page_name_id&booking_id=".$PostId."'>".get_option('home')."/?page_id=$page_name_id&booking_id=".$PostId."</a>";
			$property_email = get_post_meta($PostId,'property_room_id',true);
			$BookingPostType = get_post_meta($PostId,'booking_post_type',true);
			$email_info = get_post_meta($property_email,'home_contact_mail',true);//Fetch email id from house's email meta information   
			$hotel_email_info = ($email_info) ? $email_info : @$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_email'];
		
			if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
				$hotel_name = get_the_title($property_email);
				$addresses = get_post_meta($property_email,'address',true);
				$address_exclude = explode(',',$addresses);
				$home_street = $address_exclude[0];
				$home_state = $address_exclude[1];
				$home_country = $address_exclude[2];
			}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
				$AllTemplaticSettings = get_option('templatic_settings');
				$hotel_name = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
				$home_street = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
				$home_state = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
				$home_country = @$AllTemplaticSettings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
			}
			
			$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
			if($home_country!=""){
				$regard_content .= ', '.$home_country;
			}
			$booking_detail = "<br/><br/>"; 
			$booking_detail .= __("Your Booking Details are as below:",BKG_DOMAIN); 
			$booking_detail .= "<br/><br/>"; 
			$booking_detail .= templatic_booking_mail_success_text_display($PostId);
			$regard_content .=  '<br />'.@$templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['contact_phone'];
			$serach_array = array('[USER_NAME]','[CANCEL_URL]', '[CONTACT_MAIL]','[ADMIN_NAME]','[BOOKING_DETAIL]');
			$replace_array = array($toEmailName,$cancel, '<i>'.$hotel_email_info.'</i>',$regard_content,$booking_detail);
			$mail_content = str_replace($serach_array,$replace_array,$email_content);
			
			templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
		//////EMAIL END////////
	}elseif(strtolower($UpdatedStatus) == strtolower("Cancelled")){
		///////EMAIL START//////
			$post_data = get_post($PostId);
			$templatic_settings = get_option('templatic_settings');
			$email_content = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_msg'];
			$email_subject = $templatic_settings['templatic_booking_system_settings']['templatic_booking_email_settings']['booking_reject_success_sub'];
			if(!$email_subject)
			{
				$email_subject = __('Booking - Could not confirm',BKG_DOMAIN);	
			}
			if(!$email_content)
			{
				$email_content = '<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking request is rejected. Please try again later.',BKG_DOMAIN).'<br /><br />'.__('Thanks for your interest.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>';
			}
			$property_id = get_post_meta($PostId,'property_room_id',true);
			$BookingPostType = get_post_meta($PostId,'booking_post_type',true);
			
			
			if($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE){
				$hotel_name = get_the_title($property_id);
				$addresses = get_post_meta($property_id,'address',true);
				$address_exclude = explode(',',$addresses);
				$home_street = $address_exclude[0];
				$home_state = $address_exclude[1];
				$home_country = $address_exclude[2];
			}elseif($BookingPostType == CUSTOM_POST_TYPE_TEMPLATIC_ROOM){
				$hotel_name = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_name'];
				$home_street = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_street'];
				$home_state = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_state'];
				$home_country = $templatic_settings["templatic_booking_system_settings"]['templatic_booking_hotel_information']['hotel_country'];
			}
			$regard_content = '<strong>'.$hotel_name.'</strong><br />'.$home_street.'<br />'.$home_state;
			if($home_country!=""){
				$regard_content .= ', '.$home_country;
			}
			$serach_array = array('[USER_NAME]', '[ADMIN_NAME]');
			$replace_array = array($toEmailName, $regard_content);
			$mail_content = str_replace($serach_array,$replace_array,$email_content);
			templ_send_email($fromEmail,$fromEmailName,$toEmail,$toEmailName,$email_subject,stripslashes($mail_content),$extra='');//to client email 
		//////EMAIL END////////
	}
}
echo $Flag;
?>