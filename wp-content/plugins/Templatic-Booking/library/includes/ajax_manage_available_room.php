<?php 
$file = dirname(__FILE__);
$file = substr($file,0,stripos($file, "wp-content"));
require($file . "/wp-load.php");
global $wpdb;

$property_id = $_REQUEST['property_id'];
$check_in_date = $_REQUEST['checkindate'];
$check_out_date = $_REQUEST['checkoutdate'];
$time_check_in_date = strtotime($_REQUEST['checkindate']);
$time_check_out_date = strtotime($_REQUEST['checkoutdate']);


$datediff = $time_check_out_date - $time_check_in_date;
$datediff = floor($datediff/(60*60*24));
$booking_availability_table = $wpdb->prefix."booking_availability_check";
$availability_chkout_date = date_i18n('Y-m-d',strtotime(date_i18n('Y-m-d', strtotime($check_out_date)) . "-1 day"));
$booked_no_of_rooms = 0;
$no_of_rooms = 0;
for($i=0;$i<$datediff ;$i++)
{
	$availability_chkin_date = date_i18n('Y-m-d',strtotime(date_i18n('Y-m-d', strtotime($check_in_date)) . "+$i day"));
	$result_availability = $wpdb->get_results("select availability_id,property_id,booking_post_id from $booking_availability_table where status='Approved' and property_id=$property_id and ((from_date between '".$availability_chkin_date."' and '".$availability_chkin_date."') and (to_date between '".$availability_chkin_date."' and '".$availability_chkin_date."') or (from_date <= '".$availability_chkin_date."' and to_date >= '".$availability_chkin_date."') or (from_date >= '".$availability_chkin_date."' and to_date <= '".$availability_chkin_date."') or (from_date >= '".$availability_chkin_date."' and from_date <= '".$availability_chkin_date."') or (to_date > '".$availability_chkin_date."' and to_date < '".$availability_chkin_date."'))");
	for($k=0;$k<count($result_availability);$k++)
	{
		$count_booked_no_of_rooms[$availability_chkin_date] += get_post_meta($result_availability[$k]->booking_post_id,'no_of_rooms',true);
	}
}


$no_of_rooms = get_post_meta($property_id,'no_of_rooms',true);

$booking_closing_table = $wpdb->prefix."booking_closing_check";

$result_closing = $wpdb->get_var("select closing_id from $booking_closing_table where property_id=$property_id and ((closing_start_date between '".$check_in_date."' and '".$availability_chkout_date."') or (closing_end_date between '".$check_in_date."' and '".$availability_chkout_date."') or (closing_start_date <= '".$check_in_date."' and closing_end_date >= '".$check_in_date."') or (closing_end_date >= '".$availability_chkout_date."' and closing_end_date <= '".$availability_chkout_date."') or ( '".$check_in_date."'  between closing_start_date and closing_end_date  ) or ( '".$availability_chkout_date."'  between closing_start_date and closing_end_date ))");

if($result_closing>0){
		echo $availability_flag = 'closed';exit;
	}

if(count($result_availability)>0){
	$booked_no_of_rooms = max($count_booked_no_of_rooms);
}
if(isset($_REQUEST['booking_post_type']) && $_REQUEST['booking_post_type'] == CUSTOM_POST_TYPE_TEMPLATIC_HOUSE)
{
	//if((count($result_availability)>0) || count($result_closing)>0)){
	if((count($result_availability)>0)){
		echo $room_available = "booked";exit;
	}else
	{
		echo $room_available = get_post_meta($property_id,'home_capacity',true);exit;
	}
}
elseif(count($result_availability) >0)
{
	$room_available = $no_of_rooms-($booked_no_of_rooms+$_REQUEST['no_of_rooms']);
	if($room_available >=0)
	{
		echo "allow";exit;
	}
	else
	{
		echo "closed";exit;
	}
}else
{
	echo $no_of_rooms;exit;
}
?>