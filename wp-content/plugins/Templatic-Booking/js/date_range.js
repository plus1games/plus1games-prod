var $date_picker = jQuery.noConflict();
$date_picker(document).ready(function() {
var from = new Date();
var to = new Date(from.getTime() + 200 * 60 * 60 * 24 * 14);
$date_picker('#datepicker-calendar').DatePicker({
	inline: true,
	calendars: 3,
	mode: 'range',
	current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
	onChange: function(dates,el) {jQuery("#booking_check_in_date_error").html('');
	  // update the range display
	  
	  if(dates[0] < from || dates[1] < from){
		$date_picker('#date-range-field span').text('Invalid date selection');
		$date_picker('#checkindate').val('');
		$date_picker('#checkoutdate').val('')
	  }else{
		$date_picker('#date-range-field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear()+' - '+
										dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
		$date_picker('#checkindate').val(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+dates[0].getFullYear());
		$date_picker('#checkoutdate').val(dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
	  }								
	 }
   });
   // initialize the special date dropdown field
   $date_picker('#date-range-field span').text('Select Check-In/Check-Out date');
   var current_td = $date_picker('tbody .datepickerDays td').hasClass('datepickerToday');
   if(current_td){
		//$date_picker('tbody .datepickerDays td').removeClass('datepickerFuture');
   }
   //datepickerFuture
   /*$date_picker('#date-range-field span').text(from.getDate()+' '+from.getMonthName(true)+', '+from.getFullYear()+' - '+
										to.getDate()+' '+to.getMonthName(true)+', '+to.getFullYear());*/
   
   $date_picker('#date-range-field').bind('click', function(){
	 $date_picker('#datepicker-calendar').toggle();
	 if($date_picker('#date-range-field a').text().charCodeAt(0) == 9660) {
	   // switch to up-arrow
	   $date_picker('#date-range-field a').html('&#9650;');
	   $date_picker('#date-range-field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
	   $date_picker('#date-range-field a').css({borderBottomRightRadius:0});
	 } else {
	   // switch to down-arrow
	   $date_picker('#date-range-field a').html('&#9660;');
	   $date_picker('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
	   $date_picker('#date-range-field a').css({borderBottomRightRadius:5});
	 }
	 return false;
   });
   $date_picker('html').click(function() {
	 if($date_picker('#datepicker-calendar').is(":visible")) {
	   $date_picker('#datepicker-calendar').hide();
	   $date_picker('#date-range-field a').html('&#9660;');
	   $date_picker('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
	   $date_picker('#date-range-field a').css({borderBottomRightRadius:5});
	 }
   });
   
   // stop the click propagation when clicking on the calendar element
   // so that we don't close it
   $date_picker('#datepicker-calendar').click(function(event){
	 event.stopPropagation();
   });
 });