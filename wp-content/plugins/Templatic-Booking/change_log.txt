Templatic Plugin - tevolution-booking  (version 1.1.1) 
========================================================================

25th April 2015 (Version 1.1.1)
------------------------------------------------------------------------
Fix - Show calendar only on single page or room and on house detail page.

Fix - Calendar url was wrong for backend for next and previous month.

Fix - Images were not getting uploaded.

Fix - Seasional price not updated.

Fix - It does not allow to book the room from front end or back end.

Fix - Change in the mail content for pending and cancel from tevolution transaction settings.

Fix - Closing date was not showing properly when querystring is not set.

Fix - Cancel booking was not proper.

Fix - Validation was not working for payment gateways.

Fix - Total Guests and Number of Rooms were showing blank options in custom field.

Fix - When a room is booked on certain dates and approved by admin, room can still be booked on same dates.

Fix - While booking from backend selecting post type with wpml its related room or house does not fetch.

Fix - User home_url() on booking submit form because booking preview page break on wordpress setup as subfolder.

Fix - Room available booking issue.

Fix - Booking calendar is not displaying correct closing dates on the house detail page. It is displaying the closing dates of other houses too.

Fix - Reset price issue.

Fix - Amount calculation issue after tax including.

Fix - WordPress 4.2 & Security related changes.
------------------------------------------------------------------------

29th April 2014 (Version 1.1.0)
--------------------------------------------------------------------
New Feature: Added a feature of minimum stay for seasonal days 
New Feature: Availability calender on room/house detail pages
--------------------------------------------------------------------
library/includes/booking_add_seasonal_price.php
templatic_booking.php
library/includes/calculate_cost.php
library/includes/shortcodes_booking_system.php
css/templatic_booking_styles.css
library/includes/booking_custom_fields_function.php
library/includes/booking_calendar.php
library/includes/booking_general_settings.php
templatic_booking_setting.php
library/booking_custom_post_type/rooms_data.php
--------------------------------------------------------------------

Fix: Check availability link was wrong when WPML is activated
--------------------------------------------------------------------
library/includes/shortcodes_booking_system.php
--------------------------------------------------------------------

Fix: Design issues with facebook share button
Fix: List and grid view not working on listing page
--------------------------------------------------------------------
css/templatic_booking_styles.css
templatic_booking.php
--------------------------------------------------------------------

Improvement: Fetched date format of WP general setting for tariff page
-----------------------------------------------------------------------
library/includes/shortcodes_booking_system.php
--------------------------------------------------------------------

Fix: Ajax related issues on some servers
--------------------------------------------------------------------
library/includes/templatic_booking_widget.php
library/includes/shortcodes_booking_system.php
--------------------------------------------------------------------

Improvement: Added WP nounce on booking cancel link for better security
-----------------------------------------------------------------------
library/includes/shortcodes_booking_system.php
library/includes/booking_paynow.php
-----------------------------------------------------------------------

Improvement: Fetch room information on booking form
-----------------------------------------------------------------------
library/includes/shortcodes_booking_system.php
css/templatic_booking_styles.css
library/includes/ajax_booking_information.php
--------------------------------------------------------------------

Fix: Background image was not showing on success page
Fix: Fetch service price as per Services charge daily or once
--------------------------------------------------------------------
library/includes/booking_custom_fields_function.php
--------------------------------------------------------------------

Fix: Sub cost total was not shown on success page and in mail
--------------------------------------------------------------------
templatic_booking.php
library/includes/booking_custom_fields_function.php
--------------------------------------------------------------------

Fix: Added "no image available"  image for listing page
--------------------------------------------------------------------
library/includes/booking_custom_fields_function.php
images/noimage-150x150.jpg
------------------------------------------------------------------------

Fix: Coupon code not working with start date set as future date of coupon code
------------------------------------------------------------------------------
library/includes/calculate_cost.php
library/includes/check_coupon_code.php
--------------------------------------------------------------------

Fix: Added transaction number when booking is free
--------------------------------------------------------------------
library/includes/booking_custom_fields_function.php
----------------------------------------------------------

Fix: Warning message shown while debug mode is on
----------------------------------------------------------
library/includes/calculate_cost.php
----------------------------------------------------------


7th March 2014 (Version 1.0.15)
--------------------------------------------------------------------
New Feature: Added an option to charge services daily or once.
--------------------------------------------------------------------
library/includes/booking_general_settings.php
library/includes/calculate_cost.php
templatic_booking_setting.php

-------------------------------------------------------------------------------
Fix: Seasonal price section - calendar field design according to WordPress 3.8 
-------------------------------------------------------------------------------
css/templatic_booking_styles.css
-----------------------------------------------------------------------

Fix: Default custom fields do not save after activate them from backend.
-----------------------------------------------------------------------
templatic_booking.php
---------------------------------------------------

Fix: Updated the list of countries
---------------------------------------------------
install.php
-------------------------------------------------------------------

Fix: Make submit booking form work as per ssl option of tevolution
-------------------------------------------------------------------
library/includes/shortcodes_booking_system.php
-----------------------------------------------------------

Fix: booking form was not working with shortcode plugin
-----------------------------------------------------------
library/includes/booking_custom_fields_function.php
-----------------------------------------------------------

Fix: Added common styling on Cancel booking page
-----------------------------------------------------------
css/templatic_booking_styles.css
--------------------------------------------------



29th January 2014 (Version 1.0.14)
-----------------------------------------------------------
Fix: Included CSV file download links
----------------------------------------------------
/library/includes/house_sample.csv
/library/includes/room_sample.csv
/templatic_booking.php
----------------------------------------------------------------------------------
Fix - fetch services only if particular room or house type is selected for services
------------------------------------------------------------------------------------
/library/includes/shortcodes_booking_system.php
------------------------------------------------------------------------------------
Fix: If closing day is before and after the available date, then booking form does
not allow to book the room for that available room
-----------------------------------------------------------------
/library/includes/ajax_manage_available_room.php
--------------------------------------------------------------
Fix: Page content was not showing properly on booking form
-----------------------------------------------------------
/library/includes/shortcodes_booking_system.php
--------------------------------------------------------------
4th December 2013 (Version 1.0.13)
-------------------------------------------------------
Improved : Enable default fields for detail page by default.
FIX : WPML custom field issue.
-------------------------------------------------------
install.php

-------------------------------------------------------------------
FIX : Room post type passed static in some places - made it dynamic.
-------------------------------------------------------------------
install.php
library/booking_custom_post_type/rooms_data.php

----------------------------------------
FIX : Warning error issue on booking page.
----------------------------------------
library/includes/sortcodes_booking_system.php



29th October 2013
----------------------------------------------------
Improved : Set default custom fields to inactive mode.
----------------------------------------------------
templatic_booking.php



28th October 2013 (Version 1.0.12)
----------------------------------------------
FIX : Removed rating from taxonomy listing page.
----------------------------------------------
templatic_booking.php

----------------------------------------------------
FIX : Added function to show excerpt for listing pages.
----------------------------------------------------
library/includes/booking_custom_fields_function.php

--------------------------
FIX : Booking design issue.
--------------------------
css/templatic_booking_styles.css



12th October 2013 (Version 1.0.11)
-----------------------------------------------------
FIX : Issue of wpml issue with booking calendar widget.
-----------------------------------------------------
library/includes/sortcodes_booking_system.php

-------------------------------------------
FIX : Issue of wpml to fetch room and house.
-------------------------------------------
library/includes/booking_custom_fields_function.php



3rd October 2013 (Version 1.0.10)
--------------------------------------------------------------------
FIX : Removed button class from Check availability link on Booking form.
--------------------------------------------------------------------
css/templatic_booking_styles.css
library/includes/sortcodes_booking_system.php

-----------------------------------------------------------------------------
FIX : Removed successfull text after success message on success booking page.
-----------------------------------------------------------------------------
library/includes/booking_custom_fields_function.php

--------------------------------
FIX : Stripslashes issue for string.
--------------------------------
library/includes/booking_custom_fields_function.php
library/includes/sortcodes_booking_system.php
library/includes/update_booking_status.php
templatic_booking.php

----------------------------------------------------
FIX :
Sharing options displaying 2 times on detail page.
Removed sharing buttons filter of tevolution.
----------------------------------------------------
templatic_booking.php
library/includes/booking_custom_fields_function.php

------------------------------------------------------------------------------------------------------------------------
FIX :
Tevolution activate for "user module" was getting overwrote with appointment plugin when deactivate the same module.
Price package should not here on add room, add house, add booking page(Its hidden now).
Removed sorting option and grid view list view option from booking plugin.
------------------------------------------------------------------------------------------------------------------------
templatic_booking.php

---------------------------------------------------------------------------------------------------------------------------------------
FIX : Remove the unnecessary column like price package status and expire date from transaction column, room and house post type columns.
---------------------------------------------------------------------------------------------------------------------------------------
library/booking_custom_post_type/custom_post_type.php

----------------------------------------
FIX : WPML booking widget redirect issue.
----------------------------------------
library/includes/templatic_booking_widget.php

-----------------------------------------------------------------------
FIX : Issue of Booking plugin custom field entry when wpml plugin activate.
-----------------------------------------------------------------------
install.php



2nd September (Version 1.0.9)
---------------------------------------------------------------------------------------------------------------------------------------
Renamed plugin folder to Templatic-Booking to avoid update conflicts with another plugin with the same name on WordPress plugin repository.
---------------------------------------------------------------------------------------------------------------------------------------
bundle_box.php
install.php
templatic_booking.php
templatic_booking_system_main.php
templatic_login.php
wp-updates-plugin.php

---------------------------------------------------------------------------------------
FIX : Booking form was not working when we activate shortcode plugin with latest updates.
---------------------------------------------------------------------------------------
library/includes/sortcodes_booking_system.php

-----------------------------------------------------------------------------------------------------
FIX : When latest Wordpress SEO (version 1.4.14) plugin is activated the Booking form page gets break.
-----------------------------------------------------------------------------------------------------
library/includes/sortcodes_booking_system.php

------------------------------------------------------------------------
Removed a theme.css as default calender is already designed in tevolution.
------------------------------------------------------------------------
Deleted file :
css/theme.css



5th August 2013 (Version 1.0.8)
---------------------------------------------------
FIX : Administrator can view the booking detail page.
---------------------------------------------------
templatic_booking.php

---------------------------------------------------------------------------------
FIX : Post permalink issue when we first time create new site with custom post type.
---------------------------------------------------------------------------------
library/booking_custom_post_type/rooms_data.php

--------------------------------------------------------
FIX : Add button class to book now button on detail page.
--------------------------------------------------------
library/includes/booking_custom_fields_function.php

-------------------------------------------------
FIX : Removed description from booking form page.
-------------------------------------------------
install.php

------------------------------------------------
FIX : Listing margin-right removed for design issue.
------------------------------------------------
css/templatic_booking_styles.css



20th July 2013 (Version 1.0.7)
----------------------------------------
FIX :
Added booking widget compatible css code.
Removed single line of booking form.
Issue of table design.
----------------------------------------
css/templatic_booking_styles.css

----------------------------------------------------------------------------------------------------------------------------
FIX :
Tariff page shows zero for the other remaining guests if a room having less no of guests than the total given in "Total Guest" field.
Booking form was not displaying date in date field If date formate selected from wordpress general settings.
Hide bottom line from tariff row if total guests is not selected.
----------------------------------------------------------------------------------------------------------------------------
library/includes/sortcodes_booking_system.php

------------------------------------------------------------------------------------------
FIX : [BOOKING_DETAIL] shortcode was not working when we approved booking from backend.
------------------------------------------------------------------------------------------
library/includes/update_booking_status.php



18th June 2013 (Version 1.0.6)
------------------------------------------------------------------
FIX : Booking detail and services are displayed to unauthorized users.
------------------------------------------------------------------
library/includes/booking_custom_fields_function.php
templatic_booking.php

-----------------------------------------------
FIX : Tariff price issue for multiple seasonal price.
-----------------------------------------------
library/includes/ajax_booking_information.php

--------------------------------------------------------------------------------------------
FIX :
Dates are not getting updated when we set closing days for any room and then remove it.
Added url in booking plugin on data insertion.
Autoinstall not working in Test site.
--------------------------------------------------------------------------------------------
templatic_booking.php

---------------------------------------------------------
FIX :
Changed sample data message according to booking plugin.
Change in insert sample data button text and css.
---------------------------------------------------------
templatic_booking_setting.php



6th June 2013 (Version 1.0.5)
-----------------------------------------------------------------------
Issue of showing multiple seasonal price in tariff on booking form - Solved.
-----------------------------------------------------------------------
library/includes/ajax_booking_information.php

--------------------------
UI related changes - Done.
--------------------------
templatic_booking.php
install.php
library/booking_custom_post_type/custom_post_type_lang.php
library/includes/sortcodes_booking_system.php

------------------------------------------------------------------------------------
Issue of room and house entry was not getting inserted after plugin activation - Solved.
------------------------------------------------------------------------------------
templatic_booking.php
templatic_booking_setting.php

-------------------------------------------
Issue of tariff page content styling - Solved.
-------------------------------------------
css/templatic_booking_styles.css

---------------------------------
Installation related issue - Solved.
---------------------------------
install.php



29th May 2013 (Vesion 1.0.4)
----------------------------------------------------------------------
Wrong calculated prices are displayed on booking success page - Solved.
----------------------------------------------------------------------
library/includes/booking_custom_fields_function.php
library/includes/calculate_cost.php
library/includes/check_coupon_code.php
library/includes/sortcodes_booking_system.php
templatic_booking.php

-------------------------------------
Issue of successful page title - Solved.
Auto install Redirection issue - Solved.
loading much on activation - Solved.
-------------------------------------
templatic_booking.php

------------------------------------------------------------
Add action to show custom fields and book now button - Done.
Show services price on successful page - Solved.
------------------------------------------------------------
library/includes/booking_custom_fields_function.php

-------------------------------------------
Validation message for same date - Changed.
-------------------------------------------
library/includes/sortcodes_booking_system.php
library/includes/templatic_booking_widget.php
templatic_booking.php

-------------------------------------
Plugin section load very slow - Solved.
-------------------------------------
wp-updates-plugin.php

------------------------------------------
Remove js showing in page source - Solved.
------------------------------------------
js/fullcalendar.min.js
templatic_booking.php

---------------------------------------
Validation not working properly - Solved.
Decimal issue in total price - Solved.
---------------------------------------
library/includes/sortcodes_booking_system.php

---------------------------------------
Show twice send inquiry button - Solved.
---------------------------------------
library/includes/booking_custom_fields_function.php




24th April 2013 (Version 1.0.3)
-------------------------------------------
Added new features - (Whole folder of plugin)
-------------------------------------------
Multiple closing days for Rooms and Houses - Done.
Tariff content on booking form - Done.
Added auto updated script - Done.
Minimum stays in houses - Done.
Tariff page template - Done.

--------------------------------------------------------------
Issue of calculating service price per day and per room - Solved.
--------------------------------------------------------------
library/includes/calculate_cost.php
templatic_booking.php

------------------------------------------
Issue of room display in drop down - Solved.
------------------------------------------
library/includes/booking_add_seasonal_price.php
library/includes/booking_calendar.php
library/includes/sortcodes_booking_system.php
library/includes/templatic_booking_widget.php

------------------------------------------------------------------------------
Coupon code does not save accurate value for coupon type percentage - Solved.
------------------------------------------------------------------------------
templatic_booking.php

---------------------------------------------------------
Show currency symbol for discount coupon in mail - Solved.
---------------------------------------------------------
library/includes/booking_custom_fields_function.php

-------------------------------
Send room type in mail - Solved.
-------------------------------
templatic_booking.php

------------------------------------------------------------------------------------------------
Do not show the comment box in booking form, booking-calendar and booking cancel pages - Solved.
------------------------------------------------------------------------------------------------
install.php

-----------------------------------------------------------------
Issue of wrong room booking validation for no room booked - Solved.
-----------------------------------------------------------------
library/includes/calculate_cost.php

---------------------------------------
Backend validation not working - Solved.
---------------------------------------
library/includes/ajax_manage_available_room.php
templatic_booking.php

------------------------------------------------
Show seasonal price title while editing it - Solved.
------------------------------------------------
library/includes/booking_add_seasonal_price.php

------------------------------------------------
Seasonal days count showing total days - Solved.
------------------------------------------------
library/includes/ajax_booking_information.php

------------------------------------------------------------------------------------------------------------
T > Listing post widget(Home Page) featured image and first image functionality not working alternately - Solved.
------------------------------------------------------------------------------------------------------------
library/booking_custom_post_type/rooms_data.php

---------------------------------------------------------------
Changed the position of tax on booking successful page - Solved.
---------------------------------------------------------------
library/includes/booking_custom_fields_function.php

----------------------------------
Issue of backend calendar - Solved.
----------------------------------
library/includes/booking_calendar.php

--------------------------------------------------------------------------------------
Redirection on themes.php and automatic autoinstall on booking plugin activation - Solved.
--------------------------------------------------------------------------------------
templatic_booking.php

-------------------------------------------------
Show original room price on success page - Solved.
-------------------------------------------------
library/includes/booking_custom_fields_function.php
templatic_booking.php

-----------------------------
Issue of Localization - Solved.
-----------------------------
library/includes/booking_custom_fields_function.php
library/includes/sortcodes_booking_system.php
templatic_booking.php
language/en_US.mo
language/en_US.po

--------------------------------------
wpml related translation issue - Solved.
--------------------------------------
library/includes/ajax_booking_information.php
library/includes/sortcodes_booking_system.php
library/includes/templatic_booking_widget.php

-----------------------------------------------
Issue of booking price on success page - Solved.
-----------------------------------------------
library/includes/booking_custom_fields_function.php
templatic_booking.php




3rd April 2013 (version 1.0.2)
----------------------------------------------------------
Deleted old payment method from the booking form - Solved.
----------------------------------------------------------
templatic_booking.php

----------------------------------------
Issue of booking calendar month - Solved.
----------------------------------------
library/includes/booking_calendar.php

------------------------------------------------------------------------------
Move deposit percentage field from manage price to general setting tab - Solved.
Description message changed in deposit amount - Solved.
------------------------------------------------------------------------------
library/includes/booking_manage_pricing_settings.php
library/includes/booking_general_settings.php
templatic_booking_setting.php

----------------------------------------------
Issue add domain name for localization - Solved.
----------------------------------------------
library/includes/sortcodes_booking_system.php

---------------------------------------------------------------------
Issue of booking email applied filters to fetch user email for hotel booking
---------------------------------------------------------------------
library/includes/booking_custom_fields_function.php

--------------------------------
Issue of seasonal prices - Solved.
--------------------------------
library/includes/calculate_cost.php

---------------------------------------------
Issue of booking cancel mail message - Solved.
---------------------------------------------
templatic_booking_setting.php

---------------------------------------------------------------------
Issue of on MAC system (site was getting blank page on MAC) - Solved.
---------------------------------------------------------------------
templatic_booking.php



21st March 2013 (version 1.0.1)
-----------------------------------------------------
Feature - Added an option to charge only Deposit amount when someone books a room/house
Improvement - Instead of page templates, create shortcodes for booking pages
Fix - Fixed some validation errors on booking form

