<?php
$settings = get_option( "templatic_settings" );

if(is_admin() && isset($_REQUEST['page']) && "templatic_booking_system" == $_REQUEST['page']){
	if($_REQUEST){
		global $wpdb;
		$settings = get_option( "templatic_settings" );
		if(isset($_REQUEST['general_submit']) && "y" == $_REQUEST['general_submit']){
			$tax_amount = @$_REQUEST['tax_amount'];
			$tax_type = ($_REQUEST['tax_type']) ? $_REQUEST['tax_type'] : "percent";
			
			$booking_form_page = (@$_REQUEST['booking_form_page']) ? $_REQUEST['booking_form_page'] : '';
			$booking_cancel_page = (@$_REQUEST['booking_cancel_page']) ? $_REQUEST['booking_cancel_page'] : '';
			$booking_calendar_page = (@$_REQUEST['booking_calendar_page']) ? $_REQUEST['booking_calendar_page'] : '';
			$service_type = (@$_REQUEST['service_type']) ? $_REQUEST['service_type'] : '';
			$detail_show_cal = (@$_REQUEST['detail_show_cal']) ? $_REQUEST['detail_show_cal'] : '';
			$general_settings_array = array();
			$general_settings_array = array(
											  "tax_amount" => $tax_amount,
											  "tax_type" => $tax_type,
											  "booking_form_page" =>$booking_form_page,
											  "booking_cancel_page" =>$booking_cancel_page,
											  "booking_calendar_page" =>$booking_calendar_page,
										  );
			$settings["templatic_booking_system_settings"]['templatic_booking_general_settings'] = $general_settings_array;
			update_option('templatic_settings',$settings);
			update_option('deposite_percentage',$_REQUEST['deposite_percentage']);
			update_option('service_type',$service_type);
			update_option('detail_show_cal',$detail_show_cal);//save the option to show booking calendar on detail page.
		}
		if(isset($_REQUEST['notification_submit']) && "y" == $_REQUEST['notification_submit']){
			$templatic_booking_success_message = ($_REQUEST['templatic_booking_success_message']) ? $_REQUEST['templatic_booking_success_message'] : BOOKING_SUCCESS_MSG;
			$prebank_message = ($_REQUEST['prebank_message']) ? $_REQUEST['prebank_message'] : PRE_BANK_PAYMENT_MSG;
			$notification_settings_array = array();
			$notification_settings_array = array("templatic_booking_success_subject" => $templatic_booking_success_subject,
										    "templatic_booking_success_message" => $templatic_booking_success_message,
										    "pay_on_arrival_subject" => $pay_on_arrival_subject,
										    "pay_on_arrival_message" => $pay_on_arrival_message,
										    "no_availability_subject" => $no_availability_subject,
										    "prebank_message" => $prebank_message,
										  );
			$settings["templatic_booking_system_settings"]['templatic_booking_notification_settings'] = $notification_settings_array;
			update_option('templatic_settings',$settings);
		}
		if(isset($_REQUEST['emails_submit']) && "y" == $_REQUEST['emails_submit']){
			$booking_request_sent_subject = ($_REQUEST['booking_request_sent_subject']) ? @$_REQUEST['booking_request_sent_subject'] : BOOKING_REQUEST_SENT_SUBJECT;
			$booking_request_sent_msg = ($_REQUEST['booking_request_sent_msg']) ? @$_REQUEST['booking_request_sent_msg'] : BOOKING_REQUEST_SENT_MSG;
			$booking_confirm_success_sub = ($_REQUEST['booking_confirm_success_sub']) ? @$_REQUEST['booking_confirm_success_sub'] : BOOKING_REQUEST_CONFIRM_SUCCESS_SUBJECT;
			$booking_confirm_success_sub = ($_REQUEST['booking_confirm_success_sub']) ? @$_REQUEST['booking_confirm_success_sub'] : BOOKING_REQUEST_CONFIRM_SUCCESS_SUBJECT;
			$booking_cancel_success_sub = ($_REQUEST['booking_cancel_success_sub']) ? @$_REQUEST['booking_cancel_success_sub'] : BOOKING_CANCEL_MSG_MAIL;
			$booking_confirm_success_msg = ($_REQUEST['booking_confirm_success_msg']) ? @$_REQUEST['booking_confirm_success_msg'] : BOOKING_REQUEST_CONFIRM_SUCCESS_MSG;
			$booking_reject_success_sub = ($_REQUEST['booking_reject_success_sub']) ? @$_REQUEST['booking_reject_success_sub'] : BOOKING_REQUEST_REJECT_SUBJECT;
			$booking_reject_success_msg = ($_REQUEST['booking_reject_success_msg']) ? @$_REQUEST['booking_reject_success_msg'] : BOOKING_REQUEST_REJECT_MSG;
			$booking_cancel_success_msg = ($_REQUEST['booking_cancel_success_msg']) ? @$_REQUEST['booking_cancel_success_msg'] : BOOK_PROCESS_CANCEL_MAIL;
			$email_settings_array = array();
			$email_settings_array = array("booking_request_sent_subject" => $booking_request_sent_subject,
										    "booking_request_sent_msg" => $booking_request_sent_msg,
										    "booking_confirm_success_sub" => $booking_confirm_success_sub,
										    "booking_confirm_success_msg" => $booking_confirm_success_msg,
										    "booking_reject_success_sub" => $booking_reject_success_sub,
										    "booking_reject_success_msg" => $booking_reject_success_msg,
											"booking_cancel_success_sub" => $booking_cancel_success_sub,
											"booking_cancel_success_msg"=> $booking_cancel_success_msg
										  );
			$settings["templatic_booking_system_settings"]['templatic_booking_email_settings'] = $email_settings_array;
			update_option('templatic_settings',$settings);
		}
		if(isset($_REQUEST['manage_price_submit']) && "y" == $_REQUEST['manage_price_submit']){
			$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
			$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
			$max_guests = explode(",",$GetPostMeta);
			$manage_prices_settings_array = array();
			$price_for_property_id = $_REQUEST["price_for_property_id"];
			for($j=1;$j<=count($max_guests);$j++){
				$price_per_day = ($_REQUEST["price_day_$j"]) ? $_REQUEST["price_day_$j"] : 0;
				$price_per_week = ($_REQUEST["price_week_$j"]) ? $_REQUEST["price_week_$j"] : 0;
				$price_per_month = ($_REQUEST["price_month_$j"]) ? $_REQUEST["price_month_$j"] : 0;
				
				$manage_prices_settings_array["price_guests_$j"] = array("price_per_day" => $price_per_day,
													  "price_per_week" => $price_per_week,
													  "price_per_month" => $price_per_month,
														);
			}
			$settings["templatic_booking_system_settings"]['templatic_booking_manage_prices_settings']["price_for_$price_for_property_id"] = $manage_prices_settings_array;
			update_option('templatic_settings',$settings);
		}
		
		if(isset($_REQUEST['seasonal_price_submit']) && "y" == $_REQUEST['seasonal_price_submit']){
			$sql_custom_max_field = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE $wpdb->posts.post_name = 'home_capacity' and $wpdb->posts.post_type = 'custom_fields'");
			$GetPostMeta = get_post_meta($sql_custom_max_field,'option_values',true);
			$max_guests = explode(",",$GetPostMeta);
			$seasonal_from_date = date('Y-m-d',strtotime($_REQUEST['booking_frmdate']));
			$seasonal_to_date = date('Y-m-d',strtotime($_REQUEST['booking_todate']));
			$seasonal_status = $_REQUEST['seasonal_status'];
			$set_price_for = $_REQUEST['set_price_for'];
			$seasonal_title = $_REQUEST['seasonal_title'];
			$seasonal_minimum_stay = $_REQUEST['seasonal_minimum_stay'];
			$flag = 0;
			
			$persons_n_prices = array();
			$price_per_day_seasonal = array();
			$price_per_week_seasonal = array();
			$price_per_month_seasonal = array();
			
			for($j=1;$j<=count($max_guests);$j++){
				$price_per_day_seasonal[] = ($_REQUEST["price_day_seasonal_$j"]) ? $_REQUEST["price_day_seasonal_$j"] : 0;
				$price_per_week_seasonal[] = ($_REQUEST["price_week_seasonal_$j"]) ? $_REQUEST["price_week_seasonal_$j"] : 0;
				$price_per_month_seasonal[] = ($_REQUEST["price_month_seasonal_$j"]) ? $_REQUEST["price_month_seasonal_$j"] : 0;
			}
			$persons_n_prices = array(
									'price_per_day' => $price_per_day_seasonal,
									'price_per_week' => $price_per_week_seasonal,
									'price_per_month' => $price_per_month_seasonal,
								);
			$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
			if(isset($_REQUEST['edit_seasonal_id']) && $_REQUEST['edit_seasonal_id']!=""){
				if($wpdb->query("update $booking_seasonal_price_table set seasonal_title = '".$seasonal_title."' , from_date='".$seasonal_from_date."', set_price_for=".$set_price_for." , seasonal_minimum_stay = '".$seasonal_minimum_stay."' , to_date='".$seasonal_to_date."', prices_per_persons='".serialize($persons_n_prices)."', status='".$seasonal_status."' where seasonal_prices_id=".$_REQUEST['edit_seasonal_id'])){
					$flag = 1;
				}else{
					$flag = 0;
				}
				echo '<form name="updated_success" action="'.admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices" method="post">
						<input type="hidden" name="update_success" value="1"/>
						<input type="hidden" name="seasonal_flag" value="'.$flag.'"/>
					  </form>
					  <script type="text/javascript">document.updated_success.submit();</script>';
			}else{
				
				if($wpdb->query("insert into $booking_seasonal_price_table set  seasonal_title = '".$seasonal_title."' , from_date='".$seasonal_from_date."', to_date='".$seasonal_to_date."', prices_per_persons='".serialize($persons_n_prices)."',set_price_for=".$set_price_for.",seasonal_minimum_stay = '".$seasonal_minimum_stay."', status='".$seasonal_status."'")){
					$flag = 1;
				}else{
					$flag = 0;
				}
				echo '<form name="inserted_success" action="'.admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices" method="post">
						<input type="hidden" name="ins_success" value="1"/>
						<input type="hidden" name="seasonal_flag" value="'.$flag.'"/>
					  </form>
					  <script type="text/javascript">document.inserted_success.submit();</script>';
			}
		}
		if(isset($_REQUEST['del_id']) && $_REQUEST['del_id']!=""){
			$id=$_REQUEST['del_id'];
			$booking_seasonal_price_table = $wpdb->prefix."booking_seasonal_price_table";
			$sql_del = $wpdb->query("delete from $booking_seasonal_price_table where seasonal_prices_id=$id");
			echo '<form name="del_succeed" action="'.admin_url().'admin.php?page=templatic_booking_system&tab=manage_prices" method="post">
					<input type="hidden" name="del_success" value="1"/>
				  </form>
				  <script type="text/javascript">document.del_succeed.submit();</script>';
		}
		if(isset($_REQUEST['hotel_information_settings']) && "y" == $_REQUEST['hotel_information_settings']){
			$hotel_name = ($_REQUEST['hotel_name']) ? $_REQUEST['hotel_name'] : '';
			$contact_email = ($_REQUEST['contact_email']) ? $_REQUEST['contact_email'] : '';
			$hotel_street = ($_REQUEST['hotel_street']) ? $_REQUEST['hotel_street'] : '';
			$hotel_state = ($_REQUEST['hotel_state']) ? $_REQUEST['hotel_state'] : '';
			$hotel_country = ($_REQUEST['hotel_country']) ? $_REQUEST['hotel_country'] : '';
			$contact_phone = ($_REQUEST['contact_phone']) ? $_REQUEST['contact_phone'] : '';
			$hotel_settings_array = array();
			$hotel_settings_array = array("hotel_name" => $hotel_name,
										    "contact_email" => $contact_email,
										    "hotel_street" => $hotel_street,
											"hotel_state" => $hotel_state,
											"hotel_country" => $hotel_country,
										    "contact_phone" => $contact_phone,
										  );
			$settings["templatic_booking_system_settings"]['templatic_booking_hotel_information'] = @$hotel_settings_array;
			update_option('templatic_settings',$settings);
		}
	}
}
/*
 * Call the templatic_booking_system_setting_admin_tabs function for display the appointment setting page tabs
 */
templatic_booking_system_setting_admin_tabs(isset($_GET['tab'])?$_GET['tab']:'hotel_information');

if ( $_GET['page'] == 'templatic_booking_system' ) :
    if ( isset ( $_GET['tab'] ) ) :
        $tab = $_GET['tab'];
    else:
        $tab = 'hotel_information';
    endif;
	switch ( $tab ) :       
		case 'hotel_information' :
			templatic_booking_system_hotel_information_settings();
			break;        
		case 'notification_settings' :
			templatic_booking_system_notification_settings();
			break;
		case 'email_settings' :
			templatic_booking_system_email_settings();
			break;
		case 'general_settings' :
			templatic_booking_system_general_settings();	
			break;		
		case 'manage_prices' :
			if(isset($_REQUEST['action']) && $_REQUEST['action']=="add_seasonal_price"){
				require_once TEMPLATIC_BOOKING_PATH.'/library/includes/booking_add_seasonal_price.php';
			}else{
				templatic_booking_system_manage_pricing();
			}	
			break;	
		case 'booking_calendar' :
			templatic_booking_system_booking_calander();
			break;
	endswitch;
endif;

/*
 * Function Name: templatic_booking_system_setting_admin_tabs
 * Return: Display Appointment Setting tab;
 */
function templatic_booking_system_setting_admin_tabs( $current = 'hotel_information' ) {
    $tabs = array( 				
				'hotel_information'=>__('Hotel Information',BKG_DOMAIN),
				'notification_settings'=>__('Notification Settings',BKG_DOMAIN),
				'email_settings'=>__('Emails Settings',BKG_DOMAIN),
				'general_settings' => __('General Settings',BKG_DOMAIN),
				'manage_prices'=>__('Manage Prices',BKG_DOMAIN),
				'booking_calendar'=>__('Booking calendar',BKG_DOMAIN),
				);
    $links = array();
	if($current=="")
		$current='hotel_information';
		
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' id='".$tab."_pointer' href='?page=templatic_booking_system&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' id='".$tab."_pointer' href='?page=templatic_booking_system&tab=$tab'>$name</a>";
        endif;
    endforeach;
	if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'templatic_booking_system'  && get_option('insert_dummy_data') != "YES")
	{
		echo "<div class='updated'><p><b>".__('Would you like to auto populate your site with Sample Rooms and Houses?',BKG_DOMAIN)." </b> <a class='button-primary' style=' background-image: linear-gradient(to bottom, green, green);' href='".site_url("/wp-admin/admin.php?page=templatic_booking_system&amp;insert_data=1")."'> ".__('Yes,insert please!',BKG_DOMAIN)."</a></p></div>";
	}
    echo '<h3 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';
}

function templatic_booking_system_general_settings(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_general_settings.php");
}
function templatic_booking_system_notification_settings(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_notification_settings.php");
}
function templatic_booking_system_email_settings(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_email_settings.php");
}
function templatic_booking_system_hotel_information_settings(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_hotel_information_settings.php");
}
function templatic_booking_system_manage_pricing(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_manage_pricing_settings.php");
}
function templatic_booking_system_booking_calander(){
	require_once(TEMPLATIC_BOOKING_PATH."/library/includes/booking_calendar.php");
}

?>