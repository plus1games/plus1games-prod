<?php
define("BOOKING_SUCCESS_MSG",'<p>'.__('Congratulations, your booking request is confirmed.',BKG_DOMAIN).'</p><p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL] '.__('by quoting the transaction number. We will be glad to assist you.',BKG_DOMAIN).'</p>');
define("PAY_CASH_ON_ARRIVAL_MESSAGE",'<p>'.__('Please pay the booking amount on arrival at our reception desk.',BKG_DOMAIN).'</p><p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL] '.__('by  quoting this transaction number. We will be glad to assist you.',BKG_DOMAIN).'</p>');
define("NO_AVAILABILITY_MESSAGE",'<p>'.__("Due to unavailability, your booking request could not be registered on the dates you specified.",BKG_DOMAIN).'</p><p>'.__('Please try again at a later time',BKG_DOMAIN).'</p>');
define("BOOKING_SUCCESS_SUBJECT",__("Booking Success Message",BKG_DOMAIN));
define("PAY_CASH_ON_ARRIVAL_SUBJECT",__("Pay Cash on Arrival Message",BKG_DOMAIN));
define("NO_AVAILABILITY_SUBJECT",__("No Availability Message",BKG_DOMAIN));
define("BOOKING_REQUEST_SENT_SUBJECT",__("Booking- Email Confirmation",BKG_DOMAIN));
define("BOOKING_REQUEST_CONFIRM_SUCCESS_SUBJECT",__("Booking-Confirmed",BKG_DOMAIN));
define("BOOKING_REQUEST_REJECT_SUBJECT",__("Booking- Could not confirm",BKG_DOMAIN));
define("BOOKING_REQUEST_SENT_MSG",'<p>'.__("Hi",BKG_DOMAIN).' [USER_NAME]<br />'.__("We'd like to inform you that your booking request has been successfully received. Expect another email from us when/if your booking gets confirmed.",BKG_DOMAIN).'</p>
<p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL]. '.__('We will be glad to assist you.',BKG_DOMAIN).'</p>
<p>[BOOKING_DETAIL]</p>
<p>'.__('If you want to cancel your booking request then click on link given below.',BKG_DOMAIN).'<br />[CANCEL_URL]</p>
<p>'.__('Thank you for staying.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>');
define("BOOKING_REQUEST_CONFIRM_SUCCESS_MSG",'BKG_DOMAIN'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('Your booking is confirmed. We hope you enjoy the stay.',BKG_DOMAIN).'</p>
<p>'.__('In case you have any queries, please email us at',BKG_DOMAIN).' [CONTACT_MAIL]. '.__('We will be glad to assist you.',BKG_DOMAIN).'</p>
<p>'.__('If you want to cancel your booking request then click on link given below.',BKG_DOMAIN).'</p>
<p>[CANCEL_URL]<br />'.__('Thank you for staying.',BKG_DOMAIN),'</p>
<p><br />[ADMIN_NAME]</p>');
define("BOOKING_REQUEST_REJECT_MSG",'<p>'.__('Hi',BKG_DOMAIN),' [USER_NAME],<br />'.__('Your booking request is rejected. Please try again later.',BKG_DOMAIN).'<br /><br />'.__('Thanks for your interest.',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>');
define('BOOKING_CANCEL_MSG_MAIL',__('Your Booking is Cancelled',BKG_DOMAIN));
define('BOOK_PROCESS_CANCEL_MAIL','<p>'.__('Hi',BKG_DOMAIN).' [USER_NAME],<br />'.__('You successfully cancelled your booking. To book another date feel free to visit our site at any time.',BKG_DOMAIN).'<br /><br />'.__('Kind regards',BKG_DOMAIN).'<br />[ADMIN_NAME]</p>');
define("BOOK_NOW_BUTTON_TEXT",__("Book Now",BKG_DOMAIN));
define("DEL_SEASONAL_SUCCESS_MESSAGE",__("Seasonal price successfully deleted",BKG_DOMAIN));
define("ADD_SEASONAL_SUCCESS_MESSAGE",__("Seasonal price successfully added",BKG_DOMAIN));
define("UPDATED_SEASONAL_SUCCESS_MESSAGE",__("Seasonal price successfully updated",BKG_DOMAIN));
define('BOOKING_CANCEL_MSG',__('Your Booking is successfully Cancelled',BKG_DOMAIN));
define('BOOK_PROCESS_CANCEL',__('The booking process could not be completed successfully...',BKG_DOMAIN));
define('PRE_BANK_PAYMENT_MSG','<p>'.__('Thank you, your request has been received successfully.',BKG_DOMAIN).'</p><p>'.__('In order to approve your booking, kindly transfer the amount of',BKG_DOMAIN).' <b>[PAYABLE_AMOUNT]</b>'.__(' at our bank with the following information',BKG_DOMAIN).' :</p></br><p>'.__('Bank Name',BKG_DOMAIN).' : <b>[BANK_NAME]</b></p><p>'.__('Account Number',BKG_DOMAIN).' : <b>[ACCOUNT_NUMBER]</b></p><br><p>'.__('Please include the ID as reference',BKG_DOMAIN).' :#[SUBMISSION_ID]</p></br>'.__('Thank you for visit at',BKG_DOMAIN).' [SITE_NAME].');
?>