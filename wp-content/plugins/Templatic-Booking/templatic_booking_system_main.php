<?php
include_once(TEMPLATIC_BOOKING_PATH.'/library/booking_custom_post_type/custom_post_type_lang.php');
include_once(TEMPLATIC_BOOKING_PATH.'/library/booking_custom_post_type/custom_post_type.php');
//Set The Booking system booking settings
function templatic_booking_system_settings(){
	global $wpdb;
?>
    <div class="wrap">
     <div class="icon32" id="icon-options-general"><br></div>
      <h2><?php _e('Templatic Booking Settings', BKG_DOMAIN); ?></h2>
    <?php	
	include_once("templatic_booking_setting.php");
	echo "</div>";
}
?>