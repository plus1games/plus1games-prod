<?php 
/**
 * Display function for the reports page. 
 */
function em_admin_reports_page(){
	global $post, $wpdb;
    $categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );
	
	$sqlstate="select DISTINCT state from zips where state!=''";
    $resultstate = $wpdb->get_results($sqlstate);
	?>	
	<style>
		span{
		   color:#999999;font-size:11px;font-style: italic;
		}
		input[type='text']{
           width:300px;
		}
		span.indicator{
		   color:green;
		}
	</style>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	<script>
		jQuery(function(){	
			jQuery("#tabs").tabs();	  
			jQuery("button[id^='btn-mail-']").click(function(){
				var rel_val       = jQuery(this).attr('rel');		
				var address       = jQuery('#'+rel_val+'-mail-address').val();
				var time_interval = jQuery('#'+rel_val+'-time-interval').val();
				var state      = jQuery('#'+rel_val+'_location_state').val();
				var city      = jQuery('#'+rel_val+'_location_state').val();

				jQuery('#indicator-'+rel_val).text('running...');
				jQuery.ajax({
						url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-mail-manual.php',
						data   :'action='+rel_val+'&address='+address+'&selstate='+state+'&selcity='+city ,
						success:function(x){
							jQuery('#indicator-'+rel_val).text('Report Generated');
						}
				});
			});

			jQuery("button[id^='btn-save-']").click(function(){
				var rel_val       = jQuery(this).attr('rel');	
				
				var address       = jQuery('#'+rel_val+'-mail-address').val();
				alert(address);
				var time_interval = jQuery('#'+rel_val+'-time-interval').val();
				 
				jQuery('#indicator-'+rel_val).text('saving...');
				jQuery.ajax({
					   url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-save.php',
					   data   :'action='+rel_val+'&address='+address+'&time_interval='+time_interval,
					   success:function(x){
							jQuery('#indicator-'+rel_val).text('settings saved');
					   }
				});
			});

			jQuery('#parkcity_location_state').change(function(){
				 var currentstate = jQuery(this).val();
				 jQuery.ajax({
				   url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-mail-manual.php',
				   data   :'currentstate='+currentstate+'&action=dropdowncity' ,
				   success:function(x){
						jQuery('#parkcity_location_town').html(x);
				   }
				 });
			}).change();
		});
	</script>

	<div class="wrap">
		<div id="icon-events" class="icon32"><br /></div>
		<h2><?php _e('Events Manager Reports','dbem'); ?></h2>
		
		<div id="tabs">
			<ul>
			<li><a href="#tabs-parkstate">Park By State</a></li>
			<li><a href="#tabs-parkcity">Park By City</a></li>
			</ul>
			<!--Tab for Park By state report  -->
			<div id="tabs-parkstate">
				<p>
					Email Address:<input type="text" value="<?php echo get_option( 'reports-parkstate-mail-address'); ?>" name="parkstate-mail-address" id="parkstate-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
					 Select State:<select name="parkstate_location_state"id="parkstate_location_state">
					<option value=""selected="selected">select state</option>
					 <?php foreach($resultstate as $v){
					    if(strlen($v->state) < 3)
						echo '<option value="'.$v->state.'">'.$v->state.'</option>';
					}  ?></select><br/><br/>
					
					<button rel="parkstate"class="wpmu-button"id="btn-save-parkstate">Save</button>&nbsp;&nbsp;<button rel="parkstate" class="wpmu-button"id="btn-mail-parkstate">Run Report</button><span class="indicator"id="indicator-parkstate"> </span>
					
				</p>
			</div>
			<!-- End park By state tab -->	
			<!--Tab for Park By city report  -->
			<div id="tabs-parkcity">
				<p>
					Email Address:<input type="text" value="<?php echo get_option( 'reports-parkcity-mail-address'); ?>" name="parkcity-mail-address" id="parkcity-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
					 Select State:<select name="parkcity_location_state"id="parkcity_location_state">
					<option value=""selected="selected">select state</option>
					 <?php foreach($resultstate as $v){
					    if(strlen($v->state) < 3)
						echo '<option value="'.$v->state.'">'.$v->state.'</option>';
					}  ?></select>City: <select name="parkcity_location_town" id="parkcity_location_town"></select>
					<br/><br/>
					
					<button rel="parkcity"class="wpmu-button"id="btn-save-parkcity">Save</button>&nbsp;&nbsp;<button rel="parkcity" class="wpmu-button"id="btn-mail-parkcity">Run Report</button><span class="indicator"id="indicator-parkcity"> </span>
					
				</p>
			</div>	
			<!-- End park By city tab -->		
		</div>		
	</div>
<?php  
}
?>