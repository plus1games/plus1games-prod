<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
require_once($parse_uri[0] .'wp-content/PHPMailer-master/PHPMailerAutoload.php');
global $wpdb;

$search_address     = get_option( 'reports-search-mail-address');
$error_address		= get_option( 'reports-error-mail-address');
$inventory_address  = get_option( 'reports-inventory-mail-address');
$activity_address	= get_option( 'reports-activity-mail-address');
$parkstate_address  = get_option( 'reports-parkstate-mail-address');
$parkcity_address	= get_option( 'reports-parkcity-mail-address');
function send_email($type,$address,$attachment,$list){
	//echo "<pre>";
	//print_r($list);
	global $wpdb;
		$mail = new PHPMailer();
		$mail->From = 'plus1game@gmail.com';
		$mail->FromName = 'Plus1Games';		
		if(strpos($address,',')!==false){
			   $addressarr=explode(',',trim($address));
				foreach($addressarr as  $name)

				{
					echo 'The no of parks in city ';
				   $mail->addAddress($name,'Plus1Games');
				}
		}else{
		  $mail->addAddress($address, 'Plus1Games');
		 
		}		
		$mail->addAttachment($attachment,$attachment); 
		$mail->isHTML(true); 
		$mail->Subject = 'Park '.$type.' Report';
		$mail->Body    = 'Kindly find attachment.';
		if(!$mail->send()) {
		    $return ='Mailer Error: ' . $mail->ErrorInfo . "<br/>";
		}else
			$return = "Park $type Report mail sent <br/>";		
        return $return;
}
function fixEncoding($in_str)
{
  $cur_encoding = mb_detect_encoding($in_str) ;
  if($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8"))
    return $in_str;
  else
    return utf8_encode($in_str);
} 

$action=$_REQUEST['action'];
switch ($action) {
    case "parkstate":
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			's'                      => $_REQUEST['name'],
			'posts_per_page'		 => '100',
			'tax_query' => array(array(
											'taxonomy' => 'listingcategory', 
											'field' => 'id',
											'terms' => 345, 
											'include_children' => true,
									)),
			'meta_key' => 'state',                    //(string) - Custom field key.
			'meta_value' => $_REQUEST['selstate'],
		);
		$query = new WP_Query( $args );
			
		$post_data = $query->posts;
		if($post_data) {
			foreach($post_data as $v) {
				$arrcity = get_post_meta($v->ID ,'city');
				$city_array[$arrcity[0]][$v->ID]=$v;
			}
			
			$totalcity=array();
			foreach($city_array as $key=>$ct){
				$totalcity[$key]=count($ct);
			}
		}
        $header = '';
        $header = array('city','total no');
		$data = array();
		$i = 0;
		foreach($totalcity as $cn=>$tn){
			$data[$i] = array($cn,$tn);
			$i++;
		}
		
		$header1 = array('Parkname','URL','city','state');
		$data1 =array();
		$j=0;
		foreach($city_array as $cn1=>$tn1){
			$k=0;
			foreach($tn1 as $pdata){
				$data1[$cn1][$k] = array($pdata->post_title,$pdata->url,$cn1,$_REQUEST['selstate']);	
				$k++;
			}
		}
	  $list =array();
	  $list[] = array("Total Park Information In State ".$_REQUEST['selstate']);
	  $list[] = array("Total number of parks :".count($post_data));
	  $list[] = $header;
	  foreach($data as $cc){
		$list[] = $cc;
	  }
	  $list[] = array("");
		foreach($data1 as $k=>$v ){
			$list[] = array('Total Parks In City '.$k);;
			$list[] = $header1;
			foreach($v as $o){
				$list[] = $o;
			}
			$list[] = array("");
		}
		$fp = fopen('park_by_satae_report.xls', 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields, "\t", '"');
		}
		fclose($fp);
		echo send_email('State('.$_REQUEST['selstate'].')',$parkstate_address,'park_by_satae_report.xls');
		unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/park_by_satae_report.xls');	
        break;
		case 'dropdowncity':
			$currentstate =$_REQUEST['currentstate'];
			$sqlcity="SELECT DISTINCT city from zips WHERE state = '".$currentstate."' ORDER BY city ASC";
			$resultcity= $wpdb->get_results($sqlcity);	   
		   foreach($resultcity as  $val){
				if(is_numeric($val->city)){}
				else{
					echo '<option value="'.$val->city.'">'.$val->city.'</option>';	  
				}
		   }
	   break;
}


?>