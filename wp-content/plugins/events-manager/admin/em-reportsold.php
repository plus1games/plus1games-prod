<?php 
/**
 * Display function for the reports page. 
 */
function em_admin_reports_page(){
	global $post, $wpdb, $EM_Event,$EM_Location;
    $categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );
	$sqlstate="select DISTINCT location_state from ".$wpdb->prefix."em_locations where location_state != ''";
    $resultstate = $wpdb->get_results($sqlstate);
	$sqlcity="select DISTINCT location_town from ".$wpdb->prefix."em_locations where location_town != ''";
    $resultcity = $wpdb->get_results($sqlcity);
	?>	
	<style>
		span{
		   color:#999999;font-size:11px;font-style: italic;
		}
		input[type='text']{
           width:300px;
		}
		span.indicator{
		   color:green;
		}
	</style>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	 <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	<script>
	jQuery(function(){	
     jQuery("#tabs").tabs();	  
	 jQuery("button[id^='btn-mail-']").click(function(){
		     var rel_val       = jQuery(this).attr('rel');		
			 var address       = jQuery('#'+rel_val+'-mail-address').val();
			 var time_interval = jQuery('#'+rel_val+'-time-interval').val();
			 var state      = jQuery('#'+rel_val+'_location_state').val();
			  var city      = jQuery('#activity_location_town').val();
			 jQuery('#indicator-'+rel_val).text('running...');
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-mail-manual.php',
			   data   :'action='+rel_val+'&address='+address+'&selstate='+state+'&selcity='+city ,
			   success:function(x){
					jQuery('#indicator-'+rel_val).text('Report Generated');
			   }
			 });
		});
		jQuery("button[id^='btn-save-']").click(function(){
		     var rel_val       = jQuery(this).attr('rel');			 
			 var address       = jQuery('#'+rel_val+'-mail-address').val();
			 var time_interval = jQuery('#'+rel_val+'-time-interval').val();
			 
			 jQuery('#indicator-'+rel_val).text('saving...');
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-save.php',
			   data   :'action='+rel_val+'&address='+address+'&time_interval='+time_interval,
			   success:function(x){
					jQuery('#indicator-'+rel_val).text('settings saved');
			   }
			 });
		});
		jQuery('#activity_location_state').change(function(){
             var currentstate = jQuery(this).val();
			 jQuery.ajax({
			   url    :'<?php echo plugins_url();?>/events-manager/admin/reports-ajax-mail-manual.php',
			   data   :'currentstate='+currentstate+'&action=dropdowncity' ,
			   success:function(x){
					jQuery('#activity_location_town').html(x);
			   }
			 });
	    }).change();


	});</script>

	<div class="wrap">
		<div id="icon-events" class="icon32"><br /></div>
		<h2><?php _e('Events Manager Reports','dbem'); ?></h2>
		<div id="tabs">
			<ul>
			<li><a href="#tabs-inventory">Park Inventory Report</a></li>
			<li><a href="#tabs-error">Park Error Report</a></li>
			<li><a href="#tabs-activity">Park Activity Report</a></li>
			<li><a href="#tabs-search">Park Search Report</a></li>
			<li><a href="#tabs-contact">Contact Information</a></li>
			</ul>
			<div id="tabs-inventory">
				<p>
					Email Address:<input type="text" value="<?php echo get_option( 'reports-inventory-mail-address'); ?>" name="inventory-mail-address" id="inventory-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
                    Time Interval:<select name="inventory-time-interval" id="inventory-time-interval">
					<option value="daily" <?php if(get_option( 'reports-inventory-time-interval')=='daily') echo "selected='selected'";?>>Daily</option>
					<option value="weekly" <?php if(get_option( 'reports-inventory-time-interval')=='weekly') echo "selected='selected'";?>>Weekly</option>
					<option value="monthly" <?php if(get_option( 'reports-inventory-time-interval')=='monthly') echo "selected='selected'";?>>Monthly</option>
					</select>
					<br/><br/>
					<input type="hidden" value="" name="inventory_location_state"id="inventory_location_state">
					<!-- Select State:<select name="inventory_location_state"id="inventory_location_state">
					<option value=""selected="selected">select state</option>
					 <?php //foreach($resultstate as $v){
					    //if(strlen($v->location_state) < 3)
						//echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					// }  ?></select><br/><br/> -->
					
					<button rel="inventory"class="wpmu-button"id="btn-save-inventory">Save</button>&nbsp;&nbsp;<button rel="inventory" class="wpmu-button"id="btn-mail-inventory">Run Report</button><span class="indicator"id="indicator-inventory"> </span>
					
				</p>
			</div>


			<div id="tabs-error">
			   <p>
					Email Address:<input type="text"  value="<?php echo get_option( 'reports-error-mail-address'); ?>" name="error-mail-address" id="error-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
                    Time Interval:<select name="error-time-interval" id="error-time-interval">
					<option value="daily" <?php if(get_option( 'reports-error-time-interval')=='daily') echo "selected='selected'";?>>Daily</option>
					<option value="weekly" <?php if(get_option( 'reports-error-time-interval')=='weekly') echo "selected='selected'";?>>Weekly</option>
					<option value="monthly" <?php if(get_option( 'reports-error-time-interval')=='monthly') echo "selected='selected'";?>>Monthly</option>
					</select>
					<br/><br/>
					Select State:<select name="error_location_state"id="error_location_state">
					<option value=""selected="selected">select state</option>
					 <?php foreach($resultstate as $v){
						echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					 }  ?></select><br/><br/>
					 <button  rel="error" class="wpmu-button"id="btn-save-error">Save</button>&nbsp;&nbsp;<button  rel="error" class="wpmu-button"id="btn-mail-error">Run Report</button><span class="indicator" id="indicator-error"> </span>
					
				</p>
			</div>


			<div id="tabs-activity">
			<p>
					Email Address:<input type="text"  value="<?php echo get_option( 'reports-activity-mail-address'); ?>" name="activity-mail-address" id="activity-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
                    Time Interval:<select name="activity-time-interval" id="activity-time-interval">
					<option value="daily" <?php if(get_option( 'reports-activity-time-interval')=='daily') echo "selected='selected'";?>>Daily</option>
					<option value="weekly" <?php if(get_option( 'reports-activity-time-interval')=='weekly') echo "selected='selected'";?>>Weekly</option>
					<option value="monthly" <?php if(get_option( 'reports-activity-time-interval')=='monthly') echo "selected='selected'";?>>Monthly</option>
					</select>
					<br/><br/>
					State: <select name="activity_location_state" id="activity_location_state">
					<?php foreach($resultstate as $v){
					    
						echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					 } ?>
					</select>   City: <select name="activity_location_town" id="activity_location_town">
					<?php foreach($resultcity as $v){
					    
						echo '<option value="'.$v->location_town.'">'.$v->location_town.'</option>';
					 } ?>
					 </select>
					<!-- Select State:<select name="activity_location_state"id="activity_location_state">
					<option value=""selected="selected">select state</option>
					 <?php foreach($resultstate as $v){
					    if(strlen($v->location_state) < 3)
						echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					 }  ?></select> --><br/><br/><button rel="activity" class="wpmu-button"id="btn-save-activity">Save</button>&nbsp;&nbsp;<button  
					 rel="activity" class="wpmu-button"id="btn-mail-activity">Run Report</button><span class="indicator" id="indicator-activity"> </span>
					
				</p>
			</div>


			<div id="tabs-search">
			<p>
					Email Address:<input type="text"  value="<?php echo get_option( 'reports-search-mail-address'); ?>" name="search-mail-address" id="search-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
                    Time Interval:<select name="search-time-interval" id="search-time-interval">
					<option value="daily" <?php if(get_option( 'reports-search-time-interval')=='daily') echo "selected='selected'";?>>Daily</option>
					<option value="weekly" <?php if(get_option( 'reports-search-time-interval')=='weekly') echo "selected='selected'";?>>Weekly</option>
					<option value="monthly" <?php if(get_option( 'reports-search-time-interval')=='monthly') echo "selected='selected'";?>>Monthly</option>
					</select>
					<br/><br/>
					<input type="hidden" value="" name="search_location_state"id="search_location_state">
					<!-- Select State:
					<select name="search_location_state"id="search_location_state">
					<option value=""selected="selected">select state</option>
					 <?php //foreach($resultstate as $v){
					   // if(strlen($v->location_state) < 3)
						//echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					// }  ?></select><br/><br/> -->
					 <button rel="search" class="wpmu-button"id="btn-save-search">Save</button>&nbsp;&nbsp;<button  rel="search" class="wpmu-button"id="btn-mail-search">Run Report</button><span class="indicator" id="indicator-search"> </span>
					
				</p>
			</div>

			<div id="tabs-contact">
				<p>
					Email Address:<input type="text" value="<?php echo get_option( 'reports-contact-mail-address'); ?>" name="contact-mail-address" id="contact-mail-address"/>	<span>Multiple addresses should be comma separated.</span><br/><br/>
                    State: <select name="contact_location_state" id="contact_location_state">
					<?php foreach($resultstate as $v){
					    
						echo '<option value="'.$v->location_state.'">'.$v->location_state.'</option>';
					 } ?>
					</select>
					<br/><br/>
					<input type="hidden" value="" name="contact_location_state"id="contact_location_state">
					
					<button rel="contact"class="wpmu-button"id="btn-save-contact">Save</button>&nbsp;&nbsp;<button rel="contact" class="wpmu-button"id="btn-mail-contact">Run Report</button><span class="indicator"id="indicator-contact"> </span>
					
				</p>
			</div>
		</div>

</div>
<?php  
}
?>