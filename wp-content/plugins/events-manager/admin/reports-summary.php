<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb, $EM_Event,$EM_Location;
$insertarr =array('server_addr'=>$_SERVER['SERVER_ADDR'], 'remote_addr'=>$_SERVER['REMOTE_ADDR'], 'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
'server_time'=>$_SERVER['REQUEST_TIME'] );
$wpdb->insert('wp_cron_chart',$insertarr);

	$sql="SELECT count(location_id) as total from wp_em_locations ";
	$park_count = $wpdb->get_results($sql);
	$totalpark =$park_count[0]->total;
	$sqlweek="SELECT count(a.location_id) as total from wp_em_locations as a LEFT JOIN wp_posts as b ON a.post_id=b.ID WHERE b.post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND b.post_date < CURDATE()";
	$park_count_week = $wpdb->get_results($sqlweek);
	$totalparkweek =$park_count_week[0]->total;
	$indoor= 0;
	$outdoor= 0;
	$indoorweek= 0;
	$outdoorweek= 0;
		$location="SELECT location_id from wp_em_locations  ";
		$resultlocation= $wpdb->get_results($location);

			foreach($resultlocation as $loc){
				$field="SELECT field_indoor FROM wp_em_location_fields where location_id =".$loc->location_id;
				$result_field=$wpdb->get_results($field);
				$chkdoor=array();
				foreach($result_field as $fd){
				$chkdoor[] = $fd->field_indoor;
				}
				if(in_array('Indoor',$chkdoor)){
					$indoor++;
				}
				else{
					$outdoor++;
				}
			}
		$locationweek="SELECT a.location_id from wp_em_locations as a LEFT JOIN wp_posts as b ON a.post_id=b.ID WHERE b.post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND b.post_date < CURDATE() ";
		$resultlocationweek= $wpdb->get_results($locationweek);

			foreach($resultlocationweek as $locw){
				$fieldweek="SELECT field_indoor FROM wp_em_location_fields  where location_id =".$locw->location_id;
				$result_field_week=$wpdb->get_results($fieldweek);
				$chkdoorweek=array();
				foreach($result_field_week as $fdw){
				$chkdoorweek[] = $fdw->field_indoor;
				}
				if(in_array('Indoor',$chkdoorweek)){
					$indoorweek++;
				}
				else{
					$outdoorweek++;
				}
			}
	
		$chart='[easychart type="vertbar" height="300" width="350" title="Total Parks/Indoor/Outdoor" groupnames="Total ,Added in Current Week" valuenames="Total,Indoor,Outdoor" group1values="'.$totalpark.','.$indoor.','.$outdoor.'" group2values="'.$totalparkweek.','.$indoorweek.','.$outdoorweek.'"]';
		//echo do_shortcode( $chart ) ;


		$sqlloc="select  a.location_state,a.location_name,a.location_slug from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID ";
		$result1 = $wpdb->get_results($sqlloc);
		$totalloc=count($result1);			
			if(!empty($result1))
			{
				foreach($result1 as $v){
					if(strlen($v->location_state) < 3 && $v->location_state !=''){
				        $locations[trim(strtoupper($v->location_state))][]=$v->location_name;
				  }	
				} 
				 ksort($locations);				 
				 foreach($locations as $key=>$val){
					  $statecount= count($val);					
						  $list1_keys[]=$key;
						  $list1_values[]=$statecount;
						  $sqllocstate="select  count(*) as total from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND a.location_state='{$key}'";
			              $result = $wpdb->get_results($sqllocstate);
						  $list2[$key]=$result[0]->total;					 
				 }
		  }		
        $content='[easychart
		type="horizbar" height="500" title="Parks Added:Total vs. New" groupnames="Total, Added in Current Week"   groupcolors="005599,229944"  valuenames="'.implode(',',$list1_keys).'" group1values="'.implode(',',$list1_values).'" group2values="'.implode(',',$list2).'"]';
		$content = trim(preg_replace('/\s+/', ' ', $content));
		//echo do_shortcode( $content ) ;

		 
	    $sqllocpop="SELECT location_id, count(*) as total FROM wp_em_events where location_id != 'Null' GROUP BY location_id  ORDER BY count(*) DESC LIMIT 20 ";
	    $resultlocpop = $wpdb->get_results($sqllocpop);
		$count =0;
			 foreach($resultlocpop as $v){
					   $location=new EM_Location($v->location_id);
					   if($location->location_name !='')
					   {
						   if($count < 10 ) {
							   $listpop[$location->location_name] = $v->total;
							   $count++;
						   }
					   }
					  
			 }
			 ksort($listpop);
			 foreach($listpop as $k=>$v){
				 $temp1 =str_replace('&','',$k);
				  $temp =str_replace('amp;',' ',$temp1);
				  $listkeypop[]   = str_replace('&','',$temp);
				  $listvaluepop[] = $v;
			 }         
		$contentpop = '[easychart
		type="horizbar" height="200" title="Most Popular Parks" groupnames="Number of events occured on location"    groupcolors="005599,229944" valuenames="'.implode(',',$listkeypop).'" group1values="'.implode(',',$listvaluepop).'"]';
		$contentpop = trim(preg_replace('/\s+/', ' ', $contentpop));
 		 //echo do_shortcode( $contentpop ) ;

        $userslocation="SELECT a.ID,a.display_name as name,COUNT(b.post_author)as counter FROM wp_users AS a LEFT JOIN wp_posts AS b ON a.ID=b.post_author  WHERE a.ID!=1 AND b.post_type='location' GROUP BY a.display_name  ORDER BY counter DESC LIMIT 0,5";
		$resultuserlocation = $wpdb->get_results($userslocation);
							
			foreach($resultuserlocation as $userloc){
				$name[]=$userloc->name;
				$loc_count[]=$userloc->counter;
			}
		$userlocchart='[easychart type="vertbar" height="300" width="350" title=" Top Users that are adding Parks" groupnames="Total Parks" valuenames="'.implode(',',$name).'" group1values="'.implode(',',$loc_count).'"]';
		//echo do_shortcode( $userlocchart ) ;

		$categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );  
	 
		$gcount=array();
			foreach($categories as $c){
				$gamecat="SELECT COUNT(DISTINCT(a.post_id)) as counter from wp_em_location_fields as a  where  a.field_category=".$c->id;
					$resultusergamesloc = $wpdb->get_results($gamecat);
						foreach($resultusergamesloc as $usergame){
								$gcount[$c->name]=$usergame->counter;
							}			
			}
			arsort($gcount);
			$game_cat =array_slice($gcount,0,20);
			ksort($game_cat);
			$game_key=array_keys($game_cat);
			$game_value= array_values($game_cat);
			$usergameloc='[easychart type="horizbar" height="500" width="500" title="Most Popular Game Categories." groupnames="Total Parks " valuenames="'.implode(',',$game_key).'" group1values="'.implode(',',$game_value).'"]';
			//echo do_shortcode( $usergameloc );

		/*$uname=array();
		$game=array();
		$gcount=array();
			foreach($categories as $c){
				 $usergamesloc="SELECT a.display_name , COUNT(b.post_author)as counter from wp_users as a LEFT JOIN wp_posts as b ON a.ID= b.post_author LEFT JOIN wp_em_location_fields as c ON b.ID=c.post_id WHERE a.ID!=1 AND  b.post_type='location' AND b.post_date >=DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND c.field_category=".$c->id." GROUP BY a.display_name ORDER BY counter DESC LIMIT 0,1";
				 
					$resultusergamesloc = $wpdb->get_results($usergamesloc);
						if(!empty($resultusergamesloc)){
							foreach($resultusergamesloc as $usergame){
								$uname=$usergame->display_name;
								  $game[]=$c->name."  (Added by:". $uname.")";
								$gcount[]=$usergame->counter;
							}
						}
			}
			$username=implode(',',$game);
			$usergameloc='[easychart type="horizbar" height="500" width="500" title=" Top Users that are adding Locations By Game Category." groupnames="Total Parks in Current Month" valuenames="'.$username.'" group1values="'.implode(',',$gcount).'"]';
			//echo do_shortcode( $usergameloc ) ;*/

		$sqlevents="SELECT a.location_state, a.location_name, c.event_name, c.event_id, c.location_id FROM wp_em_locations AS a JOIN wp_posts AS b ON a.post_id = b.ID JOIN wp_em_events AS c ON a.location_id = c.location_id WHERE c.event_status =1";
		$resultevent = $wpdb->get_results($sqlevents);
		$tevent1=count($resultevent);			
			if(!empty($resultevent))
				{
					foreach($resultevent as $v){
						 $events[$v->location_state][$v->event_id]=$v->event_name;
						}	
					ksort($events);				 
					 foreach($events as $key=>$val){
						  $eventcount= count($val);
						  if(strlen($key) < 3){
							  $listevent_keys[]=$key;
							  $listevent_values[]=$eventcount;
							  $sqleventsweek="select  count(*) as total from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID JOIN wp_em_events AS c ON a.location_id = c.location_id WHERE (c.event_start_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND c.event_start_date < CURDATE() OR (c.event_end_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND c.event_end_date < CURDATE())) AND c.event_status =1 AND a.location_state='{$key}'";
							  $tevent2 = $wpdb->get_results($sqleventsweek);
							  $listevent2[$key]=$tevent2[0]->total;
						  }
					 }
				}
		$contentevent ='[easychart
		type="horizbar" height="200" title="Events:Total vs. New" groupnames="Total, Current Week"    groupcolors="005599,229944" valuenames="'.implode(',',$listevent_keys).'" group1values="'.implode(',',$listevent_values).'" group2values="'.implode(',',$listevent2).'"]';
		$contentevent = trim(preg_replace('/\s+/', ' ', $contentevent));         
		//echo do_shortcode( $contentevent ) ;

		$userevent="SELECT a.display_name,COUNT(b.event_owner)as counter FROM wp_users AS a LEFT JOIN wp_em_events AS b ON a.ID=b.event_owner LEFT JOIN wp_posts AS c ON b.post_id=c.ID WHERE a.ID!=1 AND (c.post_type='event' OR c.post_type='event-recurring') GROUP BY a.display_name  ORDER BY counter DESC LIMIT 0,5";

		$resultuserevent = $wpdb->get_results($userevent);
			foreach($resultuserevent as $userevent){
				$euname[]=$userevent->display_name;
				$event_count[]=$userevent->counter;
			}
		$usereventchart='[easychart type="vertbar" height="300" width="350" title=" Top Users that are adding Events" groupnames="Total Events" valuenames="'.implode(',',$euname).'" group1values="'.implode(',',$event_count).'"]';
		//echo do_shortcode( $usereventchart ) ;


		$postdate=date('Y-m-d H:i:s',time());
		$title='Weekly Parks and Events Stats';
		$post_arr=array(
				   'post_author'=>    1,			   
				   'post_title'=>	  $title,
				   'post_status'=>	  'publish',
				   'comment_status'=> 'closed',	
				   'post_type'=>	  'post',
				   'post_content'=>   'Here are weekly statistics for Parks and Events <br/> '.$chart.'<br/><br/>'.$content.'<br/><br/>'.$contentpop.'<br/><br/>'.$userlocchart.'<br/><br/>'.$usergameloc.'<br/><br/>'.$contentevent.'<br/><br/>'.$usereventchart
			  );
			$chkduppost = "SELECT ID from wp_posts where post_date >=DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND post_type= 'post' AND post_title ='$title'";
			 $oldres = $wpdb->get_results($chkduppost);
			//if(empty($oldres)){
		    echo $post_id = wp_insert_post( $post_arr );
			// }
			
?>