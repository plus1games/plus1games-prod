<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $wpdb;

$action            =$_GET['action'];
$address           =$_GET['address'];
$time_interval     =$_GET['time_interval'];
switch ($action) {
    case "inventory":
        update_option( 'reports-inventory-mail-address', $address);
	    update_option( 'reports-inventory-time-interval', $time_interval);
        break;
    case "error":
        update_option( 'reports-error-mail-address',$address);
	    update_option( 'reports-error-time-interval', $time_interval);
        break;
    case "activity":
        update_option( 'reports-activity-mail-address', $address);
	    update_option( 'reports-activity-time-interval', $time_interval);
        break;
	case "search":
        update_option( 'reports-search-mail-address', $address);
	    update_option( 'reports-search-time-interval', $time_interval);
        break;
	case "parkstate":
		update_option( 'reports-parkstate-mail-address', $address);
	    update_option( 'reports-parkbystate-time-interval', $time_interval);
		break;
	case "parkcity":
		update_option( 'reports-parkcity-mail-address', $address);
	    update_option( 'reports-parkcity-time-interval', $time_interval);
		break;
}