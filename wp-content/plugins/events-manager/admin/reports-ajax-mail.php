<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
require_once($parse_uri[0] .'wp-content/PHPMailer-master/PHPMailerAutoload.php');
global $wpdb;

$search_time        = get_option( 'reports-search-time-interval');
$error_time			= get_option( 'reports-error-time-interval');
$inventory_time		= get_option( 'reports-inventory-time-interval');
$activity_time		= get_option( 'reports-activity-time-interval');

$search_address     = get_option( 'reports-search-mail-address');
$error_address		= get_option( 'reports-error-mail-address');
$inventory_address  = get_option( 'reports-inventory-mail-address');
$activity_address	= get_option( 'reports-activity-mail-address');

$resultcurdate = mysql_query("SELECT CURDATE()");
while($rowcurdate=mysql_fetch_array($resultcurdate))
{ 
		$getsqldate = $rowcurdate['CURDATE()'] ;
}
$myrows = $wpdb->get_results( "SELECT * from wp_em_reports_generated" );

$last_activity_time  =$myrows[0]->date;
$last_error_time	 =$myrows[1]->date;
$last_search_time	 =$myrows[2]->date;
$last_inventory_time =$myrows[3]->date;

function get_seconds($param){
   if($param == 'weekly'){
        $sec=6*24*60*60;
   }
   if($param == 'daily'){
        $sec=23*60*60;
   }
   if($param == 'monthly'){
        $sec=29*24*60*60;
   }
   return $sec;
}
$todaytime       = strtotime($getsqldate);
$l_search_t      = strtotime($last_search_time);
$l_activity_t    = strtotime($last_activity_time);
$l_error_t       = strtotime($last_error_time);
$l_inventory_t   = strtotime($last_inventory_time);

$search_diff     = get_seconds($search_time);
$activity_diff   = get_seconds($activity_time);
$error_diff      = get_seconds($error_time);
$inventory_diff  = get_seconds($inventory_time);

function send_email($type,$address,$attachment,$date){
	global $wpdb;
		$mail = new PHPMailer();
		$mail->From = 'plus1games@gmail.com';
		$mail->FromName = 'Plus1Games';		
		if(strpos($address,',')!==false){
			   $addressarr=explode(',',trim($address));
				foreach($addressarr as  $name)
				{
				   $mail->addAddress($name,'Plus1Games');
				}
		}else{
		  $mail->addAddress($address, 'Plus1Games');
		 
		}		
		$mail->addAttachment($attachment,$attachment); 
		$mail->isHTML(true); 
		$mail->Subject = 'Park '.$type.' Report';
		$mail->Body    = 'Kindly find attachment.';
		if(!$mail->send()) {
		    $return ='Mailer Error: ' . $mail->ErrorInfo . "<br/>";
		}else
			$return = "Park $type Report mail sent <br/>";		
		$query="update wp_em_reports_generated set date='{$date}' WHERE type='{$type}'";		
		$wpdb->query($query);
		
		
        return $return;
}
function send_email_error($othermails,$address,$attachment,$date){
	global $wpdb;
	    $exthtml = '';
	    if(!empty($othermails)){
			$exthtml .= "<br/>Following users also received error reports:<br/>";
            foreach($othermails as $ok => $ov){
            $exthtml .= $ok ." (".$ov.")<br>";
			}
		}
		$mail = new PHPMailer();
		$mail->From = 'plus1games@gmail.com';
		$mail->FromName = 'Plus1Games';		
		if(strpos($address,',')!==false){
			   $addressarr=explode(',',trim($address));
				foreach($addressarr as  $name)
				{
				   $mail->addAddress($name,'Plus1Games');
				}
		}else{
		  $mail->addAddress($address, 'Plus1Games');
		 
		}		
		$mail->addAttachment($attachment,$attachment); 
		$mail->isHTML(true); 
		$mail->Subject = 'Park Error Report';
		$mail->Body    = 'Kindly find attachment.'.$exthtml;
		if(!$mail->send()) {
		    $return ='Mailer Error: ' . $mail->ErrorInfo . "<br/>";
		}else
			$return = "Park Error Report mail sent <br/>";		
		$query="update wp_em_reports_generated set date='{$date}' WHERE type='Error'";		
		$wpdb->query($query);
		
		
        return $return;
}
function ind_send_email($address1,$address2,$attachment){
	global $wpdb;
		$mail = new PHPMailer();
		$mail->From = 'plus1game@gmail.com';
		$mail->FromName = 'Plus1Games';
		$mail->addAddress($address1, $address2);
		$mail->addAttachment($attachment,$attachment); 
		$mail->isHTML(true); 
		$mail->Subject = 'Park Error Report';
		$mail->Body    = 'Kindly find attachment.';
		if(!$mail->send()) {
		    $return ='Mailer Error: ' . $mail->ErrorInfo . "<br/>";
		}else
			$return = "Park Error Report mail sent <br/>";		
		
		
		
        return $return;
}

if($todaytime - $l_error_t > $error_diff){
	      $otherusers = array();
	      $list = array (); 
		    $zipcodes = $wpdb->get_results( "SELECT state FROM zips" );
	        foreach($zipcodes as $zip){
		          $allzips[]= $zip->state;
		    }			
	        $list[]=array("Location Error Information for ".site_url());
			$sqlallusers="SELECT DISTINCT post_author from wp_posts WHERE post_type='location' AND (post_status='pending' OR post_status='publish') ";
			$resultallusers = $wpdb->get_results($sqlallusers);
            $totaouserscount = count($resultallusers);
			$totalemptylocations = 0;
			$totalinvalidlocations = 0;
			if($totaouserscount	> 0 ){
				$list_e2[] = array('USER','NUMBER of Locations having no website or phone');	
				foreach($resultallusers as $v){					
					$sqlemptyphone="select b.ID,a.location_id,a.location_state,a.location_town,a.location_name from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE a.location_website='' AND location_phone='' AND b.post_author = '{$v->post_author}'";
					$resultemptyphone = $wpdb->get_results($sqlemptyphone);			
					$emptycountphone=count($resultemptyphone) ;
					$totalemptylocations = $totalemptylocations + $emptycountphone;
					$user_info = get_userdata($v->post_author);
					$user_name= $user_info->user_login ;
					$list_e2[] = array($user_name,$emptycountphone);
					if($emptycountphone > 0){
						foreach($resultemptyphone as $v){
							   $emptylocationsphone[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
						}						
					}
				}
				$list[]=array("Total Number of Locations having no website or phone : ".$totalemptylocations); 
				foreach($emptylocationsphone as $key=>$val){
						$list_e3[]=array("Locations having no website or phone for user: '".$key." '");
						$list_e3[]=array("LOCATION","URL","STATE","CITY");
						foreach($val as $k1=>$v1){
							$namestate=explode('|',$v1);
							$list_e3[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);
						}
						 $list_e3[]=array("","");
					}
          $list_e4 = array('User','Total Number of Incomplete Locations');
		   $emptycount=0;
		  foreach($resultallusers as $v){	
          $sqlempty="select b.ID,a.location_id,a.location_state,a.location_name,a.location_name from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_author = '{$v->post_author}' ";
		  $resultempty = $wpdb->get_results($sqlempty);
		  $user_info = get_userdata($v->post_author);
		  $user_name= $user_info->user_login ;
		  foreach($resultempty as $v){
			 
			 $sqlfield="select field_id  ,field_name ,field_desc  from wp_em_location_fields where location_id='".$v->location_id."' ";
			 $resultfield = $wpdb->get_results($sqlfield);
			 $fieldcount=count($resultfield );
			 if(!empty($fieldcount)){
				if($fieldcount >= 2){
					if($resultfield[0]->field_name =='' && $resultfield[1]->field_name =='' && $resultfield[0]->field_desc =='' && $resultfield[1]->field_desc ==''){
						$emptylocations[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
						$emptycount++;
					}
				}else{
					if($resultfield[0]->field_name =='' && $resultfield[0]->field_desc =='' ){
						$emptylocations[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
						$emptycount++;
					}
				}

			 }else{
				  $emptylocations[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
				  $emptycount++;
			 }
			}
		  }
			
			if($emptycount >0){
				$list_e1=array("Total number of incomplete locations are : '".$emptycount." '");				
				 $list_e5[]=array("USER","NUMBER of Incomplete Locations");
                 foreach($emptylocations as $key=>$val){
				 				
						$tempcnt =count($val);
					    $list_e5[]=array($key,$tempcnt);							  
			   }
			   $list_e5[]=array("","");
			   foreach($emptylocations as $key=>$val){
				 $list_e6[]=array("Incomplete locations for User: '".$key." '");
				 $list_e6[]=array("LOCATION","URL");
				  foreach($val as $k1=>$v1){
						$namestate=explode('|',$v1);
						$list_e6[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);

				  }
				   $list_e6[]=array("","");
			   }
			}
			//invalid states
               $list_e8[] = array('USER','NUMBER of Locations having invalid states.');	
			    
				foreach($resultallusers as $v){	
					$invalidcountlocationsuser =0;
				    $sqlinvalidlocations="select b.ID,a.location_id,a.location_state,a.location_town,a.location_country,a.location_name from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_author = '{$v->post_author}'";
					$resultinvalidlocations = $wpdb->get_results($sqlinvalidlocations);
					$invalidcountlocations=count($resultinvalidlocations) ;					
					$user_info = get_userdata($v->post_author);					
					$user_name= $user_info->user_login ;
					if($invalidcountlocations > 0){
						foreach($resultinvalidlocations as $v){
							if(!in_array(trim($v->location_state),$allzips) && ($v->location_country == 'US')){
							   $totalinvalidlocationarray[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
							   $invalidcountlocationsuser = $invalidcountlocationsuser + 1;
						         }
						}
					}
					$list_e8[] = array($user_name,$invalidcountlocationsuser);
                    $totalinvalidlocations = $totalinvalidlocations + $invalidcountlocationsuser;
				}
				
				$list_e7 =array("Total Number of Locations having invalid states: ".$totalinvalidlocations); 
				foreach($totalinvalidlocationarray as $key=>$val){
						$list_e10[]=array("Locations having invalid states for user: '".$key." '");
						$list_e10[]=array("LOCATION","URL","STATE","CITY");
						foreach($val as $k1=>$v1){
							$namestate=explode('|',$v1);
							$list_e10[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);
						}
						 $list_e10[]=array("","");
					}
			//invalid s---
			}
			
			
			// indivisual email
			$uic =0;
			foreach($resultallusers as $v){
				    $ilist = array();					
				    $totalrecordscount = 0;
					$phone_loc_cnt =0;
					$incom_loc_cnt =0;
					$inv_loc_cnt =0;
					$sql_phone_loc="select b.ID,a.location_id,a.location_state,a.location_town,a.location_name from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE a.location_website='' AND location_phone='' AND b.post_author = '{$v->post_author}'";
					$res_phone_loc = $wpdb->get_results($sql_phone_loc);			
					$phone_loc_cnt=count($res_phone_loc) ;
					$user_info = get_userdata($v->post_author);					
					$user_name= $user_info->user_login ;					
					$user_email= $user_info->user_email ;
					$totalrecordscount = $totalrecordscount + $phone_loc_cnt;
					
					if($phone_loc_cnt > 0){
						foreach($res_phone_loc as $v1){
							   $total_phone_loc_array[$user_name][$v1->ID]=$v1->location_name.'|'.$v1->location_state.'|'.$v1->location_town;
						}						
					}					
					$ilist_11=array("Total Number of Locations having no website or phone : ".$phone_loc_cnt); 
				    foreach($total_phone_loc_array as $key=>$val){
						$ilist_12[]=array("Locations having no website or phone :");
						$ilist_12[]=array("LOCATION","URL","STATE","CITY");
						foreach($val as $k1=>$v1){
							$namestate=explode('|',$v1);
							$ilist_12[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);
						}
						 $ilist_12[]=array("","");
					}
					$sql_inv_loc ="select b.ID,a.location_id,a.location_state,a.location_town,a.location_country,a.location_name from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_author = '{$v->post_author}'";
					$res_inv_loc = $wpdb->get_results($sql_inv_loc);
					$tot_loc_cnt=count($res_inv_loc) ;
					if($tot_loc_cnt > 0){
						foreach($res_inv_loc as $v){
							if(!in_array(trim($v->location_state),$allzips) && ($v->location_country == 'US')){
							   $total_inv_loc_array[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
							   $inv_loc_cnt = $inv_loc_cnt + 1;
						         }
								 $sqlfield="select field_id  ,field_name ,field_desc  from wp_em_location_fields where location_id='".$v->location_id."' ";
								 $resultfield = $wpdb->get_results($sqlfield);
								 $fieldcount=count($resultfield );
								 if(!empty($resultfield)){
									if($fieldcount >= 2){
										if($resultfield[0]->field_name =='' && $resultfield[1]->field_name =='' && $resultfield[0]->field_desc =='' && $resultfield[1]->field_desc ==''){
											$incom_loc_array[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
											$incom_loc_cnt++;
										}
									}else{
										if($resultfield[0]->field_name =='' && $resultfield[0]->field_desc =='' ){
											$incom_loc_array[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
											$incom_loc_cnt++;
										}
									}

								 }else{
									  $incom_loc_array[$user_name][$v->ID]=$v->location_name.'|'.$v->location_state.'|'.$v->location_town;
									 $incom_loc_cnt++;
								 }
								
						}
					}
					
                    $totalrecordscount = $totalrecordscount + $inv_loc_cnt + $incom_loc_cnt;
					if($incom_loc_cnt >0){
					$ilist_21=array("Total number of incomplete locations are : '".$incom_loc_cnt." '");				
					 $ilist_22[]=array("Incomplete Locations:");
					 $ilist_22[]=array("LOCATION","URL","STATE","CITY");
					   foreach($incom_loc_array as $key=>$val){
						  foreach($val as $k1=>$v1){
								$namestate=explode('|',$v1);
								$ilist_22[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);

						  }
						  $ilist_22[]=array("","");
					   }
				    }
					$ilist_31 =array("Total Number of Locations having invalid states: ".$inv_loc_cnt);
					$ilist_32[]=array("Locations having invalid states:");
					$ilist_32[]=array("LOCATION","URL","STATE","CITY");
					foreach($total_inv_loc_array as $key=>$val){							
							foreach($val as $k1=>$v1){
								$namestate=explode('|',$v1);
								$ilist_32[]=array($namestate[0],site_url().'/?post_type=location&p='.$k1,$namestate[1],$namestate[2]);
							}
							 $ilist_32[]=array("","");
						}
						$ilist[]=array("Reports- ".site_url());
						$ilist[]= array("Location error report for :".$user_name);
						$ilist[]=$ilist_11;
						$ilist[]=$ilist_21;
						$ilist[]=$ilist_31;
						if($phone_loc_cnt >0){
						foreach($ilist_12 as $v){
						$ilist[] =$v;
					    }
						}
						if($incom_loc_cnt >0){
                        foreach($ilist_22 as $v){
						$ilist[] =$v;
					    }
						}
						if($inv_loc_cnt >0){
						foreach($ilist_32 as $v){
						$ilist[] =$v;
					    }
						}						
						$fp = fopen('weekly_park_error_report'.$uic.'.xls', 'w');			
						foreach ($ilist as $fields) {
							fputcsv($fp, $fields, "\t", '"');
						}
						fclose($fp);	
						if($user_name != ''){
						$otherusers[$user_name]=$user_email;
						echo ind_send_email($user_email,$user_name,'weekly_park_error_report'.$uic.'.xls');
						unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/weekly_park_error_report'.$uic.'.xls');}
						unset($ilist,$ilist_11,$ilist_12,$ilist_21,$ilist_22,$ilist_31,$ilist_32,$total_inv_loc_array,$incom_loc_array,$total_phone_loc_array);
						$uic++;
				}
				foreach($list_e2 as $v){
				$list[] =$v;
			}
			$list[] =$list_e1;
			$list[] =$list_e4;
			foreach($list_e5 as $v){
				$list[] =$v;
			}
			$list[] =$list_e7;
			foreach($list_e8 as $v){
				$list[] =$v;
			}
			foreach($list_e3 as $v){
				$list[] =$v;
			}
			foreach($list_e6 as $v){
				$list[] =$v;
			}
			
			
			foreach($list_e10 as $v){
				$list[] =$v;
			}
			$fp = fopen('park_error_report.xls', 'w');			
			foreach ($list as $fields) {
				fputcsv($fp, $fields, "\t", '"');
			}
			fclose($fp);
			echo send_email_error($otherusers,$error_address,'park_error_report.xls',$getsqldate);
			unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/park_error_report.xls');
}

if($todaytime - $l_activity_t > $activity_diff){
			$list = array ();
	        $list[]=array("Location Activity Information for ".site_url());
			$sqlloct="select  a.location_state,a.location_name,a.location_slug from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE  b.post_date < CURDATE() AND  a.location_status=1 ";
			$result1t = $wpdb->get_results($sqlloct);
			$totalloct=count($result1t);
			$list[]=array("Total locations : '".$totalloct."'");
			if(!empty($result1t))
			{
				foreach($result1t as $v){
				 $locationst[$v->location_state][$v->location_slug]=$v->location_name;
			 }
			 $list[]=array("STATE","NUMBER");
			 foreach($locationst as $key=>$val){			
				 
				   	    $tempcnt =count($val);
					    $list[]=array($key,$tempcnt);	
				  }
				 $list[]=array("","");
			
			 foreach($locationst as $key=>$val){
				  $list[]=array("Locations for state: '".$key."'");
				  $list[]=array("LOCATION","URL");
				  foreach($val as $k1=>$v1){
						$list[]=array($v1,site_url()."/locations/".$k1);

				  }
				 $list[]=array("","");
			  }
		    }
			$sqlloc="select  a.location_state,a.location_name,a.location_slug from wp_em_locations as a JOIN wp_posts as b ON a.post_id = b.ID WHERE b.post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND b.post_date < CURDATE() AND  a.location_status=1 ";
			$result1 = $wpdb->get_results($sqlloc);
			$totalloc=count($result1);
			$list[]=array("Total locations added within this week: '".$totalloc."'");
			if(!empty($result1))
			{
				foreach($result1 as $v){
				 $locations[$v->location_state][$v->location_slug]=$v->location_name;
			 }
			 $list[]=array("STATE","NUMBER");
			 foreach($locations as $key=>$val){				 
				
				  $tempcnt =count($val);
				  $list[]=array($key,$tempcnt);	
				 
			 }
			 $list[]=array("","");
			 foreach($locations as $key=>$val){
				  $list[]=array("Locations added within this week for state: '".$key."'");
				  $list[]=array("LOCATION","URL");
				  foreach($val as $k1=>$v1){
						$list[]=array($v1,site_url()."/locations/".$k1);

				  }
				 $list[]=array("","");
			 }
		  }		  
		   $fp = fopen('park_activity_report.xls', 'w');
			foreach ($list as $fields) {
				fputcsv($fp, $fields, "\t", '"');
			}
			fclose($fp);			
			echo send_email('Activity',$activity_address,'park_activity_report.xls',$getsqldate);
			unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/park_activity_report.xls');

}
if($todaytime - $l_search_t > $search_diff){
	       $list = array ();
			$list[]=array("Location Search Information for ".site_url());
			$sqlsearchevent="select *  from zipsearch_record WHERE search_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND search_date < CURDATE() AND type='event'";
			$resultsearchevent = $wpdb->get_results($sqlsearchevent);
			$count_r_s_e= count($resultsearchevent);
			$list[]=array("Number of time users searched for events with ZIP within this week:{$count_r_s_e}");
            if($count_r_s_e > 0){
                 foreach($resultsearchevent as $val){
                      $tmp[]=$val->zipcode;
				 }
				  $list[]= $tmp;
			}
           
			$sqlsearchlocation="select *  from zipsearch_record WHERE search_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND search_date < CURDATE() AND type='location'";
			$resultsearchlocation = $wpdb->get_results($sqlsearchlocation);
			$count_r_s_l= count($resultsearchlocation);
			$list[]=array("Number of time users searched for locations with ZIP  within this week:{$count_r_s_l}");
            if($count_r_s_l > 0){
                 foreach($resultsearchlocation as $val){
                      $tmp[]=$val->zipcode;
				 }
				 $list[]= $tmp;
			}
            
            $fp = fopen('park_search_report.xls', 'w');
			foreach ($list as $fields) {
				fputcsv($fp, $fields, "\t", '"');
			}
			fclose($fp);
			echo send_email('Search',$search_address,'park_search_report.xls',$getsqldate);
			unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/park_search_report.xls');
}
if($todaytime - $l_inventory_t > $inventory_diff){
           $list = array ();
	      $list[]=array("Location Inventory Information for ".site_url());
            $sqllocationusert="SELECT DISTINCT post_author from wp_posts WHERE post_type='location' AND (post_status='pending' OR post_status='publish')  AND post_date  < CURDATE()";
			$resultlocationusert = $wpdb->get_results($sqllocationusert);
             $tempt = count($resultlocationusert);             
			if(!empty($resultlocationusert)){
				$list_1=array('TOTAL NUMBER OF USERS ADDED LOCATIONS: '.$tempt);
				$list_5=array("User","Total Parks", "Parks in week");
				$total_locations= 0;
					foreach($resultlocationusert as $v){
							$sqluserinfo="SELECT user_login from wp_users WHERE ID='{$v->post_author}'";
							$resultuserinfo = $wpdb->get_results($sqluserinfo);
							
							$sqluserlocno="SELECT ID,post_title  from wp_posts WHERE post_type ='location' AND (post_status='pending' OR post_status='publish')  AND post_date  < CURDATE() AND post_author ='{$v->post_author}'";
							$resultuserlocno = $wpdb->get_results($sqluserlocno);							
							$countuserlocno =count($resultuserlocno);
							$sqluserlocnoweek="SELECT ID,post_title  from wp_posts WHERE post_type ='location' AND (post_status='pending' OR post_status='publish') AND post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND post_date  < CURDATE() AND post_author ='{$v->post_author}'";
							$resultuserlocnoweek = $wpdb->get_results($sqluserlocnoweek);							
							$countuserlocnoweek =count($resultuserlocnoweek);
							$total_locations=$total_locations + $countuserlocno;
							if($resultuserinfo[0]->user_login != ''){							
							$list_6[]=array($resultuserinfo[0]->user_login, $countuserlocno,$countuserlocnoweek ) ;
							$list_7[]=array("LOCATIONS ADDED BY USER :".$resultuserinfo[0]->user_login);
							$list_7[]=array("Title","URL","State","City");
							foreach($resultuserlocno as $v){
								$st=get_post_meta($v->ID,'_location_state');
								$ct=get_post_meta($v->ID,'_location_town');
                                $list_7[]=array($v->post_title,site_url().'/?post_type=location&p='.$v->ID,$st[0],$ct[0]);
							}
				         }
				   }
				   $list_2=array("Total number of locations: ".$total_locations);
				   
		   }
            $sqllocationuser="SELECT DISTINCT post_author from wp_posts WHERE post_type	='location' AND (post_status='pending' OR post_status='publish') AND post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND post_date  < CURDATE()";
			$resultlocationuser = $wpdb->get_results($sqllocationuser);
			$temp = count($resultlocationuser);
			$list_3=array('NUMBER OF USERS ADDED LOCATIONS THIS WEEK: '.$temp);
			if(!empty($resultlocationuser)){
				
				$list_8=array("USER","NUMBER OF LOCATIONS IN WEEK");
				$total_locations_week= 0;
					foreach($resultlocationuser as $v){
							$sqluserinfo="SELECT user_login from wp_users WHERE ID='{$v->post_author}'";
							$resultuserinfo = $wpdb->get_results($sqluserinfo);
							
							$sqluserlocno="SELECT ID,post_title  from wp_posts WHERE post_type ='location' AND (post_status='pending' OR post_status='publish') AND post_date >=DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND post_date  < CURDATE() AND post_author ='{$v->post_author}'";
							$resultuserlocno = $wpdb->get_results($sqluserlocno);
							$countuserlocno =count($resultuserlocno);
							$total_locations_week=$total_locations_week + $countuserlocno;
							if($resultuserinfo[0]->user_login != ''){
							$list_9[]=array($resultuserinfo[0]->user_login, $countuserlocno ) ;
							$list_10[]=array("LOCATIONS ADDED BY USER :".$resultuserinfo[0]->user_login);
							$list_10[]=array("Title","URL","State","City");
									foreach($resultuserlocno as $v){
										 $st=get_post_meta($v->ID,'_location_state');
								         $ct=get_post_meta($v->ID,'_location_town');
                                         $list_10[]=array($v->post_title,site_url().'/?post_type=location&p='.$v->ID,$st[0],$ct[0]);
									}
							}
				   }
				   $list_4=array("Total number of locations added in this week: ".$total_locations_week);	
				   
		   }
		   $list[]=$list_1;
		   $list[]=$list_2;
		   $list[]=$list_3;
		   if($temp > 0)
		   $list[]=$list_4;
		   $list[]=$list_5;
		   foreach($list_6 as $v){
                  $list[]=$v;
		   }
		   foreach($list_7 as $v){
                  $list[]=$v;
		   }
		   $list[]=array('','');
		   $list[]=array('','');
		   if($total_locations_week > 0 &&  $temp > 0 ){
				   $list[]=$list_8;
				   foreach($list_9 as $v){
						  $list[]=$v;
				   }
				   foreach($list_10 as $v){
						  $list[]=$v;
				   }
		   }
			$fp = fopen('park_inventory_report.xls', 'w');
			foreach ($list as $fields) {
				fputcsv($fp, $fields, "\t", '"');
			}
			fclose($fp);
			echo send_email('Inventory',$inventory_address,'park_inventory_report.xls',$getsqldate);
			unlink($parse_uri[0] .'wp-content/plugins/events-manager/admin/park_inventory_report.xls');
}


?>