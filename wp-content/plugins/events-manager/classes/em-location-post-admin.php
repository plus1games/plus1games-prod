<?php

require( dirname(dirname(dirname(dirname(dirname((__FILE__)))))). '/wp-load.php' );
class EM_Location_Post_Admin{
	function init(){
		global $pagenow;
		if($pagenow == 'post.php' || $pagenow == 'post-new.php' ){ //only needed if editing post
			add_action('admin_head', array('EM_Location_Post_Admin','admin_head'));
		}
		//Meta Boxes
		add_action('add_meta_boxes', array('EM_Location_Post_Admin','meta_boxes'));
		//Save/Edit actions
		add_action('save_post',array('EM_Location_Post_Admin','save_post'));
		add_action('before_delete_post',array('EM_Location_Post_Admin','before_delete_post'),10,1);
		add_action('trashed_post',array('EM_Location_Post_Admin','trashed_post'),10,1);
		add_action('untrash_post',array('EM_Location_Post_Admin','untrash_post'),10,1);
		add_action('untrashed_post',array('EM_Location_Post_Admin','untrashed_post'),10,1);
		//Notices
		add_action('admin_notices',array('EM_Location_Post_Admin','admin_notices'));
		add_action('post_updated_messages',array('EM_Location_Post_Admin','admin_notices_filter'),1,1);
	}
	
	function admin_head(){ 
		global $post, $EM_Location; 
		if( !empty($post) && $post->post_type == EM_POST_TYPE_LOCATION ){
			$EM_Location = em_get_location($post);
		}
	}
	
	function admin_notices(){
		//When editing
		global $post, $EM_Notices;
		if( !empty($post) && $post->post_type == EM_POST_TYPE_LOCATION){
		}
	}
	
	function admin_notices_filter($messages){
		//When editing
		global $post, $EM_Notices;

		if( $post->post_type == EM_POST_TYPE_LOCATION ){
			if ( $EM_Notices->count_errors() > 0 ) {
				unset($_GET['message']);
			}
		}
		return $messages;
	}
  
	
	function save_post($post_id){
		global $wpdb, $EM_Location, $EM_Notices;		
		           // echo'<pre>';print_r($_POST);die('here');
		$saving_status = !in_array(get_post_status($post_id), array('trash','auto-draft')) && !defined('DOING_AUTOSAVE');
		$is_post_type = get_post_type() == EM_POST_TYPE_LOCATION;
		//if(!defined('UNTRASHING_'.$post_id) && $is_post_type && $saving_status){
			if( wp_verify_nonce($_REQUEST['_emnonce'], 'edit_location')){
					
				do_action('em_location_save_pre', $this);				
				$EM_Location = em_get_location($post_id, 'post_id');					
				$get_meta = $EM_Location->get_post_meta();
				$save_meta = $EM_Location->save_meta();
				
               
			
				//Handle Errors by making post draft
				if( !$get_meta || !$save_meta ){
					
					
					$EM_Location->set_status(null, true);
					$EM_Notices->add_error( '<strong>'.sprintf(__('Your %s details are incorrect and cannot be published, please correct these errors first:','dbem'),__('location','dbem')).'</strong>', true); //Always seems to redirect, so we make it static
					$EM_Notices->add_error($EM_Location->get_errors(), true); //Always seems to redirect, so we make it static
					
					apply_filters('em_location_save', false , $this);
					
				}else{
					
					if($_FILES['location-document']['name']!=''){ 
						if($_FILES['location-document']['type']=='application/pdf'){
					 	 EM_Object::pdf_upload();
					     $upload_url=wp_upload_dir();   
			             $pdf_name=$upload_url['subdir'].'/'.$_FILES['location-document']['name'];
                         $EM_Location->pdf_attachment= $pdf_name;
					  
                         $wpdb->query("UPDATE wp_em_locations SET pdf_attachment='{$EM_Location->pdf_attachment}' WHERE location_id='{$EM_Location->location_id}'");
					 }
					}
					//----------------add 12july---------------------
					$location_id=$EM_Location->location_id;
					$post_id=$EM_Location->post_id;
					$status=$EM_Location->status;
                    
					   $field_category=$EM_Location->field_category;
					    $field_name=$EM_Location->field_name;
					     $field_indoor=$EM_Location->field_indoor;
						  $field_public=$EM_Location->field_public;
						   $field_desc=$EM_Location->field_desc;
						    $field_size=$EM_Location->field_size;
						     $field_type=$EM_Location->field_type;
						      $field_rate=$EM_Location->field_rate;						
					   	$newfieldsarr =array_keys($field_name);
					    $query4="SELECT field_id from wp_em_location_fields WHERE location_id=".$location_id;
                        $res4= $wpdb->get_results($query4);
					    $oldfieldsarr =array();
					    foreach($res4 as $fval){
                          $oldfieldsarr[] = $fval->field_id;
					    }
						/*echo "<pre>";
						print_r($oldfieldsarr);
						echo "-----------------------";
                        print_r($newfieldsarr);
						die;*/
					    $status0=1;
					    foreach($newfieldsarr as $addfield){
						   $categoryfield=$field_category[$addfield];
							  $name=$field_name[$addfield];
								$indoor=$field_indoor[$addfield];
								  $public=$field_public[$addfield];
								    $desc=$field_desc[$addfield];
								      $size=$field_size[$addfield];
										$type=$field_type[$addfield];
										   $rate=$field_rate[$addfield];							
											   $field_group_id=$location_id."-".$addfield;
                         if(in_array($addfield,$oldfieldsarr)){
                              $sqlupdate = "UPDATE wp_em_location_fields SET
								          location_id = '".$location_id."',
										   post_id = '".$post_id."',
										    field_group_id = '".$field_group_id."',
											 field_category = '".$categoryfield."',
											  field_name = '".$name."',
											   field_indoor = '".$indoor."',
											    field_public = '".$public."',
											   field_desc = '".$desc."',
											  field_size = '".$size."',
											 field_type = '".$type."',
											field_rate = '".$rate."'
											WHERE field_id ='".$addfield."'";
										 $wpdb->query($sqlupdate);	
						 }else{                             
							$queryrest="INSERT INTO wp_em_location_fields (location_id, post_id,field_group_id, field_category, field_name, field_indoor, field_public, field_desc, field_size, field_type, field_rate, field_location_status) VALUES
							('".$location_id."','". $post_id."', '".$field_group_id."','".$categoryfield."','". $name."','".$indoor."','". $public."','". $desc."','" .$size."','". $type."','". $rate."','". $status0."')";
							$wpdb->query($queryrest);
							 
						 }
					  }
					  foreach($oldfieldsarr as $delfield){
                          if(!in_array($delfield,$newfieldsarr)){
                            $query5="DELETE FROM wp_em_location_fields WHERE field_id=".$delfield;
					         IF(!$wpdb->query($query5)){
								 print_r($wpdb->last_error);die;
							 }  
							 
						  }
					    }
					//-----------------------------------------------------------------
                 $galleryimages_previous=$_REQUEST['galleryimages_previous'];		
				  
					$totalgalleryimages= count($_FILES['location_gallery_images']['name']);
					for($c = 0;$c < $totalgalleryimages;$c++){
						 $location_gallery_images[$c]=array
						(
							'name' =>		$_FILES['location_gallery_images']['name'][$c],
							'type' =>		$_FILES['location_gallery_images']['type'][$c],
							'tmp_name' =>	$_FILES['location_gallery_images']['tmp_name'][$c],
							'error' =>		$_FILES['location_gallery_images']['error'][$c],
							'size' =>		$_FILES['location_gallery_images']['size'][$c]
						);
					}					
					$galleryimages=array(); 
					

					foreach($location_gallery_images as $v){
					  if($v['name']  != '' && strpos($v['type'],'image/') !== false){
						 $galleryimages[] = EM_Object::gallery_image_upload($v);
					  }
					}

					if(is_array($galleryimages_previous)){
					$allgalleryimages=array_merge($galleryimages_previous,$galleryimages );			
					$serializedimages=serialize($allgalleryimages);
					}else{
						   $serializedimages=serialize($galleryimages);
					}
					
					$wpdb->query("UPDATE wp_em_locations SET galleryimages ='".$serializedimages."' WHERE location_id='{$EM_Location->location_id}'");
                    $wpdb->query("UPDATE wp_em_locations SET location_contactname  ='".$_REQUEST['location_contactname']."' WHERE location_id='{$EM_Location->location_id}'");
					$wpdb->query("UPDATE wp_em_locations SET location_website      ='".$_REQUEST['location_website']."' WHERE location_id='{$EM_Location->location_id}'");
					$wpdb->query("UPDATE wp_em_locations SET location_phone        ='".$_REQUEST['location_phone']."' WHERE location_id='{$EM_Location->location_id}'");
					$wpdb->query("UPDATE wp_em_locations SET location_contactemail ='".$_REQUEST['location_contactemail']."' WHERE location_id='{$EM_Location->location_id}'");
					$wpdb->query("UPDATE wp_em_locations SET location_acres        ='".$_REQUEST['location_acres']."' WHERE location_id='{$EM_Location->location_id}'");
					 $wpdb->query("UPDATE wp_em_locations SET location_public      ='".$_REQUEST['location_public']."' WHERE location_id='{$EM_Location->location_id}'");
					$wpdb->query("UPDATE wp_em_locations SET location_authorized   ='".$_REQUEST['location_authorized']."' WHERE location_id='{$EM_Location->location_id}'");
                    
		               //if($_POST['animities'] > '0'){
						    $animity='';
					   $animity1='';
                if($_POST['animities'] > 0){
                 $animity = serialize($_POST['animities']);
                 $animity1 = implode(",",$_POST['animities']);}	
                   $wpdb->query("UPDATE wp_em_locations SET location_animities='".$animity."' WHERE location_id='{$EM_Location->location_id}'");
			             $wpdb->query("UPDATE wp_em_locations SET location_animities1='".$animity1."' WHERE location_id='{$EM_Location->location_id}'");
	                //}
                   if($_REQUEST['location_document_delete'] == 1){
                    $wpdb->query("UPDATE wp_em_locations SET pdf_attachment='' WHERE location_id='{$EM_Location->location_id}'");
					}
				

				      
					//--------------------------------------------------
					apply_filters('em_location_save', true , $this);

				}
			}else{
			 
				//do a quick and dirty update
				do_action('em_location_save_pre', $this);
				$EM_Location = new EM_Location($post_id, 'post_id');
				//check for existence of index
				//$loc_truly_exists = $wpdb->get_var('SELECT location_id FROM '.EM_LOCATIONS_TABLE." WHERE location_id={$EM_Location->location_id}") == $EM_Location->location_id;
				$loc_truly_exists =($wpdb->get_var('SELECT location_id FROM '.EM_LOCATIONS_TABLE." WHERE location_id={$EM_Location->location_id}") == $EM_Location->location_id) ? true : false;
				if(empty($EM_Location->location_id) || !$loc_truly_exists){ $EM_Location->save_meta(); }
				//continue
				$location_status = ($EM_Location->is_published()) ? 1:0;
				$wpdb->query("UPDATE ".EM_LOCATIONS_TABLE." SET location_name='{$EM_Location->location_name}', location_slug='{$EM_Location->location_slug}', location_private='{$EM_Location->location_private}',location_status={$location_status} WHERE location_id='{$EM_Location->location_id}'");
				apply_filters('em_location_save', true , $this);
			}
		//}
		$wpdb->query("UPDATE wp_em_locations SET location_name   ='".$_REQUEST['location_name']."' WHERE location_id='{$EM_Location->location_id}'");
	}

	function before_delete_post($post_id){
		if(get_post_type($post_id) == EM_POST_TYPE_LOCATION){
			$EM_Location = em_get_location($post_id,'post_id');
			$EM_Location->delete_meta();
		}
	}
	
	function trashed_post($post_id){
		if(get_post_type($post_id) == EM_POST_TYPE_LOCATION){
			global $EM_Notices;
			$EM_Location = em_get_location($post_id,'post_id');
			$EM_Location->set_status(null);
			$EM_Notices->remove_all(); //no validation/notices needed
		}
	}
	
	function untrash_post($post_id){
		if(get_post_type($post_id) == EM_POST_TYPE_LOCATION){
			//set a constant so we know this event doesn't need 'saving'
			if(!defined('UNTRASHING_'.$post_id)) define('UNTRASHING_'.$post_id, true);
		}
	}
	 
	function untrashed_post($post_id){
		if(get_post_type($post_id) == EM_POST_TYPE_LOCATION){
			global $EM_Notices;
			$EM_Location = em_get_location($post_id,'post_id');
			$EM_Location->set_status(1);
			$EM_Notices->remove_all(); //no validation/notices needed
		}
	}
	
	function meta_boxes(){
		add_meta_box('em-location-where', __('Where','dbem'), array('EM_Location_Post_Admin','meta_box_where'),EM_POST_TYPE_LOCATION, 'normal','high');

//--------------------10 july--------------------------------
       add_meta_box('em-location-website', __('Contact Information','dbem'), array('EM_Location_Post_Admin','meta_box_website'),EM_POST_TYPE_LOCATION, 'normal','high');
	    add_meta_box('em-location-acres', __('Acres','dbem'), array('EM_Location_Post_Admin','meta_box_acres'),EM_POST_TYPE_LOCATION, 'normal','high');
		add_meta_box('em-location-authorized', __('Location: Authorized Reresentative','dbem'), array('EM_Location_Post_Admin','meta_box_authorized'),EM_POST_TYPE_LOCATION, 'normal','high');
		add_meta_box('em-location-public', __('Location: Public/Private','dbem'), array('EM_Location_Post_Admin','meta_box_public'),EM_POST_TYPE_LOCATION, 'normal','high');
		add_meta_box('em-location-Fields', __('Fields','dbem'), array('EM_Location_Post_Admin','meta_box_Fields'),EM_POST_TYPE_LOCATION, 'normal','high');
		//add_meta_box('em-location-Fields-contact', __('Fields Contact','dbem'), array('EM_Location_Post_Admin','meta_box_Fields_contact'),EM_POST_TYPE_LOCATION, 'normal','low');
		add_meta_box('em-location-Attach', __('Attach','dbem'), array('EM_Location_Post_Admin','meta_box_Attach'),EM_POST_TYPE_LOCATION, 'normal','low');
    add_meta_box('em-location-Animities', __('Location Amenities','dbem'), array('EM_Location_Post_Admin','meta_box_animities'),EM_POST_TYPE_LOCATION, 'normal','high');
	 add_meta_box('em-location-Gallery', __('Location Gallery','dbem'), array('EM_Location_Post_Admin','meta_box_gallery'),EM_POST_TYPE_LOCATION, 'normal','high');
		//------------------------------------------------------------
		//add_meta_box('em-location-metadump', __('EM_Location Meta Dump','dbem'), array('EM_Location_Post_Admin','meta_box_metadump'),EM_POST_TYPE_LOCATION, 'normal','high');
		if( get_option('dbem_location_attributes_enabled') ){
			add_meta_box('em-location-attributes', __('Attributes','dbem'), array('EM_Location_Post_Admin','meta_box_attributes'),EM_POST_TYPE_LOCATION, 'normal','default');
		}
		
	}
	
	function meta_box_metadump(){
		global $post,$EM_Location;
		echo "<pre>"; print_r(get_post_custom($post->ID)); echo "</pre>";
		echo "<pre>"; print_r($EM_Location); echo "</pre>";
	}
	function meta_box_where(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/where.php',true);		
	}
	//--------------------10 july--------------------------------
	function meta_box_website(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/website.php',true);		
	}
	function meta_box_acres(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/acres.php',true);		
	}
		function meta_box_public(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/public.php',true);		
	}
	function meta_box_Fields(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/Fields.php',true);		
	}
	function meta_box_Fields_contact(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/fields_contact.php',true);		
	}
	
		function meta_box_Attach(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/attached_document.php',true);		
	}
  function meta_box_animities(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/animities_admin.php',true);		
	}
	 function meta_box_gallery(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/gallery.php',true);		
	}
	//--------------------------------------------------------
	function meta_box_attributes(){
		em_locate_template('forms/location/attributes.php',true);
	}
	function meta_box_authorized(){
		?><input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>" /><?php
		em_locate_template('forms/location/authorized.php',true);		
	}
}
add_action('admin_init',array('EM_Location_Post_Admin','init'));