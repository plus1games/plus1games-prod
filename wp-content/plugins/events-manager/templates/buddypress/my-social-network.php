<?php 
 global $wpdb,$bp; 
 $curr_user_id=$bp->loggedin_user->id;
 ?>
<style>
input{
	width:300px;
 }
 .err{
	 background-color: #8FF57A;
    border-color: #80CF70;
	border-radius: 3px 3px 3px 3px;    
    margin-bottom: 7px;
	padding:7px;
 }
 </style>
 <?php 
 $sql_old="SELECT * from wp_bp_user_social_info WHERE UserId=".$curr_user_id." AND ConsumerKey!=''";
 $rows =  $wpdb->get_results($sql_old,ARRAY_A); 
  if(count($rows)>0){
	 $tmsg= '<h5>You have already saved your Twitter Account information.You can edit if you want:</h5>';
     $status='update';
 }
 else{
     $tmsg= '<h5>If you want your events to tweet on twitter, please provide following information:</h5>';
	 $status='add';
 }

 echo '<div id="cosumerInformation"></div>';
 echo $tmsg;
 ?>
 <input type="text" placeholder="consumerKey"name ="consumerKey" id="consumerKey"value=""><br/>
 <input type="text" placeholder="consumerSecret"name ="consumerSecret" id="consumerSecret"value=""><br/>
  <input type="text" placeholder="oAuthToken"name ="oAuthToken" id="oAuthToken" value=""><br/>
   <input type="text" placeholder="oAuthSecret"name ="oAuthSecret" id="oAuthSecret"value=""><br/>
    <button id="cosumerInfo">Save</button><br/><br/>
	<script>
	$(document).ready(function(){
		$("#cosumerInfo").click(function(){
		 var callbkurl="<?php echo content_url(); ?>/plugins/custom-plugin/custom-php/consumerInfoSocialService.php";
		 var curr_user_id='<?php echo $curr_user_id; ?>';	
		 var status='<?php echo $status; ?>';
		 var consumerKey=$("#consumerKey").val();
		 var consumerSecret=$("#consumerSecret").val();
		 var oAuthToken=$("#oAuthToken").val();
		 var oAuthSecret=$("#oAuthSecret").val();	
		 if($.trim(consumerKey).length == 0 || $.trim(consumerSecret).length == 0 || $.trim(oAuthToken).length == 0 || $.trim(oAuthSecret).length == 0){
			 $("#cosumerInformation").addClass("err").html('<span style="color:red;">All fields are required.</span>');
			 return false;
		 }else{ 
		 var data= 'consumerKey='+consumerKey+'&&consumerSecret='+consumerSecret+'&&oAuthToken='+oAuthToken+'&&oAuthSecret='+oAuthSecret+'&&curr_user='+curr_user_id+'&&status='+status;	  	 
		    $.ajax({
						   url: callbkurl,
						   data: data,
						   success:function(msg){
							$("#cosumerInformation").addClass("err").html(msg);							
						   }
					   });
					   $("#consumerKey").val(''); 
			  $("#consumerSecret").val('');
			  $("#oAuthToken").val('');
			  $("#oAuthSecret").val('');
              //return false;
		 }   
		});
	});
	</script>
 <?php 
 $sql_old_fb="SELECT * from wp_bp_user_social_info WHERE UserId=".$curr_user_id." AND FB_appid!=''";
 $rows_fb =  $wpdb->get_results($sql_old_fb,ARRAY_A);
  if(count($rows_fb)>0){
	 $fbmsg= '<h5>You have already saved your FaceBook Account information.You can edit if you want:</h5>';
     $status_fb='update';
 }
 else{
     $fbmsg= '<h5>If you want your events to post on FaceBook, please provide following information:</h5>';
	 $status_fb='add';
 }

 echo '<div id="cosumerInformationFB"></div>';
 echo $fbmsg;
 ?>
	 <input type="text" placeholder="FB_appid"name ="fb_appid" id="fb_appid"value=""><br/>
 <input type="text" placeholder="FB_Secret"name ="fb_secret" id="fb_secret"value=""><br/>
<button id="cosumerInfoFb">Save</button><br/><br/>

	<script>
	$(document).ready(function(){
		$("#cosumerInfoFb").click(function(){
		 var callbkurl="<?php echo content_url(); ?>/plugins/custom-plugin/custom-php/consumerInfoSocialService.php";
		 var curr_user_id='<?php echo $curr_user_id; ?>';	
		 var status_fb='<?php echo $status_fb; ?>';
		 var fb_appid=$("#fb_appid").val();
		 var fb_secret=$("#fb_secret").val();
		 	
		 if($.trim(fb_appid).length == 0 || $.trim(fb_secret).length == 0 ){
			 $("#cosumerInformationFB").addClass("err").html('<span style="color:red;">All fields are required.</span>');
			 return false;
		 }else{ 
		 var data= 'fb_appid='+fb_appid+'&&fb_secret='+fb_secret+'&&curr_user='+curr_user_id+'&&status_fb='+status_fb;	  	 
		    $.ajax({
						   url: callbkurl,
						   data: data,
						   success:function(msg){
							$("#cosumerInformationFB").addClass("err").html(msg);							
						   }
					   });
					   $("#fb_appid").val(''); 
			  $("#fb_secret").val('');
			 
              //return false;
		 }   
		});
	});
	</script>