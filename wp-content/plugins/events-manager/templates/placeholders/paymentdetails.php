<?php 
/******************************************************
PaymentDetails.php
This page is specified as the ReturnURL for the Pay Operation.
When returned from PayPal this page is called.
Page get the payment details for the payKey either stored
in the session or passed in the Request.
******************************************************/
ob_start();

  require( dirname(dirname(dirname(dirname(dirname(dirname((__FILE__))))))). '/wp-load.php' );
global $EM_Notices,$EM_Event, $post ,$EM_Booking,$wpdb;


define('API_USERNAME', 'php2we_1360651370_biz_api1.yahoo.in');
define('API_PASSWORD', '1360651391');
define('API_SIGNATURE', 'Atsqc23yLp8wyrYnedqeJFMZpw3SApNOnlF8WAhSqWc5CThiw49Gm37I');
define('API_AppID', 'APP-80W284485P519543T');
define('API_ENDPOINT', 'https://svcs.sandbox.paypal.com/');
define('USE_PROXY',FALSE);
define('PROXY_HOST', $_SERVER['SERVER_ADDR']);
define('PROXY_PORT', $_SERVER['SERVER_PORT']);
// Ack related and Header constants
define('ACK_SUCCESS', 'SUCCESS');
define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');
define('APPLICATION_ID', 'APP-80W284485P519543T');
define('DEVICE_ID','mydevice');
define('PAYPAL_REDIRECT_URL', 'https://www.sandbox.paypal.com/webscr&cmd=');
define('DEVELOPER_PORTAL', 'https://developer.paypal.com');
define('LOGFILENAME', '../Log/logdata.log');
define('DEVICE_IPADDRESS', $_SERVER['SERVER_ADDR']);
//This SDK supports only Name Value(NV) Request and Response Data Formats. for XML,SOAP,JSON use the SOAP SDK from X.com
define('REQUEST_FORMAT','NV');
define('RESPONSE_FORMAT','NV');
define('X_PAYPAL_REQUEST_SOURCE','PHP_NVP_SDK_V1.1');

class RequestEnvelope 
{
	 static $requestEnvelopeErrorLanguage = "requestEnvelope.errorLanguage";
}
 //AdaptivePayments API Request constants	  
class PaymentDetails 
{
	static $payKey = "payKey";
}
class CancelPreapproval
{
	static $preapprovalKey = "preapprovalKey";
}
class ConvertCurrency
{
	static $baseAmountList_currency_0_amount = "baseAmountList.currency(0).amount";
	static $baseAmountList_currency_0_code ="baseAmountList.currency(0).code";
	static $convertToCurrencyList_currencyCode_0 ="convertToCurrencyList.currencyCode(0)";
	static $convertToCurrencyList_currencyCode_1 ="convertToCurrencyList.currencyCode(1)";
	static $convertToCurrencyList_currencyCode_2 ="convertToCurrencyList.currencyCode(2)";
}
class ExecutePayment
{
	static $payKey = "payKey";
}
class GetPaymentOptions
{
	static $payKey = "payKey";
}
class Pay
{
	static $actionType = "actionType";
	static $cancelUrl = "cancelUrl";
	static $returnUrl ="returnUrl";
	static $currencyCode ="currencyCode";
	 static $receiverAmount= array(
			 "receiverList.receiver[0].amount",
	 		 "receiverList.receiver[1].amount",
			 "receiverList.receiver[2].amount",
			 "receiverList.receiver[3].amount",
			 "receiverList.receiver[4].amount",
			 "receiverList.receiver[5].amount",
			 );
			  static $receiverEmail = array(
			 "receiverList.receiver[0].email",
			 "receiverList.receiver[1].email",
			 "receiverList.receiver[2].email",
			 "receiverList.receiver[3].email",
			 "receiverList.receiver[4].email",
			 "receiverList.receiver[5].email",
			 
			);
			 static $primaryReceiver = array(
			 "receiverList.receiver[0].primary[0]",
			"receiverList.receiver[1].primary[1]",
			 "receiverList.receiver[2].primary[2]",
			 "receiverList.receiver[3].primary[3]",
			 "receiverList.receiver[4].primary[4]",
			 "receiverList.receiver[5].primary[5]",
			 
			);
	
	static $receiverPrimary6 ="receiverList.receiver[5].primary[5]";
	
	static $senderEmail= "senderEmail";
	static $clientDetails_deviceId= "clientDetails.deviceId";
	static $clientDetails_ipAddress ="clientDetails.ipAddress";
	static $clientDetails_applicationId= "clientDetails.applicationId";
	static $memo ="memo";
	static $feesPayer ="feesPayer";
	static $preapprovalKey ="preapprovalKey";
	
}
class Preapproval
{
	static $cancelUrl = 'cancelUrl' ;
	static $returnUrl  = 'returnUrl' ;
	static $currencyCode ='currencyCode';
	static $startingDate ='startingDate' ;
	static $endingDate	 = 'endingDate' ;
	static $maxNumberOfPayments	= 'maxNumberOfPayments' ;
	static $maxTotalAmountOfAllPayments ='maxTotalAmountOfAllPayments' ;
	static $requestEnvelope_senderEmail	= 'requestEnvelope.senderEmail' ;

}
class PreapprovalDetail
{
	static $preapprovalKey= 'preapprovalKey';
}
class Refund
{
	static $currencyCode = 'currencyCode';
	static $payKey = 'payKey';
	static $receiverList_receiver_0_amount = 'receiverList.receiver[0].amount';
	static $receiverList_receiver_0_email = 'receiverList.receiver[0].email';
}
class SetPaymentOption
{
	static $displayOptions_emailHeaderImageUrl ='displayOptions.emailHeaderImageUrl';
	static $displayOptions_emailMarketingImageUrl= 'displayOptions.emailMarketingImageUrl';
	static $payKey= 'payKey' ;
}
 //AdaptiveAccounts API Request constants	
class AddBankAccount
{
	         static  $bankAccountNumber = "bankAccountNumber";
             static  $bankAccountType = "bankAccountType";
             static  $bankCountryCode = "bankCountryCode";
             static  $bankName = "bankName";
             static  $confirmationType = "confirmationType";
             static  $emailAddress = "emailAddress";
             static  $routingNumber = "routingNumber";
             static  $webOptionscancelUrl = "webOptions.cancelUrl";
             static  $webOptionscancelUrlDescription = "webOptions.cancelUrlDescription";
             static  $webOptionsreturnUrl = "webOptions.returnUrl";
             static  $webOptionsreturnUrlDescription = "webOptions.returnUrlDescription";
             static  $createAccountKey = "createAccountKey";
}
class AddPaymentCard
{
	        static $cardNumber = "cardNumber";
            static $cardType = "cardType";
            static $confirmationType = "confirmationType";
            static $emailAddress = "emailAddress";
            static $expirationDatemonth = "expirationDate.month";
            static $expirationDateyear = "expirationDate.year";
            static $billingAddressline1 = "billingAddress.line1";
            static $billingAddressline2 = "billingAddress.line2";
            static $billingAddresscity = "billingAddress.city";
            static $billingAddressstate = "billingAddress.state";
            static $billingAddresspostalCode = "billingAddress.postalCode";
            static $billingAddresscountryCode = "billingAddress.countryCode";
            static $nameOnCardfirstName = "nameOnCard.firstName";
            static $nameOnCardlastName = "nameOnCard.lastName";
            static $webOptionscancelUrl = "webOptions.cancelUrl";
            static $webOptionscancelUrlDescription = "webOptions.cancelUrlDescription";
            static $webOptionsreturnUrl = "webOptions.returnUrl";
            static $webOptionsreturnUrlDescription = "webOptions.returnUrlDescription";
            static $createAccountKey = "createAccountKey";
            static $cardVerificationNumber = "cardVerificationNumber";
}
class CreateAccount
{
	
	        static $accountType="accountType";
            static $addresscity="address.city";
            static $addresscountryCode="address.countryCode";
            static $addressline1="address.line1";
            static $addressline2="address.line2";
            static $addresspostalCode="address.postalCode";
            static $addressstate="address.state";
            static $citizenshipCountryCode="citizenshipCountryCode";
            static $contactPhoneNumber="contactPhoneNumber";
            static $currencyCode="currencyCode";
            static $dateOfBirth="dateOfBirth";
            static $namefirstName="name.firstName";
            static $namelastName="name.lastName";
            static $namemiddleName="name.middleName";
            static $namesalutation="name.salutation";
            static $notificationURL="notificationURL";
            static $partnerField1="partnerField1";
            static $partnerField2="partnerField2";
            static $partnerField3="partnerField3";
            static $partnerField4="partnerField4";
            static $partnerField5="partnerField5";
            static $preferredLanguageCode="preferredLanguageCode";
            static $createAccountWebOptionsreturnUrl="createAccountWebOptions.returnUrl";
            static $registrationType="registrationType";
            static $sandboxEmailAddress="sandboxEmailAddress";
            static $emailAddress="emailAddress";
            //For Business Account
            static $businessInfoaverageMonthlyVolume="businessInfo.averageMonthlyVolume";
            static $businessInfoaveragePrice = "businessInfo.averagePrice";
            static $businessInfobusinessAddresscity = "businessInfo.businessAddress.city";
            static $businessInfobusinessAddresscountryCode = "businessInfo.businessAddress.countryCode";
            static $businessInfobusinessAddressline1 = "businessInfo.businessAddress.line1";
            static $businessInfobusinessAddressline2 = "businessInfo.businessAddress.line2";
            static $businessInfobusinessAddresspostalCode = "businessInfo.businessAddress.postalCode";
            static $businessInfobusinessAddressstate = "businessInfo.businessAddress.state";
            static $businessInfobusinessName = "businessInfo.businessName";
            static $businessInfobusinessType = "businessInfo.businessType";
            static $businessInfocustomerServiceEmail = "businessInfo.customerServiceEmail";
            static $businessInfocustomerServicePhone = "businessInfo.customerServicePhone";
            static $businessInfodateOfEstablishment = "businessInfo.dateOfEstablishment";
            static $businessInfopercentageRevenueFromOnline = "businessInfo.percentageRevenueFromOnline";
            static $businessInfosalesVenue = "businessInfo.salesVenue";
            static $businessInfocategory = "businessInfo.category";
            static $businessInfosubCategory = "businessInfo.subCategory";
            static $businessInfowebSite = "businessInfo.webSite";
            static $businessInfoworkPhone = "businessInfo.workPhone";
}
class GetVerifiedStatus
{
			static $emailAddress="emailAddress";
            static $matchCriteria="matchCriteria";
            static $firstName="firstName";
            static $lastName = "lastName";
}
class SetFundingSourceConfirmed
{
	 		static $emailAddress="emailAddress";
            static $fundingSourceKey = "fundingSourceKey";
}

class RequestPermissions
{
		static $callback = "callback";
	      static $scope = array(
			 "scope(0)",
			 "scope(1)",
			 "scope(2)",
			 "scope(3)",
			 "scope(4)",
			 "scope(5)",
			 "scope(6)",
			 "scope(7)",
			 "scope(8)",
			 "scope(9)",
			 "scope(10)",
			 "scope(11)",
			 "scope(12)",
			 "scope(13)",
			 "scope(14)",
			 "scope(15)",
			 "scope(16)",
			  "scope(17)", 
			  "scope(18)",
			  "scope(19)",
			  "scope(20)", 
			  "scope(21)",
			  "scope(22)", 
			  "scope(23)",
			  "scope(24)"
			);
}
class GetAccessToken
{
	 		static $token="token";
	 		static $verifier="verifier";
}

class GetPermissions
{
	 		static $token="token";
}
class CancelPermissions
{
	 		static $token="token";
}
class GetBasicPersonalData{
static $attributeList=array("attributeList.attribute(0)","attributeList.attribute(1)","attributeList.attribute(2)","attributeList.attribute(3)","attributeList.attribute(4)","attributeList.attribute(5)","attributeList.attribute(6)");
}
class GetAdvancedPersonalData{
static $attributeList=array("attributeList.attribute(0)","attributeList.attribute(1)","attributeList.attribute(2)","attributeList.attribute(3)","attributeList.attribute(4)","attributeList.attribute(5)","attributeList.attribute(6)","attributeList.attribute(7)","attributeList.attribute(8)","attributeList.attribute(9)","attributeList.attribute(10)","attributeList.attribute(11)","attributeList.attribute(12)","attributeList.attribute(13)");
}
?>

<?php
/****************************************************
CallerService.php

This file uses the constants.php to get parameters needed 
to make an API call and calls the server.if you want use your
own credentials, you have to change the constants.php

Called by 

****************************************************/


//session_start();

/**
  * hash_call: Function to perform the API call to PayPal using API signature
  * @methodName is name of API  method.
  * @nvpStr is nvp string.
  * returns an associtive array containing the response from the server.
*/


function hash_call($methodName,$nvpStr,$sandboxEmailAddress = '')
{
	//declaring of global variables
	
    $URL= 'https://svcs.sandbox.paypal.com/'.$methodName;
	//setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$URL);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	//turning off the server and peer verification(TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt ($ch, CURLOPT_CAINFO, "/homepages/39/d419033474/htdocs/dev/wp-content/plugins/events-manager/templates/placeholders/cacert.pem");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POST, 1);
    //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
   //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
	if(USE_PROXY)
	curl_setopt ($ch, CURLOPT_PROXY, PROXY_HOST.":".PROXY_PORT); 
	$headers_array = setupHeaders();
    if(!empty($sandboxEmailAddress)) {
    	$headers_array[] = "X-PAYPAL-SANDBOX-EMAIL-ADDRESS: ".$sandboxEmailAddress;
    }
	
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
    curl_setopt($ch, CURLOPT_HEADER, false);
	//setting the nvpreq as POST FIELD to curl
	curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpStr);

	$code = curl_getinfo ($ch, CURLINFO_HTTP_CODE);

	
	
	if(curl_exec($ch) === false)
{
    echo 'Curl error: ' . curl_error($ch);
}
else
{
    echo 'Operation completed without any errors';
}
	
	
	
	//getting response from server
	$response = curl_exec($ch);
	 $info = curl_getinfo($ch);


	if (curl_errno($ch) == 60) {
	 	curl_setopt($ch, CURLOPT_CAINFO,'/homepages/39/d419033474/htdocs/dev/wp-content/plugins/events-manager/templates/placeholders/cacert.pem');
	 	$response = curl_exec($ch);
		}
 	
	//convrting NVPResponse to an Associative Array
	$nvpResArray=deformatNVP($response);
	//$nvpReqArray=deformatNVP($nvpreq);
	//$_SESSION['nvpReqArray']=$nvpReqArray;

	if (curl_errno($ch)) {
		// moving to display page to display curl errors
		  $_SESSION['curl_error_no']=curl_errno($ch) ;
		  $_SESSION['curl_error_msg']=curl_error($ch);
		  $location = "APIError.php";
		  header("Location: $location");
	 } else {
		 //closing the curl
			curl_close($ch);
	  }

return $nvpResArray;
}

/** This function will take NVPString and convert it to an Associative Array and it will decode the response.
  * It is usefull to search for a particular key and displaying arrays.
  * @nvpstr is NVPString.
  * @nvpArray is Associative Array.
  */

function deformatNVP($nvpstr)
{

	$intial=0;
 	$nvpArray = array();


	while(strlen($nvpstr)){
		//postion of Key
		$keypos= strpos($nvpstr,'=');
		//position of value
		$valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

		/*getting the Key and Value values and storing in a Associative Array*/
		$keyval=substr($nvpstr,$intial,$keypos);
		$valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
		//decoding the respose
		$nvpArray[urldecode($keyval)] =urldecode( $valval);
		$nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
     }
	return $nvpArray;
}



function setupHeaders() {
    $headers_arr = array();
	$API_UserName = "php2we_1360651370_biz_api1.yahoo.in"; //TODO
$API_Password = "1360651391"; //TODO
$API_Signature = "Atsqc23yLp8wyrYnedqeJFMZpw3SApNOnlF8WAhSqWc5CThiw49Gm37I"; //TODO
	
//Default App ID for Sandbox	
$API_AppID = "APP-80W284485P519543T";
$API_RequestFormat = "NV";
$API_ResponseFormat = "NV";	



// Ack related and Header constants
define('ACK_SUCCESS', 'SUCCESS');
define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');
define('APPLICATION_ID', 'APP-80W284485P519543T');
define('DEVICE_ID','mydevice');
define('PAYPAL_REDIRECT_URL', 'https://www.sandbox.paypal.com/webscr&cmd=');
define('DEVELOPER_PORTAL', 'https://developer.paypal.com');
define('LOGFILENAME', '../Log/logdata.log');
define('DEVICE_IPADDRESS', $_SERVER['SERVER_ADDR']);
//This SDK supports only Name Value(NV) Request and Response Data Formats. for XML,SOAP,JSON use the SOAP SDK from X.com
define('REQUEST_FORMAT','NV');
define('RESPONSE_FORMAT','NV');
define('X_PAYPAL_REQUEST_SOURCE','PHP_NVP_SDK_V1.1');




    $headers_arr[]="X-PAYPAL-SECURITY-SIGNATURE: ".$API_Signature;
	$headers_arr[]="X-PAYPAL-SECURITY-USERID:  ".$API_UserName;
	$headers_arr[]="X-PAYPAL-SECURITY-PASSWORD: ".$API_Password;
	$headers_arr[]="X-PAYPAL-APPLICATION-ID: ".$API_AppID;
    $headers_arr[] = "X-PAYPAL-REQUEST-DATA-FORMAT: ".$API_RequestFormat;
    $headers_arr[] = "X-PAYPAL-RESPONSE-DATA-FORMAT: " .$API_ResponseFormat;
	$headers_arr[]="X-PAYPAL-DEVICE-IPADDRESS: ".'127.0.0.1'; 

	if(!defined('X-PAYPAL-REQUEST-SOURCE'))
	{
		$headers_arr[]="X-PAYPAL-REQUEST-SOURCE: ".X_PAYPAL_REQUEST_SOURCE;
	}
	else 
	$headers_arr[]="X-PAYPAL-REQUEST-SOURCE: ".X_PAYPAL_REQUEST_SOURCE."-".X-PAYPAL-REQUEST-SOURCE;
	return $headers_arr;
  
}


session_start();
	if(isset($_GET['cs'])) {
		$_SESSION['payKey'] = '';
	}
	try {
		if(isset($_SESSION['payKey'])){
			$payKey = $_SESSION['payKey'];
			echo "request".$payKey;
			}
			if(empty($payKey))
			{
				$payKey = $_SESSION['payKey'];
			}
			$request_array = array (
			PaymentDetails::$payKey=> $payKey,
			RequestEnvelope::$requestEnvelopeErrorLanguage=> 'en_US'
			);
			
			
			$nvpStr=http_build_query($request_array, '', '&');
			$resArray=hash_call("AdaptivePayments/PaymentDetails",$nvpStr);
	
			
			/* Display the API response back to the browser.
			   If the response from PayPal was a success, display the response parameters'
			   If the response was an error, display the errors received using APIError.php.
			 */
			$ack = strtoupper($resArray["responseEnvelope.ack"]);
			
		  if($ack!="SUCCESS"){
			$_SESSION['reshash']=$resArray;
			$location = "APIError.php";
			header("Location: $location");
 
			}
	}
	catch(Exception $ex) {

	throw new Exception('Error occurred in PaymentDetails method');
	}

    			foreach ($resArray as $rVal){
    	list($qKey, $qVal) = explode ("=", $rVal);
			$resArray[$qKey] = $qVal;
    }
	
	
	
	 $event_idd = $_POST['event_id'];
	 $book_id = $_SESSION['booking_idd'];
	$price = $_SESSION['price'];
	$paykeyy = $resArray[payKey];
	 $correlationIdd = $resArray["responseEnvelope.correlationId"];
	 $ackk = $resArray["responseEnvelope.ack"];
	 $timestampp = $resArray["responseEnvelope.timestamp"];
	 $buildd = $resArray["responseEnvelope.build"];
	 $status = $resArray[status];
	 $currencyCode = $resArray[currencyCode];
	 $actionType = $resArray[actionType];
	 $sender_accntid = $resArray["sender.accountId"];
	 $sender_email = $resArray["sender.email"];
	 $paymentExecStatuss = $resArray[paymentExecStatus];


$queryres="INSERT INTO  wp_em_transactions (booking_id, paykey,correlationId, transaction_payment_type, transaction_timestamp, transaction_total_amount, transaction_currency, transaction_status, transaction_gateway, ack, sender_email, sender_accntid, build) VALUES
('".$book_id."','". $paykeyy."', '".$correlationIdd."','".$actionType."','". $timestampp."','".$price."','". $currencyCode."','". $status."','paypal','". $ackk."','". $sender_email."','". $sender_accntid."','". $buildd."')";

$qweres=mysql_query($queryres) or die("insert query error".mysql_error()); 
$redurl= site_url()."/events/";
if($qweres)
 wp_safe_redirect($redurl);
       			?>