<?php
require( dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/wp-load.php' );
$events_count = EM_Events::count( apply_filters('em_content_events_args', $args) );
global $EM_Event, $post ,$wpdb, $current_user, $display_name,$EM_Location;
if($_GET['searchtype'] =='games'){
	        $selcatid			=$_GET['selcatid'];
			$event_door			=$_GET['selnewdoor'];
			$zipvalue			=$_GET['zipvalue'];			
			$dateweek1		    =$_GET['dateweek1'];
			$dateweek2	        =$_GET['dateweek2'];			
			$maximumdistance    =$_GET['maximumdistance'];
			$distancetype       =$_GET['distancetype'];			
						
			if($zipvalue !=''){
					$date=date('Y-m-d H:i:s',time());
					$insertdata =array('zipcode'=>$zipvalue,'type'=>'event','search_date'=>$date);			
					$insert_id =$wpdb->insert( 'zipsearch_record', $insertdata );

			}
		if (class_exists('EM_Events')) {	
			$date1 = date("Y-m-d", strtotime(str_replace('-','/',$dateweek1)));
			$date2 = date("Y-m-d", strtotime(str_replace('-','/',$dateweek2)));
			if($dateweek1 !='' && $dateweek2 !=''){
				$scope = $date1.','.$date2;
			}else{
				$scope = 'future';
			}
			$zipvaluecombine = $zipvalue.'|'.$maximumdistance.'|'.$distancetype;
			
			 echo EM_Events::output( array(
					'category'=>$selcatid,					
					'event_door'=>$event_door,
					'zip_code' => $zipvaluecombine,		
					'scope'=>$scope ,
					'pagination'=>0,
					'limit'=>10000
					));
			
					
			if(is_user_logged_in()){	
			echo '<br  style="clear:both;"/><a style="text-decoration:none;" target="_blank" href="'.site_url().'/members/'.wp_get_current_user()->data->user_login.'/events/my-events" > 	<button type="button">Add Event</button> </a>';
			}else{
				echo '<br  style="clear:both;"/> <button class ="mustlogin" style="text-decoration:none;" type="button">Add Event </button><br/> ';
			  echo '<br/><div class="loginmsg" style="display:none;padding:8px;padding-left:8px; border:1px solid red;background-color:#FFEBE8;color:#000;">You must login to add events.</div>';
			}

		 }	?>
		 <script>
		 jQuery(".mustlogin" ).click(function(){		
				   jQuery(".loginmsg" ).css("display","block");			   
		 });
		 </script>
<?php }
if($_GET['searchtype'] =='places'){  
			$selcatid			=$_GET['selcatid'];			
			$zipvalue			=$_GET['zipvalue'];
			$maximumdistance    =$_GET['maximumdistance'];
			$distancetype       =$_GET['distancetype'];	
			$selectdoor			=$_GET['selnewdoor'];
			$post_ids="";
			if($zipvalue	 !='' && $selectdoor !=''){
		 $sqlzip = "SELECT * FROM zips WHERE zip='".$zipvalue."'";
		     $rs=mysql_query($sqlzip);		  
		     $row=mysql_fetch_assoc($rs);		  
			 $state=$row['state'];
			 $city=$row['city'];
		$post_id='SELECT DISTINCT(post_id) FROM wp_em_locations WHERE location_town="'.$city.'" AND location_state="'.$state.'"';
		$result_postid=$wpdb->get_results($post_id);
		
		$arrayindoor=array();
		$arrayoutdoor=array();
		foreach($result_postid as $v)
			{
				$sqlfield="SELECT field_indoor FROM wp_em_location_fields WHERE post_id='".$v->post_id."'";
				$resultfield=$wpdb->get_results($sqlfield);
				$chkdoortype=array();
				foreach($resultfield as $rv)
				{
					$chkdoortype[]=$rv->field_indoor;
				}
				if(in_array('Indoor',$chkdoortype)){
					$arrayindoor[]=$v->post_id;
				}
				else{
					$arrayoutdoor[]=$v->post_id;
				}
			}
		 $indoor_postid=implode(',',$arrayindoor);
		 $outdoor_postid=implode(',',$arrayoutdoor);
		 if($selectdoor == 'Indoor'){
			 $post_ids = $indoor_postid;
		 }
		 elseif($selectdoor == 'Outdoor'){
			 $post_ids = $outdoor_postid;
		 }
		 else{
		 }
		 }
	   if($distancetype=='km'){
          $totaldistance=$maximumdistance * 0.62;
	   }else
	   $totaldistance=$maximumdistance;
	   $page_number =$_GET['page_number'];
	   $zipradius=$zipvalue.'||'.$totaldistance;	  

		if($zipvalue !=''){
			$date=date('Y-m-d H:i:s',time());
			$insertdata =array('zipcode'=>$zipvalue,'type'=>'location','search_date'=>$date);			
			$insert_id =$wpdb->insert( 'zipsearch_record', $insertdata ); 
			echo '<ul class="content">';
			if($post_ids !=''){
			echo EM_Locations::output( array('post_id'=>$post_ids,'pagination'=>0,'category'=>$selcatid, 'zip_code'=>$zipradius,'is_Ajax'=>$_GET['is_Ajax'],'page'=>$page_number) );
			}
			else{
				echo EM_Locations::output( array('pagination'=>0,'category'=>$selcatid, 'zip_code'=>$zipradius,'is_Ajax'=>$_GET['is_Ajax'],'page'=>$page_number) );
			}
			echo '</ul>';
		}else{
			
			echo '<ul class="content">';
			echo EM_Locations::output( array('pagination'=>0,'category'=>$selcatid,'is_Ajax'=>$_GET['is_Ajax'],'page'=>$page_number) );
			echo '</ul>';
		}

if(is_user_logged_in()){	
	echo '<br style="clear:both;"/><a style="text-decoration:none;" target="_blank" href= "'.site_url().'/members/'.wp_get_current_user()->data->user_login.'/events/my-locations" > 
	 <button type="button">Add Location</button> </a>';
	}else{
		echo '<br  style="clear:both;"/> <button class="mustlogin" style="text-decoration:none;" type="button">Add Location </button> ';
      echo '<div class="loginmsg" style="display:none;padding:8px;padding-left:8px; border:1px solid red;background-color:#FFEBE8;color:#000;">You must login to add locations.</div>';
	}

?>
<script>
	 jQuery(".mustlogin" ).click(function(){		
	           jQuery(".loginmsg" ).css("display","block");
	 });
	 </script>
	 <link type="text/css" href="<?php echo plugins_url(); ?>/events-manager/includes/css/paginate.css" rel="stylesheet" />
<script type="text/javascript" src= "<?php echo plugins_url(); ?>/events-manager/includes/js/paginate.js"></script>
<script type="text/javascript">
//<![CDATA[

jQuery(document).ready( function($){
	$('#home_page_result').pajinate({       
		nav_label_first : '<<',
					nav_label_last : '>>',
					nav_label_prev : '<',
					nav_label_next : '>'

	});
	$('td', 'table').each(function(i) {
    $(this).text(i+1);
});
});
//]]>
</script>
<br/><br/><div class="page_navigation"></div>     	
<?php } ?>	