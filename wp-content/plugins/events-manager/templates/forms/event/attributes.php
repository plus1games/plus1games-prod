<?php
global $EM_Event;
$attributes = em_get_attributes();
$has_depreciated = false;
?>
<script>
	jQuery(document).ready(function($){ 
					
				$('#event_level').change(function() {
				  var val = $(this).val();
				  $('.event_list_images').hide();				  
				
				  $('#'+val).show();
				    //$('#Beginner1').hide();
				}).change();
});
</script>
<div id="event-attributes">
	<?php if( !empty($attributes['names']) && count( $attributes['names'] ) > 0 ) : ?>
		<table class="form-table">
			<thead>
				<tr valign="top">
					<td><strong>Attribute Name</strong></td>
					<td><strong>Value</strong></td>
				</tr>
			</thead> 
			<tbody id="mtm_body">
				<?php
				
				$count = 1;
				
				foreach( $attributes['names'] as $name){
					?>
					<tr valign="top" id="em_attribute_<?php echo $count ?>">
						<td scope="row"><?php echo $name ?></td>
						<td>
							<?php if( count($attributes['values'][$name]) > 0 ): ?>
							
							<select name="em_attributes[<?php echo $name ?>]" id="<?php echo strtolower (str_replace(' ', '_',$name)); ?>" >
								<?php 
								
							foreach($attributes['values'][$name] as $attribute_val): ?>
									<?php if( array_key_exists($name, $EM_Event->event_attributes) && $EM_Event->event_attributes[$name]==$attribute_val ): ?>
										<option value="<?php echo $attribute_val; ?>" selected="selected"><?php echo $attribute_val; ?></option>
									<?php else: ?>
										<option value="<?php echo $attribute_val; ?>"><?php echo $attribute_val; ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
							
							<?php else: ?>
							<input type="text" name="em_attributes[<?php echo $name ?>]" value="<?php echo array_key_exists($name, $EM_Event->event_attributes) ? htmlspecialchars($EM_Event->event_attributes[$name], ENT_QUOTES):''; ?>" />
							<?php endif; ?>
						</td>
					</tr>
					<?php
					$count++;
				}
				if($count == 1){
					?>
					<tr><td colspan="2"><?php echo sprintf(__("You don't have any custom attributes defined in any of your Events Manager template settings. Please add them the <a href='%s'>settings page</a>",'dbem'),EM_ADMIN_URL ."&amp;page=events-manager-options"); ?></td></tr>
					<?php
				}
				?>
				<tr><td> </td><td><div id="result_div" style="height:auto;width:auto;">

									<!--<span id='Beginner1'  style="display:block;"><img src="../wp-content/plugins/events-manager/images/Beginner.jpg" ></span>
					-->
										
										<?php 
										//if(isset($EM_Event->event_attributes['Event Level'])){

						
						$style=$EM_Event->event_attributes['Event Level'];
						//echo $style;

                     ?>
				

					<span id='Recreation' class='event_list_images' style='<?php echo ($style == "Recreation")?  " "  :  "display:none"; ?>'><img src="../wp-content/plugins/events-manager/images/Recreation.jpg" ></span>
					
					<span id='Beginner' class='event_list_images' style='<?php echo ($style == "Beginner")?  " "  :  "display:none"; ?>'><img src="../wp-content/plugins/events-manager/images/Beginner.jpg" ></span>
					
					<span id='Intermediate' class='event_list_images' style='<?php echo ($style == "Intermediate")?  " "  :  "display:none"; ?>'><img src="../wp-content/plugins/events-manager/images/Intermediate.jpg" ></span>
					
					<span id='Advance' class='event_list_images' style='<?php echo ($style == "Advance")?  " "  :  "display:none"; ?>'><img src="../wp-content/plugins/events-manager/images/Advance.jpg" ></span>
					
					<span id='Competition' class='event_list_images' style='<?php echo ($style == "Competition")?  " "  :  "display:none"; ?>'><img src="../wp-content/plugins/events-manager/images/Competition.jpg" ></span>
					
<?php	// }?>
				</div></td></tr>
			</tbody>
		</table>
		<?php if( count(array_diff(array_keys($EM_Event->event_attributes), $attributes['names'])) > 0 ): ?>
		<p><strong><?php _e('Depreciated Attributes', 'dbem')?></strong></p>
		<p><em><?php _e("If you see any attributes under here, that means they're not used in Events Manager formats. To add them, you need to add the custom attribute again to a formatting option in the settings page. To remove any of these depreciated attributes, give it a blank value and save.") ?></em></p>
		<table class="form-table">
			<thead>
				<tr valign="top">
					<td><strong>Attribute Name</strong></td>
					<td><strong>Value</strong></td>
				</tr>
			</thead> 
			<tbody id="mtm_body">
				<?php
				if( is_array($EM_Event->event_attributes) and count($EM_Event->event_attributes) > 0){
					foreach( $EM_Event->event_attributes as $name => $value){
						if( is_array($value) ) $value = serialize($value);
						if( !in_array($name, $attributes['names']) ){
							?>
							<tr valign="top" id="em_attribute_<?php echo $count ?>">
								<td scope="row"><?php echo $name ?></td>
								<td>
									<input type="text" name="em_attributes[<?php echo $name ?>]" value="<?php echo htmlspecialchars($value, ENT_QUOTES); ?>" />
								</td>
							</tr>
							<?php
							$count++;
						}
					}
				}
				?>
			</tbody>
		</table>
		<?php endif; ?>
	<?php else : ?>
		<p>
		<?php _e('In order to use attributes, you must define some in your templates, otherwise they\'ll never show. Go to Events > Settings > General to add attribute placeholders.', 'dbem'); ?>
		</p> 
		<script>
			jQuery(document).ready(function($){ 
				$('#event_attributes').addClass('closed'); });
		</script>
	<?php endif; ?>
</div>