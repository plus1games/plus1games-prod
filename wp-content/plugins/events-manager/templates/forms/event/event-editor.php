<?php
/* WARNING! This file may change in the near future as we intend to add features to the event editor. If at all possible, try making customizations using CSS, jQuery, or using our hooks and filters. - 2012-02-14 */
/* 
 * To ensure compatability, it is recommended you maintain class, id and form name attributes, unless you now what you're doing. 
 * You also must keep the _wpnonce hidden field in this form too.
 */

 global $EM_Event, $EM_Notices, $bp,$wpdb;
//check that user can access this page
if( is_object($EM_Event) && !$EM_Event->can_manage('edit_events','edit_others_events') ){
	?>
	<div class="wrap"><h2><?php _e('Unauthorized Access','dbem'); ?></h2><p><?php echo sprintf(__('You do not have the rights to manage this %s.','dbem'),__('Event','dbem')); ?></p></div>
	<?php
	return false;
}elseif( !is_object($EM_Event) ){
	$EM_Event = new EM_Event();
}
$required = '*';

echo $EM_Notices;
echo '<div id="caterror" style="display:none;padding:8px;padding-left:8px; border:1px solid red;background-color:#FFEBE8;color:#000;"> Event category is required.</div>';
//Success notice
if( !empty($_REQUEST['success']) ){
	$EM_Event = new $EM_Event(); //reset the event
	if(!get_option('dbem_events_form_reshow')) return false;
}
?>
<div>
	<div id="add_event" class="add_events" >Add Event12313
	</div>
	<div id="event_links" class="event_links" >
		<ul>
			<li class ="event_active_li" id="1" rel="info">Event</li>
			<li rel="where" id="2">When/Where</li>
			<li rel="social" id="3">Social Media</li>
			<li rel="details" id="4">Details</li>
			<li rel="games" id="5">Games</li>
			<li rel="game_details" id="6">Game Details</li>
			<li rel="document" id="7">Documents</li>
			<li rel="booking" id="8">Bookings/Registration</li>
			<li rel="save_event" id="9">Remove/Save Event</li>
		</ul>
	</div>
	<script>
		jQuery(document).ready(function($){ 
			$('#event_level').change(function() {
				var val = $(this).val();
				$('.event_list_images').hide();				  
				$('#'+val).show();
			});	
		});
	</script>
	<div class="event-extra-details">				
		<?php 
			//--------------------------------------------------
								
			//---------------------------------------------------?>	
		<?php if(get_option('dbem_attributes_enabled')) : ?>
				<?php
					$attributes = em_get_attributes();
						//echo "<pre>";
						//print_r($attributes);
						$has_depreciated = false;
						?>
				<?php if( count( $attributes['names'] ) > 0 ) : ?>
					<?php foreach( $attributes['names'] as $name) : ?>
						<div class="event-attributes">
							<label for="em_attributes[<?php echo $name ?>]"><?php echo $name ?></label>
								<?php if( count($attributes['values'][$name]) > 0 ): ?>
										<select name="em_attributes[<?php echo $name ?>]" id="<?php echo strtolower (str_replace(' ', '_',$name)); ?>">
											<?php foreach($attributes['values'][$name] as $attribute_val): ?>
												<?php if( is_array($EM_Event->event_attributes) && array_key_exists($name, $EM_Event->event_attributes) && $EM_Event->event_attributes[$name]==$attribute_val ): ?>
												<option value="<?php echo $attribute_val; ?>" selected="selected"><?php echo $attribute_val; ?></option>
												<?php else: ?>
												<option value="<?php echo $attribute_val; ?>"><?php echo $attribute_val; ?></option>
												<?php endif; ?>
											<?php endforeach; ?>
										</select><br/><br/>
												<?php else: ?>
										<input type="text" name="em_attributes[<?php echo $name ?>]" value="<?php echo array_key_exists($name, $EM_Event->event_attributes) ? htmlspecialchars($EM_Event->event_attributes[$name], ENT_QUOTES):''; ?>" />
										<?php endif; ?>						
								</div>
									<?php endforeach; ?>
						<div id="result_div" style="height:auto;width:auto;">				
							<?php endif; 
								endif; ?>
							</div>
						</div>
	<form enctype='multipart/form-data' id="event-form" method="post" action="">
			<div class="wrap">
				<?php do_action('em_front_event_form_header'); ?>
				<?php if(get_option('dbem_events_anonymous_submissions') && !is_user_logged_in()): ?>
					<h4 class="event-form-submitter"><?php _e ( 'Your Details', 'dbem' ); ?></h4>
				<?php endif; ?>
				<div id="event_forms" >
					<div id="info" class="event_details">
						<h4 class="event-form-name"><?php _e ( 'Event Name', 'dbem' ); ?></h4>
						<div class="inside event-form-name">
							<input type="text" name="event_name" id="event-name" value="<?php echo htmlspecialchars($EM_Event->event_name,ENT_QUOTES); ?>" /><?php echo $required; ?>
							<br />
							<?php _e ( 'The event name. Example: Birthday party', 'dbem' )?>
							<?php if( empty($EM_Event->group_id) ): ?>
							<?php 
								$user_groups = array();
								if( !empty($bp->groups) ){
									$group_data = groups_get_user_groups(get_current_user_id());
									foreach( $group_data['groups'] as $group_id ){
										if( groups_is_user_admin(get_current_user_id(), $group_id) ){
											$user_groups[] = groups_get_group( array('group_id'=>$group_id)); 
										}
									}
								} 
							?>
							<?php if( count($user_groups) > 0 ): ?>
								<p>
									<select name="group_id">
										<option value="<?php echo $BP_Group->id; ?>">Not a Group Event</option>
									<?php
									foreach($user_groups as $BP_Group){
										?>
										<option value="<?php echo $BP_Group->id; ?>"><?php echo $BP_Group->name; ?></option>
										<?php
									} 
									?>
									</select>
									<br />
									<?php _e ( 'Select a group you admin to attach this event to it. Note that all other admins of that group can modify the booking, and you will not be able to unattach the event without deleting it.', 'dbem' )?>
								</p>
							<?php endif; ?>
							<?php endif; ?>
						</div>
						<div class="inside event-form-details">
							<div class="event-editor">
								<?php if( get_option('dbem_events_form_editor') && function_exists('wp_editor') ): ?>
									<?php wp_editor($EM_Event->post_content, 'em-editor-content', array('textarea_name'=>'content') ); ?> 
								<?php else: ?>
									<textarea name="content" rows="10" style="width:100%"><?php echo $EM_Event->post_content ?></textarea>
									<br />
									<?php _e ( 'Details about the event.', 'dbem' )?><?php _e ( 'HTML Allowed.', 'dbem' )?>
								<?php endif; ?>
							</div>
						</div>
						<div class="continue" rel="2">Save & Continue</div>
					</div>
					<div id="where" class="event_details">
						<h4 class="event-form-when"><?php _e ( 'When', 'dbem' ); ?></h4>
						<div class="inside">
						<?php 
							if( empty($EM_Event->event_id) && $EM_Event->can_manage('edit_recurring_events','edit_others_recurring_events') && get_option('dbem_recurrence_enabled') ){
								em_locate_template('forms/event/when-with-recurring.php',true);
							}elseif( $EM_Event->is_recurring()  ){
								em_locate_template('forms/event/recurring-when.php',true);
								?>
								<script type="text/javascript">
									jQuery(document).ready( function($) {
										//Recurrence Warnings
										$('#event_form').submit( function(event){
											confirmation = confirm(EM.event_reschedule_warning);
											if( confirmation == false ){
												event.preventDefault();
											}
										});
									});		
								</script>
								<?php
							}else{
								em_locate_template('forms/event/when.php',true);
							}
						?>
						</div>
						<h4 class="event-form-where"><?php  _e ( 'Where', 'dbem' ); ?></h4>
						<div class="inside">
						<?php em_locate_template('forms/event/location.php',true); ?>
						</div>
						<div class="continue" rel="3">Save & Continue</div>
					</div>
					<div id="social" class="event_details">
						<h4 class="event-form-where"><?php  _e ( 'Social Media: FaceBook/Twitter/GooglePlus', 'dbem' ); ?></h4>
						<?php em_locate_template('forms/event/socialmediaservices.php',true); ?>
						
						<div class="continue" rel="4">Save & Continue</div>
					</div>
					<div id="details" class="event_details">
						<h4 class="event-form-where"><?php  _e ( 'Event Level', 'dbem' ); ?></h4>
						<?php
							em_locate_template('forms/event/eventlevel.php',true);

							?>
							<h4 class="event-form-where"><?php  _e ( 'Event Gender', 'dbem' ); ?></h4>
						<?php
							em_locate_template('forms/event/eventsex.php',true);

							?>
							<h4 class="event-form-where"><?php  _e ( 'Event Age Group', 'dbem' ); ?></h4>
						<?php
							em_locate_template('forms/event/eventage.php',true);

							?>
						<div class="continue" rel="5">Save & Continue</div>
					</div>
					<div id="games" class="event_details">
						<h4 class="event-form-where"><?php  _e ( 'Choose Game ', 'dbem' ); ?></h4>
						<?php
							em_locate_template('forms/event/eventtypecategories.php',true);

						?>
						<div class="continue" rel="6">Save & Continue</div>
					</div>
					<div id="game_details" class="event_details">
						<h4 class="event-form-where"><?php  _e ( 'Game Type', 'dbem' ); ?></h4>
					<?php
						em_locate_template('forms/event/eventdoor.php',true);

						?>
					<h4 class="event-form-where"><?php  _e ( 'Public/Invite', 'dbem' ); ?></h4>
						<?php em_locate_template('forms/event/invites.php',true); ?>
						<div class="continue" rel="7">Save & Continue</div>
					</div>
					
					
					<div id="document" class="event_details">
						<?php if( $EM_Event->can_manage('upload_event_images','upload_event_images') ): ?>
						<h4><?php _e ( 'File/Image', 'dbem' ); ?></h4>
						<div class="inside event-form-image">
							<?php if ($EM_Event->get_image_url() != '') : ?> 
								<img src='<?php echo $EM_Event->get_image_url('medium'); ?>' alt='<?php echo $EM_Event->event_name ?>'/>
							<?php else : ?> 
								<?php _e('No File uploaded for this event yet', 'dbem') ?>
							<?php endif; ?>
							<br /><br />
							<label for='event_image'><?php _e('Upload/change File', 'dbem') ?></label> <input id='event-image' name='event_image' id='event_image' type='file' size='40' />
							<br />
							<label for='event_image_delete'><?php _e('Delete File?', 'dbem') ?></label> <input id='event-image-delete' name='event_image_delete' id='event_image_delete' type='checkbox' value='1' />
						</div>
						<?php endif; ?>
									
						<div class="continue" rel="8">Save & Continue</div>
					</div>
					<div id="booking" class="event_details">
						<?php if( get_option('dbem_rsvp_enabled') && $EM_Event->can_manage('manage_bookings','manage_others_bookings') ) : ?>
						<!-- START Bookings -->
						<h4><?php _e('Bookings/Registration','dbem'); ?></h4>
						<div class="inside event-form-bookings">				
							<?php em_locate_template('forms/event/bookings.php',true); ?>
						</div>
						<!-- END Bookings -->
						<?php endif; ?>	
						<div class="continue" rel="9">Save & Continue</div>
					</div>
					<div id="save_event" class="event_details">
						<?php do_action('em_front_event_form_footer'); ?>
					
   
					<p class="submit">	
						<input type="submit" name="events_update" value="<?php _e ( 'Submit Event', 'dbem' ); ?> &raquo;" />
					</p>
					<input type="hidden" name="event_id" value="<?php echo $EM_Event->event_id; ?>" />
					<input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce('wpnonce_event_save'); ?>" />
					<input type="hidden" name="action" value="event_save" />
					<?php if( !empty($_REQUEST['redirect_to']) ): ?>
					<input type="hidden" name="redirect_to" value="<?php echo $_REQUEST['redirect_to']; ?>" />
					<?php endif; ?></div>
				</div>
			</div>
	</form>
	<?php em_locate_template('forms/tickets-form.php', true); //put here as it can't be in the add event form 
	?>
</div>
<style>
#add_event{
	background-color:#284C94;
	color: #ffffff;
	font-size: 20px;
	font-weight: bold;
	text-shadow: 2px 1px 0 000000;
	padding:15px 0px 15px 20px;
}
#event_links{
	width:24%;
	float:left;
}
#event_forms{
	width:74%;
	float:right;
	padding:10px;
	background-color:#fff;
}

.event_details{
	display:none;
	position:relative;
	height:auto;
	min-height:545px;
}
#event_links ul li{
	color:#C3C3C3;
	font-size:17px;
	cursor:pointer;
	padding:20px 0 20px 15px;
	border:2px solid #F4F4F4;
	background:#fff;
}
#event_links ul li.event_active_li{ 
	color:black;
	background-color:#EEEEEE;
	position:relative;
} 
#event_links ul li.event_active_li:after , #event_links ul li.event_active_li:before { 
	left: 78%;
    position: absolute;
	top: 50%; 
	border: solid transparent; 
	content: " ";
}
#event_links ul li.event_active_li:after {
	border-color: rgba(136, 183, 213, 0); 
	border-right-color: #fff; 
	border-width: 31px; 
	margin-top: -30px;
} 
.event_details h4{
	background:none;
	margin-left:25px !important;
	margin-right:10px;
	}
.event_details .inside{
	margin-left:25px;
	margin-right:10px;
}
.event_details .wp-editor-wrap{
	border:1px solid #F5F5F5;
	
}
.event_details .continue{
	background-color:#284C94 !important;
	color: #ffffff;font-size: 15px;
	text-shadow: 2px 1px 0 000000;
	padding:10px;
	width:110px;
	margin:20px;
	float:right;
	border-radius:5px;
	cursor:pointer;
	bottom:0px;
	right:0px;
	position:absolute;
}
#save_event input[type="submit"]{
	background:  #284C94 !important;
	border-radius: 5px;
    color: #fff;
    font-size: 14px;
    font-weight: bold;
    height: auto;
    padding: 14px;
    width: 140px;
}
</style>


<script type="text/javascript">
var jq14 = jQuery.noConflict(true); 

	jq14(document).ready( function($){
		$('#info').css('display','block');
		$('#event_links ul li').click(function() {
		var rel_val   = $(this).attr('rel');
		$('.event_details').css('display','none');
		$(this).addClass('event_active_li').siblings().removeClass('event_active_li');
		$('#'+rel_val).css('display','block');
		});

		$('#event_forms .continue').click(function() {
			var button_rel=$(this).attr('rel');
			$('#event_links ul li#'+button_rel).trigger( "click" );

		});
		$('#Recreation').click(function(){
			var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/callback2.php";
			var level='Recreation';
			$('#Recreation').css("box-shadow","1px 1px 11px #6495ED");
			$('#Beginner').css("box-shadow","none");
			$('#Intermediate').css("box-shadow","none");
			$('#Advance').css("box-shadow","none");
			$('#Competition').css("box-shadow","none");
			$.ajax({					
				url: callurl,
				data: "data="+level,
				success:function(msg){
					$("#showhidden").html(msg);	
				}
			});
		});

	}(jq14));
</script>