<?php
/* 
 * Remember that this file is only used if you have chosen to override location pages with formats in your events manager settings!
 * You can also override the single location page completely in any case (e.g. at a level where you can control sidebars etc.), as described here - http://codex.wordpress.org/Post_Types#Template_Files
 * Your file would be named single-location.php
 */
/*
 * This page displays a single event, called during the em_content() if this is an event page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display events however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Events::output() 
 */
global $EM_Location;
/* @var $EM_Location EM_Location */
echo  $EM_Location->output_single();
?>
<style>
p, #add_show_review,table{
 max-width:620px;
}
.fielddiv{
overflow:scroll;width:620px;margin-bottom:20px;
}
a{
color:#2BCBFE;
}
.pikachoose{
margin-top:20px;
}
#show_hide_map{
cursor:pointer;
float:right;background: #CDCDCD;padding:5px;width:100px;
}
</style>

<script>

jQuery(document).ready( function($){
	$("#show_hide_map").click(function(){
		var attr_ref=	$(this).attr('ref');
		if(attr_ref == 'open'){
		 $(this).html('Hide the Map');
		 $(this).attr( "ref", "close" );
		 }
		 if(attr_ref == 'close'){
		  $(this).html('Show the Map');
		  $(this).attr( "ref", "open" );
		}
		$(".em-locations-map").slideToggle("slow");
	});
	$('.featured-thumbnail').remove();
	
	$(".wpcr_respond").parent(":not('#add_show_review')").children(".wpcr_respond").css("display","none");	
});
</script>