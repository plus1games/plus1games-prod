<?php
/*
 * Default Location List Template
 * This page displays a list of locations, called during the em_content() if this is an events list page.
 * You can override the default display settings pages by copying this file to yourthemefolder/plugins/events-manager/templates/ and modifying it however you need.
 * You can display locations (or whatever) however you wish, there are a few variables made available to you:
 * 
 * $args - the args passed onto EM_Locations::output()
 * 
 */ 
 require( dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/wp-load.php' );
global $post, $wpdb, $EM_Event;
 $categories = EM_Categories::get( apply_filters('em_content_categories_args', $args) );
	?>
 <style>	
	.ui-autocomplete-loading { background:url('<?php echo content_url(); ?>/plugins/events-manager/includes/images/ajxloader.gif') no-repeat right center }
	ul.pagination{
		margin-left:70px; display:none;
	}
	#showcatlocation a div{position:relative;right:13px;}
	.ui-autocomplete-loading {
			background: url('<?php echo content_url(); ?>/plugins/events-manager/images/loading37.gif') no-repeat right center !important;
			}
			 .ui-autocomplete {
			max-height: 150px;
			overflow-y: auto;			
			overflow-x: hidden;
	</style>
	 <link type="text/css" href="<?php echo plugins_url(); ?>/events-manager/includes/css/paginate.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo content_url(); ?>/plugins/events-manager/includes/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo content_url(); ?>/plugins/events-manager/includes/css/jquery.multiselect.css">
<link rel="stylesheet" type="text/css" href="<?php echo content_url(); ?>/plugins/events-manager/includes/css/jquery-uii.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo 
     content_url(); ?>/plugins/wp-customer-reviews/wp-customer-reviews.css" /> 
<script src="http://slidesjs.com/js/slides.js"></script>
<script src="<?php echo content_url();?>/plugins/events-manager/includes/js/plugins.js"></script>
<script src="<?php echo content_url();?>/plugins/events-manager/includes/js/jquery.multiselect.js"></script> 

<script>
var allcats = []; 
jQuery(document).ready( function($){	
	$('a.see_reviews').click(function(){
       var show_id= $(this).attr('id');
       var parts= show_id.split('_');
      $('#show_review_'+parts[2]).toggle();
	});
	$(function(){
			$('#categorylist').slides({
				//preload: true,
				
			});
		});

		var responseurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/visual_search.php";
var responseurl2="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/visual_search_categories.php";
var img_src='<img src="<?php echo content_url(); ?>/plugins/events-manager/includes/images/ajxloader.gif">';
            $("#search" ).click(function(){
              $("#categorylist").html(img_src);	
			     allcats.length=0;				
                 var typecat=$("#typesearch").val();			     
				 $("#showres").html('');				
			     $("#eventlevels").css("display","none");			  
                 $.ajax({
								  url: responseurl2,
								  data: "typecat="+typecat,
								 success:function(msg){
									$("#categorylist").html(msg);	
								 }
						});
			   });
			   $(document).find("#stop" ).click(function(){
               $(document).find('img').css("box-shadow","none");			   
			   $("#showres").html('');			   
			   $("#categorylist").html(img_src);
			   allcats.length=0;
			   $('#search_locat').val('');
	           $('#enter_zip').val('');
	           $('#radius').val('');
			   $("#typesearch").val('');

				$.ajax({
								  url: responseurl2,
								  data: "typecat=",
								 success:function(msg){
									$("#categorylist").html(msg);	
								 }
						});
				});                     
					 $("#categorylist .slides_container img" ).click(function(){
						 var selcatid=$(this).attr("id");
						 var valindex = $.inArray(selcatid , allcats);
	                        if(valindex < 0){
                             $(this).css("box-shadow","1px 1px 11px #6495ED");
							 allcats.push(selcatid);
							 }else{
							  allcats.splice(valindex , 1);
							  $(this).css("box-shadow","none");
							 }						
					   }); 
	$("#loc_search").click(function(){   
	 var search_id =$('#search_locat').val();
	 var zip_id =$('#enter_zip').val();
	 var radius = $('#radius').val();
     var distance_unit = $('#distance_unit').val();
     var animi = $('#animiti').val();   // alert(animi);
     
	$('#hdn_cat_id').val(allcats);	
	$('#hdn_search_id').val(search_id);
	$('#hdn_zip_id').val(zip_id);
    $('#hdn_radius').val(radius);
	$('hdn_distance_unit').val(distance_unit);
		 
	 var callurl="<?php echo content_url(); ?>/plugins/events-manager/templates/forms/location_search_ajax_new.php";
	 var data='cat_id='+allcats+'&zip_id='+zip_id+'&radius='+radius+'&distance_unit='+distance_unit+'&search_id='+search_id+'&is_Ajax=1&page_number=1'
   +'&animi='+animi; 
	 /*if(zip_id ==''&& radius !=''  || zip_id !='' && radius ==''){
     $('#ziperr').html('Both zip & radius are required');
	 return false;
	 }	*/
	 $('#showcatlocation').html(img_src);
	  $('#ziperr').html('');//alert(data);
	 $.ajax({
		 url:callurl,
			 data:data,      
			 success:function(msg){//alert(msg);
			 $('#showcatlocation').html(msg);
		 }
	}); 
 });
	
});
  $(document).ready(function(){	
	 $("#em-date-tdy").val($.datepicker.formatDate('mm-dd-yy', new Date()));
	 var cityzipautocompleteurl ="<?php echo content_url(); ?>/plugins/events-manager/templates/scripts/cityzipautocomplete.php";
				  $( "#txtcity" ).autocomplete({
                  source: cityzipautocompleteurl,	
				  minLength:2,
				 select: function( event, ui ) {
				 var zipcode = ui.item.id;
				  $( "#enter_zip" ).val(zipcode);
				}
			  });	
    // $("#advncd_srcc").click(function(){
    // $("#eventlevels").toggle();
     //});
    });

</script>
<script type="text/javascript">
$(function(){
	$("#animiti").multiselect({
   selectedList: 15 });
});
</script>
<div class="locat_sear">
<?php if( get_option('dbem_search_form_categories') ): ?>	
		<!-- START Category Search -->
		
		<div> <span style="color: #414141;
    font: 20px/22px Patua One,cursive !important;
    letter-spacing: -1px;" > Choose Game: </span> <input type="text" name="typesearch" id="typesearch" placeholder="Search.." style="width:135px;border-color:#ffffff #ffffff #eeeeee #eeeeee;" value=""><img id ="search" title="search" src="<?php echo content_url(); ?>/plugins/events-manager/images/search.png" width="18" height="18"style="cursor:pointer;border:none;"><a style="padding-left:10px;" id="stop"href="javascript:void(0);"><img src="<?php echo content_url(); ?>/plugins/events-manager/images/Refresh-Icon.png"></a>
 </div> 
<div id="categorylist" >
<table><tr><td><a style="cursor:pointer;"class="prev"><div class="prev1"></div></a></td><td width="">
<div class="slides_container">
<?php
$categories = array_chunk($categories,12);
//echo '<pre>';
//print_r($categories);
foreach($categories as $category){
	echo '<div class="caticondiv" >';
	$i = 0;
	foreach($category as $val){
		$object_id = $val->id;
		$querycat = "SELECT * from wp_em_meta where object_id='".$object_id."'AND meta_key='category-image'";
		$rescat = mysql_query($querycat);					  
		$rowcat = mysql_fetch_assoc($rescat);
		if($rowcat['meta_value']=="")
			{
			echo '<div class="caticon"><img id="'.$rowcat['object_id'].'" src="http://plus1games.com/wp-content/uploads/2013/02/Yoga1-300x300.jpg" title="'.$val->name.'" ><center style="overflow:hidden;height:22px;">'.$val->name.'</center></div>';
			}
			else
			{
             echo '<div class="caticon"><img id="'.$rowcat['object_id'].'" src="'.$rowcat['meta_value'].'" title="'.$val->name.'" ><center style="overflow:hidden;height:22px;">'.$val->name.'</center></div>'; 
			 }
		$i++;
		if($i==4){
			$i = 0;
			echo '<br>';
		}		
	}
	echo '</div>';

	}
?>
</div></td><td>
<a class="next" style="cursor:pointer;"><div class="next1"></div></a>
</td></tr></table>
</div>
		

		<!-- END Category Search -->
		<?php endif; ?>
		<br/><br/><input type="text" value="" id="txtcity"  name="txtcity" placeholder="Enter city ( Min 2 char)"/>
		<input value=""name ="enter_zip" id="enter_zip" type="hidden">
		<input style="width:75px;"placeholder="Distance" value=""style="margin-left:30px;" name ="radius" id="radius" type="text">
        <select style="height:24px;"name="distance_unit" id="distance_unit">
		<option value="miles">miles</option>
		<option value="km">km</option>
		</select>
		<div id="ziperr" style="color:red;"></div>
		<br/>Search Keyword: 
		<input placeholder="Search" value=""style="margin-left:30px;" name ="search_locat" id="search_locat" type="text">
	<!-- <div id="advncd_srcc" style="background:url(<?php //echo content_url(); ?>/plugins/events-manager/images/advanced.png);width:115px;height:36px;cursor:pointer;margin-bottom: 20px;margin-top: 20px;"></div>	 -->
<div id="eventlevels">
<?php global $lanimi; ?>
	 <select title="Basic example" multiple="multiple" name="animities[]" size="5" id="animiti">
	 <?php foreach($lanimi as $vamenity){
           echo '<option value="'.$vamenity.'">'.$vamenity.'</option>';
		} ?>
	</select>       </div>		
    <div style="clear:both;background-image:url('<?php echo get_template_directory_uri();?>/images_new/req-button.png');
width:183px;height:60px;color:#ffffff;padding-top:33px;padding-left:40px;cursor:pointer;font-size:27px;height:52px;margin-left:-15px;" id="loc_search"> Get Results</div>
		</div>

<div id="showcatlocation" style="width:auto;float:left;">

<?php
$args['limit'] = get_option('dbem_locations_default_limit');
$args['page'] = (!empty($_REQUEST['pno']) && is_numeric($_REQUEST['pno']) )? $_REQUEST['pno'] : 1;
$args['offset'] = ($args['page']-1)*$args['limit'];
$args['pagination'] = true;
/*echo '<ul class="content">';
echo EM_Locations::output(apply_filters('em_content_locations_args', $args));
echo '</ul>';*/
?>
</div>
 <input type='hidden' id='hdn_cat_id' name= 'hdn_cat_id' value=''>
<input type='hidden' id='hdn_search_id' name= 'hdn_search_id' value=''> 
<input type='hidden' id='hdn_zip_id' name= 'hdn_zip_id' value=''> 
<input type='hidden' id='hdn_radius' name= 'hdn_radius' value=''>
<input type='hidden' id='hdn_distance_unit' name= 'hdn_distance_unit' value=''>






