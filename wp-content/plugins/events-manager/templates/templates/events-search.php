<?php 
/* 
 * By modifying this in your theme folder within plugins/events-manager/templates/events-search.php, you can change the way the search form will look.
 * To ensure compatability, it is recommended you maintain class, id and form name attributes, unless you now what you're doing. 
 * You also must keep the _wpnonce hidden field in this form too.
 */
 //ECHO "<pre>";
 //print_r($_REQUEST);
?>
	<script>
    
			$(document).ready(function(){
	      		 $('input[name=selage]').click(function(){
					         if($(this).is(':checked')){
							  if($(this).val()== 5){
								   $("#event_age1").val(5);
							   $('#agefields').css('display','inline');
							   $('#eventgrade').css('display','none');
                                $('#gradeerror').css('display','none');
							
							  }else if($(this).val()== 4){
								    $("#event_age1").val(4);
								  $('#eventgrade').css('display','inline');
                               $('#agefields').css('display','none');
							   $('#ageerror').css('display','none');							   
							  }else{								   
                               $('#agefields').css('display','none');
							     $('#eventgrade').css('display','none');
								 $('#gradeerror').css('display','none');
								 $('#ageerror').css('display','none');
								  $("#event_age1").val('');
								  var tempageval =$(this).val();
                                $("#event_age").val(tempageval);
								  
						  $(document).find('#ageerror').html('');
						   $(document).find('#gradeerror').html('');	
							  }
				         }
						 });
					
					   });  

	 $(function() {
    $('#searchform').submit(function() {
		var tmpq = $("#event_age1").val();		
			 if(tmpq == 5){
                                 var tmp1= $('#eventminage').val();
							     var tmp2= $('#eventmaxage').val();								
							    if(tmp1 == '' || tmp2 == '' || isNaN(tmp1) || isNaN(tmp2) || (tmp1 > 90) || (tmp2 > 100) || (parseInt(tmp1) >= parseInt(tmp2))){
								    $(document).find('#ageerror').html('<span style="color:red;">Please enter valid age.</span>');
								    $('#ageerror').css('display','inline');
                                    return false;
							      }
							        var tempageval = tmp1+','+tmp2;
								$("#event_age").val(tempageval);
								 }
								 if(tmpq == 4){								
							    var tmp11= $('#eventfromgrade').val();
							    var tmp21= $('#eventtograde').val();
							    if(tmp11 == '' || tmp21 == '' || parseInt(tmp11) >= parseInt(tmp21)){
								  $(document).find('#gradeerror').html('<span style="color:red;">Please enter valid grade.</span>');
								  $('#gradeerror').css('display','inline');
                               return false;
							  }
							   var tempageval = tmp11+'@'+tmp21;
							   $("#event_age").val(tempageval);
							   }
							   $(document).find('#ageerror').html('');
						   $(document).find('#gradeerror').html('');	
							    return true;
								
       
    });
});
     
			
			</script>

<div class="em-events-search" style="min-height: 200px;margin-bottom: 10px;width: 100%;">
	<?php 
	global $em_localized_js,$agearray;
	$s_default = get_option('dbem_search_form_text_label');	
	$s = !empty($_REQUEST['em_search']) ? $_REQUEST['em_search']:$s_default;
	if( !isset($_REQUEST['country']) ){
		$country = get_option('dbem_location_default_country');
	}elseif( !empty($_REQUEST['country']) ){
		$country = $_REQUEST['country'];
	}
	//convert scope to an array in event of pagination
	if(!empty($_REQUEST['scope']) && !is_array($_REQUEST['scope'])){ $_REQUEST['scope'] = explode(',',$_REQUEST['scope']); }
	//get the events page to display search results
	?>
	<form id="searchform" action="<?php echo EM_URI; ?>" method="post" class="em-events-search-form">
		<?php do_action('em_template_events_search_form_header'); ?>
		
		
	
<div id="game_search_smpl">
		
	<div class="game_div">
		<?php if( get_option('dbem_search_form_text') ): ?>
			<!-- START General Search -->
			<?php /* This general search will find matches within event_name, event_notes, and the location_name, address, town, state and country. */ ?>
			<input class="game_input" style="margin-bottom: 0;" type="text" name="em_search" class="em-events-search-text" value="<?php echo $s; ?>" onfocus="if(this.value=='<?php echo $s_default; ?>')this.value=''" onblur="if(this.value=='')this.value='<?php echo $s_default; ?>'" />
			<?php endif; ?>	
	</div>
			
	<div class="game_div">
		<?php 
			$selected = !empty($_REQUEST['category']) ? $_REQUEST['category'] :'' ;
		?>
		<input class="game_input" type="text" style="margin-bottom:0px !important;"  id="categorytmp" name="categorytmp" onfocus="if(this.value=='Game Category')this.value=''" placeholder="Game Cateory" value="" onblur="if(this.value=='')this.value='Game Category'">
		<input class="game_input" type="hidden" id="category" name="category">	
	</div>
		
	<div class="game_div"> 
         <input class="game_input" type="text" name="event_owner" value="<?php echo $_REQUEST['event_owner']; ?>" onfocus="if(this.value=='Event Owner')this.value=''" placeholder="Event Owner" onblur="if(this.value=='')this.value='Event Owner'">
           
	</div>
	
	<div class="game_div"> 
		<select name="event_level" class="game_search_simple">
			<option value=''>All Levels</option>  
			<option class="" value='Recreation' <?php echo ($_REQUEST['event_level']=='Recreation') ? 'selected="selected"':'';?> >Recreation</option> 
			<option class="" value='Beginner' <?php echo ($_REQUEST['event_level']=='Beginner') ? 'selected="selected"':'';?> >Beginner</option>
			<option value='Intermediate' <?php echo ($_REQUEST['event_level']=='Intermediate') ? 'selected="selected"':'';?> >Intermediate</option>
			<option value='Advance'<?php echo ($_REQUEST['event_level']=='Advance') ? 'selected="selected"':'';?> >Advance</option> 
			<option class="" value='Competition' <?php echo ($_REQUEST['event_level']=='Competition') ? 'selected="selected"':'';?> >Competition</option>
		</select>
	</div>
	 
	<div class="game_div"> 
		<select name="event_door" class="game_search_simple">
		<!--	<option value=''>Door</option>  -->
			<option value='Outdoor' <?php echo ($_REQUEST['event_door']=='Outdoor') ? 'selected="selected"':'';?> >Outdoor</option>
			<option value='Indoor' <?php echo ($_REQUEST['event_door']=='Indoor') ? 'selected="selected"':'';?> >Indoor</option>
		</select>
	</div>

	<div class="game_div"> 
		<?php if( get_option('dbem_search_form_dates') ): ?>
		<!-- START Date Search -->
		<span class="em-events-search-dates">
			<?php _e('between','dbem'); ?>:
			<input class="game_input2" type="text" id="em-date-start-loc" />
			<input type="hidden" id="em-date-start" name="scope[0]" value="<?php if( !empty($_REQUEST['scope'][0]) ) echo $_REQUEST['scope'][0]; ?>" />
			<?php _e('and','dbem'); ?>
			<input class="game_input2" type="text" id="em-date-end-loc" />
			<input type="hidden" id="em-date-end" name="scope[1]" value="<?php if( !empty($_REQUEST['scope'][1]) ) echo $_REQUEST['scope'][1]; ?>" />
		</span>
		<!-- END Date Search -->
		<?php endif; ?>
	</div>
		
	<div class="game_div"> 
		<select name="event_sex" class="game_search_simple">
			<option value=''>Gender</option>
			<option value='male' <?php echo ($_REQUEST['event_sex']=='male') ? 'selected="selected"':'';?> >male</option>
			<option value='female'<?php echo ($_REQUEST['event_sex']=='female') ? 'selected="selected"':'';?> >female</option>
			<option value='coed'<?php echo ($_REQUEST['event_sex']=='coed') ? 'selected="selected"':'';?> >coed</option>
		</select>
	</div> 

	<div class="game_div">	
		<?php if( get_option('dbem_search_form_countries') ): ?>
		<!-- START Country Search -->
		<select name="country" class="game_search_simple em-events-search-country">
			<option value=''><?php echo get_option('dbem_search_form_countries_label'); ?></option>
			<?php 
			//get the counties from locations table
			global $wpdb;
			$countries = em_get_countries();
			$em_countries = $wpdb->get_results("SELECT DISTINCT location_country FROM ".EM_LOCATIONS_TABLE." WHERE location_country IS NOT NULL AND location_country != '' ORDER BY location_country ASC", ARRAY_N);
			foreach($em_countries as $em_country): 
			?>
			 <option value="<?php echo $em_country[0]; ?>" <?php echo (!empty($country) && $country == $em_country[0]) ? 'selected="selected"':''; ?>><?php echo $countries[$em_country[0]]; ?></option>
			<?php endforeach; ?>
		</select>
		<!-- END Country Search -->	
		<?php endif; ?>
	</div>
		
	<div class="game_div">
		<?php if( get_option('dbem_search_form_regions') ): ?>
		<!-- START Region Search -->
		<select name="region" class="game_search_simple em-events-search-region">
			<option value=''><?php echo get_option('dbem_search_form_regions_label'); ?></option>
			<?php 
			if( !empty($country) ){
				//get the counties from locations table
				global $wpdb;
				$em_states = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT location_region FROM ".EM_LOCATIONS_TABLE." WHERE location_region IS NOT NULL AND location_region != '' AND location_country=%s ORDER BY location_region", $country), ARRAY_N);
				foreach($em_states as $state){
					?>
					 <option <?php echo (!empty($_REQUEST['region']) && $_REQUEST['region'] == $state[0]) ? 'selected="selected"':''; ?>><?php echo $state[0]; ?></option>
					<?php 
				}
			}
			?>
		</select>	
		<!-- END Region Search -->	
		<?php endif; ?>
	</div>	
	
	<div class="game_div">
		<?php if( get_option('dbem_search_form_states') ): ?>
		<!-- START State/County Search -->
		<select name="state" class="game_search_simple em-events-search-state">
			<option value=''><?php echo get_option('dbem_search_form_states_label'); ?></option>
			<?php 
			if( !empty($country) ){
				//get the counties from locations table
				global $wpdb;
				$cond = !empty($_REQUEST['region']) ? $wpdb->prepare(" AND location_region=%s ", $_REQUEST['region']):'';
				$em_states = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT location_state FROM ".EM_LOCATIONS_TABLE." WHERE location_state IS NOT NULL AND location_state != '' AND location_country=%s $cond ORDER BY location_state", $country), ARRAY_N);
				foreach($em_states as $state){
					?>
					 <option <?php echo (!empty($_REQUEST['state']) && $_REQUEST['state'] == $state[0]) ? 'selected="selected"':''; ?>><?php echo $state[0]; ?></option>
					<?php 
				}
			}
			?>
		</select>
		<!-- END State/County Search -->
		<?php endif; ?>
	</div>

	<div class="game_div">
		<?php if( get_option('dbem_search_form_towns') ): ?>
		<!-- START City Search -->
		<select name="town" class="game_search_simple em-events-search-town">
			<option value=''><?php echo get_option('dbem_search_form_towns_label'); ?></option>
			<?php 
			if( !empty($country) ){
				//get the counties from locations table
				global $wpdb;
				$cond = !empty($_REQUEST['region']) ? $wpdb->prepare(" AND location_region=%s ", $_REQUEST['region']):'';
				$cond .= !empty($_REQUEST['state']) ? $wpdb->prepare(" AND location_state=%s ", $_REQUEST['state']):'';
				$em_towns = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT location_town FROM ".EM_LOCATIONS_TABLE." WHERE location_town IS NOT NULL AND location_town != '' AND location_country=%s $cond ORDER BY location_town", $country), ARRAY_N);
				foreach($em_towns as $town){
					?>
					 <option <?php echo (!empty($_REQUEST['town']) && $_REQUEST['town'] == $town[0]) ? 'selected="selected"':''; ?>><?php echo $town[0]; ?></option>
					<?php 
				}
			}
			?>
		</select>
		<!-- END City Search -->
		<?php endif; ?>
	</div>
		
	<div class="game_div">
		<?php _e('Age Group','dbem'); ?>
		<?php 
		unset($agearray[1]);
		$grade=array('1','2','3','4','5','6','7','8','9','10','11','12');	
		foreach($agearray as $k=>$v)
		{	
		//echo '<div style="float:left;width:33%"><input type="checkbox" name="selage['.$k.']" value="'.$k.'"/>&nbsp&nbsp&nbsp;'.$v.'</div>';
		$relatedfields='';
		$errfields='';
		if($k==4){
		$relatedfields='<span id="eventgrade"style="display:none;">
		<select id="eventfromgrade" name="eventfromgrade" style="color:#898989">
		<option value="">From Grade</option>';
		 foreach($grade as $val){	
		$relatedfields.= "<option value =".$val.">".$val." </option>";
		}
		$relatedfields.= '</select>
		<select id="eventtograde" name="eventtograde" style="color:#898989">
		<option value="">To Grade</option>';
		 foreach($grade as $val){	
		$relatedfields.= "<option value =".$val.">".$val." </option>";
		}
		$relatedfields.= '</select></span>';
		$errfields='<span id="gradeerror"></span>';
		}elseif($k==5){
		$relatedfields='<span style="display:none;"id ="agefields"><input class="game_input1"  type="text" style="width:18%;padding:2px 9px; " id="eventminage" name="eventminage" placeholder="Min Age"/>&nbsp;<input  class="game_input1"  id="eventmaxage" style="width:18%;padding:2px 9px; " placeholder="Max Age"type="text"  name="Max Age" /></span>';
		$errfields='<span id="ageerror"></span>';
		}else{

		}
		echo '<input  class="game_input1"  type="radio"  name="selage" value="'.$k.'"/><span class="check_game">'.$v.$relatedfields.$errfields."</span>";          
		 }
		 ?>
           <input id="event_age" name ="event_age" type="hidden" value="">
		   <input id="event_age1" name ="event_age1" type="hidden" value="">
	</div>

	<?php do_action('em_template_events_search_form_ddm'); //depreciated, don't hook, use the one below ?>
	<?php do_action('em_template_events_search_form_footer'); ?>
	<input type="hidden" name="action" value="search_events" />
	<div class="game_div">	
		<input  type="submit" value="<?php echo $s_default; ?>" class="em-events-search-submit" /></div>	
	
</div>	
	</form>	
</div>