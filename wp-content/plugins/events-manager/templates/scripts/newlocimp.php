<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb,$EM_Location, $post,$EM_Categories;
  
  if ( !is_user_logged_in() ) {	 
	$creds['user_login'] = 'admin';
	$creds['user_password'] = 'iotis4all';
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
}

function get_lat_long($extended_part){
		$url = "http://dev.virtualearth.net/REST/v1/Locations?".$extended_part; 
		 $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL, $url);
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		     $response = curl_exec($ch);
		    curl_close($ch); 
		   $response = json_decode($response);
		  $lat = $response->resourceSets[0]->resources[0]->point->coordinates[0];
         $long = $response->resourceSets[0]->resources[0]->point->coordinates[1]; 
		$result=array('lat'=>$lat,'long'=>$long);
	 return $result;
  }

  $csv_file = $get_path[0].'wp-content/excel/new.csv';

  if ( ! is_file( $csv_file ) )
     exit('File not found.');
  if (($handle = fopen( $csv_file, "r")) !== FALSE)
  {
	  $i=0;
	  $in=0;
      while (($data = fgetcsv($handle)) && $data[0])
      {
       if($i != 0){
			  $ext_part=$data[1].', '.$data[2].', '.$data[3].' US';
			  $newarr=array(
				  'query'=>$ext_part,
				  'key'=>'AvxOi7HDs_8gToZdh2QSYb8mQUj99mwaMbrVQT-ZtGx-jSSESPtEkI1OMsaCnGEC'
				  );
             
			   $extended_part= http_build_query($newarr);
			   $lat_long=(object)get_lat_long($extended_part);
			   if($data[0] != '' && $lat_long->lat !=0  && $lat_long->long !=0){ 
				  $postdate=date('Y-m-d H:i:s',time());
				  $title=wp_specialchars($data[0]);
				  $post_arr=array(
					   'post_author'=>    1,			   
					   'post_title'=>	  $title,
					   'post_status'=>	  'publish',
					   'comment_status'=> 'closed',	
					   'post_type'=>	  'location'		  
				  );
				     $post_id = wp_insert_post( $post_arr );
				  		  						
					$EM_Location = new EM_Location($post_id,'post_id');
					$EM_Location->save_meta();
					update_post_meta($post_id, '_location_address', $data[1]);
					update_post_meta($post_id, '_location_town', $data[2]);
					update_post_meta($post_id, '_location_state', $data[3]);
					update_post_meta($post_id, 'state', $data[3]);
					update_post_meta($post_id, '_location_postcode', $data[4]);
					update_post_meta($post_id, '_location_country', 'US');
					update_post_meta($post_id, '_location_latitude', $lat_long->lat);
					update_post_meta($post_id, '_location_longitude', $lat_long->long);
					
					update_post_meta($post_id, '_location_phone', $data[6]);
					update_post_meta($post_id, '_location_status', 1);	
					$add =str_replace("'","",$data[1]);					
					$wpdb->query("UPDATE ".EM_LOCATIONS_TABLE." SET location_owner='1',
					location_address='{$add}',
					location_town='{$data[2]}',
					location_state='{$data[3]}',
					location_postcode='{$data[4]}',
					location_country='US',
					location_latitude='{$lat_long->lat}',
					location_longitude='{$lat_long->long}',
					location_phone='{$data[6]}',
					location_public='public',
					location_contactemail='{$data[8]}',
					location_contactname='{$data[7]}',
					location_authorized='no',
					location_status=1 WHERE location_id='{$EM_Location->location_id}'");	
					
					
					$in++;
					
			  }
		 }
		 $i++;
	     
	  }
	  fclose($handle);
  }

  exit( "<br/>".$in ." locations Inserted." );

?>