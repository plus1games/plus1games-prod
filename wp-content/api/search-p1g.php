<?php
/*******************************************/
//Server variable configuration
ini_set('safe_mode', 'Off');
ini_set('max_file_uploads', '400M');
ini_set('max_execution_time', '3000');
ini_set('max_input_time', '3000');
ini_set('memory_limit', '-1');
ini_set('output_buffering', '4096');

error_reporting(E_ALL);
ini_set("display_errors", 1);
/*******************************************/

$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $current_cityinfo;
$searchString = $_REQUEST['search_string'];
$searchRadius = $_REQUEST['radius'];
$lat = $_REQUEST['latitude'];
$long = $_REQUEST['longitude'];
$miles_range=explode('-',$searchRadius);
$to_miles = trim($miles_range[0]);
$miles = trim($miles_range[1]);
if($miles ==''){ $miles = '1000'; }	

if(empty($lat) && empty($long)){
	if(is_plugin_active('Tevolution-LocationManager/location-manager')){
		$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$url = "http://freegeoip.net/json/$ip";
		$data=wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
		if ($data) {
			$location = json_decode($data['body']);				
			$lat = $location->latitude;
			$long = $location->longitude;
		}else{
			$lat =$current_cityinfo['lat'];
			$long = $current_cityinfo['lng'];
		}
	}else{ /* if location manager is deactiv then get latitude and longitude from map setting and find results from that location */

		$city_googlemap_setting = get_option('city_googlemap_setting');
		$lat =$city_googlemap_setting['map_city_latitude'];
		$long = $city_googlemap_setting['map_city_longitude'];
		
		if(empty($lat) || empty($long)){
		
			$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
			$url = "http://freegeoip.net/json/$ip";
			$data=wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
			
			if ($data) {
				$location = json_decode($data['body']);				
				$lat = $location->latitude;
				$long = $location->longitude;
			}
		}
	}
}

$result = array();
$result['events'] = array();
$result['parks'] = array();
$result['groups'] = array();
$result['goods'] = array();

if(!empty($searchString) && !empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT * FROM (SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value = '".$searchString."') OR (meta_key='_location_state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')
	UNION DISTINCT
	SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value like '%".$searchString."%') OR (meta_key='_location_state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')) as new_redious
	ORDER BY redious";

	$eventSearchResult = $wpdb->get_col($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $eventPostId) {
			$eventPostmeta = get_post_meta( $eventPostId );
			$eventQuery = "SELECT * FROM wp_em_events WHERE event_id = ".$eventPostmeta['_event_id'][0];
			$eventDetail = $wpdb->get_results($eventQuery,ARRAY_A);
			
			if(!empty($eventDetail)) {			
				foreach($eventDetail as $v){
					$event_id= $v['event_id'];
					$result['events']['records'][$event_id]= $v;
					$result['events']['records'][$event_id]['location']=array();
					$result['events']['records'][$event_id]['location']['location_name']=$eventPostmeta['_location_name'][0];
					$result['events']['records'][$event_id]['location']['address']=$eventPostmeta['_location_address'][0];
					$result['events']['records'][$event_id]['location']['city']=$eventPostmeta['_location_town'][0];
					$result['events']['records'][$event_id]['location']['state']=$eventPostmeta['_location_state'][0];
					$result['events']['records'][$event_id]['location']['latitude']=$eventPostmeta['_location_latitude'][0];
					$result['events']['records'][$event_id]['location']['longitude']=$eventPostmeta['_location_longitude'][0];
				}
			} else {
				$result['events']['records']= 'No event found';
			}
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT * FROM (SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345) 
	UNION DISTINCT
	SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345)) as new_redious
	ORDER BY redious";

	$parksSearchResult = $wpdb->get_col($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $parksSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 345, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$parksQuery = new WP_Query( $args );
		if(!empty($parksQuery)){
			foreach($parksQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['parks']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['parks']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['parks']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['parks']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['parks']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['parks']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['parks']['records']='No Park Found';

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT * FROM (SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)
	UNION 
	SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)) as new_redious
	ORDER BY redious";

	$groupsSearchResult = $wpdb->get_col($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $groupsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 406, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$groupsQuery = new WP_Query( $args );
		if(!empty($groupsQuery)){
			foreach($groupsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['groups']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['groups']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['groups']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['groups']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['groups']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['groups']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['groups']['records']='No Sport Groups Found';

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT * FROM (SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)
	UNION DISTINCT
	SELECT DISTINCT id, truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) as redious FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)) as new_redious
	ORDER BY redious";

	$goodsSearchResult = $wpdb->get_col($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $goodsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 400, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$goodsQuery = new WP_Query( $args );
		if(!empty($goodsQuery)){
			foreach($goodsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['goods']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['goods']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['goods']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['goods']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['goods']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['goods']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['goods']['records']='No Sporting Goods Found';

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'success';
	//echo "<pre>";print_r($result);echo "</pre>";
	echo json_encode($result);

} elseif (!empty($searchString) && empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value = '".$searchString."') OR (meta_key='_location_state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND wp_posts.post_status = 'publish' AND ((meta_key='_location_town' AND meta_value like '%".$searchString."%') OR (meta_key='_location_state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')";

	$eventSearchResult = $wpdb->get_col($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $eventPostId) {
			$eventPostmeta = get_post_meta( $eventPostId );
			$eventQuery = "SELECT * FROM wp_em_events WHERE event_id = ".$eventPostmeta['_event_id'][0];
			$eventDetail = $wpdb->get_results($eventQuery,ARRAY_A);
			
			if(!empty($eventDetail)) {			
				foreach($eventDetail as $v){
					$event_id= $v['event_id'];
					$result['events']['records'][$event_id]= $v;
					$result['events']['records'][$event_id]['location']=array();
					$result['events']['records'][$event_id]['location']['location_name']=$eventPostmeta['_location_name'][0];
					$result['events']['records'][$event_id]['location']['address']=$eventPostmeta['_location_address'][0];
					$result['events']['records'][$event_id]['location']['city']=$eventPostmeta['_location_town'][0];
					$result['events']['records'][$event_id]['location']['state']=$eventPostmeta['_location_state'][0];
					$result['events']['records'][$event_id]['location']['latitude']=$eventPostmeta['_location_latitude'][0];
					$result['events']['records'][$event_id]['location']['longitude']=$eventPostmeta['_location_longitude'][0];
				}
			} else {
				$result['events']['records']= 'No event found';
			}
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345) 
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345)";

	$parksSearchResult = $wpdb->get_col($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $parksSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 345, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$parksQuery = new WP_Query( $args );
		if(!empty($parksQuery)){
			foreach($parksQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['parks']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['parks']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['parks']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['parks']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['parks']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['parks']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['parks']['records']='No Park Found';

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)";

	$groupsSearchResult = $wpdb->get_col($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $groupsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 406, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$groupsQuery = new WP_Query( $args );
		if(!empty($groupsQuery)){
			foreach($groupsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['groups']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['groups']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['groups']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['groups']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['groups']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['groups']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['groups']['records']='No Sport Groups Found';

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value = '".$searchString."') OR (meta_key='state' AND meta_value = '".$searchString."') OR post_title = '".$searchString."')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postmeta ON (wp_postmeta.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND wp_posts.post_status = 'publish' AND ((meta_key='city' AND meta_value like '%".$searchString."%') OR (meta_key='state' AND meta_value like '%".$searchString."%') OR post_title like '%".$searchString."%')) INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)";

	$goodsSearchResult = $wpdb->get_col($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $goodsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 400, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$goodsQuery = new WP_Query( $args );
		if(!empty($goodsQuery)){
			foreach($goodsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['goods']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['goods']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['goods']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['goods']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['goods']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['goods']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['goods']['records']='No Sporting Goods Found';

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'radius, latitude, longitude parameters are empty';
	//echo "<pre>";print_r($result);echo "</pre>";
	echo json_encode($result);

} elseif (empty($searchString) && !empty($searchRadius)) {

	/*******************************************************************************/
	//Code is for events with search string
	$eventSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_em_locations ON (wp_em_locations.post_id = wp_posts.id AND wp_posts.post_type = 'event' AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`location_latitude`)) * sin( radians('".$lat."')) + cos(radians(`location_latitude`)) * cos( radians('".$lat."')) * cos( radians(`location_longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'event-categories')";

	$eventSearchResult = $wpdb->get_col($eventSearchQuery);
	$result['events']['result']= 'success';
	$result['events']['total_events'] = count($eventSearchResult);
	$result['events']['records'] = array();
	if(!empty($eventSearchResult)){
		foreach ($eventSearchResult as $eventPostId) {
			$eventPostmeta = get_post_meta( $eventPostId );
			$eventQuery = "SELECT * FROM wp_em_events WHERE event_id = ".$eventPostmeta['_event_id'][0];
			$eventDetail = $wpdb->get_results($eventQuery,ARRAY_A);
			
			if(!empty($eventDetail)) {			
				foreach($eventDetail as $v){
					$event_id= $v['event_id'];
					$result['events']['records'][$event_id]= $v;
					$result['events']['records'][$event_id]['location']=array();
					$result['events']['records'][$event_id]['location']['location_name']=$eventPostmeta['_location_name'][0];
					$result['events']['records'][$event_id]['location']['address']=$eventPostmeta['_location_address'][0];
					$result['events']['records'][$event_id]['location']['city']=$eventPostmeta['_location_town'][0];
					$result['events']['records'][$event_id]['location']['state']=$eventPostmeta['_location_state'][0];
					$result['events']['records'][$event_id]['location']['latitude']=$eventPostmeta['_location_latitude'][0];
					$result['events']['records'][$event_id]['location']['longitude']=$eventPostmeta['_location_longitude'][0];
				}
			} else {
				$result['events']['records']= 'No event found';
			}
		}
	} else $result['events']['records']= 'No event found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for places(parks) with search string
	$parksSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345) 
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 345)";

	$parksSearchResult = $wpdb->get_col($parksSearchQuery);
	$result['parks']['result']= 'success';
	$result['parks']['total_parks'] = count($parksSearchResult);
	$result['parks']['records'] = array();
	if(!empty($parksSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $parksSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 345, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$parksQuery = new WP_Query( $args );
		if(!empty($parksQuery)){
			foreach($parksQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['parks']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['parks']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['parks']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['parks']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['parks']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['parks']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['parks']['records']='No Park Found';

	} else $result['parks']['records']='No Park Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport groups with search string
	$groupsSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 406)";

	$groupsSearchResult = $wpdb->get_col($groupsSearchQuery);
	$result['groups']['result']= 'success';
	$result['groups']['total_groups'] = count($groupsSearchResult);
	$result['groups']['records'] = array();
	if(!empty($groupsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $groupsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 406, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$groupsQuery = new WP_Query( $args );
		if(!empty($groupsQuery)){
			foreach($groupsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['groups']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['groups']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['groups']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['groups']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['groups']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['groups']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['groups']['records']='No Sport Groups Found';

	} else $result['groups']['records']='No Sport Groups Found';
	/*******************************************************************************/


	/*******************************************************************************/
	//Code is for sport goods with search string
	$goodsSearchQuery = "SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)
	UNION DISTINCT
	SELECT DISTINCT id FROM wp_posts INNER JOIN wp_postcodes ON (wp_postcodes.post_id = wp_posts.id AND wp_posts.post_type = 'listing' AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) <= ".$miles." AND truncate((degrees(acos( sin(radians(`latitude`)) * sin( radians('".$lat."')) + cos(radians(`latitude`)) * cos( radians('".$lat."')) * cos( radians(`longitude` - '".$long."') ) ) ) * 69.09),1) >= ".$to_miles.") INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) LEFT JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id) INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'listingcategory' AND wp_term_taxonomy.term_id = 400)";

	$goodsSearchResult = $wpdb->get_col($goodsSearchQuery);
	$result['goods']['result']= 'success';
	$result['goods']['total_goods'] = count($goodsSearchResult);
	$result['goods']['records'] = array();
	if(!empty($goodsSearchResult)){
		
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'post__in'               => $goodsSearchResult,
			'posts_per_page'		 => '-1',
			'tax_query' => array(array(
				'taxonomy' => 'listingcategory', 
				'field' => 'id',
				'terms' => 400, 
				'include_children' => true, 
				'operator' => 'IN',
				)),
			);
		$goodsQuery = new WP_Query( $args );
		if(!empty($goodsQuery)){
			foreach($goodsQuery as $key=>$val){
				if($key == 'posts'){
					foreach($val as $k=>$v){
						
						$v = get_object_vars($v);
						
						$result['goods']['records'][$v['ID']]=$v;
						$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
						$image = wp_get_attachment_image_src( $featuredimg_id );
						if(!empty($image))
							$result['goods']['records'][$v['ID']]['featured_image']=$image[0];
						$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
						$resultimages=$wpdb->get_results($sqlimage);
						$result['goods']['records'][$v['ID']]['images']=array();
						if(!empty($resultimages)){
							foreach($resultimages as $img){
								if($img->ID !=$featuredimg_id){
									$result['goods']['records'][$v['ID']]['images'][]=$img->guid;
								}
							}
						}
						$result['goods']['records'][$v['ID']]['meta']=get_post_meta($v['ID']);
						$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						if(!empty($terminfo)){
							foreach($terminfo as $t){
								$t = get_object_vars($t);
								$result['goods']['records'][$v['ID']]['term'][]=$t;
							}
						}
					}
				}
			}
		} else $result['goods']['records']='No Sporting Goods Found';

	} else $result['goods']['records']='No Sporting Goods Found';
	/*******************************************************************************/
	
	$result['result']= 'search_string parameter is empty';
	//echo "<pre>";print_r($result);echo "</pre>";
	echo json_encode($result);

} elseif (empty($searchString) && empty($searchRadius)) {

	$result['result']= 'search_string, radius parameters are empty';

	$result['events']['result']= 'unsuccess';
	$result['events']['total_events'] = 0;
	$result['events']['records']= 'No event found';


	$result['parks']['result']= 'unsuccess';
	$result['parks']['total_parks'] = 0;
	$result['parks']['records']='No Park Found';


	$result['groups']['result']= 'unsuccess';
	$result['groups']['total_groups'] = 0;
	$result['groups']['records']='No Sport Groups Found';


	$result['goods']['result']= 'unsuccess';
	$result['goods']['total_goods'] = 0;
	$result['goods']['records']='No Sporting Goods Found';

	echo json_encode($result);
}
?>