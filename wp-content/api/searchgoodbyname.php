<?php
header('Content-type: application/json');
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;
if($_REQUEST['name'] !=''){
	$args = array (
		'post_type'              => 'listing',
		'post_status'            => 'publish',
		's'                      => $_REQUEST['name'],
		'posts_per_page'		 => '-1',
		'tax_query' => array(array(
										'taxonomy' => 'listingcategory', 
										'field' => 'id',
										'terms' => 400, 
										'include_children' => true,
								)),
	);

	$query = new WP_Query( $args );

	foreach($query as $key=>$val){
		if($key == 'posts'){
			$goods =array();
			$goods['result']= 'success';
			$goods['total_sporting_goods']=0;
			$goods['goods']=array();
			$i=0;
			foreach($val as $k=>$v){
				
				$v = get_object_vars($v);
				if (stripos($v['post_title'],$_REQUEST['name']) !== false) {
					$goods['total_sporting_goods']= $i+1;
					$goods['goods'][$v['ID']]=$v;
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ) );
					$goods['goods'][$v['ID']]['featured_image']=$image[0];
					$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
					$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
					$resultimages=$wpdb->get_results($sqlimage);
					$goods['goods'][$v['ID']]['images']=array();
					foreach($resultimages as $img){
						if($img->ID !=$featuredimg_id){
							$goods['goods'][$v['ID']]['images'][]=$img->guid;
						}
					}
					$goods['goods'][$v['ID']]['meta']=get_post_meta($v['ID']);
					$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
					foreach($terminfo as $t){
						$t = get_object_vars($t);
						$goods['goods'][$v['ID']]['term'][]=$t;
					}
					$i++;
				}
			}
		}
	}
}
if(empty($goods['goods'])){
	$goods['goods']=array();
	$goods['goods']='No Sporting Goods Found';
}
echo json_encode($goods);

?>