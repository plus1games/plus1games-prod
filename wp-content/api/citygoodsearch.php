<?php
header('Content-type: application/json');
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;
if(isset($_REQUEST['cityname']) ) {
	$good=array();
	$good['result']= 'success';
	$good['total_sporting_goods']=0;
	if($_REQUEST['cityname'] !=''){
		$args = array (
			'post_type'              => 'listing',
			'post_status'            => 'publish',
			'posts_per_page'		 => '-1',
			'tax_query'				 => array(array(
											'taxonomy' => 'listingcategory', 
											'field' => 'id',
											'terms' => 400, 
											'include_children' => true,
									)),
			'meta_query'			=>  array(array(
													  'key'     => 'city',
													  'value'   => $_REQUEST['cityname'],
													  'compare' => '='
													)
											),
		);

		$query = new WP_Query( $args );
		
		foreach($query as $key=>$val){
			if($key == 'posts'){
				$i=0;
				foreach($val as $v){
					$good['total_sporting_goods']= $i+1;
					$v = get_object_vars($v);
					$good['goods'][$v['ID']]=$v;
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $v['ID'] ) );
					$good['goods'][$v['ID']]['featured_image']=$image[0];
					$featuredimg_id = get_post_thumbnail_id( $v['ID'] );
					$sqlimage = "SELECT ID, guid FROM wp_posts WHERE post_parent=".$v['ID']." AND post_type='attachment'";
					$resultimages=$wpdb->get_results($sqlimage);
					$good['goods'][$v['ID']]['images']=array();
					foreach($resultimages as $img){
						if($img->ID != $featuredimg_id){
							$good['goods'][$v['ID']]['images'][]=$img->guid;
						}
					}
					$good['goods'][$v['ID']]['meta']=get_post_meta($v['ID']);
					$terminfo = wp_get_post_terms($v['ID'], 'listingcategory');
						foreach($terminfo as $t){
							$t = get_object_vars($t);
							$good['goods'][$v['ID']]['term'][]=$t;
						}
					$i++;
				}
			}
		}
	}

	if(empty($good['goods'])){
		$good['goods']=array();
		$good['goods']='No Sporting Goods Found';
	}
	//print_r($good);
	echo json_encode($good);
}
?>