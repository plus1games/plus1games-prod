<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);
global $wpdb, $post;

$args = array (
		'post_type'              => 'listing',
		'post_status'            => 'publish',
		'posts_per_page'		 => '10000',
		'offset'				 =>  20000,
		'tax_query' => array(array(
										'taxonomy' => 'listingcategory', 
										'field' => 'id',
										'terms' => 345, 
										'include_children' => true,
								)),
	);
	$query = new WP_Query( $args );
	$parks=array();
	foreach($query as $key=>$value){
		if($key =='posts'){
			foreach($value as $k=>$v){
				$v = get_object_vars($v);
				$fiedsarr=array('ID','post_content','post_title','post_status','post_author');
				foreach($v as $t=>$tv){
					if(in_array($t,$fiedsarr)){
						$parks[$v['ID']][$t] = $tv;
					}
				}
				//$parks[$v['ID']] = $v;
				$post_meta = get_post_meta($v['ID']);
				$metafieldarr=array('address','phone','email','website','state','city','zip');
				$metaarr=array();
				foreach($post_meta as $mk=>$mv){
					if(in_array($mk,$metafieldarr)) {
						$metaarr[$mk]=$mv[0];
					}
				}
				$parks[$v['ID']] = array_merge($parks[$v['ID']],$metaarr);
			}
		}
	}
	//pr($parks);

	$fp = fopen('parks_reportnew3.xls', 'w');
	$csvtitle= array('Park Name','address','city','state','zip','phone','email','website');
			fputcsv($fp, $csvtitle, "\t", '"');
			foreach ($parks as $k=>$fields) {
				$newarr['Park Name']=$fields['post_title'];
				$newarr['address']=$fields['address'];
				$newarr['city']=$fields['city'];
				$newarr['state']=$fields['state'];
				$newarr['zip']=$fields['zip'];
				$newarr['phone']=$fields['phone'];
				$newarr['email']=$fields['email'];
				$newarr['website']=$fields['website'];
				fputcsv($fp, $newarr, "\t", '"');
				
			}
			fclose($fp);
?>