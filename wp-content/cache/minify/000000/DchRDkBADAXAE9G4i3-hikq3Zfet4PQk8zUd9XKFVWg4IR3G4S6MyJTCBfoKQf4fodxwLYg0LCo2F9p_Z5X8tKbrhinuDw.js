
/* jquery.lightbox.js */

/* 1   */ /**
/* 2   *|  * jQuery lightBox plugin
/* 3   *|  * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
/* 4   *|  * and adapted to me for use like a plugin from jQuery.
/* 5   *|  * @name jquery-lightbox-0.5.js
/* 6   *|  * @author Leandro Vieira Pinho - http://leandrovieira.com
/* 7   *|  * @version 0.5
/* 8   *|  * @date April 11, 2008
/* 9   *|  * @category jQuery plugin
/* 10  *|  * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
/* 11  *|  * @license CCAttribution-ShareAlike 2.5 Brazil - http://creativecommons.org/licenses/by-sa/2.5/br/deed.en_US
/* 12  *|  * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
/* 13  *|  */
/* 14  */ // Offering a Custom Alias suport - More info: http://docs.jquery.com/Plugins/Authoring#Custom_Alias
/* 15  */ (function($) {
/* 16  */ 	/**
/* 17  *| 	 * $ is an alias to jQuery object
/* 18  *| 	 *
/* 19  *| 	 */
/* 20  */ 	$.fn.lightBox = function(settings) {
/* 21  */ 		// Settings to configure the jQuery lightBox plugin how you like
/* 22  */ 		settings = jQuery.extend({
/* 23  */ 			// Configuration related to overlay
/* 24  */ 			overlayBgColor: 		'#000',		// (string) Background color to overlay; inform a hexadecimal value like: #RRGGBB. Where RR, GG, and BB are the hexadecimal values for the red, green, and blue values of the color.
/* 25  */ 			overlayOpacity:			0.8,		// (integer) Opacity value to overlay; inform: 0.X. Where X are number from 0 to 9
/* 26  */ 			// Configuration related to navigation
/* 27  */ 			fixedNavigation:		false,		// (boolean) Boolean that informs if the navigation (next and prev button) will be fixed or not in the interface.
/* 28  */ 			// Configuration related to images
/* 29  */ 			imageLoading:			IMAGE_LOADING,		// (string) Path and the name of the loading icon
/* 30  */ 			imageBtnPrev:			IMAGE_PREV,			// (string) Path and the name of the prev button image
/* 31  */ 			imageBtnNext:			IMAGE_NEXT,			// (string) Path and the name of the next button image
/* 32  */ 			imageBtnClose:			IMAGE_CLOSE,		// (string) Path and the name of the close btn
/* 33  */ 			imageBlank:				IMAGE_BLANK,			// (string) Path and the name of a blank image (one pixel)
/* 34  */ 			// Configuration related to container image box
/* 35  */ 			containerBorderSize:	10,			// (integer) If you adjust the padding in the CSS for the container, #lightbox-container-image-box, you will need to update this value
/* 36  */ 			containerResizeSpeed:	400,		// (integer) Specify the resize duration of container image. These number are miliseconds. 400 is default.
/* 37  */ 			// Configuration related to texts in caption. For example: Image 2 of 8. You can alter either "Image" and "of" texts.
/* 38  */ 			txtImage:				'Image',	// (string) Specify text "Image"
/* 39  */ 			txtOf:					'of',		// (string) Specify text "of"
/* 40  */ 			// Configuration related to keyboard navigation
/* 41  */ 			keyToClose:				'c',		// (string) (c = close) Letter to close the jQuery lightBox interface. Beyond this letter, the letter X and the SCAPE key is used to.
/* 42  */ 			keyToPrev:				'p',		// (string) (p = previous) Letter to show the previous image
/* 43  */ 			keyToNext:				'n',		// (string) (n = next) Letter to show the next image.
/* 44  */ 			// Don�t alter these variables in any way
/* 45  */ 			imageArray:				[],
/* 46  */ 			activeImage:			0
/* 47  */ 		},settings);
/* 48  */ 		// Caching the jQuery object with all elements matched
/* 49  */ 		var jQueryMatchedObj = this; // This, in this context, refer to jQuery object
/* 50  */ 		/**

/* jquery.lightbox.js */

/* 51  *| 		 * Initializing the plugin calling the start function
/* 52  *| 		 *
/* 53  *| 		 * @return boolean false
/* 54  *| 		 */
/* 55  */ 		function _initialize() {
/* 56  */ 			_start(this,jQueryMatchedObj); // This, in this context, refer to object (link) which the user have clicked
/* 57  */ 			return false; // Avoid the browser following the link
/* 58  */ 		}
/* 59  */ 		/**
/* 60  *| 		 * Start the jQuery lightBox plugin
/* 61  *| 		 *
/* 62  *| 		 * @param object objClicked The object (link) whick the user have clicked
/* 63  *| 		 * @param object jQueryMatchedObj The jQuery object with all elements matched
/* 64  *| 		 */
/* 65  */ 		function _start(objClicked,jQueryMatchedObj) {
/* 66  */ 			// Hime some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
/* 67  */ 			$('embed, object, select').css({ 'visibility' : 'hidden' });
/* 68  */ 			// Call the function to create the markup structure; style some elements; assign events in some elements.
/* 69  */ 			_set_interface();
/* 70  */ 			// Unset total images in imageArray
/* 71  */ 			settings.imageArray.length = 0;
/* 72  */ 			// Unset image active information
/* 73  */ 			settings.activeImage = 0;
/* 74  */ 			// We have an image set? Or just an image? Let�s see it.
/* 75  */ 			if ( jQueryMatchedObj.length == 1 ) {
/* 76  */ 				settings.imageArray.push(new Array(objClicked.getAttribute('href'),objClicked.getAttribute('title')));
/* 77  */ 			} else {
/* 78  */ 				// Add an Array (as many as we have), with href and title atributes, inside the Array that storage the images references		
/* 79  */ 				for ( var i = 0; i < jQueryMatchedObj.length; i++ ) {
/* 80  */ 					settings.imageArray.push(new Array(jQueryMatchedObj[i].getAttribute('href'),jQueryMatchedObj[i].getAttribute('title')));
/* 81  */ 				}
/* 82  */ 			}
/* 83  */ 			while ( settings.imageArray[settings.activeImage][0] != objClicked.getAttribute('href') ) {
/* 84  */ 				settings.activeImage++;
/* 85  */ 			}
/* 86  */ 			// Call the function that prepares image exibition
/* 87  */ 			_set_image_to_view();
/* 88  */ 		}
/* 89  */ 		/**
/* 90  *| 		 * Create the jQuery lightBox plugin interface
/* 91  *| 		 *
/* 92  *| 		 * The HTML markup will be like that:
/* 93  *| 			<div id="jquery-overlay"></div>
/* 94  *| 			<div id="jquery-lightbox">
/* 95  *| 				<div id="lightbox-container-image-box">
/* 96  *| 					<div id="lightbox-container-image">
/* 97  *| 						<img src="../fotos/XX.jpg" id="lightbox-image">
/* 98  *| 						<div id="lightbox-nav">
/* 99  *| 							<a href="#" id="lightbox-nav-btnPrev"></a>
/* 100 *| 							<a href="#" id="lightbox-nav-btnNext"></a>

/* jquery.lightbox.js */

/* 101 *| 						</div>
/* 102 *| 						<div id="lightbox-loading">
/* 103 *| 							<a href="#" id="lightbox-loading-link">
/* 104 *| 								<img src="../images/lightbox-ico-loading.gif">
/* 105 *| 							</a>
/* 106 *| 						</div>
/* 107 *| 					</div>
/* 108 *| 				</div>
/* 109 *| 				<div id="lightbox-container-image-data-box">
/* 110 *| 					<div id="lightbox-container-image-data">
/* 111 *| 						<div id="lightbox-image-details">
/* 112 *| 							<span id="lightbox-image-details-caption"></span>
/* 113 *| 							<span id="lightbox-image-details-currentNumber"></span>
/* 114 *| 						</div>
/* 115 *| 						<div id="lightbox-secNav">
/* 116 *| 							<a href="#" id="lightbox-secNav-btnClose">
/* 117 *| 								<img src="../images/lightbox-btn-close.gif">
/* 118 *| 							</a>
/* 119 *| 						</div>
/* 120 *| 					</div>
/* 121 *| 				</div>
/* 122 *| 			</div>
/* 123 *| 		 *
/* 124 *| 		 */
/* 125 */ 		function _set_interface() {
/* 126 */ 			// Apply the HTML markup into body tag
/* 127 */ 			$('body').append('<div id="jquery-overlay"></div><div id="jquery-lightbox"><div class="lightbox-container-close-button"><div id="lightbox-container-image-box"><div id="lightbox-container-image"><img id="lightbox-image"><div style="" id="lightbox-nav"><a href="#" id="lightbox-nav-btnPrev"></a><a href="#" id="lightbox-nav-btnNext"></a></div><div id="lightbox-loading"><a href="#" id="lightbox-loading-link"><img src="' + settings.imageLoading + '"></a></div></div></div><div id="lightbox-container-image-data-box"><div id="lightbox-container-image-data"><div id="lightbox-image-details"><span id="lightbox-image-details-caption"></span><span id="lightbox-image-details-currentNumber"></span></div><div id="lightbox-secNav"><a href="#" id="lightbox-secNav-btnClose"><img src="' + settings.imageBtnClose + '"></a></div></div></div></div></div>');	
/* 128 */ 			// Get page sizes
/* 129 */ 			var arrPageSizes = ___getPageSize();
/* 130 */ 			// Style overlay and show it
/* 131 */ 			$('#jquery-overlay').css({
/* 132 */ 				backgroundColor:	settings.overlayBgColor,
/* 133 */ 				opacity:			settings.overlayOpacity,
/* 134 */ 				width:				arrPageSizes[0],
/* 135 */ 				height:				arrPageSizes[1]
/* 136 */ 			}).fadeIn();
/* 137 */ 			// Get page scroll
/* 138 */ 			var arrPageScroll = ___getPageScroll();
/* 139 */ 			// Calculate top and left offset for the jquery-lightbox div object and show it
/* 140 */ 			$('#jquery-lightbox').css({
/* 141 */ 				top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
/* 142 */ 				left:	arrPageScroll[0]
/* 143 */ 			}).show();
/* 144 */ 			// Assigning click events in elements to close overlay
/* 145 */ 			$('#jquery-overlay,#jquery-lightbox').click(function() {
/* 146 */ 				_finish();									
/* 147 */ 			});
/* 148 */ 			// Assign the _finish function to lightbox-loading-link and lightbox-secNav-btnClose objects
/* 149 */ 			$('#lightbox-loading-link,#lightbox-secNav-btnClose').click(function() {
/* 150 */ 				_finish();

/* jquery.lightbox.js */

/* 151 */ 				return false;
/* 152 */ 			});
/* 153 */ 			// If window was resized, calculate the new overlay dimensions
/* 154 */ 			$(window).resize(function() {
/* 155 */ 				// Get page sizes
/* 156 */ 				var arrPageSizes = ___getPageSize();
/* 157 */ 				// Style overlay and show it
/* 158 */ 				$('#jquery-overlay').css({
/* 159 */ 					width:		arrPageSizes[0],
/* 160 */ 					height:		arrPageSizes[1]
/* 161 */ 				});
/* 162 */ 				// Get page scroll
/* 163 */ 				var arrPageScroll = ___getPageScroll();
/* 164 */ 				// Calculate top and left offset for the jquery-lightbox div object and show it
/* 165 */ 				$('#jquery-lightbox').css({
/* 166 */ 					top:	arrPageScroll[1] + (arrPageSizes[3] / 10),
/* 167 */ 					left:	arrPageScroll[0]
/* 168 */ 				});
/* 169 */ 			});
/* 170 */ 		}
/* 171 */ 		/**
/* 172 *| 		 * Prepares image exibition; doing a image�s preloader to calculate it�s size
/* 173 *| 		 *
/* 174 *| 		 */
/* 175 */ 		function _set_image_to_view() { // show the loading
/* 176 */ 			// Show the loading
/* 177 */ 			$('#lightbox-loading').show();
/* 178 */ 			if ( settings.fixedNavigation ) {
/* 179 */ 				$('#lightbox-image,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
/* 180 */ 			} else {
/* 181 */ 				// Hide some elements
/* 182 */ 				$('#lightbox-image,#lightbox-nav,#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber').hide();
/* 183 */ 			}
/* 184 */ 			// Image preload process
/* 185 */ 			var objImagePreloader = new Image();
/* 186 */ 			objImagePreloader.onload = function() {
/* 187 */ 				$('#lightbox-image').attr('src',settings.imageArray[settings.activeImage][0]);
/* 188 */ 				// Perfomance an effect in the image container resizing it
/* 189 */ 				_resize_container_image_box(objImagePreloader.width,objImagePreloader.height);
/* 190 */ 				//	clear onLoad, IE behaves irratically with animated gifs otherwise
/* 191 */ 				objImagePreloader.onload=function(){};
/* 192 */ 			};
/* 193 */ 			objImagePreloader.src = settings.imageArray[settings.activeImage][0];
/* 194 */ 		};
/* 195 */ 		/**
/* 196 *| 		 * Perfomance an effect in the image container resizing it
/* 197 *| 		 *
/* 198 *| 		 * @param integer intImageWidth The image�s width that will be showed
/* 199 *| 		 * @param integer intImageHeight The image�s height that will be showed
/* 200 *| 		 */

/* jquery.lightbox.js */

/* 201 */ 		function _resize_container_image_box(intImageWidth,intImageHeight) {
/* 202 */ 			// Get current width and height
/* 203 */ 			var intCurrentWidth = $('#lightbox-container-image-box').width();
/* 204 */ 			var intCurrentHeight = $('#lightbox-container-image-box').height();
/* 205 */ 			// Get the width and height of the selected image plus the padding
/* 206 */ 			var intWidth = (intImageWidth + (settings.containerBorderSize * 2)); // Plus the image�s width and the left and right padding value
/* 207 */ 			var intHeight = (intImageHeight + (settings.containerBorderSize * 2)); // Plus the image�s height and the left and right padding value
/* 208 */ 			// Diferences
/* 209 */ 			var intDiffW = intCurrentWidth - intWidth;
/* 210 */ 			var intDiffH = intCurrentHeight - intHeight;
/* 211 */ 			// Perfomance the effect
/* 212 */ 			$('#lightbox-container-image-box').animate({ width: intWidth, height: intHeight },settings.containerResizeSpeed,function() { _show_image(); });
/* 213 */ 			if ( ( intDiffW == 0 ) && ( intDiffH == 0 ) ) {
/* 214 */ 				if ( $.browser.msie ) {
/* 215 */ 					___pause(250);
/* 216 */ 				} else {
/* 217 */ 					___pause(100);	
/* 218 */ 				}
/* 219 */ 			} 
/* 220 */ 			$('#lightbox-container-image-data-box').css({ width: intImageWidth });
/* 221 */ 			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ height: intImageHeight + (settings.containerBorderSize * 2) });
/* 222 */ 		};
/* 223 */ 		/**
/* 224 *| 		 * Show the prepared image
/* 225 *| 		 *
/* 226 *| 		 */
/* 227 */ 		function _show_image() {
/* 228 */ 			$('#lightbox-loading').hide();
/* 229 */ 			$('#lightbox-image').fadeIn(function() {
/* 230 */ 				_show_image_data();
/* 231 */ 				_set_navigation();
/* 232 */ 			});
/* 233 */ 			_preload_neighbor_images();
/* 234 */ 		};
/* 235 */ 		/**
/* 236 *| 		 * Show the image information
/* 237 *| 		 *
/* 238 *| 		 */
/* 239 */ 		function _show_image_data() {
/* 240 */ 			$('#lightbox-container-image-data-box').slideDown('fast');
/* 241 */ 			$('#lightbox-image-details-caption').hide();
/* 242 */ 			if ( settings.imageArray[settings.activeImage][1] ) {
/* 243 */ 				$('#lightbox-image-details-caption').html(settings.imageArray[settings.activeImage][1]).show();
/* 244 */ 			}
/* 245 */ 			// If we have a image set, display 'Image X of X'
/* 246 */ 			if ( settings.imageArray.length > 1 ) {
/* 247 */ 				$('#lightbox-image-details-currentNumber').html(settings.txtImage + ' ' + ( settings.activeImage + 1 ) + ' ' + settings.txtOf + ' ' + settings.imageArray.length).show();
/* 248 */ 			}		
/* 249 */ 		}
/* 250 */ 		/**

/* jquery.lightbox.js */

/* 251 *| 		 * Display the button navigations
/* 252 *| 		 *
/* 253 *| 		 */
/* 254 */ 		function _set_navigation() {
/* 255 */ 			$('#lightbox-nav').show();
/* 256 */ 			// Instead to define this configuration in CSS file, we define here. And it�s need to IE. Just.
/* 257 */ 			$('#lightbox-nav-btnPrev,#lightbox-nav-btnNext').css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
/* 258 */ 			
/* 259 */ 			// Show the prev button, if not the first image in set
/* 260 */ 			if ( settings.activeImage != 0 ) {
/* 261 */ 				if ( settings.fixedNavigation ) {
/* 262 */ 					$('#lightbox-nav-btnPrev').css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' })
/* 263 */ 						.unbind()
/* 264 */ 						.bind('click',function() {
/* 265 */ 							settings.activeImage = settings.activeImage - 1;
/* 266 */ 							_set_image_to_view();
/* 267 */ 							return false;
/* 268 */ 						});
/* 269 */ 				} else {
/* 270 */ 					// Show the images button for Next buttons
/* 271 */ 					$('#lightbox-nav-btnPrev').unbind().hover(function() {
/* 272 */ 						$(this).css({ 'background' : 'url(' + settings.imageBtnPrev + ') left 15% no-repeat' });
/* 273 */ 					},function() {
/* 274 */ 						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
/* 275 */ 					}).show().bind('click',function() {
/* 276 */ 						settings.activeImage = settings.activeImage - 1;
/* 277 */ 						_set_image_to_view();
/* 278 */ 						return false;
/* 279 */ 					});
/* 280 */ 				}
/* 281 */ 			}
/* 282 */ 			
/* 283 */ 			// Show the next button, if not the last image in set
/* 284 */ 			if ( settings.activeImage != ( settings.imageArray.length -1 ) ) {
/* 285 */ 				if ( settings.fixedNavigation ) {
/* 286 */ 					$('#lightbox-nav-btnNext').css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' })
/* 287 */ 						.unbind()
/* 288 */ 						.bind('click',function() {
/* 289 */ 							settings.activeImage = settings.activeImage + 1;
/* 290 */ 							_set_image_to_view();
/* 291 */ 							return false;
/* 292 */ 						});
/* 293 */ 				} else {
/* 294 */ 					// Show the images button for Next buttons
/* 295 */ 					$('#lightbox-nav-btnNext').unbind().hover(function() {
/* 296 */ 						$(this).css({ 'background' : 'url(' + settings.imageBtnNext + ') right 15% no-repeat' });
/* 297 */ 					},function() {
/* 298 */ 						$(this).css({ 'background' : 'transparent url(' + settings.imageBlank + ') no-repeat' });
/* 299 */ 					}).show().bind('click',function() {
/* 300 */ 						settings.activeImage = settings.activeImage + 1;

/* jquery.lightbox.js */

/* 301 */ 						_set_image_to_view();
/* 302 */ 						return false;
/* 303 */ 					});
/* 304 */ 				}
/* 305 */ 			}
/* 306 */ 			// Enable keyboard navigation
/* 307 */ 			_enable_keyboard_navigation();
/* 308 */ 		}
/* 309 */ 		/**
/* 310 *| 		 * Enable a support to keyboard navigation
/* 311 *| 		 *
/* 312 *| 		 */
/* 313 */ 		function _enable_keyboard_navigation() {
/* 314 */ 			$(document).keydown(function(objEvent) {
/* 315 */ 				_keyboard_action(objEvent);
/* 316 */ 			});
/* 317 */ 		}
/* 318 */ 		/**
/* 319 *| 		 * Disable the support to keyboard navigation
/* 320 *| 		 *
/* 321 *| 		 */
/* 322 */ 		function _disable_keyboard_navigation() {
/* 323 */ 			$(document).unbind();
/* 324 */ 		}
/* 325 */ 		/**
/* 326 *| 		 * Perform the keyboard actions
/* 327 *| 		 *
/* 328 *| 		 */
/* 329 */ 		function _keyboard_action(objEvent) {
/* 330 */ 			// To ie
/* 331 */ 			if ( objEvent == null ) {
/* 332 */ 				keycode = event.keyCode;
/* 333 */ 				escapeKey = 27;
/* 334 */ 			// To Mozilla
/* 335 */ 			} else {
/* 336 */ 				keycode = objEvent.keyCode;
/* 337 */ 				escapeKey = objEvent.DOM_VK_ESCAPE;
/* 338 */ 			}
/* 339 */ 			// Get the key in lower case form
/* 340 */ 			key = String.fromCharCode(keycode).toLowerCase();
/* 341 */ 			// Verify the keys to close the ligthBox
/* 342 */ 			if ( ( key == settings.keyToClose ) || ( key == 'x' ) || ( keycode == escapeKey ) ) {
/* 343 */ 				_finish();
/* 344 */ 			}
/* 345 */ 			// Verify the key to show the previous image
/* 346 */ 			if ( ( key == settings.keyToPrev ) || ( keycode == 37 ) ) {
/* 347 */ 				// If we�re not showing the first image, call the previous
/* 348 */ 				if ( settings.activeImage != 0 ) {
/* 349 */ 					settings.activeImage = settings.activeImage - 1;
/* 350 */ 					_set_image_to_view();

/* jquery.lightbox.js */

/* 351 */ 					_disable_keyboard_navigation();
/* 352 */ 				}
/* 353 */ 			}
/* 354 */ 			// Verify the key to show the next image
/* 355 */ 			if ( ( key == settings.keyToNext ) || ( keycode == 39 ) ) {
/* 356 */ 				// If we�re not showing the last image, call the next
/* 357 */ 				if ( settings.activeImage != ( settings.imageArray.length - 1 ) ) {
/* 358 */ 					settings.activeImage = settings.activeImage + 1;
/* 359 */ 					_set_image_to_view();
/* 360 */ 					_disable_keyboard_navigation();
/* 361 */ 				}
/* 362 */ 			}
/* 363 */ 		}
/* 364 */ 		/**
/* 365 *| 		 * Preload prev and next images being showed
/* 366 *| 		 *
/* 367 *| 		 */
/* 368 */ 		function _preload_neighbor_images() {
/* 369 */ 			if ( (settings.imageArray.length -1) > settings.activeImage ) {
/* 370 */ 				objNext = new Image();
/* 371 */ 				objNext.src = settings.imageArray[settings.activeImage + 1][0];
/* 372 */ 			}
/* 373 */ 			if ( settings.activeImage > 0 ) {
/* 374 */ 				objPrev = new Image();
/* 375 */ 				objPrev.src = settings.imageArray[settings.activeImage -1][0];
/* 376 */ 			}
/* 377 */ 		}
/* 378 */ 		/**
/* 379 *| 		 * Remove jQuery lightBox plugin HTML markup
/* 380 *| 		 *
/* 381 *| 		 */
/* 382 */ 		function _finish() {
/* 383 */ 			$('#jquery-lightbox').remove();
/* 384 */ 			$('#jquery-overlay').fadeOut(function() { $('#jquery-overlay').remove(); });
/* 385 */ 			// Show some elements to avoid conflict with overlay in IE. These elements appear above the overlay.
/* 386 */ 			$('embed, object, select').css({ 'visibility' : 'visible' });
/* 387 */ 		}
/* 388 */ 		/**
/* 389 *| 		 / THIRD FUNCTION
/* 390 *| 		 * getPageSize() by quirksmode.com
/* 391 *| 		 *
/* 392 *| 		 * @return Array Return an array with page width, height and window width, height
/* 393 *| 		 */
/* 394 */ 		function ___getPageSize() {
/* 395 */ 			var xScroll, yScroll;
/* 396 */ 			if (window.innerHeight && window.scrollMaxY) {	
/* 397 */ 				xScroll = window.innerWidth + window.scrollMaxX;
/* 398 */ 				yScroll = window.innerHeight + window.scrollMaxY;
/* 399 */ 			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
/* 400 */ 				xScroll = document.body.scrollWidth;

/* jquery.lightbox.js */

/* 401 */ 				yScroll = document.body.scrollHeight;
/* 402 */ 			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
/* 403 */ 				xScroll = document.body.offsetWidth;
/* 404 */ 				yScroll = document.body.offsetHeight;
/* 405 */ 			}
/* 406 */ 			var windowWidth, windowHeight;
/* 407 */ 			if (self.innerHeight) {	// all except Explorer
/* 408 */ 				if(document.documentElement.clientWidth){
/* 409 */ 					windowWidth = document.documentElement.clientWidth; 
/* 410 */ 				} else {
/* 411 */ 					windowWidth = self.innerWidth;
/* 412 */ 				}
/* 413 */ 				windowHeight = self.innerHeight;
/* 414 */ 			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
/* 415 */ 				windowWidth = document.documentElement.clientWidth;
/* 416 */ 				windowHeight = document.documentElement.clientHeight;
/* 417 */ 			} else if (document.body) { // other Explorers
/* 418 */ 				windowWidth = document.body.clientWidth;
/* 419 */ 				windowHeight = document.body.clientHeight;
/* 420 */ 			}	
/* 421 */ 			// for small pages with total height less then height of the viewport
/* 422 */ 			if(yScroll < windowHeight){
/* 423 */ 				pageHeight = windowHeight;
/* 424 */ 			} else { 
/* 425 */ 				pageHeight = yScroll;
/* 426 */ 			}
/* 427 */ 			// for small pages with total width less then width of the viewport
/* 428 */ 			if(xScroll < windowWidth){	
/* 429 */ 				pageWidth = xScroll;		
/* 430 */ 			} else {
/* 431 */ 				pageWidth = windowWidth;
/* 432 */ 			}
/* 433 */ 			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
/* 434 */ 			return arrayPageSize;
/* 435 */ 		};
/* 436 */ 		/**
/* 437 *| 		 / THIRD FUNCTION
/* 438 *| 		 * getPageScroll() by quirksmode.com
/* 439 *| 		 *
/* 440 *| 		 * @return Array Return an array with x,y page scroll values.
/* 441 *| 		 */
/* 442 */ 		function ___getPageScroll() {
/* 443 */ 			var xScroll, yScroll;
/* 444 */ 			if (self.pageYOffset) {
/* 445 */ 				yScroll = self.pageYOffset;
/* 446 */ 				xScroll = self.pageXOffset;
/* 447 */ 			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
/* 448 */ 				yScroll = document.documentElement.scrollTop;
/* 449 */ 				xScroll = document.documentElement.scrollLeft;
/* 450 */ 			} else if (document.body) {// all other Explorers

/* jquery.lightbox.js */

/* 451 */ 				yScroll = document.body.scrollTop;
/* 452 */ 				xScroll = document.body.scrollLeft;	
/* 453 */ 			}
/* 454 */ 			arrayPageScroll = new Array(xScroll,yScroll);
/* 455 */ 			return arrayPageScroll;
/* 456 */ 		};
/* 457 */ 		 /**
/* 458 *| 		  * Stop the code execution from a escified time in milisecond
/* 459 *| 		  *
/* 460 *| 		  */
/* 461 */ 		 function ___pause(ms) {
/* 462 */ 			var date = new Date(); 
/* 463 */ 			curDate = null;
/* 464 */ 			do { var curDate = new Date(); }
/* 465 */ 			while ( curDate - date < ms);
/* 466 */ 		 };
/* 467 */ 		// Return the jQuery object for chaining. The unbind method is used to avoid click conflict when the plugin is called more than once
/* 468 */ 		return this.unbind('click').click(_initialize);
/* 469 */ 	};
/* 470 */ })(jQuery); // Call and execute the function immediately passing the jQuery object
/* 471 */ 
