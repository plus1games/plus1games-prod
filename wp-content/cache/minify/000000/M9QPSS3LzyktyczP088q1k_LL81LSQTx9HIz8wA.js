
/* foundation.min.js */

/* 1    */ !function(a,b,c,d){"use strict";function e(a){return("string"==typeof a||a instanceof String)&&(a=a.replace(/^['\\/"]+|(;\s?})+|['\\/"]+$/g,"")),a}var f=function(b){for(var c=b.length,d=a("head");c--;)0===d.has("."+b[c]).length&&d.append('<meta class="'+b[c]+'" />')};f(["foundation-mq-small","foundation-mq-medium","foundation-mq-large","foundation-mq-xlarge","foundation-mq-xxlarge","foundation-data-attribute-namespace"]),a(function(){"undefined"!=typeof FastClick&&"undefined"!=typeof c.body&&FastClick.attach(c.body)});var g=function(b,d){if("string"==typeof b){if(d){var e;if(d.jquery){if(e=d[0],!e)return d}else e=d;return a(e.querySelectorAll(b))}return a(c.querySelectorAll(b))}return a(b,d)},h=function(a){var b=[];return a||b.push("data"),this.namespace.length>0&&b.push(this.namespace),b.push(this.name),b.join("-")},i=function(a){for(var b=a.split("-"),c=b.length,d=[];c--;)0!==c?d.push(b[c]):this.namespace.length>0?d.push(this.namespace,b[c]):d.push(b[c]);return d.reverse().join("-")},j=function(b,c){var d=this,e=!g(this).data(this.attr_name(!0));return g(this.scope).is("["+this.attr_name()+"]")?(g(this.scope).data(this.attr_name(!0)+"-init",a.extend({},this.settings,c||b,this.data_options(g(this.scope)))),e&&this.events(this.scope)):g("["+this.attr_name()+"]",this.scope).each(function(){var e=!g(this).data(d.attr_name(!0)+"-init");g(this).data(d.attr_name(!0)+"-init",a.extend({},d.settings,c||b,d.data_options(g(this)))),e&&d.events(this)}),"string"==typeof b?this[b].call(this,c):void 0},k=function(a,b){function c(){b(a[0])}function d(){if(this.one("load",c),/MSIE (\d+\.\d+);/.test(navigator.userAgent)){var a=this.attr("src"),b=a.match(/\?/)?"&":"?";b+="random="+(new Date).getTime(),this.attr("src",a+b)}}return a.attr("src")?void(a[0].complete||4===a[0].readyState?c():d.call(a)):void c()};b.matchMedia=b.matchMedia||function(a){var b,c=a.documentElement,d=c.firstElementChild||c.firstChild,e=a.createElement("body"),f=a.createElement("div");return f.id="mq-test-1",f.style.cssText="position:absolute;top:-100em",e.style.background="none",e.appendChild(f),function(a){return f.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',c.insertBefore(e,d),b=42===f.offsetWidth,c.removeChild(e),{matches:b,media:a}}}(c),function(){function a(){c&&(f(a),h&&jQuery.fx.tick())}for(var c,d=0,e=["webkit","moz"],f=b.requestAnimationFrame,g=b.cancelAnimationFrame,h="undefined"!=typeof jQuery.fx;d<e.length&&!f;d++)f=b[e[d]+"RequestAnimationFrame"],g=g||b[e[d]+"CancelAnimationFrame"]||b[e[d]+"CancelRequestAnimationFrame"];f?(b.requestAnimationFrame=f,b.cancelAnimationFrame=g,h&&(jQuery.fx.timer=function(b){b()&&jQuery.timers.push(b)&&!c&&(c=!0,a())},jQuery.fx.stop=function(){c=!1})):(b.requestAnimationFrame=function(a){var c=(new Date).getTime(),e=Math.max(0,16-(c-d)),f=b.setTimeout(function(){a(c+e)},e);return d=c+e,f},b.cancelAnimationFrame=function(a){clearTimeout(a)})}(jQuery),b.Foundation={name:"Foundation",version:"5.3.1",media_queries:{small:g(".foundation-mq-small").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),medium:g(".foundation-mq-medium").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),large:g(".foundation-mq-large").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),xlarge:g(".foundation-mq-xlarge").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),xxlarge:g(".foundation-mq-xxlarge").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,"")},stylesheet:a("<style></style>").appendTo("head")[0].sheet,global:{namespace:d},init:function(a,b,c,d,e){var f=[a,c,d,e],h=[];if(this.rtl=/rtl/i.test(g("html").attr("dir")),this.scope=a||this.scope,this.set_namespace(),b&&"string"==typeof b&&!/reflow/i.test(b))this.libs.hasOwnProperty(b)&&h.push(this.init_lib(b,f));else for(var i in this.libs)h.push(this.init_lib(i,b));return a},init_lib:function(b,c){return this.libs.hasOwnProperty(b)?(this.patch(this.libs[b]),c&&c.hasOwnProperty(b)?("undefined"!=typeof this.libs[b].settings?a.extend(!0,this.libs[b].settings,c[b]):"undefined"!=typeof this.libs[b].defaults&&a.extend(!0,this.libs[b].defaults,c[b]),this.libs[b].init.apply(this.libs[b],[this.scope,c[b]])):(c=c instanceof Array?c:new Array(c),this.libs[b].init.apply(this.libs[b],c))):function(){}},patch:function(a){a.scope=this.scope,a.namespace=this.global.namespace,a.rtl=this.rtl,a.data_options=this.utils.data_options,a.attr_name=h,a.add_namespace=i,a.bindings=j,a.S=this.utils.S},inherit:function(a,b){for(var c=b.split(" "),d=c.length;d--;)this.utils.hasOwnProperty(c[d])&&(a[c[d]]=this.utils[c[d]])},set_namespace:function(){var b=this.global.namespace===d?a(".foundation-data-attribute-namespace").css("font-family"):this.global.namespace;this.global.namespace=b===d||/false/i.test(b)?"":b},libs:{},utils:{S:g,throttle:function(a,b){var c=null;return function(){var d=this,e=arguments;null==c&&(c=setTimeout(function(){a.apply(d,e),c=null},b))}},debounce:function(a,b,c){var d,e;return function(){var f=this,g=arguments,h=function(){d=null,c||(e=a.apply(f,g))},i=c&&!d;return clearTimeout(d),d=setTimeout(h,b),i&&(e=a.apply(f,g)),e}},data_options:function(b,c){function d(a){return!isNaN(a-0)&&null!==a&&""!==a&&a!==!1&&a!==!0}function e(b){return"string"==typeof b?a.trim(b):b}c=c||"options";var f,g,h,i={},j=function(a){var b=Foundation.global.namespace;return a.data(b.length>0?b+"-"+c:c)},k=j(b);if("object"==typeof k)return k;for(h=(k||":").split(";"),f=h.length;f--;)g=h[f].split(":"),g=[g[0],g.slice(1).join(":")],/true/i.test(g[1])&&(g[1]=!0),/false/i.test(g[1])&&(g[1]=!1),d(g[1])&&(g[1]=-1===g[1].indexOf(".")?parseInt(g[1],10):parseFloat(g[1])),2===g.length&&g[0].length>0&&(i[e(g[0])]=e(g[1]));return i},register_media:function(b,c){Foundation.media_queries[b]===d&&(a("head").append('<meta class="'+c+'"/>'),Foundation.media_queries[b]=e(a("."+c).css("font-family")))},add_custom_rule:function(a,b){if(b===d&&Foundation.stylesheet)Foundation.stylesheet.insertRule(a,Foundation.stylesheet.cssRules.length);else{var c=Foundation.media_queries[b];c!==d&&Foundation.stylesheet.insertRule("@media "+Foundation.media_queries[b]+"{ "+a+" }")}},image_loaded:function(a,b){var c=this,d=a.length;0===d&&b(a),a.each(function(){k(c.S(this),function(){d-=1,0===d&&b(a)})})},random_str:function(){return this.fidx||(this.fidx=0),this.prefix=this.prefix||[this.name||"F",(+new Date).toString(36)].join("-"),this.prefix+(this.fidx++).toString(36)}}},a.fn.foundation=function(){var a=Array.prototype.slice.call(arguments,0);return this.each(function(){return Foundation.init.apply(Foundation,[this].concat(a)),this})}}(jQuery,window,window.document),function(a,b,c){"use strict";Foundation.libs.abide={name:"abide",version:"5.3.3",settings:{live_validate:!0,focus_on_invalid:!0,error_labels:!0,timeout:1e3,patterns:{alpha:/^[a-zA-Z]+$/,alpha_numeric:/^[a-zA-Z0-9]+$/,integer:/^[-+]?\d+$/,number:/^[-+]?\d*(?:[\.\,]\d+)?$/,card:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,cvv:/^([0-9]){3,4}$/,email:/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,url:/^(https?|ftp|file|ssh):\/\/(((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/,domain:/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/,datetime:/^([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9])T([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])(Z|([\-\+]([0-1][0-9])\:00))$/,date:/(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))$/,time:/^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/,dateISO:/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/,month_day_year:/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.]\d{4}$/,color:/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/},validators:{equalTo:function(a){var b=c.getElementById(a.getAttribute(this.add_namespace("data-equalto"))).value,d=a.value,e=b===d;return e}}},timer:null,init:function(a,b,c){this.bindings(b,c)},events:function(b){var c=this,d=c.S(b).attr("novalidate","novalidate"),e=d.data(this.attr_name(!0)+"-init")||{};this.invalid_attr=this.add_namespace("data-invalid"),d.off(".abide").on("submit.fndtn.abide validate.fndtn.abide",function(a){var b=/ajax/i.test(c.S(this).attr(c.attr_name()));return c.validate(c.S(this).find("input, textarea, select").get(),a,b)}).on("reset",function(){return c.reset(a(this))}).find("input, textarea, select").off(".abide").on("blur.fndtn.abide change.fndtn.abide",function(a){c.validate([this],a)}).on("keydown.fndtn.abide",function(a){e.live_validate===!0&&(clearTimeout(c.timer),c.timer=setTimeout(function(){c.validate([this],a)}.bind(this),e.timeout))})},reset:function(b){b.removeAttr(this.invalid_attr),a(this.invalid_attr,b).removeAttr(this.invalid_attr),a(".error",b).not("small").removeClass("error")},validate:function(a,b,c){for(var d=this.parse_patterns(a),e=d.length,f=this.S(a[0]).closest("form"),g=/submit/.test(b.type),h=0;e>h;h++)if(!d[h]&&(g||c))return this.settings.focus_on_invalid&&a[h].focus(),f.trigger("invalid"),this.S(a[h]).closest("form").attr(this.invalid_attr,""),!1;return(g||c)&&f.trigger("valid"),f.removeAttr(this.invalid_attr),c?!1:!0},parse_patterns:function(a){for(var b=a.length,c=[];b--;)c.push(this.pattern(a[b]));return this.check_validation_and_apply_styles(c)},pattern:function(a){var b=a.getAttribute("type"),c="string"==typeof a.getAttribute("required"),d=a.getAttribute("pattern")||"";return this.settings.patterns.hasOwnProperty(d)&&d.length>0?[a,this.settings.patterns[d],c]:d.length>0?[a,new RegExp(d),c]:this.settings.patterns.hasOwnProperty(b)?[a,this.settings.patterns[b],c]:(d=/.*/,[a,d,c])},check_validation_and_apply_styles:function(b){var c=b.length,d=[],e=this.S(b[0][0]).closest("[data-"+this.attr_name(!0)+"]");for(e.data(this.attr_name(!0)+"-init")||{};c--;){var f,g,h=b[c][0],i=b[c][2],j=h.value.trim(),k=this.S(h).parent(),l=h.getAttribute(this.add_namespace("data-abide-validator")),m="radio"===h.type,n="checkbox"===h.type,o=this.S('label[for="'+h.getAttribute("id")+'"]'),p=i?h.value.length>0:!0;h.getAttribute(this.add_namespace("data-equalto"))&&(l="equalTo"),f=k.is("label")?k.parent():k,m&&i?d.push(this.valid_radio(h,i)):n&&i?d.push(this.valid_checkbox(h,i)):(l&&(g=this.settings.validators[l].apply(this,[h,i,f]),d.push(g)),d.push(b[c][1].test(j)&&p||!i&&h.value.length<1||a(h).attr("disabled")?!0:!1),d=[d.every(function(a){return a})],d[0]?(this.S(h).removeAttr(this.invalid_attr),f.removeClass("error"),o.length>0&&this.settings.error_labels&&o.removeClass("error"),a(h).triggerHandler("valid")):(f.addClass("error"),this.S(h).attr(this.invalid_attr,""),o.length>0&&this.settings.error_labels&&o.addClass("error"),a(h).triggerHandler("invalid")))}return d},valid_checkbox:function(a,b){var a=this.S(a),c=a.is(":checked")||!b;return c?a.removeAttr(this.invalid_attr).parent().removeClass("error"):a.attr(this.invalid_attr,"").parent().addClass("error"),c},valid_radio:function(a){for(var b=a.getAttribute("name"),c=this.S(a).closest("[data-"+this.attr_name(!0)+"]").find("[name='"+b+"']"),d=c.length,e=!1,f=0;d>f;f++)c[f].checked&&(e=!0);for(var f=0;d>f;f++)e?this.S(c[f]).removeAttr(this.invalid_attr).parent().removeClass("error"):this.S(c[f]).attr(this.invalid_attr,"").parent().addClass("error");return e},valid_equal:function(a,b,d){var e=c.getElementById(a.getAttribute(this.add_namespace("data-equalto"))).value,f=a.value,g=e===f;return g?(this.S(a).removeAttr(this.invalid_attr),d.removeClass("error")):(this.S(a).attr(this.invalid_attr,""),d.addClass("error")),g},valid_oneof:function(a,b,c,d){var a=this.S(a),e=this.S("["+this.add_namespace("data-oneof")+"]"),f=e.filter(":checked").length>0;if(f?a.removeAttr(this.invalid_attr).parent().removeClass("error"):a.attr(this.invalid_attr,"").parent().addClass("error"),!d){var g=this;e.each(function(){g.valid_oneof.call(g,this,null,null,!0)})}return f}}}(jQuery,window,window.document),function(a){"use strict";Foundation.libs.accordion={name:"accordion",version:"5.3.3",settings:{active_class:"active",multi_expand:!1,toggleable:!0,callback:function(){}},init:function(a,b,c){this.bindings(b,c)},events:function(){var b=this,c=this.S;c(this.scope).off(".fndtn.accordion").on("click.fndtn.accordion","["+this.attr_name()+"] > dd > a",function(d){var e=c(this).closest("["+b.attr_name()+"]"),f=b.attr_name()+"="+e.attr(b.attr_name()),g=e.data(b.attr_name(!0)+"-init"),h=c("#"+this.href.split("#")[1]),i=a("> dd",e),j=i.children(".content"),k=j.filter("."+g.active_class);return d.preventDefault(),e.attr(b.attr_name())&&(j=j.add("["+f+"] dd > .content"),i=i.add("["+f+"] dd")),g.toggleable&&h.is(k)?(h.parent("dd").toggleClass(g.active_class,!1),h.toggleClass(g.active_class,!1),g.callback(h),h.triggerHandler("toggled",[e]),void e.triggerHandler("toggled",[h])):(g.multi_expand||(j.removeClass(g.active_class),i.removeClass(g.active_class)),h.addClass(g.active_class).parent().addClass(g.active_class),g.callback(h),h.triggerHandler("toggled",[e]),void e.triggerHandler("toggled",[h]))})},off:function(){},reflow:function(){}}}(jQuery,window,window.document),function(a){"use strict";Foundation.libs.alert={name:"alert",version:"5.3.3",settings:{callback:function(){}},init:function(a,b,c){this.bindings(b,c)},events:function(){var b=this,c=this.S;a(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(a){var d=c(this).closest("["+b.attr_name()+"]"),e=d.data(b.attr_name(!0)+"-init")||b.settings;a.preventDefault(),Modernizr.csstransitions?(d.addClass("alert-close"),d.on("transitionend webkitTransitionEnd oTransitionEnd",function(){c(this).trigger("close").trigger("close.fndtn.alert").remove(),e.callback()})):d.fadeOut(300,function(){c(this).trigger("close").trigger("close.fndtn.alert").remove(),e.callback()})})},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.clearing={name:"clearing",version:"5.3.3",settings:{templates:{viewing:'<a href="#" class="clearing-close">&times;</a><div class="visible-img" style="display: none"><div class="clearing-touch-label"></div><img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" /><p class="clearing-caption"></p><a href="#" class="clearing-main-prev"><span></span></a><a href="#" class="clearing-main-next"><span></span></a></div>'},close_selectors:".clearing-close, div.clearing-blackout",open_selectors:"",skip_selector:"",touch_label:"",init:!1,locked:!1},init:function(a,b,c){var d=this;Foundation.inherit(this,"throttle image_loaded"),this.bindings(b,c),d.S(this.scope).is("["+this.attr_name()+"]")?this.assemble(d.S("li",this.scope)):d.S("["+this.attr_name()+"]",this.scope).each(function(){d.assemble(d.S("li",this))})},events:function(d){var e=this,f=e.S,g=a(".scroll-container");g.length>0&&(this.scope=g),f(this.scope).off(".clearing").on("click.fndtn.clearing","ul["+this.attr_name()+"] li "+this.settings.open_selectors,function(a,b,c){var b=b||f(this),c=c||b,d=b.next("li"),g=b.closest("["+e.attr_name()+"]").data(e.attr_name(!0)+"-init"),h=f(a.target);a.preventDefault(),g||(e.init(),g=b.closest("["+e.attr_name()+"]").data(e.attr_name(!0)+"-init")),c.hasClass("visible")&&b[0]===c[0]&&d.length>0&&e.is_open(b)&&(c=d,h=f("img",c)),e.open(h,b,c),e.update_paddles(c)}).on("click.fndtn.clearing",".clearing-main-next",function(a){e.nav(a,"next")}).on("click.fndtn.clearing",".clearing-main-prev",function(a){e.nav(a,"prev")}).on("click.fndtn.clearing",this.settings.close_selectors,function(a){Foundation.libs.clearing.close(a,this)}),a(c).on("keydown.fndtn.clearing",function(a){e.keydown(a)}),f(b).off(".clearing").on("resize.fndtn.clearing",function(){e.resize()}),this.swipe_events(d)},swipe_events:function(){var a=this,b=a.S;b(this.scope).on("touchstart.fndtn.clearing",".visible-img",function(a){a.touches||(a=a.originalEvent);var c={start_page_x:a.touches[0].pageX,start_page_y:a.touches[0].pageY,start_time:(new Date).getTime(),delta_x:0,is_scrolling:d};b(this).data("swipe-transition",c),a.stopPropagation()}).on("touchmove.fndtn.clearing",".visible-img",function(c){if(c.touches||(c=c.originalEvent),!(c.touches.length>1||c.scale&&1!==c.scale)){var d=b(this).data("swipe-transition");if("undefined"==typeof d&&(d={}),d.delta_x=c.touches[0].pageX-d.start_page_x,Foundation.rtl&&(d.delta_x=-d.delta_x),"undefined"==typeof d.is_scrolling&&(d.is_scrolling=!!(d.is_scrolling||Math.abs(d.delta_x)<Math.abs(c.touches[0].pageY-d.start_page_y))),!d.is_scrolling&&!d.active){c.preventDefault();var e=d.delta_x<0?"next":"prev";d.active=!0,a.nav(c,e)}}}).on("touchend.fndtn.clearing",".visible-img",function(a){b(this).data("swipe-transition",{}),a.stopPropagation()})},assemble:function(b){var c=b.parent();if(!c.parent().hasClass("carousel")){c.after('<div id="foundationClearingHolder"></div>');var d=c.detach(),e="";if(null!=d[0]){e=d[0].outerHTML;var f=this.S("#foundationClearingHolder"),g=c.data(this.attr_name(!0)+"-init"),h={grid:'<div class="carousel">'+e+"</div>",viewing:g.templates.viewing},i='<div class="clearing-assembled"><div>'+h.viewing+h.grid+"</div></div>",j=this.settings.touch_label;Modernizr.touch&&(i=a(i).find(".clearing-touch-label").html(j).end()),f.after(i).remove()}}},open:function(b,d,e){function f(){setTimeout(function(){this.image_loaded(m,function(){1!==m.outerWidth()||o?g.call(this,m):f.call(this)}.bind(this))}.bind(this),100)}function g(b){var c=a(b);c.css("visibility","visible"),i.css("overflow","hidden"),j.addClass("clearing-blackout"),k.addClass("clearing-container"),l.show(),this.fix_height(e).caption(h.S(".clearing-caption",l),h.S("img",e)).center_and_label(b,n).shift(d,e,function(){e.closest("li").siblings().removeClass("visible"),e.closest("li").addClass("visible")}),l.trigger("opened.fndtn.clearing")}var h=this,i=a(c.body),j=e.closest(".clearing-assembled"),k=h.S("div",j).first(),l=h.S(".visible-img",k),m=h.S("img",l).not(b),n=h.S(".clearing-touch-label",k),o=!1;a("body").on("touchmove",function(a){a.preventDefault()}),m.error(function(){o=!0}),this.locked()||(l.trigger("open.fndtn.clearing"),m.attr("src",this.load(b)).css("visibility","hidden"),f.call(this))},close:function(b,d){b.preventDefault();var e,f,g=function(a){return/blackout/.test(a.selector)?a:a.closest(".clearing-blackout")}(a(d)),h=a(c.body);return d===b.target&&g&&(h.css("overflow",""),e=a("div",g).first(),f=a(".visible-img",e),f.trigger("close.fndtn.clearing"),this.settings.prev_index=0,a("ul["+this.attr_name()+"]",g).attr("style","").closest(".clearing-blackout").removeClass("clearing-blackout"),e.removeClass("clearing-container"),f.hide(),f.trigger("closed.fndtn.clearing")),a("body").off("touchmove"),!1},is_open:function(a){return a.parent().prop("style").length>0},keydown:function(b){var c=a(".clearing-blackout ul["+this.attr_name()+"]"),d=this.rtl?37:39,e=this.rtl?39:37,f=27;b.which===d&&this.go(c,"next"),b.which===e&&this.go(c,"prev"),b.which===f&&this.S("a.clearing-close").trigger("click").trigger("click.fndtn.clearing")},nav:function(b,c){var d=a("ul["+this.attr_name()+"]",".clearing-blackout");b.preventDefault(),this.go(d,c)},resize:function(){var b=a("img",".clearing-blackout .visible-img"),c=a(".clearing-touch-label",".clearing-blackout");b.length&&(this.center_and_label(b,c),b.trigger("resized.fndtn.clearing"))},fix_height:function(a){var b=a.parent().children(),c=this;return b.each(function(){var a=c.S(this),b=a.find("img");a.height()>b.outerHeight()&&a.addClass("fix-height")}).closest("ul").width(100*b.length+"%"),this},update_paddles:function(a){a=a.closest("li");var b=a.closest(".carousel").siblings(".visible-img");a.next().length>0?this.S(".clearing-main-next",b).removeClass("disabled"):this.S(".clearing-main-next",b).addClass("disabled"),a.prev().length>0?this.S(".clearing-main-prev",b).removeClass("disabled"):this.S(".clearing-main-prev",b).addClass("disabled")},center_and_label:function(a,b){return this.rtl?(a.css({marginRight:-(a.outerWidth()/2),marginTop:-(a.outerHeight()/2),left:"auto",right:"50%"}),b.length>0&&b.css({marginRight:-(b.outerWidth()/2),marginTop:-(a.outerHeight()/2)-b.outerHeight()-10,left:"auto",right:"50%"})):(a.css({marginLeft:-(a.outerWidth()/2),marginTop:-(a.outerHeight()/2)}),b.length>0&&b.css({marginLeft:-(b.outerWidth()/2),marginTop:-(a.outerHeight()/2)-b.outerHeight()-10})),this},load:function(a){var b;return b="A"===a[0].nodeName?a.attr("href"):a.parent().attr("href"),this.preload(a),b?b:a.attr("src")},preload:function(a){this.img(a.closest("li").next()).img(a.closest("li").prev())},img:function(a){if(a.length){var b=new Image,c=this.S("a",a);b.src=c.length?c.attr("href"):this.S("img",a).attr("src")}return this},caption:function(a,b){var c=b.attr("data-caption");return c?a.html(c).show():a.text("").hide(),this},go:function(a,b){var c=this.S(".visible",a),d=c[b]();this.settings.skip_selector&&0!=d.find(this.settings.skip_selector).length&&(d=d[b]()),d.length&&this.S("img",d).trigger("click",[c,d]).trigger("click.fndtn.clearing",[c,d]).trigger("change.fndtn.clearing")},shift:function(a,b,c){var d,e=b.parent(),f=this.settings.prev_index||b.index(),g=this.direction(e,a,b),h=this.rtl?"right":"left",i=parseInt(e.css("left"),10),j=b.outerWidth(),k={};b.index()===f||/skip/.test(g)?/skip/.test(g)&&(d=b.index()-this.settings.up_count,this.lock(),d>0?(k[h]=-(d*j),e.animate(k,300,this.unlock())):(k[h]=0,e.animate(k,300,this.unlock()))):/left/.test(g)?(this.lock(),k[h]=i+j,e.animate(k,300,this.unlock())):/right/.test(g)&&(this.lock(),k[h]=i-j,e.animate(k,300,this.unlock())),c()},direction:function(a,b,c){var d,e=this.S("li",a),f=e.outerWidth()+e.outerWidth()/4,g=Math.floor(this.S(".clearing-container").outerWidth()/f)-1,h=e.index(c);return this.settings.up_count=g,d=this.adjacent(this.settings.prev_index,h)?h>g&&h>this.settings.prev_index?"right":h>g-1&&h<=this.settings.prev_index?"left":!1:"skip",this.settings.prev_index=h,d},adjacent:function(a,b){for(var c=b+1;c>=b-1;c--)if(c===a)return!0;return!1},lock:function(){this.settings.locked=!0},unlock:function(){this.settings.locked=!1},locked:function(){return this.settings.locked},off:function(){this.S(this.scope).off(".fndtn.clearing"),this.S(b).off(".fndtn.clearing")},reflow:function(){this.init()}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.dropdown={name:"dropdown",version:"5.3.3",settings:{active_class:"open",align:"bottom",is_hover:!1,opened:function(){},closed:function(){}},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c)},events:function(){var c=this,d=c.S;d(this.scope).off(".dropdown").on("click.fndtn.dropdown","["+this.attr_name()+"]",function(b){var e=d(this).data(c.attr_name(!0)+"-init")||c.settings;(!e.is_hover||Modernizr.touch)&&(b.preventDefault(),c.toggle(a(this)))}).on("mouseenter.fndtn.dropdown","["+this.attr_name()+"], ["+this.attr_name()+"-content]",function(a){var b,e,f=d(this);clearTimeout(c.timeout),f.data(c.data_attr())?(b=d("#"+f.data(c.data_attr())),e=f):(b=f,e=d("["+c.attr_name()+"='"+b.attr("id")+"']"));var g=e.data(c.attr_name(!0)+"-init")||c.settings;d(a.target).data(c.data_attr())&&g.is_hover&&c.closeall.call(c),g.is_hover&&c.open.apply(c,[b,e])}).on("mouseleave.fndtn.dropdown","["+this.attr_name()+"], ["+this.attr_name()+"-content]",function(){var a=d(this);c.timeout=setTimeout(function(){if(a.data(c.data_attr())){var b=a.data(c.data_attr(!0)+"-init")||c.settings;b.is_hover&&c.close.call(c,d("#"+a.data(c.data_attr())))}else{var e=d("["+c.attr_name()+'="'+d(this).attr("id")+'"]'),b=e.data(c.attr_name(!0)+"-init")||c.settings;b.is_hover&&c.close.call(c,a)}}.bind(this),150)}).on("click.fndtn.dropdown",function(b){var e=d(b.target).closest("["+c.attr_name()+"-content]");if(!(d(b.target).closest("["+c.attr_name()+"]").length>0))return!d(b.target).data("revealId")&&e.length>0&&(d(b.target).is("["+c.attr_name()+"-content]")||a.contains(e.first()[0],b.target))?void b.stopPropagation():void c.close.call(c,d("["+c.attr_name()+"-content]"))}).on("opened.fndtn.dropdown","["+c.attr_name()+"-content]",function(){c.settings.opened.call(this)}).on("closed.fndtn.dropdown","["+c.attr_name()+"-content]",function(){c.settings.closed.call(this)}),d(b).off(".dropdown").on("resize.fndtn.dropdown",c.throttle(function(){c.resize.call(c)},50)),this.resize()},close:function(a){var b=this;a.each(function(){b.S(this).hasClass(b.settings.active_class)&&(b.S(this).css(Foundation.rtl?"right":"left","-99999px").removeClass(b.settings.active_class).prev("["+b.attr_name()+"]").removeClass(b.settings.active_class).removeData("target"),b.S(this).trigger("closed").trigger("closed.fndtn.dropdown",[a]))})},closeall:function(){var b=this;a.each(b.S("["+this.attr_name()+"-content]"),function(){b.close.call(b,b.S(this))})},open:function(a,b){this.css(a.addClass(this.settings.active_class),b),a.prev("["+this.attr_name()+"]").addClass(this.settings.active_class),a.data("target",b.get(0)).trigger("opened").trigger("opened.fndtn.dropdown",[a,b])},data_attr:function(){return this.namespace.length>0?this.namespace+"-"+this.name:this.name},toggle:function(a){var b=this.S("#"+a.data(this.data_attr()));0!==b.length&&(this.close.call(this,this.S("["+this.attr_name()+"-content]").not(b)),b.hasClass(this.settings.active_class)?(this.close.call(this,b),b.data("target")!==a.get(0)&&this.open.call(this,b,a)):this.open.call(this,b,a))},resize:function(){var a=this.S("["+this.attr_name()+"-content].open"),b=this.S("["+this.attr_name()+"='"+a.attr("id")+"']");a.length&&b.length&&this.css(a,b)},css:function(a,b){var c=Math.max((b.width()-a.width())/2,8);if(this.clear_idx(),this.small()){var d=this.dirs.bottom.call(a,b);a.attr("style","").removeClass("drop-left drop-right drop-top").css({position:"absolute",width:"95%","max-width":"none",top:d.top}),a.css(Foundation.rtl?"right":"left",c)}else{var e=b.data(this.attr_name(!0)+"-init")||this.settings;this.style(a,b,e)}return a},style:function(b,c,d){var e=a.extend({position:"absolute"},this.dirs[d.align].call(b,c,d));b.attr("style","").css(e)},dirs:{_base:function(a){var b=this.offsetParent(),c=b.offset(),d=a.offset();return d.top-=c.top,d.left-=c.left,d},top:function(a){var b=Foundation.libs.dropdown,c=b.dirs._base.call(this,a),d=8;return this.addClass("drop-top"),(a.outerWidth()<this.outerWidth()||b.small())&&b.adjust_pip(d,c),Foundation.rtl?{left:c.left-this.outerWidth()+a.outerWidth(),top:c.top-this.outerHeight()}:{left:c.left,top:c.top-this.outerHeight()}},bottom:function(a){var b=Foundation.libs.dropdown,c=b.dirs._base.call(this,a),d=8;return(a.outerWidth()<this.outerWidth()||b.small())&&b.adjust_pip(d,c),b.rtl?{left:c.left-this.outerWidth()+a.outerWidth(),top:c.top+a.outerHeight()}:{left:c.left,top:c.top+a.outerHeight()}},left:function(a){var b=Foundation.libs.dropdown.dirs._base.call(this,a);return this.addClass("drop-left"),{left:b.left-this.outerWidth(),top:b.top}},right:function(a){var b=Foundation.libs.dropdown.dirs._base.call(this,a);return this.addClass("drop-right"),{left:b.left+a.outerWidth(),top:b.top}}},adjust_pip:function(a,b){var c=Foundation.stylesheet;this.small()&&(a+=b.left-8),this.rule_idx=c.cssRules.length;var d=".f-dropdown.open:before",e=".f-dropdown.open:after",f="left: "+a+"px;",g="left: "+(a-1)+"px;";c.insertRule?(c.insertRule([d,"{",f,"}"].join(" "),this.rule_idx),c.insertRule([e,"{",g,"}"].join(" "),this.rule_idx+1)):(c.addRule(d,f,this.rule_idx),c.addRule(e,g,this.rule_idx+1))},clear_idx:function(){var a=Foundation.stylesheet;this.rule_idx&&(a.deleteRule(this.rule_idx),a.deleteRule(this.rule_idx),delete this.rule_idx)},small:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},off:function(){this.S(this.scope).off(".fndtn.dropdown"),this.S("html, body").off(".fndtn.dropdown"),this.S(b).off(".fndtn.dropdown"),this.S("[data-dropdown-content]").off(".fndtn.dropdown")},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.equalizer={name:"equalizer",version:"5.3.3",settings:{use_tallest:!0,before_height_change:a.noop,after_height_change:a.noop,equalize_on_stack:!1},init:function(a,b,c){Foundation.inherit(this,"image_loaded"),this.bindings(b,c),this.reflow()},events:function(){this.S(b).off(".equalizer").on("resize.fndtn.equalizer",function(){this.reflow()}.bind(this))},equalize:function(b){var c=!1,d=b.find("["+this.attr_name()+"-watch]:visible"),e=b.data(this.attr_name(!0)+"-init");if(0!==d.length){var f=d.first().offset().top;if(e.before_height_change(),b.trigger("before-height-change").trigger("before-height-change.fndth.equalizer"),d.height("inherit"),d.each(function(){var b=a(this);b.offset().top!==f&&(c=!0)}),e.equalize_on_stack!==!1||!c){var g=d.map(function(){return a(this).outerHeight(!1)}).get();if(e.use_tallest){var h=Math.max.apply(null,g);d.css("height",h)}else{var i=Math.min.apply(null,g);d.css("height",i)}e.after_height_change(),b.trigger("after-height-change").trigger("after-height-change.fndtn.equalizer")}}},reflow:function(){var b=this;this.S("["+this.attr_name()+"]",this.scope).each(function(){var c=a(this);b.image_loaded(b.S("img",this),function(){b.equalize(c)})})}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.interchange={name:"interchange",version:"5.3.3",cache:{},images_loaded:!1,nodes_loaded:!1,settings:{load_attr:"interchange",named_queries:{"default":"only screen",small:Foundation.media_queries.small,medium:Foundation.media_queries.medium,large:Foundation.media_queries.large,xlarge:Foundation.media_queries.xlarge,xxlarge:Foundation.media_queries.xxlarge,landscape:"only screen and (orientation: landscape)",portrait:"only screen and (orientation: portrait)",retina:"only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx)"},directives:{replace:function(b,c,d){if(/IMG/.test(b[0].nodeName)){var e=b[0].src;if(new RegExp(c,"i").test(e))return;return b[0].src=c,d(b[0].src)}var f=b.data(this.data_attr+"-last-path"),g=this;if(f!=c)return/\.(gif|jpg|jpeg|tiff|png)([?#].*)?/i.test(c)?(a(b).css("background-image","url("+c+")"),b.data("interchange-last-path",c),d(c)):a.get(c,function(a){b.html(a),b.data(g.data_attr+"-last-path",c),d()})}}},init:function(b,c,d){Foundation.inherit(this,"throttle random_str"),this.data_attr=this.set_data_attr(),a.extend(!0,this.settings,c,d),this.bindings(c,d),this.load("images"),this.load("nodes")},get_media_hash:function(){var a="";for(var b in this.settings.named_queries)a+=matchMedia(this.settings.named_queries[b]).matches.toString();return a},events:function(){var c,d=this;return a(b).off(".interchange").on("resize.fndtn.interchange",d.throttle(function(){var a=d.get_media_hash();
/* 2    */ a!==c&&d.resize(),c=a},50)),this},resize:function(){var b=this.cache;if(!this.images_loaded||!this.nodes_loaded)return void setTimeout(a.proxy(this.resize,this),50);for(var c in b)if(b.hasOwnProperty(c)){var d=this.results(c,b[c]);d&&this.settings.directives[d.scenario[1]].call(this,d.el,d.scenario[0],function(){if(arguments[0]instanceof Array)var a=arguments[0];else var a=Array.prototype.slice.call(arguments,0);d.el.trigger(d.scenario[1],a)})}},results:function(a,b){var c=b.length;if(c>0)for(var d=this.S("["+this.add_namespace("data-uuid")+'="'+a+'"]');c--;){var e,f=b[c][2];if(e=matchMedia(this.settings.named_queries.hasOwnProperty(f)?this.settings.named_queries[f]:f),e.matches)return{el:d,scenario:b[c]}}return!1},load:function(a,b){return("undefined"==typeof this["cached_"+a]||b)&&this["update_"+a](),this["cached_"+a]},update_images:function(){var a=this.S("img["+this.data_attr+"]"),b=a.length,c=b,d=0,e=this.data_attr;for(this.cache={},this.cached_images=[],this.images_loaded=0===b;c--;){if(d++,a[c]){var f=a[c].getAttribute(e)||"";f.length>0&&this.cached_images.push(a[c])}d===b&&(this.images_loaded=!0,this.enhance("images"))}return this},update_nodes:function(){var a=this.S("["+this.data_attr+"]").not("img"),b=a.length,c=b,d=0,e=this.data_attr;for(this.cached_nodes=[],this.nodes_loaded=0===b;c--;){d++;var f=a[c].getAttribute(e)||"";f.length>0&&this.cached_nodes.push(a[c]),d===b&&(this.nodes_loaded=!0,this.enhance("nodes"))}return this},enhance:function(c){for(var d=this["cached_"+c].length;d--;)this.object(a(this["cached_"+c][d]));return a(b).trigger("resize").trigger("resize.fndtn.interchange")},convert_directive:function(a){var b=this.trim(a);return b.length>0?b:"replace"},parse_scenario:function(a){var b=a[0].match(/(.+),\s*(\w+)\s*$/),c=a[1];if(b)var d=b[1],e=b[2];else var f=a[0].split(/,\s*$/),d=f[0],e="";return[this.trim(d),this.convert_directive(e),this.trim(c)]},object:function(a){var b=this.parse_data_attr(a),c=[],d=b.length;if(d>0)for(;d--;){var e=b[d].split(/\((.*?)(\))$/);if(e.length>1){var f=this.parse_scenario(e);c.push(f)}}return this.store(a,c)},store:function(a,b){var c=this.random_str(),d=a.data(this.add_namespace("uuid",!0));return this.cache[d]?this.cache[d]:(a.attr(this.add_namespace("data-uuid"),c),this.cache[c]=b)},trim:function(b){return"string"==typeof b?a.trim(b):b},set_data_attr:function(a){return a?this.namespace.length>0?this.namespace+"-"+this.settings.load_attr:this.settings.load_attr:this.namespace.length>0?"data-"+this.namespace+"-"+this.settings.load_attr:"data-"+this.settings.load_attr},parse_data_attr:function(a){for(var b=a.attr(this.attr_name()).split(/\[(.*?)\]/),c=b.length,d=[];c--;)b[c].replace(/[\W\d]+/,"").length>4&&d.push(b[c]);return d},reflow:function(){this.load("images",!0),this.load("nodes",!0)}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.joyride={name:"joyride",version:"5.3.3",defaults:{expose:!1,modal:!0,tip_location:"bottom",nub_position:"auto",scroll_speed:1500,scroll_animation:"linear",timer:0,start_timer_on_click:!0,start_offset:0,next_button:!0,prev_button:!0,tip_animation:"fade",pause_after:[],exposed:[],tip_animation_fade_speed:300,cookie_monster:!1,cookie_name:"joyride",cookie_domain:!1,cookie_expires:365,tip_container:"body",abort_on_close:!0,tip_location_patterns:{top:["bottom"],bottom:[],left:["right","top","bottom"],right:["left","top","bottom"]},post_ride_callback:function(){},post_step_callback:function(){},pre_step_callback:function(){},pre_ride_callback:function(){},post_expose_callback:function(){},template:{link:'<a href="#close" class="joyride-close-tip">&times;</a>',timer:'<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',tip:'<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',wrapper:'<div class="joyride-content-wrapper"></div>',button:'<a href="#" class="small button joyride-next-tip"></a>',prev_button:'<a href="#" class="small button joyride-prev-tip"></a>',modal:'<div class="joyride-modal-bg"></div>',expose:'<div class="joyride-expose-wrapper"></div>',expose_cover:'<div class="joyride-expose-cover"></div>'},expose_add_class:""},init:function(b,c,d){Foundation.inherit(this,"throttle random_str"),this.settings=this.settings||a.extend({},this.defaults,d||c),this.bindings(c,d)},events:function(){var c=this;a(this.scope).off(".joyride").on("click.fndtn.joyride",".joyride-next-tip, .joyride-modal-bg",function(a){a.preventDefault(),this.settings.$li.next().length<1?this.end():this.settings.timer>0?(clearTimeout(this.settings.automate),this.hide(),this.show(),this.startTimer()):(this.hide(),this.show())}.bind(this)).on("click.fndtn.joyride",".joyride-prev-tip",function(a){a.preventDefault(),this.settings.$li.prev().length<1||(this.settings.timer>0?(clearTimeout(this.settings.automate),this.hide(),this.show(null,!0),this.startTimer()):(this.hide(),this.show(null,!0)))}.bind(this)).on("click.fndtn.joyride",".joyride-close-tip",function(a){a.preventDefault(),this.end(this.settings.abort_on_close)}.bind(this)),a(b).off(".joyride").on("resize.fndtn.joyride",c.throttle(function(){if(a("["+c.attr_name()+"]").length>0&&c.settings.$next_tip){if(c.settings.exposed.length>0){var b=a(c.settings.exposed);b.each(function(){var b=a(this);c.un_expose(b),c.expose(b)})}c.is_phone()?c.pos_phone():c.pos_default(!1)}},100))},start:function(){var b=this,c=a("["+this.attr_name()+"]",this.scope),d=["timer","scrollSpeed","startOffset","tipAnimationFadeSpeed","cookieExpires"],e=d.length;!c.length>0||(this.settings.init||this.events(),this.settings=c.data(this.attr_name(!0)+"-init"),this.settings.$content_el=c,this.settings.$body=a(this.settings.tip_container),this.settings.body_offset=a(this.settings.tip_container).position(),this.settings.$tip_content=this.settings.$content_el.find("> li"),this.settings.paused=!1,this.settings.attempts=0,"function"!=typeof a.cookie&&(this.settings.cookie_monster=!1),(!this.settings.cookie_monster||this.settings.cookie_monster&&!a.cookie(this.settings.cookie_name))&&(this.settings.$tip_content.each(function(c){var f=a(this);this.settings=a.extend({},b.defaults,b.data_options(f));for(var g=e;g--;)b.settings[d[g]]=parseInt(b.settings[d[g]],10);b.create({$li:f,index:c})}),!this.settings.start_timer_on_click&&this.settings.timer>0?(this.show("init"),this.startTimer()):this.show("init")))},resume:function(){this.set_li(),this.show()},tip_template:function(b){var c,d;return b.tip_class=b.tip_class||"",c=a(this.settings.template.tip).addClass(b.tip_class),d=a.trim(a(b.li).html())+this.prev_button_text(b.prev_button_text,b.index)+this.button_text(b.button_text)+this.settings.template.link+this.timer_instance(b.index),c.append(a(this.settings.template.wrapper)),c.first().attr(this.add_namespace("data-index"),b.index),a(".joyride-content-wrapper",c).append(d),c[0]},timer_instance:function(b){var c;return c=0===b&&this.settings.start_timer_on_click&&this.settings.timer>0||0===this.settings.timer?"":a(this.settings.template.timer)[0].outerHTML},button_text:function(b){return this.settings.tip_settings.next_button?(b=a.trim(b)||"Next",b=a(this.settings.template.button).append(b)[0].outerHTML):b="",b},prev_button_text:function(b,c){return this.settings.tip_settings.prev_button?(b=a.trim(b)||"Previous",b=0==c?a(this.settings.template.prev_button).append(b).addClass("disabled")[0].outerHTML:a(this.settings.template.prev_button).append(b)[0].outerHTML):b="",b},create:function(b){this.settings.tip_settings=a.extend({},this.settings,this.data_options(b.$li));var c=b.$li.attr(this.add_namespace("data-button"))||b.$li.attr(this.add_namespace("data-text")),d=b.$li.attr(this.add_namespace("data-button-prev"))||b.$li.attr(this.add_namespace("data-prev-text")),e=b.$li.attr("class"),f=a(this.tip_template({tip_class:e,index:b.index,button_text:c,prev_button_text:d,li:b.$li}));a(this.settings.tip_container).append(f)},show:function(b,c){var e=null;this.settings.$li===d||-1===a.inArray(this.settings.$li.index(),this.settings.pause_after)?(this.settings.paused?this.settings.paused=!1:this.set_li(b,c),this.settings.attempts=0,this.settings.$li.length&&this.settings.$target.length>0?(b&&(this.settings.pre_ride_callback(this.settings.$li.index(),this.settings.$next_tip),this.settings.modal&&this.show_modal()),this.settings.pre_step_callback(this.settings.$li.index(),this.settings.$next_tip),this.settings.modal&&this.settings.expose&&this.expose(),this.settings.tip_settings=a.extend({},this.settings,this.data_options(this.settings.$li)),this.settings.timer=parseInt(this.settings.timer,10),this.settings.tip_settings.tip_location_pattern=this.settings.tip_location_patterns[this.settings.tip_settings.tip_location],/body/i.test(this.settings.$target.selector)||this.scroll_to(),this.is_phone()?this.pos_phone(!0):this.pos_default(!0),e=this.settings.$next_tip.find(".joyride-timer-indicator"),/pop/i.test(this.settings.tip_animation)?(e.width(0),this.settings.timer>0?(this.settings.$next_tip.show(),setTimeout(function(){e.animate({width:e.parent().width()},this.settings.timer,"linear")}.bind(this),this.settings.tip_animation_fade_speed)):this.settings.$next_tip.show()):/fade/i.test(this.settings.tip_animation)&&(e.width(0),this.settings.timer>0?(this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed).show(),setTimeout(function(){e.animate({width:e.parent().width()},this.settings.timer,"linear")}.bind(this),this.settings.tip_animation_fade_speed)):this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed)),this.settings.$current_tip=this.settings.$next_tip):this.settings.$li&&this.settings.$target.length<1?this.show():this.end()):this.settings.paused=!0},is_phone:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},hide:function(){this.settings.modal&&this.settings.expose&&this.un_expose(),this.settings.modal||a(".joyride-modal-bg").hide(),this.settings.$current_tip.css("visibility","hidden"),setTimeout(a.proxy(function(){this.hide(),this.css("visibility","visible")},this.settings.$current_tip),0),this.settings.post_step_callback(this.settings.$li.index(),this.settings.$current_tip)},set_li:function(a,b){a?(this.settings.$li=this.settings.$tip_content.eq(this.settings.start_offset),this.set_next_tip(),this.settings.$current_tip=this.settings.$next_tip):(this.settings.$li=b?this.settings.$li.prev():this.settings.$li.next(),this.set_next_tip()),this.set_target()},set_next_tip:function(){this.settings.$next_tip=a(".joyride-tip-guide").eq(this.settings.$li.index()),this.settings.$next_tip.data("closed","")},set_target:function(){var b=this.settings.$li.attr(this.add_namespace("data-class")),d=this.settings.$li.attr(this.add_namespace("data-id")),e=function(){return d?a(c.getElementById(d)):b?a("."+b).first():a("body")};this.settings.$target=e()},scroll_to:function(){var c,d;c=a(b).height()/2,d=Math.ceil(this.settings.$target.offset().top-c+this.settings.$next_tip.outerHeight()),0!=d&&a("html, body").stop().animate({scrollTop:d},this.settings.scroll_speed,"swing")},paused:function(){return-1===a.inArray(this.settings.$li.index()+1,this.settings.pause_after)},restart:function(){this.hide(),this.settings.$li=d,this.show("init")},pos_default:function(a){var b=this.settings.$next_tip.find(".joyride-nub"),c=Math.ceil(b.outerWidth()/2),d=Math.ceil(b.outerHeight()/2),e=a||!1;if(e&&(this.settings.$next_tip.css("visibility","hidden"),this.settings.$next_tip.show()),/body/i.test(this.settings.$target.selector))this.settings.$li.length&&this.pos_modal(b);else{var f=this.settings.tip_settings.tipAdjustmentY?parseInt(this.settings.tip_settings.tipAdjustmentY):0,g=this.settings.tip_settings.tipAdjustmentX?parseInt(this.settings.tip_settings.tipAdjustmentX):0;this.bottom()?(this.settings.$next_tip.css(this.rtl?{top:this.settings.$target.offset().top+d+this.settings.$target.outerHeight()+f,left:this.settings.$target.offset().left+this.settings.$target.outerWidth()-this.settings.$next_tip.outerWidth()+g}:{top:this.settings.$target.offset().top+d+this.settings.$target.outerHeight()+f,left:this.settings.$target.offset().left+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"top")):this.top()?(this.settings.$next_tip.css(this.rtl?{top:this.settings.$target.offset().top-this.settings.$next_tip.outerHeight()-d+f,left:this.settings.$target.offset().left+this.settings.$target.outerWidth()-this.settings.$next_tip.outerWidth()}:{top:this.settings.$target.offset().top-this.settings.$next_tip.outerHeight()-d+f,left:this.settings.$target.offset().left+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"bottom")):this.right()?(this.settings.$next_tip.css({top:this.settings.$target.offset().top+f,left:this.settings.$target.outerWidth()+this.settings.$target.offset().left+c+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"left")):this.left()&&(this.settings.$next_tip.css({top:this.settings.$target.offset().top+f,left:this.settings.$target.offset().left-this.settings.$next_tip.outerWidth()-c+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"right")),!this.visible(this.corners(this.settings.$next_tip))&&this.settings.attempts<this.settings.tip_settings.tip_location_pattern.length&&(b.removeClass("bottom").removeClass("top").removeClass("right").removeClass("left"),this.settings.tip_settings.tip_location=this.settings.tip_settings.tip_location_pattern[this.settings.attempts],this.settings.attempts++,this.pos_default())}e&&(this.settings.$next_tip.hide(),this.settings.$next_tip.css("visibility","visible"))},pos_phone:function(b){var c=this.settings.$next_tip.outerHeight(),d=(this.settings.$next_tip.offset(),this.settings.$target.outerHeight()),e=a(".joyride-nub",this.settings.$next_tip),f=Math.ceil(e.outerHeight()/2),g=b||!1;e.removeClass("bottom").removeClass("top").removeClass("right").removeClass("left"),g&&(this.settings.$next_tip.css("visibility","hidden"),this.settings.$next_tip.show()),/body/i.test(this.settings.$target.selector)?this.settings.$li.length&&this.pos_modal(e):this.top()?(this.settings.$next_tip.offset({top:this.settings.$target.offset().top-c-f}),e.addClass("bottom")):(this.settings.$next_tip.offset({top:this.settings.$target.offset().top+d+f}),e.addClass("top")),g&&(this.settings.$next_tip.hide(),this.settings.$next_tip.css("visibility","visible"))},pos_modal:function(a){this.center(),a.hide(),this.show_modal()},show_modal:function(){if(!this.settings.$next_tip.data("closed")){var b=a(".joyride-modal-bg");b.length<1&&a("body").append(this.settings.template.modal).show(),/pop/i.test(this.settings.tip_animation)?b.show():b.fadeIn(this.settings.tip_animation_fade_speed)}},expose:function(){var c,d,e,f,g,h="expose-"+this.random_str(6);if(arguments.length>0&&arguments[0]instanceof a)e=arguments[0];else{if(!this.settings.$target||/body/i.test(this.settings.$target.selector))return!1;e=this.settings.$target}return e.length<1?(b.console&&console.error("element not valid",e),!1):(c=a(this.settings.template.expose),this.settings.$body.append(c),c.css({top:e.offset().top,left:e.offset().left,width:e.outerWidth(!0),height:e.outerHeight(!0)}),d=a(this.settings.template.expose_cover),f={zIndex:e.css("z-index"),position:e.css("position")},g=null==e.attr("class")?"":e.attr("class"),e.css("z-index",parseInt(c.css("z-index"))+1),"static"==f.position&&e.css("position","relative"),e.data("expose-css",f),e.data("orig-class",g),e.attr("class",g+" "+this.settings.expose_add_class),d.css({top:e.offset().top,left:e.offset().left,width:e.outerWidth(!0),height:e.outerHeight(!0)}),this.settings.modal&&this.show_modal(),this.settings.$body.append(d),c.addClass(h),d.addClass(h),e.data("expose",h),this.settings.post_expose_callback(this.settings.$li.index(),this.settings.$next_tip,e),void this.add_exposed(e))},un_expose:function(){var c,d,e,f,g,h=!1;if(arguments.length>0&&arguments[0]instanceof a)d=arguments[0];else{if(!this.settings.$target||/body/i.test(this.settings.$target.selector))return!1;d=this.settings.$target}return d.length<1?(b.console&&console.error("element not valid",d),!1):(c=d.data("expose"),e=a("."+c),arguments.length>1&&(h=arguments[1]),h===!0?a(".joyride-expose-wrapper,.joyride-expose-cover").remove():e.remove(),f=d.data("expose-css"),"auto"==f.zIndex?d.css("z-index",""):d.css("z-index",f.zIndex),f.position!=d.css("position")&&("static"==f.position?d.css("position",""):d.css("position",f.position)),g=d.data("orig-class"),d.attr("class",g),d.removeData("orig-classes"),d.removeData("expose"),d.removeData("expose-z-index"),void this.remove_exposed(d))},add_exposed:function(b){this.settings.exposed=this.settings.exposed||[],b instanceof a||"object"==typeof b?this.settings.exposed.push(b[0]):"string"==typeof b&&this.settings.exposed.push(b)},remove_exposed:function(b){var c,d;for(b instanceof a?c=b[0]:"string"==typeof b&&(c=b),this.settings.exposed=this.settings.exposed||[],d=this.settings.exposed.length;d--;)if(this.settings.exposed[d]==c)return void this.settings.exposed.splice(d,1)},center:function(){var c=a(b);return this.settings.$next_tip.css({top:(c.height()-this.settings.$next_tip.outerHeight())/2+c.scrollTop(),left:(c.width()-this.settings.$next_tip.outerWidth())/2+c.scrollLeft()}),!0},bottom:function(){return/bottom/i.test(this.settings.tip_settings.tip_location)},top:function(){return/top/i.test(this.settings.tip_settings.tip_location)},right:function(){return/right/i.test(this.settings.tip_settings.tip_location)},left:function(){return/left/i.test(this.settings.tip_settings.tip_location)},corners:function(c){var d=a(b),e=d.height()/2,f=Math.ceil(this.settings.$target.offset().top-e+this.settings.$next_tip.outerHeight()),g=d.width()+d.scrollLeft(),h=d.height()+f,i=d.height()+d.scrollTop(),j=d.scrollTop();return j>f&&(j=0>f?0:f),h>i&&(i=h),[c.offset().top<j,g<c.offset().left+c.outerWidth(),i<c.offset().top+c.outerHeight(),d.scrollLeft()>c.offset().left]},visible:function(a){for(var b=a.length;b--;)if(a[b])return!1;return!0},nub_position:function(a,b,c){a.addClass("auto"===b?c:b)},startTimer:function(){this.settings.$li.length?this.settings.automate=setTimeout(function(){this.hide(),this.show(),this.startTimer()}.bind(this),this.settings.timer):clearTimeout(this.settings.automate)},end:function(b){this.settings.cookie_monster&&a.cookie(this.settings.cookie_name,"ridden",{expires:this.settings.cookie_expires,domain:this.settings.cookie_domain}),this.settings.timer>0&&clearTimeout(this.settings.automate),this.settings.modal&&this.settings.expose&&this.un_expose(),this.settings.$next_tip.data("closed",!0),a(".joyride-modal-bg").hide(),this.settings.$current_tip.hide(),("undefined"==typeof b||b===!1)&&(this.settings.post_step_callback(this.settings.$li.index(),this.settings.$current_tip),this.settings.post_ride_callback(this.settings.$li.index(),this.settings.$current_tip)),a(".joyride-tip-guide").remove()},off:function(){a(this.scope).off(".joyride"),a(b).off(".joyride"),a(".joyride-close-tip, .joyride-next-tip, .joyride-modal-bg").off(".joyride"),a(".joyride-tip-guide, .joyride-modal-bg").remove(),clearTimeout(this.settings.automate),this.settings={}},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs["magellan-expedition"]={name:"magellan-expedition",version:"5.3.3",settings:{active_class:"active",threshold:0,destination_threshold:20,throttle_delay:30,fixed_top:0},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c)},events:function(){var c=this,d=c.S,e=c.settings;c.set_expedition_position(),d(c.scope).off(".magellan").on("click.fndtn.magellan","["+c.add_namespace("data-magellan-arrival")+'] a[href^="#"]',function(b){b.preventDefault();var d=a(this).closest("["+c.attr_name()+"]"),e=d.data("magellan-expedition-init"),f=this.hash.split("#").join(""),g=a("a[name='"+f+"']");0===g.length&&(g=a("#"+f));var h=g.offset().top-e.destination_threshold+1;h-=d.outerHeight(),a("html, body").stop().animate({scrollTop:h},700,"swing",function(){history.pushState?history.pushState(null,null,"#"+f):location.hash="#"+f})}).on("scroll.fndtn.magellan",c.throttle(this.check_for_arrivals.bind(this),e.throttle_delay)),a(b).on("resize.fndtn.magellan",c.throttle(this.set_expedition_position.bind(this),e.throttle_delay))},check_for_arrivals:function(){var a=this;a.update_arrivals(),a.update_expedition_positions()},set_expedition_position:function(){var b=this;a("["+this.attr_name()+"=fixed]",b.scope).each(function(){var c,d,e=a(this),f=e.data("magellan-expedition-init"),g=e.attr("styles");e.attr("style",""),c=e.offset().top+f.threshold,d=parseInt(e.data("magellan-fixed-top")),isNaN(d)||(b.settings.fixed_top=d),e.data(b.data_attr("magellan-top-offset"),c),e.attr("style",g)})},update_expedition_positions:function(){var c=this,d=a(b).scrollTop();a("["+this.attr_name()+"=fixed]",c.scope).each(function(){var b=a(this),e=b.data("magellan-expedition-init"),f=b.attr("style"),g=b.data("magellan-top-offset");if(d+c.settings.fixed_top>=g){var h=b.prev("["+c.add_namespace("data-magellan-expedition-clone")+"]");0===h.length&&(h=b.clone(),h.removeAttr(c.attr_name()),h.attr(c.add_namespace("data-magellan-expedition-clone"),""),b.before(h)),b.css({position:"fixed",top:e.fixed_top})}else b.prev("["+c.add_namespace("data-magellan-expedition-clone")+"]").remove(),b.attr("style",f).css("position","").css("top","").removeClass("fixed")})},update_arrivals:function(){var c=this,d=a(b).scrollTop();a("["+this.attr_name()+"]",c.scope).each(function(){var b=a(this),e=b.data(c.attr_name(!0)+"-init"),f=c.offsets(b,d),g=b.find("["+c.add_namespace("data-magellan-arrival")+"]"),h=!1;f.each(function(a,d){if(d.viewport_offset>=d.top_offset){var f=b.find("["+c.add_namespace("data-magellan-arrival")+"]");return f.not(d.arrival).removeClass(e.active_class),d.arrival.addClass(e.active_class),h=!0,!0}}),h||g.removeClass(e.active_class)})},offsets:function(b,c){var d=this,e=b.data(d.attr_name(!0)+"-init"),f=c;return b.find("["+d.add_namespace("data-magellan-arrival")+"]").map(function(){var c=a(this).data(d.data_attr("magellan-arrival")),g=a("["+d.add_namespace("data-magellan-destination")+"="+c+"]");if(g.length>0){var h=Math.floor(g.offset().top-e.destination_threshold-b.outerHeight());return{destination:g,arrival:a(this),top_offset:h,viewport_offset:f}}}).sort(function(a,b){return a.top_offset<b.top_offset?-1:a.top_offset>b.top_offset?1:0})},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},off:function(){this.S(this.scope).off(".magellan"),this.S(b).off(".magellan")},reflow:function(){var b=this;a("["+b.add_namespace("data-magellan-expedition-clone")+"]",b.scope).remove()}}}(jQuery,window,window.document),function(){"use strict";Foundation.libs.offcanvas={name:"offcanvas",version:"5.3.3",settings:{open_method:"move",close_on_click:!1},init:function(a,b,c){this.bindings(b,c)},events:function(){var a=this,b=a.S,c="",d="",e="";"move"===this.settings.open_method?(c="move-",d="right",e="left"):"overlap"===this.settings.open_method&&(c="offcanvas-overlap"),b(this.scope).off(".offcanvas").on("click.fndtn.offcanvas",".left-off-canvas-toggle",function(b){a.click_toggle_class(b,c+d)}).on("click.fndtn.offcanvas",".left-off-canvas-menu a",function(b){var e=a.get_settings(b);e.close_on_click&&a.hide.call(a,c+d,a.get_wrapper(b))}).on("click.fndtn.offcanvas",".right-off-canvas-toggle",function(b){a.click_toggle_class(b,c+e)}).on("click.fndtn.offcanvas",".right-off-canvas-menu a",function(b){var d=a.get_settings(b);d.close_on_click&&a.hide.call(a,c+e,a.get_wrapper(b))}).on("click.fndtn.offcanvas",".exit-off-canvas",function(b){a.click_remove_class(b,c+e),d&&a.click_remove_class(b,c+d)})},toggle:function(a,b){b=b||this.get_wrapper(),b.is("."+a)?this.hide(a,b):this.show(a,b)},show:function(a,b){b=b||this.get_wrapper(),b.trigger("open").trigger("open.fndtn.offcanvas"),b.addClass(a)},hide:function(a,b){b=b||this.get_wrapper(),b.trigger("close").trigger("close.fndtn.offcanvas"),b.removeClass(a)},click_toggle_class:function(a,b){a.preventDefault();var c=this.get_wrapper(a);this.toggle(b,c)},click_remove_class:function(a,b){a.preventDefault();var c=this.get_wrapper(a);this.hide(b,c)},get_settings:function(a){var b=this.S(a.target).closest("["+this.attr_name()+"]");return b.data(this.attr_name(!0)+"-init")||this.settings},get_wrapper:function(a){var b=this.S(a?a.target:this.scope).closest(".off-canvas-wrap");return 0===b.length&&(b=this.S(".off-canvas-wrap")),b},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";var e=function(){},f=function(e,f){if(e.hasClass(f.slides_container_class))return this;var j,k,l,m,n,o,p=this,q=e,r=0,s=!1;p.slides=function(){return q.children(f.slide_selector)},p.slides().first().addClass(f.active_slide_class),p.update_slide_number=function(b){f.slide_number&&(k.find("span:first").text(parseInt(b)+1),k.find("span:last").text(p.slides().length)),f.bullets&&(l.children().removeClass(f.bullets_active_class),a(l.children().get(b)).addClass(f.bullets_active_class))},p.update_active_link=function(b){var c=a('[data-orbit-link="'+p.slides().eq(b).attr("data-orbit-slide")+'"]');c.siblings().removeClass(f.bullets_active_class),c.addClass(f.bullets_active_class)},p.build_markup=function(){q.wrap('<div class="'+f.container_class+'"></div>'),j=q.parent(),q.addClass(f.slides_container_class),f.stack_on_small&&j.addClass(f.stack_on_small_class),f.navigation_arrows&&(j.append(a('<a href="#"><span></span></a>').addClass(f.prev_class)),j.append(a('<a href="#"><span></span></a>').addClass(f.next_class))),f.timer&&(m=a("<div>").addClass(f.timer_container_class),m.append("<span>"),m.append(a("<div>").addClass(f.timer_progress_class)),m.addClass(f.timer_paused_class),j.append(m)),f.slide_number&&(k=a("<div>").addClass(f.slide_number_class),k.append("<span></span> "+f.slide_number_text+" <span></span>"),j.append(k)),f.bullets&&(l=a("<ol>").addClass(f.bullets_container_class),j.append(l),l.wrap('<div class="orbit-bullets-container"></div>'),p.slides().each(function(b){var c=a("<li>").attr("data-orbit-slide",b).on("click",p.link_bullet);l.append(c)}))},p._goto=function(b,c){if(b===r)return!1;"object"==typeof o&&o.restart();var d=p.slides(),e="next";if(s=!0,r>b&&(e="prev"),b>=d.length){if(!f.circular)return!1;b=0}else if(0>b){if(!f.circular)return!1;b=d.length-1}var g=a(d.get(r)),h=a(d.get(b));g.css("zIndex",2),g.removeClass(f.active_slide_class),h.css("zIndex",4).addClass(f.active_slide_class),q.trigger("before-slide-change.fndtn.orbit"),f.before_slide_change(),p.update_active_link(b);var i=function(){var a=function(){r=b,s=!1,c===!0&&(o=p.create_timer(),o.start()),p.update_slide_number(r),q.trigger("after-slide-change.fndtn.orbit",[{slide_number:r,total_slides:d.length}]),f.after_slide_change(r,d.length)};q.height()!=h.height()&&f.variable_height?q.animate({height:h.height()},250,"linear",a):a()};if(1===d.length)return i(),!1;var j=function(){"next"===e&&n.next(g,h,i),"prev"===e&&n.prev(g,h,i)};h.height()>q.height()&&f.variable_height?q.animate({height:h.height()},250,"linear",j):j()},p.next=function(a){a.stopImmediatePropagation(),a.preventDefault(),p._goto(r+1)},p.prev=function(a){a.stopImmediatePropagation(),a.preventDefault(),p._goto(r-1)},p.link_custom=function(b){b.preventDefault();var c=a(this).attr("data-orbit-link");if("string"==typeof c&&""!=(c=a.trim(c))){var d=j.find("[data-orbit-slide="+c+"]");-1!=d.index()&&p._goto(d.index())}},p.link_bullet=function(){var b=a(this).attr("data-orbit-slide");if("string"==typeof b&&""!=(b=a.trim(b)))if(isNaN(parseInt(b))){var c=j.find("[data-orbit-slide="+b+"]");-1!=c.index()&&p._goto(c.index()+1)}else p._goto(parseInt(b))},p.timer_callback=function(){p._goto(r+1,!0)},p.compute_dimensions=function(){var b=a(p.slides().get(r)),c=b.height();f.variable_height||p.slides().each(function(){a(this).height()>c&&(c=a(this).height())}),q.height(c)},p.create_timer=function(){var a=new g(j.find("."+f.timer_container_class),f,p.timer_callback);return a},p.stop_timer=function(){"object"==typeof o&&o.stop()},p.toggle_timer=function(){var a=j.find("."+f.timer_container_class);a.hasClass(f.timer_paused_class)?("undefined"==typeof o&&(o=p.create_timer()),o.start()):"object"==typeof o&&o.stop()},p.init=function(){p.build_markup(),f.timer&&(o=p.create_timer(),Foundation.utils.image_loaded(this.slides().children("img"),o.start)),n=new i(f,q),"slide"===f.animation&&(n=new h(f,q)),j.on("click","."+f.next_class,p.next),j.on("click","."+f.prev_class,p.prev),f.next_on_click&&j.on("click","."+f.slides_container_class+" [data-orbit-slide]",p.link_bullet),j.on("click",p.toggle_timer),f.swipe&&j.on("touchstart.fndtn.orbit",function(a){a.touches||(a=a.originalEvent);var b={start_page_x:a.touches[0].pageX,start_page_y:a.touches[0].pageY,start_time:(new Date).getTime(),delta_x:0,is_scrolling:d};j.data("swipe-transition",b),a.stopPropagation()}).on("touchmove.fndtn.orbit",function(a){if(a.touches||(a=a.originalEvent),!(a.touches.length>1||a.scale&&1!==a.scale)){var b=j.data("swipe-transition");if("undefined"==typeof b&&(b={}),b.delta_x=a.touches[0].pageX-b.start_page_x,"undefined"==typeof b.is_scrolling&&(b.is_scrolling=!!(b.is_scrolling||Math.abs(b.delta_x)<Math.abs(a.touches[0].pageY-b.start_page_y))),!b.is_scrolling&&!b.active){a.preventDefault();var c=b.delta_x<0?r+1:r-1;b.active=!0,p._goto(c)}}}).on("touchend.fndtn.orbit",function(a){j.data("swipe-transition",{}),a.stopPropagation()}),j.on("mouseenter.fndtn.orbit",function(){f.timer&&f.pause_on_hover&&p.stop_timer()}).on("mouseleave.fndtn.orbit",function(){f.timer&&f.resume_on_mouseout&&o.start()}),a(c).on("click","[data-orbit-link]",p.link_custom),a(b).on("load resize",p.compute_dimensions),Foundation.utils.image_loaded(this.slides().children("img"),p.compute_dimensions),Foundation.utils.image_loaded(this.slides().children("img"),function(){j.prev("."+f.preloader_class).css("display","none"),p.update_slide_number(0),p.update_active_link(0),q.trigger("ready.fndtn.orbit")})},p.init()},g=function(a,b,c){var d,e,f=this,g=b.timer_speed,h=a.find("."+b.timer_progress_class),i=-1;this.update_progress=function(a){var b=h.clone();b.attr("style",""),b.css("width",a+"%"),h.replaceWith(b),h=b},this.restart=function(){clearTimeout(e),a.addClass(b.timer_paused_class),i=-1,f.update_progress(0)},this.start=function(){return a.hasClass(b.timer_paused_class)?(i=-1===i?g:i,a.removeClass(b.timer_paused_class),d=(new Date).getTime(),h.animate({width:"100%"},i,"linear"),e=setTimeout(function(){f.restart(),c()},i),void a.trigger("timer-started.fndtn.orbit")):!0},this.stop=function(){if(a.hasClass(b.timer_paused_class))return!0;clearTimeout(e),a.addClass(b.timer_paused_class);var c=(new Date).getTime();i-=c-d;var h=100-i/g*100;f.update_progress(h),a.trigger("timer-stopped.fndtn.orbit")}},h=function(b){var c=b.animation_speed,d=1===a("html[dir=rtl]").length,e=d?"marginRight":"marginLeft",f={};f[e]="0%",this.next=function(a,b,d){a.animate({marginLeft:"-100%"},c),b.animate(f,c,function(){a.css(e,"100%"),d()})},this.prev=function(a,b,d){a.animate({marginLeft:"100%"},c),b.css(e,"-100%"),b.animate(f,c,function(){a.css(e,"100%"),d()})}},i=function(b){{var c=b.animation_speed;1===a("html[dir=rtl]").length}this.next=function(a,b,d){b.css({margin:"0%",opacity:"0.01"}),b.animate({opacity:"1"},c,"linear",function(){a.css("margin","100%"),d()})},this.prev=function(a,b,d){b.css({margin:"0%",opacity:"0.01"}),b.animate({opacity:"1"},c,"linear",function(){a.css("margin","100%"),d()})}};Foundation.libs=Foundation.libs||{},Foundation.libs.orbit={name:"orbit",version:"5.3.1",settings:{animation:"slide",timer_speed:1e4,pause_on_hover:!0,resume_on_mouseout:!1,next_on_click:!0,animation_speed:500,stack_on_small:!1,navigation_arrows:!0,slide_number:!0,slide_number_text:"of",container_class:"orbit-container",stack_on_small_class:"orbit-stack-on-small",next_class:"orbit-next",prev_class:"orbit-prev",timer_container_class:"orbit-timer",timer_paused_class:"paused",timer_progress_class:"orbit-progress",slides_container_class:"orbit-slides-container",preloader_class:"preloader",slide_selector:"*",bullets_container_class:"orbit-bullets",bullets_active_class:"active",slide_number_class:"orbit-slide-number",caption_class:"orbit-caption",active_slide_class:"active",orbit_transition_class:"orbit-transitioning",bullets:!0,circular:!0,timer:!0,variable_height:!1,swipe:!0,before_slide_change:e,after_slide_change:e},init:function(a,b,c){this.bindings(b,c)
/* 3    */ },events:function(a){var b=new f(this.S(a),this.S(a).data("orbit-init"));this.S(a).data(this.name+"-instance",b)},reflow:function(){var a=this;if(a.S(a.scope).is("[data-orbit]")){var b=a.S(a.scope),c=b.data(a.name+"-instance");c.compute_dimensions()}else a.S("[data-orbit]",a.scope).each(function(b,c){var d=a.S(c),e=(a.data_options(d),d.data(a.name+"-instance"));e.compute_dimensions()})}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";function e(a){var b=/fade/i.test(a),c=/pop/i.test(a);return{animate:b||c,pop:c,fade:b}}Foundation.libs.reveal={name:"reveal",version:"5.3.3",locked:!1,settings:{animation:"fadeAndPop",animation_speed:250,close_on_background_click:!0,close_on_esc:!0,dismiss_modal_class:"close-reveal-modal",bg_class:"reveal-modal-bg",root_element:"body",open:function(){},opened:function(){},close:function(){},closed:function(){},bg:a(".reveal-modal-bg"),css:{open:{opacity:0,visibility:"visible",display:"block"},close:{opacity:1,visibility:"hidden",display:"none"}}},init:function(b,c,d){a.extend(!0,this.settings,c,d),this.bindings(c,d)},events:function(){var a=this,b=a.S;return b(this.scope).off(".reveal").on("click.fndtn.reveal","["+this.add_namespace("data-reveal-id")+"]:not([disabled])",function(c){if(c.preventDefault(),!a.locked){var d=b(this),e=d.data(a.data_attr("reveal-ajax"));if(a.locked=!0,"undefined"==typeof e)a.open.call(a,d);else{var f=e===!0?d.attr("href"):e;a.open.call(a,d,{url:f})}}}),b(c).on("click.fndtn.reveal",this.close_targets(),function(c){if(c.preventDefault(),!a.locked){var d=b("["+a.attr_name()+"].open").data(a.attr_name(!0)+"-init"),e=b(c.target)[0]===b("."+d.bg_class)[0];if(e){if(!d.close_on_background_click)return;c.stopPropagation()}a.locked=!0,a.close.call(a,e?b("["+a.attr_name()+"].open"):b(this).closest("["+a.attr_name()+"]"))}}),b("["+a.attr_name()+"]",this.scope).length>0?b(this.scope).on("open.fndtn.reveal",this.settings.open).on("opened.fndtn.reveal",this.settings.opened).on("opened.fndtn.reveal",this.open_video).on("close.fndtn.reveal",this.settings.close).on("closed.fndtn.reveal",this.settings.closed).on("closed.fndtn.reveal",this.close_video):b(this.scope).on("open.fndtn.reveal","["+a.attr_name()+"]",this.settings.open).on("opened.fndtn.reveal","["+a.attr_name()+"]",this.settings.opened).on("opened.fndtn.reveal","["+a.attr_name()+"]",this.open_video).on("close.fndtn.reveal","["+a.attr_name()+"]",this.settings.close).on("closed.fndtn.reveal","["+a.attr_name()+"]",this.settings.closed).on("closed.fndtn.reveal","["+a.attr_name()+"]",this.close_video),!0},key_up_on:function(){var a=this;return a.S("body").off("keyup.fndtn.reveal").on("keyup.fndtn.reveal",function(b){var c=a.S("["+a.attr_name()+"].open"),d=c.data(a.attr_name(!0)+"-init")||a.settings;d&&27===b.which&&d.close_on_esc&&!a.locked&&a.close.call(a,c)}),!0},key_up_off:function(){return this.S("body").off("keyup.fndtn.reveal"),!0},open:function(b,c){var d,e=this;b?"undefined"!=typeof b.selector?d=e.S("#"+b.data(e.data_attr("reveal-id"))).first():(d=e.S(this.scope),c=b):d=e.S(this.scope);var f=d.data(e.attr_name(!0)+"-init");if(f=f||this.settings,d.hasClass("open")&&b.attr("data-reveal-id")==d.attr("id"))return e.close(d);if(!d.hasClass("open")){var g=e.S("["+e.attr_name()+"].open");if("undefined"==typeof d.data("css-top")&&d.data("css-top",parseInt(d.css("top"),10)).data("offset",this.cache_offset(d)),this.key_up_on(d),d.trigger("open").trigger("open.fndtn.reveal"),g.length<1&&this.toggle_bg(d,!0),"string"==typeof c&&(c={url:c}),"undefined"!=typeof c&&c.url){var h="undefined"!=typeof c.success?c.success:null;a.extend(c,{success:function(b,c,i){a.isFunction(h)&&h(b,c,i),d.html(b),e.S(d).foundation("section","reflow"),e.S(d).children().foundation(),g.length>0&&e.hide(g,f.css.close),e.show(d,f.css.open)}}),a.ajax(c)}else g.length>0&&this.hide(g,f.css.close),this.show(d,f.css.open)}},close:function(a){var a=a&&a.length?a:this.S(this.scope),b=this.S("["+this.attr_name()+"].open"),c=a.data(this.attr_name(!0)+"-init")||this.settings;b.length>0&&(this.locked=!0,this.key_up_off(a),a.trigger("close").trigger("close.fndtn.reveal"),this.toggle_bg(a,!1),this.hide(b,c.css.close,c))},close_targets:function(){var a="."+this.settings.dismiss_modal_class;return this.settings.close_on_background_click?a+", ."+this.settings.bg_class:a},toggle_bg:function(b,c){0===this.S("."+this.settings.bg_class).length&&(this.settings.bg=a("<div />",{"class":this.settings.bg_class}).appendTo("body").hide());var e=this.settings.bg.filter(":visible").length>0;c!=e&&((c==d?e:!c)?this.hide(this.settings.bg):this.show(this.settings.bg))},show:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init")||this.settings,g=f.root_element;if(0===c.parent(g).length){var h=c.wrap('<div style="display: none;" />').parent();c.on("closed.fndtn.reveal.wrapped",function(){c.detach().appendTo(h),c.unwrap().unbind("closed.fndtn.reveal.wrapped")}),c.detach().appendTo(g)}var i=e(f.animation);if(i.animate||(this.locked=!1),i.pop){d.top=a(b).scrollTop()-c.data("offset")+"px";var j={top:a(b).scrollTop()+c.data("css-top")+"px",opacity:1};return setTimeout(function(){return c.css(d).animate(j,f.animation_speed,"linear",function(){this.locked=!1,c.trigger("opened").trigger("opened.fndtn.reveal")}.bind(this)).addClass("open")}.bind(this),f.animation_speed/2)}if(i.fade){d.top=a(b).scrollTop()+c.data("css-top")+"px";var j={opacity:1};return setTimeout(function(){return c.css(d).animate(j,f.animation_speed,"linear",function(){this.locked=!1,c.trigger("opened").trigger("opened.fndtn.reveal")}.bind(this)).addClass("open")}.bind(this),f.animation_speed/2)}return c.css(d).show().css({opacity:1}).addClass("open").trigger("opened").trigger("opened.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeIn(f.animation_speed/2):(this.locked=!1,c.show())},hide:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init");f=f||this.settings;var g=e(f.animation);if(g.animate||(this.locked=!1),g.pop){var h={top:-a(b).scrollTop()-c.data("offset")+"px",opacity:0};return setTimeout(function(){return c.animate(h,f.animation_speed,"linear",function(){this.locked=!1,c.css(d).trigger("closed").trigger("closed.fndtn.reveal")}.bind(this)).removeClass("open")}.bind(this),f.animation_speed/2)}if(g.fade){var h={opacity:0};return setTimeout(function(){return c.animate(h,f.animation_speed,"linear",function(){this.locked=!1,c.css(d).trigger("closed").trigger("closed.fndtn.reveal")}.bind(this)).removeClass("open")}.bind(this),f.animation_speed/2)}return c.hide().css(d).removeClass("open").trigger("closed").trigger("closed.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeOut(f.animation_speed/2):c.hide()},close_video:function(b){var c=a(".flex-video",b.target),d=a("iframe",c);d.length>0&&(d.attr("data-src",d[0].src),d.attr("src",d.attr("src")),c.hide())},open_video:function(b){var c=a(".flex-video",b.target),e=c.find("iframe");if(e.length>0){var f=e.attr("data-src");if("string"==typeof f)e[0].src=e.attr("data-src");else{var g=e[0].src;e[0].src=d,e[0].src=g}c.show()}},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},cache_offset:function(a){var b=a.show().height()+parseInt(a.css("top"),10);return a.hide(),b},off:function(){a(this.scope).off(".fndtn.reveal")},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.slider={name:"slider",version:"5.3.3",settings:{start:0,end:100,step:1,initial:null,display_selector:"",vertical:!1,on_change:function(){}},cache:{},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c),this.reflow()},events:function(){var c=this;a(this.scope).off(".slider").on("mousedown.fndtn.slider touchstart.fndtn.slider pointerdown.fndtn.slider","["+c.attr_name()+"]:not(.disabled, [disabled]) .range-slider-handle",function(b){c.cache.active||(b.preventDefault(),c.set_active_slider(a(b.target)))}).on("mousemove.fndtn.slider touchmove.fndtn.slider pointermove.fndtn.slider",function(d){if(c.cache.active)if(d.preventDefault(),a.data(c.cache.active[0],"settings").vertical){var e=0;d.pageY||(e=b.scrollY),c.calculate_position(c.cache.active,(d.pageY||d.originalEvent.clientY||d.originalEvent.touches[0].clientY||d.currentPoint.y)+e)}else c.calculate_position(c.cache.active,d.pageX||d.originalEvent.clientX||d.originalEvent.touches[0].clientX||d.currentPoint.x)}).on("mouseup.fndtn.slider touchend.fndtn.slider pointerup.fndtn.slider",function(){c.remove_active_slider()}).on("change.fndtn.slider",function(){c.settings.on_change()}),c.S(b).on("resize.fndtn.slider",c.throttle(function(){c.reflow()},300))},set_active_slider:function(a){this.cache.active=a},remove_active_slider:function(){this.cache.active=null},calculate_position:function(b,c){var d=this,e=a.data(b[0],"settings"),f=(a.data(b[0],"handle_l"),a.data(b[0],"handle_o"),a.data(b[0],"bar_l")),g=a.data(b[0],"bar_o");requestAnimationFrame(function(){var a;a=Foundation.rtl&&!e.vertical?d.limit_to((g+f-c)/f,0,1):d.limit_to((c-g)/f,0,1),a=e.vertical?1-a:a;var h=d.normalized_value(a,e.start,e.end,e.step);d.set_ui(b,h)})},set_ui:function(b,c){var d=a.data(b[0],"settings"),e=a.data(b[0],"handle_l"),f=a.data(b[0],"bar_l"),g=this.normalized_percentage(c,d.start,d.end),h=g*(f-e)-1,i=100*g;Foundation.rtl&&!d.vertical&&(h=-h),h=d.vertical?-h+f-e+1:h,this.set_translate(b,h,d.vertical),d.vertical?b.siblings(".range-slider-active-segment").css("height",i+"%"):b.siblings(".range-slider-active-segment").css("width",i+"%"),b.parent().attr(this.attr_name(),c).trigger("change").trigger("change.fndtn.slider"),b.parent().children("input[type=hidden]").val(c),""!=d.input_id&&a(d.display_selector).each(function(){this.hasOwnProperty("value")?a(this).val(c):a(this).text(c)})},normalized_percentage:function(a,b,c){return(a-b)/(c-b)},normalized_value:function(a,b,c,d){var e=c-b,f=a*e,g=(f-f%d)/d,h=f%d,i=h>=.5*d?d:0;return g*d+i+b},set_translate:function(b,c,d){d?a(b).css("-webkit-transform","translateY("+c+"px)").css("-moz-transform","translateY("+c+"px)").css("-ms-transform","translateY("+c+"px)").css("-o-transform","translateY("+c+"px)").css("transform","translateY("+c+"px)"):a(b).css("-webkit-transform","translateX("+c+"px)").css("-moz-transform","translateX("+c+"px)").css("-ms-transform","translateX("+c+"px)").css("-o-transform","translateX("+c+"px)").css("transform","translateX("+c+"px)")},limit_to:function(a,b,c){return Math.min(Math.max(a,b),c)},initialize_settings:function(b){var c=a.extend({},this.settings,this.data_options(a(b).parent()));c.vertical?(a.data(b,"bar_o",a(b).parent().offset().top),a.data(b,"bar_l",a(b).parent().outerHeight()),a.data(b,"handle_o",a(b).offset().top),a.data(b,"handle_l",a(b).outerHeight())):(a.data(b,"bar_o",a(b).parent().offset().left),a.data(b,"bar_l",a(b).parent().outerWidth()),a.data(b,"handle_o",a(b).offset().left),a.data(b,"handle_l",a(b).outerWidth())),a.data(b,"bar",a(b).parent()),a.data(b,"settings",c)},set_initial_position:function(b){var c=a.data(b.children(".range-slider-handle")[0],"settings"),d=c.initial?c.initial:Math.floor(.5*(c.end-c.start)/c.step)*c.step+c.start,e=b.children(".range-slider-handle");this.set_ui(e,d)},set_value:function(b){var c=this;a("["+c.attr_name()+"]",this.scope).each(function(){a(this).attr(c.attr_name(),b)}),a(this.scope).attr(c.attr_name())&&a(this.scope).attr(c.attr_name(),b),c.reflow()},reflow:function(){var b=this;b.S("["+this.attr_name()+"]").each(function(){var c=a(this).children(".range-slider-handle")[0],d=a(this).attr(b.attr_name());b.initialize_settings(c),d?b.set_ui(a(c),parseFloat(d)):b.set_initial_position(a(this))})}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.tab={name:"tab",version:"5.3.3",settings:{active_class:"active",callback:function(){},deep_linking:!1,scroll_to_content:!0,is_hover:!1},default_tab_hashes:[],init:function(a,b,c){var d=this,e=this.S;this.bindings(b,c),this.handle_location_hash_change(),e("["+this.attr_name()+"] > .active > a",this.scope).each(function(){d.default_tab_hashes.push(this.hash)})},events:function(){var a=this,c=this.S;c(this.scope).off(".tab").on("click.fndtn.tab","["+this.attr_name()+"] > * > a",function(b){var d=c(this).closest("["+a.attr_name()+"]").data(a.attr_name(!0)+"-init");(!d.is_hover||Modernizr.touch)&&(b.preventDefault(),b.stopPropagation(),a.toggle_active_tab(c(this).parent()))}).on("mouseenter.fndtn.tab","["+this.attr_name()+"] > * > a",function(){var b=c(this).closest("["+a.attr_name()+"]").data(a.attr_name(!0)+"-init");b.is_hover&&a.toggle_active_tab(c(this).parent())}),c(b).on("hashchange.fndtn.tab",function(b){b.preventDefault(),a.handle_location_hash_change()})},handle_location_hash_change:function(){var b=this,c=this.S;c("["+this.attr_name()+"]",this.scope).each(function(){var e=c(this).data(b.attr_name(!0)+"-init");if(e.deep_linking){var f=b.scope.location.hash;if(""!=f){var g=c(f);if(g.hasClass("content")&&g.parent().hasClass("tab-content"))b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href="+f+"]").parent());else{var h=g.closest(".content").attr("id");h!=d&&b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href=#"+h+"]").parent(),f)}}else for(var i in b.default_tab_hashes)b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href="+b.default_tab_hashes[i]+"]").parent())}})},toggle_active_tab:function(c,e){var f=this.S,g=c.closest("["+this.attr_name()+"]"),h=c.children("a").first(),i="#"+h.attr("href").split("#")[1],j=f(i),k=c.siblings(),l=g.data(this.attr_name(!0)+"-init");if(f(this).data(this.data_attr("tab-content"))&&(i="#"+f(this).data(this.data_attr("tab-content")).split("#")[1],j=f(i)),l.deep_linking){var m=a("body,html").scrollTop();b.location.hash=e!=d?e:i,l.scroll_to_content?e==d||e==i?c.parent()[0].scrollIntoView():f(i)[0].scrollIntoView():(e==d||e==i)&&a("body,html").scrollTop(m)}c.addClass(l.active_class).triggerHandler("opened"),k.removeClass(l.active_class),j.siblings().removeClass(l.active_class).end().addClass(l.active_class),l.callback(c),j.triggerHandler("toggled",[c]),g.triggerHandler("toggled",[j])},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},off:function(){},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.tooltip={name:"tooltip",version:"5.3.3",settings:{additional_inheritable_classes:[],tooltip_class:".tooltip",append_to:"body",touch_close_text:"Tap To Close",disable_for_touch:!1,hover_delay:200,show_on:"all",tip_template:function(a,b){return'<span data-selector="'+a+'" class="'+Foundation.libs.tooltip.settings.tooltip_class.substring(1)+'">'+b+'<span class="nub"></span></span>'}},cache:{},init:function(a,b,c){Foundation.inherit(this,"random_str"),this.bindings(b,c)},should_show:function(b){var c=a.extend({},this.settings,this.data_options(b));return"all"===c.show_on?!0:this.small()&&"small"===c.show_on?!0:this.medium()&&"medium"===c.show_on?!0:this.large()&&"large"===c.show_on?!0:!1},medium:function(){return matchMedia(Foundation.media_queries.medium).matches},large:function(){return matchMedia(Foundation.media_queries.large).matches},events:function(b){var c=this,d=c.S;c.create(this.S(b)),a(this.scope).off(".tooltip").on("mouseenter.fndtn.tooltip mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip","["+this.attr_name()+"]",function(b){var e=d(this),f=a.extend({},c.settings,c.data_options(e)),g=!1;if(Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&d(b.target).is("a"))return!1;if(/mouse/i.test(b.type)&&c.ie_touch(b))return!1;if(e.hasClass("open"))Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&b.preventDefault(),c.hide(e);else{if(f.disable_for_touch&&Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type))return;!f.disable_for_touch&&Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&(b.preventDefault(),d(f.tooltip_class+".open").hide(),g=!0),/enter|over/i.test(b.type)?this.timer=setTimeout(function(){c.showTip(e)}.bind(this),c.settings.hover_delay):"mouseout"===b.type||"mouseleave"===b.type?(clearTimeout(this.timer),c.hide(e)):c.showTip(e)}}).on("mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip","["+this.attr_name()+"].open",function(b){return/mouse/i.test(b.type)&&c.ie_touch(b)?!1:void(("touch"!=a(this).data("tooltip-open-event-type")||"mouseleave"!=b.type)&&("mouse"==a(this).data("tooltip-open-event-type")&&/MSPointerDown|touchstart/i.test(b.type)?c.convert_to_touch(a(this)):c.hide(a(this))))}).on("DOMNodeRemoved DOMAttrModified","["+this.attr_name()+"]:not(a)",function(){c.hide(d(this))})},ie_touch:function(){return!1},showTip:function(a){var b=this.getTip(a);return this.should_show(a,b)?this.show(a):void 0},getTip:function(b){var c=this.selector(b),d=a.extend({},this.settings,this.data_options(b)),e=null;return c&&(e=this.S('span[data-selector="'+c+'"]'+d.tooltip_class)),"object"==typeof e?e:!1},selector:function(a){var b=a.attr("id"),c=a.attr(this.attr_name())||a.attr("data-selector");return(b&&b.length<1||!b)&&"string"!=typeof c&&(c=this.random_str(6),a.attr("data-selector",c)),b&&b.length>0?b:c},create:function(c){var d=this,e=a.extend({},this.settings,this.data_options(c)),f=this.settings.tip_template;"string"==typeof e.tip_template&&b.hasOwnProperty(e.tip_template)&&(f=b[e.tip_template]);var g=a(f(this.selector(c),a("<div></div>").html(c.attr("title")).html())),h=this.inheritable_classes(c);g.addClass(h).appendTo(e.append_to),Modernizr.touch&&(g.append('<span class="tap-to-close">'+e.touch_close_text+"</span>"),g.on("touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip",function(){d.hide(c)})),c.removeAttr("title").attr("title","")},reposition:function(b,c,d){var e,f,g,h,i;if(c.css("visibility","hidden").show(),e=b.data("width"),f=c.children(".nub"),g=f.outerHeight(),h=f.outerHeight(),c.css(this.small()?{width:"100%"}:{width:e?e:"auto"}),i=function(a,b,c,d,e){return a.css({top:b?b:"auto",bottom:d?d:"auto",left:e?e:"auto",right:c?c:"auto"}).end()},i(c,b.offset().top+b.outerHeight()+10,"auto","auto",b.offset().left),this.small())i(c,b.offset().top+b.outerHeight()+10,"auto","auto",12.5,a(this.scope).width()),c.addClass("tip-override"),i(f,-g,"auto","auto",b.offset().left);else{var j=b.offset().left;Foundation.rtl&&(f.addClass("rtl"),j=b.offset().left+b.outerWidth()-c.outerWidth()),i(c,b.offset().top+b.outerHeight()+10,"auto","auto",j),c.removeClass("tip-override"),d&&d.indexOf("tip-top")>-1?(Foundation.rtl&&f.addClass("rtl"),i(c,b.offset().top-c.outerHeight(),"auto","auto",j).removeClass("tip-override")):d&&d.indexOf("tip-left")>-1?(i(c,b.offset().top+b.outerHeight()/2-c.outerHeight()/2,"auto","auto",b.offset().left-c.outerWidth()-g).removeClass("tip-override"),f.removeClass("rtl")):d&&d.indexOf("tip-right")>-1&&(i(c,b.offset().top+b.outerHeight()/2-c.outerHeight()/2,"auto","auto",b.offset().left+b.outerWidth()+g).removeClass("tip-override"),f.removeClass("rtl"))}c.css("visibility","visible").hide()},small:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},inheritable_classes:function(b){var c=a.extend({},this.settings,this.data_options(b)),d=["tip-top","tip-left","tip-bottom","tip-right","radius","round"].concat(c.additional_inheritable_classes),e=b.attr("class"),f=e?a.map(e.split(" "),function(b){return-1!==a.inArray(b,d)?b:void 0}).join(" "):"";return a.trim(f)},convert_to_touch:function(b){var c=this,d=c.getTip(b),e=a.extend({},c.settings,c.data_options(b));0===d.find(".tap-to-close").length&&(d.append('<span class="tap-to-close">'+e.touch_close_text+"</span>"),d.on("click.fndtn.tooltip.tapclose touchstart.fndtn.tooltip.tapclose MSPointerDown.fndtn.tooltip.tapclose",function(){c.hide(b)})),b.data("tooltip-open-event-type","touch")},show:function(a){var b=this.getTip(a);"touch"==a.data("tooltip-open-event-type")&&this.convert_to_touch(a),this.reposition(a,b,a.attr("class")),a.addClass("open"),b.fadeIn(150)},hide:function(a){var b=this.getTip(a);b.fadeOut(150,function(){b.find(".tap-to-close").remove(),b.off("click.fndtn.tooltip.tapclose MSPointerDown.fndtn.tapclose"),a.removeClass("open")})},off:function(){var b=this;this.S(this.scope).off(".fndtn.tooltip"),this.S(this.settings.tooltip_class).each(function(c){a("["+b.attr_name()+"]").eq(c).attr("title",a(this).text())}).remove()},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c){"use strict";Foundation.libs.topbar={name:"topbar",version:"5.3.3",settings:{index:0,sticky_class:"sticky",custom_back_text:!0,back_text:"Back",mobile_show_parent_link:!0,is_hover:!0,scrolltop:!0,sticky_on:"all"},init:function(b,c,d){Foundation.inherit(this,"add_custom_rule register_media throttle");var e=this;e.register_media("topbar","foundation-mq-topbar"),this.bindings(c,d),e.S("["+this.attr_name()+"]",this.scope).each(function(){{var b=a(this),c=b.data(e.attr_name(!0)+"-init");e.S("section",this)}b.data("index",0);var d=b.parent();d.hasClass("fixed")||e.is_sticky(b,d,c)?(e.settings.sticky_class=c.sticky_class,e.settings.sticky_topbar=b,b.data("height",d.outerHeight()),b.data("stickyoffset",d.offset().top)):b.data("height",b.outerHeight()),c.assembled||e.assemble(b),c.is_hover?e.S(".has-dropdown",b).addClass("not-click"):e.S(".has-dropdown",b).removeClass("not-click"),e.add_custom_rule(".f-topbar-fixed { padding-top: "+b.data("height")+"px }"),d.hasClass("fixed")&&e.S("body").addClass("f-topbar-fixed")})},is_sticky:function(a,b,c){var d=b.hasClass(c.sticky_class);return d&&"all"===c.sticky_on?!0:d&&this.small()&&"small"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches&&!matchMedia(Foundation.media_queries.large).matches:d&&this.medium()&&"medium"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&matchMedia(Foundation.media_queries.medium).matches&&!matchMedia(Foundation.media_queries.large).matches:d&&this.large()&&"large"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&matchMedia(Foundation.media_queries.medium).matches&&matchMedia(Foundation.media_queries.large).matches:!1},toggle:function(c){var d,e=this;d=c?e.S(c).closest("["+this.attr_name()+"]"):e.S("["+this.attr_name()+"]");var f=d.data(this.attr_name(!0)+"-init"),g=e.S("section, .section",d);e.breakpoint()&&(e.rtl?(g.css({right:"0%"}),a(">.name",g).css({right:"100%"})):(g.css({left:"0%"}),a(">.name",g).css({left:"100%"})),e.S("li.moved",g).removeClass("moved"),d.data("index",0),d.toggleClass("expanded").css("height","")),f.scrolltop?d.hasClass("expanded")?d.parent().hasClass("fixed")&&(f.scrolltop?(d.parent().removeClass("fixed"),d.addClass("fixed"),e.S("body").removeClass("f-topbar-fixed"),b.scrollTo(0,0)):d.parent().removeClass("expanded")):d.hasClass("fixed")&&(d.parent().addClass("fixed"),d.removeClass("fixed"),e.S("body").addClass("f-topbar-fixed")):(e.is_sticky(d,d.parent(),f)&&d.parent().addClass("fixed"),d.parent().hasClass("fixed")&&(d.hasClass("expanded")?(d.addClass("fixed"),d.parent().addClass("expanded"),e.S("body").addClass("f-topbar-fixed")):(d.removeClass("fixed"),d.parent().removeClass("expanded"),e.update_sticky_positioning())))},timer:null,events:function(){var c=this,d=this.S;d(this.scope).off(".topbar").on("click.fndtn.topbar","["+this.attr_name()+"] .toggle-topbar",function(a){a.preventDefault(),c.toggle(this)}).on("click.fndtn.topbar",'.top-bar .top-bar-section li a[href^="#"],['+this.attr_name()+'] .top-bar-section li a[href^="#"]',function(){var b=a(this).closest("li");!c.breakpoint()||b.hasClass("back")||b.hasClass("has-dropdown")||c.toggle()}).on("click.fndtn.topbar","["+this.attr_name()+"] li.has-dropdown",function(b){var e=d(this),f=d(b.target),g=e.closest("["+c.attr_name()+"]"),h=g.data(c.attr_name(!0)+"-init");return f.data("revealId")?void c.toggle():void(c.breakpoint()||(!h.is_hover||Modernizr.touch)&&(b.stopImmediatePropagation(),e.hasClass("hover")?(e.removeClass("hover").find("li").removeClass("hover"),e.parents("li.hover").removeClass("hover")):(e.addClass("hover"),a(e).siblings().removeClass("hover"),"A"===f[0].nodeName&&f.parent().hasClass("has-dropdown")&&b.preventDefault())))}).on("click.fndtn.topbar","["+this.attr_name()+"] .has-dropdown>a",function(a){if(c.breakpoint()){a.preventDefault();var b=d(this),e=b.closest("["+c.attr_name()+"]"),f=e.find("section, .section"),g=(b.next(".dropdown").outerHeight(),b.closest("li"));e.data("index",e.data("index")+1),g.addClass("moved"),c.rtl?(f.css({right:-(100*e.data("index"))+"%"}),f.find(">.name").css({right:100*e.data("index")+"%"})):(f.css({left:-(100*e.data("index"))+"%"}),f.find(">.name").css({left:100*e.data("index")+"%"})),e.css("height",b.siblings("ul").outerHeight(!0)+e.data("height"))}}),d(b).off(".topbar").on("resize.fndtn.topbar",c.throttle(function(){c.resize.call(c)},50)).trigger("resize").trigger("resize.fndtn.topbar"),d("body").off(".topbar").on("click.fndtn.topbar",function(a){var b=d(a.target).closest("li").closest("li.hover");b.length>0||d("["+c.attr_name()+"] li.hover").removeClass("hover")}),d(this.scope).on("click.fndtn.topbar","["+this.attr_name()+"] .has-dropdown .back",function(a){a.preventDefault();var b=d(this),e=b.closest("["+c.attr_name()+"]"),f=e.find("section, .section"),g=(e.data(c.attr_name(!0)+"-init"),b.closest("li.moved")),h=g.parent();e.data("index",e.data("index")-1),c.rtl?(f.css({right:-(100*e.data("index"))+"%"}),f.find(">.name").css({right:100*e.data("index")+"%"})):(f.css({left:-(100*e.data("index"))+"%"}),f.find(">.name").css({left:100*e.data("index")+"%"})),0===e.data("index")?e.css("height",""):e.css("height",h.outerHeight(!0)+e.data("height")),setTimeout(function(){g.removeClass("moved")},300)})},resize:function(){var a=this;a.S("["+this.attr_name()+"]").each(function(){var b,d=a.S(this),e=d.data(a.attr_name(!0)+"-init"),f=d.parent("."+a.settings.sticky_class);if(!a.breakpoint()){var g=d.hasClass("expanded");d.css("height","").removeClass("expanded").find("li").removeClass("hover"),g&&a.toggle(d)}a.is_sticky(d,f,e)&&(f.hasClass("fixed")?(f.removeClass("fixed"),b=f.offset().top,a.S(c.body).hasClass("f-topbar-fixed")&&(b-=d.data("height")),d.data("stickyoffset",b),f.addClass("fixed")):(b=f.offset().top,d.data("stickyoffset",b)))})},breakpoint:function(){return!matchMedia(Foundation.media_queries.topbar).matches},small:function(){return matchMedia(Foundation.media_queries.small).matches},medium:function(){return matchMedia(Foundation.media_queries.medium).matches},large:function(){return matchMedia(Foundation.media_queries.large).matches},assemble:function(b){var c=this,d=b.data(this.attr_name(!0)+"-init"),e=c.S("section",b);e.detach(),c.S(".has-dropdown>a",e).each(function(){var b,e=c.S(this),f=e.siblings(".dropdown"),g=e.attr("href");f.find(".title.back").length||(b=a(1==d.mobile_show_parent_link&&g?'<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5></li><li class="parent-link show-for-small"><a class="parent-link js-generated" href="'+g+'">'+e.html()+"</a></li>":'<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5>'),a("h5>a",b).html(1==d.custom_back_text?d.back_text:"&laquo; "+e.html()),f.prepend(b))}),e.appendTo(b),this.sticky(),this.assembled(b)},assembled:function(b){b.data(this.attr_name(!0),a.extend({},b.data(this.attr_name(!0)),{assembled:!0}))},height:function(b){var c=0,d=this;return a("> li",b).each(function(){c+=d.S(this).outerHeight(!0)}),c},sticky:function(){var a=this;this.S(b).on("scroll",function(){a.update_sticky_positioning()})},update_sticky_positioning:function(){var a="."+this.settings.sticky_class,c=this.S(b),d=this;if(d.settings.sticky_topbar&&d.is_sticky(this.settings.sticky_topbar,this.settings.sticky_topbar.parent(),this.settings)){var e=this.settings.sticky_topbar.data("stickyoffset");d.S(a).hasClass("expanded")||(c.scrollTop()>e?d.S(a).hasClass("fixed")||(d.S(a).addClass("fixed"),d.S("body").addClass("f-topbar-fixed")):c.scrollTop()<=e&&d.S(a).hasClass("fixed")&&(d.S(a).removeClass("fixed"),d.S("body").removeClass("f-topbar-fixed")))}},off:function(){this.S(this.scope).off(".fndtn.topbar"),this.S(b).off(".fndtn.topbar")},reflow:function(){}}}(jQuery,this,this.document);
/* 4    */ 
/* 5    */ /*!
/* 6    *|  * Modernizr v2.8.3
/* 7    *|  * www.modernizr.com
/* 8    *|  *
/* 9    *|  * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
/* 10   *|  * Available under the BSD and MIT licenses: www.modernizr.com/license/
/* 11   *|  */
/* 12   */ 
/* 13   */ /*
/* 14   *|  * Modernizr tests which native CSS3 and HTML5 features are available in
/* 15   *|  * the current UA and makes the results available to you in two ways:
/* 16   *|  * as properties on a global Modernizr object, and as classes on the
/* 17   *|  * <html> element. This information allows you to progressively enhance
/* 18   *|  * your pages with a granular level of control over the experience.
/* 19   *|  *
/* 20   *|  * Modernizr has an optional (not included) conditional resource loader
/* 21   *|  * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
/* 22   *|  * To get a build that includes Modernizr.load(), as well as choosing
/* 23   *|  * which tests to include, go to www.modernizr.com/download/
/* 24   *|  *
/* 25   *|  * Authors        Faruk Ates, Paul Irish, Alex Sexton
/* 26   *|  * Contributors   Ryan Seddon, Ben Alman
/* 27   *|  */
/* 28   */ 
/* 29   */ window.Modernizr = (function( window, document, undefined ) {
/* 30   */ 
/* 31   */     var version = '2.8.3',
/* 32   */ 
/* 33   */     Modernizr = {},
/* 34   */ 
/* 35   */     /*>>cssclasses*/
/* 36   */     // option for enabling the HTML classes to be added
/* 37   */     enableClasses = true,
/* 38   */     /*>>cssclasses*/
/* 39   */ 
/* 40   */     docElement = document.documentElement,
/* 41   */ 
/* 42   */     /**
/* 43   *|      * Create our "modernizr" element that we do most feature tests on.
/* 44   *|      */
/* 45   */     mod = 'modernizr',
/* 46   */     modElem = document.createElement(mod),
/* 47   */     mStyle = modElem.style,
/* 48   */ 
/* 49   */     /**
/* 50   *|      * Create the input element for various Web Forms feature tests.

/* foundation.min.js */

/* 51   *|      */
/* 52   */     inputElem /*>>inputelem*/ = document.createElement('input') /*>>inputelem*/ ,
/* 53   */ 
/* 54   */     /*>>smile*/
/* 55   */     smile = ':)',
/* 56   */     /*>>smile*/
/* 57   */ 
/* 58   */     toString = {}.toString,
/* 59   */ 
/* 60   */     // TODO :: make the prefixes more granular
/* 61   */     /*>>prefixes*/
/* 62   */     // List of property values to set for css tests. See ticket #21
/* 63   */     prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
/* 64   */     /*>>prefixes*/
/* 65   */ 
/* 66   */     /*>>domprefixes*/
/* 67   */     // Following spec is to expose vendor-specific style properties as:
/* 68   */     //   elem.style.WebkitBorderRadius
/* 69   */     // and the following would be incorrect:
/* 70   */     //   elem.style.webkitBorderRadius
/* 71   */ 
/* 72   */     // Webkit ghosts their properties in lowercase but Opera & Moz do not.
/* 73   */     // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
/* 74   */     //   erik.eae.net/archives/2008/03/10/21.48.10/
/* 75   */ 
/* 76   */     // More here: github.com/Modernizr/Modernizr/issues/issue/21
/* 77   */     omPrefixes = 'Webkit Moz O ms',
/* 78   */ 
/* 79   */     cssomPrefixes = omPrefixes.split(' '),
/* 80   */ 
/* 81   */     domPrefixes = omPrefixes.toLowerCase().split(' '),
/* 82   */     /*>>domprefixes*/
/* 83   */ 
/* 84   */     /*>>ns*/
/* 85   */     ns = {'svg': 'http://www.w3.org/2000/svg'},
/* 86   */     /*>>ns*/
/* 87   */ 
/* 88   */     tests = {},
/* 89   */     inputs = {},
/* 90   */     attrs = {},
/* 91   */ 
/* 92   */     classes = [],
/* 93   */ 
/* 94   */     slice = classes.slice,
/* 95   */ 
/* 96   */     featureName, // used in testing loop
/* 97   */ 
/* 98   */ 
/* 99   */     /*>>teststyles*/
/* 100  */     // Inject element with style element and some CSS rules

/* foundation.min.js */

/* 101  */     injectElementWithStyles = function( rule, callback, nodes, testnames ) {
/* 102  */ 
/* 103  */       var style, ret, node, docOverflow,
/* 104  */           div = document.createElement('div'),
/* 105  */           // After page load injecting a fake body doesn't work so check if body exists
/* 106  */           body = document.body,
/* 107  */           // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
/* 108  */           fakeBody = body || document.createElement('body');
/* 109  */ 
/* 110  */       if ( parseInt(nodes, 10) ) {
/* 111  */           // In order not to give false positives we create a node for each test
/* 112  */           // This also allows the method to scale for unspecified uses
/* 113  */           while ( nodes-- ) {
/* 114  */               node = document.createElement('div');
/* 115  */               node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
/* 116  */               div.appendChild(node);
/* 117  */           }
/* 118  */       }
/* 119  */ 
/* 120  */       // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
/* 121  */       // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
/* 122  */       // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
/* 123  */       // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
/* 124  */       // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
/* 125  */       style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
/* 126  */       div.id = mod;
/* 127  */       // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
/* 128  */       // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
/* 129  */       (body ? div : fakeBody).innerHTML += style;
/* 130  */       fakeBody.appendChild(div);
/* 131  */       if ( !body ) {
/* 132  */           //avoid crashing IE8, if background image is used
/* 133  */           fakeBody.style.background = '';
/* 134  */           //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
/* 135  */           fakeBody.style.overflow = 'hidden';
/* 136  */           docOverflow = docElement.style.overflow;
/* 137  */           docElement.style.overflow = 'hidden';
/* 138  */           docElement.appendChild(fakeBody);
/* 139  */       }
/* 140  */ 
/* 141  */       ret = callback(div, rule);
/* 142  */       // If this is done after page load we don't want to remove the body so check if body exists
/* 143  */       if ( !body ) {
/* 144  */           fakeBody.parentNode.removeChild(fakeBody);
/* 145  */           docElement.style.overflow = docOverflow;
/* 146  */       } else {
/* 147  */           div.parentNode.removeChild(div);
/* 148  */       }
/* 149  */ 
/* 150  */       return !!ret;

/* foundation.min.js */

/* 151  */ 
/* 152  */     },
/* 153  */     /*>>teststyles*/
/* 154  */ 
/* 155  */     /*>>mq*/
/* 156  */     // adapted from matchMedia polyfill
/* 157  */     // by Scott Jehl and Paul Irish
/* 158  */     // gist.github.com/786768
/* 159  */     testMediaQuery = function( mq ) {
/* 160  */ 
/* 161  */       var matchMedia = window.matchMedia || window.msMatchMedia;
/* 162  */       if ( matchMedia ) {
/* 163  */         return matchMedia(mq) && matchMedia(mq).matches || false;
/* 164  */       }
/* 165  */ 
/* 166  */       var bool;
/* 167  */ 
/* 168  */       injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function( node ) {
/* 169  */         bool = (window.getComputedStyle ?
/* 170  */                   getComputedStyle(node, null) :
/* 171  */                   node.currentStyle)['position'] == 'absolute';
/* 172  */       });
/* 173  */ 
/* 174  */       return bool;
/* 175  */ 
/* 176  */      },
/* 177  */      /*>>mq*/
/* 178  */ 
/* 179  */ 
/* 180  */     /*>>hasevent*/
/* 181  */     //
/* 182  */     // isEventSupported determines if a given element supports the given event
/* 183  */     // kangax.github.com/iseventsupported/
/* 184  */     //
/* 185  */     // The following results are known incorrects:
/* 186  */     //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
/* 187  */     //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
/* 188  */     //   ...
/* 189  */     isEventSupported = (function() {
/* 190  */ 
/* 191  */       var TAGNAMES = {
/* 192  */         'select': 'input', 'change': 'input',
/* 193  */         'submit': 'form', 'reset': 'form',
/* 194  */         'error': 'img', 'load': 'img', 'abort': 'img'
/* 195  */       };
/* 196  */ 
/* 197  */       function isEventSupported( eventName, element ) {
/* 198  */ 
/* 199  */         element = element || document.createElement(TAGNAMES[eventName] || 'div');
/* 200  */         eventName = 'on' + eventName;

/* foundation.min.js */

/* 201  */ 
/* 202  */         // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
/* 203  */         var isSupported = eventName in element;
/* 204  */ 
/* 205  */         if ( !isSupported ) {
/* 206  */           // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
/* 207  */           if ( !element.setAttribute ) {
/* 208  */             element = document.createElement('div');
/* 209  */           }
/* 210  */           if ( element.setAttribute && element.removeAttribute ) {
/* 211  */             element.setAttribute(eventName, '');
/* 212  */             isSupported = is(element[eventName], 'function');
/* 213  */ 
/* 214  */             // If property was created, "remove it" (by setting value to `undefined`)
/* 215  */             if ( !is(element[eventName], 'undefined') ) {
/* 216  */               element[eventName] = undefined;
/* 217  */             }
/* 218  */             element.removeAttribute(eventName);
/* 219  */           }
/* 220  */         }
/* 221  */ 
/* 222  */         element = null;
/* 223  */         return isSupported;
/* 224  */       }
/* 225  */       return isEventSupported;
/* 226  */     })(),
/* 227  */     /*>>hasevent*/
/* 228  */ 
/* 229  */     // TODO :: Add flag for hasownprop ? didn't last time
/* 230  */ 
/* 231  */     // hasOwnProperty shim by kangax needed for Safari 2.0 support
/* 232  */     _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;
/* 233  */ 
/* 234  */     if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
/* 235  */       hasOwnProp = function (object, property) {
/* 236  */         return _hasOwnProperty.call(object, property);
/* 237  */       };
/* 238  */     }
/* 239  */     else {
/* 240  */       hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
/* 241  */         return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
/* 242  */       };
/* 243  */     }
/* 244  */ 
/* 245  */     // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
/* 246  */     // es5.github.com/#x15.3.4.5
/* 247  */ 
/* 248  */     if (!Function.prototype.bind) {
/* 249  */       Function.prototype.bind = function bind(that) {
/* 250  */ 

/* foundation.min.js */

/* 251  */         var target = this;
/* 252  */ 
/* 253  */         if (typeof target != "function") {
/* 254  */             throw new TypeError();
/* 255  */         }
/* 256  */ 
/* 257  */         var args = slice.call(arguments, 1),
/* 258  */             bound = function () {
/* 259  */ 
/* 260  */             if (this instanceof bound) {
/* 261  */ 
/* 262  */               var F = function(){};
/* 263  */               F.prototype = target.prototype;
/* 264  */               var self = new F();
/* 265  */ 
/* 266  */               var result = target.apply(
/* 267  */                   self,
/* 268  */                   args.concat(slice.call(arguments))
/* 269  */               );
/* 270  */               if (Object(result) === result) {
/* 271  */                   return result;
/* 272  */               }
/* 273  */               return self;
/* 274  */ 
/* 275  */             } else {
/* 276  */ 
/* 277  */               return target.apply(
/* 278  */                   that,
/* 279  */                   args.concat(slice.call(arguments))
/* 280  */               );
/* 281  */ 
/* 282  */             }
/* 283  */ 
/* 284  */         };
/* 285  */ 
/* 286  */         return bound;
/* 287  */       };
/* 288  */     }
/* 289  */ 
/* 290  */     /**
/* 291  *|      * setCss applies given styles to the Modernizr DOM node.
/* 292  *|      */
/* 293  */     function setCss( str ) {
/* 294  */         mStyle.cssText = str;
/* 295  */     }
/* 296  */ 
/* 297  */     /**
/* 298  *|      * setCssAll extrapolates all vendor-specific css strings.
/* 299  *|      */
/* 300  */     function setCssAll( str1, str2 ) {

/* foundation.min.js */

/* 301  */         return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
/* 302  */     }
/* 303  */ 
/* 304  */     /**
/* 305  *|      * is returns a boolean for if typeof obj is exactly type.
/* 306  *|      */
/* 307  */     function is( obj, type ) {
/* 308  */         return typeof obj === type;
/* 309  */     }
/* 310  */ 
/* 311  */     /**
/* 312  *|      * contains returns a boolean for if substr is found within str.
/* 313  *|      */
/* 314  */     function contains( str, substr ) {
/* 315  */         return !!~('' + str).indexOf(substr);
/* 316  */     }
/* 317  */ 
/* 318  */     /*>>testprop*/
/* 319  */ 
/* 320  */     // testProps is a generic CSS / DOM property test.
/* 321  */ 
/* 322  */     // In testing support for a given CSS property, it's legit to test:
/* 323  */     //    `elem.style[styleName] !== undefined`
/* 324  */     // If the property is supported it will return an empty string,
/* 325  */     // if unsupported it will return undefined.
/* 326  */ 
/* 327  */     // We'll take advantage of this quick test and skip setting a style
/* 328  */     // on our modernizr element, but instead just testing undefined vs
/* 329  */     // empty string.
/* 330  */ 
/* 331  */     // Because the testing of the CSS property names (with "-", as
/* 332  */     // opposed to the camelCase DOM properties) is non-portable and
/* 333  */     // non-standard but works in WebKit and IE (but not Gecko or Opera),
/* 334  */     // we explicitly reject properties with dashes so that authors
/* 335  */     // developing in WebKit or IE first don't end up with
/* 336  */     // browser-specific content by accident.
/* 337  */ 
/* 338  */     function testProps( props, prefixed ) {
/* 339  */         for ( var i in props ) {
/* 340  */             var prop = props[i];
/* 341  */             if ( !contains(prop, "-") && mStyle[prop] !== undefined ) {
/* 342  */                 return prefixed == 'pfx' ? prop : true;
/* 343  */             }
/* 344  */         }
/* 345  */         return false;
/* 346  */     }
/* 347  */     /*>>testprop*/
/* 348  */ 
/* 349  */     // TODO :: add testDOMProps
/* 350  */     /**

/* foundation.min.js */

/* 351  *|      * testDOMProps is a generic DOM property test; if a browser supports
/* 352  *|      *   a certain property, it won't return undefined for it.
/* 353  *|      */
/* 354  */     function testDOMProps( props, obj, elem ) {
/* 355  */         for ( var i in props ) {
/* 356  */             var item = obj[props[i]];
/* 357  */             if ( item !== undefined) {
/* 358  */ 
/* 359  */                 // return the property name as a string
/* 360  */                 if (elem === false) return props[i];
/* 361  */ 
/* 362  */                 // let's bind a function
/* 363  */                 if (is(item, 'function')){
/* 364  */                   // default to autobind unless override
/* 365  */                   return item.bind(elem || obj);
/* 366  */                 }
/* 367  */ 
/* 368  */                 // return the unbound function or obj or value
/* 369  */                 return item;
/* 370  */             }
/* 371  */         }
/* 372  */         return false;
/* 373  */     }
/* 374  */ 
/* 375  */     /*>>testallprops*/
/* 376  */     /**
/* 377  *|      * testPropsAll tests a list of DOM properties we want to check against.
/* 378  *|      *   We specify literally ALL possible (known and/or likely) properties on
/* 379  *|      *   the element including the non-vendor prefixed one, for forward-
/* 380  *|      *   compatibility.
/* 381  *|      */
/* 382  */     function testPropsAll( prop, prefixed, elem ) {
/* 383  */ 
/* 384  */         var ucProp  = prop.charAt(0).toUpperCase() + prop.slice(1),
/* 385  */             props   = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');
/* 386  */ 
/* 387  */         // did they call .prefixed('boxSizing') or are we just testing a prop?
/* 388  */         if(is(prefixed, "string") || is(prefixed, "undefined")) {
/* 389  */           return testProps(props, prefixed);
/* 390  */ 
/* 391  */         // otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
/* 392  */         } else {
/* 393  */           props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
/* 394  */           return testDOMProps(props, prefixed, elem);
/* 395  */         }
/* 396  */     }
/* 397  */     /*>>testallprops*/
/* 398  */ 
/* 399  */ 
/* 400  */     /**

/* foundation.min.js */

/* 401  *|      * Tests
/* 402  *|      * -----
/* 403  *|      */
/* 404  */ 
/* 405  */     // The *new* flexbox
/* 406  */     // dev.w3.org/csswg/css3-flexbox
/* 407  */ 
/* 408  */     tests['flexbox'] = function() {
/* 409  */       return testPropsAll('flexWrap');
/* 410  */     };
/* 411  */ 
/* 412  */     // The *old* flexbox
/* 413  */     // www.w3.org/TR/2009/WD-css3-flexbox-20090723/
/* 414  */ 
/* 415  */     tests['flexboxlegacy'] = function() {
/* 416  */         return testPropsAll('boxDirection');
/* 417  */     };
/* 418  */ 
/* 419  */     // On the S60 and BB Storm, getContext exists, but always returns undefined
/* 420  */     // so we actually have to call getContext() to verify
/* 421  */     // github.com/Modernizr/Modernizr/issues/issue/97/
/* 422  */ 
/* 423  */     tests['canvas'] = function() {
/* 424  */         var elem = document.createElement('canvas');
/* 425  */         return !!(elem.getContext && elem.getContext('2d'));
/* 426  */     };
/* 427  */ 
/* 428  */     tests['canvastext'] = function() {
/* 429  */         return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
/* 430  */     };
/* 431  */ 
/* 432  */     // webk.it/70117 is tracking a legit WebGL feature detect proposal
/* 433  */ 
/* 434  */     // We do a soft detect which may false positive in order to avoid
/* 435  */     // an expensive context creation: bugzil.la/732441
/* 436  */ 
/* 437  */     tests['webgl'] = function() {
/* 438  */         return !!window.WebGLRenderingContext;
/* 439  */     };
/* 440  */ 
/* 441  */     /*
/* 442  *|      * The Modernizr.touch test only indicates if the browser supports
/* 443  *|      *    touch events, which does not necessarily reflect a touchscreen
/* 444  *|      *    device, as evidenced by tablets running Windows 7 or, alas,
/* 445  *|      *    the Palm Pre / WebOS (touch) phones.
/* 446  *|      *
/* 447  *|      * Additionally, Chrome (desktop) used to lie about its support on this,
/* 448  *|      *    but that has since been rectified: crbug.com/36415
/* 449  *|      *
/* 450  *|      * We also test for Firefox 4 Multitouch Support.

/* foundation.min.js */

/* 451  *|      *
/* 452  *|      * For more info, see: modernizr.github.com/Modernizr/touch.html
/* 453  *|      */
/* 454  */ 
/* 455  */     tests['touch'] = function() {
/* 456  */         var bool;
/* 457  */ 
/* 458  */         if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
/* 459  */           bool = true;
/* 460  */         } else {
/* 461  */           injectElementWithStyles(['@media (',prefixes.join('touch-enabled),('),mod,')','{#modernizr{top:9px;position:absolute}}'].join(''), function( node ) {
/* 462  */             bool = node.offsetTop === 9;
/* 463  */           });
/* 464  */         }
/* 465  */ 
/* 466  */         return bool;
/* 467  */     };
/* 468  */ 
/* 469  */ 
/* 470  */     // geolocation is often considered a trivial feature detect...
/* 471  */     // Turns out, it's quite tricky to get right:
/* 472  */     //
/* 473  */     // Using !!navigator.geolocation does two things we don't want. It:
/* 474  */     //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
/* 475  */     //   2. Disables page caching in WebKit: webk.it/43956
/* 476  */     //
/* 477  */     // Meanwhile, in Firefox < 8, an about:config setting could expose
/* 478  */     // a false positive that would throw an exception: bugzil.la/688158
/* 479  */ 
/* 480  */     tests['geolocation'] = function() {
/* 481  */         return 'geolocation' in navigator;
/* 482  */     };
/* 483  */ 
/* 484  */ 
/* 485  */     tests['postmessage'] = function() {
/* 486  */       return !!window.postMessage;
/* 487  */     };
/* 488  */ 
/* 489  */ 
/* 490  */     // Chrome incognito mode used to throw an exception when using openDatabase
/* 491  */     // It doesn't anymore.
/* 492  */     tests['websqldatabase'] = function() {
/* 493  */       return !!window.openDatabase;
/* 494  */     };
/* 495  */ 
/* 496  */     // Vendors had inconsistent prefixing with the experimental Indexed DB:
/* 497  */     // - Webkit's implementation is accessible through webkitIndexedDB
/* 498  */     // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
/* 499  */     // For speed, we don't test the legacy (and beta-only) indexedDB
/* 500  */     tests['indexedDB'] = function() {

/* foundation.min.js */

/* 501  */       return !!testPropsAll("indexedDB", window);
/* 502  */     };
/* 503  */ 
/* 504  */     // documentMode logic from YUI to filter out IE8 Compat Mode
/* 505  */     //   which false positives.
/* 506  */     tests['hashchange'] = function() {
/* 507  */       return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
/* 508  */     };
/* 509  */ 
/* 510  */     // Per 1.6:
/* 511  */     // This used to be Modernizr.historymanagement but the longer
/* 512  */     // name has been deprecated in favor of a shorter and property-matching one.
/* 513  */     // The old API is still available in 1.6, but as of 2.0 will throw a warning,
/* 514  */     // and in the first release thereafter disappear entirely.
/* 515  */     tests['history'] = function() {
/* 516  */       return !!(window.history && history.pushState);
/* 517  */     };
/* 518  */ 
/* 519  */     tests['draganddrop'] = function() {
/* 520  */         var div = document.createElement('div');
/* 521  */         return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
/* 522  */     };
/* 523  */ 
/* 524  */     // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
/* 525  */     // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
/* 526  */     // FF10 still uses prefixes, so check for it until then.
/* 527  */     // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
/* 528  */     tests['websockets'] = function() {
/* 529  */         return 'WebSocket' in window || 'MozWebSocket' in window;
/* 530  */     };
/* 531  */ 
/* 532  */ 
/* 533  */     // css-tricks.com/rgba-browser-support/
/* 534  */     tests['rgba'] = function() {
/* 535  */         // Set an rgba() color and check the returned value
/* 536  */ 
/* 537  */         setCss('background-color:rgba(150,255,150,.5)');
/* 538  */ 
/* 539  */         return contains(mStyle.backgroundColor, 'rgba');
/* 540  */     };
/* 541  */ 
/* 542  */     tests['hsla'] = function() {
/* 543  */         // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
/* 544  */         //   except IE9 who retains it as hsla
/* 545  */ 
/* 546  */         setCss('background-color:hsla(120,40%,100%,.5)');
/* 547  */ 
/* 548  */         return contains(mStyle.backgroundColor, 'rgba') || contains(mStyle.backgroundColor, 'hsla');
/* 549  */     };
/* 550  */ 

/* foundation.min.js */

/* 551  */     tests['multiplebgs'] = function() {
/* 552  */         // Setting multiple images AND a color on the background shorthand property
/* 553  */         //  and then querying the style.background property value for the number of
/* 554  */         //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!
/* 555  */ 
/* 556  */         setCss('background:url(https://),url(https://),red url(https://)');
/* 557  */ 
/* 558  */         // If the UA supports multiple backgrounds, there should be three occurrences
/* 559  */         //   of the string "url(" in the return value for elemStyle.background
/* 560  */ 
/* 561  */         return (/(url\s*\(.*?){3}/).test(mStyle.background);
/* 562  */     };
/* 563  */ 
/* 564  */ 
/* 565  */ 
/* 566  */     // this will false positive in Opera Mini
/* 567  */     //   github.com/Modernizr/Modernizr/issues/396
/* 568  */ 
/* 569  */     tests['backgroundsize'] = function() {
/* 570  */         return testPropsAll('backgroundSize');
/* 571  */     };
/* 572  */ 
/* 573  */     tests['borderimage'] = function() {
/* 574  */         return testPropsAll('borderImage');
/* 575  */     };
/* 576  */ 
/* 577  */ 
/* 578  */     // Super comprehensive table about all the unique implementations of
/* 579  */     // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance
/* 580  */ 
/* 581  */     tests['borderradius'] = function() {
/* 582  */         return testPropsAll('borderRadius');
/* 583  */     };
/* 584  */ 
/* 585  */     // WebOS unfortunately false positives on this test.
/* 586  */     tests['boxshadow'] = function() {
/* 587  */         return testPropsAll('boxShadow');
/* 588  */     };
/* 589  */ 
/* 590  */     // FF3.0 will false positive on this test
/* 591  */     tests['textshadow'] = function() {
/* 592  */         return document.createElement('div').style.textShadow === '';
/* 593  */     };
/* 594  */ 
/* 595  */ 
/* 596  */     tests['opacity'] = function() {
/* 597  */         // Browsers that actually have CSS Opacity implemented have done so
/* 598  */         //  according to spec, which means their return values are within the
/* 599  */         //  range of [0.0,1.0] - including the leading zero.
/* 600  */ 

/* foundation.min.js */

/* 601  */         setCssAll('opacity:.55');
/* 602  */ 
/* 603  */         // The non-literal . in this regex is intentional:
/* 604  */         //   German Chrome returns this value as 0,55
/* 605  */         // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
/* 606  */         return (/^0.55$/).test(mStyle.opacity);
/* 607  */     };
/* 608  */ 
/* 609  */ 
/* 610  */     // Note, Android < 4 will pass this test, but can only animate
/* 611  */     //   a single property at a time
/* 612  */     //   goo.gl/v3V4Gp
/* 613  */     tests['cssanimations'] = function() {
/* 614  */         return testPropsAll('animationName');
/* 615  */     };
/* 616  */ 
/* 617  */ 
/* 618  */     tests['csscolumns'] = function() {
/* 619  */         return testPropsAll('columnCount');
/* 620  */     };
/* 621  */ 
/* 622  */ 
/* 623  */     tests['cssgradients'] = function() {
/* 624  */         /**
/* 625  *|          * For CSS Gradients syntax, please see:
/* 626  *|          * webkit.org/blog/175/introducing-css-gradients/
/* 627  *|          * developer.mozilla.org/en/CSS/-moz-linear-gradient
/* 628  *|          * developer.mozilla.org/en/CSS/-moz-radial-gradient
/* 629  *|          * dev.w3.org/csswg/css3-images/#gradients-
/* 630  *|          */
/* 631  */ 
/* 632  */         var str1 = 'background-image:',
/* 633  */             str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
/* 634  */             str3 = 'linear-gradient(left top,#9f9, white);';
/* 635  */ 
/* 636  */         setCss(
/* 637  */              // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
/* 638  */               (str1 + '-webkit- '.split(' ').join(str2 + str1) +
/* 639  */              // standard syntax             // trailing 'background-image:'
/* 640  */               prefixes.join(str3 + str1)).slice(0, -str1.length)
/* 641  */         );
/* 642  */ 
/* 643  */         return contains(mStyle.backgroundImage, 'gradient');
/* 644  */     };
/* 645  */ 
/* 646  */ 
/* 647  */     tests['cssreflections'] = function() {
/* 648  */         return testPropsAll('boxReflect');
/* 649  */     };
/* 650  */ 

/* foundation.min.js */

/* 651  */ 
/* 652  */     tests['csstransforms'] = function() {
/* 653  */         return !!testPropsAll('transform');
/* 654  */     };
/* 655  */ 
/* 656  */ 
/* 657  */     tests['csstransforms3d'] = function() {
/* 658  */ 
/* 659  */         var ret = !!testPropsAll('perspective');
/* 660  */ 
/* 661  */         // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
/* 662  */         //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
/* 663  */         //   some conditions. As a result, Webkit typically recognizes the syntax but
/* 664  */         //   will sometimes throw a false positive, thus we must do a more thorough check:
/* 665  */         if ( ret && 'webkitPerspective' in docElement.style ) {
/* 666  */ 
/* 667  */           // Webkit allows this media query to succeed only if the feature is enabled.
/* 668  */           // `@media (transform-3d),(-webkit-transform-3d){ ... }`
/* 669  */           injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function( node, rule ) {
/* 670  */             ret = node.offsetLeft === 9 && node.offsetHeight === 3;
/* 671  */           });
/* 672  */         }
/* 673  */         return ret;
/* 674  */     };
/* 675  */ 
/* 676  */ 
/* 677  */     tests['csstransitions'] = function() {
/* 678  */         return testPropsAll('transition');
/* 679  */     };
/* 680  */ 
/* 681  */ 
/* 682  */     /*>>fontface*/
/* 683  */     // @font-face detection routine by Diego Perini
/* 684  */     // javascript.nwbox.com/CSSSupport/
/* 685  */ 
/* 686  */     // false positives:
/* 687  */     //   WebOS github.com/Modernizr/Modernizr/issues/342
/* 688  */     //   WP7   github.com/Modernizr/Modernizr/issues/538
/* 689  */     tests['fontface'] = function() {
/* 690  */         var bool;
/* 691  */ 
/* 692  */         injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function( node, rule ) {
/* 693  */           var style = document.getElementById('smodernizr'),
/* 694  */               sheet = style.sheet || style.styleSheet,
/* 695  */               cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';
/* 696  */ 
/* 697  */           bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
/* 698  */         });
/* 699  */ 
/* 700  */         return bool;

/* foundation.min.js */

/* 701  */     };
/* 702  */     /*>>fontface*/
/* 703  */ 
/* 704  */     // CSS generated content detection
/* 705  */     tests['generatedcontent'] = function() {
/* 706  */         var bool;
/* 707  */ 
/* 708  */         injectElementWithStyles(['#',mod,'{font:0/0 a}#',mod,':after{content:"',smile,'";visibility:hidden;font:3px/1 a}'].join(''), function( node ) {
/* 709  */           bool = node.offsetHeight >= 3;
/* 710  */         });
/* 711  */ 
/* 712  */         return bool;
/* 713  */     };
/* 714  */ 
/* 715  */ 
/* 716  */ 
/* 717  */     // These tests evaluate support of the video/audio elements, as well as
/* 718  */     // testing what types of content they support.
/* 719  */     //
/* 720  */     // We're using the Boolean constructor here, so that we can extend the value
/* 721  */     // e.g.  Modernizr.video     // true
/* 722  */     //       Modernizr.video.ogg // 'probably'
/* 723  */     //
/* 724  */     // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
/* 725  */     //                     thx to NielsLeenheer and zcorpan
/* 726  */ 
/* 727  */     // Note: in some older browsers, "no" was a return value instead of empty string.
/* 728  */     //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
/* 729  */     //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5
/* 730  */ 
/* 731  */     tests['video'] = function() {
/* 732  */         var elem = document.createElement('video'),
/* 733  */             bool = false;
/* 734  */ 
/* 735  */         // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
/* 736  */         try {
/* 737  */             if ( bool = !!elem.canPlayType ) {
/* 738  */                 bool      = new Boolean(bool);
/* 739  */                 bool.ogg  = elem.canPlayType('video/ogg; codecs="theora"')      .replace(/^no$/,'');
/* 740  */ 
/* 741  */                 // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
/* 742  */                 bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"') .replace(/^no$/,'');
/* 743  */ 
/* 744  */                 bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');
/* 745  */             }
/* 746  */ 
/* 747  */         } catch(e) { }
/* 748  */ 
/* 749  */         return bool;
/* 750  */     };

/* foundation.min.js */

/* 751  */ 
/* 752  */     tests['audio'] = function() {
/* 753  */         var elem = document.createElement('audio'),
/* 754  */             bool = false;
/* 755  */ 
/* 756  */         try {
/* 757  */             if ( bool = !!elem.canPlayType ) {
/* 758  */                 bool      = new Boolean(bool);
/* 759  */                 bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'');
/* 760  */                 bool.mp3  = elem.canPlayType('audio/mpeg;')               .replace(/^no$/,'');
/* 761  */ 
/* 762  */                 // Mimetypes accepted:
/* 763  */                 //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
/* 764  */                 //   bit.ly/iphoneoscodecs
/* 765  */                 bool.wav  = elem.canPlayType('audio/wav; codecs="1"')     .replace(/^no$/,'');
/* 766  */                 bool.m4a  = ( elem.canPlayType('audio/x-m4a;')            ||
/* 767  */                               elem.canPlayType('audio/aac;'))             .replace(/^no$/,'');
/* 768  */             }
/* 769  */         } catch(e) { }
/* 770  */ 
/* 771  */         return bool;
/* 772  */     };
/* 773  */ 
/* 774  */ 
/* 775  */     // In FF4, if disabled, window.localStorage should === null.
/* 776  */ 
/* 777  */     // Normally, we could not test that directly and need to do a
/* 778  */     //   `('localStorage' in window) && ` test first because otherwise Firefox will
/* 779  */     //   throw bugzil.la/365772 if cookies are disabled
/* 780  */ 
/* 781  */     // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
/* 782  */     // will throw the exception:
/* 783  */     //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
/* 784  */     // Peculiarly, getItem and removeItem calls do not throw.
/* 785  */ 
/* 786  */     // Because we are forced to try/catch this, we'll go aggressive.
/* 787  */ 
/* 788  */     // Just FWIW: IE8 Compat mode supports these features completely:
/* 789  */     //   www.quirksmode.org/dom/html5.html
/* 790  */     // But IE8 doesn't support either with local files
/* 791  */ 
/* 792  */     tests['localstorage'] = function() {
/* 793  */         try {
/* 794  */             localStorage.setItem(mod, mod);
/* 795  */             localStorage.removeItem(mod);
/* 796  */             return true;
/* 797  */         } catch(e) {
/* 798  */             return false;
/* 799  */         }
/* 800  */     };

/* foundation.min.js */

/* 801  */ 
/* 802  */     tests['sessionstorage'] = function() {
/* 803  */         try {
/* 804  */             sessionStorage.setItem(mod, mod);
/* 805  */             sessionStorage.removeItem(mod);
/* 806  */             return true;
/* 807  */         } catch(e) {
/* 808  */             return false;
/* 809  */         }
/* 810  */     };
/* 811  */ 
/* 812  */ 
/* 813  */     tests['webworkers'] = function() {
/* 814  */         return !!window.Worker;
/* 815  */     };
/* 816  */ 
/* 817  */ 
/* 818  */     tests['applicationcache'] = function() {
/* 819  */         return !!window.applicationCache;
/* 820  */     };
/* 821  */ 
/* 822  */ 
/* 823  */     // Thanks to Erik Dahlstrom
/* 824  */     tests['svg'] = function() {
/* 825  */         return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
/* 826  */     };
/* 827  */ 
/* 828  */     // specifically for SVG inline in HTML, not within XHTML
/* 829  */     // test page: paulirish.com/demo/inline-svg
/* 830  */     tests['inlinesvg'] = function() {
/* 831  */       var div = document.createElement('div');
/* 832  */       div.innerHTML = '<svg/>';
/* 833  */       return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
/* 834  */     };
/* 835  */ 
/* 836  */     // SVG SMIL animation
/* 837  */     tests['smil'] = function() {
/* 838  */         return !!document.createElementNS && /SVGAnimate/.test(toString.call(document.createElementNS(ns.svg, 'animate')));
/* 839  */     };
/* 840  */ 
/* 841  */     // This test is only for clip paths in SVG proper, not clip paths on HTML content
/* 842  */     // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg
/* 843  */ 
/* 844  */     // However read the comments to dig into applying SVG clippaths to HTML content here:
/* 845  */     //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
/* 846  */     tests['svgclippaths'] = function() {
/* 847  */         return !!document.createElementNS && /SVGClipPath/.test(toString.call(document.createElementNS(ns.svg, 'clipPath')));
/* 848  */     };
/* 849  */ 
/* 850  */     /*>>webforms*/

/* foundation.min.js */

/* 851  */     // input features and input types go directly onto the ret object, bypassing the tests loop.
/* 852  */     // Hold this guy to execute in a moment.
/* 853  */     function webforms() {
/* 854  */         /*>>input*/
/* 855  */         // Run through HTML5's new input attributes to see if the UA understands any.
/* 856  */         // We're using f which is the <input> element created early on
/* 857  */         // Mike Taylr has created a comprehensive resource for testing these attributes
/* 858  */         //   when applied to all input types:
/* 859  */         //   miketaylr.com/code/input-type-attr.html
/* 860  */         // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary
/* 861  */ 
/* 862  */         // Only input placeholder is tested while textarea's placeholder is not.
/* 863  */         // Currently Safari 4 and Opera 11 have support only for the input placeholder
/* 864  */         // Both tests are available in feature-detects/forms-placeholder.js
/* 865  */         Modernizr['input'] = (function( props ) {
/* 866  */             for ( var i = 0, len = props.length; i < len; i++ ) {
/* 867  */                 attrs[ props[i] ] = !!(props[i] in inputElem);
/* 868  */             }
/* 869  */             if (attrs.list){
/* 870  */               // safari false positive's on datalist: webk.it/74252
/* 871  */               // see also github.com/Modernizr/Modernizr/issues/146
/* 872  */               attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
/* 873  */             }
/* 874  */             return attrs;
/* 875  */         })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
/* 876  */         /*>>input*/
/* 877  */ 
/* 878  */         /*>>inputtypes*/
/* 879  */         // Run through HTML5's new input types to see if the UA understands any.
/* 880  */         //   This is put behind the tests runloop because it doesn't return a
/* 881  */         //   true/false like all the other tests; instead, it returns an object
/* 882  */         //   containing each input type with its corresponding true/false value
/* 883  */ 
/* 884  */         // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
/* 885  */         Modernizr['inputtypes'] = (function(props) {
/* 886  */ 
/* 887  */             for ( var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++ ) {
/* 888  */ 
/* 889  */                 inputElem.setAttribute('type', inputElemType = props[i]);
/* 890  */                 bool = inputElem.type !== 'text';
/* 891  */ 
/* 892  */                 // We first check to see if the type we give it sticks..
/* 893  */                 // If the type does, we feed it a textual value, which shouldn't be valid.
/* 894  */                 // If the value doesn't stick, we know there's input sanitization which infers a custom UI
/* 895  */                 if ( bool ) {
/* 896  */ 
/* 897  */                     inputElem.value         = smile;
/* 898  */                     inputElem.style.cssText = 'position:absolute;visibility:hidden;';
/* 899  */ 
/* 900  */                     if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {

/* foundation.min.js */

/* 901  */ 
/* 902  */                       docElement.appendChild(inputElem);
/* 903  */                       defaultView = document.defaultView;
/* 904  */ 
/* 905  */                       // Safari 2-4 allows the smiley as a value, despite making a slider
/* 906  */                       bool =  defaultView.getComputedStyle &&
/* 907  */                               defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
/* 908  */                               // Mobile android web browser has false positive, so must
/* 909  */                               // check the height to see if the widget is actually there.
/* 910  */                               (inputElem.offsetHeight !== 0);
/* 911  */ 
/* 912  */                       docElement.removeChild(inputElem);
/* 913  */ 
/* 914  */                     } else if ( /^(search|tel)$/.test(inputElemType) ){
/* 915  */                       // Spec doesn't define any special parsing or detectable UI
/* 916  */                       //   behaviors so we pass these through as true
/* 917  */ 
/* 918  */                       // Interestingly, opera fails the earlier test, so it doesn't
/* 919  */                       //  even make it here.
/* 920  */ 
/* 921  */                     } else if ( /^(url|email)$/.test(inputElemType) ) {
/* 922  */                       // Real url and email support comes with prebaked validation.
/* 923  */                       bool = inputElem.checkValidity && inputElem.checkValidity() === false;
/* 924  */ 
/* 925  */                     } else {
/* 926  */                       // If the upgraded input compontent rejects the :) text, we got a winner
/* 927  */                       bool = inputElem.value != smile;
/* 928  */                     }
/* 929  */                 }
/* 930  */ 
/* 931  */                 inputs[ props[i] ] = !!bool;
/* 932  */             }
/* 933  */             return inputs;
/* 934  */         })('search tel url email datetime date month week time datetime-local number range color'.split(' '));
/* 935  */         /*>>inputtypes*/
/* 936  */     }
/* 937  */     /*>>webforms*/
/* 938  */ 
/* 939  */ 
/* 940  */     // End of test definitions
/* 941  */     // -----------------------
/* 942  */ 
/* 943  */ 
/* 944  */ 
/* 945  */     // Run through all tests and detect their support in the current UA.
/* 946  */     // todo: hypothetically we could be doing an array of tests and use a basic loop here.
/* 947  */     for ( var feature in tests ) {
/* 948  */         if ( hasOwnProp(tests, feature) ) {
/* 949  */             // run the test, throw the return value into the Modernizr,
/* 950  */             //   then based on that boolean, define an appropriate className

/* foundation.min.js */

/* 951  */             //   and push it into an array of classes we'll join later.
/* 952  */             featureName  = feature.toLowerCase();
/* 953  */             Modernizr[featureName] = tests[feature]();
/* 954  */ 
/* 955  */             classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
/* 956  */         }
/* 957  */     }
/* 958  */ 
/* 959  */     /*>>webforms*/
/* 960  */     // input tests need to run.
/* 961  */     Modernizr.input || webforms();
/* 962  */     /*>>webforms*/
/* 963  */ 
/* 964  */ 
/* 965  */     /**
/* 966  *|      * addTest allows the user to define their own feature tests
/* 967  *|      * the result will be added onto the Modernizr object,
/* 968  *|      * as well as an appropriate className set on the html element
/* 969  *|      *
/* 970  *|      * @param feature - String naming the feature
/* 971  *|      * @param test - Function returning true if feature is supported, false if not
/* 972  *|      */
/* 973  */      Modernizr.addTest = function ( feature, test ) {
/* 974  */        if ( typeof feature == 'object' ) {
/* 975  */          for ( var key in feature ) {
/* 976  */            if ( hasOwnProp( feature, key ) ) {
/* 977  */              Modernizr.addTest( key, feature[ key ] );
/* 978  */            }
/* 979  */          }
/* 980  */        } else {
/* 981  */ 
/* 982  */          feature = feature.toLowerCase();
/* 983  */ 
/* 984  */          if ( Modernizr[feature] !== undefined ) {
/* 985  */            // we're going to quit if you're trying to overwrite an existing test
/* 986  */            // if we were to allow it, we'd do this:
/* 987  */            //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
/* 988  */            //   docElement.className = docElement.className.replace( re, '' );
/* 989  */            // but, no rly, stuff 'em.
/* 990  */            return Modernizr;
/* 991  */          }
/* 992  */ 
/* 993  */          test = typeof test == 'function' ? test() : test;
/* 994  */ 
/* 995  */          if (typeof enableClasses !== "undefined" && enableClasses) {
/* 996  */            docElement.className += ' ' + (test ? '' : 'no-') + feature;
/* 997  */          }
/* 998  */          Modernizr[feature] = test;
/* 999  */ 
/* 1000 */        }

/* foundation.min.js */

/* 1001 */ 
/* 1002 */        return Modernizr; // allow chaining.
/* 1003 */      };
/* 1004 */ 
/* 1005 */ 
/* 1006 */     // Reset modElem.cssText to nothing to reduce memory footprint.
/* 1007 */     setCss('');
/* 1008 */     modElem = inputElem = null;
/* 1009 */ 
/* 1010 */     /*>>shiv*/
/* 1011 */     /**
/* 1012 *|      * @preserve HTML5 Shiv prev3.7.1 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
/* 1013 *|      */
/* 1014 */     ;(function(window, document) {
/* 1015 */         /*jshint evil:true */
/* 1016 */         /** version */
/* 1017 */         var version = '3.7.0';
/* 1018 */ 
/* 1019 */         /** Preset options */
/* 1020 */         var options = window.html5 || {};
/* 1021 */ 
/* 1022 */         /** Used to skip problem elements */
/* 1023 */         var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;
/* 1024 */ 
/* 1025 */         /** Not all elements can be cloned in IE **/
/* 1026 */         var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;
/* 1027 */ 
/* 1028 */         /** Detect whether the browser supports default html5 styles */
/* 1029 */         var supportsHtml5Styles;
/* 1030 */ 
/* 1031 */         /** Name of the expando, to work with multiple documents or to re-shiv one document */
/* 1032 */         var expando = '_html5shiv';
/* 1033 */ 
/* 1034 */         /** The id for the the documents expando */
/* 1035 */         var expanID = 0;
/* 1036 */ 
/* 1037 */         /** Cached data for each document */
/* 1038 */         var expandoData = {};
/* 1039 */ 
/* 1040 */         /** Detect whether the browser supports unknown elements */
/* 1041 */         var supportsUnknownElements;
/* 1042 */ 
/* 1043 */         (function() {
/* 1044 */           try {
/* 1045 */             var a = document.createElement('a');
/* 1046 */             a.innerHTML = '<xyz></xyz>';
/* 1047 */             //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
/* 1048 */             supportsHtml5Styles = ('hidden' in a);
/* 1049 */ 
/* 1050 */             supportsUnknownElements = a.childNodes.length == 1 || (function() {

/* foundation.min.js */

/* 1051 */               // assign a false positive if unable to shiv
/* 1052 */               (document.createElement)('a');
/* 1053 */               var frag = document.createDocumentFragment();
/* 1054 */               return (
/* 1055 */                 typeof frag.cloneNode == 'undefined' ||
/* 1056 */                 typeof frag.createDocumentFragment == 'undefined' ||
/* 1057 */                 typeof frag.createElement == 'undefined'
/* 1058 */               );
/* 1059 */             }());
/* 1060 */           } catch(e) {
/* 1061 */             // assign a false positive if detection fails => unable to shiv
/* 1062 */             supportsHtml5Styles = true;
/* 1063 */             supportsUnknownElements = true;
/* 1064 */           }
/* 1065 */ 
/* 1066 */         }());
/* 1067 */ 
/* 1068 */         /*--------------------------------------------------------------------------*/
/* 1069 */ 
/* 1070 */         /**
/* 1071 *|          * Creates a style sheet with the given CSS text and adds it to the document.
/* 1072 *|          * @private
/* 1073 *|          * @param {Document} ownerDocument The document.
/* 1074 *|          * @param {String} cssText The CSS text.
/* 1075 *|          * @returns {StyleSheet} The style element.
/* 1076 *|          */
/* 1077 */         function addStyleSheet(ownerDocument, cssText) {
/* 1078 */           var p = ownerDocument.createElement('p'),
/* 1079 */           parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;
/* 1080 */ 
/* 1081 */           p.innerHTML = 'x<style>' + cssText + '</style>';
/* 1082 */           return parent.insertBefore(p.lastChild, parent.firstChild);
/* 1083 */         }
/* 1084 */ 
/* 1085 */         /**
/* 1086 *|          * Returns the value of `html5.elements` as an array.
/* 1087 *|          * @private
/* 1088 *|          * @returns {Array} An array of shived element node names.
/* 1089 *|          */
/* 1090 */         function getElements() {
/* 1091 */           var elements = html5.elements;
/* 1092 */           return typeof elements == 'string' ? elements.split(' ') : elements;
/* 1093 */         }
/* 1094 */ 
/* 1095 */         /**
/* 1096 *|          * Returns the data associated to the given document
/* 1097 *|          * @private
/* 1098 *|          * @param {Document} ownerDocument The document.
/* 1099 *|          * @returns {Object} An object of data.
/* 1100 *|          */

/* foundation.min.js */

/* 1101 */         function getExpandoData(ownerDocument) {
/* 1102 */           var data = expandoData[ownerDocument[expando]];
/* 1103 */           if (!data) {
/* 1104 */             data = {};
/* 1105 */             expanID++;
/* 1106 */             ownerDocument[expando] = expanID;
/* 1107 */             expandoData[expanID] = data;
/* 1108 */           }
/* 1109 */           return data;
/* 1110 */         }
/* 1111 */ 
/* 1112 */         /**
/* 1113 *|          * returns a shived element for the given nodeName and document
/* 1114 *|          * @memberOf html5
/* 1115 *|          * @param {String} nodeName name of the element
/* 1116 *|          * @param {Document} ownerDocument The context document.
/* 1117 *|          * @returns {Object} The shived element.
/* 1118 *|          */
/* 1119 */         function createElement(nodeName, ownerDocument, data){
/* 1120 */           if (!ownerDocument) {
/* 1121 */             ownerDocument = document;
/* 1122 */           }
/* 1123 */           if(supportsUnknownElements){
/* 1124 */             return ownerDocument.createElement(nodeName);
/* 1125 */           }
/* 1126 */           if (!data) {
/* 1127 */             data = getExpandoData(ownerDocument);
/* 1128 */           }
/* 1129 */           var node;
/* 1130 */ 
/* 1131 */           if (data.cache[nodeName]) {
/* 1132 */             node = data.cache[nodeName].cloneNode();
/* 1133 */           } else if (saveClones.test(nodeName)) {
/* 1134 */             node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
/* 1135 */           } else {
/* 1136 */             node = data.createElem(nodeName);
/* 1137 */           }
/* 1138 */ 
/* 1139 */           // Avoid adding some elements to fragments in IE < 9 because
/* 1140 */           // * Attributes like `name` or `type` cannot be set/changed once an element
/* 1141 */           //   is inserted into a document/fragment
/* 1142 */           // * Link elements with `src` attributes that are inaccessible, as with
/* 1143 */           //   a 403 response, will cause the tab/window to crash
/* 1144 */           // * Script elements appended to fragments will execute when their `src`
/* 1145 */           //   or `text` property is set
/* 1146 */           return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
/* 1147 */         }
/* 1148 */ 
/* 1149 */         /**
/* 1150 *|          * returns a shived DocumentFragment for the given document

/* foundation.min.js */

/* 1151 *|          * @memberOf html5
/* 1152 *|          * @param {Document} ownerDocument The context document.
/* 1153 *|          * @returns {Object} The shived DocumentFragment.
/* 1154 *|          */
/* 1155 */         function createDocumentFragment(ownerDocument, data){
/* 1156 */           if (!ownerDocument) {
/* 1157 */             ownerDocument = document;
/* 1158 */           }
/* 1159 */           if(supportsUnknownElements){
/* 1160 */             return ownerDocument.createDocumentFragment();
/* 1161 */           }
/* 1162 */           data = data || getExpandoData(ownerDocument);
/* 1163 */           var clone = data.frag.cloneNode(),
/* 1164 */           i = 0,
/* 1165 */           elems = getElements(),
/* 1166 */           l = elems.length;
/* 1167 */           for(;i<l;i++){
/* 1168 */             clone.createElement(elems[i]);
/* 1169 */           }
/* 1170 */           return clone;
/* 1171 */         }
/* 1172 */ 
/* 1173 */         /**
/* 1174 *|          * Shivs the `createElement` and `createDocumentFragment` methods of the document.
/* 1175 *|          * @private
/* 1176 *|          * @param {Document|DocumentFragment} ownerDocument The document.
/* 1177 *|          * @param {Object} data of the document.
/* 1178 *|          */
/* 1179 */         function shivMethods(ownerDocument, data) {
/* 1180 */           if (!data.cache) {
/* 1181 */             data.cache = {};
/* 1182 */             data.createElem = ownerDocument.createElement;
/* 1183 */             data.createFrag = ownerDocument.createDocumentFragment;
/* 1184 */             data.frag = data.createFrag();
/* 1185 */           }
/* 1186 */ 
/* 1187 */ 
/* 1188 */           ownerDocument.createElement = function(nodeName) {
/* 1189 */             //abort shiv
/* 1190 */             if (!html5.shivMethods) {
/* 1191 */               return data.createElem(nodeName);
/* 1192 */             }
/* 1193 */             return createElement(nodeName, ownerDocument, data);
/* 1194 */           };
/* 1195 */ 
/* 1196 */           ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
/* 1197 */                                                           'var n=f.cloneNode(),c=n.createElement;' +
/* 1198 */                                                           'h.shivMethods&&(' +
/* 1199 */                                                           // unroll the `createElement` calls
/* 1200 */                                                           getElements().join().replace(/[\w\-]+/g, function(nodeName) {

/* foundation.min.js */

/* 1201 */             data.createElem(nodeName);
/* 1202 */             data.frag.createElement(nodeName);
/* 1203 */             return 'c("' + nodeName + '")';
/* 1204 */           }) +
/* 1205 */             ');return n}'
/* 1206 */                                                          )(html5, data.frag);
/* 1207 */         }
/* 1208 */ 
/* 1209 */         /*--------------------------------------------------------------------------*/
/* 1210 */ 
/* 1211 */         /**
/* 1212 *|          * Shivs the given document.
/* 1213 *|          * @memberOf html5
/* 1214 *|          * @param {Document} ownerDocument The document to shiv.
/* 1215 *|          * @returns {Document} The shived document.
/* 1216 *|          */
/* 1217 */         function shivDocument(ownerDocument) {
/* 1218 */           if (!ownerDocument) {
/* 1219 */             ownerDocument = document;
/* 1220 */           }
/* 1221 */           var data = getExpandoData(ownerDocument);
/* 1222 */ 
/* 1223 */           if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
/* 1224 */             data.hasCSS = !!addStyleSheet(ownerDocument,
/* 1225 */                                           // corrects block display not defined in IE6/7/8/9
/* 1226 */                                           'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
/* 1227 */                                             // adds styling not present in IE6/7/8/9
/* 1228 */                                             'mark{background:#FF0;color:#000}' +
/* 1229 */                                             // hides non-rendered elements
/* 1230 */                                             'template{display:none}'
/* 1231 */                                          );
/* 1232 */           }
/* 1233 */           if (!supportsUnknownElements) {
/* 1234 */             shivMethods(ownerDocument, data);
/* 1235 */           }
/* 1236 */           return ownerDocument;
/* 1237 */         }
/* 1238 */ 
/* 1239 */         /*--------------------------------------------------------------------------*/
/* 1240 */ 
/* 1241 */         /**
/* 1242 *|          * The `html5` object is exposed so that more elements can be shived and
/* 1243 *|          * existing shiving can be detected on iframes.
/* 1244 *|          * @type Object
/* 1245 *|          * @example
/* 1246 *|          *
/* 1247 *|          * // options can be changed before the script is included
/* 1248 *|          * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
/* 1249 *|          */
/* 1250 */         var html5 = {

/* foundation.min.js */

/* 1251 */ 
/* 1252 */           /**
/* 1253 *|            * An array or space separated string of node names of the elements to shiv.
/* 1254 *|            * @memberOf html5
/* 1255 *|            * @type Array|String
/* 1256 *|            */
/* 1257 */           'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',
/* 1258 */ 
/* 1259 */           /**
/* 1260 *|            * current version of html5shiv
/* 1261 *|            */
/* 1262 */           'version': version,
/* 1263 */ 
/* 1264 */           /**
/* 1265 *|            * A flag to indicate that the HTML5 style sheet should be inserted.
/* 1266 *|            * @memberOf html5
/* 1267 *|            * @type Boolean
/* 1268 *|            */
/* 1269 */           'shivCSS': (options.shivCSS !== false),
/* 1270 */ 
/* 1271 */           /**
/* 1272 *|            * Is equal to true if a browser supports creating unknown/HTML5 elements
/* 1273 *|            * @memberOf html5
/* 1274 *|            * @type boolean
/* 1275 *|            */
/* 1276 */           'supportsUnknownElements': supportsUnknownElements,
/* 1277 */ 
/* 1278 */           /**
/* 1279 *|            * A flag to indicate that the document's `createElement` and `createDocumentFragment`
/* 1280 *|            * methods should be overwritten.
/* 1281 *|            * @memberOf html5
/* 1282 *|            * @type Boolean
/* 1283 *|            */
/* 1284 */           'shivMethods': (options.shivMethods !== false),
/* 1285 */ 
/* 1286 */           /**
/* 1287 *|            * A string to describe the type of `html5` object ("default" or "default print").
/* 1288 *|            * @memberOf html5
/* 1289 *|            * @type String
/* 1290 *|            */
/* 1291 */           'type': 'default',
/* 1292 */ 
/* 1293 */           // shivs the document according to the specified `html5` object options
/* 1294 */           'shivDocument': shivDocument,
/* 1295 */ 
/* 1296 */           //creates a shived element
/* 1297 */           createElement: createElement,
/* 1298 */ 
/* 1299 */           //creates a shived documentFragment
/* 1300 */           createDocumentFragment: createDocumentFragment

/* foundation.min.js */

/* 1301 */         };
/* 1302 */ 
/* 1303 */         /*--------------------------------------------------------------------------*/
/* 1304 */ 
/* 1305 */         // expose html5
/* 1306 */         window.html5 = html5;
/* 1307 */ 
/* 1308 */         // shiv the document
/* 1309 */         shivDocument(document);
/* 1310 */ 
/* 1311 */     }(this, document));
/* 1312 */     /*>>shiv*/
/* 1313 */ 
/* 1314 */     // Assign private properties to the return object with prefix
/* 1315 */     Modernizr._version      = version;
/* 1316 */ 
/* 1317 */     // expose these for the plugin API. Look in the source for how to join() them against your input
/* 1318 */     /*>>prefixes*/
/* 1319 */     Modernizr._prefixes     = prefixes;
/* 1320 */     /*>>prefixes*/
/* 1321 */     /*>>domprefixes*/
/* 1322 */     Modernizr._domPrefixes  = domPrefixes;
/* 1323 */     Modernizr._cssomPrefixes  = cssomPrefixes;
/* 1324 */     /*>>domprefixes*/
/* 1325 */ 
/* 1326 */     /*>>mq*/
/* 1327 */     // Modernizr.mq tests a given media query, live against the current state of the window
/* 1328 */     // A few important notes:
/* 1329 */     //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
/* 1330 */     //   * A max-width or orientation query will be evaluated against the current state, which may change later.
/* 1331 */     //   * You must specify values. Eg. If you are testing support for the min-width media query use:
/* 1332 */     //       Modernizr.mq('(min-width:0)')
/* 1333 */     // usage:
/* 1334 */     // Modernizr.mq('only screen and (max-width:768)')
/* 1335 */     Modernizr.mq            = testMediaQuery;
/* 1336 */     /*>>mq*/
/* 1337 */ 
/* 1338 */     /*>>hasevent*/
/* 1339 */     // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
/* 1340 */     // Modernizr.hasEvent('gesturestart', elem)
/* 1341 */     Modernizr.hasEvent      = isEventSupported;
/* 1342 */     /*>>hasevent*/
/* 1343 */ 
/* 1344 */     /*>>testprop*/
/* 1345 */     // Modernizr.testProp() investigates whether a given style property is recognized
/* 1346 */     // Note that the property names must be provided in the camelCase variant.
/* 1347 */     // Modernizr.testProp('pointerEvents')
/* 1348 */     Modernizr.testProp      = function(prop){
/* 1349 */         return testProps([prop]);
/* 1350 */     };

/* foundation.min.js */

/* 1351 */     /*>>testprop*/
/* 1352 */ 
/* 1353 */     /*>>testallprops*/
/* 1354 */     // Modernizr.testAllProps() investigates whether a given style property,
/* 1355 */     //   or any of its vendor-prefixed variants, is recognized
/* 1356 */     // Note that the property names must be provided in the camelCase variant.
/* 1357 */     // Modernizr.testAllProps('boxSizing')
/* 1358 */     Modernizr.testAllProps  = testPropsAll;
/* 1359 */     /*>>testallprops*/
/* 1360 */ 
/* 1361 */ 
/* 1362 */     /*>>teststyles*/
/* 1363 */     // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
/* 1364 */     // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
/* 1365 */     Modernizr.testStyles    = injectElementWithStyles;
/* 1366 */     /*>>teststyles*/
/* 1367 */ 
/* 1368 */ 
/* 1369 */     /*>>prefixed*/
/* 1370 */     // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
/* 1371 */     // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'
/* 1372 */ 
/* 1373 */     // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
/* 1374 */     // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
/* 1375 */     //
/* 1376 */     //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');
/* 1377 */ 
/* 1378 */     // If you're trying to ascertain which transition end event to bind to, you might do something like...
/* 1379 */     //
/* 1380 */     //     var transEndEventNames = {
/* 1381 */     //       'WebkitTransition' : 'webkitTransitionEnd',
/* 1382 */     //       'MozTransition'    : 'transitionend',
/* 1383 */     //       'OTransition'      : 'oTransitionEnd',
/* 1384 */     //       'msTransition'     : 'MSTransitionEnd',
/* 1385 */     //       'transition'       : 'transitionend'
/* 1386 */     //     },
/* 1387 */     //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];
/* 1388 */ 
/* 1389 */     Modernizr.prefixed      = function(prop, obj, elem){
/* 1390 */       if(!obj) {
/* 1391 */         return testPropsAll(prop, 'pfx');
/* 1392 */       } else {
/* 1393 */         // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
/* 1394 */         return testPropsAll(prop, obj, elem);
/* 1395 */       }
/* 1396 */     };
/* 1397 */     /*>>prefixed*/
/* 1398 */ 
/* 1399 */ 
/* 1400 */     /*>>cssclasses*/

/* foundation.min.js */

/* 1401 */     // Remove "no-js" class from <html> element, if it exists:
/* 1402 */     docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +
/* 1403 */ 
/* 1404 */                             // Add the new classes to the <html> element.
/* 1405 */                             (enableClasses ? ' js ' + classes.join(' ') : '');
/* 1406 */     /*>>cssclasses*/
/* 1407 */ 
/* 1408 */     return Modernizr;
/* 1409 */ 
/* 1410 */ })(this, this.document);
/* 1411 */ 
/* 1412 */ /*foundation script call */
/* 1413 */ jQuery(document).foundation();
