
/* markermanager.js */

/* 1    */ /**
/* 2    *|  * @name MarkerManager v3
/* 3    *|  * @version 1.0
/* 4    *|  * @copyright (c) 2007 Google Inc.
/* 5    *|  * @author Doug Ricket, Bjorn Brala (port to v3), others,
/* 6    *|  *
/* 7    *|  * @fileoverview Marker manager is an interface between the map and the user,
/* 8    *|  * designed to manage adding and removing many points when the viewport changes.
/* 9    *|  * <br /><br />
/* 10   *|  * <b>How it Works</b>:<br/> 
/* 11   *|  * The MarkerManager places its markers onto a grid, similar to the map tiles.
/* 12   *|  * When the user moves the viewport, it computes which grid cells have
/* 13   *|  * entered or left the viewport, and shows or hides all the markers in those
/* 14   *|  * cells.
/* 15   *|  * (If the users scrolls the viewport beyond the markers that are loaded,
/* 16   *|  * no markers will be visible until the <code>EVENT_moveend</code> 
/* 17   *|  * triggers an update.)
/* 18   *|  * In practical consequences, this allows 10,000 markers to be distributed over
/* 19   *|  * a large area, and as long as only 100-200 are visible in any given viewport,
/* 20   *|  * the user will see good performance corresponding to the 100 visible markers,
/* 21   *|  * rather than poor performance corresponding to the total 10,000 markers.
/* 22   *|  * Note that some code is optimized for speed over space,
/* 23   *|  * with the goal of accommodating thousands of markers.
/* 24   *|  */
/* 25   */ /*
/* 26   *|  * Licensed under the Apache License, Version 2.0 (the "License");
/* 27   *|  * you may not use this file except in compliance with the License.
/* 28   *|  * You may obtain a copy of the License at
/* 29   *|  *
/* 30   *|  *     http://www.apache.org/licenses/LICENSE-2.0
/* 31   *|  *
/* 32   *|  * Unless required by applicable law or agreed to in writing, software
/* 33   *|  * distributed under the License is distributed on an "AS IS" BASIS,
/* 34   *|  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/* 35   *|  * See the License for the specific language governing permissions and
/* 36   *|  * limitations under the License. 
/* 37   *|  */
/* 38   */ /**
/* 39   *|  * @name MarkerManagerOptions
/* 40   *|  * @class This class represents optional arguments to the {@link MarkerManager}
/* 41   *|  *     constructor.
/* 42   *|  * @property {Number} maxZoom Sets the maximum zoom level monitored by a
/* 43   *|  *     marker manager. If not given, the manager assumes the maximum map zoom
/* 44   *|  *     level. This value is also used when markers are added to the manager
/* 45   *|  *     without the optional {@link maxZoom} parameter.
/* 46   *|  * @property {Number} borderPadding Specifies, in pixels, the extra padding
/* 47   *|  *     outside the map's current viewport monitored by a manager. Markers that
/* 48   *|  *     fall within this padding are added to the map, even if they are not fully
/* 49   *|  *     visible.
/* 50   *|  * @property {Boolean} trackMarkers=false Indicates whether or not a marker

/* markermanager.js */

/* 51   *|  *     manager should track markers' movements. If you wish to move managed
/* 52   *|  *     markers using the {@link setPoint}/{@link setLatLng} methods, 
/* 53   *|  *     this option should be set to {@link true}.
/* 54   *|  */
/* 55   */ /**
/* 56   *|  * Creates a new MarkerManager that will show/hide markers on a map.
/* 57   *|  *
/* 58   *|  * Events:
/* 59   *|  * @event changed (Parameters: shown bounds, shown markers) Notify listeners when the state of what is displayed changes.
/* 60   *|  * @event loaded MarkerManager has succesfully been initialized.
/* 61   *|  *
/* 62   *|  * @constructor
/* 63   *|  * @param {Map} map The map to manage.
/* 64   *|  * @param {Object} opt_opts A container for optional arguments:
/* 65   *|  *   {Number} maxZoom The maximum zoom level for which to create tiles.
/* 66   *|  *   {Number} borderPadding The width in pixels beyond the map border,
/* 67   *|  *                   where markers should be display.
/* 68   *|  *   {Boolean} trackMarkers Whether or not this manager should track marker
/* 69   *|  *                   movements.
/* 70   *|  */
/* 71   */ function MarkerManager(map, opt_opts) {
/* 72   */   var me = this;
/* 73   */   me.map_ = map;
/* 74   */   me.mapZoom_ = map.getZoom();
/* 75   */   
/* 76   */   me.projectionHelper_ = new ProjectionHelperOverlay(map);
/* 77   */   google.maps.event.addListener(me.projectionHelper_, 'ready', function () {
/* 78   */     me.projection_ = this.getProjection();
/* 79   */     me.initialize(map, opt_opts);
/* 80   */   });
/* 81   */ }
/* 82   */   
/* 83   */ MarkerManager.prototype.initialize = function (map, opt_opts) {
/* 84   */   var me = this;
/* 85   */   
/* 86   */   opt_opts = opt_opts || {};
/* 87   */   me.tileSize_ = MarkerManager.DEFAULT_TILE_SIZE_;
/* 88   */   var mapTypes = map.mapTypes;
/* 89   */   // Find max zoom level
/* 90   */   var mapMaxZoom = 1;
/* 91   */   for (var sType in mapTypes ) {
/* 92   */     if (typeof map.mapTypes.get(sType) === 'object' && typeof map.mapTypes.get(sType).maxZoom === 'number') {
/* 93   */       var mapTypeMaxZoom = map.mapTypes.get(sType).maxZoom;
/* 94   */       if (mapTypeMaxZoom > mapMaxZoom) {
/* 95   */         mapMaxZoom = mapTypeMaxZoom;
/* 96   */       }
/* 97   */     }
/* 98   */   }
/* 99   */   
/* 100  */   me.maxZoom_  = opt_opts.maxZoom || 19;

/* markermanager.js */

/* 101  */   me.trackMarkers_ = opt_opts.trackMarkers;
/* 102  */   me.show_ = opt_opts.show || true;
/* 103  */   var padding;
/* 104  */   if (typeof opt_opts.borderPadding === 'number') {
/* 105  */     padding = opt_opts.borderPadding;
/* 106  */   } else {
/* 107  */     padding = MarkerManager.DEFAULT_BORDER_PADDING_;
/* 108  */   }
/* 109  */   // The padding in pixels beyond the viewport, where we will pre-load markers.
/* 110  */   me.swPadding_ = new google.maps.Size(-padding, padding);
/* 111  */   me.nePadding_ = new google.maps.Size(padding, -padding);
/* 112  */   me.borderPadding_ = padding;
/* 113  */   me.gridWidth_ = {};
/* 114  */   me.grid_ = {};
/* 115  */   me.grid_[me.maxZoom_] = {};
/* 116  */   me.numMarkers_ = {};
/* 117  */   me.numMarkers_[me.maxZoom_] = 0;
/* 118  */   google.maps.event.addListener(map, 'dragend', function () {
/* 119  */     me.onMapMoveEnd_();
/* 120  */   });
/* 121  */   google.maps.event.addListener(map, 'zoom_changed', function () {
/* 122  */     me.onMapMoveEnd_();
/* 123  */   });
/* 124  */   /**
/* 125  *|    * This closure provide easy access to the map.
/* 126  *|    * They are used as callbacks, not as methods.
/* 127  *|    * @param GMarker marker Marker to be removed from the map
/* 128  *|    * @private
/* 129  *|    */
/* 130  */   me.removeOverlay_ = function (marker) {
/* 131  */     marker.setMap(null);
/* 132  */     me.shownMarkers_--;
/* 133  */   };
/* 134  */   /**
/* 135  *|    * This closure provide easy access to the map.
/* 136  *|    * They are used as callbacks, not as methods.
/* 137  *|    * @param GMarker marker Marker to be added to the map
/* 138  *|    * @private
/* 139  *|    */
/* 140  */   me.addOverlay_ = function (marker) {
/* 141  */     if (me.show_) {
/* 142  */       marker.setMap(me.map_);
/* 143  */       me.shownMarkers_++;
/* 144  */     }
/* 145  */   };
/* 146  */   me.resetManager_();
/* 147  */   me.shownMarkers_ = 0;
/* 148  */   me.shownBounds_ = me.getMapGridBounds_();
/* 149  */   
/* 150  */   google.maps.event.trigger(me, 'loaded');

/* markermanager.js */

/* 151  */   
/* 152  */ };
/* 153  */ /**
/* 154  *|  *  Default tile size used for deviding the map into a grid.
/* 155  *|  */
/* 156  */ MarkerManager.DEFAULT_TILE_SIZE_ = 1024;
/* 157  */ /*
/* 158  *|  *  How much extra space to show around the map border so
/* 159  *|  *  dragging doesn't result in an empty place.
/* 160  *|  */
/* 161  */ MarkerManager.DEFAULT_BORDER_PADDING_ = 100;
/* 162  */ /**
/* 163  *|  *  Default tilesize of single tile world.
/* 164  *|  */
/* 165  */ MarkerManager.MERCATOR_ZOOM_LEVEL_ZERO_RANGE = 256;
/* 166  */ /**
/* 167  *|  * Initializes MarkerManager arrays for all zoom levels
/* 168  *|  * Called by constructor and by clearAllMarkers
/* 169  *|  */
/* 170  */ MarkerManager.prototype.resetManager_ = function () {
/* 171  */   var mapWidth = MarkerManager.MERCATOR_ZOOM_LEVEL_ZERO_RANGE;
/* 172  */   for (var zoom = 0; zoom <= this.maxZoom_; ++zoom) {
/* 173  */     this.grid_[zoom] = {};
/* 174  */     this.numMarkers_[zoom] = 0;
/* 175  */     this.gridWidth_[zoom] = Math.ceil(mapWidth / this.tileSize_);
/* 176  */     mapWidth <<= 1;
/* 177  */   }
/* 178  */ };
/* 179  */ /**
/* 180  *|  * Removes all markers in the manager, and
/* 181  *|  * removes any visible markers from the map.
/* 182  *|  */
/* 183  */ MarkerManager.prototype.clearMarkers = function () {
/* 184  */   this.processAll_(this.shownBounds_, this.removeOverlay_);
/* 185  */   this.resetManager_();
/* 186  */ };
/* 187  */ /**
/* 188  *|  * Gets the tile coordinate for a given latlng point.
/* 189  *|  *
/* 190  *|  * @param {LatLng} latlng The geographical point.
/* 191  *|  * @param {Number} zoom The zoom level.
/* 192  *|  * @param {google.maps.Size} padding The padding used to shift the pixel coordinate.
/* 193  *|  *               Used for expanding a bounds to include an extra padding
/* 194  *|  *               of pixels surrounding the bounds.
/* 195  *|  * @return {GPoint} The point in tile coordinates.
/* 196  *|  *
/* 197  *|  */
/* 198  */ MarkerManager.prototype.getTilePoint_ = function (latlng, zoom, padding) {
/* 199  */   var pixelPoint = this.projectionHelper_.LatLngToPixel(latlng, zoom);
/* 200  */   var point = new google.maps.Point(

/* markermanager.js */

/* 201  */     Math.floor((pixelPoint.x + padding.width) / this.tileSize_),
/* 202  */     Math.floor((pixelPoint.y + padding.height) / this.tileSize_)
/* 203  */   );
/* 204  */   return point;
/* 205  */ };
/* 206  */ /**
/* 207  *|  * Finds the appropriate place to add the marker to the grid.
/* 208  *|  * Optimized for speed; does not actually add the marker to the map.
/* 209  *|  * Designed for batch-processing thousands of markers.
/* 210  *|  *
/* 211  *|  * @param {Marker} marker The marker to add.
/* 212  *|  * @param {Number} minZoom The minimum zoom for displaying the marker.
/* 213  *|  * @param {Number} maxZoom The maximum zoom for displaying the marker.
/* 214  *|  */
/* 215  */ MarkerManager.prototype.addMarkerBatch_ = function (marker, minZoom, maxZoom) {
/* 216  */   var me = this;
/* 217  */   var mPoint = marker.getPosition();
/* 218  */   marker.MarkerManager_minZoom = minZoom;
/* 219  */   
/* 220  */   
/* 221  */   // Tracking markers is expensive, so we do this only if the
/* 222  */   // user explicitly requested it when creating marker manager.
/* 223  */   if (this.trackMarkers_) {
/* 224  */     google.maps.event.addListener(marker, 'changed', function (a, b, c) {
/* 225  */       me.onMarkerMoved_(a, b, c);
/* 226  */     });
/* 227  */   }
/* 228  */   var gridPoint = this.getTilePoint_(mPoint, maxZoom, new google.maps.Size(0, 0, 0, 0));
/* 229  */   for (var zoom = maxZoom; zoom >= minZoom; zoom--) {
/* 230  */     var cell = this.getGridCellCreate_(gridPoint.x, gridPoint.y, zoom);
/* 231  */     cell.push(marker);
/* 232  */     gridPoint.x = gridPoint.x >> 1;
/* 233  */     gridPoint.y = gridPoint.y >> 1;
/* 234  */   }
/* 235  */ };
/* 236  */ /**
/* 237  *|  * Returns whether or not the given point is visible in the shown bounds. This
/* 238  *|  * is a helper method that takes care of the corner case, when shownBounds have
/* 239  *|  * negative minX value.
/* 240  *|  *
/* 241  *|  * @param {Point} point a point on a grid.
/* 242  *|  * @return {Boolean} Whether or not the given point is visible in the currently
/* 243  *|  * shown bounds.
/* 244  *|  */
/* 245  */ MarkerManager.prototype.isGridPointVisible_ = function (point) {
/* 246  */   var vertical = this.shownBounds_.minY <= point.y &&
/* 247  */       point.y <= this.shownBounds_.maxY;
/* 248  */   var minX = this.shownBounds_.minX;
/* 249  */   var horizontal = minX <= point.x && point.x <= this.shownBounds_.maxX;
/* 250  */   if (!horizontal && minX < 0) {

/* markermanager.js */

/* 251  */     // Shifts the negative part of the rectangle. As point.x is always less
/* 252  */     // than grid width, only test shifted minX .. 0 part of the shown bounds.
/* 253  */     var width = this.gridWidth_[this.shownBounds_.z];
/* 254  */     horizontal = minX + width <= point.x && point.x <= width - 1;
/* 255  */   }
/* 256  */   return vertical && horizontal;
/* 257  */ };
/* 258  */ /**
/* 259  *|  * Reacts to a notification from a marker that it has moved to a new location.
/* 260  *|  * It scans the grid all all zoom levels and moves the marker from the old grid
/* 261  *|  * location to a new grid location.
/* 262  *|  *
/* 263  *|  * @param {Marker} marker The marker that moved.
/* 264  *|  * @param {LatLng} oldPoint The old position of the marker.
/* 265  *|  * @param {LatLng} newPoint The new position of the marker.
/* 266  *|  */
/* 267  */ MarkerManager.prototype.onMarkerMoved_ = function (marker, oldPoint, newPoint) {
/* 268  */   // NOTE: We do not know the minimum or maximum zoom the marker was
/* 269  */   // added at, so we start at the absolute maximum. Whenever we successfully
/* 270  */   // remove a marker at a given zoom, we add it at the new grid coordinates.
/* 271  */   var zoom = this.maxZoom_;
/* 272  */   var changed = false;
/* 273  */   var oldGrid = this.getTilePoint_(oldPoint, zoom, new google.maps.Size(0, 0, 0, 0));
/* 274  */   var newGrid = this.getTilePoint_(newPoint, zoom, new google.maps.Size(0, 0, 0, 0));
/* 275  */   while (zoom >= 0 && (oldGrid.x !== newGrid.x || oldGrid.y !== newGrid.y)) {
/* 276  */     var cell = this.getGridCellNoCreate_(oldGrid.x, oldGrid.y, zoom);
/* 277  */     if (cell) {
/* 278  */       if (this.removeFromArray_(cell, marker)) {
/* 279  */         this.getGridCellCreate_(newGrid.x, newGrid.y, zoom).push(marker);
/* 280  */       }
/* 281  */     }
/* 282  */     // For the current zoom we also need to update the map. Markers that no
/* 283  */     // longer are visible are removed from the map. Markers that moved into
/* 284  */     // the shown bounds are added to the map. This also lets us keep the count
/* 285  */     // of visible markers up to date.
/* 286  */     if (zoom === this.mapZoom_) {
/* 287  */       if (this.isGridPointVisible_(oldGrid)) {
/* 288  */         if (!this.isGridPointVisible_(newGrid)) {
/* 289  */           this.removeOverlay_(marker);
/* 290  */           changed = true;
/* 291  */         }
/* 292  */       } else {
/* 293  */         if (this.isGridPointVisible_(newGrid)) {
/* 294  */           this.addOverlay_(marker);
/* 295  */           changed = true;
/* 296  */         }
/* 297  */       }
/* 298  */     }
/* 299  */     oldGrid.x = oldGrid.x >> 1;
/* 300  */     oldGrid.y = oldGrid.y >> 1;

/* markermanager.js */

/* 301  */     newGrid.x = newGrid.x >> 1;
/* 302  */     newGrid.y = newGrid.y >> 1;
/* 303  */     --zoom;
/* 304  */   }
/* 305  */   if (changed) {
/* 306  */     this.notifyListeners_();
/* 307  */   }
/* 308  */ };
/* 309  */ /**
/* 310  *|  * Removes marker from the manager and from the map
/* 311  *|  * (if it's currently visible).
/* 312  *|  * @param {GMarker} marker The marker to delete.
/* 313  *|  */
/* 314  */ MarkerManager.prototype.removeMarker = function (marker) {
/* 315  */   var zoom = this.maxZoom_;
/* 316  */   var changed = false;
/* 317  */   var point = marker.getPosition();
/* 318  */   var grid = this.getTilePoint_(point, zoom, new google.maps.Size(0, 0, 0, 0));
/* 319  */   while (zoom >= 0) {
/* 320  */     var cell = this.getGridCellNoCreate_(grid.x, grid.y, zoom);
/* 321  */     if (cell) {
/* 322  */       this.removeFromArray_(cell, marker);
/* 323  */     }
/* 324  */     // For the current zoom we also need to update the map. Markers that no
/* 325  */     // longer are visible are removed from the map. This also lets us keep the count
/* 326  */     // of visible markers up to date.
/* 327  */     if (zoom === this.mapZoom_) {
/* 328  */       if (this.isGridPointVisible_(grid)) {
/* 329  */         this.removeOverlay_(marker);
/* 330  */         changed = true;
/* 331  */       }
/* 332  */     }
/* 333  */     grid.x = grid.x >> 1;
/* 334  */     grid.y = grid.y >> 1;
/* 335  */     --zoom;
/* 336  */   }
/* 337  */   if (changed) {
/* 338  */     this.notifyListeners_();
/* 339  */   }
/* 340  */   this.numMarkers_[marker.MarkerManager_minZoom]--;
/* 341  */ };
/* 342  */ /**
/* 343  *|  * Add many markers at once.
/* 344  *|  * Does not actually update the map, just the internal grid.
/* 345  *|  *
/* 346  *|  * @param {Array of Marker} markers The markers to add.
/* 347  *|  * @param {Number} minZoom The minimum zoom level to display the markers.
/* 348  *|  * @param {Number} opt_maxZoom The maximum zoom level to display the markers.
/* 349  *|  */
/* 350  */ MarkerManager.prototype.addMarkers = function (markers, minZoom, opt_maxZoom) {

/* markermanager.js */

/* 351  */   var maxZoom = this.getOptMaxZoom_(opt_maxZoom);
/* 352  */   for (var i = markers.length - 1; i >= 0; i--) {
/* 353  */     this.addMarkerBatch_(markers[i], minZoom, maxZoom);
/* 354  */   }
/* 355  */   this.numMarkers_[minZoom] += markers.length;
/* 356  */ };
/* 357  */ /**
/* 358  *|  * Returns the value of the optional maximum zoom. This method is defined so
/* 359  *|  * that we have just one place where optional maximum zoom is calculated.
/* 360  *|  *
/* 361  *|  * @param {Number} opt_maxZoom The optinal maximum zoom.
/* 362  *|  * @return The maximum zoom.
/* 363  *|  */
/* 364  */ MarkerManager.prototype.getOptMaxZoom_ = function (opt_maxZoom) {
/* 365  */   return opt_maxZoom || this.maxZoom_;
/* 366  */ };
/* 367  */ /**
/* 368  *|  * Calculates the total number of markers potentially visible at a given
/* 369  *|  * zoom level.
/* 370  *|  *
/* 371  *|  * @param {Number} zoom The zoom level to check.
/* 372  *|  */
/* 373  */ MarkerManager.prototype.getMarkerCount = function (zoom) {
/* 374  */   var total = 0;
/* 375  */   for (var z = 0; z <= zoom; z++) {
/* 376  */     total += this.numMarkers_[z];
/* 377  */   }
/* 378  */   return total;
/* 379  */ };
/* 380  */ /** 
/* 381  *|  * Returns a marker given latitude, longitude and zoom. If the marker does not 
/* 382  *|  * exist, the method will return a new marker. If a new marker is created, 
/* 383  *|  * it will NOT be added to the manager. 
/* 384  *|  * 
/* 385  *|  * @param {Number} lat - the latitude of a marker. 
/* 386  *|  * @param {Number} lng - the longitude of a marker. 
/* 387  *|  * @param {Number} zoom - the zoom level 
/* 388  *|  * @return {GMarker} marker - the marker found at lat and lng 
/* 389  *|  */ 
/* 390  */ MarkerManager.prototype.getMarker = function (lat, lng, zoom) {
/* 391  */   var mPoint = new google.maps.LatLng(lat, lng); 
/* 392  */   var gridPoint = this.getTilePoint_(mPoint, zoom, new google.maps.Size(0, 0, 0, 0));
/* 393  */   var marker = new google.maps.Marker({position: mPoint}); 
/* 394  */     
/* 395  */   var cellArray = this.getGridCellNoCreate_(gridPoint.x, gridPoint.y, zoom);
/* 396  */   if (cellArray !== undefined) {
/* 397  */     for (var i = 0; i < cellArray.length; i++) 
/* 398  */     { 
/* 399  */       if (lat === cellArray[i].getLatLng().lat() && lng === cellArray[i].getLatLng().lng()) {
/* 400  */         marker = cellArray[i]; 

/* markermanager.js */

/* 401  */       } 
/* 402  */     } 
/* 403  */   } 
/* 404  */   return marker; 
/* 405  */ }; 
/* 406  */ /**
/* 407  *|  * Add a single marker to the map.
/* 408  *|  *
/* 409  *|  * @param {Marker} marker The marker to add.
/* 410  *|  * @param {Number} minZoom The minimum zoom level to display the marker.
/* 411  *|  * @param {Number} opt_maxZoom The maximum zoom level to display the marker.
/* 412  *|  */
/* 413  */ MarkerManager.prototype.addMarker = function (marker, minZoom, opt_maxZoom) {
/* 414  */   var maxZoom = this.getOptMaxZoom_(opt_maxZoom);
/* 415  */   this.addMarkerBatch_(marker, minZoom, maxZoom);
/* 416  */   var gridPoint = this.getTilePoint_(marker.getPosition(), this.mapZoom_, new google.maps.Size(0, 0, 0, 0));
/* 417  */   if (this.isGridPointVisible_(gridPoint) &&
/* 418  */       minZoom <= this.shownBounds_.z &&
/* 419  */       this.shownBounds_.z <= maxZoom) {
/* 420  */     this.addOverlay_(marker);
/* 421  */     this.notifyListeners_();
/* 422  */   }
/* 423  */   this.numMarkers_[minZoom]++;
/* 424  */ };
/* 425  */ /**
/* 426  *|  * Helper class to create a bounds of INT ranges.
/* 427  *|  * @param bounds Array.<Object.<string, number>> Bounds object.
/* 428  *|  * @constructor
/* 429  *|  */
/* 430  */ function GridBounds(bounds) {
/* 431  */   // [sw, ne]
/* 432  */   
/* 433  */   this.minX = Math.min(bounds[0].x, bounds[1].x);
/* 434  */   this.maxX = Math.max(bounds[0].x, bounds[1].x);
/* 435  */   this.minY = Math.min(bounds[0].y, bounds[1].y);
/* 436  */   this.maxY = Math.max(bounds[0].y, bounds[1].y);
/* 437  */       
/* 438  */ }
/* 439  */ /**
/* 440  *|  * Returns true if this bounds equal the given bounds.
/* 441  *|  * @param {GridBounds} gridBounds GridBounds The bounds to test.
/* 442  *|  * @return {Boolean} This Bounds equals the given GridBounds.
/* 443  *|  */
/* 444  */ GridBounds.prototype.equals = function (gridBounds) {
/* 445  */   if (this.maxX === gridBounds.maxX && this.maxY === gridBounds.maxY && this.minX === gridBounds.minX && this.minY === gridBounds.minY) {
/* 446  */     return true;
/* 447  */   } else {
/* 448  */     return false;
/* 449  */   }  
/* 450  */ };

/* markermanager.js */

/* 451  */ /**
/* 452  *|  * Returns true if this bounds (inclusively) contains the given point.
/* 453  *|  * @param {Point} point  The point to test.
/* 454  *|  * @return {Boolean} This Bounds contains the given Point.
/* 455  *|  */
/* 456  */ GridBounds.prototype.containsPoint = function (point) {
/* 457  */   var outer = this;
/* 458  */   return (outer.minX <= point.x && outer.maxX >= point.x && outer.minY <= point.y && outer.maxY >= point.y);
/* 459  */ };
/* 460  */ /**
/* 461  *|  * Get a cell in the grid, creating it first if necessary.
/* 462  *|  *
/* 463  *|  * Optimization candidate
/* 464  *|  *
/* 465  *|  * @param {Number} x The x coordinate of the cell.
/* 466  *|  * @param {Number} y The y coordinate of the cell.
/* 467  *|  * @param {Number} z The z coordinate of the cell.
/* 468  *|  * @return {Array} The cell in the array.
/* 469  *|  */
/* 470  */ MarkerManager.prototype.getGridCellCreate_ = function (x, y, z) {
/* 471  */   var grid = this.grid_[z];
/* 472  */   if (x < 0) {
/* 473  */     x += this.gridWidth_[z];
/* 474  */   }
/* 475  */   var gridCol = grid[x];
/* 476  */   if (!gridCol) {
/* 477  */     gridCol = grid[x] = [];
/* 478  */     return (gridCol[y] = []);
/* 479  */   }
/* 480  */   var gridCell = gridCol[y];
/* 481  */   if (!gridCell) {
/* 482  */     return (gridCol[y] = []);
/* 483  */   }
/* 484  */   return gridCell;
/* 485  */ };
/* 486  */ /**
/* 487  *|  * Get a cell in the grid, returning undefined if it does not exist.
/* 488  *|  *
/* 489  *|  * NOTE: Optimized for speed -- otherwise could combine with getGridCellCreate_.
/* 490  *|  *
/* 491  *|  * @param {Number} x The x coordinate of the cell.
/* 492  *|  * @param {Number} y The y coordinate of the cell.
/* 493  *|  * @param {Number} z The z coordinate of the cell.
/* 494  *|  * @return {Array} The cell in the array.
/* 495  *|  */
/* 496  */ MarkerManager.prototype.getGridCellNoCreate_ = function (x, y, z) {
/* 497  */   var grid = this.grid_[z];
/* 498  */   
/* 499  */   if (x < 0) {
/* 500  */     x += this.gridWidth_[z];

/* markermanager.js */

/* 501  */   }
/* 502  */   var gridCol = grid[x];
/* 503  */   return gridCol ? gridCol[y] : undefined;
/* 504  */ };
/* 505  */ /**
/* 506  *|  * Turns at geographical bounds into a grid-space bounds.
/* 507  *|  *
/* 508  *|  * @param {LatLngBounds} bounds The geographical bounds.
/* 509  *|  * @param {Number} zoom The zoom level of the bounds.
/* 510  *|  * @param {google.maps.Size} swPadding The padding in pixels to extend beyond the
/* 511  *|  * given bounds.
/* 512  *|  * @param {google.maps.Size} nePadding The padding in pixels to extend beyond the
/* 513  *|  * given bounds.
/* 514  *|  * @return {GridBounds} The bounds in grid space.
/* 515  *|  */
/* 516  */ MarkerManager.prototype.getGridBounds_ = function (bounds, zoom, swPadding, nePadding) {
/* 517  */   zoom = Math.min(zoom, this.maxZoom_);
/* 518  */   var bl = bounds.getSouthWest();
/* 519  */   var tr = bounds.getNorthEast();
/* 520  */   var sw = this.getTilePoint_(bl, zoom, swPadding);
/* 521  */   var ne = this.getTilePoint_(tr, zoom, nePadding);
/* 522  */   var gw = this.gridWidth_[zoom];
/* 523  */   // Crossing the prime meridian requires correction of bounds.
/* 524  */   if (tr.lng() < bl.lng() || ne.x < sw.x) {
/* 525  */     sw.x -= gw;
/* 526  */   }
/* 527  */   if (ne.x - sw.x  + 1 >= gw) {
/* 528  */     // Computed grid bounds are larger than the world; truncate.
/* 529  */     sw.x = 0;
/* 530  */     ne.x = gw - 1;
/* 531  */   }
/* 532  */   var gridBounds = new GridBounds([sw, ne]);
/* 533  */   gridBounds.z = zoom;
/* 534  */   return gridBounds;
/* 535  */ };
/* 536  */ /**
/* 537  *|  * Gets the grid-space bounds for the current map viewport.
/* 538  *|  *
/* 539  *|  * @return {Bounds} The bounds in grid space.
/* 540  *|  */
/* 541  */ MarkerManager.prototype.getMapGridBounds_ = function () {
/* 542  */   return this.getGridBounds_(this.map_.getBounds(), this.mapZoom_, this.swPadding_, this.nePadding_);
/* 543  */ };
/* 544  */ /**
/* 545  *|  * Event listener for map:movend.
/* 546  *|  * NOTE: Use a timeout so that the user is not blocked
/* 547  *|  * from moving the map.
/* 548  *|  *
/* 549  *|  * Removed this because a a lack of a scopy override/callback function on events. 
/* 550  *|  */

/* markermanager.js */

/* 551  */ MarkerManager.prototype.onMapMoveEnd_ = function () {
/* 552  */   this.objectSetTimeout_(this, this.updateMarkers_, 0);
/* 553  */ };
/* 554  */ /**
/* 555  *|  * Call a function or evaluate an expression after a specified number of
/* 556  *|  * milliseconds.
/* 557  *|  *
/* 558  *|  * Equivalent to the standard window.setTimeout function, but the given
/* 559  *|  * function executes as a method of this instance. So the function passed to
/* 560  *|  * objectSetTimeout can contain references to this.
/* 561  *|  *    objectSetTimeout(this, function () { alert(this.x) }, 1000);
/* 562  *|  *
/* 563  *|  * @param {Object} object  The target object.
/* 564  *|  * @param {Function} command  The command to run.
/* 565  *|  * @param {Number} milliseconds  The delay.
/* 566  *|  * @return {Boolean}  Success.
/* 567  *|  */
/* 568  */ MarkerManager.prototype.objectSetTimeout_ = function (object, command, milliseconds) {
/* 569  */   return window.setTimeout(function () {
/* 570  */     command.call(object);
/* 571  */   }, milliseconds);
/* 572  */ };
/* 573  */ /**
/* 574  *|  * Is this layer visible?
/* 575  *|  *
/* 576  *|  * Returns visibility setting
/* 577  *|  *
/* 578  *|  * @return {Boolean} Visible
/* 579  *|  */
/* 580  */ MarkerManager.prototype.visible = function () {
/* 581  */   return this.show_ ? true : false;
/* 582  */ };
/* 583  */ /**
/* 584  *|  * Returns true if the manager is hidden.
/* 585  *|  * Otherwise returns false.
/* 586  *|  * @return {Boolean} Hidden
/* 587  *|  */
/* 588  */ MarkerManager.prototype.isHidden = function () {
/* 589  */   return !this.show_;
/* 590  */ };
/* 591  */ /**
/* 592  *|  * Shows the manager if it's currently hidden.
/* 593  *|  */
/* 594  */ MarkerManager.prototype.show = function () {
/* 595  */   this.show_ = true;
/* 596  */   this.refresh();
/* 597  */ };
/* 598  */ /**
/* 599  *|  * Hides the manager if it's currently visible
/* 600  *|  */

/* markermanager.js */

/* 601  */ MarkerManager.prototype.hide = function () {
/* 602  */   this.show_ = false;
/* 603  */   this.refresh();
/* 604  */ };
/* 605  */ /**
/* 606  *|  * Toggles the visibility of the manager.
/* 607  *|  */
/* 608  */ MarkerManager.prototype.toggle = function () {
/* 609  */   this.show_ = !this.show_;
/* 610  */   this.refresh();
/* 611  */ };
/* 612  */ /**
/* 613  *|  * Refresh forces the marker-manager into a good state.
/* 614  *|  * <ol>
/* 615  *|  *   <li>If never before initialized, shows all the markers.</li>
/* 616  *|  *   <li>If previously initialized, removes and re-adds all markers.</li>
/* 617  *|  * </ol>
/* 618  *|  */
/* 619  */ MarkerManager.prototype.refresh = function () {
/* 620  */   if (this.shownMarkers_ > 0) {
/* 621  */     this.processAll_(this.shownBounds_, this.removeOverlay_);
/* 622  */   }
/* 623  */   // An extra check on this.show_ to increase performance (no need to processAll_)
/* 624  */   if (this.show_) {
/* 625  */     this.processAll_(this.shownBounds_, this.addOverlay_);
/* 626  */   }
/* 627  */   this.notifyListeners_();
/* 628  */ };
/* 629  */ /**
/* 630  *|  * After the viewport may have changed, add or remove markers as needed.
/* 631  *|  */
/* 632  */ MarkerManager.prototype.updateMarkers_ = function () {
/* 633  */   this.mapZoom_ = this.map_.getZoom();
/* 634  */   var newBounds = this.getMapGridBounds_();
/* 635  */     
/* 636  */   // If the move does not include new grid sections,
/* 637  */   // we have no work to do:
/* 638  */   if (newBounds.equals(this.shownBounds_) && newBounds.z === this.shownBounds_.z) {
/* 639  */     return;
/* 640  */   }
/* 641  */   if (newBounds.z !== this.shownBounds_.z) {
/* 642  */     this.processAll_(this.shownBounds_, this.removeOverlay_);
/* 643  */     if (this.show_) { // performance
/* 644  */       this.processAll_(newBounds, this.addOverlay_);
/* 645  */     }
/* 646  */   } else {
/* 647  */     // Remove markers:
/* 648  */     this.rectangleDiff_(this.shownBounds_, newBounds, this.removeCellMarkers_);
/* 649  */     // Add markers:
/* 650  */     if (this.show_) { // performance

/* markermanager.js */

/* 651  */       this.rectangleDiff_(newBounds, this.shownBounds_, this.addCellMarkers_);
/* 652  */     }
/* 653  */   }
/* 654  */   this.shownBounds_ = newBounds;
/* 655  */   this.notifyListeners_();
/* 656  */ };
/* 657  */ /**
/* 658  *|  * Notify listeners when the state of what is displayed changes.
/* 659  *|  */
/* 660  */ MarkerManager.prototype.notifyListeners_ = function () {
/* 661  */   google.maps.event.trigger(this, 'changed', this.shownBounds_, this.shownMarkers_);
/* 662  */ };
/* 663  */ /**
/* 664  *|  * Process all markers in the bounds provided, using a callback.
/* 665  *|  *
/* 666  *|  * @param {Bounds} bounds The bounds in grid space.
/* 667  *|  * @param {Function} callback The function to call for each marker.
/* 668  *|  */
/* 669  */ MarkerManager.prototype.processAll_ = function (bounds, callback) {
/* 670  */   for (var x = bounds.minX; x <= bounds.maxX; x++) {
/* 671  */     for (var y = bounds.minY; y <= bounds.maxY; y++) {
/* 672  */       this.processCellMarkers_(x, y,  bounds.z, callback);
/* 673  */     }
/* 674  */   }
/* 675  */ };
/* 676  */ /**
/* 677  *|  * Process all markers in the grid cell, using a callback.
/* 678  *|  *
/* 679  *|  * @param {Number} x The x coordinate of the cell.
/* 680  *|  * @param {Number} y The y coordinate of the cell.
/* 681  *|  * @param {Number} z The z coordinate of the cell.
/* 682  *|  * @param {Function} callback The function to call for each marker.
/* 683  *|  */
/* 684  */ MarkerManager.prototype.processCellMarkers_ = function (x, y, z, callback) {
/* 685  */   var cell = this.getGridCellNoCreate_(x, y, z);
/* 686  */   if (cell) {
/* 687  */     for (var i = cell.length - 1; i >= 0; i--) {
/* 688  */       callback(cell[i]);
/* 689  */     }
/* 690  */   }
/* 691  */ };
/* 692  */ /**
/* 693  *|  * Remove all markers in a grid cell.
/* 694  *|  *
/* 695  *|  * @param {Number} x The x coordinate of the cell.
/* 696  *|  * @param {Number} y The y coordinate of the cell.
/* 697  *|  * @param {Number} z The z coordinate of the cell.
/* 698  *|  */
/* 699  */ MarkerManager.prototype.removeCellMarkers_ = function (x, y, z) {
/* 700  */   this.processCellMarkers_(x, y, z, this.removeOverlay_);

/* markermanager.js */

/* 701  */ };
/* 702  */ /**
/* 703  *|  * Add all markers in a grid cell.
/* 704  *|  *
/* 705  *|  * @param {Number} x The x coordinate of the cell.
/* 706  *|  * @param {Number} y The y coordinate of the cell.
/* 707  *|  * @param {Number} z The z coordinate of the cell.
/* 708  *|  */
/* 709  */ MarkerManager.prototype.addCellMarkers_ = function (x, y, z) {
/* 710  */   this.processCellMarkers_(x, y, z, this.addOverlay_);
/* 711  */ };
/* 712  */ /**
/* 713  *|  * Use the rectangleDiffCoords_ function to process all grid cells
/* 714  *|  * that are in bounds1 but not bounds2, using a callback, and using
/* 715  *|  * the current MarkerManager object as the instance.
/* 716  *|  *
/* 717  *|  * Pass the z parameter to the callback in addition to x and y.
/* 718  *|  *
/* 719  *|  * @param {Bounds} bounds1 The bounds of all points we may process.
/* 720  *|  * @param {Bounds} bounds2 The bounds of points to exclude.
/* 721  *|  * @param {Function} callback The callback function to call
/* 722  *|  *                   for each grid coordinate (x, y, z).
/* 723  *|  */
/* 724  */ MarkerManager.prototype.rectangleDiff_ = function (bounds1, bounds2, callback) {
/* 725  */   var me = this;
/* 726  */   me.rectangleDiffCoords_(bounds1, bounds2, function (x, y) {
/* 727  */     callback.apply(me, [x, y, bounds1.z]);
/* 728  */   });
/* 729  */ };
/* 730  */ /**
/* 731  *|  * Calls the function for all points in bounds1, not in bounds2
/* 732  *|  *
/* 733  *|  * @param {Bounds} bounds1 The bounds of all points we may process.
/* 734  *|  * @param {Bounds} bounds2 The bounds of points to exclude.
/* 735  *|  * @param {Function} callback The callback function to call
/* 736  *|  *                   for each grid coordinate.
/* 737  *|  */
/* 738  */ MarkerManager.prototype.rectangleDiffCoords_ = function (bounds1, bounds2, callback) {
/* 739  */   var minX1 = bounds1.minX;
/* 740  */   var minY1 = bounds1.minY;
/* 741  */   var maxX1 = bounds1.maxX;
/* 742  */   var maxY1 = bounds1.maxY;
/* 743  */   var minX2 = bounds2.minX;
/* 744  */   var minY2 = bounds2.minY;
/* 745  */   var maxX2 = bounds2.maxX;
/* 746  */   var maxY2 = bounds2.maxY;
/* 747  */   var x, y;
/* 748  */   for (x = minX1; x <= maxX1; x++) {  // All x in R1
/* 749  */     // All above:
/* 750  */     for (y = minY1; y <= maxY1 && y < minY2; y++) {  // y in R1 above R2

/* markermanager.js */

/* 751  */       callback(x, y);
/* 752  */     }
/* 753  */     // All below:
/* 754  */     for (y = Math.max(maxY2 + 1, minY1);  // y in R1 below R2
/* 755  */          y <= maxY1; y++) {
/* 756  */       callback(x, y);
/* 757  */     }
/* 758  */   }
/* 759  */   for (y = Math.max(minY1, minY2);
/* 760  */        y <= Math.min(maxY1, maxY2); y++) {  // All y in R2 and in R1
/* 761  */     // Strictly left:
/* 762  */     for (x = Math.min(maxX1 + 1, minX2) - 1;
/* 763  */          x >= minX1; x--) {  // x in R1 left of R2
/* 764  */       callback(x, y);
/* 765  */     }
/* 766  */     // Strictly right:
/* 767  */     for (x = Math.max(minX1, maxX2 + 1);  // x in R1 right of R2
/* 768  */          x <= maxX1; x++) {
/* 769  */       callback(x, y);
/* 770  */     }
/* 771  */   }
/* 772  */ };
/* 773  */ /**
/* 774  *|  * Removes value from array. O(N).
/* 775  *|  *
/* 776  *|  * @param {Array} array  The array to modify.
/* 777  *|  * @param {any} value  The value to remove.
/* 778  *|  * @param {Boolean} opt_notype  Flag to disable type checking in equality.
/* 779  *|  * @return {Number}  The number of instances of value that were removed.
/* 780  *|  */
/* 781  */ MarkerManager.prototype.removeFromArray_ = function (array, value, opt_notype) {
/* 782  */   var shift = 0;
/* 783  */   for (var i = 0; i < array.length; ++i) {
/* 784  */     if (array[i] === value || (opt_notype && array[i] === value)) {
/* 785  */       array.splice(i--, 1);
/* 786  */       shift++;
/* 787  */     }
/* 788  */   }
/* 789  */   return shift;
/* 790  */ };
/* 791  */ /**
/* 792  *| *   Projection overlay helper. Helps in calculating
/* 793  *| *   that markers get into the right grid.
/* 794  *| *   @constructor
/* 795  *| *   @param {Map} map The map to manage.
/* 796  *| **/
/* 797  */ function ProjectionHelperOverlay(map) {
/* 798  */   
/* 799  */   this.setMap(map);
/* 800  */   var TILEFACTOR = 8;

/* markermanager.js */

/* 801  */   var TILESIDE = 1 << TILEFACTOR;
/* 802  */   var RADIUS = 7;
/* 803  */   this._map = map;
/* 804  */   this._zoom = -1;
/* 805  */   this._X0 =
/* 806  */   this._Y0 =
/* 807  */   this._X1 =
/* 808  */   this._Y1 = -1;
/* 809  */   
/* 810  */ }
/* 811  */ ProjectionHelperOverlay.prototype = new google.maps.OverlayView();
/* 812  */ /**
/* 813  *|  *  Helper function to convert Lng to X
/* 814  *|  *  @private
/* 815  *|  *  @param {float} lng
/* 816  *|  **/
/* 817  */ ProjectionHelperOverlay.prototype.LngToX_ = function (lng) {
/* 818  */   return (1 + lng / 180);
/* 819  */ };
/* 820  */ /**
/* 821  *|  *  Helper function to convert Lat to Y
/* 822  *|  *  @private
/* 823  *|  *  @param {float} lat
/* 824  *|  **/
/* 825  */ ProjectionHelperOverlay.prototype.LatToY_ = function (lat) {
/* 826  */   var sinofphi = Math.sin(lat * Math.PI / 180);
/* 827  */   return (1 - 0.5 / Math.PI * Math.log((1 + sinofphi) / (1 - sinofphi)));
/* 828  */ };
/* 829  */ /**
/* 830  *| *   Old school LatLngToPixel
/* 831  *| *   @param {LatLng} latlng google.maps.LatLng object
/* 832  *| *   @param {Number} zoom Zoom level
/* 833  *| *   @return {position} {x: pixelPositionX, y: pixelPositionY}
/* 834  *| **/
/* 835  */ ProjectionHelperOverlay.prototype.LatLngToPixel = function (latlng, zoom) {
/* 836  */   var map = this._map;
/* 837  */   var div = this.getProjection().fromLatLngToDivPixel(latlng);
/* 838  */   var abs = {x: ~~(0.5 + this.LngToX_(latlng.lng()) * (2 << (zoom + 6))), y: ~~(0.5 + this.LatToY_(latlng.lat()) * (2 << (zoom + 6)))};
/* 839  */   return abs;
/* 840  */ };
/* 841  */ /**
/* 842  *|  * Draw function only triggers a ready event for
/* 843  *|  * MarkerManager to know projection can proceed to
/* 844  *|  * initialize.
/* 845  *|  */
/* 846  */ ProjectionHelperOverlay.prototype.draw = function () {
/* 847  */   if (!this.ready) {
/* 848  */     this.ready = true;
/* 849  */     google.maps.event.trigger(this, 'ready');
/* 850  */   }

/* markermanager.js */

/* 851  */ };
/* 852  */ // ==ClosureCompiler==
/* 853  */ function MarkerClusterer(e,t,n){this.extend(MarkerClusterer,google.maps.OverlayView);this.map_=e;this.markers_=[];this.clusters_=[];this.sizes=[53,56,66,78,90];this.styles_=[];this.ready_=false;var r=n||{};this.gridSize_=r["gridSize"]||60;this.minClusterSize_=r["minimumClusterSize"]||2;this.maxZoom_=r["maxZoom"]||null;this.styles_=r["styles"]||[];this.imagePath_=r["imagePath"]||this.MARKER_CLUSTER_IMAGE_PATH_;this.imageExtension_=r["imageExtension"]||this.MARKER_CLUSTER_IMAGE_EXTENSION_;this.zoomOnClick_=true;if(r["zoomOnClick"]!=undefined){this.zoomOnClick_=r["zoomOnClick"]}this.averageCenter_=false;if(r["averageCenter"]!=undefined){this.averageCenter_=r["averageCenter"]}this.setupStyles_();this.setMap(e);this.prevZoom_=this.map_.getZoom();var i=this;google.maps.event.addListener(this.map_,"zoom_changed",function(){var e=i.map_.getZoom();var t=i.map_.minZoom||0;var n=Math.min(i.map_.maxZoom||100,i.map_.mapTypes[i.map_.getMapTypeId()].maxZoom);e=Math.min(Math.max(e,t),n);if(i.prevZoom_!=e){i.prevZoom_=e;i.resetViewport()}});google.maps.event.addListener(this.map_,"idle",function(){i.redraw()});if(t&&t.length){this.addMarkers(t,false)}}function Cluster(e){this.markerClusterer_=e;this.map_=e.getMap();this.gridSize_=e.getGridSize();this.minClusterSize_=e.getMinClusterSize();this.averageCenter_=e.isAverageCenter();this.center_=null;this.markers_=[];this.bounds_=null;this.clusterIcon_=new ClusterIcon(this,e.getStyles(),e.getGridSize())}function ClusterIcon(e,t,n){e.getMarkerClusterer().extend(ClusterIcon,google.maps.OverlayView);this.styles_=t;this.padding_=n||0;this.cluster_=e;this.center_=null;this.map_=e.getMap();this.div_=null;this.sums_=null;this.visible_=false;this.setMap(this.map_)}MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/"+"images/m";MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_="png";MarkerClusterer.prototype.extend=function(e,t){return function(e){for(var t in e.prototype){this.prototype[t]=e.prototype[t]}return this}.apply(e,[t])};MarkerClusterer.prototype.onAdd=function(){this.setReady_(true)};MarkerClusterer.prototype.draw=function(){};MarkerClusterer.prototype.setupStyles_=function(){if(this.styles_.length){return}for(var e=0,t;t=this.sizes[e];e++){this.styles_.push({url:this.imagePath_+(e+1)+"."+this.imageExtension_,height:t,width:t})}};MarkerClusterer.prototype.fitMapToMarkers=function(){var e=this.getMarkers();var t=new google.maps.LatLngBounds;for(var n=0,r;r=e[n];n++){t.extend(r.getPosition())}this.map_.fitBounds(t)};MarkerClusterer.prototype.setStyles=function(e){this.styles_=e};MarkerClusterer.prototype.getStyles=function(){return this.styles_};MarkerClusterer.prototype.isZoomOnClick=function(){return this.zoomOnClick_};MarkerClusterer.prototype.isAverageCenter=function(){return this.averageCenter_};MarkerClusterer.prototype.getMarkers=function(){return this.markers_};MarkerClusterer.prototype.getTotalMarkers=function(){return this.markers_.length};MarkerClusterer.prototype.setMaxZoom=function(e){this.maxZoom_=e};MarkerClusterer.prototype.getMaxZoom=function(){return this.maxZoom_};MarkerClusterer.prototype.calculator_=function(e,t){var n=0;var r=e.length;var i=r;while(i!==0){i=parseInt(i/10,10);n++}n=Math.min(n,t);return{text:r,index:n}};MarkerClusterer.prototype.setCalculator=function(e){this.calculator_=e};MarkerClusterer.prototype.getCalculator=function(){return this.calculator_};MarkerClusterer.prototype.addMarkers=function(e,t){for(var n=0,r;r=e[n];n++){this.pushMarkerTo_(r)}if(!t){this.redraw()}};MarkerClusterer.prototype.pushMarkerTo_=function(e){e.isAdded=false;if(e["draggable"]){var t=this;google.maps.event.addListener(e,"dragend",function(){e.isAdded=false;t.repaint()})}this.markers_.push(e)};MarkerClusterer.prototype.addMarker=function(e,t){this.pushMarkerTo_(e);if(!t){this.redraw()}};MarkerClusterer.prototype.removeMarker_=function(e){var t=-1;if(this.markers_.indexOf){t=this.markers_.indexOf(e)}else{for(var n=0,r;r=this.markers_[n];n++){if(r==e){t=n;break}}}if(t==-1){return false}e.setMap(null);this.markers_.splice(t,1);return true};MarkerClusterer.prototype.removeMarker=function(e,t){var n=this.removeMarker_(e);if(!t&&n){this.resetViewport();this.redraw();return true}else{return false}};MarkerClusterer.prototype.removeMarkers=function(e,t){var n=false;for(var r=0,i;i=e[r];r++){var s=this.removeMarker_(i);n=n||s}if(!t&&n){this.resetViewport();this.redraw();return true}};MarkerClusterer.prototype.setReady_=function(e){if(!this.ready_){this.ready_=e;this.createClusters_()}};MarkerClusterer.prototype.getTotalClusters=function(){return this.clusters_.length};MarkerClusterer.prototype.getMap=function(){return this.map_};MarkerClusterer.prototype.setMap=function(e){this.map_=e};MarkerClusterer.prototype.getGridSize=function(){return this.gridSize_};MarkerClusterer.prototype.setGridSize=function(e){this.gridSize_=e};MarkerClusterer.prototype.getMinClusterSize=function(){return this.minClusterSize_};MarkerClusterer.prototype.setMinClusterSize=function(e){this.minClusterSize_=e};MarkerClusterer.prototype.getExtendedBounds=function(e){var t=this.getProjection();var n=new google.maps.LatLng(e.getNorthEast().lat(),e.getNorthEast().lng());var r=new google.maps.LatLng(e.getSouthWest().lat(),e.getSouthWest().lng());var i=t.fromLatLngToDivPixel(n);i.x+=this.gridSize_;i.y-=this.gridSize_;var s=t.fromLatLngToDivPixel(r);s.x-=this.gridSize_;s.y+=this.gridSize_;var o=t.fromDivPixelToLatLng(i);var u=t.fromDivPixelToLatLng(s);e.extend(o);e.extend(u);return e};MarkerClusterer.prototype.isMarkerInBounds_=function(e,t){return t.contains(e.getPosition())};MarkerClusterer.prototype.clearMarkers=function(){this.resetViewport(true);this.markers_=[]};MarkerClusterer.prototype.resetViewport=function(e){for(var t=0,n;n=this.clusters_[t];t++){n.remove()}for(var t=0,r;r=this.markers_[t];t++){r.isAdded=false;if(e){r.setMap(null)}}this.clusters_=[]};MarkerClusterer.prototype.repaint=function(){var e=this.clusters_.slice();this.clusters_.length=0;this.resetViewport();this.redraw();window.setTimeout(function(){for(var t=0,n;n=e[t];t++){n.remove()}},0)};MarkerClusterer.prototype.redraw=function(){this.createClusters_()};MarkerClusterer.prototype.distanceBetweenPoints_=function(e,t){if(!e||!t){return 0}var n=6371;var r=(t.lat()-e.lat())*Math.PI/180;var i=(t.lng()-e.lng())*Math.PI/180;var s=Math.sin(r/2)*Math.sin(r/2)+Math.cos(e.lat()*Math.PI/180)*Math.cos(t.lat()*Math.PI/180)*Math.sin(i/2)*Math.sin(i/2);var o=2*Math.atan2(Math.sqrt(s),Math.sqrt(1-s));var u=n*o;return u};MarkerClusterer.prototype.addToClosestCluster_=function(e){var t=4e4;var n=null;var r=e.getPosition();for(var i=0,s;s=this.clusters_[i];i++){var o=s.getCenter();if(o){var u=this.distanceBetweenPoints_(o,e.getPosition());if(u<t){t=u;n=s}}}if(n&&n.isMarkerInClusterBounds(e)){n.addMarker(e)}else{var s=new Cluster(this);s.addMarker(e);this.clusters_.push(s)}};MarkerClusterer.prototype.createClusters_=function(){if(!this.ready_){return}var e=new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(),this.map_.getBounds().getNorthEast());var t=this.getExtendedBounds(e);for(var n=0,r;r=this.markers_[n];n++){if(!r.isAdded&&this.isMarkerInBounds_(r,t)){this.addToClosestCluster_(r)}}};Cluster.prototype.isMarkerAlreadyAdded=function(e){if(this.markers_.indexOf){return this.markers_.indexOf(e)!=-1}else{for(var t=0,n;n=this.markers_[t];t++){if(n==e){return true}}}return false};Cluster.prototype.addMarker=function(e){if(this.isMarkerAlreadyAdded(e)){return false}if(!this.center_){this.center_=e.getPosition();this.calculateBounds_()}else{if(this.averageCenter_){var t=this.markers_.length+1;var n=(this.center_.lat()*(t-1)+e.getPosition().lat())/t;var r=(this.center_.lng()*(t-1)+e.getPosition().lng())/t;this.center_=new google.maps.LatLng(n,r);this.calculateBounds_()}}e.isAdded=true;this.markers_.push(e);var i=this.markers_.length;if(i<this.minClusterSize_&&e.getMap()!=this.map_){e.setMap(this.map_)}if(i==this.minClusterSize_){for(var s=0;s<i;s++){this.markers_[s].setMap(null)}}if(i>=this.minClusterSize_){e.setMap(null)}this.updateIcon();return true};Cluster.prototype.getMarkerClusterer=function(){return this.markerClusterer_};Cluster.prototype.getBounds=function(){var e=new google.maps.LatLngBounds(this.center_,this.center_);var t=this.getMarkers();for(var n=0,r;r=t[n];n++){e.extend(r.getPosition())}return e};Cluster.prototype.remove=function(){this.clusterIcon_.remove();this.markers_.length=0;delete this.markers_};Cluster.prototype.getSize=function(){return this.markers_.length};Cluster.prototype.getMarkers=function(){return this.markers_};Cluster.prototype.getCenter=function(){return this.center_};Cluster.prototype.calculateBounds_=function(){var e=new google.maps.LatLngBounds(this.center_,this.center_);this.bounds_=this.markerClusterer_.getExtendedBounds(e)};Cluster.prototype.isMarkerInClusterBounds=function(e){return this.bounds_.contains(e.getPosition())};Cluster.prototype.getMap=function(){return this.map_};Cluster.prototype.updateIcon=function(){var e=this.map_.getZoom();var t=this.markerClusterer_.getMaxZoom();if(t&&e>t){for(var n=0,r;r=this.markers_[n];n++){r.setMap(this.map_)}return}if(this.markers_.length<this.minClusterSize_){this.clusterIcon_.hide();return}var i=this.markerClusterer_.getStyles().length;var s=this.markerClusterer_.getCalculator()(this.markers_,i);this.clusterIcon_.setCenter(this.center_);this.clusterIcon_.setSums(s);this.clusterIcon_.show()};ClusterIcon.prototype.triggerClusterClick=function(){var e=this.cluster_.getMarkerClusterer();google.maps.event.trigger(e,"clusterclick",this.cluster_);if(e.isZoomOnClick()){this.map_.fitBounds(this.cluster_.getBounds())}};ClusterIcon.prototype.onAdd=function(){this.div_=document.createElement("DIV");if(this.visible_){var e=this.getPosFromLatLng_(this.center_);this.div_.style.cssText=this.createCss(e);this.div_.innerHTML=this.sums_.text}var t=this.getPanes();t.overlayMouseTarget.appendChild(this.div_);var n=this;google.maps.event.addDomListener(this.div_,"click",function(){n.triggerClusterClick()})};ClusterIcon.prototype.getPosFromLatLng_=function(e){var t=this.getProjection().fromLatLngToDivPixel(e);t.x-=parseInt(this.width_/2,10);t.y-=parseInt(this.height_/2,10);return t};ClusterIcon.prototype.draw=function(){if(this.visible_){var e=this.getPosFromLatLng_(this.center_);this.div_.style.top=e.y+"px";this.div_.style.left=e.x+"px"}};ClusterIcon.prototype.hide=function(){if(this.div_){this.div_.style.display="none"}this.visible_=false};ClusterIcon.prototype.show=function(){if(this.div_){var e=this.getPosFromLatLng_(this.center_);this.div_.style.cssText=this.createCss(e);this.div_.style.display=""}this.visible_=true};ClusterIcon.prototype.remove=function(){this.setMap(null)};ClusterIcon.prototype.onRemove=function(){if(this.div_&&this.div_.parentNode){this.hide();this.div_.parentNode.removeChild(this.div_);this.div_=null}};ClusterIcon.prototype.setSums=function(e){this.sums_=e;this.text_=e.text;this.index_=e.index;if(this.div_){this.div_.innerHTML=e.text}this.useStyle()};ClusterIcon.prototype.useStyle=function(){var e=Math.max(0,this.sums_.index-1);e=Math.min(this.styles_.length-1,e);var t=this.styles_[e];this.url_=t["url"];this.height_=t["height"];this.width_=t["width"];this.textColor_=t["textColor"];this.anchor_=t["anchor"];this.textSize_=t["textSize"];this.backgroundPosition_=t["backgroundPosition"]};ClusterIcon.prototype.setCenter=function(e){this.center_=e};ClusterIcon.prototype.createCss=function(e){var t=[];t.push("background-image:url("+this.url_+");");var n=this.backgroundPosition_?this.backgroundPosition_:"0 0";t.push("background-position:"+n+";");if(typeof this.anchor_==="object"){if(typeof this.anchor_[0]==="number"&&this.anchor_[0]>0&&this.anchor_[0]<this.height_){t.push("height:"+(this.height_-this.anchor_[0])+"px; padding-top:"+this.anchor_[0]+"px;")}else{t.push("height:"+this.height_+"px; line-height:"+this.height_+"px;")}if(typeof this.anchor_[1]==="number"&&this.anchor_[1]>0&&this.anchor_[1]<this.width_){t.push("width:"+(this.width_-this.anchor_[1])+"px; padding-left:"+this.anchor_[1]+"px;")}else{t.push("width:"+this.width_+"px; text-align:center;")}}else{t.push("height:"+this.height_+"px; line-height:"+this.height_+"px; width:"+this.width_+"px; text-align:center;")}var r=this.textColor_?this.textColor_:"black";var i=this.textSize_?this.textSize_:11;t.push("cursor:pointer; top:"+e.y+"px; left:"+e.x+"px; color:"+r+"; position:absolute; font-size:"+i+"px; font-family:Arial,sans-serif; font-weight:bold");return t.join("")};window["MarkerClusterer"]=MarkerClusterer;MarkerClusterer.prototype["addMarker"]=MarkerClusterer.prototype.addMarker;MarkerClusterer.prototype["addMarkers"]=MarkerClusterer.prototype.addMarkers;MarkerClusterer.prototype["clearMarkers"]=MarkerClusterer.prototype.clearMarkers;MarkerClusterer.prototype["fitMapToMarkers"]=MarkerClusterer.prototype.fitMapToMarkers;MarkerClusterer.prototype["getCalculator"]=MarkerClusterer.prototype.getCalculator;MarkerClusterer.prototype["getGridSize"]=MarkerClusterer.prototype.getGridSize;MarkerClusterer.prototype["getExtendedBounds"]=MarkerClusterer.prototype.getExtendedBounds;MarkerClusterer.prototype["getMap"]=MarkerClusterer.prototype.getMap;MarkerClusterer.prototype["getMarkers"]=MarkerClusterer.prototype.getMarkers;MarkerClusterer.prototype["getMaxZoom"]=MarkerClusterer.prototype.getMaxZoom;MarkerClusterer.prototype["getStyles"]=MarkerClusterer.prototype.getStyles;MarkerClusterer.prototype["getTotalClusters"]=MarkerClusterer.prototype.getTotalClusters;MarkerClusterer.prototype["getTotalMarkers"]=MarkerClusterer.prototype.getTotalMarkers;MarkerClusterer.prototype["redraw"]=MarkerClusterer.prototype.redraw;MarkerClusterer.prototype["removeMarker"]=MarkerClusterer.prototype.removeMarker;MarkerClusterer.prototype["removeMarkers"]=MarkerClusterer.prototype.removeMarkers;MarkerClusterer.prototype["resetViewport"]=MarkerClusterer.prototype.resetViewport;MarkerClusterer.prototype["repaint"]=MarkerClusterer.prototype.repaint;MarkerClusterer.prototype["setCalculator"]=MarkerClusterer.prototype.setCalculator;MarkerClusterer.prototype["setGridSize"]=MarkerClusterer.prototype.setGridSize;MarkerClusterer.prototype["setMaxZoom"]=MarkerClusterer.prototype.setMaxZoom;MarkerClusterer.prototype["onAdd"]=MarkerClusterer.prototype.onAdd;MarkerClusterer.prototype["draw"]=MarkerClusterer.prototype.draw;Cluster.prototype["getCenter"]=Cluster.prototype.getCenter;Cluster.prototype["getSize"]=Cluster.prototype.getSize;Cluster.prototype["getMarkers"]=Cluster.prototype.getMarkers;ClusterIcon.prototype["onAdd"]=ClusterIcon.prototype.onAdd;ClusterIcon.prototype["draw"]=ClusterIcon.prototype.draw;ClusterIcon.prototype["onRemove"]=ClusterIcon.prototype.onRemove
/* 854  */ 
/* 855  */ // marker infobubble window
/* 856  */ function InfoBubble(e){ this.extend(InfoBubble,google.maps.OverlayView);this.baseZIndex_=100;this.isOpen_=false;var t=e||{};if(t["backgroundColor"]==undefined){t["backgroundColor"]=this.BACKGROUND_COLOR_}if(t["borderColor"]==undefined){t["borderColor"]=this.BORDER_COLOR_}if(t["borderRadius"]==undefined){t["borderRadius"]=this.BORDER_RADIUS_}if(t["borderWidth"]==undefined){t["borderWidth"]=this.BORDER_WIDTH_}if(t["padding"]==undefined){t["padding"]=this.PADDING_}if(t["arrowPosition"]==undefined){t["arrowPosition"]=this.ARROW_POSITION_}if(t["minWidth"]==undefined){t["minWidth"]=this.MIN_WIDTH_}this.buildDom_();this.setValues(t)}window["InfoBubble"]=InfoBubble;InfoBubble.prototype.ARROW_SIZE_=15;InfoBubble.prototype.ARROW_STYLE_=0;InfoBubble.prototype.SHADOW_STYLE_=1;InfoBubble.prototype.MIN_WIDTH_=50;InfoBubble.prototype.ARROW_POSITION_=50;InfoBubble.prototype.PADDING_=10;InfoBubble.prototype.BORDER_WIDTH_=1;InfoBubble.prototype.BORDER_COLOR_="#ccc";InfoBubble.prototype.BORDER_RADIUS_=10;InfoBubble.prototype.BACKGROUND_COLOR_="#fff";InfoBubble.prototype.extend=function(e,t){return function(e){for(var t in e.prototype){this.prototype[t]=e.prototype[t]}return this}.apply(e,[t])};InfoBubble.prototype.buildDom_=function(){var e=this.bubble_=document.createElement("DIV");e.style["position"]="absolute";e.style["zIndex"]=this.baseZIndex_;var t=this.close_=document.createElement("IMG");t.style["position"]="absolute";t.style["width"]=this.px(12);t.style["height"]=this.px(12);t.style["border"]=0;t.style["zIndex"]=this.baseZIndex_+1;t.style["cursor"]="pointer";t.src=closeimg;var n=this;google.maps.event.addDomListener(t,"click",function(){n.close();google.maps.event.trigger(n,"closeclick")});var r=this.contentContainer_=document.createElement("DIV");r.style["overflowX"]="visible";r.style["overflowY"]="visible";r.style["cursor"]="default";r.style["clear"]="both";r.style["position"]="relative";r.className="map_infobubble map_popup";var i=this.content_=document.createElement("DIV");r.appendChild(i);var s=this.arrow_=document.createElement("DIV");s.style["position"]="relative";s.className="map_infoarrow";var o=this.arrowOuter_=document.createElement("DIV");var u=this.arrowInner_=document.createElement("DIV");var a=this.getArrowSize_();o.style["position"]=u.style["position"]="absolute";o.style["left"]=u.style["left"]="50%";o.style["height"]=u.style["height"]="0";o.style["width"]=u.style["width"]="0";o.style["marginLeft"]=this.px(-a);o.style["borderWidth"]=this.px(a);o.style["borderBottomWidth"]=0;var f=document.createElement("DIV");f.style["position"]="absolute";e.style["display"]=f.style["display"]="none";e.appendChild(t);e.appendChild(r);s.appendChild(o);s.appendChild(u);e.appendChild(s);var l=document.createElement("style");l.setAttribute("type","text/css");var c="";l.textContent=c;document.getElementsByTagName("head")[0].appendChild(l)};InfoBubble.prototype.setBackgroundClassName=function(e){this.set("backgroundClassName",e)};InfoBubble.prototype["setBackgroundClassName"]=InfoBubble.prototype.setBackgroundClassName;InfoBubble.prototype.getArrowStyle_=function(){return parseInt(this.get("arrowStyle"),10)||0};InfoBubble.prototype.setArrowStyle=function(e){this.set("arrowStyle",e)};InfoBubble.prototype["setArrowStyle"]=InfoBubble.prototype.setArrowStyle;InfoBubble.prototype.getArrowSize_=function(){return parseInt(this.get("arrowSize"),10)||0};InfoBubble.prototype.getArrowPosition_=function(){return parseInt(this.get("arrowPosition"),10)||0};InfoBubble.prototype.setZIndex=function(e){this.set("zIndex",e)};InfoBubble.prototype["setZIndex"]=InfoBubble.prototype.setZIndex;InfoBubble.prototype.getZIndex=function(){return parseInt(this.get("zIndex"),10)||this.baseZIndex_};InfoBubble.prototype.setShadowStyle=function(e){this.set("shadowStyle",e)};InfoBubble.prototype["setShadowStyle"]=InfoBubble.prototype.setShadowStyle;InfoBubble.prototype.getShadowStyle_=function(){return parseInt(this.get("shadowStyle"),10)||0};InfoBubble.prototype.showCloseButton=function(){this.set("hideCloseButton",false)};InfoBubble.prototype["showCloseButton"]=InfoBubble.prototype.showCloseButton;InfoBubble.prototype.hideCloseButton=function(){this.set("hideCloseButton",true)};InfoBubble.prototype["hideCloseButton"]=InfoBubble.prototype.hideCloseButton;InfoBubble.prototype.getBorderRadius_=function(){return parseInt(this.get("borderRadius"),10)||0};InfoBubble.prototype.getBorderWidth_=function(){return parseInt(this.get("borderWidth"),10)||0};InfoBubble.prototype.setBorderWidth=function(e){this.set("borderWidth",e)};InfoBubble.prototype["setBorderWidth"]=InfoBubble.prototype.setBorderWidth;InfoBubble.prototype.getPadding_=function(){return parseInt(this.get("padding"),10)||0};InfoBubble.prototype.px=function(e){if(e){return e+"px"}return e};InfoBubble.prototype.addEvents_=function(){var e=["mousedown","mousemove","mouseover","mouseout","mouseup","mousewheel","DOMMouseScroll","touchstart","touchend","touchmove","dblclick","contextmenu","click"];var t=this.bubble_;this.listeners_=[];for(var n=0,r;r=e[n];n++){this.listeners_.push(google.maps.event.addDomListener(t,r,function(e){e.cancelBubble=true;if(e.stopPropagation){e.stopPropagation()}}))}};InfoBubble.prototype.onAdd=function(){if(!this.bubble_){this.buildDom_()}this.addEvents_();var e=this.getPanes();if(e){e.floatPane.appendChild(this.bubble_)}};InfoBubble.prototype["onAdd"]=InfoBubble.prototype.onAdd;InfoBubble.prototype.draw=function(){var e=this.getProjection();if(!e){return}var t=this.get("position");if(!t){this.close();return}var n=0;var r=this.getAnchorHeight_();var i=this.getArrowSize_();var s=this.getArrowPosition_();s=s/100;var o=e.fromLatLngToDivPixel(t);var u=this.contentContainer_.offsetWidth;var a=this.bubble_.offsetHeight;if(!u){return}var f=o.y-(a+i);if(r){f-=r}var l=o.x-u*s;this.bubble_.style["top"]=this.px(f);this.bubble_.style["left"]=this.px(l)};InfoBubble.prototype["draw"]=InfoBubble.prototype.draw;InfoBubble.prototype.onRemove=function(){if(this.bubble_&&this.bubble_.parentNode){this.bubble_.parentNode.removeChild(this.bubble_)}for(var e=0,t;t=this.listeners_[e];e++){google.maps.event.removeListener(t)}};InfoBubble.prototype["onRemove"]=InfoBubble.prototype.onRemove;InfoBubble.prototype.isOpen=function(){return this.isOpen_};InfoBubble.prototype["isOpen"]=InfoBubble.prototype.isOpen;InfoBubble.prototype.close=function(){if(this.bubble_){this.bubble_.style["display"]="none"}this.isOpen_=false};InfoBubble.prototype["close"]=InfoBubble.prototype.close;InfoBubble.prototype.open=function(e,t){var n=this;window.setTimeout(function(){n.open_(e,t)},0)};InfoBubble.prototype.open_=function(e,t){this.updateContent_();if(e){this.setMap(e)}if(t){this.set("anchor",t);this.bindTo("anchorPoint",t);this.bindTo("position",t)}this.bubble_.style["display"]="";this.redraw_();this.isOpen_=true;var n=!this.get("disableAutoPan");if(n){var r=this;window.setTimeout(function(){r.panToView()},200)}};InfoBubble.prototype["open"]=InfoBubble.prototype.open;InfoBubble.prototype.setPosition=function(e){if(e){this.set("position",e)}};InfoBubble.prototype["setPosition"]=InfoBubble.prototype.setPosition;InfoBubble.prototype.getPosition=function(){return this.get("position")};InfoBubble.prototype["getPosition"]=InfoBubble.prototype.getPosition;InfoBubble.prototype.panToView=function(){var e=this.getProjection();if(!e){return}if(!this.bubble_){return}var t=this.getAnchorHeight_();var n=this.bubble_.offsetHeight+t;var r=this.get("map");var i=r.getDiv();var s=i.offsetHeight;var o=this.getPosition();var u=e.fromLatLngToContainerPixel(r.getCenter());var a=e.fromLatLngToContainerPixel(o);var f=u.y-n;var l=s-u.y;var c=f<0;var h=0;if(c){f*=-1;h=(f+l)/2}a.y-=h;o=e.fromContainerPixelToLatLng(a);if(r.getCenter()!=o){r.panTo(o)}};InfoBubble.prototype["panToView"]=InfoBubble.prototype.panToView;InfoBubble.prototype.htmlToDocumentFragment_=function(e){e=e.replace(/^\s*([\S\s]*)\b\s*$/,"$1");var t=document.createElement("DIV");t.innerHTML=e;if(t.childNodes.length==1){return t.removeChild(t.firstChild)}else{var n=document.createDocumentFragment();while(t.firstChild){n.appendChild(t.firstChild)}return n}};InfoBubble.prototype.removeChildren_=function(e){if(!e){return}var t;while(t=e.firstChild){e.removeChild(t)}};InfoBubble.prototype.setContent=function(e){this.set("content",e)};InfoBubble.prototype["setContent"]=InfoBubble.prototype.setContent;InfoBubble.prototype.getContent=function(){return this.get("content")};InfoBubble.prototype["getContent"]=InfoBubble.prototype.getContent;InfoBubble.prototype.updateContent_=function(){if(!this.content_){return}this.removeChildren_(this.content_);var e=this.getContent();if(e){if(typeof e=="string"){e=this.htmlToDocumentFragment_(e)}this.content_.appendChild(e);var t=this;var n=this.content_.getElementsByTagName("IMG");for(var r=0,i;i=n[r];r++){google.maps.event.addDomListener(i,"load",function(){t.imageLoaded_()})}google.maps.event.trigger(this,"domready")}this.redraw_()};InfoBubble.prototype.imageLoaded_=function(){var e=!this.get("disableAutoPan");this.redraw_()};InfoBubble.prototype.setMaxWidth=function(e){this.set("maxWidth",e)};InfoBubble.prototype["setMaxWidth"]=InfoBubble.prototype.setMaxWidth;InfoBubble.prototype.setMaxHeight=function(e){this.set("maxHeight",e)};InfoBubble.prototype["setMaxHeight"]=InfoBubble.prototype.setMaxHeight;InfoBubble.prototype.setMinWidth=function(e){this.set("minWidth",e)};InfoBubble.prototype["setMinWidth"]=InfoBubble.prototype.setMinWidth;InfoBubble.prototype.setMinHeight=function(e){this.set("minHeight",e)};InfoBubble.prototype["setMinHeight"]=InfoBubble.prototype.setMinHeight;InfoBubble.prototype.getElementSize_=function(e,t,n){var r=document.createElement("DIV");r.style["display"]="inline";r.style["position"]="absolute";r.style["visibility"]="hidden";if(typeof e=="string"){r.innerHTML=e}else{r.appendChild(e.cloneNode(true))}document.body.appendChild(r);var i=new google.maps.Size(r.offsetWidth,r.offsetHeight);if(t&&i.width>t){r.style["width"]=this.px(t);i=new google.maps.Size(r.offsetWidth,r.offsetHeight)}if(n&&i.height>n){r.style["height"]=this.px(n);i=new google.maps.Size(r.offsetWidth,r.offsetHeight)}document.body.removeChild(r);delete r;return i};InfoBubble.prototype.redraw_=function(){this.figureOutSize_();this.positionCloseButton_();this.draw()};InfoBubble.prototype.figureOutSize_=function(){var e=this.get("map");if(!e){return}var t=this.getPadding_();var n=this.getBorderWidth_();var r=this.getBorderRadius_();var i=this.getArrowSize_();var s=e.getDiv();var o=i*2;var u=s.offsetWidth-o;var a=s.offsetHeight-o-this.getAnchorHeight_();var f=0;var l=this.get("minWidth")||0;var c=this.get("minHeight")||0;var h=this.get("maxWidth")||0;var p=this.get("maxHeight")||0;h=Math.min(u,h);p=Math.min(a,p);var d=0;var v=this.get("content");if(typeof v=="string"){v=this.htmlToDocumentFragment_(v)}if(v){var m=this.getElementSize_(v,h,p);if(l<m.width){l=m.width}if(c<m.height){c=m.height}}if(h){l=Math.min(l,h)}if(p){c=Math.min(c,p)}l=Math.max(l,d);if(l==d){l=l+2*t}i=i*2;l=Math.max(l,i);if(l>u){l=u}if(c>a){c=a-f}this.contentContainer_.style["width"]=this.px(l)};InfoBubble.prototype.getAnchorHeight_=function(){var e=this.get("anchor");if(e){var t=this.get("anchorPoint");if(t){return-1*t.y}}return 0};InfoBubble.prototype.positionCloseButton_=function(){var e=this.getBorderRadius_();var t=this.getBorderWidth_();var n=2;var r=56;r+=t;n+=t;var i=this.contentContainer_;if(i&&i.clientHeight<i.scrollHeight){n+=15}this.close_.style["right"]=this.px(n);this.close_.style["top"]=this.px(r)}
/* 857  */ 
/* 858  */ 
/* 859  */ /* Delete google Map marker */
/* 860  */ function googlemaplisting_deleteMarkers() {
/* 861  */ 	if (markerArray && markerArray.length > 0){
/* 862  */ 		for (i in markerArray){
/* 863  */ 			if (!isNaN(i)){
/* 864  */ 				markerArray[i].setMap(null);
/* 865  */ 				infoBubble.close();
/* 866  */ 			}
/* 867  */ 		}
/* 868  */ 		markerArray.length = 0;
/* 869  */ 	}
/* 870  */ 	mgr.clearMarkers();
/* 871  */ 	if(clustering !=1){
/* 872  */ 		markerClusterer.clearMarkers();
/* 873  */ 	}
/* 874  */ }
/* 875  */ 
/* 876  */ 
/* 877  */ /* Add google Map marker */
/* 878  */ function templ_add_googlemap_markers(markers){
/* 879  */ 	mgr = new MarkerManager( map );	
/* 880  */ 	infowindow = new google.maps.InfoWindow();
/* 881  */ 	if (markers && markers.length > 0) {
/* 882  */ 		for (var i = 0; i < markers.length; i++) {	
/* 883  */ 			var details = markers[i];
/* 884  */ 			var image = new google.maps.MarkerImage(details.icons);
/* 885  */ 			var myLatLng = new google.maps.LatLng(details.location[0], details.location[1]);
/* 886  */ 			if(typeof details.load_content != 'undefined'){
/* 887  */ 				var details_load_content=details.load_content;
/* 888  */ 			}else{
/* 889  */ 				var details_load_content='';
/* 890  */ 			}
/* 891  */ 			
/* 892  */ 			
/* 893  */ 			markers[i] = new google.maps.Marker({ title: details.name, position: myLatLng, icon: image ,content: details.message,post_id:details.pid ,load_content:details_load_content});
/* 894  */ 			markerArray.push(markers[i]);
/* 895  */ 			
/* 896  */ 			attachMessage(markers[i], details.message,details.pid,details.load_content);
/* 897  */ 			bounds.extend(myLatLng);
/* 898  */ 			var pinpointElement = document.getElementById( 'pinpoint_'+details.pid );
/* 899  */ 			if ( pinpointElement ) {
/* 900  */ 				if(pippoint_effects=='hover'){

/* markermanager.js */

/* 901  */ 					google.maps.event.addDomListener( pinpointElement, 'mouseover', (function( theMarker ) {								
/* 902  */ 					 return function() {
/* 903  */ 						google.maps.event.trigger( theMarker, 'click' );
/* 904  */ 					 };
/* 905  */ 				  })(markers[i]) );
/* 906  */ 				}else if(pippoint_effects=='click'){
/* 907  */ 					
/* 908  */ 					google.maps.event.addDomListener( pinpointElement, 'click', (function( theMarker ) {
/* 909  */ 					 return function() {
/* 910  */ 						google.maps.event.trigger( theMarker, 'click' );
/* 911  */ 					 };
/* 912  */ 				  })(markers[i]) );
/* 913  */ 					
/* 914  */ 				}// Pinpoint click
/* 915  */ 				
/* 916  */ 			}// pinpointElement
/* 917  */ 			
/* 918  */ 		}
/* 919  */ 	}// markers if condition
/* 920  */ 
/* 921  */ 	google.maps.event.addListener(mgr, 'loaded', function() {
/* 922  */ 		mgr.addMarkers( markerArray, 0 );
/* 923  */ 		mgr.refresh();
/* 924  */ 	});
/* 925  */ 	
/* 926  */ 	/* Start New information window on map 	*/
/* 927  */ 	 infowindow = new InfoBubble({
/* 928  */ 		 maxWidth:210,minWidth:210,display: "inline-block", overflow: "auto" ,backgroundColor:"#fff"
/* 929  */ 	  });			
/* 930  */ 	/* End */
/* 931  */ 	
/* 932  */ 	/* Set marker cluster on google map */
/* 933  */ 	if(clustering !=1){
/* 934  */ 		markerClusterer = new MarkerClusterer(map, markers,{maxZoom: 0,gridSize: 40,styles: null,infoOnClick: 1,infoOnClickZoom: 18,});
/* 935  */ 		 google.maps.event.addListener(markerClusterer, 'clusterclick', function(cluster) {
/* 936  */ 			/* Convert lat/long from cluster object to a usable MVCObject */
/* 937  */ 			var info = new google.maps.MVCObject;
/* 938  */ 			info.set('position', cluster.center_);
/* 939  */ 			/*Get markers*/
/* 940  */ 			var markers = (cluster.getMarkers()) ? cluster.getMarkers() : '""';
/* 941  */ 			var content = post_id = "";
/* 942  */ 			/*Get all the titles*/
/* 943  */ 			for(var i = 0; i < markers.length; i++) {			
/* 944  */ 				content += markers[i].content + "\n";
/* 945  */ 				post_id += markers[i].post_id + ",";
/* 946  */ 				var load_content=markers[i].load_content;
/* 947  */ 			}
/* 948  */ 			if(load_content==1){
/* 949  */ 				content='<div class="google-map-info"><div class="map-inner-wrapper"><div class="map-item-info"><i class="fa fa-circle-o-notch fa-spin fa-2x"></i></div></div></div>';
/* 950  */ 			}

/* markermanager.js */

/* 951  */ 			if(map.getZoom()==21){
/* 952  */ 				infowindow.close();
/* 953  */ 				infowindow.setContent( content );
/* 954  */ 				infowindow.open(map, info);
/* 955  */ 				if(typeof load_content != 'undefined'){
/* 956  */ 					/*Load marke data */
/* 957  */ 					jQuery.ajax({
/* 958  */ 						url:tevolutionajaxUrl,
/* 959  */ 						data:'action=mapmarker_post_detail&post_id='+post_id,
/* 960  */ 						success:function(results){
/* 961  */ 							infowindow.close();
/* 962  */ 							infowindow.setContent(results);
/* 963  */ 							infowindow.open(map, info);
/* 964  */ 						}
/* 965  */ 					});
/* 966  */ 				}
/* 967  */ 			}
/* 968  */ 		});
/* 969  */ 	}
/* 970  */ }
/* 971  */ 
/* 972  */ // but that message is not within the marker's instance data 
/* 973  */ function attachMessage(marker, msg, post_id, load_content) {
/* 974  */ 	google.maps.event.addListener(marker, 'click', function() {
/* 975  */ 		infoBubble.setContent(msg);
/* 976  */ 		infoBubble.open(map, marker);
/* 977  */ 		/*Load content is define then get the marker data using ajax */
/* 978  */ 		if(typeof load_content != 'undefined'){			
/* 979  */ 		 	/*Load marke data */
/* 980  */ 			jQuery.ajax({  
/* 981  */ 				url:tevolutionajaxUrl,			
/* 982  */ 				data:'action=mapmarker_post_detail&post_id='+post_id,
/* 983  */ 				success:function(results){
/* 984  */ 					//alert('Helloo'+results);
/* 985  */ 					infoBubble.setContent(results);
/* 986  */ 					infoBubble.open(map, marker);
/* 987  */ 				}
/* 988  */ 			});
/* 989  */ 		}
/* 990  */ 	});
/* 991  */ }	
/* 992  */ 
/* 993  */ /* Refresh_markers */
/* 994  */ var search_map_ajax = null;
/* 995  */ var data_map = null;
/* 996  */ function refresh_markers() {
/* 997  */ 	/*  Is not search page then return script*/
/* 998  */ 	/*if(is_search=='' || is_search.length <= 0){		
/* 999  *| 		//return;	
/* 1000 *| 	}*/

/* markermanager.js */

/* 1001 */ 	/* is draging and bounds modified  not set then return script */
/* 1002 */ 	if (!dragging  ||  !bounds_modified) return;
/* 1003 */ 	
/* 1004 */ 	
/* 1005 */ 	bounds_modified = false;
/* 1006 */ 	dragging = false;
/* 1007 */ 	/* Get north east and soth west bounds */
/* 1008 */ 	var ne = new_bounds.getNorthEast();
/* 1009 */ 	var sw = new_bounds.getSouthWest();
/* 1010 */ 	if(query_string!=''){
/* 1011 */ 		url_data = 'sw_lat='+sw.lat()+"&ne_lat="+ne.lat()+"&sw_lng="+sw.lng()+"&ne_lng="+ne.lng()+"&"+query_string;
/* 1012 */ 	}else{
/* 1013 */ 		url_data = 'sw_lat='+sw.lat()+"&ne_lat="+ne.lat()+"&sw_lng="+sw.lng()+"&ne_lng="+ne.lng();
/* 1014 */ 	}
/* 1015 */ 	
/* 1016 */ 	
/* 1017 */ 	var from_data=jQuery(".tmpl_filter_results").serialize();	
/* 1018 */ 	jQuery('.search_result_listing').addClass('loading_results');
/* 1019 */ 	
/* 1020 */ 	var opr=(ajaxUrl.indexOf("?")!='-1')? '&' :'?';
/* 1021 */ 	search_map_ajax =jQuery.ajax({
/* 1022 */ 		url:ajaxUrl+opr+'action=search_map_ajax&'+url_data+'&'+from_data,
/* 1023 */ 		type:'POST',
/* 1024 */ 		async: true,
/* 1025 */ 		data:from_data,
/* 1026 */ 		beforeSend : function(){
/* 1027 */ 			if(search_map_ajax != null){
/* 1028 */ 				search_map_ajax.abort();
/* 1029 */ 			}
/* 1030 */         },
/* 1031 */ 		success:function(results){			
/* 1032 */ 			jQuery('.search_result_listing').removeClass('loading_results');
/* 1033 */ 			jQuery('.search_result_listing').html(results);
/* 1034 */ 		}
/* 1035 */ 	});
/* 1036 */ 	
/* 1037 */ 	/* Call Google Map markers data */
/* 1038 */ 	//jQuery('.listing_google_map').addClass('loading_map_markers');
/* 1039 */ 	setTimeout(function(){
/* 1040 */ 	data_map =jQuery.ajax({
/* 1041 */ 		url:ajaxUrl+opr+'action=search_map_ajax&data_map=1&'+url_data+'&'+from_data,
/* 1042 */ 		type:'POST',
/* 1043 */ 		async: true,
/* 1044 */ 		data:from_data,
/* 1045 */ 		dataType: 'json',
/* 1046 */ 		beforeSend : function(){
/* 1047 */ 			if(data_map != null){
/* 1048 */ 				data_map.abort();
/* 1049 */ 			}
/* 1050 */         },

/* markermanager.js */

/* 1051 */ 		success:function(results){
/* 1052 */ 			//jQuery('.listing_google_map').removeClass('loading_map_markers');
/* 1053 */ 			googlemaplisting_deleteMarkers();
/* 1054 */ 			markers=results.markers;
/* 1055 */ 			templ_add_googlemap_markers(markers);
/* 1056 */ 		},
/* 1057 */ 	});
/* 1058 */ 	}, 200);
/* 1059 */ }
