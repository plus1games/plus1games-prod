
/* search-autocomplete.min.js */

/* 1  */ ! function(a) {
/* 2  */     a(function() {
/* 3  */         var b = a.extend({
/* 4  */             fieldName: "#s",
/* 5  */             maxRows: 10,
/* 6  */             minLength: 4
/* 7  */         }, SearchAutocomplete);
/* 8  */         b.fieldName = a("<div />").html(b.fieldName).text(), a(b.fieldName).autocomplete({
/* 9  */             source: function(c, d) {
/* 10 */ 				var p1gSearchCat = jQuery('input[name=p1g_search_cat]:checked').val();
/* 11 */ 				if(p1gSearchCat == 'Games') {
/* 12 */ 					var postType = 'event';
/* 13 */ 					var taxonomyType = null;
/* 14 */ 				} else {
/* 15 */ 					var postType = 'listing';
/* 16 */ 					
/* 17 */ 					if (p1gSearchCat == 'Places') {
/* 18 */ 						var taxonomyType = 'places';
/* 19 */ 					} else if (p1gSearchCat == 'Sport Groups') {
/* 20 */ 						var taxonomyType = 'sport-groups';
/* 21 */ 					} else if (p1gSearchCat == 'Sporting Goods') {
/* 22 */ 						var taxonomyType = 'sporting-goods';
/* 23 */ 					}
/* 24 */ 				}
/* 25 */ 				
/* 26 */                 a.ajax({
/* 27 */                     url: b.ajaxurl,
/* 28 */                     dataType: "json",
/* 29 */                     data: {
/* 30 */                         action: "autocompleteCallback",
/* 31 */                         term: this.term,
/* 32 */ 					    post_type: postType,
/* 33 */ 					    taxonomy: taxonomyType
/* 34 */                     },
/* 35 */                     success: function(b) {
/* 36 */                         d(a.map(b.results, function(a) {
/* 37 */                             return {
/* 38 */                                 label: a.title,
/* 39 */                                 value: a.title,
/* 40 */                                 url: a.url
/* 41 */                             }
/* 42 */                         }))
/* 43 */                     },
/* 44 */                     error: function(a, b, c) {}
/* 45 */                 })
/* 46 */             },
/* 47 */             delay: b.delay,
/* 48 */             minLength: b.minLength,
/* 49 */             autoFocus: "true" === b.autoFocus,
/* 50 */             search: function(b) {

/* search-autocomplete.min.js */

/* 51 */                 a(b.currentTarget).addClass("sa_searching")
/* 52 */             },
/* 53 */             create: function() {},
/* 54 */             select: function(a, b) {
/* 55 */                return "#" === b.item.url ? !0 : void(location = b.item.url)
/* 56 */             },
/* 57 */             open: function(b) {
/* 58 */                 var c = a(this).data("uiAutocomplete");
/* 59 */                 c.menu.element.find("a").each(function() {
/* 60 */                     var b = a(this),
/* 61 */                         d = a.trim(c.term).split(" ").join("|");
/* 62 */                     b.html(b.text().replace(new RegExp("(" + d + ")", "gi"), '<span class="sa-found-text">$1</span>'))
/* 63 */                 }), a(b.target).removeClass("sa_searching")
/* 64 */             },
/* 65 */             close: function() {}
/* 66 */         })
/* 67 */     })
/* 68 */ }(jQuery);                   

;
/* cookiechoices.js */

/* 1   */ /*
/* 2   *|  Copyright 2014 Google Inc. All rights reserved.
/* 3   *| 
/* 4   *|  Licensed under the Apache License, Version 2.0 (the "License");
/* 5   *|  you may not use this file except in compliance with the License.
/* 6   *|  You may obtain a copy of the License at
/* 7   *| 
/* 8   *|  http://www.apache.org/licenses/LICENSE-2.0
/* 9   *| 
/* 10  *|  Unless required by applicable law or agreed to in writing, software
/* 11  *|  distributed under the License is distributed on an "AS IS" BASIS,
/* 12  *|  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/* 13  *|  See the License for the specific language governing permissions and
/* 14  *|  limitations under the License.
/* 15  *|  */
/* 16  */ 
/* 17  */ (function(window) {
/* 18  */ 
/* 19  */     if (!!window.cookieChoices) {
/* 20  */         return window.cookieChoices;
/* 21  */     }
/* 22  */ 
/* 23  */     var cookieChoices = (function() {
/* 24  */ 
/* 25  */         var cookieName = 'displayCookieConsent';
/* 26  */         var cookieConsentId = 'cookieChoiceInfo';
/* 27  */         var dismissLinkId = 'cookieChoiceDismiss';
/* 28  */         var dismissIconId = 'cookieChoiceDismissIcon';
/* 29  */ 
/* 30  */         function showCookieBar(data) {
/* 31  */ 
/* 32  */             if (typeof data != 'undefined' && typeof data.linkHref != 'undefined') {
/* 33  */ 
/* 34  */                 data.position = data.position || "bottom";
/* 35  */                 data.language = data.language || "en";
/* 36  */                 data.styles = data.styles || 'position:fixed; width:100%; background-color:#EEEEEE; background-color:rgba(238, 238, 238, 0.9); margin:0; left:0; ' + data.position + ':0; padding:4px; z-index:1000; text-align:center;';
/* 37  */ 
/* 38  */ 
/* 39  */                 switch (data.language) {
/* 40  */ 
/* 41  */                     case "de":
/* 42  */ 
/* 43  */                         data.cookieText = data.cookieText || "Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden. ";
/* 44  */                         data.dismissText = data.dismissText || "OK";
/* 45  */                         data.linkText = data.linkText || "Weitere Informationen";
/* 46  */ 
/* 47  */                         break;
/* 48  */ 
/* 49  */                     case "it":
/* 50  */ 

/* cookiechoices.js */

/* 51  */                         data.cookieText = data.cookieText || "I cookie ci aiutano ad erogare servizi di qualità. Utilizzando i nostri servizi, l'utente accetta le nostre modalità d'uso dei cookie.";
/* 52  */                         data.dismissText = data.dismissText || "OK";
/* 53  */                         data.linkText = data.linkText || "Ulteriori informazioni";
/* 54  */ 
/* 55  */                         break;
/* 56  */ 
/* 57  */                     case "fr":
/* 58  */ 
/* 59  */                         data.cookieText = data.cookieText || "Les cookies nous permettent de vous proposer nos services plus facilement. En utilisant nos services, vous nous donnez expressément votre accord pour exploiter ces cookies.";
/* 60  */                         data.dismissText = data.dismissText || "OK";
/* 61  */                         data.linkText = data.linkText || "En savoir plus";
/* 62  */ 
/* 63  */                         break;
/* 64  */ 
/* 65  */                     case "nl":
/* 66  */ 
/* 67  */                         data.cookieText = data.cookieText || "Cookies helpen ons bij het leveren van onze diensten. Door gebruik te maken van onze diensten, gaat u akkoord met ons gebruik van cookies.";
/* 68  */                         data.dismissText = data.dismissText || "OK";
/* 69  */                         data.linkText = data.linkText || "Meer informatie";
/* 70  */ 
/* 71  */                         break;
/* 72  */ 
/* 73  */                     case "fi":
/* 74  */ 
/* 75  */                         data.cookieText = data.cookieText || "Evästeet auttavat meitä palvelujemme toimituksessa. Käyttämällä palvelujamme hyväksyt evästeiden käytön.";
/* 76  */                         data.dismissText = data.dismissText || "Selvä";
/* 77  */                         data.linkText = data.linkText || "Lisätietoja";
/* 78  */ 
/* 79  */                         break;
/* 80  */ 
/* 81  */                     default:
/* 82  */ 
/* 83  */                         data.cookieText = data.cookieText || "Cookies help us deliver our services. By using our services, you agree to our use of cookies.";
/* 84  */                         data.dismissText = data.dismissText || "Got it";
/* 85  */                         data.linkText = data.linkText || "Learn more";
/* 86  */ 
/* 87  */                 }
/* 88  */ 
/* 89  */                 _showCookieConsent(data.cookieText, data.dismissText, data.linkText, data.linkHref, data.styles, false);
/* 90  */ 
/* 91  */             }
/* 92  */ 
/* 93  */         }
/* 94  */ 
/* 95  */         function _createHeaderElement(cookieText, dismissText, linkText, linkHref, styles) {
/* 96  */             var butterBarStyles = styles;
/* 97  */             var cookieConsentElement = document.createElement('div');
/* 98  */             var wrapper = document.createElement('div');
/* 99  */             wrapper.style.cssText = "padding-right: 50px;";
/* 100 */ 

/* cookiechoices.js */

/* 101 */             cookieConsentElement.id = cookieConsentId;
/* 102 */             cookieConsentElement.style.cssText = butterBarStyles;
/* 103 */ 
/* 104 */             wrapper.appendChild(_createConsentText(cookieText));
/* 105 */             if (!!linkText && !!linkHref) {
/* 106 */                 wrapper.appendChild(_createInformationLink(linkText, linkHref));
/* 107 */             }
/* 108 */             wrapper.appendChild(_createDismissLink(dismissText));
/* 109 */ 
/* 110 */             cookieConsentElement.appendChild(wrapper);
/* 111 */             cookieConsentElement.appendChild(_createDismissIcon());
/* 112 */ 
/* 113 */             return cookieConsentElement;
/* 114 */         }
/* 115 */ 
/* 116 */         function _createDialogElement(cookieText, dismissText, linkText, linkHref) {
/* 117 */             var glassStyle = 'position:fixed;width:100%;height:100%;z-index:999;' +
/* 118 */                 'top:0;left:0;opacity:0.5;filter:alpha(opacity=50);' +
/* 119 */                 'background-color:#ccc;';
/* 120 */             var dialogStyle = 'z-index:1000;position:fixed;left:50%;top:50%';
/* 121 */             var contentStyle = 'position:relative;left:-50%;margin-top:-25%;' +
/* 122 */                 'background-color:#fff;padding:20px;box-shadow:4px 4px 25px #888;';
/* 123 */ 
/* 124 */             var cookieConsentElement = document.createElement('div');
/* 125 */             cookieConsentElement.id = cookieConsentId;
/* 126 */ 
/* 127 */             var glassPanel = document.createElement('div');
/* 128 */             glassPanel.style.cssText = glassStyle;
/* 129 */ 
/* 130 */             var content = document.createElement('div');
/* 131 */             content.style.cssText = contentStyle;
/* 132 */ 
/* 133 */             var dialog = document.createElement('div');
/* 134 */             dialog.style.cssText = dialogStyle;
/* 135 */ 
/* 136 */             var dismissLink = _createDismissLink(dismissText);
/* 137 */             dismissLink.style.display = 'block';
/* 138 */             dismissLink.style.textAlign = 'right';
/* 139 */             dismissLink.style.marginTop = '8px';
/* 140 */ 
/* 141 */             content.appendChild(_createConsentText(cookieText));
/* 142 */             if (!!linkText && !!linkHref) {
/* 143 */                 content.appendChild(_createInformationLink(linkText, linkHref));
/* 144 */             }
/* 145 */             content.appendChild(dismissLink);
/* 146 */             dialog.appendChild(content);
/* 147 */             cookieConsentElement.appendChild(glassPanel);
/* 148 */             cookieConsentElement.appendChild(dialog);
/* 149 */             return cookieConsentElement;
/* 150 */         }

/* cookiechoices.js */

/* 151 */ 
/* 152 */         function _setElementText(element, text) {
/* 153 */             // IE8 does not support textContent, so we should fallback to innerText.
/* 154 */             var supportsTextContent = 'textContent' in document.body;
/* 155 */ 
/* 156 */             if (supportsTextContent) {
/* 157 */                 element.textContent = text;
/* 158 */             } else {
/* 159 */                 element.innerText = text;
/* 160 */             }
/* 161 */         }
/* 162 */ 
/* 163 */         function _createConsentText(cookieText) {
/* 164 */             var consentText = document.createElement('span');
/* 165 */             _setElementText(consentText, cookieText);
/* 166 */             return consentText;
/* 167 */         }
/* 168 */ 
/* 169 */         function _createDismissLink(dismissText) {
/* 170 */             var dismissLink = document.createElement('a');
/* 171 */             _setElementText(dismissLink, dismissText);
/* 172 */             dismissLink.id = dismissLinkId;
/* 173 */             dismissLink.href = '#';
/* 174 */             dismissLink.style.marginLeft = '24px';
/* 175 */             return dismissLink;
/* 176 */         }
/* 177 */ 
/* 178 */         function _createDismissIcon() {
/* 179 */             var dismissIcon = document.createElement('a');
/* 180 */             dismissIcon.id = dismissIconId;
/* 181 */             dismissIcon.href = '#';
/* 182 */             dismissIcon.style.cssText = 'width: 50px; height: 100%; background-size: 20px; display: inline-block; position: absolute; right: 0px; top: 0px; background-position: 34% 50%; background-color: #CCCCCC; background-color: rgba(204, 204, 204, 0.6); background-repeat: no-repeat; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABixJREFUeNrUW2tsVFUQnlZBaRFQUVFbY/0BKD76UBRUfASj1R9GJBoCiVYIMfgg4a9NjBWQHzSS4CMo8QEagqhBajRifCuaIIgVioooKolIRBCotvKo87lz29nZu2Xv7j1n707yZZN795w5M2fuOTNz5pSRHzpf0MC4hHEeYwRjMON4RhdjD+NXxreMDYzNjHbGXpcDK3PUbwXjUsYNjKsYNYyzRdhcaR/jJ0YH4x3GOsZ2SjiNZsyVQffEDChkDWMyozJpFgDTns2YlGVwvzC+Y3zP2MrYwTgopn+EcSLjBMbpjFGiyJHyOyikvy2MpxnLGAeKOePVMpB/Qmbsa0YLYwLjtDz6LmeMYUxjvCprhOXxDePOYgk/k7HLDAiz+iLjWsbAmPnViJVtCVHEK/LeC2E2V5gBdDOWMC70wB87x3T5lPQYdvmwhoaQGfhEzNw3DZNPbL8ay1FZhMtdMLzJfIeY9WYHpp7PpKw3k7JcFtbY6FZGp2LwM+PGBG2/QxjPGCWsjksJEPRv1fEm2aKSSM1GCdg9BhTSYb0x+88YZybcGZslvkUw5icLWe07zMwnXfiA5hhLeDAfD3Gl+eZHUmnRAjV+eJxXRGk8XTU+xJhIpUfYCt8ynungXBqeYzy8ZipdqhbrDWRpyaWR3k7WRQxhk0h3KHkOSICVlerEwcGf/2WMj8hsgMMcg3aFo65na4yTlJVeVn9cmkcSpI3xhGOvb5OE3VHoIuXLdEtWKoNGSTTXI38eE4EBPK43lfJaHQhfLymzYGFujNh+mRrfkrA/zMvVTLIoYLHZe1sdCQ/sjLqtSR9d0n639WkqJWPTI15UvtFdqwMlWOGRTarNs693VT8z9IsJEkriRbukpygBSqiXdFocwoOaVF9v6xePqhePxDBrcSghbuFBZ4n5o78/KJWppuMY76uEQlzJjUKU4EL4sM+gMVj9A28JTIfHuHjlowSXwoMestZ+u2wreLDWwfYVRQmuhQddZ5Im9Jh6sNiRA5OLEuo8CG/9HaTS0hyY+xx6cf0pwZfwoKGMbcLnRzJJj1sc+/FWCfMZl3kUPqAvhBeyXfSnYn6Nh+jMKuGgZ+FBHwo/nGj1LoDA5Z5C1PkSbWpF4LzhYk/824Tn4XLxAwI64mkAqwLtK1ovXqgPOqSyRmkp7ys9MK8TUw87Al/oSQFrVc4jLdCY6EF4veChGuS5IijhU5Uh+j/tFTCf4pBpmJMTfPMLPSphoCRVwOc3MjPwsEfh7WrvSwlV1Jf0xcJL9yumK4okvE8l1FPfydEHeHA1pepv8GAjFXiWFoNv3+pYCVNV38iA08mUOjQInJLRRRTeZWYpoOfDskIvqYfTiiy8SyUgd7mZ+hKrvWO6SzF6LQHCu1LCOOX5YgHsrUA7l/GXvEBsUF2A8HElMF0oYZHqZ1E2/ziv42SmsQ6E708JUas/hqrMF3aBjFOvSYoBQuTKiJ13OI7qrBLujtj+XtUW4XDGmWeFESIqAygQx047HYa0gRLeYJwaoR1k25qLbFpLaDAk4gAbJcHhkpryGNcDJv7IesBaaayghUqfqim93qHpWA0mm2xNbYkrYDml1zsc09MtE18gaLSBop/JJ4VmUHqpz7hcG9ZIqBg0frYEhceFDV1GOy9qB/gUjlJp1grhSs42NfaPKM+S3rlm751VAsKPkM9Wl/jlXUpfbgIleFBzEj7zX6rx4hMoOM+JWoHVxhIWUPKqx8Yas0ey9+a4OkfUtMoooa2AoClumqmCuWDmYz/lwiLylFECwt4pRRQcE/CCGdMOyXI5I0SKnYbp654dJvglsynzztJ75OnuEC5DbqTMMz6kmxoc8j2FUifY9tpOl+zzg3ya3xCJFfZR5gUqnLzcQ6m64zgW4fGSwNhOmadJH8uEFI0ukO8w7O7g75SqyYETdT2lEq7D+ukLfnqVeHFTxaLaKfwYrUNc3YKy2HHW9dbKanybOCNh1Cku9h4xW3w2OJ87STI8+D2jn/YkyYylsivtT6IzUiUzgzq83VT4nWG447gh+rg4NbEWY7uu7MY2hcuUKGtFgTLq9IZLCq1CtlaM4bCsHTis3CuK+4HxOeMrMfduFwP8T4ABAECF2S1VopbxAAAAAElFTkSuQmCC);';
/* 183 */             return dismissIcon;
/* 184 */         }
/* 185 */ 
/* 186 */         function _createInformationLink(linkText, linkHref) {
/* 187 */             var infoLink = document.createElement('a');
/* 188 */             _setElementText(infoLink, linkText);
/* 189 */             infoLink.href = linkHref;
/* 190 */             infoLink.target = '_blank';
/* 191 */             infoLink.style.marginLeft = '8px';
/* 192 */             return infoLink;
/* 193 */         }
/* 194 */ 
/* 195 */         function _dismissLinkClick() {
/* 196 */             _saveUserPreference();
/* 197 */             _removeCookieConsent();
/* 198 */             return false;
/* 199 */         }
/* 200 */ 

/* cookiechoices.js */

/* 201 */         function _showCookieConsent(cookieText, dismissText, linkText, linkHref, styles, isDialog) {
/* 202 */             if (_shouldDisplayConsent()) {
/* 203 */                 _removeCookieConsent();
/* 204 */                 var consentElement = (isDialog) ?
/* 205 */                     _createDialogElement(cookieText, dismissText, linkText, linkHref) :
/* 206 */                     _createHeaderElement(cookieText, dismissText, linkText, linkHref, styles);
/* 207 */                 var fragment = document.createDocumentFragment();
/* 208 */                 fragment.appendChild(consentElement);
/* 209 */                 document.body.appendChild(fragment.cloneNode(true));
/* 210 */                 document.getElementById(dismissLinkId).onclick = _dismissLinkClick;
/* 211 */                 document.getElementById(dismissIconId).onclick = _dismissLinkClick;
/* 212 */             }
/* 213 */         }
/* 214 */ 
/* 215 */         function showCookieConsentBar(cookieText, dismissText, linkText, linkHref) {
/* 216 */             _showCookieConsent(cookieText, dismissText, linkText, linkHref, false);
/* 217 */         }
/* 218 */ 
/* 219 */         function showCookieConsentDialog(cookieText, dismissText, linkText, linkHref) {
/* 220 */             _showCookieConsent(cookieText, dismissText, linkText, linkHref, true);
/* 221 */         }
/* 222 */ 
/* 223 */         function _removeCookieConsent() {
/* 224 */             var cookieChoiceElement = document.getElementById(cookieConsentId);
/* 225 */             if (cookieChoiceElement != null) {
/* 226 */                 cookieChoiceElement.parentNode.removeChild(cookieChoiceElement);
/* 227 */             }
/* 228 */         }
/* 229 */ 
/* 230 */         function _saveUserPreference() {
/* 231 */             // Set the cookie expiry to one year after today.
/* 232 */             var expiryDate = new Date();
/* 233 */             expiryDate.setFullYear(expiryDate.getFullYear() + 1);
/* 234 */             document.cookie = cookieName + '=y;path=/; expires=' + expiryDate.toGMTString();
/* 235 */         }
/* 236 */ 
/* 237 */         function _shouldDisplayConsent() {
/* 238 */             // Display the header only if the cookie has not been set.
/* 239 */             return !document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
/* 240 */         }
/* 241 */ 
/* 242 */         var exports = {};
/* 243 */         exports.showCookieBar = showCookieBar;
/* 244 */         exports.showCookieConsentBar = showCookieConsentBar;
/* 245 */         exports.showCookieConsentDialog = showCookieConsentDialog;
/* 246 */         return exports;
/* 247 */     })();
/* 248 */ 
/* 249 */     window.cookieChoices = cookieChoices;
/* 250 */     return cookieChoices;

/* cookiechoices.js */

/* 251 */ })(this);
/* 252 */ 

;
/* _supreme.min.js */

/* 1 */ function PlaceholderFormSubmit(e){for(var t=0;t<e.elements.length;t++){var n=e.elements[t];HandlePlaceholderItemSubmit(n)}}function HandlePlaceholderItemSubmit(e){if(e.name){var t=e.getAttribute("placeholder");t&&t.length>0&&e.value===t&&(e.value="",window.setTimeout(function(){e.value=t},100))}}function ReplaceWithText(e){if(!_placeholderSupport){var t=document.createElement("input");t.type="password",t.id=e.id,t.name=e.name,t.className=e.className;for(var n=0;n<e.attributes.length;n++){var i=e.attributes.item(n).nodeName,s=e.attributes.item(n).nodeValue;"type"!==i&&"name"!==i&&t.setAttribute(i,s)}t.originalTextbox=e,e.parentNode.replaceChild(t,e),HandlePlaceholder(t),_placeholderSupport||(e.onblur=function(){this.dummyTextbox&&0===this.value.length&&this.parentNode.replaceChild(this.dummyTextbox,this)})}}function HandlePlaceholder(e){if(_placeholderSupport)Debug("browser has native support for placeholder");else{var t=e.getAttribute("placeholder");t&&t.length>0?(Debug("Placeholder found for input box '"+e.name+"': "+t),e.value=t,e.setAttribute("old_color",e.style.color),e.style.color="#c0c0c0",e.onfocus=function(){var e=this;this.originalTextbox&&(e=this.originalTextbox,e.dummyTextbox=this,this.parentNode.replaceChild(this.originalTextbox,this),e.focus()),Debug("input box '"+e.name+"' focus"),e.style.color=e.getAttribute("old_color"),e.value===t&&(e.value="")},e.onblur=function(){var e=this;Debug("input box '"+e.name+"' blur"),""===e.value&&(e.style.color="#c0c0c0",e.value=t)}):Debug("input box '"+e.name+"' does not have placeholder attribute")}}function Debug(e){if("undefined"!=typeof _debug&&_debug){var t=document.getElementById("Console");t||(t=document.createElement("div"),t.id="Console",document.body.appendChild(t)),t.innerHTML+=e+"<br />"}}!function(e,t){"use strict";function n(t){e.fn.cycle.debug&&i(t)}function i(){window.console&&console.log&&console.log("[cycle] "+Array.prototype.join.call(arguments," "))}function s(t,n,i){var s=e(t).data("cycle.opts"),a=!!t.cyclePause;a&&s.paused?s.paused(t,s,n,i):!a&&s.resumed&&s.resumed(t,s,n,i)}function a(n,a,o){function c(t,n,s){if(!t&&n===!0){var a=e(s).data("cycle.opts");if(!a)return i("options not found, can not resume"),!1;s.cycleTimeout&&(clearTimeout(s.cycleTimeout),s.cycleTimeout=0),f(a.elements,a,1,!a.backwards)}}if(n.cycleStop===t&&(n.cycleStop=0),(a===t||null===a)&&(a={}),a.constructor==String){switch(a){case"destroy":case"stop":var l=e(n).data("cycle.opts");return l?(n.cycleStop++,n.cycleTimeout&&clearTimeout(n.cycleTimeout),n.cycleTimeout=0,l.elements&&e(l.elements).stop(),e(n).removeData("cycle.opts"),"destroy"==a&&r(n,l),!1):!1;case"toggle":return n.cyclePause=1===n.cyclePause?0:1,c(n.cyclePause,o,n),s(n),!1;case"pause":return n.cyclePause=1,s(n),!1;case"resume":return n.cyclePause=0,c(!1,o,n),s(n),!1;case"prev":case"next":return(l=e(n).data("cycle.opts"))?(e.fn.cycle[a](l),!1):(i('options not found, "prev/next" ignored'),!1);default:a={fx:a}}return a}if(a.constructor==Number){var u=a;return(a=e(n).data("cycle.opts"))?0>u||u>=a.elements.length?(i("invalid slide index: "+u),!1):(a.nextSlide=u,n.cycleTimeout&&(clearTimeout(n.cycleTimeout),n.cycleTimeout=0),"string"==typeof o&&(a.oneTimeFx=o),f(a.elements,a,1,u>=a.currSlide),!1):(i("options not found, can not advance slide"),!1)}return a}function o(t,n){if(!e.support.opacity&&n.cleartype&&t.style.filter)try{t.style.removeAttribute("filter")}catch(i){}}function r(t,n){n.next&&e(n.next).unbind(n.prevNextEvent),n.prev&&e(n.prev).unbind(n.prevNextEvent),(n.pager||n.pagerAnchorBuilder)&&e.each(n.pagerAnchors||[],function(){this.unbind().remove()}),n.pagerAnchors=null,e(t).unbind("mouseenter.cycle mouseleave.cycle"),n.destroy&&n.destroy(n)}function c(n,a,r,c,p){var g,y=e.extend({},e.fn.cycle.defaults,c||{},e.metadata?n.metadata():e.meta?n.data():{}),x=e.isFunction(n.data)?n.data(y.metaAttr):null;x&&(y=e.extend(y,x)),y.autostop&&(y.countdown=y.autostopCount||r.length);var b=n[0];if(n.data("cycle.opts",y),y.$cont=n,y.stopCount=b.cycleStop,y.elements=r,y.before=y.before?[y.before]:[],y.after=y.after?[y.after]:[],!e.support.opacity&&y.cleartype&&y.after.push(function(){o(this,y)}),y.continuous&&y.after.push(function(){f(r,y,0,!y.backwards)}),l(y),e.support.opacity||!y.cleartype||y.cleartypeNoBg||v(a),"static"==n.css("position")&&n.css("position","relative"),y.width&&n.width(y.width),y.height&&"auto"!=y.height&&n.height(y.height),y.startingSlide!==t?(y.startingSlide=parseInt(y.startingSlide,10),y.startingSlide>=r.length||y.startSlide<0?y.startingSlide=0:g=!0):y.startingSlide=y.backwards?r.length-1:0,y.random){y.randomMap=[];for(var w=0;w<r.length;w++)y.randomMap.push(w);if(y.randomMap.sort(function(){return Math.random()-.5}),g)for(var S=0;S<r.length;S++)y.startingSlide==y.randomMap[S]&&(y.randomIndex=S);else y.randomIndex=1,y.startingSlide=y.randomMap[1]}else y.startingSlide>=r.length&&(y.startingSlide=0);y.currSlide=y.startingSlide||0;var T=y.startingSlide;a.css({position:"absolute",top:0,left:0}).hide().each(function(t){var n;n=y.backwards?T?T>=t?r.length+(t-T):T-t:r.length-t:T?t>=T?r.length-(t-T):T-t:r.length-t,e(this).css("z-index",n)}),e(r[T]).css("opacity",1).show(),o(r[T],y),y.fit&&(y.aspect?a.each(function(){var t=e(this),n=y.aspect===!0?t.width()/t.height():y.aspect;y.width&&t.width()!=y.width&&(t.width(y.width),t.height(y.width/n)),y.height&&t.height()<y.height&&(t.height(y.height),t.width(y.height*n))}):(y.width&&a.width(y.width),y.height&&"auto"!=y.height&&a.height(y.height))),!y.center||y.fit&&!y.aspect||a.each(function(){var t=e(this);t.css({"margin-left":y.width?(y.width-t.width())/2+"px":0,"margin-top":y.height?(y.height-t.height())/2+"px":0})}),!y.center||y.fit||y.slideResize||a.each(function(){var t=e(this);t.css({"margin-left":y.width?(y.width-t.width())/2+"px":0,"margin-top":y.height?(y.height-t.height())/2+"px":0})});var I=(y.containerResize||y.containerResizeHeight)&&!n.innerHeight();if(I){for(var C=0,A=0,B=0;B<r.length;B++){var O=e(r[B]),P=O[0],k=O.outerWidth(),N=O.outerHeight();k||(k=P.offsetWidth||P.width||O.attr("width")),N||(N=P.offsetHeight||P.height||O.attr("height")),C=k>C?k:C,A=N>A?N:A}y.containerResize&&C>0&&A>0&&n.css({width:C+"px",height:A+"px"}),y.containerResizeHeight&&A>0&&n.css({height:A+"px"})}var W=!1;if(y.pause&&n.bind("mouseenter.cycle",function(){W=!0,this.cyclePause++,s(b,!0)}).bind("mouseleave.cycle",function(){W&&this.cyclePause--,s(b,!0)}),u(y)===!1)return!1;var M=!1;if(c.requeueAttempts=c.requeueAttempts||0,a.each(function(){var t=e(this);if(this.cycleH=y.fit&&y.height?y.height:t.height()||this.offsetHeight||this.height||t.attr("height")||0,this.cycleW=y.fit&&y.width?y.width:t.width()||this.offsetWidth||this.width||t.attr("width")||0,t.is("img")){var n=e.browser.msie&&28==this.cycleW&&30==this.cycleH&&!this.complete,s=e.browser.mozilla&&34==this.cycleW&&19==this.cycleH&&!this.complete,a=e.browser.opera&&(42==this.cycleW&&19==this.cycleH||37==this.cycleW&&17==this.cycleH)&&!this.complete,o=0===this.cycleH&&0===this.cycleW&&!this.complete;if(n||s||a||o){if(p.s&&y.requeueOnImageNotLoaded&&++c.requeueAttempts<100)return i(c.requeueAttempts," - img slide not loaded, requeuing slideshow: ",this.src,this.cycleW,this.cycleH),setTimeout(function(){e(p.s,p.c).cycle(c)},y.requeueTimeout),M=!0,!1;i("could not determine size of image: "+this.src,this.cycleW,this.cycleH)}}return!0}),M)return!1;if(y.cssBefore=y.cssBefore||{},y.cssAfter=y.cssAfter||{},y.cssFirst=y.cssFirst||{},y.animIn=y.animIn||{},y.animOut=y.animOut||{},a.not(":eq("+T+")").css(y.cssBefore),e(a[T]).css(y.cssFirst),y.timeout){y.timeout=parseInt(y.timeout,10),y.speed.constructor==String&&(y.speed=e.fx.speeds[y.speed]||parseInt(y.speed,10)),y.sync||(y.speed=y.speed/2);for(var H="none"==y.fx?0:"shuffle"==y.fx?500:250;y.timeout-y.speed<H;)y.timeout+=y.speed}if(y.easing&&(y.easeIn=y.easeOut=y.easing),y.speedIn||(y.speedIn=y.speed),y.speedOut||(y.speedOut=y.speed),y.slideCount=r.length,y.currSlide=y.lastSlide=T,y.random?(++y.randomIndex==r.length&&(y.randomIndex=0),y.nextSlide=y.randomMap[y.randomIndex]):y.nextSlide=y.backwards?0===y.startingSlide?r.length-1:y.startingSlide-1:y.startingSlide>=r.length-1?0:y.startingSlide+1,!y.multiFx){var _=e.fn.cycle.transitions[y.fx];if(e.isFunction(_))_(n,a,y);else if("custom"!=y.fx&&!y.multiFx)return i("unknown transition: "+y.fx,"; slideshow terminating"),!1}var j=a[T];return y.skipInitializationCallbacks||(y.before.length&&y.before[0].apply(j,[j,j,y,!0]),y.after.length&&y.after[0].apply(j,[j,j,y,!0])),y.next&&e(y.next).bind(y.prevNextEvent,function(){return h(y,1)}),y.prev&&e(y.prev).bind(y.prevNextEvent,function(){return h(y,0)}),(y.pager||y.pagerAnchorBuilder)&&m(r,y),d(y,r),y}function l(t){t.original={before:[],after:[]},t.original.cssBefore=e.extend({},t.cssBefore),t.original.cssAfter=e.extend({},t.cssAfter),t.original.animIn=e.extend({},t.animIn),t.original.animOut=e.extend({},t.animOut),e.each(t.before,function(){t.original.before.push(this)}),e.each(t.after,function(){t.original.after.push(this)})}function u(t){var s,a,o=e.fn.cycle.transitions;if(t.fx.indexOf(",")>0){for(t.multiFx=!0,t.fxs=t.fx.replace(/\s*/g,"").split(","),s=0;s<t.fxs.length;s++){var r=t.fxs[s];a=o[r],a&&o.hasOwnProperty(r)&&e.isFunction(a)||(i("discarding unknown transition: ",r),t.fxs.splice(s,1),s--)}if(!t.fxs.length)return i("No valid transitions named; slideshow terminating."),!1}else if("all"==t.fx){t.multiFx=!0,t.fxs=[];for(var c in o)o.hasOwnProperty(c)&&(a=o[c],o.hasOwnProperty(c)&&e.isFunction(a)&&t.fxs.push(c))}if(t.multiFx&&t.randomizeEffects){var l=Math.floor(20*Math.random())+30;for(s=0;l>s;s++){var u=Math.floor(Math.random()*t.fxs.length);t.fxs.push(t.fxs.splice(u,1)[0])}n("randomized fx sequence: ",t.fxs)}return!0}function d(t,n){t.addSlide=function(i,s){var a=e(i),o=a[0];t.autostopCount||t.countdown++,n[s?"unshift":"push"](o),t.els&&t.els[s?"unshift":"push"](o),t.slideCount=n.length,t.random&&(t.randomMap.push(t.slideCount-1),t.randomMap.sort(function(){return Math.random()-.5})),a.css("position","absolute"),a[s?"prependTo":"appendTo"](t.$cont),s&&(t.currSlide++,t.nextSlide++),e.support.opacity||!t.cleartype||t.cleartypeNoBg||v(a),t.fit&&t.width&&a.width(t.width),t.fit&&t.height&&"auto"!=t.height&&a.height(t.height),o.cycleH=t.fit&&t.height?t.height:a.height(),o.cycleW=t.fit&&t.width?t.width:a.width(),a.css(t.cssBefore),(t.pager||t.pagerAnchorBuilder)&&e.fn.cycle.createPagerAnchor(n.length-1,o,e(t.pager),n,t),e.isFunction(t.onAddSlide)?t.onAddSlide(a):a.hide()}}function f(i,s,a,o){function r(){{var e=0;s.timeout}s.timeout&&!s.continuous?(e=p(i[s.currSlide],i[s.nextSlide],s,o),"shuffle"==s.fx&&(e-=s.speedOut)):s.continuous&&c.cyclePause&&(e=10),e>0&&(c.cycleTimeout=setTimeout(function(){f(i,s,0,!s.backwards)},e))}var c=s.$cont[0],l=i[s.currSlide],u=i[s.nextSlide];if(a&&s.busy&&s.manualTrump&&(n("manualTrump in go(), stopping active transition"),e(i).stop(!0,!0),s.busy=0,clearTimeout(c.cycleTimeout)),s.busy)return void n("transition active, ignoring new tx request");if(c.cycleStop==s.stopCount&&(0!==c.cycleTimeout||a)){if(!a&&!c.cyclePause&&!s.bounce&&(s.autostop&&--s.countdown<=0||s.nowrap&&!s.random&&s.nextSlide<s.currSlide))return void(s.end&&s.end(s));var d=!1;if(!a&&c.cyclePause||s.nextSlide==s.currSlide)r();else{d=!0;var h=s.fx;l.cycleH=l.cycleH||e(l).height(),l.cycleW=l.cycleW||e(l).width(),u.cycleH=u.cycleH||e(u).height(),u.cycleW=u.cycleW||e(u).width(),s.multiFx&&(o&&(s.lastFx===t||++s.lastFx>=s.fxs.length)?s.lastFx=0:!o&&(s.lastFx===t||--s.lastFx<0)&&(s.lastFx=s.fxs.length-1),h=s.fxs[s.lastFx]),s.oneTimeFx&&(h=s.oneTimeFx,s.oneTimeFx=null),e.fn.cycle.resetState(s,h),s.before.length&&e.each(s.before,function(e,t){c.cycleStop==s.stopCount&&t.apply(u,[l,u,s,o])});var m=function(){s.busy=0,e.each(s.after,function(e,t){c.cycleStop==s.stopCount&&t.apply(u,[l,u,s,o])}),c.cycleStop||r()};n("tx firing("+h+"); currSlide: "+s.currSlide+"; nextSlide: "+s.nextSlide),s.busy=1,s.fxFn?s.fxFn(l,u,s,m,o,a&&s.fastOnEvent):e.isFunction(e.fn.cycle[s.fx])?e.fn.cycle[s.fx](l,u,s,m,o,a&&s.fastOnEvent):e.fn.cycle.custom(l,u,s,m,o,a&&s.fastOnEvent)}if(d||s.nextSlide==s.currSlide){var v;s.lastSlide=s.currSlide,s.random?(s.currSlide=s.nextSlide,++s.randomIndex==i.length&&(s.randomIndex=0,s.randomMap.sort(function(){return Math.random()-.5})),s.nextSlide=s.randomMap[s.randomIndex],s.nextSlide==s.currSlide&&(s.nextSlide=s.currSlide==s.slideCount-1?0:s.currSlide+1)):s.backwards?(v=s.nextSlide-1<0,v&&s.bounce?(s.backwards=!s.backwards,s.nextSlide=1,s.currSlide=0):(s.nextSlide=v?i.length-1:s.nextSlide-1,s.currSlide=v?0:s.nextSlide+1)):(v=s.nextSlide+1==i.length,v&&s.bounce?(s.backwards=!s.backwards,s.nextSlide=i.length-2,s.currSlide=i.length-1):(s.nextSlide=v?0:s.nextSlide+1,s.currSlide=v?i.length-1:s.nextSlide-1))}d&&s.pager&&s.updateActivePagerLink(s.pager,s.currSlide,s.activePagerClass)}}function p(e,t,i,s){if(i.timeoutFn){for(var a=i.timeoutFn.call(e,e,t,i,s);"none"!=i.fx&&a-i.speed<250;)a+=i.speed;if(n("calculated timeout: "+a+"; speed: "+i.speed),a!==!1)return a}return i.timeout}function h(t,n){var i=n?1:-1,s=t.elements,a=t.$cont[0],o=a.cycleTimeout;if(o&&(clearTimeout(o),a.cycleTimeout=0),t.random&&0>i)t.randomIndex--,-2==--t.randomIndex?t.randomIndex=s.length-2:-1==t.randomIndex&&(t.randomIndex=s.length-1),t.nextSlide=t.randomMap[t.randomIndex];else if(t.random)t.nextSlide=t.randomMap[t.randomIndex];else if(t.nextSlide=t.currSlide+i,t.nextSlide<0){if(t.nowrap)return!1;t.nextSlide=s.length-1}else if(t.nextSlide>=s.length){if(t.nowrap)return!1;t.nextSlide=0}var r=t.onPrevNextEvent||t.prevNextClick;return e.isFunction(r)&&r(i>0,t.nextSlide,s[t.nextSlide]),f(s,t,1,n),!1}function m(t,n){var i=e(n.pager);e.each(t,function(s,a){e.fn.cycle.createPagerAnchor(s,a,i,t,n)}),n.updateActivePagerLink(n.pager,n.startingSlide,n.activePagerClass)}function v(t){function i(e){return e=parseInt(e,10).toString(16),e.length<2?"0"+e:e}function s(t){for(;t&&"html"!=t.nodeName.toLowerCase();t=t.parentNode){var n=e.css(t,"background-color");if(n&&n.indexOf("rgb")>=0){var s=n.match(/\d+/g);return"#"+i(s[0])+i(s[1])+i(s[2])}if(n&&"transparent"!=n)return n}return"#ffffff"}n("applying clearType background-color hack"),t.each(function(){e(this).css("background-color",s(this))})}var g="2.9999.6";e.support===t&&(e.support={opacity:!e.browser.msie}),e.expr[":"].paused=function(e){return e.cyclePause},e.fn.cycle=function(t,s){var o={s:this.selector,c:this.context};return 0===this.length&&"stop"!=t?!e.isReady&&o.s?(i("DOM not ready, queuing slideshow"),e(function(){e(o.s,o.c).cycle(t,s)}),this):(i("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this):this.each(function(){var r=a(this,t,s);if(r!==!1){r.updateActivePagerLink=r.updateActivePagerLink||e.fn.cycle.updateActivePagerLink,this.cycleTimeout&&clearTimeout(this.cycleTimeout),this.cycleTimeout=this.cyclePause=0,this.cycleStop=0;var l=e(this),u=r.slideExpr?e(r.slideExpr,this):l.children(),d=u.get();if(d.length<2)return void i("terminating; too few slides: "+d.length);var h=c(l,u,d,r,o);if(h!==!1){var m=h.continuous?10:p(d[h.currSlide],d[h.nextSlide],h,!h.backwards);m&&(m+=h.delay||0,10>m&&(m=10),n("first timeout: "+m),this.cycleTimeout=setTimeout(function(){f(d,h,0,!r.backwards)},m))}}})},e.fn.cycle.resetState=function(t,n){n=n||t.fx,t.before=[],t.after=[],t.cssBefore=e.extend({},t.original.cssBefore),t.cssAfter=e.extend({},t.original.cssAfter),t.animIn=e.extend({},t.original.animIn),t.animOut=e.extend({},t.original.animOut),t.fxFn=null,e.each(t.original.before,function(){t.before.push(this)}),e.each(t.original.after,function(){t.after.push(this)});var i=e.fn.cycle.transitions[n];e.isFunction(i)&&i(t.$cont,e(t.elements),t)},e.fn.cycle.updateActivePagerLink=function(t,n,i){e(t).each(function(){e(this).children().removeClass(i).eq(n).addClass(i)})},e.fn.cycle.next=function(e){h(e,1)},e.fn.cycle.prev=function(e){h(e,0)},e.fn.cycle.createPagerAnchor=function(t,i,a,o,r){var c;if(e.isFunction(r.pagerAnchorBuilder)?(c=r.pagerAnchorBuilder(t,i),n("pagerAnchorBuilder("+t+", el) returned: "+c)):c='<a href="#">'+(t+1)+"</a>",c){var l=e(c);if(0===l.parents("body").length){var u=[];a.length>1?(a.each(function(){var t=l.clone(!0);e(this).append(t),u.push(t[0])}),l=e(u)):l.appendTo(a)}r.pagerAnchors=r.pagerAnchors||[],r.pagerAnchors.push(l);var d=function(n){n.preventDefault(),r.nextSlide=t;var i=r.$cont[0],s=i.cycleTimeout;s&&(clearTimeout(s),i.cycleTimeout=0);var a=r.onPagerEvent||r.pagerClick;e.isFunction(a)&&a(r.nextSlide,o[r.nextSlide]),f(o,r,1,r.currSlide<t)};/mouseenter|mouseover/i.test(r.pagerEvent)?l.hover(d,function(){}):l.bind(r.pagerEvent,d),/^click/.test(r.pagerEvent)||r.allowPagerClickBubble||l.bind("click.cycle",function(){return!1});var p=r.$cont[0],h=!1;r.pauseOnPagerHover&&l.hover(function(){h=!0,p.cyclePause++,s(p,!0,!0)},function(){h&&p.cyclePause--,s(p,!0,!0)})}},e.fn.cycle.hopsFromLast=function(e,t){var n,i=e.lastSlide,s=e.currSlide;return n=t?s>i?s-i:e.slideCount-i:i>s?i-s:i+e.slideCount-s},e.fn.cycle.commonReset=function(t,n,i,s,a,o){e(i.elements).not(t).hide(),"undefined"==typeof i.cssBefore.opacity&&(i.cssBefore.opacity=1),i.cssBefore.display="block",i.slideResize&&s!==!1&&n.cycleW>0&&(i.cssBefore.width=n.cycleW),i.slideResize&&a!==!1&&n.cycleH>0&&(i.cssBefore.height=n.cycleH),i.cssAfter=i.cssAfter||{},i.cssAfter.display="none",e(t).css("zIndex",i.slideCount+(o===!0?1:0)),e(n).css("zIndex",i.slideCount+(o===!0?0:1))},e.fn.cycle.custom=function(t,n,i,s,a,o){var r=e(t),c=e(n),l=i.speedIn,u=i.speedOut,d=i.easeIn,f=i.easeOut;c.css(i.cssBefore),o&&(l=u="number"==typeof o?o:1,d=f=null);var p=function(){c.animate(i.animIn,l,d,function(){s()})};r.animate(i.animOut,u,f,function(){r.css(i.cssAfter),i.sync||p()}),i.sync&&p()},e.fn.cycle.transitions={fade:function(t,n,i){n.not(":eq("+i.currSlide+")").css("opacity",0),i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i),i.cssBefore.opacity=0}),i.animIn={opacity:1},i.animOut={opacity:0},i.cssBefore={top:0,left:0}}},e.fn.cycle.ver=function(){return g},e.fn.cycle.defaults={activePagerClass:"activeSlide",after:null,allowPagerClickBubble:!1,animIn:null,animOut:null,aspect:!1,autostop:0,autostopCount:0,backwards:!1,before:null,center:null,cleartype:!e.support.opacity,cleartypeNoBg:!1,containerResize:1,containerResizeHeight:0,continuous:0,cssAfter:null,cssBefore:null,delay:0,easeIn:null,easeOut:null,easing:null,end:null,fastOnEvent:0,fit:0,fx:"fade",fxFn:null,height:"auto",manualTrump:!0,metaAttr:"cycle",next:null,nowrap:0,onPagerEvent:null,onPrevNextEvent:null,pager:null,pagerAnchorBuilder:null,pagerEvent:"click.cycle",pause:0,pauseOnPagerHover:0,prev:null,prevNextEvent:"click.cycle",random:0,randomizeEffects:1,requeueOnImageNotLoaded:!0,requeueTimeout:250,rev:0,shuffle:null,skipInitializationCallbacks:!1,slideExpr:null,slideResize:1,speed:1e3,speedIn:null,speedOut:null,startingSlide:t,sync:1,timeout:4e3,timeoutFn:null,updateActivePagerLink:null,width:null}}(jQuery),function(e){"use strict";e.fn.cycle.transitions.none=function(t,n,i){i.fxFn=function(t,n,i,s){e(n).show(),e(t).hide(),s()}},e.fn.cycle.transitions.fadeout=function(t,n,i){n.not(":eq("+i.currSlide+")").css({display:"block",opacity:1}),i.before.push(function(t,n,i,s,a,o){e(t).css("zIndex",i.slideCount+(o!==!0?1:0)),e(n).css("zIndex",i.slideCount+(o!==!0?0:1))}),i.animIn.opacity=1,i.animOut.opacity=0,i.cssBefore.opacity=1,i.cssBefore.display="block",i.cssAfter.zIndex=0},e.fn.cycle.transitions.scrollUp=function(t,n,i){t.css("overflow","hidden"),i.before.push(e.fn.cycle.commonReset);var s=t.height();i.cssBefore.top=s,i.cssBefore.left=0,i.cssFirst.top=0,i.animIn.top=0,i.animOut.top=-s},e.fn.cycle.transitions.scrollDown=function(t,n,i){t.css("overflow","hidden"),i.before.push(e.fn.cycle.commonReset);var s=t.height();i.cssFirst.top=0,i.cssBefore.top=-s,i.cssBefore.left=0,i.animIn.top=0,i.animOut.top=s},e.fn.cycle.transitions.scrollLeft=function(t,n,i){t.css("overflow","hidden"),i.before.push(e.fn.cycle.commonReset);var s=t.width();i.cssFirst.left=0,i.cssBefore.left=s,i.cssBefore.top=0,i.animIn.left=0,i.animOut.left=0-s},e.fn.cycle.transitions.scrollRight=function(t,n,i){t.css("overflow","hidden"),i.before.push(e.fn.cycle.commonReset);var s=t.width();i.cssFirst.left=0,i.cssBefore.left=-s,i.cssBefore.top=0,i.animIn.left=0,i.animOut.left=s},e.fn.cycle.transitions.scrollHorz=function(t,n,i){t.css("overflow","hidden").width(),i.before.push(function(t,n,i,s){i.rev&&(s=!s),e.fn.cycle.commonReset(t,n,i),i.cssBefore.left=s?n.cycleW-1:1-n.cycleW,i.animOut.left=s?-t.cycleW:t.cycleW}),i.cssFirst.left=0,i.cssBefore.top=0,i.animIn.left=0,i.animOut.top=0},e.fn.cycle.transitions.scrollVert=function(t,n,i){t.css("overflow","hidden"),i.before.push(function(t,n,i,s){i.rev&&(s=!s),e.fn.cycle.commonReset(t,n,i),i.cssBefore.top=s?1-n.cycleH:n.cycleH-1,i.animOut.top=s?t.cycleH:-t.cycleH}),i.cssFirst.top=0,i.cssBefore.left=0,i.animIn.top=0,i.animOut.left=0},e.fn.cycle.transitions.slideX=function(t,n,i){i.before.push(function(t,n,i){e(i.elements).not(t).hide(),e.fn.cycle.commonReset(t,n,i,!1,!0),i.animIn.width=n.cycleW}),i.cssBefore.left=0,i.cssBefore.top=0,i.cssBefore.width=0,i.animIn.width="show",i.animOut.width=0},e.fn.cycle.transitions.slideY=function(t,n,i){i.before.push(function(t,n,i){e(i.elements).not(t).hide(),e.fn.cycle.commonReset(t,n,i,!0,!1),i.animIn.height=n.cycleH}),i.cssBefore.left=0,i.cssBefore.top=0,i.cssBefore.height=0,i.animIn.height="show",i.animOut.height=0},e.fn.cycle.transitions.shuffle=function(t,n,i){var s,a=t.css("overflow","visible").width();for(n.css({left:0,top:0}),i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!0,!0)}),i.speedAdjusted||(i.speed=i.speed/2,i.speedAdjusted=!0),i.random=0,i.shuffle=i.shuffle||{left:-a,top:15},i.els=[],s=0;s<n.length;s++)i.els.push(n[s]);for(s=0;s<i.currSlide;s++)i.els.push(i.els.shift());i.fxFn=function(t,n,i,s,a){i.rev&&(a=!a);var o=e(a?t:n);e(n).css(i.cssBefore);var r=i.slideCount;o.animate(i.shuffle,i.speedIn,i.easeIn,function(){for(var n=e.fn.cycle.hopsFromLast(i,a),c=0;n>c;c++)a?i.els.push(i.els.shift()):i.els.unshift(i.els.pop());if(a)for(var l=0,u=i.els.length;u>l;l++)e(i.els[l]).css("z-index",u-l+r);else{var d=e(t).css("z-index");o.css("z-index",parseInt(d,10)+1+r)}o.animate({left:0,top:0},i.speedOut,i.easeOut,function(){e(a?this:t).hide(),s&&s()})})},e.extend(i.cssBefore,{display:"block",opacity:1,top:0,left:0})},e.fn.cycle.transitions.turnUp=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!1),i.cssBefore.top=n.cycleH,i.animIn.height=n.cycleH,i.animOut.width=n.cycleW}),i.cssFirst.top=0,i.cssBefore.left=0,i.cssBefore.height=0,i.animIn.top=0,i.animOut.height=0},e.fn.cycle.transitions.turnDown=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!1),i.animIn.height=n.cycleH,i.animOut.top=t.cycleH}),i.cssFirst.top=0,i.cssBefore.left=0,i.cssBefore.top=0,i.cssBefore.height=0,i.animOut.height=0},e.fn.cycle.transitions.turnLeft=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!0),i.cssBefore.left=n.cycleW,i.animIn.width=n.cycleW}),i.cssBefore.top=0,i.cssBefore.width=0,i.animIn.left=0,i.animOut.width=0},e.fn.cycle.transitions.turnRight=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!0),i.animIn.width=n.cycleW,i.animOut.left=t.cycleW}),e.extend(i.cssBefore,{top:0,left:0,width:0}),i.animIn.left=0,i.animOut.width=0},e.fn.cycle.transitions.zoom=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!1,!0),i.cssBefore.top=n.cycleH/2,i.cssBefore.left=n.cycleW/2,e.extend(i.animIn,{top:0,left:0,width:n.cycleW,height:n.cycleH}),e.extend(i.animOut,{width:0,height:0,top:t.cycleH/2,left:t.cycleW/2})}),i.cssFirst.top=0,i.cssFirst.left=0,i.cssBefore.width=0,i.cssBefore.height=0},e.fn.cycle.transitions.fadeZoom=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!1),i.cssBefore.left=n.cycleW/2,i.cssBefore.top=n.cycleH/2,e.extend(i.animIn,{top:0,left:0,width:n.cycleW,height:n.cycleH})}),i.cssBefore.width=0,i.cssBefore.height=0,i.animOut.opacity=0},e.fn.cycle.transitions.blindX=function(t,n,i){var s=t.css("overflow","hidden").width();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i),i.animIn.width=n.cycleW,i.animOut.left=t.cycleW}),i.cssBefore.left=s,i.cssBefore.top=0,i.animIn.left=0,i.animOut.left=s},e.fn.cycle.transitions.blindY=function(t,n,i){var s=t.css("overflow","hidden").height();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i),i.animIn.height=n.cycleH,i.animOut.top=t.cycleH}),i.cssBefore.top=s,i.cssBefore.left=0,i.animIn.top=0,i.animOut.top=s},e.fn.cycle.transitions.blindZ=function(t,n,i){var s=t.css("overflow","hidden").height(),a=t.width();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i),i.animIn.height=n.cycleH,i.animOut.top=t.cycleH}),i.cssBefore.top=s,i.cssBefore.left=a,i.animIn.top=0,i.animIn.left=0,i.animOut.top=s,i.animOut.left=a},e.fn.cycle.transitions.growX=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!0),i.cssBefore.left=this.cycleW/2,i.animIn.left=0,i.animIn.width=this.cycleW,i.animOut.left=0}),i.cssBefore.top=0,i.cssBefore.width=0},e.fn.cycle.transitions.growY=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!1),i.cssBefore.top=this.cycleH/2,i.animIn.top=0,i.animIn.height=this.cycleH,i.animOut.top=0}),i.cssBefore.height=0,i.cssBefore.left=0},e.fn.cycle.transitions.curtainX=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!1,!0,!0),i.cssBefore.left=n.cycleW/2,i.animIn.left=0,i.animIn.width=this.cycleW,i.animOut.left=t.cycleW/2,i.animOut.width=0}),i.cssBefore.top=0,i.cssBefore.width=0},e.fn.cycle.transitions.curtainY=function(t,n,i){i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!1,!0),i.cssBefore.top=n.cycleH/2,i.animIn.top=0,i.animIn.height=n.cycleH,i.animOut.top=t.cycleH/2,i.animOut.height=0}),i.cssBefore.height=0,i.cssBefore.left=0},e.fn.cycle.transitions.cover=function(t,n,i){var s=i.direction||"left",a=t.css("overflow","hidden").width(),o=t.height();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i),"right"==s?i.cssBefore.left=-a:"up"==s?i.cssBefore.top=o:"down"==s?i.cssBefore.top=-o:i.cssBefore.left=a}),i.animIn.left=0,i.animIn.top=0,i.cssBefore.top=0,i.cssBefore.left=0},e.fn.cycle.transitions.uncover=function(t,n,i){var s=i.direction||"left",a=t.css("overflow","hidden").width(),o=t.height();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!0,!0),"right"==s?i.animOut.left=a:"up"==s?i.animOut.top=-o:"down"==s?i.animOut.top=o:i.animOut.left=-a}),i.animIn.left=0,i.animIn.top=0,i.cssBefore.top=0,i.cssBefore.left=0},e.fn.cycle.transitions.toss=function(t,n,i){var s=t.css("overflow","visible").width(),a=t.height();i.before.push(function(t,n,i){e.fn.cycle.commonReset(t,n,i,!0,!0,!0),i.animOut.left||i.animOut.top?i.animOut.opacity=0:e.extend(i.animOut,{left:2*s,top:-a/2,opacity:0})}),i.cssBefore.left=0,i.cssBefore.top=0,i.animIn.left=0},e.fn.cycle.transitions.wipe=function(t,n,i){var s=t.css("overflow","hidden").width(),a=t.height();i.cssBefore=i.cssBefore||{};var o;if(i.clip)if(/l2r/.test(i.clip))o="rect(0px 0px "+a+"px 0px)";else if(/r2l/.test(i.clip))o="rect(0px "+s+"px "+a+"px "+s+"px)";else if(/t2b/.test(i.clip))o="rect(0px "+s+"px 0px 0px)";else if(/b2t/.test(i.clip))o="rect("+a+"px "+s+"px "+a+"px 0px)";else if(/zoom/.test(i.clip)){var r=parseInt(a/2,10),c=parseInt(s/2,10);o="rect("+r+"px "+c+"px "+r+"px "+c+"px)"}i.cssBefore.clip=i.cssBefore.clip||o||"rect(0px 0px 0px 0px)";var l=i.cssBefore.clip.match(/(\d+)/g),u=parseInt(l[0],10),d=parseInt(l[1],10),f=parseInt(l[2],10),p=parseInt(l[3],10);i.before.push(function(t,n,i){if(t!=n){var o=e(t),r=e(n);e.fn.cycle.commonReset(t,n,i,!0,!0,!1),i.cssAfter.display="block";var c=1,l=parseInt(i.speedIn/13,10)-1;!function h(){var e=u?u-parseInt(c*(u/l),10):0,t=p?p-parseInt(c*(p/l),10):0,n=a>f?f+parseInt(c*((a-f)/l||1),10):a,i=s>d?d+parseInt(c*((s-d)/l||1),10):s;r.css({clip:"rect("+e+"px "+i+"px "+n+"px "+t+"px)"}),c++<=l?setTimeout(h,13):o.css("display","none")}()}}),e.extend(i.cssBefore,{display:"block",opacity:1,top:0,left:0}),i.animIn={left:0},i.animOut={left:0}}}(jQuery),jQuery(document).ready(function(){jQuery(".currentmenu2,.currentmenu2 div").click(function(){jQuery(this).parent().find("ul.mega").slideToggle("slow",function(){})})}),function(e,t){"use strict";var n,i=t.event;i.special.smartresize={setup:function(){t(this).bind("resize",i.special.smartresize.handler)},teardown:function(){t(this).unbind("resize",i.special.smartresize.handler)},handler:function(e,t){var i=this,s=arguments;e.type="smartresize",n&&clearTimeout(n),n=setTimeout(function(){jQuery.event.handle.apply(i,s)},"execAsap"===t?0:100)}},t.fn.smartresize=function(e){return e?this.bind("smartresize",e):this.trigger("smartresize",["execAsap"])},t.Mason=function(e,n){this.element=t(n),this._create(e),this._init()},t.Mason.settings={isResizable:!0,isAnimated:!1,animationOptions:{queue:!1,duration:500},gutterWidth:0,isRTL:!1,isFitWidth:!1,containerStyle:{position:"relative"}},t.Mason.prototype={_filterFindBricks:function(e){var t=this.options.itemSelector;return t?e.filter(t).add(e.find(t)):e},_getBricks:function(e){var t=this._filterFindBricks(e).css({position:"absolute"}).addClass("masonry-brick");return t},_create:function(n){this.options=t.extend(!0,{},t.Mason.settings,n),this.styleQueue=[];var i=this.element[0].style;this.originalStyle={height:i.height||""};var s=this.options.containerStyle;for(var a in s)this.originalStyle[a]=i[a]||"";this.element.css(s),this.horizontalDirection=this.options.isRTL?"right":"left",this.offset={x:parseInt(this.element.css("padding-"+this.horizontalDirection),10),y:parseInt(this.element.css("padding-top"),10)},this.isFluid=this.options.columnWidth&&"function"==typeof this.options.columnWidth;var o=this;setTimeout(function(){o.element.addClass("masonry")},0),this.options.isResizable&&t(e).bind("smartresize.masonry",function(){o.resize()}),this.reloadItems()},_init:function(e){this._getColumns(),this._reLayout(e)},option:function(e){t.isPlainObject(e)&&(this.options=t.extend(!0,this.options,e))},layout:function(e,t){for(var n=0,i=e.length;i>n;n++)this._placeBrick(e[n]);var s={};if(s.height=Math.max.apply(Math,this.colYs),this.options.isFitWidth){var a=0;for(n=this.cols;--n&&0===this.colYs[n];)a++;s.width=(this.cols-a)*this.columnWidth-this.options.gutterWidth}this.styleQueue.push({$el:this.element,style:s});var o,r=this.isLaidOut&&this.options.isAnimated?"animate":"css",c=this.options.animationOptions;for(n=0,i=this.styleQueue.length;i>n;n++)o=this.styleQueue[n],o.$el[r](o.style,c);this.styleQueue=[],t&&t.call(e),this.isLaidOut=!0},_getColumns:function(){var e=this.options.isFitWidth?this.element.parent():this.element,t=e.width();this.columnWidth=this.isFluid?this.options.columnWidth(t):this.options.columnWidth||this.$bricks.outerWidth(!0)||t,this.columnWidth+=this.options.gutterWidth,this.cols=Math.floor((t+this.options.gutterWidth)/this.columnWidth),this.cols=Math.max(this.cols,1)},_placeBrick:function(e){var n,i,s,a,o,r=t(e);if(n=Math.ceil(r.outerWidth(!0)/(this.columnWidth+this.options.gutterWidth)),n=Math.min(n,this.cols),1===n)s=this.colYs;else for(i=this.cols+1-n,s=[],o=0;i>o;o++)a=this.colYs.slice(o,o+n),s[o]=Math.max.apply(Math,a);for(var c=Math.min.apply(Math,s),l=0,u=0,d=s.length;d>u;u++)if(s[u]===c){l=u;break}var f={top:c+this.offset.y};f[this.horizontalDirection]=this.columnWidth*l+this.offset.x,this.styleQueue.push({$el:r,style:f});var p=c+r.outerHeight(!0),h=this.cols+1-d;for(u=0;h>u;u++)this.colYs[l+u]=p},resize:function(){var e=this.cols;this._getColumns(),(this.isFluid||this.cols!==e)&&this._reLayout()},_reLayout:function(e){var t=this.cols;for(this.colYs=[];t--;)this.colYs.push(0);this.layout(this.$bricks,e)},reloadItems:function(){this.$bricks=this._getBricks(this.element.children())},reload:function(e){this.reloadItems(),this._init(e)},appended:function(e,t,n){if(t){this._filterFindBricks(e).css({top:this.element.height()});
/* 2 */ var i=this;setTimeout(function(){i._appended(e,n)},1)}else this._appended(e,n)},_appended:function(e,t){var n=this._getBricks(e);this.$bricks=this.$bricks.add(n),this.layout(n,t)},remove:function(e){this.$bricks=this.$bricks.not(e),e.remove()},destroy:function(){this.$bricks.removeClass("masonry-brick").each(function(){this.style.position="",this.style.top="",this.style.left=""});var n=this.element[0].style;for(var i in this.originalStyle)n[i]=this.originalStyle[i];this.element.unbind(".masonry").removeClass("masonry").removeData("masonry"),t(e).unbind(".masonry")}},t.fn.imagesLoaded=function(e){function n(e){var s=e.target;s.src!==r&&-1===t.inArray(s,c)&&(c.push(s),--o<=0&&(setTimeout(i),a.unbind(".imagesLoaded",n)))}function i(){e.call(s,a)}var s=this,a=s.find("img").add(s.filter("img")),o=a.length,r="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",c=[];return o||i(),a.bind("load.imagesLoaded error.imagesLoaded",n).each(function(){var e=this.src;this.src=r,this.src=e}),s};var s=function(t){e.console&&e.console.error(t)};t.fn.masonry=function(e){if("string"==typeof e){var n=Array.prototype.slice.call(arguments,1);this.each(function(){var i=t.data(this,"masonry");if(i){if(!t.isFunction(i[e])||"_"===e.charAt(0))return void s("no such method '"+e+"' for masonry instance");i[e].apply(i,n)}else s("cannot call methods on masonry prior to initialization; attempted to call method '"+e+"'")})}else this.each(function(){var n=t.data(this,"masonry");n?(n.option(e||{}),n._init()):t.data(this,"masonry",new t.Mason(e,this))});return this}}(window,jQuery),$j=jQuery.noConflict(),$j(document).ready(function(){$j(".hide-if-no-js").show(),$j("div.menu li:has(ul.sub-menu) > a").addClass("with-ul").append('<span class="sub-indicator">&raquo;</span>'),$j("#menu-primary-title").click(function(){$j("#menu-primary .menu").toggleClass("visible")}),$j("#menu-secondary-title").click(function(){$j("#menu-secondary .menu").toggleClass("visible")}),$j("#menu-subsidiary-title").click(function(){$j("#menu-subsidiary .menu").toggleClass("visible")}),$j("#menu-header-primary-title").click(function(){$j("#menu-header-primary .menu").toggleClass("visible")}),$j("#menu-header-secondary-title").click(function(){$j("#menu-header-secondary .menu").toggleClass("visible")}),$j("#menu-header-horizontal-title").click(function(){$j("#menu-header-horizontal .menu").toggleClass("visible")}),$j(".sidebar-wrap").masonry({itemSelector:".widget"})}),jQuery.cookie=function(e,t,n){if("undefined"==typeof t){var i=null;if(document.cookie&&""!=document.cookie)for(var s=document.cookie.split(";"),a=0;a<s.length;a++){var o=jQuery.trim(s[a]);if(o.substring(0,e.length+1)==e+"="){i=decodeURIComponent(o.substring(e.length+1));break}}return i}n=n||{},null===t&&(t="",n.expires=-1);var r="";if(n.expires&&("number"==typeof n.expires||n.expires.toUTCString)){var c;"number"==typeof n.expires?(c=new Date,c.setTime(c.getTime()+24*n.expires*60*60*1e3)):c=n.expires,r="; expires="+c.toUTCString()}var l=n.path?"; path="+n.path:"",u=n.domain?"; domain="+n.domain:"",d=n.secure?"; secure":"";document.cookie=[e,"=",encodeURIComponent(t),r,l,u,d].join("")},jQuery(window).resize(function(){if(jQuery(window).width()>=980){jQuery.cookie("wsize",jQuery(window).width());{jQuery("#woo_shoppingcart_box").attr("id","woo_mob_cart"),jQuery("#woo_shopping_cart").attr("id","woo_mob_shopping_cart")}}else{jQuery.cookie("wsize",jQuery(window).width());{jQuery("#woo_mob_cart").attr("id","woo_shoppingcart_box"),jQuery("#woo_mob_shopping_cart").attr("id","woo_shopping_cart")}}}),jQuery(document).ready(function(){if(jQuery(window).width()>=980){jQuery.cookie("wsize",jQuery(window).width());{jQuery("#woo_shoppingcart_box").attr("id","woo_mob_cart"),jQuery("#woo_shopping_cart").attr("id","woo_mob_shopping_cart")}}}),jQuery("[placeholder]").focus(function(){var e=jQuery(this);e.val()==e.attr("placeholder")&&(e.val(""),e.removeClass("placeholder"))}).blur(function(){var e=jQuery(this);(""==e.val()||e.val()==e.attr("placeholder"))&&e.addClass("placeholder")}).blur().parents("form").submit(function(){jQuery(this).find("[placeholder]").each(function(){var e=jQuery(this);e.val()==e.attr("placeholder")&&e.val("")})}),jQuery(".accordion").on("click","dd",function(){jQuery("dd.active").find(".content").slideUp(400,"swing"),jQuery(this).hasClass("active")||jQuery(this).find(".content").slideToggle(400,"swing")});var _debug=!1,_placeholderSupport=function(){var e=document.createElement("input");return e.type="text","undefined"!=typeof e.placeholder}();window.onload=function(){for(var e=document.getElementsByTagName("input"),t=document.getElementsByTagName("textarea"),n=[],i=0;i<e.length;i++)n.push(e[i]);for(var i=0;i<t.length;i++)n.push(t[i]);for(var i=0;i<n.length;i++){var s=n[i];s.type&&""!=s.type&&"text"!=s.type&&"textarea"!=s.type?"password"==s.type&&ReplaceWithText(s):HandlePlaceholder(s)}if(!_placeholderSupport)for(var i=0;i<document.forms.length;i++){var a=document.forms[i];a.attachEvent?a.attachEvent("onsubmit",function(){PlaceholderFormSubmit(a)}):a.addEventListener&&a.addEventListener("submit",function(){PlaceholderFormSubmit(a)},!1)}},jQuery(document).ready(function(){var e=jQuery(".large-8.columns .header-widget-wrap div").children().length;e>0&&(jQuery(".middle.tab-bar-section").after("<section class='right-small'><a href='#' class='mobile-search'><i class='fa fa-search'></i></a></section>"),jQuery("body").addClass("mobile-header")),jQuery(".mobile-search").click(function(){jQuery(".large-8.columns .header-widget-wrap").slideToggle("medium"),jQuery("#main").toggleClass("overlay-search")}),jQuery("#main.overlay-search").click(function(){jQuery(".large-8.columns #sidebar-header").slideUp("medium"),jQuery("#main").removeClass("overlay-search")})}),function(e){e.flexslider=function(t,n){var i=e(t);i.vars=e.extend({},e.flexslider.defaults,n);var s,a=i.vars.namespace,o=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,r=("ontouchstart"in window||o||window.DocumentTouch&&document instanceof DocumentTouch)&&i.vars.touch,c="click touchend MSPointerUp",l="",u="vertical"===i.vars.direction,d=i.vars.reverse,f=i.vars.itemWidth>0,p="fade"===i.vars.animation,h=""!==i.vars.asNavFor,m={},v=!0;e.data(t,"flexslider",i),m={init:function(){i.animating=!1,i.currentSlide=parseInt(i.vars.startAt?i.vars.startAt:0),isNaN(i.currentSlide)&&(i.currentSlide=0),i.animatingTo=i.currentSlide,i.atEnd=0===i.currentSlide||i.currentSlide===i.last,i.containerSelector=i.vars.selector.substr(0,i.vars.selector.search(" ")),i.slides=e(i.vars.selector,i),i.container=e(i.containerSelector,i),i.count=i.slides.length,i.syncExists=e(i.vars.sync).length>0,"slide"===i.vars.animation&&(i.vars.animation="swing"),i.prop=u?"top":i.vars.rtl?"marginRight":"marginLeft",i.args={},i.manualPause=!1,i.stopped=!1,i.started=!1,i.startTimeout=null,i.transitions=!i.vars.video&&!p&&i.vars.useCSS&&function(){var e=document.createElement("div"),t=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var n in t)if(void 0!==e.style[t[n]])return i.pfx=t[n].replace("Perspective","").toLowerCase(),i.prop="-"+i.pfx+"-transform",!0;return!1}(),""!==i.vars.controlsContainer&&(i.controlsContainer=e(i.vars.controlsContainer).length>0&&e(i.vars.controlsContainer)),""!==i.vars.manualControls&&(i.manualControls=e(i.vars.manualControls).length>0&&e(i.vars.manualControls)),i.vars.randomize&&(i.slides.sort(function(){return Math.round(Math.random())-.5}),i.container.empty().append(i.slides)),i.doMath(),i.setup("init"),i.vars.rtl&&i.addClass("flexslider-rtl"),i.vars.controlNav&&m.controlNav.setup(),i.vars.directionNav&&m.directionNav.setup(),i.vars.keyboard&&(1===e(i.containerSelector).length||i.vars.multipleKeyboard)&&e(document).bind("keyup",function(e){var t=e.keyCode;if(!i.animating&&(39===t||37===t)){var n=i.vars.rtl?37===t?i.getTarget("next"):39===t?i.getTarget("prev"):!1:39===t?i.getTarget("next"):37===t?i.getTarget("prev"):!1;i.flexAnimate(n,i.vars.pauseOnAction)}}),i.vars.mousewheel&&i.bind("mousewheel",function(e,t){e.preventDefault();var n=i.getTarget(0>t?"next":"prev");i.flexAnimate(n,i.vars.pauseOnAction)}),i.vars.pausePlay&&m.pausePlay.setup(),i.vars.slideshow&&i.vars.pauseInvisible&&m.pauseInvisible.init(),i.vars.slideshow&&(i.vars.pauseOnHover&&i.hover(function(){i.manualPlay||i.manualPause||i.pause()},function(){i.manualPause||i.manualPlay||i.stopped||i.play()}),i.vars.pauseInvisible&&m.pauseInvisible.isHidden()||(i.vars.initDelay>0?i.startTimeout=setTimeout(i.play,i.vars.initDelay):i.play())),h&&m.asNav.setup(),r&&i.vars.touch&&m.touch(),(!p||p&&i.vars.smoothHeight)&&e(window).bind("resize orientationchange focus",m.resize),i.find("img").attr("draggable","false"),setTimeout(function(){i.vars.start(i)},200)},asNav:{setup:function(){i.asNav=!0,i.animatingTo=Math.floor(i.currentSlide/i.move),i.currentItem=i.currentSlide,i.slides.removeClass(a+"active-slide").eq(i.currentItem).addClass(a+"active-slide"),o?(t._slider=i,i.slides.each(function(){var t=this;t._gesture=new MSGesture,t._gesture.target=t,t.addEventListener("MSPointerDown",function(e){e.preventDefault(),e.currentTarget._gesture&&e.currentTarget._gesture.addPointer(e.pointerId)},!1),t.addEventListener("MSGestureTap",function(t){t.preventDefault();var n=e(this),s=n.index();e(i.vars.asNavFor).data("flexslider").animating||n.hasClass("active")||(i.direction=i.currentItem<s?"next":"prev",i.flexAnimate(s,i.vars.pauseOnAction,!1,!0,!0))})})):i.slides.click(function(t){t.preventDefault();var n,s=e(this),o=s.index();n=i.vars.rtl?s.offset().right+e(i).scrollLeft():s.offset().left-e(i).scrollLeft(),0>=n&&s.hasClass(a+"active-slide")?i.flexAnimate(i.getTarget("prev"),!0):e(i.vars.asNavFor).data("flexslider").animating||s.hasClass(a+"active-slide")||(i.direction=i.currentItem<o?"next":"prev",i.flexAnimate(o,i.vars.pauseOnAction,!1,!0,!0))})}},controlNav:{setup:function(){i.manualControls?m.controlNav.setupManual():m.controlNav.setupPaging()},setupPaging:function(){var t,n,s="thumbnails"===i.vars.controlNav?"control-thumbs":"control-paging",o=1;if(i.controlNavScaffold=e('<ol class="'+a+"control-nav "+a+s+'"></ol>'),i.pagingCount>1)for(var r=0;r<i.pagingCount;r++){if(n=i.slides.eq(r),t="thumbnails"===i.vars.controlNav?'<img src="'+n.attr("data-thumb")+'"/>':"<a>"+o+"</a>","thumbnails"===i.vars.controlNav&&!0===i.vars.thumbCaptions){var u=n.attr("data-thumbcaption");""!=u&&void 0!=u&&(t+='<span class="'+a+'caption">'+u+"</span>")}i.controlNavScaffold.append("<li>"+t+"</li>"),o++}i.controlsContainer?e(i.controlsContainer).append(i.controlNavScaffold):i.append(i.controlNavScaffold),m.controlNav.set(),m.controlNav.active(),i.controlNavScaffold.delegate("a, img",c,function(t){if(t.preventDefault(),""===l||l===t.type){var n=e(this),s=i.controlNav.index(n);n.hasClass(a+"active")||(i.direction=s>i.currentSlide?"next":"prev",i.flexAnimate(s,i.vars.pauseOnAction))}""===l&&(l=t.type),m.setToClearWatchedEvent()})},setupManual:function(){i.controlNav=i.manualControls,m.controlNav.active(),i.controlNav.bind(c,function(t){if(t.preventDefault(),""===l||l===t.type){var n=e(this),s=i.controlNav.index(n);n.hasClass(a+"active")||(i.direction=s>i.currentSlide?"next":"prev",i.flexAnimate(s,i.vars.pauseOnAction))}""===l&&(l=t.type),m.setToClearWatchedEvent()})},set:function(){var t="thumbnails"===i.vars.controlNav?"img":"a";i.controlNav=e("."+a+"control-nav li "+t,i.controlsContainer?i.controlsContainer:i)},active:function(){i.controlNav.removeClass(a+"active").eq(i.animatingTo).addClass(a+"active")},update:function(t,n){i.pagingCount>1&&"add"===t?i.controlNavScaffold.append(e("<li><a>"+i.count+"</a></li>")):1===i.pagingCount?i.controlNavScaffold.find("li").remove():i.controlNav.eq(n).closest("li").remove(),m.controlNav.set(),i.pagingCount>1&&i.pagingCount!==i.controlNav.length?i.update(n,t):m.controlNav.active()}},directionNav:{setup:function(){var t=e('<ul class="'+a+'direction-nav"><li><a class="'+a+'prev" href="#">'+i.vars.prevText+'</a></li><li><a class="'+a+'next" href="#">'+i.vars.nextText+"</a></li></ul>");i.controlsContainer?(e(i.controlsContainer).append(t),i.directionNav=e("."+a+"direction-nav li a",i.controlsContainer)):(i.append(t),i.directionNav=e("."+a+"direction-nav li a",i)),m.directionNav.update(),i.directionNav.bind(c,function(t){t.preventDefault();var n;(""===l||l===t.type)&&(n=i.getTarget(e(this).hasClass(a+"next")?"next":"prev"),i.flexAnimate(n,i.vars.pauseOnAction)),""===l&&(l=t.type),m.setToClearWatchedEvent()})},update:function(){var e=a+"disabled";1===i.pagingCount?i.directionNav.addClass(e).attr("tabindex","-1"):i.vars.animationLoop?i.directionNav.removeClass(e).removeAttr("tabindex"):0===i.animatingTo?i.directionNav.removeClass(e).filter("."+a+"prev").addClass(e).attr("tabindex","-1"):i.animatingTo===i.last?i.directionNav.removeClass(e).filter("."+a+"next").addClass(e).attr("tabindex","-1"):i.directionNav.removeClass(e).removeAttr("tabindex")}},pausePlay:{setup:function(){var t=e('<div class="'+a+'pauseplay"><a></a></div>');i.controlsContainer?(i.controlsContainer.append(t),i.pausePlay=e("."+a+"pauseplay a",i.controlsContainer)):(i.append(t),i.pausePlay=e("."+a+"pauseplay a",i)),m.pausePlay.update(i.vars.slideshow?a+"pause":a+"play"),i.pausePlay.bind(c,function(t){t.preventDefault(),(""===l||l===t.type)&&(e(this).hasClass(a+"pause")?(i.manualPause=!0,i.manualPlay=!1,i.pause()):(i.manualPause=!1,i.manualPlay=!0,i.play())),""===l&&(l=t.type),m.setToClearWatchedEvent()})},update:function(e){"play"===e?i.pausePlay.removeClass(a+"pause").addClass(a+"play").html(i.vars.playText):i.pausePlay.removeClass(a+"play").addClass(a+"pause").html(i.vars.pauseText)}},touch:function(){function e(e){i.animating?e.preventDefault():(window.navigator.msPointerEnabled||1===e.touches.length)&&(i.pause(),v=u?i.h:i.w,y=Number(new Date),b=e.touches[0].pageX,w=e.touches[0].pageY,m=f&&d&&i.animatingTo===i.last?0:f&&d?i.limit-(i.itemW+i.vars.itemMargin)*i.move*i.animatingTo:f&&i.currentSlide===i.last?i.limit:f?(i.itemW+i.vars.itemMargin)*i.move*i.currentSlide:d?(i.last-i.currentSlide+i.cloneOffset)*v:(i.currentSlide+i.cloneOffset)*v,l=u?w:b,h=u?b:w,t.addEventListener("touchmove",n,!1),t.addEventListener("touchend",s,!1))}function n(e){b=e.touches[0].pageX,w=e.touches[0].pageY,g=u?l-w:l-b,x=u?Math.abs(g)<Math.abs(b-h):Math.abs(g)<Math.abs(w-h);var t=500;(!x||Number(new Date)-y>t)&&(e.preventDefault(),!p&&i.transitions&&(i.vars.animationLoop||(g/=0===i.currentSlide&&0>g||i.currentSlide===i.last&&g>0?Math.abs(g)/v+2:1),i.setProps(m+g,"setTouch")))}function s(){if(t.removeEventListener("touchmove",n,!1),i.animatingTo===i.currentSlide&&!x&&null!==g){var e=d?-g:g,a=i.getTarget(e>0?"next":"prev");i.canAdvance(a)&&(Number(new Date)-y<550&&Math.abs(e)>50||Math.abs(e)>v/2)?i.flexAnimate(a,i.vars.pauseOnAction):p||i.flexAnimate(i.currentSlide,i.vars.pauseOnAction,!0)}t.removeEventListener("touchend",s,!1),l=null,h=null,g=null,m=null}function a(e){e.stopPropagation(),i.animating?e.preventDefault():(i.pause(),t._gesture.addPointer(e.pointerId),S=0,v=u?i.h:i.w,y=Number(new Date),m=f&&d&&i.animatingTo===i.last?0:f&&d?i.limit-(i.itemW+i.vars.itemMargin)*i.move*i.animatingTo:f&&i.currentSlide===i.last?i.limit:f?(i.itemW+i.vars.itemMargin)*i.move*i.currentSlide:d?(i.last-i.currentSlide+i.cloneOffset)*v:(i.currentSlide+i.cloneOffset)*v)}function r(e){e.stopPropagation();var n=e.target._slider;if(n){var i=-e.translationX,s=-e.translationY;return S+=u?s:i,g=S,x=u?Math.abs(S)<Math.abs(-i):Math.abs(S)<Math.abs(-s),e.detail===e.MSGESTURE_FLAG_INERTIA?void setImmediate(function(){t._gesture.stop()}):void((!x||Number(new Date)-y>500)&&(e.preventDefault(),!p&&n.transitions&&(n.vars.animationLoop||(g=S/(0===n.currentSlide&&0>S||n.currentSlide===n.last&&S>0?Math.abs(S)/v+2:1)),n.setProps(m+g,"setTouch"))))}}function c(e){e.stopPropagation();var t=e.target._slider;if(t){if(t.animatingTo===t.currentSlide&&!x&&null!==g){var n=d?-g:g,i=t.getTarget(n>0?"next":"prev");t.canAdvance(i)&&(Number(new Date)-y<550&&Math.abs(n)>50||Math.abs(n)>v/2)?t.flexAnimate(i,t.vars.pauseOnAction):p||t.flexAnimate(t.currentSlide,t.vars.pauseOnAction,!0)}l=null,h=null,g=null,m=null,S=0}}var l,h,m,v,g,y,x=!1,b=0,w=0,S=0;o?(t.style.msTouchAction="none",t._gesture=new MSGesture,t._gesture.target=t,t.addEventListener("MSPointerDown",a,!1),t._slider=i,t.addEventListener("MSGestureChange",r,!1),t.addEventListener("MSGestureEnd",c,!1)):t.addEventListener("touchstart",e,!1)},resize:function(){!i.animating&&i.is(":visible")&&(f||i.doMath(),p?m.smoothHeight():f?(i.slides.width(i.computedW),i.update(i.pagingCount),i.setProps()):u?(i.viewport.height(i.h),i.setProps(i.h,"setTotal")):(i.vars.smoothHeight&&m.smoothHeight(),i.newSlides.width(i.computedW),i.setProps(i.computedW,"setTotal")))},smoothHeight:function(e){if(!u||p){var t=p?i:i.viewport;e?t.animate({height:i.slides.eq(i.animatingTo).height()},e):t.height(i.slides.eq(i.animatingTo).height())}},sync:function(t){var n=e(i.vars.sync).data("flexslider"),s=i.animatingTo;switch(t){case"animate":n.flexAnimate(s,i.vars.pauseOnAction,!1,!0);break;case"play":n.playing||n.asNav||n.play();break;case"pause":n.pause()}},pauseInvisible:{visProp:null,init:function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)e[t]+"Hidden"in document&&(m.pauseInvisible.visProp=e[t]+"Hidden");if(m.pauseInvisible.visProp){var n=m.pauseInvisible.visProp.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(n,function(){m.pauseInvisible.isHidden()?i.startTimeout?clearTimeout(i.startTimeout):i.pause():i.started?i.play():i.vars.initDelay>0?setTimeout(i.play,i.vars.initDelay):i.play()})}},isHidden:function(){return document[m.pauseInvisible.visProp]||!1}},setToClearWatchedEvent:function(){clearTimeout(s),s=setTimeout(function(){l=""},3e3)}},i.flexAnimate=function(t,n,s,o,c){if(i.vars.animationLoop||t===i.currentSlide||(i.direction=t>i.currentSlide?"next":"prev"),h&&1===i.pagingCount&&(i.direction=i.currentItem<t?"next":"prev"),!i.animating&&(i.canAdvance(t,c)||s)&&i.is(":visible")){if(h&&o){var l=e(i.vars.asNavFor).data("flexslider");if(i.atEnd=0===t||t===i.count-1,l.flexAnimate(t,!0,!1,!0,c),i.direction=i.currentItem<t?"next":"prev",l.direction=i.direction,Math.ceil((t+1)/i.visible)-1===i.currentSlide||0===t)return i.currentItem=t,i.slides.removeClass(a+"active-slide").eq(t).addClass(a+"active-slide"),!1;i.currentItem=t,i.slides.removeClass(a+"active-slide").eq(t).addClass(a+"active-slide"),t=Math.floor(t/i.visible)}if(i.animating=!0,i.animatingTo=t,n&&i.pause(),i.vars.before(i),i.syncExists&&!c&&m.sync("animate"),i.vars.controlNav&&m.controlNav.active(),f||i.slides.removeClass(a+"active-slide").eq(t).addClass(a+"active-slide"),i.atEnd=0===t||t===i.last,i.vars.directionNav&&m.directionNav.update(),t===i.last&&(i.vars.end(i),i.vars.animationLoop||i.pause()),p)r?(i.slides.eq(i.currentSlide).css({opacity:0,zIndex:1}),i.slides.eq(t).css({opacity:1,zIndex:2}),i.wrapup(x)):(i.slides.eq(i.currentSlide).css({zIndex:1}).animate({opacity:0},i.vars.animationSpeed,i.vars.easing),i.slides.eq(t).css({zIndex:2}).animate({opacity:1},i.vars.animationSpeed,i.vars.easing,i.wrapup));else{var v,g,y,x=u?i.slides.filter(":first").height():i.computedW;f?(v=i.vars.itemMargin,y=(i.itemW+v)*i.move*i.animatingTo,g=y>i.limit&&1!==i.visible?i.limit:y):g=0===i.currentSlide&&t===i.count-1&&i.vars.animationLoop&&"next"!==i.direction?d?(i.count+i.cloneOffset)*x:0:i.currentSlide===i.last&&0===t&&i.vars.animationLoop&&"prev"!==i.direction?d?0:(i.count+1)*x:d?(i.count-1-t+i.cloneOffset)*x:(t+i.cloneOffset)*x,i.setProps(g,"",i.vars.animationSpeed),i.transitions?(i.vars.animationLoop&&i.atEnd||(i.animating=!1,i.currentSlide=i.animatingTo),i.container.unbind("webkitTransitionEnd transitionend"),i.container.bind("webkitTransitionEnd transitionend",function(){i.wrapup(x)})):i.container.animate(i.args,i.vars.animationSpeed,i.vars.easing,function(){i.wrapup(x)})}i.vars.smoothHeight&&m.smoothHeight(i.vars.animationSpeed)}},i.wrapup=function(e){p||f||(0===i.currentSlide&&i.animatingTo===i.last&&i.vars.animationLoop?i.setProps(e,"jumpEnd"):i.currentSlide===i.last&&0===i.animatingTo&&i.vars.animationLoop&&i.setProps(e,"jumpStart")),i.animating=!1,i.currentSlide=i.animatingTo,i.vars.after(i)},i.animateSlides=function(){!i.animating&&v&&i.flexAnimate(i.getTarget("next"))},i.pause=function(){clearInterval(i.animatedSlides),i.animatedSlides=null,i.playing=!1,i.vars.pausePlay&&m.pausePlay.update("play"),i.syncExists&&m.sync("pause")},i.play=function(){i.playing&&clearInterval(i.animatedSlides),i.animatedSlides=i.animatedSlides||setInterval(i.animateSlides,i.vars.slideshowSpeed),i.started=i.playing=!0,i.vars.pausePlay&&m.pausePlay.update("pause"),i.syncExists&&m.sync("play")},i.stop=function(){i.pause(),i.stopped=!0},i.canAdvance=function(e,t){var n=h?i.pagingCount-1:i.last;return t?!0:h&&i.currentItem===i.count-1&&0===e&&"prev"===i.direction?!0:h&&0===i.currentItem&&e===i.pagingCount-1&&"next"!==i.direction?!1:e!==i.currentSlide||h?i.vars.animationLoop?!0:i.atEnd&&0===i.currentSlide&&e===n&&"next"!==i.direction?!1:i.atEnd&&i.currentSlide===n&&0===e&&"next"===i.direction?!1:!0:!1},i.getTarget=function(e){return i.direction=e,"next"===e?i.currentSlide===i.last?0:i.currentSlide+1:0===i.currentSlide?i.last:i.currentSlide-1},i.setProps=function(e,t,n){var s=function(){var n=e?e:(i.itemW+i.vars.itemMargin)*i.move*i.animatingTo,s=function(){if(f)return"setTouch"===t?e:d&&i.animatingTo===i.last?0:d?i.limit-(i.itemW+i.vars.itemMargin)*i.move*i.animatingTo:i.animatingTo===i.last?i.limit:n;switch(t){case"setTotal":return d?(i.count-1-i.currentSlide+i.cloneOffset)*e:(i.currentSlide+i.cloneOffset)*e;case"setTouch":return d?e:e;case"jumpEnd":return d?e:i.count*e;case"jumpStart":return d?i.count*e:e;default:return e}}();return-1*s+"px"}();i.transitions&&(s=u?"translate3d(0,"+s+",0)":"translate3d("+((i.vars.rtl?-1:1)*parseInt(s)+"px")+",0,0)",n=void 0!==n?n/1e3+"s":"0s",i.container.css("-"+i.pfx+"-transition-duration",n)),i.args[i.prop]=s,(i.transitions||void 0===n)&&i.container.css(i.args)},i.setup=function(t){if(p)i.slides.css(i.vars.rtl?{width:"100%","float":"right",marginLeft:"-100%",position:"relative"}:{width:"100%","float":"left",marginRight:"-100%",position:"relative"}),"init"===t&&(r?i.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+i.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(i.currentSlide).css({opacity:1,zIndex:2}):i.slides.css({opacity:0,display:"block",zIndex:1}).eq(i.currentSlide).css({zIndex:2}).animate({opacity:1},i.vars.animationSpeed,i.vars.easing)),i.vars.smoothHeight&&m.smoothHeight();else{var n,s;"init"===t&&(i.viewport=e('<div class="'+a+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(i).append(i.container),i.cloneCount=0,i.cloneOffset=0,d&&(s=e.makeArray(i.slides).reverse(),i.slides=e(s),i.container.empty().append(i.slides))),i.vars.animationLoop&&!f&&(i.cloneCount=2,i.cloneOffset=1,"init"!==t&&i.container.find(".clone").remove(),i.container.append(i.slides.first().clone().addClass("clone").attr("aria-hidden","true")).prepend(i.slides.last().clone().addClass("clone").attr("aria-hidden","true"))),i.newSlides=e(i.vars.selector,i),n=d?i.count-1-i.currentSlide+i.cloneOffset:i.currentSlide+i.cloneOffset,u&&!f?(i.container.height(200*(i.count+i.cloneCount)+"%").css("position","absolute").width("100%"),setTimeout(function(){i.newSlides.css({display:"block"}),i.doMath(),i.viewport.height(i.h),i.setProps(n*i.h,"init")},"init"===t?100:0)):(i.container.width(200*(i.count+i.cloneCount)+"%"),i.setProps(n*i.computedW,"init"),setTimeout(function(){i.doMath(),i.newSlides.css(i.vars.rtl?{width:i.computedW,"float":"right",display:"block"}:{width:i.computedW,"float":"left",display:"block"}),i.vars.smoothHeight&&m.smoothHeight()},"init"===t?100:0))}f||i.slides.removeClass(a+"active-slide").eq(i.currentSlide).addClass(a+"active-slide")},i.doMath=function(){var e=i.slides.first(),t=i.vars.itemMargin,n=i.vars.minItems,s=i.vars.maxItems;i.w=void 0===i.viewport?i.width():i.viewport.width(),i.h=e.height(),i.boxPadding=e.outerWidth()-e.width(),f?(i.itemT=i.vars.itemWidth+t,i.minW=n?n*i.itemT:i.w,i.maxW=s?s*i.itemT-t:i.w,i.itemW=i.minW>i.w?(i.w-t*(n-1))/n:i.maxW<i.w?(i.w-t*(s-1))/s:i.vars.itemWidth>i.w?i.w:i.vars.itemWidth,i.visible=Math.floor(i.w/i.itemW),i.move=i.vars.move>0&&i.vars.move<i.visible?i.vars.move:i.visible,i.pagingCount=Math.ceil((i.count-i.visible)/i.move+1),i.last=i.pagingCount-1,i.limit=1===i.pagingCount?0:i.vars.itemWidth>i.w?i.itemW*(i.count-1)+t*(i.count-1):(i.itemW+t)*i.count-i.w-t):(i.itemW=i.w,i.pagingCount=i.count,i.last=i.count-1),i.computedW=i.itemW-i.boxPadding},i.update=function(e,t){i.doMath(),f||(e<i.currentSlide?i.currentSlide+=1:e<=i.currentSlide&&0!==e&&(i.currentSlide-=1),i.animatingTo=i.currentSlide),i.vars.controlNav&&!i.manualControls&&("add"===t&&!f||i.pagingCount>i.controlNav.length?m.controlNav.update("add"):("remove"===t&&!f||i.pagingCount<i.controlNav.length)&&(f&&i.currentSlide>i.last&&(i.currentSlide-=1,i.animatingTo-=1),m.controlNav.update("remove",i.last))),i.vars.directionNav&&m.directionNav.update()},i.addSlide=function(t,n){var s=e(t);i.count+=1,i.last=i.count-1,u&&d?void 0!==n?i.slides.eq(i.count-n).after(s):i.container.prepend(s):void 0!==n?i.slides.eq(n).before(s):i.container.append(s),i.update(n,"add"),i.slides=e(i.vars.selector+":not(.clone)",i),i.setup(),i.vars.added(i)},i.removeSlide=function(t){var n=isNaN(t)?i.slides.index(e(t)):t;i.count-=1,i.last=i.count-1,isNaN(t)?e(t,i.slides).remove():u&&d?i.slides.eq(i.last).remove():i.slides.eq(t).remove(),i.doMath(),i.update(n,"remove"),i.slides=e(i.vars.selector+":not(.clone)",i),i.setup(),i.vars.removed(i)},m.init()},e(window).blur(function(){focused=!1}).focus(function(){focused=!0}),e.flexslider.defaults={namespace:"flex-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){},rtl:!1},e.fn.flexslider=function(t){if(void 0===t&&(t={}),"object"==typeof t)return this.each(function(){var n=e(this),i=t.selector?t.selector:".slides > li",s=n.find(i);1===s.length&&t.allowOneSlide===!0||0===s.length?(s.fadeIn(400),t.start&&t.start(n)):void 0===n.data("flexslider")&&new e.flexslider(this,t)});var n=e(this).data("flexslider");switch(t){case"play":n.play();break;case"pause":n.pause();break;case"stop":n.stop();break;case"next":n.flexAnimate(n.getTarget("next"),!0);break;case"prev":case"previous":n.flexAnimate(n.getTarget("prev"),!0);break;default:"number"==typeof t&&n.flexAnimate(t,!0)}}}(jQuery);

;
/* wp-embed.min.js */

/* 1 */ !function(a,b){"use strict";function c(){if(!e){e=!0;var a,c,d,f,g=-1!==navigator.appVersion.indexOf("MSIE 10"),h=!!navigator.userAgent.match(/Trident.*rv:11\./),i=b.querySelectorAll("iframe.wp-embedded-content");for(c=0;c<i.length;c++)if(d=i[c],!d.getAttribute("data-secret")){if(f=Math.random().toString(36).substr(2,10),d.src+="#?secret="+f,d.setAttribute("data-secret",f),g||h)a=d.cloneNode(!0),a.removeAttribute("security"),d.parentNode.replaceChild(a,d)}else;}}var d=!1,e=!1;if(b.querySelector)if(a.addEventListener)d=!0;if(a.wp=a.wp||{},!a.wp.receiveEmbedMessage)if(a.wp.receiveEmbedMessage=function(c){var d=c.data;if(d.secret||d.message||d.value)if(!/[^a-zA-Z0-9]/.test(d.secret)){var e,f,g,h,i,j=b.querySelectorAll('iframe[data-secret="'+d.secret+'"]'),k=b.querySelectorAll('blockquote[data-secret="'+d.secret+'"]');for(e=0;e<k.length;e++)k[e].style.display="none";for(e=0;e<j.length;e++)if(f=j[e],c.source===f.contentWindow){if(f.removeAttribute("style"),"height"===d.message){if(g=parseInt(d.value,10),g>1e3)g=1e3;else if(200>~~g)g=200;f.height=g}if("link"===d.message)if(h=b.createElement("a"),i=b.createElement("a"),h.href=f.getAttribute("src"),i.href=d.value,i.host===h.host)if(b.activeElement===f)a.top.location.href=d.value}else;}},d)a.addEventListener("message",a.wp.receiveEmbedMessage,!1),b.addEventListener("DOMContentLoaded",c,!1),a.addEventListener("load",c,!1)}(window,document);

;
/* tevolution-script.min.js */

/* 1 */ function tmpl_insta_search_widget(e){var r=null,a="";jQuery("."+e+" .searchpost").autocomplete({minLength:0,create:function(){jQuery(this).data("ui-autocomplete")._renderItem=function(e,r){return jQuery("<li>").addClass("instant_search").append("<a>").attr("href",r.url).html(r.label).appendTo(e)}},source:function(t,l){var n="";jQuery("."+e+" input[name^='post_type']").each(function(){n+=jQuery(this).val()+","}),(""==a||""!=t.term)&&(a=t.term);var s=jQuery("form."+e).serialize();r=jQuery.ajax({url:tevolutionajaxUrl,type:"POST",dataType:"json",data:"action=tevolution_autocomplete_callBack&search_text="+a+"&post_type="+n+"&"+s,beforeSend:function(){null!=r&&r.abort()},success:function(e){l(jQuery.map(e.results,function(e){return{label:e.title,value:e.label,url:e.url}}))}})},autoFocus:!1,scroll:!0,select:function(e,r){return"#"===r.item.url?!0:void(location=r.item.url)},open:function(e){var r=jQuery(this).data("uiAutocomplete");r.menu.element.find("a").each(function(){var e=jQuery(this),a=jQuery.trim(r.term).split(" ").join("|");e.html(e.text().replace(new RegExp("("+a+")","gi"),"$1"))}),jQuery(e.target).removeClass("sa_searching")}}).focus(function(){jQuery(this).autocomplete("search","")})}function addToFavourite(e,r){return 0!=current_user?(r="add"==r?"add":"removed",jQuery.ajax({url:ajaxUrl,type:"POST",async:!0,data:"action=tmpl_add_to_favourites&ptype=favorite&action1="+r+"&pid="+e,success:function(r){1==favourites_sort&&(document.getElementById("post-"+e).style.display="none"),jQuery(".fav_"+e).html(r)}}),!1):void 0}function tmpl_registretion_frm(){jQuery("#tmpl_reg_login_container #tmpl_sign_up").show(),jQuery("#tmpl_reg_login_container #tmpl_login_frm").hide()}function tmpl_login_frm(){jQuery("#tmpl_reg_login_container #tmpl_sign_up").hide(),jQuery("#tmpl_reg_login_container #tmpl_login_frm").show()}function tmpl_printpage(){window.print()}function chkemail(e){if(jQuery("#"+e+" #user_email").val())var r=jQuery("#"+e+" #user_email").val();return jQuery("#"+e+" .user_email_spin").remove(),jQuery("#"+e+" input#user_email").css("display","inline"),jQuery("#"+e+" input#user_email").after("<i class='fa fa-circle-o-notch fa-spin user_email_spin ajax-fa-spin'></i>"),chkemailRequest=jQuery.ajax({url:ajaxUrl,type:"POST",async:!0,data:"action=tmpl_ajax_check_user_email&user_email="+r,beforeSend:function(){null!=chkemailRequest&&chkemailRequest.abort()},success:function(r){var a=r.split(",");"email"==a[1]&&(a[0]>0?(jQuery("#"+e+" #user_email_error").html(user_email_error),jQuery("#"+e+" #user_email_already_exist").val(0),jQuery("#"+e+" #user_email_error").removeClass("available_tick"),jQuery("#"+e+" #user_email_error").addClass("message_error2"),reg_email=0):(jQuery("#"+e+" #user_email_error").html(user_email_verified),jQuery("#"+e+" #user_email_already_exist").val(1),jQuery("#"+e+" #user_email_error").addClass("available_tick"),jQuery("#"+e+" #user_email_error").removeClass("message_error2"),reg_email=1)),jQuery("#"+e+" .user_email_spin").remove()}}),!0}function chkname(e){if(jQuery("#"+e+" #user_fname").val())var r=jQuery("#"+e+" #user_fname").val();return jQuery("#"+e+" .user_fname_spin").remove(),jQuery("#"+e+" input#user_fname").css("display","inline"),jQuery("#"+e+" input#user_fname").after("<i class='fa fa-circle-o-notch fa-spin user_fname_spin ajax-fa-spin'></i>"),chknameRequest=jQuery.ajax({url:ajaxUrl,type:"POST",async:!0,data:"action=tmpl_ajax_check_user_email&user_fname="+r,beforeSend:function(){null!=chknameRequest&&chknameRequest.abort()},success:function(r){var a=r.split(",");"fname"==a[1]&&(a[0]>0?(jQuery("#"+e+" #user_fname_error").html(user_fname_error),jQuery("#"+e+" #user_fname_already_exist").val(0),jQuery("#"+e+" #user_fname_error").addClass("message_error2"),jQuery("#"+e+" #user_fname_error").removeClass("available_tick"),reg_name=0):(jQuery("#"+e+" #user_fname_error").html(user_fname_verified),jQuery("#"+e+" #user_fname_already_exist").val(1),jQuery("#"+e+" #user_fname_error").removeClass("message_error2"),jQuery("#"+e+" #user_fname_error").addClass("available_tick"),2==jQuery(""+e+" #userform div").size()&&checkclick&&document.userform.submit(),reg_name=1)),jQuery("#"+e+" .user_fname_spin").remove()}}),!0}function set_login_registration_frm(e){"existing_user"==e?(document.getElementById("login_user_meta").style.display="none",document.getElementById("login_user_frm_id").style.display="",document.getElementById("login_type").value=e,document.getElementById("monetize_preview")&&(document.getElementById("monetize_preview").style.display="none")):(document.getElementById("login_user_meta").style.display="block",document.getElementById("login_user_frm_id").style.display="none",document.getElementById("login_type").value=e,document.getElementById("monetize_preview")&&(document.getElementById("monetize_preview").style.display="block"))}function showNextsubmitStep(){var e="post";jQuery(".step-wrapper").removeClass("current"),jQuery(".content").slideUp(500,function(){"plan"===currentStep&&(1==jQuery("#pkg_type").val()||1==pkg_post?e="post":2==jQuery("#pkg_type").val()&&(jQuery("#step-post").css("display","none"),0===parseInt(jQuery("#step-auth").length)?(jQuery("#select_payment").html("2"),user_login=!0):(jQuery("#span_user_login").html("2"),jQuery("#select_payment").html("3"),user_login=!1),e=user_login?"payment":"auth")),"post"==currentStep&&(user_login=0===parseInt(jQuery("#step-auth").length)?!0:!1,e=user_login?"payment":"auth"),"auth"==currentStep&&user_login&&(e="payment"),jQuery(".step-"+e+"  .content").slideDown(10).end(),jQuery(".step-"+e).addClass("current")})}function tmpl_close_popup(){jQuery(".reveal-modal-bg").click(function(){jQuery(".reveal-modal").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery(".eveal-modal").removeClass("open")})}function tmpl_thousandseperator(e){0==num_decimals&&(e=parseFloat(e).toFixed(2));var r=e.split("."),a=r[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g,"$1"+thousands_sep),t=r[1];return 0==num_decimals?a:a+"."+t}function toggle_post_type(){var e=document.getElementById("toggle_postID");e.style.display="none"==e.style.display?"block":"none","paf_row toggleoff"==document.getElementById("toggle_post_type").getAttribute("class")?jQuery("#toggle_post_type").removeClass("paf_row toggleoff").addClass("paf_row toggleon"):jQuery("#toggle_post_type").removeClass("paf_row toggleon").addClass("paf_row toggleoff"),-1!=document.getElementById("toggle_post_type").getAttribute("class").search("toggleoff")&&-1!=document.getElementById("toggle_post_type").getAttribute("class").search("map_category_fullscreen")&&jQuery("#toggle_post_type").removeClass("paf_row toggleoff map_category_fullscreen").addClass("paf_row toggleon map_category_fullscreen"),-1!=document.getElementById("toggle_post_type").getAttribute("class").search("toggleon")&&-1!=document.getElementById("toggle_post_type").getAttribute("class").search("map_category_fullscreen")&&jQuery("#toggle_post_type").removeClass("paf_row toggleon map_category_fullscreen").addClass("paf_row toggleoff map_category_fullscreen")}var captcha="";jQuery(document).ready(function(){jQuery("input.ui-autocomplete-input").click(function(){jQuery("body").addClass("overlay-dark"),jQuery("input.ui-autocomplete-input").addClass("temp-zindex")}),jQuery(".exit-selection").click(function(){jQuery("body").removeClass("overlay-dark"),jQuery(".ui-widget-content.ui-autocomplete.ui-front").css("display","none"),jQuery("input.ui-autocomplete-input").removeClass("temp-zindex")}),jQuery("html").keydown(function(e){27==e.which&&(jQuery("body").removeClass("overlay-dark"),jQuery(".ui-widget-content.ui-autocomplete.ui-front").css("display","none"),jQuery("input.ui-autocomplete-input").removeClass("temp-zindex"))})}),jQuery("ul.sorting_option").on("click",".init",function(){jQuery(this).closest("ul.sorting_option").children("li:not(.init)").slideToggle(30),jQuery(".exit-sorting").toggle()});var allOptions=jQuery("ul.sorting_option").children("li:not(.init)");jQuery(".exit-sorting").on("click",function(){allOptions.slideUp(30),jQuery(".exit-sorting").css("display","none")}),jQuery("ul.sorting_option").on("click","li:not(.init)",function(){allOptions.removeClass("selected"),jQuery(this).addClass("selected"),jQuery("ul.sorting_option").children(".init").html(jQuery(this).html()),allOptions.slideUp(),jQuery(".exit-sorting").css("display","none")}),jQuery(document).ready(function(){jQuery(".autor_delete_link").click(function(){return confirm(delete_confirm)?(jQuery(this).after("<span class='delete_append'><?php _e('Deleting.','templatic');?></span>"),jQuery(".delete_append").css({margin:"5px","vertical-align":"sub","font-size":"14px"}),setTimeout(function(){jQuery(".delete_append").html(deleting)},700),setTimeout(function(){jQuery(".delete_append").html(deleting)},1400),jQuery.ajax({url:ajaxUrl,type:"POST",data:"action=delete_auth_post&security="+delete_auth_post+"&postId="+jQuery(this).attr("data-deleteid")+"&currUrl="+currUrl,success:function(e){window.location.href=e}}),!1):!1})}),jQuery(document).ready(function(){jQuery(".browse_by_category ul.children").css({display:"none"}),jQuery("ul.browse_by_category li:has(> ul)").addClass("hasChildren"),jQuery("ul.browse_by_category li.hasChildren").mouseenter(function(){return jQuery(this).addClass("heyHover").children("ul").show(),!1}),jQuery("ul.browse_by_category li.hasChildren").mouseleave(function(){return jQuery(this).removeClass("heyHover").children("ul").hide(),!1})}),jQuery(document).ready(function(){function e(){return""==n.val()?(n.addClass("error"),s.text(fullname_error_msg),s.addClass("message_error2"),!1):(n.removeClass("error"),s.text(""),s.removeClass("message_error2"),!0)}function r(){var e=0;if(""==o.val())e=1;else if(""!=o.val()){var r=o.val(),a=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;e=a.test(r)?0:1}return 1==e?(o.addClass("error"),i.text(email_error_msg),i.addClass("message_error2"),!1):(o.removeClass("error"),i.text(""),i.removeClass("message_error"),!0)}function a(){return""==jQuery("#inq_subject").val()?(u.addClass("error"),c.text(subject_error_msg),c.addClass("message_error2"),!1):(u.removeClass("error"),c.text(""),c.removeClass("message_error2"),!0)}function t(){return""==jQuery("#inq_msg").val()?(m.addClass("error"),_.text(comment_error_msg),_.addClass("message_error2"),!1):(m.removeClass("error"),_.text(""),_.removeClass("message_error2"),!0)}var l=jQuery("#inquiry_frm"),n=jQuery("#full_name"),s=jQuery("#full_nameInfo"),o=jQuery("#your_iemail"),i=jQuery("#your_iemailInfo"),u=jQuery("#inq_subject"),c=jQuery("#inq_subInfo"),m=jQuery("#inq_msg"),_=jQuery("#inq_msgInfo");n.blur(e),o.blur(r),u.blur(a),m.blur(t),m.keyup(t),l.submit(function(){if(e()&r()&a()&t()){document.getElementById("process_state").style.display="block";var s=l.serialize();return jQuery.ajax({url:ajaxUrl,type:"POST",data:"action=tevolution_send_inquiry_form&"+s+"&postid="+current_post_id,success:function(e){document.getElementById("process_state").style.display="none",1==e?jQuery("#send_inquiry_msg").html(captcha_invalid_msg):(jQuery("#send_inquiry_msg").html(e),setTimeout(function(){jQuery("#lean_overlay").fadeOut(10),jQuery("#inquiry_div").css({display:"none"}),jQuery("#tmpl_send_inquiry").removeClass("open"),jQuery("#tmpl_send_inquiry").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#inq_subject").val(""),jQuery("#inq_msg").html(""),jQuery("#send_inquiry_msg").html(""),n.val(""),o.val(""),jQuery("#contact_number").val("")},2e3))}}),!1}return!1})}),jQuery(document).ready(function(){function e(){return""==jQuery("#to_name_friend").val()?(s.addClass("error"),o.text(friendname_error_msg),o.addClass("message_error2"),!1):(s.removeClass("error"),o.text(""),o.removeClass("message_error2"),!0)}function r(){var e=0;if(""==i.val())e=1;else if(""!=jQuery("#to_friend_email").val()){var r=jQuery("#to_friend_email").val(),a=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;e=a.test(r)?0:1}return e?(i.addClass("error"),u.text(friendemail_error_msg),u.addClass("message_error2"),!1):(i.removeClass("error"),u.text(""),u.removeClass("message_error"),!0)}function a(){return""==jQuery("#yourname").val()?(c.addClass("error"),m.text(fullname_error_msg),m.addClass("message_error2"),!1):(c.removeClass("error"),m.text(""),m.removeClass("message_error2"),!0)}function t(){var e=0;if(""==jQuery("#youremail").val())e=1;else if(""!=jQuery("#youremail").val()){var r=jQuery("#youremail").val(),a=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;e=a.test(r)?0:1}return e?(_.addClass("error"),d.text(email_error_msg),d.addClass("message_error2"),!1):(_.removeClass("error"),d.text(""),d.removeClass("message_error"),!0)}function l(){return""==jQuery("#frnd_comments").val()?(y.addClass("error"),p.text(friend_comment_error_msg),p.addClass("message_error2"),!1):(y.removeClass("error"),p.text(""),p.removeClass("message_error2"),!0)}var n=jQuery("#send_to_frnd"),s=jQuery("#to_name_friend"),o=jQuery("#to_name_friendInfo"),i=jQuery("#to_friend_email"),u=jQuery("#to_friend_emailInfo"),c=jQuery("#yourname"),m=jQuery("#yournameInfo"),_=jQuery("#youremail"),d=jQuery("#youremailInfo"),y=jQuery("#frnd_comments"),p=jQuery("#frnd_commentsInfo");s.blur(e),i.blur(r),c.blur(a),_.blur(t),y.blur(l),s.keyup(e),i.keyup(r),c.keyup(a),_.keyup(t),y.keyup(l),n.submit(function(){if(e()&r()&a()&t()&l()){{jQuery("#recaptcha_widget_div").html()}document.getElementById("process_send_friend").style.display="block";var o=n.serialize();return jQuery.ajax({url:ajaxUrl,type:"POST",data:"action=tevolution_send_friendto_form&"+o,success:function(e){document.getElementById("process_send_friend").style.display="none",1==e?jQuery("#send_friend_msg").html(captcha_invalid_msg):(jQuery("#send_friend_msg").html(e),setTimeout(function(){jQuery("#lean_overlay").fadeOut(200),jQuery("#tmpl_send_to_frd").removeClass("open"),jQuery("#tmpl_send_to_frd").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#send_friend_msg").html(""),jQuery("#frnd_subject").val(""),jQuery("#frnd_comments").html(""),c.val(""),_.val(""),s.val(""),i.val("")},2e3))}}),!1}return!1})}),jQuery.noConflict();var checkclick=!1,reg_email=0,reg_name=0,chkemailRequest=null,chknameRequest=null;jQuery("form#submit_form #register_form").live("click",function(){1==reg_name&&1==reg_email&&(user_login=!0,currentStep="auth",jQuery("div#step-auth").addClass("complete"),parseFloat(jQuery("#total_price").val())<=0||""==jQuery("#total_price").val()||jQuery("#package_free_submission").val()>0?(jQuery(".wp-editor-container textarea").each(function(){var e=jQuery(this).attr("id");jQuery("<input>").attr({type:"hidden",id:e,name:e,value:tinyMCE.get(e).getContent()}).appendTo("#submit_form")}),jQuery("#submit_form").submit()):(finishStep.push("step-auth"),showNextsubmitStep()))});var chkusernameRequest=null,user_login_name=!1;jQuery("form#loginform #user_login,#login_widget form#loginform #user_login,.login_pop_class form#loginform #user_login").live("keyup",function(e){var r=(jQuery(this).serialize(),jQuery(this).val());chkusernameRequest=jQuery.ajax({type:"POST",dataType:"json",url:ajaxUrl,data:"action=ajaxcheckusername&username="+r,beforeSend:function(){null!=chkusernameRequest&&chkusernameRequest.abort()},success:function(r){var a=jQuery(e.currentTarget);$ul=jQuery(a).next(),1==r?($ul.removeClass("message_error2"),$ul.addClass("available_tick"),$ul.html(user_name_verified),user_login_name=!0):($ul.removeClass("available_tick"),$ul.addClass("message_error2"),$ul.html(user_name_error),user_login_name=!1)}}),e.preventDefault()}),jQuery("form#loginform,form#loginform,form#loginform").submit(function(){if(user_login_name)return!0;var r=(jQuery(this).serialize(),jQuery("form#loginform #user_login,#login_widget form#loginform #user_login,.login_pop_class form#loginform #user_login").val());chkusernameRequest=jQuery.ajax({type:"POST",dataType:"json",url:ajaxUrl,data:"action=ajaxcheckusername&username="+r,beforeSend:function(){null!=chkusernameRequest&&chkusernameRequest.abort()},success:function(r){var a=jQuery(e.currentTarget);return $ul=jQuery(a).next(),1==r?($ul.removeClass("message_error2"),$ul.addClass("available_tick"),$ul.html(user_name_verified),!0):($ul.removeClass("available_tick"),$ul.addClass("message_error2"),$ul.html(user_name_error),!1)}})}),jQuery("form#loginform .lw_fpw_lnk,#login_widget form#loginform .lw_fpw_lnk,.login_pop_class form#loginform .lw_fpw_lnk").live("click",function(e){jQuery(".forgotpassword").show(),e.preventDefault()}),eval(function(e,r,a,t,l,n){if(l=function(e){return(r>e?"":l(parseInt(e/r)))+((e%=r)>35?String.fromCharCode(e+29):e.toString(36))},!"".replace(/^/,String)){for(;a--;)n[l(a)]=t[a]||l(a);t=[function(e){return n[e]}],l=function(){return"\\w+"},a=1}for(;a--;)t[a]&&(e=e.replace(new RegExp("\\b"+l(a)+"\\b","g"),t[a]));return e}(";(6($,g,h,i){l j='1Y',23={3i:'1Y',L:{O:C,E:C,z:C,I:C,p:C,K:C,N:C,B:C},2a:0,18:'',12:'',3:h.3h.1a,x:h.12,1p:'1Y.3d',y:{},1q:0,1w:w,3c:w,3b:w,2o:C,1X:6(){},38:6(){},1P:6(){},26:6(){},8:{O:{3:'',15:C,1j:'37',13:'35-4Y',2p:''},E:{3:'',15:C,R:'1L',11:'4V',H:'',1A:'C',2c:'C',2d:'',1B:'',13:'4R'},z:{3:'',15:C,y:'33',2m:'',16:'',1I:'',13:'35'},I:{3:'',15:C,Q:'4K'},p:{3:'',15:C,1j:'37'},K:{3:'',15:C,11:'1'},N:{3:'',15:C,22:''},B:{3:'',1s:'',1C:'',11:'33'}}},1n={O:\"\",E:\"1D://4J.E.o/4x?q=4u%2X,%4j,%4i,%4h,%4f,%4e,46,%45,%44%42%41%40%2X=%27{3}%27&1y=?\",z:\"S://3W.3P.z.o/1/3D/y.2G?3={3}&1y=?\",I:\"S://3l.I.o/2.0/5a.59?54={3}&Q=1c&1y=?\",p:'S://52.p.o/4Q/2G/4B/m?3={3}&1y=?',K:\"\",N:\"S://1o.N.o/4z/y/L?4r=4o&3={3}&1y=?\",B:\"\"},2A={O:6(b){l c=b.4.8.O;$(b.r).X('.8').Z('<n G=\"U 4d\"><n G=\"g-25\" m-1j=\"'+c.1j+'\" m-1a=\"'+(c.3!==''?c.3:b.4.3)+'\" m-2p=\"'+c.2p+'\"></n></n>');g.3Z={13:b.4.8.O.13};l d=0;9(A 2x==='F'&&d==0){d=1;(6(){l a=h.1g('P');a.Q='x/1c';a.1r=w;a.17='//3w.2w.o/Y/25.Y';l s=h.1d('P')[0];s.1e.1f(a,s)})()}J{2x.25.3X()}},E:6(c){l e=c.4.8.E;$(c.r).X('.8').Z('<n G=\"U E\"><n 2T=\"1V-47\"></n><n G=\"1V-1L\" m-1a=\"'+(e.3!==''?e.3:c.4.3)+'\" m-1A=\"'+e.1A+'\" m-11=\"'+e.11+'\" m-H=\"'+e.H+'\" m-3u-2c=\"'+e.2c+'\" m-R=\"'+e.R+'\" m-2d=\"'+e.2d+'\" m-1B=\"'+e.1B+'\" m-16=\"'+e.16+'\"></n></n>');l f=0;9(A 1i==='F'&&f==0){f=1;(6(d,s,a){l b,2s=d.1d(s)[0];9(d.3x(a)){1v}b=d.1g(s);b.2T=a;b.17='//4c.E.4n/'+e.13+'/4t.Y#4C=1';2s.1e.1f(b,2s)}(h,'P','E-5g'))}J{1i.3n.3p()}},z:6(b){l c=b.4.8.z;$(b.r).X('.8').Z('<n G=\"U z\"><a 1a=\"1D://z.o/L\" G=\"z-L-U\" m-3=\"'+(c.3!==''?c.3:b.4.3)+'\" m-y=\"'+c.y+'\" m-x=\"'+b.4.x+'\" m-16=\"'+c.16+'\" m-2m=\"'+c.2m+'\" m-1I=\"'+c.1I+'\" m-13=\"'+c.13+'\">3q</a></n>');l d=0;9(A 2j==='F'&&d==0){d=1;(6(){l a=h.1g('P');a.Q='x/1c';a.1r=w;a.17='//1M.z.o/1N.Y';l s=h.1d('P')[0];s.1e.1f(a,s)})()}J{$.3C({3:'//1M.z.o/1N.Y',3E:'P',3F:w})}},I:6(a){l b=a.4.8.I;$(a.r).X('.8').Z('<n G=\"U I\"><a G=\"3H '+b.Q+'\" 3L=\"3U 3V\" 1a=\"S://I.o/2y?3='+V((b.3!==''?b.3:a.4.3))+'\"></a></n>');l c=0;9(A 43==='F'&&c==0){c=1;(6(){l s=h.1g('2z'),24=h.1d('2z')[0];s.Q='x/1c';s.1r=w;s.17='//1N.I.o/8.Y';24.1e.1f(s,24)})()}},p:6(a){9(a.4.8.p.1j=='4g'){l b='H:2r;',2e='D:2B;H:2r;1B-1j:4y;1t-D:2B;',2l='D:2C;1t-D:2C;2k-50:1H;'}J{l b='H:53;',2e='2g:58;2f:0 1H;D:1u;H:5c;1t-D:1u;',2l='2g:5d;D:1u;1t-D:1u;'}l c=a.1w(a.4.y.p);9(A c===\"F\"){c=0}$(a.r).X('.8').Z('<n G=\"U p\"><n 1T=\"'+b+'1B:5i 5j,5k,5l-5n;5t:3k;1S:#3m;2D:3o-2E;2g:2F;D:1u;1t-D:3r;2k:0;2f:0;x-3s:0;3t-2b:3v;\">'+'<n 1T=\"'+2e+'2H-1S:#2I;2k-3y:3z;3A:3B;x-2b:2J;1O:2K 2L #3G;1O-2M:1H;\">'+c+'</n>'+'<n 1T=\"'+2l+'2D:2E;2f:0;x-2b:2J;x-3I:2F;H:2r;2H-1S:#3J;1O:2K 2L #3K;1O-2M:1H;1S:#2I;\">'+'<2N 17=\"S://1o.p.o/3M/2N/p.3N.3O\" D=\"10\" H=\"10\" 3Q=\"3R\" /> 3S</n></n></n>');$(a.r).X('.p').3T('1P',6(){a.2O('p')})},K:6(b){l c=b.4.8.K;$(b.r).X('.8').Z('<n G=\"U K\"><2P:28 11=\"'+c.11+'\" 3h=\"'+(c.3!==''?c.3:b.4.3)+'\"></2P:28></n>');l d=0;9(A 1E==='F'&&d==0){d=1;(6(){l a=h.1g('P');a.Q='x/1c';a.1r=w;a.17='//1M.K.o/1/1N.Y';l s=h.1d('P')[0];s.1e.1f(a,s)})();s=g.3Y(6(){9(A 1E!=='F'){1E.2Q();21(s)}},20)}J{1E.2Q()}},N:6(b){l c=b.4.8.N;$(b.r).X('.8').Z('<n G=\"U N\"><P Q=\"1Z/L\" m-3=\"'+(c.3!==''?c.3:b.4.3)+'\" m-22=\"'+c.22+'\"></P></n>');l d=0;9(A g.2R==='F'&&d==0){d=1;(6(){l a=h.1g('P');a.Q='x/1c';a.1r=w;a.17='//1M.N.o/1Z.Y';l s=h.1d('P')[0];s.1e.1f(a,s)})()}J{g.2R.1W()}},B:6(b){l c=b.4.8.B;$(b.r).X('.8').Z('<n G=\"U B\"><a 1a=\"S://B.o/1K/2u/U/?3='+(c.3!==''?c.3:b.4.3)+'&1s='+c.1s+'&1C='+c.1C+'\" G=\"1K-3j-U\" y-11=\"'+c.11+'\">48 49</a></n>');(6(){l a=h.1g('P');a.Q='x/1c';a.1r=w;a.17='//4a.B.o/Y/4b.Y';l s=h.1d('P')[0];s.1e.1f(a,s)})()}},2S={O:6(){},E:6(){1V=g.2v(6(){9(A 1i!=='F'){1i.2t.2q('2U.2u',6(a){1m.1l(['1k','E','1L',a])});1i.2t.2q('2U.4k',6(a){1m.1l(['1k','E','4l',a])});1i.2t.2q('4m.1A',6(a){1m.1l(['1k','E','1A',a])});21(1V)}},2V)},z:6(){2W=g.2v(6(){9(A 2j!=='F'){2j.4p.4q('1J',6(a){9(a){1m.1l(['1k','z','1J'])}});21(2W)}},2V)},I:6(){},p:6(){},K:6(){},N:6(){6 4s(){1m.1l(['1k','N','L'])}},B:6(){}},2Y={O:6(a){g.19(\"1D://4v.2w.o/L?4w=\"+a.8.O.13+\"&3=\"+V((a.8.O.3!==''?a.8.O.3:a.3)),\"\",\"1b=0, 1G=0, H=2Z, D=20\")},E:6(a){g.19(\"S://1o.E.o/30/30.3d?u=\"+V((a.8.E.3!==''?a.8.E.3:a.3))+\"&t=\"+a.x+\"\",\"\",\"1b=0, 1G=0, H=2Z, D=20\")},z:6(a){g.19(\"1D://z.o/4A/1J?x=\"+V(a.x)+\"&3=\"+V((a.8.z.3!==''?a.8.z.3:a.3))+(a.8.z.16!==''?'&16='+a.8.z.16:''),\"\",\"1b=0, 1G=0, H=31, D=32\")},I:6(a){g.19(\"S://I.o/4D/4E/2y?3=\"+V((a.8.I.3!==''?a.8.I.3:a.3))+\"&12=\"+a.x+\"&1I=w&1T=w\",\"\",\"1b=0, 1G=0, H=31, D=32\")},p:6(a){g.19('S://1o.p.o/4F?v=5&4G&4H=4I&3='+V((a.8.p.3!==''?a.8.p.3:a.3))+'&12='+a.x,'p','1b=1F,H=1h,D=1h')},K:6(a){g.19('S://1o.K.o/28/?3='+V((a.8.p.3!==''?a.8.p.3:a.3)),'K','1b=1F,H=1h,D=1h')},N:6(a){g.19('1D://1o.N.o/4L/L?3='+V((a.8.p.3!==''?a.8.p.3:a.3))+'&4M=&4N=w','N','1b=1F,H=1h,D=1h')},B:6(a){g.19('S://B.o/1K/2u/U/?3='+V((a.8.B.3!==''?a.8.B.3:a.3))+'&1s='+V(a.8.B.1s)+'&1C='+a.8.B.1C,'B','1b=1F,H=4O,D=4P')}};6 T(a,b){7.r=a;7.4=$.4S(w,{},23,b);7.4.L=b.L;7.4T=23;7.4U=j;7.1W()};T.W.1W=6(){l c=7;9(7.4.1p!==''){1n.O=7.4.1p+'?3={3}&Q=O';1n.K=7.4.1p+'?3={3}&Q=K';1n.B=7.4.1p+'?3={3}&Q=B'}$(7.r).4W(7.4.3i);9(A $(7.r).m('12')!=='F'){7.4.12=$(7.r).4X('m-12')}9(A $(7.r).m('3')!=='F'){7.4.3=$(7.r).m('3')}9(A $(7.r).m('x')!=='F'){7.4.x=$(7.r).m('x')}$.1z(7.4.L,6(a,b){9(b===w){c.4.2a++}});9(c.4.3b===w){$.1z(7.4.L,6(a,b){9(b===w){4Z{c.34(a)}51(e){}}})}J 9(c.4.18!==''){7.4.26(7,7.4)}J{7.2n()}$(7.r).1X(6(){9($(7).X('.8').36===0&&c.4.3c===w){c.2n()}c.4.1X(c,c.4)},6(){c.4.38(c,c.4)});$(7.r).1P(6(){c.4.1P(c,c.4);1v C})};T.W.2n=6(){l c=7;$(7.r).Z('<n G=\"8\"></n>');$.1z(c.4.L,6(a,b){9(b==w){2A[a](c);9(c.4.2o===w){2S[a]()}}})};T.W.34=6(c){l d=7,y=0,3=1n[c].1x('{3}',V(7.4.3));9(7.4.8[c].15===w&&7.4.8[c].3!==''){3=1n[c].1x('{3}',7.4.8[c].3)}9(3!=''&&d.4.1p!==''){$.55(3,6(a){9(A a.y!==\"F\"){l b=a.y+'';b=b.1x('\\56\\57','');y+=1Q(b,10)}J 9(a.m&&a.m.36>0&&A a.m[0].39!==\"F\"){y+=1Q(a.m[0].39,10)}J 9(A a.3a!==\"F\"){y+=1Q(a.3a,10)}J 9(A a[0]!==\"F\"){y+=1Q(a[0].5b,10)}J 9(A a[0]!==\"F\"){}d.4.y[c]=y;d.4.1q+=y;d.2i();d.1R()}).5e(6(){d.4.y[c]=0;d.1R()})}J{d.2i();d.4.y[c]=0;d.1R()}};T.W.1R=6(){l a=0;5f(e 1Z 7.4.y){a++}9(a===7.4.2a){7.4.26(7,7.4)}};T.W.2i=6(){l a=7.4.1q,18=7.4.18;9(7.4.1w===w){a=7.1w(a)}9(18!==''){18=18.1x('{1q}',a);$(7.r).1U(18)}J{$(7.r).1U('<n G=\"5h\"><a G=\"y\" 1a=\"#\">'+a+'</a>'+(7.4.12!==''?'<a G=\"L\" 1a=\"#\">'+7.4.12+'</a>':'')+'</n>')}};T.W.1w=6(a){9(a>=3e){a=(a/3e).3f(2)+\"M\"}J 9(a>=3g){a=(a/3g).3f(1)+\"k\"}1v a};T.W.2O=6(a){2Y[a](7.4);9(7.4.2o===w){l b={O:{14:'5m',R:'+1'},E:{14:'E',R:'1L'},z:{14:'z',R:'1J'},I:{14:'I',R:'29'},p:{14:'p',R:'29'},K:{14:'K',R:'29'},N:{14:'N',R:'L'},B:{14:'B',R:'1K'}};1m.1l(['1k',b[a].14,b[a].R])}};T.W.5o=6(){l a=$(7.r).1U();$(7.r).1U(a.1x(7.4.1q,7.4.1q+1))};T.W.5p=6(a,b){9(a!==''){7.4.3=a}9(b!==''){7.4.x=b}};$.5q[j]=6(b){l c=5r;9(b===i||A b==='5s'){1v 7.1z(6(){9(!$.m(7,'2h'+j)){$.m(7,'2h'+j,5u T(7,b))}})}J 9(A b==='5v'&&b[0]!=='5w'&&b!=='1W'){1v 7.1z(6(){l a=$.m(7,'2h'+j);9(a 5x T&&A a[b]==='6'){a[b].5y(a,5z.W.5A.5B(c,1))}})}}})(5C,5D,5E);",62,351,"|||url|options||function|this|buttons|if||||||||||||var|data|div|com|delicious||element|||||true|text|count|twitter|typeof|pinterest|false|height|facebook|undefined|class|width|digg|else|stumbleupon|share||linkedin|googlePlus|script|type|action|http|Plugin|button|encodeURIComponent|prototype|find|js|append||layout|title|lang|site|urlCount|via|src|template|open|href|toolbar|javascript|getElementsByTagName|parentNode|insertBefore|createElement|550|FB|size|_trackSocial|push|_gaq|urlJson|www|urlCurl|total|async|media|line|20px|return|shorterTotal|replace|callback|each|send|font|description|https|STMBLPN|no|status|3px|related|tweet|pin|like|platform|widgets|border|click|parseInt|rendererPerso|color|style|html|fb|init|hover|sharrre|in|500|clearInterval|counter|defaults|s1|plusone|render||badge|add|shareTotal|align|faces|colorscheme|cssCount|padding|float|plugin_|renderer|twttr|margin|cssShare|hashtags|loadButtons|enableTracking|annotation|subscribe|50px|fjs|Event|create|setInterval|google|gapi|submit|SCRIPT|loadButton|35px|18px|display|block|none|json|background|fff|center|1px|solid|radius|img|openPopup|su|processWidgets|IN|tracking|id|edge|1000|tw|20url|popup|900|sharer|650|360|horizontal|getSocialJson|en|length|medium|hide|total_count|shares|enableCounter|enableHover|php|1e6|toFixed|1e3|location|className|it|pointer|services|666666|XFBML|inline|parse|Tweet|normal|indent|vertical|show|baseline|apis|getElementById|bottom|5px|overflow|hidden|ajax|urls|dataType|cache|ccc|DiggThisButton|decoration|7EACEE|40679C|rel|static|small|gif|api|alt|Delicious|Add|on|nofollow|external|cdn|go|setTimeout|___gcfg|20WHERE|20link_stat|20FROM|__DBW|20click_count|20comments_fbid|commentsbox_count|root|Pin|It|assets|pinit|connect|googleplus|20total_count|20comment_count|tall|20like_count|20share_count|20normalized_url|remove|unlike|message|net|jsonp|events|bind|format|LinkedInShare|all|SELECT|plus|hl|fql|15px|countserv|intent|urlinfo|xfbml|tools|diggthis|save|noui|jump|close|graph|DiggCompact|cws|token|isFramed|700|300|v2|en_US|extend|_defaults|_name|button_count|addClass|attr|US|try|top|catch|feeds|93px|links|getJSON|u00c2|u00a0|right|getInfo|story|total_posts|26px|left|error|for|jssdk|box|12px|Arial|Helvetica|sans|Google|serif|simulateClick|update|fn|arguments|object|cursor|new|string|_|instanceof|apply|Array|slice|call|jQuery|window|document".split("|"),0,{})),jQuery(document).ready(function(){function e(e){return""==l.val()?(l.addClass("error"),n.text(fullname_error_msg),n.addClass("message_error2"),!1):""!=l&&(""==jQuery("#claimer_id").val()||jQuery("#claimer_id").val()<=0)?(jQuery(".user_fname_spin").remove(),jQuery("input#claimer_name").css("display","inline"),""==e&&jQuery("input#claimer_name").after("<i class='fa fa-circle-o-notch fa-spin user_fname_spin ajax-fa-spin'></i>"),chknameRequest=jQuery.ajax({url:ajaxUrl,type:"POST",async:!0,data:"action=tmpl_ajax_check_user_email&user_fname="+l.val(),beforeSend:function(){null!=chknameRequest&&chknameRequest.abort()},success:function(e){var r=e.split(",");"fname"==r[1]&&(r[0]>0?(document.getElementById("claimer_nameInfo").innerHTML=user_fname_error+user_login_link,document.getElementById("claimer_name_already_exist").value=0,jQuery("#claimer_nameInfo").addClass("message_error2"),jQuery("#claimer_nameInfo").removeClass("available_tick"),reg_name=0):(document.getElementById("claimer_nameInfo").innerHTML=user_fname_verified,document.getElementById("claimer_name_already_exist").value=1,jQuery("#claimer_nameInfo").removeClass("message_error2"),jQuery("#claimer_nameInfo").addClass("available_tick"),2==jQuery("#claim_listing_frm div").size()&&checkclick&&document.claim_listing_frm.submit(),reg_name=1)),jQuery(".user_fname_spin").remove()}}),1==reg_name?!0:!1):(l.removeClass("error"),n.text(""),n.removeClass("message_error2"),!0)}function r(e){var r=0;if(""==s.val())r=1;else if(""!=s.val()){var a=s.val(),t=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;r=t.test(a)?0:1}if(1==r)return s.addClass("error"),o.text(""==s.val()?email_balnk_msg:email_error_msg),o.addClass("message_error2"),!1;if(""==s.val())return s.addClass("error"),o.text(email_error_msg),o.addClass("message_error2"),!1;var a=s.val(),t=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(t.test(a)){if(r=0,""!=s&&(""==jQuery("#claimer_id").val()||jQuery("#claimer_id").val()<=0))return jQuery(".user_email_spin").remove(),jQuery("input#claimer_email").css("display","inline"),""==e&&jQuery("input#claimer_email").after("<i class='fa fa-circle-o-notch fa-spin user_email_spin ajax-fa-spin'></i>"),chknameRequest=jQuery.ajax({url:ajaxUrl,type:"POST",async:!0,data:"action=tmpl_ajax_check_user_email&user_email="+s.val(),beforeSend:function(){null!=chknameRequest&&chknameRequest.abort()},success:function(e){var r=e.split(",");"email"==r[1]&&(r[0]>0?(document.getElementById("claimer_emailInfo").innerHTML=user_email_error,document.getElementById("claimer_name_already_exist").value=0,jQuery("#claimer_emailInfo").addClass("message_error2"),jQuery("#claimer_emailInfo").removeClass("available_tick"),reg_email=0):(document.getElementById("claimer_emailInfo").innerHTML=user_email_verified,document.getElementById("claimer_name_already_exist").value=1,jQuery("#claimer_emailInfo").removeClass("message_error2"),jQuery("#claimer_emailInfo").addClass("available_tick"),2==jQuery("#claim_listing_frm div").size()&&checkclick&&document.claim_listing_frm.submit(),reg_email=1)),jQuery(".user_email_spin").remove()}}),1==reg_email?!0:!1}else r=1}function a(){return""==jQuery("#claim_msg").val()?(i.addClass("error"),u.text(claim_error_msg),u.addClass("message_error2"),!1):(i.removeClass("error"),u.text(""),u.removeClass("message_error2"),!0)}jQuery("#claimer_name").focus();var t=jQuery("#claim_listing_frm"),l=jQuery("#claimer_name"),n=jQuery("#claimer_nameInfo"),s=jQuery("#claimer_email"),o=jQuery("#claimer_emailInfo"),i=jQuery("#claim_msg"),u=jQuery("#claim_msgInfo");l.blur(e),s.blur(r),i.blur(a);var c="";t.submit(function(){if(""==jQuery("#claimer_id").val()||jQuery("#claimer_id").val()<=0){var n=e(is_submit=1),o=r(is_submit=1);c=n&&o?!0:!1}else c=!0;if(c&a()){document.getElementById("process_claimownership").style.display="block";var i=t.serialize();return jQuery.ajax({url:ajaxUrl,type:"POST",data:"action=tevolution_claimowner_ship&"+i,success:function(e){document.getElementById("process_claimownership").style.display="none",1==e?jQuery("#claimownership_msg").html(captcha_invalid_msg):(jQuery("#claimownership_msg").html(e),setTimeout(function(){jQuery("#lean_overlay").fadeOut(200),jQuery("#tmpl_claim_listing").removeClass("open"),jQuery("#tmpl_claim_listing").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#claimownership_msg").html(""),l.val(""),s.val(""),jQuery(".claim_ownership").html('<a href="javascript:void(0)" class="claimed">'+already_claimed_msg+"</a>"),jQuery("#claimer_contact").val("")},2e3))}}),!1}return!1})}),jQuery(function(){jQuery("#tmpl_reg_login_container")&&jQuery("#tmpl_reg_login_container .modal_close").click(function(){jQuery("#tmpl_reg_login_container").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#tmpl_reg_login_container").removeClass("open")}),jQuery("#lean_overlay")&&jQuery("#lean_overlay").click(function(){captcha&&jQuery("#recaptcha_widget_div").html(captcha)}),jQuery(".modal_close")&&jQuery(".modal_close").click(function(){captcha&&jQuery("#recaptcha_widget_div").html(captcha)}),jQuery(".reveal-modal-bg").live("click",function(){captcha&&jQuery("#recaptcha_widget_div").html(captcha)}),jQuery("#tmpl_send_inquiry")&&(jQuery("#tmpl_send_inquiry .modal_close").click(function(){jQuery("#tmpl_send_inquiry").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#tmpl_send_inquiry").removeClass("open")}),tmpl_close_popup()),jQuery("#claim-header")&&(jQuery("#claim-header .modal_close").click(function(){jQuery("#tmpl_claim_listing").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#tmpl_claim_listing").removeClass("open")}),tmpl_close_popup()),jQuery("#tmpl_send_to_frd")&&(jQuery("#tmpl_send_to_frd .modal_close").click(function(){jQuery("#tmpl_send_to_frd").attr("style",""),jQuery(".reveal-modal-bg").css("display","none"),jQuery("#tmpl_send_to_frd").removeClass("open")
/* 2 */ }),tmpl_close_popup())}),jQuery(window).load(function(){jQuery(".sort_options select,#searchform select,#submit_form select,.search_filter select,.tmpl_search_property select,.widget_location_nav select,#srchevent select,#header_location .location_nav select,.horizontal_location_nav select,.widget select").wrap("<div class='select-wrap'></div>"),jQuery(".peoplelisting li").wrapInner("<div class='peopleinfo-wrap'></div>"),jQuery.browser.opera||jQuery(".sort_options select,#searchform select,#submit_form select,.search_filter select,.tmpl_search_property select,.widget_location_nav select,#srchevent select,#header_location .location_nav select,.horizontal_location_nav select,.widget select").each(function(){var e=jQuery(this).attr("title");if("multiple"!=jQuery(this).attr("multiple")){var e=jQuery("option:selected",this).text();jQuery(this).css({"z-index":10,opacity:0,"-khtml-appearance":"none"}).after('<span class="select">'+e+"</span>").change(function(){val=jQuery("option:selected",this).text(),jQuery(this).next().text(val)})}})}),jQuery(document).ready(function(){var e=null,r="";jQuery("#searchform .range_address").autocomplete({minLength:0,create:function(){jQuery(this).data("ui-autocomplete")._renderItem=function(e,r){return jQuery("<li>").addClass("instant_search").append("<a>").attr("href",r.url).html(r.label).appendTo(e)}},source:function(a,t){(""==r||""!=a.term)&&(r=a.term);var l=jQuery(".miles_range_post_type").val();e=jQuery.ajax({url:tevolutionajaxUrl,type:"POST",dataType:"json",data:"action=tevolution_autocomplete_address_callBack&search_text="+r+"&post_type="+l,beforeSend:function(){null!=e&&e.abort()},success:function(e){t(jQuery.map(e.results,function(e){return{label:e.title,value:e.label,url:e.url}}))}})},autoFocus:!0,scroll:!0,select:function(e,r){return"#"===r.item.url?!0:void 0},open:function(e){var r=jQuery(this).data("uiAutocomplete");r.menu.element.find("a").each(function(){var e=jQuery(this),a=jQuery.trim(r.term).split(" ").join("|");e.html(e.text().replace(new RegExp("("+a+")","gi"),"$1"))}),jQuery(e.target).removeClass("sa_searching")}}).focus(function(){jQuery(this).autocomplete("search","")}),jQuery("iframe").each(function(){var e=jQuery(this).attr("src");e&&(e.indexOf("?")>-1?jQuery(this).attr("src",e+"&wmode=transparent"):jQuery(this).attr("src",e+"?wmode=transparent"))})}),jQuery("#listpagi .search_pagination .page-numbers").live("click",function(e){e.preventDefault(),post_link=jQuery(this).attr("href"),post_link=post_link.replace("#038;","&"),jQuery(".search_result_listing").addClass("loading_results");var r=jQuery(".tmpl_filter_results").serialize();return jQuery.ajax({url:post_link+"&"+r,type:"POST",async:!0,success:function(e){jQuery(".search_result_listing").removeClass("loading_results"),jQuery(".search_result_listing").html(e),jQuery("html, body").animate({scrollTop:0},200)}}),jQuery.ajax({url:post_link+"&data_map=1&"+r,type:"POST",async:!0,dataType:"json",success:function(e){googlemaplisting_deleteMarkers(),markers=e.markers,templ_add_googlemap_markers(markers)}}),!1});

;
/* soliloquy-min.js */

/* 1 */ function soliloquyIsMobile(){var e=!1;return function(t){(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(t)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(t.substr(0,4)))&&(e=!0)}(navigator.userAgent||navigator.vendor||window.opera),e}function soliloquyYouTubeVids(e,t,i,s,n,$){$("#"+n).show().css({display:"block","z-index":"1210"}),YT.Player&&(soliloquy_youtube[t]=new YT.Player(n,{videoId:t,playerVars:e,events:{onStateChange:soliloquyYouTubeOnStateChange}}))}function soliloquyYouTubeOnStateChange(e){var t=jQuery(e.target.a).data("soliloquy-slider-id");(e.data===YT.PlayerState.PAUSED||e.data===YT.PlayerState.ENDED)&&soliloquy_slider[t]&&soliloquy_slider[t].startAuto(),e.data===YT.PlayerState.PLAYING&&soliloquy_slider[t]&&soliloquy_slider[t].stopAuto()}function onYouTubeIframeAPIReady(){}function soliloquyVimeoVids(e,t,i,s,n,$){if($("#"+n).show().css({display:"block","z-index":"1210"}),$f){var o={};$.each($("#"+n)[0].attributes,function(e,t){o[t.nodeName]=t.nodeValue}),e.player_id=n,o.src="//player.vimeo.com/video/"+t+"?"+$.param(e),o.frameborder=0,$("#"+n).replaceWith(function(){return $("<iframe />",o).append($(this).contents())}),soliloquy_vimeo[t]=$f($("#"+n)[0]),soliloquy_vimeo[t].addEvent("ready",function(){soliloquy_vimeo[t].addEvent("play",soliloquyVimeoSliderPause),soliloquy_vimeo[t].addEvent("pause",soliloquyVimeoSliderStart),soliloquy_vimeo[t].addEvent("finish",soliloquyVimeoSliderStart)})}}function soliloquyVimeoSliderPause(e){var t=jQuery("#"+e).data("soliloquy-slider-id");soliloquy_slider[t]&&soliloquy_slider[t].stopAuto()}function soliloquyVimeoSliderStart(e){var t=jQuery("#"+e).data("soliloquy-slider-id");soliloquy_slider[t]&&soliloquy_slider[t].startAuto()}function soliloquyWistiaVids(e,t,i,s,n,$){if($("#"+n).show().css({display:"block","z-index":"1210"}),wistiaEmbeds){var o={};$.each($("#"+n)[0].attributes,function(e,t){o[t.nodeName]=t.nodeValue}),e.container=n,o.src="//fast.wistia.net/embed/iframe/"+t+"?"+$.param(e),o.frameborder=0,$("#"+n).replaceWith(function(){return $("<iframe />",o).addClass("wistia_embed").append($(this).contents())}),wistiaEmbeds.onFind(function(e){t==e.hashedId()&&(soliloquy_wistia[t]=e,soliloquy_wistia[t].bind("play",function(){var e=$(this.container).data("soliloquy-slider-id");soliloquy_slider[e]&&soliloquy_slider[e].stopAuto()}),soliloquy_wistia[t].bind("pause",function(){var e=$(this.container).data("soliloquy-slider-id");soliloquy_slider[e]&&soliloquy_slider[e].startAuto()}),soliloquy_wistia[t].bind("end",function(){var e=$(this.container).data("soliloquy-slider-id");soliloquy_slider[e]&&soliloquy_slider[e].startAuto()}),e.play())})}}!function($){var e={},t={keyboard:!1,mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,slideZIndex:50,wrapperClass:"soliloquy-wrapper",touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,autoSlideForOnePage:!1,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){},onSliderResize:function(){}};$.fn.soliloquy=function(s){if(0==this.length)return this;if(this.length>1)return this.each(function(){$(this).soliloquy(s)}),this;var n={},o=this;e.el=this;var a=$(window).width(),l=$(window).height(),r=function(){n.settings=$.extend({},t,s),n.settings.slideWidth=parseInt(n.settings.slideWidth),n.children=o.children(n.settings.slideSelector),n.children.length<n.settings.minSlides&&(n.settings.minSlides=n.children.length),n.children.length<n.settings.maxSlides&&(n.settings.maxSlides=n.children.length),n.settings.randomStart&&(n.settings.startSlide=Math.floor(Math.random()*n.children.length)),n.active={index:n.settings.startSlide},n.carousel=n.settings.minSlides>1||n.settings.maxSlides>1,n.carousel&&(n.settings.preloadImages="all"),n.minThreshold=n.settings.minSlides*n.settings.slideWidth+(n.settings.minSlides-1)*n.settings.slideMargin,n.maxThreshold=n.settings.maxSlides*n.settings.slideWidth+(n.settings.maxSlides-1)*n.settings.slideMargin,n.working=!1,n.controls={},n.interval=null,n.animProp="vertical"==n.settings.mode?"top":"left",n.usingCSS=n.settings.useCSS&&"fade"!=n.settings.mode&&function(){var e=document.createElement("div"),t=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in t)if(void 0!==e.style[t[i]])return n.cssPrefix=t[i].replace("Perspective","").toLowerCase(),n.animProp="-"+n.cssPrefix+"-transform",!0;return!1}(),"vertical"==n.settings.mode&&(n.settings.maxSlides=n.settings.minSlides),o.data("origStyle",o.attr("style")),o.children(n.settings.slideSelector).each(function(){$(this).data("origStyle",$(this).attr("style"))}),d()},d=function(){o.wrap('<div class="'+n.settings.wrapperClass+'"><div class="soliloquy-viewport"></div></div>'),n.viewport=o.parent(),n.loader=$('<div class="soliloquy-loading" />'),n.viewport.prepend(n.loader),o.css({width:"horizontal"==n.settings.mode?100*n.children.length+215+"%":"auto",position:"relative"}),n.usingCSS&&n.settings.easing?o.css("-"+n.cssPrefix+"-transition-timing-function",n.settings.easing):n.settings.easing||(n.settings.easing="swing");var e=v();n.viewport.css({width:"100%",position:"relative"}),"fade"!=n.settings.mode&&n.viewport.css({overflow:"hidden"}),n.viewport.parent().css({maxWidth:p()}),n.settings.pager||n.viewport.parent().css({margin:"0 auto 0px"}),n.children.css({"float":"left",listStyle:"none",position:"relative"}),n.children.css("width",h()),"horizontal"==n.settings.mode&&n.settings.slideMargin>0&&n.children.css("marginRight",n.settings.slideMargin),"vertical"==n.settings.mode&&n.settings.slideMargin>0&&n.children.css("marginBottom",n.settings.slideMargin),"fade"==n.settings.mode&&(n.children.css({zIndex:0,display:"none",marginRight:"-100%",width:"100%"}),n.children.eq(n.settings.startSlide).css({zIndex:n.settings.slideZIndex,display:"block"})),n.controls.el=$('<div class="soliloquy-controls" />'),n.settings.captions&&T(),n.active.last=n.settings.startSlide==m()-1;var t=n.children.eq(n.settings.startSlide);"all"==n.settings.preloadImages&&(t=n.children),n.settings.ticker?n.settings.pager=!1:(n.settings.pager&&w(),n.settings.controls&&b(),n.settings.auto&&n.settings.autoControls&&q(),(n.settings.controls||n.settings.autoControls||n.settings.pager)&&n.viewport.after(n.controls.el)),c(t,g)},c=function(e,t){var i=e.find("img, iframe").length;if(0==i)return void t();var s=0;e.find("img, iframe").each(function(){$(this).one("load",function(){++s==i&&t()}).each(function(){this.complete&&$(this).load()})})},g=function(){if(n.settings.infiniteLoop&&"fade"!=n.settings.mode&&!n.settings.ticker){var e="vertical"==n.settings.mode?n.settings.minSlides:n.settings.maxSlides,t=n.children.slice(0,e).clone().addClass("soliloquy-clone"),i=n.children.slice(-e).clone().addClass("soliloquy-clone");o.append(t).prepend(i)}n.loader.remove(),x(),"vertical"==n.settings.mode&&(n.settings.adaptiveHeight=!0),n.viewport.height(u()),o.redrawSlider(),n.settings.onSliderLoad(n.active.index),n.initialized=!0,n.settings.responsive&&$(window).bind("resize",F),n.settings.auto&&n.settings.autoStart&&(m()>1||n.settings.autoSlideForOnePage)&&D(),n.settings.ticker&&W(),n.settings.pager&&I(n.settings.startSlide),n.settings.controls&&_(),n.settings.touchEnabled&&!n.settings.ticker&&H(),n.settings.keyboard&&!n.settings.ticker&&$("body").on("keydown",function(e){return"textarea"!=e.target.type&&"input"!=e.target.type?39==e.keyCode?(k(e),!1):37==e.keyCode?(C(e),!1):void 0:void 0})},u=function(){var e=0,t=$();if("vertical"==n.settings.mode||n.settings.adaptiveHeight)if(n.carousel){var s=1==n.settings.moveSlides?n.active.index:n.active.index*f();for(t=n.children.eq(s),i=1;i<=n.settings.maxSlides-1;i++)t=s+i>=n.children.length?t.add(n.children.eq(i-1)):t.add(n.children.eq(s+i))}else t=n.children.eq(n.active.index);else t=n.children;return"vertical"==n.settings.mode?(t.each(function(t){e+=$(this).outerHeight()}),n.settings.slideMargin>0&&(e+=n.settings.slideMargin*(n.settings.minSlides-1))):e=Math.max.apply(Math,t.map(function(){return $(this).outerHeight(!1)}).get()),"border-box"==n.viewport.css("box-sizing")?e+=parseFloat(n.viewport.css("padding-top"))+parseFloat(n.viewport.css("padding-bottom"))+parseFloat(n.viewport.css("border-top-width"))+parseFloat(n.viewport.css("border-bottom-width")):"padding-box"==n.viewport.css("box-sizing")&&(e+=parseFloat(n.viewport.css("padding-top"))+parseFloat(n.viewport.css("padding-bottom"))),e},p=function(){var e="100%";return n.settings.slideWidth>0&&(e="horizontal"==n.settings.mode?n.settings.maxSlides*n.settings.slideWidth+(n.settings.maxSlides-1)*n.settings.slideMargin:n.settings.slideWidth),"fade"==n.settings.mode?"100%":e},h=function(){var e=n.settings.slideWidth,t=n.viewport.width();return 0==n.settings.slideWidth||n.settings.slideWidth>t&&!n.carousel||"vertical"==n.settings.mode?e=t:n.settings.maxSlides>1&&"horizontal"==n.settings.mode&&(t>n.maxThreshold||t<n.minThreshold&&(e=(t-n.settings.slideMargin*(n.settings.minSlides-1))/n.settings.minSlides)),"fade"==n.settings.mode?"100%":e},v=function(){var e=1;if("horizontal"==n.settings.mode&&n.settings.slideWidth>0)if(n.viewport.width()<n.minThreshold)e=n.settings.minSlides;else if(n.viewport.width()>n.maxThreshold)e=n.settings.maxSlides;else{var t=n.children.first().width()+n.settings.slideMargin;e=Math.floor((n.viewport.width()+n.settings.slideMargin)/t)}else"vertical"==n.settings.mode&&(e=n.settings.minSlides);return e},m=function(){var e=0;if(n.settings.moveSlides>0)if(n.settings.infiniteLoop)e=Math.ceil(n.children.length/f());else for(var t=0,i=0;t<n.children.length;)++e,t=i+v(),i+=n.settings.moveSlides<=v()?n.settings.moveSlides:v();else e=Math.ceil(n.children.length/v());return e},f=function(){return n.settings.moveSlides>0&&n.settings.moveSlides<=v()?n.settings.moveSlides:v()},x=function(){if(n.children.length>n.settings.maxSlides&&n.active.last&&!n.settings.infiniteLoop){if("horizontal"==n.settings.mode){var e=n.children.last(),t=e.position();S(-(t.left-(n.viewport.width()-e.outerWidth())),"reset",0)}else if("vertical"==n.settings.mode){var i=n.children.length-n.settings.minSlides,t=n.children.eq(i).position();S(-t.top,"reset",0)}}else{var t=n.children.eq(n.active.index*f()).position();n.active.index==m()-1&&(n.active.last=!0),void 0!=t&&("horizontal"==n.settings.mode?S(-t.left,"reset",0):"vertical"==n.settings.mode&&S(-t.top,"reset",0))}},S=function(e,t,i,s){if(n.usingCSS){var a="vertical"==n.settings.mode?"translate3d(0, "+e+"px, 0)":"translate3d("+e+"px, 0, 0)";o.css("-"+n.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==t?(o.css(n.animProp,a),o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),M()})):"reset"==t?o.css(n.animProp,a):"ticker"==t&&(o.css("-"+n.cssPrefix+"-transition-timing-function","linear"),o.css(n.animProp,a),o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),S(s.resetValue,"reset",0),Y()}))}else{var l={};l[n.animProp]=e,"slide"==t?o.animate(l,i,n.settings.easing,function(){M()}):"reset"==t?o.css(n.animProp,e):"ticker"==t&&o.animate(l,speed,"linear",function(){S(s.resetValue,"reset",0),Y()})}},y=function(){for(var e="",t=m(),i=0;t>i;i++){var s="";n.settings.buildPager&&$.isFunction(n.settings.buildPager)?(s=n.settings.buildPager(i),n.pagerEl.addClass("soliloquy-custom-pager")):(s=i+1,n.pagerEl.addClass("soliloquy-default-pager")),e+='<div class="soliloquy-pager-item"><a href="" data-slide-index="'+i+'" class="soliloquy-pager-link"><span>'+s+"</span></a></div>"}n.pagerEl.html(e)},w=function(){n.settings.pagerCustom?n.pagerEl=$(n.settings.pagerCustom):(n.pagerEl=$('<div class="soliloquy-pager" />'),n.settings.pagerSelector?$(n.settings.pagerSelector).html(n.pagerEl):n.controls.el.addClass("soliloquy-has-pager").append(n.pagerEl),y()),n.pagerEl.on("click","a",z)},b=function(){n.controls.next=$('<a class="soliloquy-next" href=""><span>'+n.settings.nextText+"</span></a>"),n.controls.prev=$('<a class="soliloquy-prev" href=""><span>'+n.settings.prevText+"</span></a>"),n.controls.next.bind("click",k),n.controls.prev.bind("click",C),n.settings.nextSelector&&$(n.settings.nextSelector).append(n.controls.next),n.settings.prevSelector&&$(n.settings.prevSelector).append(n.controls.prev),n.settings.nextSelector||n.settings.prevSelector||(n.controls.directionEl=$('<div class="soliloquy-controls-direction" />'),n.controls.directionEl.append(n.controls.prev).append(n.controls.next),n.controls.el.addClass("soliloquy-has-controls-direction").append(n.controls.directionEl))},q=function(){n.controls.start=$('<div class="soliloquy-controls-auto-item"><a class="soliloquy-start" href=""><span>'+n.settings.startText+"</span></a></div>"),n.controls.stop=$('<div class="soliloquy-controls-auto-item"><a class="soliloquy-stop" href=""><span>'+n.settings.stopText+"</span></a></div>"),n.controls.autoEl=$('<div class="soliloquy-controls-auto" />'),n.controls.autoEl.on("click",".soliloquy-start",E),n.controls.autoEl.on("click",".soliloquy-stop",P),n.settings.autoControlsCombine?n.controls.autoEl.append(n.controls.start):n.controls.autoEl.append(n.controls.start).append(n.controls.stop),n.settings.autoControlsSelector?$(n.settings.autoControlsSelector).html(n.controls.autoEl):n.controls.el.addClass("soliloquy-has-controls-auto").append(n.controls.autoEl),A(n.settings.autoStart?"stop":"start")},T=function(){n.children.each(function(e){var t=$(this).find("img:first").attr("title");void 0!=t&&(""+t).length&&$(this).append('<div class="soliloquy-caption"><span>'+t+"</span></div>")})},k=function(e){n.settings.auto&&o.stopAuto(),o.goToNextSlide(),e.preventDefault()},C=function(e){n.settings.auto&&o.stopAuto(),o.goToPrevSlide(),e.preventDefault()},E=function(e){o.startAuto(),e.preventDefault()},P=function(e){o.stopAuto(),e.preventDefault()},z=function(e){n.settings.auto&&o.stopAuto();var t=$(e.currentTarget);if(void 0!==t.attr("data-slide-index")){var i=parseInt(t.attr("data-slide-index"));i!=n.active.index&&o.goToSlide(i),e.preventDefault()}},I=function(e){var t=n.children.length;return"short"==n.settings.pagerType?(n.settings.maxSlides>1&&(t=Math.ceil(n.children.length/n.settings.maxSlides)),void n.pagerEl.html(e+1+n.settings.pagerShortSeparator+t)):(n.pagerEl.find("a").removeClass("active"),void n.pagerEl.each(function(t,i){$(i).find("a").eq(e).addClass("active")}))},M=function(){if(n.settings.infiniteLoop){var e="";0==n.active.index?e=n.children.eq(0).position():n.active.index==m()-1&&n.carousel?e=n.children.eq((m()-1)*f()).position():n.active.index==n.children.length-1&&(e=n.children.eq(n.children.length-1).position()),e&&("horizontal"==n.settings.mode?S(-e.left,"reset",0):"vertical"==n.settings.mode&&S(-e.top,"reset",0))}n.working=!1,"fade"==n.settings.mode&&n.viewport.css({overflow:""}),n.settings.onSlideAfter(n.children.eq(n.active.index),n.oldIndex,n.active.index)},A=function(e){n.settings.autoControlsCombine?n.controls.autoEl.html(n.controls[e]):(n.controls.autoEl.find("a").removeClass("active"),n.controls.autoEl.find("a:not(.soliloquy-"+e+")").addClass("active"))},_=function(){1==m()?(n.controls.prev.addClass("disabled"),n.controls.next.addClass("disabled")):!n.settings.infiniteLoop&&n.settings.hideControlOnEnd&&(0==n.active.index?(n.controls.prev.addClass("disabled"),n.controls.next.removeClass("disabled")):n.active.index==m()-1?(n.controls.next.addClass("disabled"),n.controls.prev.removeClass("disabled")):(n.controls.prev.removeClass("disabled"),n.controls.next.removeClass("disabled")))},D=function(){if(n.settings.autoDelay>0)var e=setTimeout(o.startAuto,n.settings.autoDelay);else o.startAuto();n.settings.autoHover&&o.hover(function(){n.interval&&(o.stopAuto(!0),n.autoPaused=!0)},function(){n.autoPaused&&(o.startAuto(!0),n.autoPaused=null)})},W=function(){var e=0;if("next"==n.settings.autoDirection)o.append(n.children.clone().addClass("soliloquy-clone"));else{o.prepend(n.children.clone().addClass("soliloquy-clone"));var t=n.children.first().position();e="horizontal"==n.settings.mode?-t.left:-t.top}S(e,"reset",0),n.settings.pager=!1,n.settings.controls=!1,n.settings.autoControls=!1,n.settings.tickerHover&&!n.usingCSS&&n.viewport.hover(function(){o.stop()},function(){var e=0;n.children.each(function(t){e+="horizontal"==n.settings.mode?$(this).outerWidth(!0):$(this).outerHeight(!0)});var t=n.settings.speed/e,i="horizontal"==n.settings.mode?"left":"top",s=t*(e-Math.abs(parseInt(o.css(i))));Y(s)}),Y()},Y=function(e){speed=e?e:n.settings.speed;var t={left:0,top:0},i={left:0,top:0};"next"==n.settings.autoDirection?t=o.find(".soliloquy-clone").first().position():i=n.children.first().position();var s="horizontal"==n.settings.mode?-t.left:-t.top,a="horizontal"==n.settings.mode?-i.left:-i.top,l={resetValue:a};S(s,"ticker",speed,l)},H=function(){n.touch={start:{x:0,y:0},end:{x:0,y:0}},n.viewport.bind("touchstart",V)},V=function(e){if(n.working)e.preventDefault();else{n.touch.originalPos=o.position();var t=e.originalEvent;n.touch.start.x=t.changedTouches[0].pageX,n.touch.start.y=t.changedTouches[0].pageY,n.viewport.bind("touchmove",L),n.viewport.bind("touchend",N)}},L=function(e){var t=e.originalEvent,i=Math.abs(t.changedTouches[0].pageX-n.touch.start.x),s=Math.abs(t.changedTouches[0].pageY-n.touch.start.y);if(3*i>s&&n.settings.preventDefaultSwipeX?e.preventDefault():3*s>i&&n.settings.preventDefaultSwipeY&&e.preventDefault(),"fade"!=n.settings.mode&&n.settings.oneToOneTouch){var o=0;if("horizontal"==n.settings.mode){var a=t.changedTouches[0].pageX-n.touch.start.x;o=n.touch.originalPos.left+a}else{var a=t.changedTouches[0].pageY-n.touch.start.y;o=n.touch.originalPos.top+a}S(o,"reset",0)}},N=function(e){n.viewport.unbind("touchmove",L);var t=e.originalEvent,i=0;if(n.touch.end.x=t.changedTouches[0].pageX,n.touch.end.y=t.changedTouches[0].pageY,"fade"==n.settings.mode){var s=Math.abs(n.touch.start.x-n.touch.end.x);s>=n.settings.swipeThreshold&&(n.touch.start.x>n.touch.end.x?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto())}else{var s=0;"horizontal"==n.settings.mode?(s=n.touch.end.x-n.touch.start.x,i=n.touch.originalPos.left):(s=n.touch.end.y-n.touch.start.y,i=n.touch.originalPos.top),!n.settings.infiniteLoop&&(0==n.active.index&&s>0||n.active.last&&0>s)?S(i,"reset",200):Math.abs(s)>=n.settings.swipeThreshold?(0>s?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto()):S(i,"reset",200)}n.viewport.unbind("touchend",N)},F=function(e){if(n.initialized){var t=$(window).width(),i=$(window).height();(a!=t||l!=i)&&(a=t,l=i,o.redrawSlider(),n.settings.onSliderResize.call(o,n.active.index))}};return o.goToSlide=function(e,t){if(!n.working&&n.active.index!=e)if(n.working=!0,n.oldIndex=n.active.index,0>e?n.active.index=m()-1:e>=m()?n.active.index=0:n.active.index=e,n.settings.onSlideBefore(n.children.eq(n.active.index),n.oldIndex,n.active.index),"next"==t?n.settings.onSlideNext(n.children.eq(n.active.index),n.oldIndex,n.active.index):"prev"==t&&n.settings.onSlidePrev(n.children.eq(n.active.index),n.oldIndex,n.active.index),n.active.last=n.active.index>=m()-1,n.settings.pager&&I(n.active.index),n.settings.controls&&_(),"fade"==n.settings.mode)n.viewport.css({overflow:"hidden"}),n.settings.adaptiveHeight&&n.viewport.height()!=u()&&n.viewport.animate({height:u()},n.settings.adaptiveHeightSpeed),n.children.filter(":visible").fadeOut(n.settings.speed).css({zIndex:0}),n.children.eq(n.active.index).css("zIndex",n.settings.slideZIndex+1).fadeIn(n.settings.speed,function(){$(this).css("zIndex",n.settings.slideZIndex),M()});else{n.settings.adaptiveHeight&&n.viewport.height()!=u()&&n.viewport.animate({height:u()},n.settings.adaptiveHeightSpeed);var i=0,s={left:0,top:0};if(!n.settings.infiniteLoop&&n.carousel&&n.active.last)if("horizontal"==n.settings.mode){var a=n.children.eq(n.children.length-1);s=a.position(),i=n.viewport.width()-a.outerWidth()}else{var l=n.children.length-n.settings.minSlides;s=n.children.eq(l).position()}else if(n.carousel&&n.active.last&&"prev"==t){var r=1==n.settings.moveSlides?n.settings.maxSlides-f():(m()-1)*f()-(n.children.length-n.settings.maxSlides),a=o.children(".soliloquy-clone").eq(r);s=a.position()}else if("next"==t&&0==n.active.index)s=o.find("> .soliloquy-clone").eq(n.settings.maxSlides).position(),n.active.last=!1;else if(e>=0){var d=e*f();s=n.children.eq(d).position()}if("undefined"!=typeof s){var c="horizontal"==n.settings.mode?-(s.left-i):-s.top;S(c,"slide",n.settings.speed)}}},o.goToNextSlide=function(){if(n.settings.infiniteLoop||!n.active.last){var e=parseInt(n.active.index)+1;o.goToSlide(e,"next")}},o.goToPrevSlide=function(){if(n.settings.infiniteLoop||0!=n.active.index){var e=parseInt(n.active.index)-1;o.goToSlide(e,"prev")}},o.startAuto=function(e){n.interval||(n.interval=setInterval(function(){"next"==n.settings.autoDirection?o.goToNextSlide():o.goToPrevSlide()},n.settings.pause),n.settings.autoControls&&1!=e&&A("stop"))},o.stopAuto=function(e){n.interval&&(clearInterval(n.interval),n.interval=null,n.settings.autoControls&&1!=e&&A("start"))},o.getCurrentSlide=function(){return n.active.index},o.getCurrentSlideElement=function(){return n.children.eq(n.active.index)},o.getSlideCount=function(){return n.children.length},o.redrawSlider=function(){n.children.add(o.find(".soliloquy-clone")).width(h()),n.viewport.css("height",u()),n.settings.ticker||x(),n.active.last&&(n.active.index=m()-1),n.active.index>=m()&&(n.active.last=!0),n.settings.pager&&!n.settings.pagerCustom&&(y(),I(n.active.index))},o.destroySlider=function(){n.initialized&&(n.initialized=!1,$(".soliloquy-clone",this).remove(),n.children.each(function(){void 0!=$(this).data("origStyle")?$(this).attr("style",$(this).data("origStyle")):$(this).removeAttr("style")}),void 0!=$(this).data("origStyle")?this.attr("style",$(this).data("origStyle")):$(this).removeAttr("style"),$(this).unwrap().unwrap(),n.controls.el&&n.controls.el.remove(),n.controls.next&&n.controls.next.remove(),n.controls.prev&&n.controls.prev.remove(),n.pagerEl&&n.settings.controls&&n.pagerEl.remove(),$(".soliloquy-caption",this).remove(),n.controls.autoEl&&n.controls.autoEl.remove(),clearInterval(n.interval),n.settings.responsive&&$(window).unbind("resize",F))},o.reloadSlider=function(e){void 0!=e&&(s=e),o.destroySlider(),r()},r(),this}}(jQuery);

;
/* foundation.min.js */

/* 1    */ !function(a,b,c,d){"use strict";function e(a){return("string"==typeof a||a instanceof String)&&(a=a.replace(/^['\\/"]+|(;\s?})+|['\\/"]+$/g,"")),a}var f=function(b){for(var c=b.length,d=a("head");c--;)0===d.has("."+b[c]).length&&d.append('<meta class="'+b[c]+'" />')};f(["foundation-mq-small","foundation-mq-medium","foundation-mq-large","foundation-mq-xlarge","foundation-mq-xxlarge","foundation-data-attribute-namespace"]),a(function(){"undefined"!=typeof FastClick&&"undefined"!=typeof c.body&&FastClick.attach(c.body)});var g=function(b,d){if("string"==typeof b){if(d){var e;if(d.jquery){if(e=d[0],!e)return d}else e=d;return a(e.querySelectorAll(b))}return a(c.querySelectorAll(b))}return a(b,d)},h=function(a){var b=[];return a||b.push("data"),this.namespace.length>0&&b.push(this.namespace),b.push(this.name),b.join("-")},i=function(a){for(var b=a.split("-"),c=b.length,d=[];c--;)0!==c?d.push(b[c]):this.namespace.length>0?d.push(this.namespace,b[c]):d.push(b[c]);return d.reverse().join("-")},j=function(b,c){var d=this,e=!g(this).data(this.attr_name(!0));return g(this.scope).is("["+this.attr_name()+"]")?(g(this.scope).data(this.attr_name(!0)+"-init",a.extend({},this.settings,c||b,this.data_options(g(this.scope)))),e&&this.events(this.scope)):g("["+this.attr_name()+"]",this.scope).each(function(){var e=!g(this).data(d.attr_name(!0)+"-init");g(this).data(d.attr_name(!0)+"-init",a.extend({},d.settings,c||b,d.data_options(g(this)))),e&&d.events(this)}),"string"==typeof b?this[b].call(this,c):void 0},k=function(a,b){function c(){b(a[0])}function d(){if(this.one("load",c),/MSIE (\d+\.\d+);/.test(navigator.userAgent)){var a=this.attr("src"),b=a.match(/\?/)?"&":"?";b+="random="+(new Date).getTime(),this.attr("src",a+b)}}return a.attr("src")?void(a[0].complete||4===a[0].readyState?c():d.call(a)):void c()};b.matchMedia=b.matchMedia||function(a){var b,c=a.documentElement,d=c.firstElementChild||c.firstChild,e=a.createElement("body"),f=a.createElement("div");return f.id="mq-test-1",f.style.cssText="position:absolute;top:-100em",e.style.background="none",e.appendChild(f),function(a){return f.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',c.insertBefore(e,d),b=42===f.offsetWidth,c.removeChild(e),{matches:b,media:a}}}(c),function(){function a(){c&&(f(a),h&&jQuery.fx.tick())}for(var c,d=0,e=["webkit","moz"],f=b.requestAnimationFrame,g=b.cancelAnimationFrame,h="undefined"!=typeof jQuery.fx;d<e.length&&!f;d++)f=b[e[d]+"RequestAnimationFrame"],g=g||b[e[d]+"CancelAnimationFrame"]||b[e[d]+"CancelRequestAnimationFrame"];f?(b.requestAnimationFrame=f,b.cancelAnimationFrame=g,h&&(jQuery.fx.timer=function(b){b()&&jQuery.timers.push(b)&&!c&&(c=!0,a())},jQuery.fx.stop=function(){c=!1})):(b.requestAnimationFrame=function(a){var c=(new Date).getTime(),e=Math.max(0,16-(c-d)),f=b.setTimeout(function(){a(c+e)},e);return d=c+e,f},b.cancelAnimationFrame=function(a){clearTimeout(a)})}(jQuery),b.Foundation={name:"Foundation",version:"5.3.1",media_queries:{small:g(".foundation-mq-small").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),medium:g(".foundation-mq-medium").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),large:g(".foundation-mq-large").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),xlarge:g(".foundation-mq-xlarge").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),xxlarge:g(".foundation-mq-xxlarge").css("font-family").replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,"")},stylesheet:a("<style></style>").appendTo("head")[0].sheet,global:{namespace:d},init:function(a,b,c,d,e){var f=[a,c,d,e],h=[];if(this.rtl=/rtl/i.test(g("html").attr("dir")),this.scope=a||this.scope,this.set_namespace(),b&&"string"==typeof b&&!/reflow/i.test(b))this.libs.hasOwnProperty(b)&&h.push(this.init_lib(b,f));else for(var i in this.libs)h.push(this.init_lib(i,b));return a},init_lib:function(b,c){return this.libs.hasOwnProperty(b)?(this.patch(this.libs[b]),c&&c.hasOwnProperty(b)?("undefined"!=typeof this.libs[b].settings?a.extend(!0,this.libs[b].settings,c[b]):"undefined"!=typeof this.libs[b].defaults&&a.extend(!0,this.libs[b].defaults,c[b]),this.libs[b].init.apply(this.libs[b],[this.scope,c[b]])):(c=c instanceof Array?c:new Array(c),this.libs[b].init.apply(this.libs[b],c))):function(){}},patch:function(a){a.scope=this.scope,a.namespace=this.global.namespace,a.rtl=this.rtl,a.data_options=this.utils.data_options,a.attr_name=h,a.add_namespace=i,a.bindings=j,a.S=this.utils.S},inherit:function(a,b){for(var c=b.split(" "),d=c.length;d--;)this.utils.hasOwnProperty(c[d])&&(a[c[d]]=this.utils[c[d]])},set_namespace:function(){var b=this.global.namespace===d?a(".foundation-data-attribute-namespace").css("font-family"):this.global.namespace;this.global.namespace=b===d||/false/i.test(b)?"":b},libs:{},utils:{S:g,throttle:function(a,b){var c=null;return function(){var d=this,e=arguments;null==c&&(c=setTimeout(function(){a.apply(d,e),c=null},b))}},debounce:function(a,b,c){var d,e;return function(){var f=this,g=arguments,h=function(){d=null,c||(e=a.apply(f,g))},i=c&&!d;return clearTimeout(d),d=setTimeout(h,b),i&&(e=a.apply(f,g)),e}},data_options:function(b,c){function d(a){return!isNaN(a-0)&&null!==a&&""!==a&&a!==!1&&a!==!0}function e(b){return"string"==typeof b?a.trim(b):b}c=c||"options";var f,g,h,i={},j=function(a){var b=Foundation.global.namespace;return a.data(b.length>0?b+"-"+c:c)},k=j(b);if("object"==typeof k)return k;for(h=(k||":").split(";"),f=h.length;f--;)g=h[f].split(":"),g=[g[0],g.slice(1).join(":")],/true/i.test(g[1])&&(g[1]=!0),/false/i.test(g[1])&&(g[1]=!1),d(g[1])&&(g[1]=-1===g[1].indexOf(".")?parseInt(g[1],10):parseFloat(g[1])),2===g.length&&g[0].length>0&&(i[e(g[0])]=e(g[1]));return i},register_media:function(b,c){Foundation.media_queries[b]===d&&(a("head").append('<meta class="'+c+'"/>'),Foundation.media_queries[b]=e(a("."+c).css("font-family")))},add_custom_rule:function(a,b){if(b===d&&Foundation.stylesheet)Foundation.stylesheet.insertRule(a,Foundation.stylesheet.cssRules.length);else{var c=Foundation.media_queries[b];c!==d&&Foundation.stylesheet.insertRule("@media "+Foundation.media_queries[b]+"{ "+a+" }")}},image_loaded:function(a,b){var c=this,d=a.length;0===d&&b(a),a.each(function(){k(c.S(this),function(){d-=1,0===d&&b(a)})})},random_str:function(){return this.fidx||(this.fidx=0),this.prefix=this.prefix||[this.name||"F",(+new Date).toString(36)].join("-"),this.prefix+(this.fidx++).toString(36)}}},a.fn.foundation=function(){var a=Array.prototype.slice.call(arguments,0);return this.each(function(){return Foundation.init.apply(Foundation,[this].concat(a)),this})}}(jQuery,window,window.document),function(a,b,c){"use strict";Foundation.libs.abide={name:"abide",version:"5.3.3",settings:{live_validate:!0,focus_on_invalid:!0,error_labels:!0,timeout:1e3,patterns:{alpha:/^[a-zA-Z]+$/,alpha_numeric:/^[a-zA-Z0-9]+$/,integer:/^[-+]?\d+$/,number:/^[-+]?\d*(?:[\.\,]\d+)?$/,card:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,cvv:/^([0-9]){3,4}$/,email:/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,url:/^(https?|ftp|file|ssh):\/\/(((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/,domain:/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/,datetime:/^([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9])T([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])(Z|([\-\+]([0-1][0-9])\:00))$/,date:/(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))$/,time:/^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/,dateISO:/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/,month_day_year:/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.]\d{4}$/,color:/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/},validators:{equalTo:function(a){var b=c.getElementById(a.getAttribute(this.add_namespace("data-equalto"))).value,d=a.value,e=b===d;return e}}},timer:null,init:function(a,b,c){this.bindings(b,c)},events:function(b){var c=this,d=c.S(b).attr("novalidate","novalidate"),e=d.data(this.attr_name(!0)+"-init")||{};this.invalid_attr=this.add_namespace("data-invalid"),d.off(".abide").on("submit.fndtn.abide validate.fndtn.abide",function(a){var b=/ajax/i.test(c.S(this).attr(c.attr_name()));return c.validate(c.S(this).find("input, textarea, select").get(),a,b)}).on("reset",function(){return c.reset(a(this))}).find("input, textarea, select").off(".abide").on("blur.fndtn.abide change.fndtn.abide",function(a){c.validate([this],a)}).on("keydown.fndtn.abide",function(a){e.live_validate===!0&&(clearTimeout(c.timer),c.timer=setTimeout(function(){c.validate([this],a)}.bind(this),e.timeout))})},reset:function(b){b.removeAttr(this.invalid_attr),a(this.invalid_attr,b).removeAttr(this.invalid_attr),a(".error",b).not("small").removeClass("error")},validate:function(a,b,c){for(var d=this.parse_patterns(a),e=d.length,f=this.S(a[0]).closest("form"),g=/submit/.test(b.type),h=0;e>h;h++)if(!d[h]&&(g||c))return this.settings.focus_on_invalid&&a[h].focus(),f.trigger("invalid"),this.S(a[h]).closest("form").attr(this.invalid_attr,""),!1;return(g||c)&&f.trigger("valid"),f.removeAttr(this.invalid_attr),c?!1:!0},parse_patterns:function(a){for(var b=a.length,c=[];b--;)c.push(this.pattern(a[b]));return this.check_validation_and_apply_styles(c)},pattern:function(a){var b=a.getAttribute("type"),c="string"==typeof a.getAttribute("required"),d=a.getAttribute("pattern")||"";return this.settings.patterns.hasOwnProperty(d)&&d.length>0?[a,this.settings.patterns[d],c]:d.length>0?[a,new RegExp(d),c]:this.settings.patterns.hasOwnProperty(b)?[a,this.settings.patterns[b],c]:(d=/.*/,[a,d,c])},check_validation_and_apply_styles:function(b){var c=b.length,d=[],e=this.S(b[0][0]).closest("[data-"+this.attr_name(!0)+"]");for(e.data(this.attr_name(!0)+"-init")||{};c--;){var f,g,h=b[c][0],i=b[c][2],j=h.value.trim(),k=this.S(h).parent(),l=h.getAttribute(this.add_namespace("data-abide-validator")),m="radio"===h.type,n="checkbox"===h.type,o=this.S('label[for="'+h.getAttribute("id")+'"]'),p=i?h.value.length>0:!0;h.getAttribute(this.add_namespace("data-equalto"))&&(l="equalTo"),f=k.is("label")?k.parent():k,m&&i?d.push(this.valid_radio(h,i)):n&&i?d.push(this.valid_checkbox(h,i)):(l&&(g=this.settings.validators[l].apply(this,[h,i,f]),d.push(g)),d.push(b[c][1].test(j)&&p||!i&&h.value.length<1||a(h).attr("disabled")?!0:!1),d=[d.every(function(a){return a})],d[0]?(this.S(h).removeAttr(this.invalid_attr),f.removeClass("error"),o.length>0&&this.settings.error_labels&&o.removeClass("error"),a(h).triggerHandler("valid")):(f.addClass("error"),this.S(h).attr(this.invalid_attr,""),o.length>0&&this.settings.error_labels&&o.addClass("error"),a(h).triggerHandler("invalid")))}return d},valid_checkbox:function(a,b){var a=this.S(a),c=a.is(":checked")||!b;return c?a.removeAttr(this.invalid_attr).parent().removeClass("error"):a.attr(this.invalid_attr,"").parent().addClass("error"),c},valid_radio:function(a){for(var b=a.getAttribute("name"),c=this.S(a).closest("[data-"+this.attr_name(!0)+"]").find("[name='"+b+"']"),d=c.length,e=!1,f=0;d>f;f++)c[f].checked&&(e=!0);for(var f=0;d>f;f++)e?this.S(c[f]).removeAttr(this.invalid_attr).parent().removeClass("error"):this.S(c[f]).attr(this.invalid_attr,"").parent().addClass("error");return e},valid_equal:function(a,b,d){var e=c.getElementById(a.getAttribute(this.add_namespace("data-equalto"))).value,f=a.value,g=e===f;return g?(this.S(a).removeAttr(this.invalid_attr),d.removeClass("error")):(this.S(a).attr(this.invalid_attr,""),d.addClass("error")),g},valid_oneof:function(a,b,c,d){var a=this.S(a),e=this.S("["+this.add_namespace("data-oneof")+"]"),f=e.filter(":checked").length>0;if(f?a.removeAttr(this.invalid_attr).parent().removeClass("error"):a.attr(this.invalid_attr,"").parent().addClass("error"),!d){var g=this;e.each(function(){g.valid_oneof.call(g,this,null,null,!0)})}return f}}}(jQuery,window,window.document),function(a){"use strict";Foundation.libs.accordion={name:"accordion",version:"5.3.3",settings:{active_class:"active",multi_expand:!1,toggleable:!0,callback:function(){}},init:function(a,b,c){this.bindings(b,c)},events:function(){var b=this,c=this.S;c(this.scope).off(".fndtn.accordion").on("click.fndtn.accordion","["+this.attr_name()+"] > dd > a",function(d){var e=c(this).closest("["+b.attr_name()+"]"),f=b.attr_name()+"="+e.attr(b.attr_name()),g=e.data(b.attr_name(!0)+"-init"),h=c("#"+this.href.split("#")[1]),i=a("> dd",e),j=i.children(".content"),k=j.filter("."+g.active_class);return d.preventDefault(),e.attr(b.attr_name())&&(j=j.add("["+f+"] dd > .content"),i=i.add("["+f+"] dd")),g.toggleable&&h.is(k)?(h.parent("dd").toggleClass(g.active_class,!1),h.toggleClass(g.active_class,!1),g.callback(h),h.triggerHandler("toggled",[e]),void e.triggerHandler("toggled",[h])):(g.multi_expand||(j.removeClass(g.active_class),i.removeClass(g.active_class)),h.addClass(g.active_class).parent().addClass(g.active_class),g.callback(h),h.triggerHandler("toggled",[e]),void e.triggerHandler("toggled",[h]))})},off:function(){},reflow:function(){}}}(jQuery,window,window.document),function(a){"use strict";Foundation.libs.alert={name:"alert",version:"5.3.3",settings:{callback:function(){}},init:function(a,b,c){this.bindings(b,c)},events:function(){var b=this,c=this.S;a(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(a){var d=c(this).closest("["+b.attr_name()+"]"),e=d.data(b.attr_name(!0)+"-init")||b.settings;a.preventDefault(),Modernizr.csstransitions?(d.addClass("alert-close"),d.on("transitionend webkitTransitionEnd oTransitionEnd",function(){c(this).trigger("close").trigger("close.fndtn.alert").remove(),e.callback()})):d.fadeOut(300,function(){c(this).trigger("close").trigger("close.fndtn.alert").remove(),e.callback()})})},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.clearing={name:"clearing",version:"5.3.3",settings:{templates:{viewing:'<a href="#" class="clearing-close">&times;</a><div class="visible-img" style="display: none"><div class="clearing-touch-label"></div><img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" /><p class="clearing-caption"></p><a href="#" class="clearing-main-prev"><span></span></a><a href="#" class="clearing-main-next"><span></span></a></div>'},close_selectors:".clearing-close, div.clearing-blackout",open_selectors:"",skip_selector:"",touch_label:"",init:!1,locked:!1},init:function(a,b,c){var d=this;Foundation.inherit(this,"throttle image_loaded"),this.bindings(b,c),d.S(this.scope).is("["+this.attr_name()+"]")?this.assemble(d.S("li",this.scope)):d.S("["+this.attr_name()+"]",this.scope).each(function(){d.assemble(d.S("li",this))})},events:function(d){var e=this,f=e.S,g=a(".scroll-container");g.length>0&&(this.scope=g),f(this.scope).off(".clearing").on("click.fndtn.clearing","ul["+this.attr_name()+"] li "+this.settings.open_selectors,function(a,b,c){var b=b||f(this),c=c||b,d=b.next("li"),g=b.closest("["+e.attr_name()+"]").data(e.attr_name(!0)+"-init"),h=f(a.target);a.preventDefault(),g||(e.init(),g=b.closest("["+e.attr_name()+"]").data(e.attr_name(!0)+"-init")),c.hasClass("visible")&&b[0]===c[0]&&d.length>0&&e.is_open(b)&&(c=d,h=f("img",c)),e.open(h,b,c),e.update_paddles(c)}).on("click.fndtn.clearing",".clearing-main-next",function(a){e.nav(a,"next")}).on("click.fndtn.clearing",".clearing-main-prev",function(a){e.nav(a,"prev")}).on("click.fndtn.clearing",this.settings.close_selectors,function(a){Foundation.libs.clearing.close(a,this)}),a(c).on("keydown.fndtn.clearing",function(a){e.keydown(a)}),f(b).off(".clearing").on("resize.fndtn.clearing",function(){e.resize()}),this.swipe_events(d)},swipe_events:function(){var a=this,b=a.S;b(this.scope).on("touchstart.fndtn.clearing",".visible-img",function(a){a.touches||(a=a.originalEvent);var c={start_page_x:a.touches[0].pageX,start_page_y:a.touches[0].pageY,start_time:(new Date).getTime(),delta_x:0,is_scrolling:d};b(this).data("swipe-transition",c),a.stopPropagation()}).on("touchmove.fndtn.clearing",".visible-img",function(c){if(c.touches||(c=c.originalEvent),!(c.touches.length>1||c.scale&&1!==c.scale)){var d=b(this).data("swipe-transition");if("undefined"==typeof d&&(d={}),d.delta_x=c.touches[0].pageX-d.start_page_x,Foundation.rtl&&(d.delta_x=-d.delta_x),"undefined"==typeof d.is_scrolling&&(d.is_scrolling=!!(d.is_scrolling||Math.abs(d.delta_x)<Math.abs(c.touches[0].pageY-d.start_page_y))),!d.is_scrolling&&!d.active){c.preventDefault();var e=d.delta_x<0?"next":"prev";d.active=!0,a.nav(c,e)}}}).on("touchend.fndtn.clearing",".visible-img",function(a){b(this).data("swipe-transition",{}),a.stopPropagation()})},assemble:function(b){var c=b.parent();if(!c.parent().hasClass("carousel")){c.after('<div id="foundationClearingHolder"></div>');var d=c.detach(),e="";if(null!=d[0]){e=d[0].outerHTML;var f=this.S("#foundationClearingHolder"),g=c.data(this.attr_name(!0)+"-init"),h={grid:'<div class="carousel">'+e+"</div>",viewing:g.templates.viewing},i='<div class="clearing-assembled"><div>'+h.viewing+h.grid+"</div></div>",j=this.settings.touch_label;Modernizr.touch&&(i=a(i).find(".clearing-touch-label").html(j).end()),f.after(i).remove()}}},open:function(b,d,e){function f(){setTimeout(function(){this.image_loaded(m,function(){1!==m.outerWidth()||o?g.call(this,m):f.call(this)}.bind(this))}.bind(this),100)}function g(b){var c=a(b);c.css("visibility","visible"),i.css("overflow","hidden"),j.addClass("clearing-blackout"),k.addClass("clearing-container"),l.show(),this.fix_height(e).caption(h.S(".clearing-caption",l),h.S("img",e)).center_and_label(b,n).shift(d,e,function(){e.closest("li").siblings().removeClass("visible"),e.closest("li").addClass("visible")}),l.trigger("opened.fndtn.clearing")}var h=this,i=a(c.body),j=e.closest(".clearing-assembled"),k=h.S("div",j).first(),l=h.S(".visible-img",k),m=h.S("img",l).not(b),n=h.S(".clearing-touch-label",k),o=!1;a("body").on("touchmove",function(a){a.preventDefault()}),m.error(function(){o=!0}),this.locked()||(l.trigger("open.fndtn.clearing"),m.attr("src",this.load(b)).css("visibility","hidden"),f.call(this))},close:function(b,d){b.preventDefault();var e,f,g=function(a){return/blackout/.test(a.selector)?a:a.closest(".clearing-blackout")}(a(d)),h=a(c.body);return d===b.target&&g&&(h.css("overflow",""),e=a("div",g).first(),f=a(".visible-img",e),f.trigger("close.fndtn.clearing"),this.settings.prev_index=0,a("ul["+this.attr_name()+"]",g).attr("style","").closest(".clearing-blackout").removeClass("clearing-blackout"),e.removeClass("clearing-container"),f.hide(),f.trigger("closed.fndtn.clearing")),a("body").off("touchmove"),!1},is_open:function(a){return a.parent().prop("style").length>0},keydown:function(b){var c=a(".clearing-blackout ul["+this.attr_name()+"]"),d=this.rtl?37:39,e=this.rtl?39:37,f=27;b.which===d&&this.go(c,"next"),b.which===e&&this.go(c,"prev"),b.which===f&&this.S("a.clearing-close").trigger("click").trigger("click.fndtn.clearing")},nav:function(b,c){var d=a("ul["+this.attr_name()+"]",".clearing-blackout");b.preventDefault(),this.go(d,c)},resize:function(){var b=a("img",".clearing-blackout .visible-img"),c=a(".clearing-touch-label",".clearing-blackout");b.length&&(this.center_and_label(b,c),b.trigger("resized.fndtn.clearing"))},fix_height:function(a){var b=a.parent().children(),c=this;return b.each(function(){var a=c.S(this),b=a.find("img");a.height()>b.outerHeight()&&a.addClass("fix-height")}).closest("ul").width(100*b.length+"%"),this},update_paddles:function(a){a=a.closest("li");var b=a.closest(".carousel").siblings(".visible-img");a.next().length>0?this.S(".clearing-main-next",b).removeClass("disabled"):this.S(".clearing-main-next",b).addClass("disabled"),a.prev().length>0?this.S(".clearing-main-prev",b).removeClass("disabled"):this.S(".clearing-main-prev",b).addClass("disabled")},center_and_label:function(a,b){return this.rtl?(a.css({marginRight:-(a.outerWidth()/2),marginTop:-(a.outerHeight()/2),left:"auto",right:"50%"}),b.length>0&&b.css({marginRight:-(b.outerWidth()/2),marginTop:-(a.outerHeight()/2)-b.outerHeight()-10,left:"auto",right:"50%"})):(a.css({marginLeft:-(a.outerWidth()/2),marginTop:-(a.outerHeight()/2)}),b.length>0&&b.css({marginLeft:-(b.outerWidth()/2),marginTop:-(a.outerHeight()/2)-b.outerHeight()-10})),this},load:function(a){var b;return b="A"===a[0].nodeName?a.attr("href"):a.parent().attr("href"),this.preload(a),b?b:a.attr("src")},preload:function(a){this.img(a.closest("li").next()).img(a.closest("li").prev())},img:function(a){if(a.length){var b=new Image,c=this.S("a",a);b.src=c.length?c.attr("href"):this.S("img",a).attr("src")}return this},caption:function(a,b){var c=b.attr("data-caption");return c?a.html(c).show():a.text("").hide(),this},go:function(a,b){var c=this.S(".visible",a),d=c[b]();this.settings.skip_selector&&0!=d.find(this.settings.skip_selector).length&&(d=d[b]()),d.length&&this.S("img",d).trigger("click",[c,d]).trigger("click.fndtn.clearing",[c,d]).trigger("change.fndtn.clearing")},shift:function(a,b,c){var d,e=b.parent(),f=this.settings.prev_index||b.index(),g=this.direction(e,a,b),h=this.rtl?"right":"left",i=parseInt(e.css("left"),10),j=b.outerWidth(),k={};b.index()===f||/skip/.test(g)?/skip/.test(g)&&(d=b.index()-this.settings.up_count,this.lock(),d>0?(k[h]=-(d*j),e.animate(k,300,this.unlock())):(k[h]=0,e.animate(k,300,this.unlock()))):/left/.test(g)?(this.lock(),k[h]=i+j,e.animate(k,300,this.unlock())):/right/.test(g)&&(this.lock(),k[h]=i-j,e.animate(k,300,this.unlock())),c()},direction:function(a,b,c){var d,e=this.S("li",a),f=e.outerWidth()+e.outerWidth()/4,g=Math.floor(this.S(".clearing-container").outerWidth()/f)-1,h=e.index(c);return this.settings.up_count=g,d=this.adjacent(this.settings.prev_index,h)?h>g&&h>this.settings.prev_index?"right":h>g-1&&h<=this.settings.prev_index?"left":!1:"skip",this.settings.prev_index=h,d},adjacent:function(a,b){for(var c=b+1;c>=b-1;c--)if(c===a)return!0;return!1},lock:function(){this.settings.locked=!0},unlock:function(){this.settings.locked=!1},locked:function(){return this.settings.locked},off:function(){this.S(this.scope).off(".fndtn.clearing"),this.S(b).off(".fndtn.clearing")},reflow:function(){this.init()}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.dropdown={name:"dropdown",version:"5.3.3",settings:{active_class:"open",align:"bottom",is_hover:!1,opened:function(){},closed:function(){}},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c)},events:function(){var c=this,d=c.S;d(this.scope).off(".dropdown").on("click.fndtn.dropdown","["+this.attr_name()+"]",function(b){var e=d(this).data(c.attr_name(!0)+"-init")||c.settings;(!e.is_hover||Modernizr.touch)&&(b.preventDefault(),c.toggle(a(this)))}).on("mouseenter.fndtn.dropdown","["+this.attr_name()+"], ["+this.attr_name()+"-content]",function(a){var b,e,f=d(this);clearTimeout(c.timeout),f.data(c.data_attr())?(b=d("#"+f.data(c.data_attr())),e=f):(b=f,e=d("["+c.attr_name()+"='"+b.attr("id")+"']"));var g=e.data(c.attr_name(!0)+"-init")||c.settings;d(a.target).data(c.data_attr())&&g.is_hover&&c.closeall.call(c),g.is_hover&&c.open.apply(c,[b,e])}).on("mouseleave.fndtn.dropdown","["+this.attr_name()+"], ["+this.attr_name()+"-content]",function(){var a=d(this);c.timeout=setTimeout(function(){if(a.data(c.data_attr())){var b=a.data(c.data_attr(!0)+"-init")||c.settings;b.is_hover&&c.close.call(c,d("#"+a.data(c.data_attr())))}else{var e=d("["+c.attr_name()+'="'+d(this).attr("id")+'"]'),b=e.data(c.attr_name(!0)+"-init")||c.settings;b.is_hover&&c.close.call(c,a)}}.bind(this),150)}).on("click.fndtn.dropdown",function(b){var e=d(b.target).closest("["+c.attr_name()+"-content]");if(!(d(b.target).closest("["+c.attr_name()+"]").length>0))return!d(b.target).data("revealId")&&e.length>0&&(d(b.target).is("["+c.attr_name()+"-content]")||a.contains(e.first()[0],b.target))?void b.stopPropagation():void c.close.call(c,d("["+c.attr_name()+"-content]"))}).on("opened.fndtn.dropdown","["+c.attr_name()+"-content]",function(){c.settings.opened.call(this)}).on("closed.fndtn.dropdown","["+c.attr_name()+"-content]",function(){c.settings.closed.call(this)}),d(b).off(".dropdown").on("resize.fndtn.dropdown",c.throttle(function(){c.resize.call(c)},50)),this.resize()},close:function(a){var b=this;a.each(function(){b.S(this).hasClass(b.settings.active_class)&&(b.S(this).css(Foundation.rtl?"right":"left","-99999px").removeClass(b.settings.active_class).prev("["+b.attr_name()+"]").removeClass(b.settings.active_class).removeData("target"),b.S(this).trigger("closed").trigger("closed.fndtn.dropdown",[a]))})},closeall:function(){var b=this;a.each(b.S("["+this.attr_name()+"-content]"),function(){b.close.call(b,b.S(this))})},open:function(a,b){this.css(a.addClass(this.settings.active_class),b),a.prev("["+this.attr_name()+"]").addClass(this.settings.active_class),a.data("target",b.get(0)).trigger("opened").trigger("opened.fndtn.dropdown",[a,b])},data_attr:function(){return this.namespace.length>0?this.namespace+"-"+this.name:this.name},toggle:function(a){var b=this.S("#"+a.data(this.data_attr()));0!==b.length&&(this.close.call(this,this.S("["+this.attr_name()+"-content]").not(b)),b.hasClass(this.settings.active_class)?(this.close.call(this,b),b.data("target")!==a.get(0)&&this.open.call(this,b,a)):this.open.call(this,b,a))},resize:function(){var a=this.S("["+this.attr_name()+"-content].open"),b=this.S("["+this.attr_name()+"='"+a.attr("id")+"']");a.length&&b.length&&this.css(a,b)},css:function(a,b){var c=Math.max((b.width()-a.width())/2,8);if(this.clear_idx(),this.small()){var d=this.dirs.bottom.call(a,b);a.attr("style","").removeClass("drop-left drop-right drop-top").css({position:"absolute",width:"95%","max-width":"none",top:d.top}),a.css(Foundation.rtl?"right":"left",c)}else{var e=b.data(this.attr_name(!0)+"-init")||this.settings;this.style(a,b,e)}return a},style:function(b,c,d){var e=a.extend({position:"absolute"},this.dirs[d.align].call(b,c,d));b.attr("style","").css(e)},dirs:{_base:function(a){var b=this.offsetParent(),c=b.offset(),d=a.offset();return d.top-=c.top,d.left-=c.left,d},top:function(a){var b=Foundation.libs.dropdown,c=b.dirs._base.call(this,a),d=8;return this.addClass("drop-top"),(a.outerWidth()<this.outerWidth()||b.small())&&b.adjust_pip(d,c),Foundation.rtl?{left:c.left-this.outerWidth()+a.outerWidth(),top:c.top-this.outerHeight()}:{left:c.left,top:c.top-this.outerHeight()}},bottom:function(a){var b=Foundation.libs.dropdown,c=b.dirs._base.call(this,a),d=8;return(a.outerWidth()<this.outerWidth()||b.small())&&b.adjust_pip(d,c),b.rtl?{left:c.left-this.outerWidth()+a.outerWidth(),top:c.top+a.outerHeight()}:{left:c.left,top:c.top+a.outerHeight()}},left:function(a){var b=Foundation.libs.dropdown.dirs._base.call(this,a);return this.addClass("drop-left"),{left:b.left-this.outerWidth(),top:b.top}},right:function(a){var b=Foundation.libs.dropdown.dirs._base.call(this,a);return this.addClass("drop-right"),{left:b.left+a.outerWidth(),top:b.top}}},adjust_pip:function(a,b){var c=Foundation.stylesheet;this.small()&&(a+=b.left-8),this.rule_idx=c.cssRules.length;var d=".f-dropdown.open:before",e=".f-dropdown.open:after",f="left: "+a+"px;",g="left: "+(a-1)+"px;";c.insertRule?(c.insertRule([d,"{",f,"}"].join(" "),this.rule_idx),c.insertRule([e,"{",g,"}"].join(" "),this.rule_idx+1)):(c.addRule(d,f,this.rule_idx),c.addRule(e,g,this.rule_idx+1))},clear_idx:function(){var a=Foundation.stylesheet;this.rule_idx&&(a.deleteRule(this.rule_idx),a.deleteRule(this.rule_idx),delete this.rule_idx)},small:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},off:function(){this.S(this.scope).off(".fndtn.dropdown"),this.S("html, body").off(".fndtn.dropdown"),this.S(b).off(".fndtn.dropdown"),this.S("[data-dropdown-content]").off(".fndtn.dropdown")},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.equalizer={name:"equalizer",version:"5.3.3",settings:{use_tallest:!0,before_height_change:a.noop,after_height_change:a.noop,equalize_on_stack:!1},init:function(a,b,c){Foundation.inherit(this,"image_loaded"),this.bindings(b,c),this.reflow()},events:function(){this.S(b).off(".equalizer").on("resize.fndtn.equalizer",function(){this.reflow()}.bind(this))},equalize:function(b){var c=!1,d=b.find("["+this.attr_name()+"-watch]:visible"),e=b.data(this.attr_name(!0)+"-init");if(0!==d.length){var f=d.first().offset().top;if(e.before_height_change(),b.trigger("before-height-change").trigger("before-height-change.fndth.equalizer"),d.height("inherit"),d.each(function(){var b=a(this);b.offset().top!==f&&(c=!0)}),e.equalize_on_stack!==!1||!c){var g=d.map(function(){return a(this).outerHeight(!1)}).get();if(e.use_tallest){var h=Math.max.apply(null,g);d.css("height",h)}else{var i=Math.min.apply(null,g);d.css("height",i)}e.after_height_change(),b.trigger("after-height-change").trigger("after-height-change.fndtn.equalizer")}}},reflow:function(){var b=this;this.S("["+this.attr_name()+"]",this.scope).each(function(){var c=a(this);b.image_loaded(b.S("img",this),function(){b.equalize(c)})})}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.interchange={name:"interchange",version:"5.3.3",cache:{},images_loaded:!1,nodes_loaded:!1,settings:{load_attr:"interchange",named_queries:{"default":"only screen",small:Foundation.media_queries.small,medium:Foundation.media_queries.medium,large:Foundation.media_queries.large,xlarge:Foundation.media_queries.xlarge,xxlarge:Foundation.media_queries.xxlarge,landscape:"only screen and (orientation: landscape)",portrait:"only screen and (orientation: portrait)",retina:"only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx)"},directives:{replace:function(b,c,d){if(/IMG/.test(b[0].nodeName)){var e=b[0].src;if(new RegExp(c,"i").test(e))return;return b[0].src=c,d(b[0].src)}var f=b.data(this.data_attr+"-last-path"),g=this;if(f!=c)return/\.(gif|jpg|jpeg|tiff|png)([?#].*)?/i.test(c)?(a(b).css("background-image","url("+c+")"),b.data("interchange-last-path",c),d(c)):a.get(c,function(a){b.html(a),b.data(g.data_attr+"-last-path",c),d()})}}},init:function(b,c,d){Foundation.inherit(this,"throttle random_str"),this.data_attr=this.set_data_attr(),a.extend(!0,this.settings,c,d),this.bindings(c,d),this.load("images"),this.load("nodes")},get_media_hash:function(){var a="";for(var b in this.settings.named_queries)a+=matchMedia(this.settings.named_queries[b]).matches.toString();return a},events:function(){var c,d=this;return a(b).off(".interchange").on("resize.fndtn.interchange",d.throttle(function(){var a=d.get_media_hash();
/* 2    */ a!==c&&d.resize(),c=a},50)),this},resize:function(){var b=this.cache;if(!this.images_loaded||!this.nodes_loaded)return void setTimeout(a.proxy(this.resize,this),50);for(var c in b)if(b.hasOwnProperty(c)){var d=this.results(c,b[c]);d&&this.settings.directives[d.scenario[1]].call(this,d.el,d.scenario[0],function(){if(arguments[0]instanceof Array)var a=arguments[0];else var a=Array.prototype.slice.call(arguments,0);d.el.trigger(d.scenario[1],a)})}},results:function(a,b){var c=b.length;if(c>0)for(var d=this.S("["+this.add_namespace("data-uuid")+'="'+a+'"]');c--;){var e,f=b[c][2];if(e=matchMedia(this.settings.named_queries.hasOwnProperty(f)?this.settings.named_queries[f]:f),e.matches)return{el:d,scenario:b[c]}}return!1},load:function(a,b){return("undefined"==typeof this["cached_"+a]||b)&&this["update_"+a](),this["cached_"+a]},update_images:function(){var a=this.S("img["+this.data_attr+"]"),b=a.length,c=b,d=0,e=this.data_attr;for(this.cache={},this.cached_images=[],this.images_loaded=0===b;c--;){if(d++,a[c]){var f=a[c].getAttribute(e)||"";f.length>0&&this.cached_images.push(a[c])}d===b&&(this.images_loaded=!0,this.enhance("images"))}return this},update_nodes:function(){var a=this.S("["+this.data_attr+"]").not("img"),b=a.length,c=b,d=0,e=this.data_attr;for(this.cached_nodes=[],this.nodes_loaded=0===b;c--;){d++;var f=a[c].getAttribute(e)||"";f.length>0&&this.cached_nodes.push(a[c]),d===b&&(this.nodes_loaded=!0,this.enhance("nodes"))}return this},enhance:function(c){for(var d=this["cached_"+c].length;d--;)this.object(a(this["cached_"+c][d]));return a(b).trigger("resize").trigger("resize.fndtn.interchange")},convert_directive:function(a){var b=this.trim(a);return b.length>0?b:"replace"},parse_scenario:function(a){var b=a[0].match(/(.+),\s*(\w+)\s*$/),c=a[1];if(b)var d=b[1],e=b[2];else var f=a[0].split(/,\s*$/),d=f[0],e="";return[this.trim(d),this.convert_directive(e),this.trim(c)]},object:function(a){var b=this.parse_data_attr(a),c=[],d=b.length;if(d>0)for(;d--;){var e=b[d].split(/\((.*?)(\))$/);if(e.length>1){var f=this.parse_scenario(e);c.push(f)}}return this.store(a,c)},store:function(a,b){var c=this.random_str(),d=a.data(this.add_namespace("uuid",!0));return this.cache[d]?this.cache[d]:(a.attr(this.add_namespace("data-uuid"),c),this.cache[c]=b)},trim:function(b){return"string"==typeof b?a.trim(b):b},set_data_attr:function(a){return a?this.namespace.length>0?this.namespace+"-"+this.settings.load_attr:this.settings.load_attr:this.namespace.length>0?"data-"+this.namespace+"-"+this.settings.load_attr:"data-"+this.settings.load_attr},parse_data_attr:function(a){for(var b=a.attr(this.attr_name()).split(/\[(.*?)\]/),c=b.length,d=[];c--;)b[c].replace(/[\W\d]+/,"").length>4&&d.push(b[c]);return d},reflow:function(){this.load("images",!0),this.load("nodes",!0)}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.joyride={name:"joyride",version:"5.3.3",defaults:{expose:!1,modal:!0,tip_location:"bottom",nub_position:"auto",scroll_speed:1500,scroll_animation:"linear",timer:0,start_timer_on_click:!0,start_offset:0,next_button:!0,prev_button:!0,tip_animation:"fade",pause_after:[],exposed:[],tip_animation_fade_speed:300,cookie_monster:!1,cookie_name:"joyride",cookie_domain:!1,cookie_expires:365,tip_container:"body",abort_on_close:!0,tip_location_patterns:{top:["bottom"],bottom:[],left:["right","top","bottom"],right:["left","top","bottom"]},post_ride_callback:function(){},post_step_callback:function(){},pre_step_callback:function(){},pre_ride_callback:function(){},post_expose_callback:function(){},template:{link:'<a href="#close" class="joyride-close-tip">&times;</a>',timer:'<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',tip:'<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',wrapper:'<div class="joyride-content-wrapper"></div>',button:'<a href="#" class="small button joyride-next-tip"></a>',prev_button:'<a href="#" class="small button joyride-prev-tip"></a>',modal:'<div class="joyride-modal-bg"></div>',expose:'<div class="joyride-expose-wrapper"></div>',expose_cover:'<div class="joyride-expose-cover"></div>'},expose_add_class:""},init:function(b,c,d){Foundation.inherit(this,"throttle random_str"),this.settings=this.settings||a.extend({},this.defaults,d||c),this.bindings(c,d)},events:function(){var c=this;a(this.scope).off(".joyride").on("click.fndtn.joyride",".joyride-next-tip, .joyride-modal-bg",function(a){a.preventDefault(),this.settings.$li.next().length<1?this.end():this.settings.timer>0?(clearTimeout(this.settings.automate),this.hide(),this.show(),this.startTimer()):(this.hide(),this.show())}.bind(this)).on("click.fndtn.joyride",".joyride-prev-tip",function(a){a.preventDefault(),this.settings.$li.prev().length<1||(this.settings.timer>0?(clearTimeout(this.settings.automate),this.hide(),this.show(null,!0),this.startTimer()):(this.hide(),this.show(null,!0)))}.bind(this)).on("click.fndtn.joyride",".joyride-close-tip",function(a){a.preventDefault(),this.end(this.settings.abort_on_close)}.bind(this)),a(b).off(".joyride").on("resize.fndtn.joyride",c.throttle(function(){if(a("["+c.attr_name()+"]").length>0&&c.settings.$next_tip){if(c.settings.exposed.length>0){var b=a(c.settings.exposed);b.each(function(){var b=a(this);c.un_expose(b),c.expose(b)})}c.is_phone()?c.pos_phone():c.pos_default(!1)}},100))},start:function(){var b=this,c=a("["+this.attr_name()+"]",this.scope),d=["timer","scrollSpeed","startOffset","tipAnimationFadeSpeed","cookieExpires"],e=d.length;!c.length>0||(this.settings.init||this.events(),this.settings=c.data(this.attr_name(!0)+"-init"),this.settings.$content_el=c,this.settings.$body=a(this.settings.tip_container),this.settings.body_offset=a(this.settings.tip_container).position(),this.settings.$tip_content=this.settings.$content_el.find("> li"),this.settings.paused=!1,this.settings.attempts=0,"function"!=typeof a.cookie&&(this.settings.cookie_monster=!1),(!this.settings.cookie_monster||this.settings.cookie_monster&&!a.cookie(this.settings.cookie_name))&&(this.settings.$tip_content.each(function(c){var f=a(this);this.settings=a.extend({},b.defaults,b.data_options(f));for(var g=e;g--;)b.settings[d[g]]=parseInt(b.settings[d[g]],10);b.create({$li:f,index:c})}),!this.settings.start_timer_on_click&&this.settings.timer>0?(this.show("init"),this.startTimer()):this.show("init")))},resume:function(){this.set_li(),this.show()},tip_template:function(b){var c,d;return b.tip_class=b.tip_class||"",c=a(this.settings.template.tip).addClass(b.tip_class),d=a.trim(a(b.li).html())+this.prev_button_text(b.prev_button_text,b.index)+this.button_text(b.button_text)+this.settings.template.link+this.timer_instance(b.index),c.append(a(this.settings.template.wrapper)),c.first().attr(this.add_namespace("data-index"),b.index),a(".joyride-content-wrapper",c).append(d),c[0]},timer_instance:function(b){var c;return c=0===b&&this.settings.start_timer_on_click&&this.settings.timer>0||0===this.settings.timer?"":a(this.settings.template.timer)[0].outerHTML},button_text:function(b){return this.settings.tip_settings.next_button?(b=a.trim(b)||"Next",b=a(this.settings.template.button).append(b)[0].outerHTML):b="",b},prev_button_text:function(b,c){return this.settings.tip_settings.prev_button?(b=a.trim(b)||"Previous",b=0==c?a(this.settings.template.prev_button).append(b).addClass("disabled")[0].outerHTML:a(this.settings.template.prev_button).append(b)[0].outerHTML):b="",b},create:function(b){this.settings.tip_settings=a.extend({},this.settings,this.data_options(b.$li));var c=b.$li.attr(this.add_namespace("data-button"))||b.$li.attr(this.add_namespace("data-text")),d=b.$li.attr(this.add_namespace("data-button-prev"))||b.$li.attr(this.add_namespace("data-prev-text")),e=b.$li.attr("class"),f=a(this.tip_template({tip_class:e,index:b.index,button_text:c,prev_button_text:d,li:b.$li}));a(this.settings.tip_container).append(f)},show:function(b,c){var e=null;this.settings.$li===d||-1===a.inArray(this.settings.$li.index(),this.settings.pause_after)?(this.settings.paused?this.settings.paused=!1:this.set_li(b,c),this.settings.attempts=0,this.settings.$li.length&&this.settings.$target.length>0?(b&&(this.settings.pre_ride_callback(this.settings.$li.index(),this.settings.$next_tip),this.settings.modal&&this.show_modal()),this.settings.pre_step_callback(this.settings.$li.index(),this.settings.$next_tip),this.settings.modal&&this.settings.expose&&this.expose(),this.settings.tip_settings=a.extend({},this.settings,this.data_options(this.settings.$li)),this.settings.timer=parseInt(this.settings.timer,10),this.settings.tip_settings.tip_location_pattern=this.settings.tip_location_patterns[this.settings.tip_settings.tip_location],/body/i.test(this.settings.$target.selector)||this.scroll_to(),this.is_phone()?this.pos_phone(!0):this.pos_default(!0),e=this.settings.$next_tip.find(".joyride-timer-indicator"),/pop/i.test(this.settings.tip_animation)?(e.width(0),this.settings.timer>0?(this.settings.$next_tip.show(),setTimeout(function(){e.animate({width:e.parent().width()},this.settings.timer,"linear")}.bind(this),this.settings.tip_animation_fade_speed)):this.settings.$next_tip.show()):/fade/i.test(this.settings.tip_animation)&&(e.width(0),this.settings.timer>0?(this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed).show(),setTimeout(function(){e.animate({width:e.parent().width()},this.settings.timer,"linear")}.bind(this),this.settings.tip_animation_fade_speed)):this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed)),this.settings.$current_tip=this.settings.$next_tip):this.settings.$li&&this.settings.$target.length<1?this.show():this.end()):this.settings.paused=!0},is_phone:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},hide:function(){this.settings.modal&&this.settings.expose&&this.un_expose(),this.settings.modal||a(".joyride-modal-bg").hide(),this.settings.$current_tip.css("visibility","hidden"),setTimeout(a.proxy(function(){this.hide(),this.css("visibility","visible")},this.settings.$current_tip),0),this.settings.post_step_callback(this.settings.$li.index(),this.settings.$current_tip)},set_li:function(a,b){a?(this.settings.$li=this.settings.$tip_content.eq(this.settings.start_offset),this.set_next_tip(),this.settings.$current_tip=this.settings.$next_tip):(this.settings.$li=b?this.settings.$li.prev():this.settings.$li.next(),this.set_next_tip()),this.set_target()},set_next_tip:function(){this.settings.$next_tip=a(".joyride-tip-guide").eq(this.settings.$li.index()),this.settings.$next_tip.data("closed","")},set_target:function(){var b=this.settings.$li.attr(this.add_namespace("data-class")),d=this.settings.$li.attr(this.add_namespace("data-id")),e=function(){return d?a(c.getElementById(d)):b?a("."+b).first():a("body")};this.settings.$target=e()},scroll_to:function(){var c,d;c=a(b).height()/2,d=Math.ceil(this.settings.$target.offset().top-c+this.settings.$next_tip.outerHeight()),0!=d&&a("html, body").stop().animate({scrollTop:d},this.settings.scroll_speed,"swing")},paused:function(){return-1===a.inArray(this.settings.$li.index()+1,this.settings.pause_after)},restart:function(){this.hide(),this.settings.$li=d,this.show("init")},pos_default:function(a){var b=this.settings.$next_tip.find(".joyride-nub"),c=Math.ceil(b.outerWidth()/2),d=Math.ceil(b.outerHeight()/2),e=a||!1;if(e&&(this.settings.$next_tip.css("visibility","hidden"),this.settings.$next_tip.show()),/body/i.test(this.settings.$target.selector))this.settings.$li.length&&this.pos_modal(b);else{var f=this.settings.tip_settings.tipAdjustmentY?parseInt(this.settings.tip_settings.tipAdjustmentY):0,g=this.settings.tip_settings.tipAdjustmentX?parseInt(this.settings.tip_settings.tipAdjustmentX):0;this.bottom()?(this.settings.$next_tip.css(this.rtl?{top:this.settings.$target.offset().top+d+this.settings.$target.outerHeight()+f,left:this.settings.$target.offset().left+this.settings.$target.outerWidth()-this.settings.$next_tip.outerWidth()+g}:{top:this.settings.$target.offset().top+d+this.settings.$target.outerHeight()+f,left:this.settings.$target.offset().left+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"top")):this.top()?(this.settings.$next_tip.css(this.rtl?{top:this.settings.$target.offset().top-this.settings.$next_tip.outerHeight()-d+f,left:this.settings.$target.offset().left+this.settings.$target.outerWidth()-this.settings.$next_tip.outerWidth()}:{top:this.settings.$target.offset().top-this.settings.$next_tip.outerHeight()-d+f,left:this.settings.$target.offset().left+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"bottom")):this.right()?(this.settings.$next_tip.css({top:this.settings.$target.offset().top+f,left:this.settings.$target.outerWidth()+this.settings.$target.offset().left+c+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"left")):this.left()&&(this.settings.$next_tip.css({top:this.settings.$target.offset().top+f,left:this.settings.$target.offset().left-this.settings.$next_tip.outerWidth()-c+g}),this.nub_position(b,this.settings.tip_settings.nub_position,"right")),!this.visible(this.corners(this.settings.$next_tip))&&this.settings.attempts<this.settings.tip_settings.tip_location_pattern.length&&(b.removeClass("bottom").removeClass("top").removeClass("right").removeClass("left"),this.settings.tip_settings.tip_location=this.settings.tip_settings.tip_location_pattern[this.settings.attempts],this.settings.attempts++,this.pos_default())}e&&(this.settings.$next_tip.hide(),this.settings.$next_tip.css("visibility","visible"))},pos_phone:function(b){var c=this.settings.$next_tip.outerHeight(),d=(this.settings.$next_tip.offset(),this.settings.$target.outerHeight()),e=a(".joyride-nub",this.settings.$next_tip),f=Math.ceil(e.outerHeight()/2),g=b||!1;e.removeClass("bottom").removeClass("top").removeClass("right").removeClass("left"),g&&(this.settings.$next_tip.css("visibility","hidden"),this.settings.$next_tip.show()),/body/i.test(this.settings.$target.selector)?this.settings.$li.length&&this.pos_modal(e):this.top()?(this.settings.$next_tip.offset({top:this.settings.$target.offset().top-c-f}),e.addClass("bottom")):(this.settings.$next_tip.offset({top:this.settings.$target.offset().top+d+f}),e.addClass("top")),g&&(this.settings.$next_tip.hide(),this.settings.$next_tip.css("visibility","visible"))},pos_modal:function(a){this.center(),a.hide(),this.show_modal()},show_modal:function(){if(!this.settings.$next_tip.data("closed")){var b=a(".joyride-modal-bg");b.length<1&&a("body").append(this.settings.template.modal).show(),/pop/i.test(this.settings.tip_animation)?b.show():b.fadeIn(this.settings.tip_animation_fade_speed)}},expose:function(){var c,d,e,f,g,h="expose-"+this.random_str(6);if(arguments.length>0&&arguments[0]instanceof a)e=arguments[0];else{if(!this.settings.$target||/body/i.test(this.settings.$target.selector))return!1;e=this.settings.$target}return e.length<1?(b.console&&console.error("element not valid",e),!1):(c=a(this.settings.template.expose),this.settings.$body.append(c),c.css({top:e.offset().top,left:e.offset().left,width:e.outerWidth(!0),height:e.outerHeight(!0)}),d=a(this.settings.template.expose_cover),f={zIndex:e.css("z-index"),position:e.css("position")},g=null==e.attr("class")?"":e.attr("class"),e.css("z-index",parseInt(c.css("z-index"))+1),"static"==f.position&&e.css("position","relative"),e.data("expose-css",f),e.data("orig-class",g),e.attr("class",g+" "+this.settings.expose_add_class),d.css({top:e.offset().top,left:e.offset().left,width:e.outerWidth(!0),height:e.outerHeight(!0)}),this.settings.modal&&this.show_modal(),this.settings.$body.append(d),c.addClass(h),d.addClass(h),e.data("expose",h),this.settings.post_expose_callback(this.settings.$li.index(),this.settings.$next_tip,e),void this.add_exposed(e))},un_expose:function(){var c,d,e,f,g,h=!1;if(arguments.length>0&&arguments[0]instanceof a)d=arguments[0];else{if(!this.settings.$target||/body/i.test(this.settings.$target.selector))return!1;d=this.settings.$target}return d.length<1?(b.console&&console.error("element not valid",d),!1):(c=d.data("expose"),e=a("."+c),arguments.length>1&&(h=arguments[1]),h===!0?a(".joyride-expose-wrapper,.joyride-expose-cover").remove():e.remove(),f=d.data("expose-css"),"auto"==f.zIndex?d.css("z-index",""):d.css("z-index",f.zIndex),f.position!=d.css("position")&&("static"==f.position?d.css("position",""):d.css("position",f.position)),g=d.data("orig-class"),d.attr("class",g),d.removeData("orig-classes"),d.removeData("expose"),d.removeData("expose-z-index"),void this.remove_exposed(d))},add_exposed:function(b){this.settings.exposed=this.settings.exposed||[],b instanceof a||"object"==typeof b?this.settings.exposed.push(b[0]):"string"==typeof b&&this.settings.exposed.push(b)},remove_exposed:function(b){var c,d;for(b instanceof a?c=b[0]:"string"==typeof b&&(c=b),this.settings.exposed=this.settings.exposed||[],d=this.settings.exposed.length;d--;)if(this.settings.exposed[d]==c)return void this.settings.exposed.splice(d,1)},center:function(){var c=a(b);return this.settings.$next_tip.css({top:(c.height()-this.settings.$next_tip.outerHeight())/2+c.scrollTop(),left:(c.width()-this.settings.$next_tip.outerWidth())/2+c.scrollLeft()}),!0},bottom:function(){return/bottom/i.test(this.settings.tip_settings.tip_location)},top:function(){return/top/i.test(this.settings.tip_settings.tip_location)},right:function(){return/right/i.test(this.settings.tip_settings.tip_location)},left:function(){return/left/i.test(this.settings.tip_settings.tip_location)},corners:function(c){var d=a(b),e=d.height()/2,f=Math.ceil(this.settings.$target.offset().top-e+this.settings.$next_tip.outerHeight()),g=d.width()+d.scrollLeft(),h=d.height()+f,i=d.height()+d.scrollTop(),j=d.scrollTop();return j>f&&(j=0>f?0:f),h>i&&(i=h),[c.offset().top<j,g<c.offset().left+c.outerWidth(),i<c.offset().top+c.outerHeight(),d.scrollLeft()>c.offset().left]},visible:function(a){for(var b=a.length;b--;)if(a[b])return!1;return!0},nub_position:function(a,b,c){a.addClass("auto"===b?c:b)},startTimer:function(){this.settings.$li.length?this.settings.automate=setTimeout(function(){this.hide(),this.show(),this.startTimer()}.bind(this),this.settings.timer):clearTimeout(this.settings.automate)},end:function(b){this.settings.cookie_monster&&a.cookie(this.settings.cookie_name,"ridden",{expires:this.settings.cookie_expires,domain:this.settings.cookie_domain}),this.settings.timer>0&&clearTimeout(this.settings.automate),this.settings.modal&&this.settings.expose&&this.un_expose(),this.settings.$next_tip.data("closed",!0),a(".joyride-modal-bg").hide(),this.settings.$current_tip.hide(),("undefined"==typeof b||b===!1)&&(this.settings.post_step_callback(this.settings.$li.index(),this.settings.$current_tip),this.settings.post_ride_callback(this.settings.$li.index(),this.settings.$current_tip)),a(".joyride-tip-guide").remove()},off:function(){a(this.scope).off(".joyride"),a(b).off(".joyride"),a(".joyride-close-tip, .joyride-next-tip, .joyride-modal-bg").off(".joyride"),a(".joyride-tip-guide, .joyride-modal-bg").remove(),clearTimeout(this.settings.automate),this.settings={}},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs["magellan-expedition"]={name:"magellan-expedition",version:"5.3.3",settings:{active_class:"active",threshold:0,destination_threshold:20,throttle_delay:30,fixed_top:0},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c)},events:function(){var c=this,d=c.S,e=c.settings;c.set_expedition_position(),d(c.scope).off(".magellan").on("click.fndtn.magellan","["+c.add_namespace("data-magellan-arrival")+'] a[href^="#"]',function(b){b.preventDefault();var d=a(this).closest("["+c.attr_name()+"]"),e=d.data("magellan-expedition-init"),f=this.hash.split("#").join(""),g=a("a[name='"+f+"']");0===g.length&&(g=a("#"+f));var h=g.offset().top-e.destination_threshold+1;h-=d.outerHeight(),a("html, body").stop().animate({scrollTop:h},700,"swing",function(){history.pushState?history.pushState(null,null,"#"+f):location.hash="#"+f})}).on("scroll.fndtn.magellan",c.throttle(this.check_for_arrivals.bind(this),e.throttle_delay)),a(b).on("resize.fndtn.magellan",c.throttle(this.set_expedition_position.bind(this),e.throttle_delay))},check_for_arrivals:function(){var a=this;a.update_arrivals(),a.update_expedition_positions()},set_expedition_position:function(){var b=this;a("["+this.attr_name()+"=fixed]",b.scope).each(function(){var c,d,e=a(this),f=e.data("magellan-expedition-init"),g=e.attr("styles");e.attr("style",""),c=e.offset().top+f.threshold,d=parseInt(e.data("magellan-fixed-top")),isNaN(d)||(b.settings.fixed_top=d),e.data(b.data_attr("magellan-top-offset"),c),e.attr("style",g)})},update_expedition_positions:function(){var c=this,d=a(b).scrollTop();a("["+this.attr_name()+"=fixed]",c.scope).each(function(){var b=a(this),e=b.data("magellan-expedition-init"),f=b.attr("style"),g=b.data("magellan-top-offset");if(d+c.settings.fixed_top>=g){var h=b.prev("["+c.add_namespace("data-magellan-expedition-clone")+"]");0===h.length&&(h=b.clone(),h.removeAttr(c.attr_name()),h.attr(c.add_namespace("data-magellan-expedition-clone"),""),b.before(h)),b.css({position:"fixed",top:e.fixed_top})}else b.prev("["+c.add_namespace("data-magellan-expedition-clone")+"]").remove(),b.attr("style",f).css("position","").css("top","").removeClass("fixed")})},update_arrivals:function(){var c=this,d=a(b).scrollTop();a("["+this.attr_name()+"]",c.scope).each(function(){var b=a(this),e=b.data(c.attr_name(!0)+"-init"),f=c.offsets(b,d),g=b.find("["+c.add_namespace("data-magellan-arrival")+"]"),h=!1;f.each(function(a,d){if(d.viewport_offset>=d.top_offset){var f=b.find("["+c.add_namespace("data-magellan-arrival")+"]");return f.not(d.arrival).removeClass(e.active_class),d.arrival.addClass(e.active_class),h=!0,!0}}),h||g.removeClass(e.active_class)})},offsets:function(b,c){var d=this,e=b.data(d.attr_name(!0)+"-init"),f=c;return b.find("["+d.add_namespace("data-magellan-arrival")+"]").map(function(){var c=a(this).data(d.data_attr("magellan-arrival")),g=a("["+d.add_namespace("data-magellan-destination")+"="+c+"]");if(g.length>0){var h=Math.floor(g.offset().top-e.destination_threshold-b.outerHeight());return{destination:g,arrival:a(this),top_offset:h,viewport_offset:f}}}).sort(function(a,b){return a.top_offset<b.top_offset?-1:a.top_offset>b.top_offset?1:0})},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},off:function(){this.S(this.scope).off(".magellan"),this.S(b).off(".magellan")},reflow:function(){var b=this;a("["+b.add_namespace("data-magellan-expedition-clone")+"]",b.scope).remove()}}}(jQuery,window,window.document),function(){"use strict";Foundation.libs.offcanvas={name:"offcanvas",version:"5.3.3",settings:{open_method:"move",close_on_click:!1},init:function(a,b,c){this.bindings(b,c)},events:function(){var a=this,b=a.S,c="",d="",e="";"move"===this.settings.open_method?(c="move-",d="right",e="left"):"overlap"===this.settings.open_method&&(c="offcanvas-overlap"),b(this.scope).off(".offcanvas").on("click.fndtn.offcanvas",".left-off-canvas-toggle",function(b){a.click_toggle_class(b,c+d)}).on("click.fndtn.offcanvas",".left-off-canvas-menu a",function(b){var e=a.get_settings(b);e.close_on_click&&a.hide.call(a,c+d,a.get_wrapper(b))}).on("click.fndtn.offcanvas",".right-off-canvas-toggle",function(b){a.click_toggle_class(b,c+e)}).on("click.fndtn.offcanvas",".right-off-canvas-menu a",function(b){var d=a.get_settings(b);d.close_on_click&&a.hide.call(a,c+e,a.get_wrapper(b))}).on("click.fndtn.offcanvas",".exit-off-canvas",function(b){a.click_remove_class(b,c+e),d&&a.click_remove_class(b,c+d)})},toggle:function(a,b){b=b||this.get_wrapper(),b.is("."+a)?this.hide(a,b):this.show(a,b)},show:function(a,b){b=b||this.get_wrapper(),b.trigger("open").trigger("open.fndtn.offcanvas"),b.addClass(a)},hide:function(a,b){b=b||this.get_wrapper(),b.trigger("close").trigger("close.fndtn.offcanvas"),b.removeClass(a)},click_toggle_class:function(a,b){a.preventDefault();var c=this.get_wrapper(a);this.toggle(b,c)},click_remove_class:function(a,b){a.preventDefault();var c=this.get_wrapper(a);this.hide(b,c)},get_settings:function(a){var b=this.S(a.target).closest("["+this.attr_name()+"]");return b.data(this.attr_name(!0)+"-init")||this.settings},get_wrapper:function(a){var b=this.S(a?a.target:this.scope).closest(".off-canvas-wrap");return 0===b.length&&(b=this.S(".off-canvas-wrap")),b},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";var e=function(){},f=function(e,f){if(e.hasClass(f.slides_container_class))return this;var j,k,l,m,n,o,p=this,q=e,r=0,s=!1;p.slides=function(){return q.children(f.slide_selector)},p.slides().first().addClass(f.active_slide_class),p.update_slide_number=function(b){f.slide_number&&(k.find("span:first").text(parseInt(b)+1),k.find("span:last").text(p.slides().length)),f.bullets&&(l.children().removeClass(f.bullets_active_class),a(l.children().get(b)).addClass(f.bullets_active_class))},p.update_active_link=function(b){var c=a('[data-orbit-link="'+p.slides().eq(b).attr("data-orbit-slide")+'"]');c.siblings().removeClass(f.bullets_active_class),c.addClass(f.bullets_active_class)},p.build_markup=function(){q.wrap('<div class="'+f.container_class+'"></div>'),j=q.parent(),q.addClass(f.slides_container_class),f.stack_on_small&&j.addClass(f.stack_on_small_class),f.navigation_arrows&&(j.append(a('<a href="#"><span></span></a>').addClass(f.prev_class)),j.append(a('<a href="#"><span></span></a>').addClass(f.next_class))),f.timer&&(m=a("<div>").addClass(f.timer_container_class),m.append("<span>"),m.append(a("<div>").addClass(f.timer_progress_class)),m.addClass(f.timer_paused_class),j.append(m)),f.slide_number&&(k=a("<div>").addClass(f.slide_number_class),k.append("<span></span> "+f.slide_number_text+" <span></span>"),j.append(k)),f.bullets&&(l=a("<ol>").addClass(f.bullets_container_class),j.append(l),l.wrap('<div class="orbit-bullets-container"></div>'),p.slides().each(function(b){var c=a("<li>").attr("data-orbit-slide",b).on("click",p.link_bullet);l.append(c)}))},p._goto=function(b,c){if(b===r)return!1;"object"==typeof o&&o.restart();var d=p.slides(),e="next";if(s=!0,r>b&&(e="prev"),b>=d.length){if(!f.circular)return!1;b=0}else if(0>b){if(!f.circular)return!1;b=d.length-1}var g=a(d.get(r)),h=a(d.get(b));g.css("zIndex",2),g.removeClass(f.active_slide_class),h.css("zIndex",4).addClass(f.active_slide_class),q.trigger("before-slide-change.fndtn.orbit"),f.before_slide_change(),p.update_active_link(b);var i=function(){var a=function(){r=b,s=!1,c===!0&&(o=p.create_timer(),o.start()),p.update_slide_number(r),q.trigger("after-slide-change.fndtn.orbit",[{slide_number:r,total_slides:d.length}]),f.after_slide_change(r,d.length)};q.height()!=h.height()&&f.variable_height?q.animate({height:h.height()},250,"linear",a):a()};if(1===d.length)return i(),!1;var j=function(){"next"===e&&n.next(g,h,i),"prev"===e&&n.prev(g,h,i)};h.height()>q.height()&&f.variable_height?q.animate({height:h.height()},250,"linear",j):j()},p.next=function(a){a.stopImmediatePropagation(),a.preventDefault(),p._goto(r+1)},p.prev=function(a){a.stopImmediatePropagation(),a.preventDefault(),p._goto(r-1)},p.link_custom=function(b){b.preventDefault();var c=a(this).attr("data-orbit-link");if("string"==typeof c&&""!=(c=a.trim(c))){var d=j.find("[data-orbit-slide="+c+"]");-1!=d.index()&&p._goto(d.index())}},p.link_bullet=function(){var b=a(this).attr("data-orbit-slide");if("string"==typeof b&&""!=(b=a.trim(b)))if(isNaN(parseInt(b))){var c=j.find("[data-orbit-slide="+b+"]");-1!=c.index()&&p._goto(c.index()+1)}else p._goto(parseInt(b))},p.timer_callback=function(){p._goto(r+1,!0)},p.compute_dimensions=function(){var b=a(p.slides().get(r)),c=b.height();f.variable_height||p.slides().each(function(){a(this).height()>c&&(c=a(this).height())}),q.height(c)},p.create_timer=function(){var a=new g(j.find("."+f.timer_container_class),f,p.timer_callback);return a},p.stop_timer=function(){"object"==typeof o&&o.stop()},p.toggle_timer=function(){var a=j.find("."+f.timer_container_class);a.hasClass(f.timer_paused_class)?("undefined"==typeof o&&(o=p.create_timer()),o.start()):"object"==typeof o&&o.stop()},p.init=function(){p.build_markup(),f.timer&&(o=p.create_timer(),Foundation.utils.image_loaded(this.slides().children("img"),o.start)),n=new i(f,q),"slide"===f.animation&&(n=new h(f,q)),j.on("click","."+f.next_class,p.next),j.on("click","."+f.prev_class,p.prev),f.next_on_click&&j.on("click","."+f.slides_container_class+" [data-orbit-slide]",p.link_bullet),j.on("click",p.toggle_timer),f.swipe&&j.on("touchstart.fndtn.orbit",function(a){a.touches||(a=a.originalEvent);var b={start_page_x:a.touches[0].pageX,start_page_y:a.touches[0].pageY,start_time:(new Date).getTime(),delta_x:0,is_scrolling:d};j.data("swipe-transition",b),a.stopPropagation()}).on("touchmove.fndtn.orbit",function(a){if(a.touches||(a=a.originalEvent),!(a.touches.length>1||a.scale&&1!==a.scale)){var b=j.data("swipe-transition");if("undefined"==typeof b&&(b={}),b.delta_x=a.touches[0].pageX-b.start_page_x,"undefined"==typeof b.is_scrolling&&(b.is_scrolling=!!(b.is_scrolling||Math.abs(b.delta_x)<Math.abs(a.touches[0].pageY-b.start_page_y))),!b.is_scrolling&&!b.active){a.preventDefault();var c=b.delta_x<0?r+1:r-1;b.active=!0,p._goto(c)}}}).on("touchend.fndtn.orbit",function(a){j.data("swipe-transition",{}),a.stopPropagation()}),j.on("mouseenter.fndtn.orbit",function(){f.timer&&f.pause_on_hover&&p.stop_timer()}).on("mouseleave.fndtn.orbit",function(){f.timer&&f.resume_on_mouseout&&o.start()}),a(c).on("click","[data-orbit-link]",p.link_custom),a(b).on("load resize",p.compute_dimensions),Foundation.utils.image_loaded(this.slides().children("img"),p.compute_dimensions),Foundation.utils.image_loaded(this.slides().children("img"),function(){j.prev("."+f.preloader_class).css("display","none"),p.update_slide_number(0),p.update_active_link(0),q.trigger("ready.fndtn.orbit")})},p.init()},g=function(a,b,c){var d,e,f=this,g=b.timer_speed,h=a.find("."+b.timer_progress_class),i=-1;this.update_progress=function(a){var b=h.clone();b.attr("style",""),b.css("width",a+"%"),h.replaceWith(b),h=b},this.restart=function(){clearTimeout(e),a.addClass(b.timer_paused_class),i=-1,f.update_progress(0)},this.start=function(){return a.hasClass(b.timer_paused_class)?(i=-1===i?g:i,a.removeClass(b.timer_paused_class),d=(new Date).getTime(),h.animate({width:"100%"},i,"linear"),e=setTimeout(function(){f.restart(),c()},i),void a.trigger("timer-started.fndtn.orbit")):!0},this.stop=function(){if(a.hasClass(b.timer_paused_class))return!0;clearTimeout(e),a.addClass(b.timer_paused_class);var c=(new Date).getTime();i-=c-d;var h=100-i/g*100;f.update_progress(h),a.trigger("timer-stopped.fndtn.orbit")}},h=function(b){var c=b.animation_speed,d=1===a("html[dir=rtl]").length,e=d?"marginRight":"marginLeft",f={};f[e]="0%",this.next=function(a,b,d){a.animate({marginLeft:"-100%"},c),b.animate(f,c,function(){a.css(e,"100%"),d()})},this.prev=function(a,b,d){a.animate({marginLeft:"100%"},c),b.css(e,"-100%"),b.animate(f,c,function(){a.css(e,"100%"),d()})}},i=function(b){{var c=b.animation_speed;1===a("html[dir=rtl]").length}this.next=function(a,b,d){b.css({margin:"0%",opacity:"0.01"}),b.animate({opacity:"1"},c,"linear",function(){a.css("margin","100%"),d()})},this.prev=function(a,b,d){b.css({margin:"0%",opacity:"0.01"}),b.animate({opacity:"1"},c,"linear",function(){a.css("margin","100%"),d()})}};Foundation.libs=Foundation.libs||{},Foundation.libs.orbit={name:"orbit",version:"5.3.1",settings:{animation:"slide",timer_speed:1e4,pause_on_hover:!0,resume_on_mouseout:!1,next_on_click:!0,animation_speed:500,stack_on_small:!1,navigation_arrows:!0,slide_number:!0,slide_number_text:"of",container_class:"orbit-container",stack_on_small_class:"orbit-stack-on-small",next_class:"orbit-next",prev_class:"orbit-prev",timer_container_class:"orbit-timer",timer_paused_class:"paused",timer_progress_class:"orbit-progress",slides_container_class:"orbit-slides-container",preloader_class:"preloader",slide_selector:"*",bullets_container_class:"orbit-bullets",bullets_active_class:"active",slide_number_class:"orbit-slide-number",caption_class:"orbit-caption",active_slide_class:"active",orbit_transition_class:"orbit-transitioning",bullets:!0,circular:!0,timer:!0,variable_height:!1,swipe:!0,before_slide_change:e,after_slide_change:e},init:function(a,b,c){this.bindings(b,c)
/* 3    */ },events:function(a){var b=new f(this.S(a),this.S(a).data("orbit-init"));this.S(a).data(this.name+"-instance",b)},reflow:function(){var a=this;if(a.S(a.scope).is("[data-orbit]")){var b=a.S(a.scope),c=b.data(a.name+"-instance");c.compute_dimensions()}else a.S("[data-orbit]",a.scope).each(function(b,c){var d=a.S(c),e=(a.data_options(d),d.data(a.name+"-instance"));e.compute_dimensions()})}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";function e(a){var b=/fade/i.test(a),c=/pop/i.test(a);return{animate:b||c,pop:c,fade:b}}Foundation.libs.reveal={name:"reveal",version:"5.3.3",locked:!1,settings:{animation:"fadeAndPop",animation_speed:250,close_on_background_click:!0,close_on_esc:!0,dismiss_modal_class:"close-reveal-modal",bg_class:"reveal-modal-bg",root_element:"body",open:function(){},opened:function(){},close:function(){},closed:function(){},bg:a(".reveal-modal-bg"),css:{open:{opacity:0,visibility:"visible",display:"block"},close:{opacity:1,visibility:"hidden",display:"none"}}},init:function(b,c,d){a.extend(!0,this.settings,c,d),this.bindings(c,d)},events:function(){var a=this,b=a.S;return b(this.scope).off(".reveal").on("click.fndtn.reveal","["+this.add_namespace("data-reveal-id")+"]:not([disabled])",function(c){if(c.preventDefault(),!a.locked){var d=b(this),e=d.data(a.data_attr("reveal-ajax"));if(a.locked=!0,"undefined"==typeof e)a.open.call(a,d);else{var f=e===!0?d.attr("href"):e;a.open.call(a,d,{url:f})}}}),b(c).on("click.fndtn.reveal",this.close_targets(),function(c){if(c.preventDefault(),!a.locked){var d=b("["+a.attr_name()+"].open").data(a.attr_name(!0)+"-init"),e=b(c.target)[0]===b("."+d.bg_class)[0];if(e){if(!d.close_on_background_click)return;c.stopPropagation()}a.locked=!0,a.close.call(a,e?b("["+a.attr_name()+"].open"):b(this).closest("["+a.attr_name()+"]"))}}),b("["+a.attr_name()+"]",this.scope).length>0?b(this.scope).on("open.fndtn.reveal",this.settings.open).on("opened.fndtn.reveal",this.settings.opened).on("opened.fndtn.reveal",this.open_video).on("close.fndtn.reveal",this.settings.close).on("closed.fndtn.reveal",this.settings.closed).on("closed.fndtn.reveal",this.close_video):b(this.scope).on("open.fndtn.reveal","["+a.attr_name()+"]",this.settings.open).on("opened.fndtn.reveal","["+a.attr_name()+"]",this.settings.opened).on("opened.fndtn.reveal","["+a.attr_name()+"]",this.open_video).on("close.fndtn.reveal","["+a.attr_name()+"]",this.settings.close).on("closed.fndtn.reveal","["+a.attr_name()+"]",this.settings.closed).on("closed.fndtn.reveal","["+a.attr_name()+"]",this.close_video),!0},key_up_on:function(){var a=this;return a.S("body").off("keyup.fndtn.reveal").on("keyup.fndtn.reveal",function(b){var c=a.S("["+a.attr_name()+"].open"),d=c.data(a.attr_name(!0)+"-init")||a.settings;d&&27===b.which&&d.close_on_esc&&!a.locked&&a.close.call(a,c)}),!0},key_up_off:function(){return this.S("body").off("keyup.fndtn.reveal"),!0},open:function(b,c){var d,e=this;b?"undefined"!=typeof b.selector?d=e.S("#"+b.data(e.data_attr("reveal-id"))).first():(d=e.S(this.scope),c=b):d=e.S(this.scope);var f=d.data(e.attr_name(!0)+"-init");if(f=f||this.settings,d.hasClass("open")&&b.attr("data-reveal-id")==d.attr("id"))return e.close(d);if(!d.hasClass("open")){var g=e.S("["+e.attr_name()+"].open");if("undefined"==typeof d.data("css-top")&&d.data("css-top",parseInt(d.css("top"),10)).data("offset",this.cache_offset(d)),this.key_up_on(d),d.trigger("open").trigger("open.fndtn.reveal"),g.length<1&&this.toggle_bg(d,!0),"string"==typeof c&&(c={url:c}),"undefined"!=typeof c&&c.url){var h="undefined"!=typeof c.success?c.success:null;a.extend(c,{success:function(b,c,i){a.isFunction(h)&&h(b,c,i),d.html(b),e.S(d).foundation("section","reflow"),e.S(d).children().foundation(),g.length>0&&e.hide(g,f.css.close),e.show(d,f.css.open)}}),a.ajax(c)}else g.length>0&&this.hide(g,f.css.close),this.show(d,f.css.open)}},close:function(a){var a=a&&a.length?a:this.S(this.scope),b=this.S("["+this.attr_name()+"].open"),c=a.data(this.attr_name(!0)+"-init")||this.settings;b.length>0&&(this.locked=!0,this.key_up_off(a),a.trigger("close").trigger("close.fndtn.reveal"),this.toggle_bg(a,!1),this.hide(b,c.css.close,c))},close_targets:function(){var a="."+this.settings.dismiss_modal_class;return this.settings.close_on_background_click?a+", ."+this.settings.bg_class:a},toggle_bg:function(b,c){0===this.S("."+this.settings.bg_class).length&&(this.settings.bg=a("<div />",{"class":this.settings.bg_class}).appendTo("body").hide());var e=this.settings.bg.filter(":visible").length>0;c!=e&&((c==d?e:!c)?this.hide(this.settings.bg):this.show(this.settings.bg))},show:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init")||this.settings,g=f.root_element;if(0===c.parent(g).length){var h=c.wrap('<div style="display: none;" />').parent();c.on("closed.fndtn.reveal.wrapped",function(){c.detach().appendTo(h),c.unwrap().unbind("closed.fndtn.reveal.wrapped")}),c.detach().appendTo(g)}var i=e(f.animation);if(i.animate||(this.locked=!1),i.pop){d.top=a(b).scrollTop()-c.data("offset")+"px";var j={top:a(b).scrollTop()+c.data("css-top")+"px",opacity:1};return setTimeout(function(){return c.css(d).animate(j,f.animation_speed,"linear",function(){this.locked=!1,c.trigger("opened").trigger("opened.fndtn.reveal")}.bind(this)).addClass("open")}.bind(this),f.animation_speed/2)}if(i.fade){d.top=a(b).scrollTop()+c.data("css-top")+"px";var j={opacity:1};return setTimeout(function(){return c.css(d).animate(j,f.animation_speed,"linear",function(){this.locked=!1,c.trigger("opened").trigger("opened.fndtn.reveal")}.bind(this)).addClass("open")}.bind(this),f.animation_speed/2)}return c.css(d).show().css({opacity:1}).addClass("open").trigger("opened").trigger("opened.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeIn(f.animation_speed/2):(this.locked=!1,c.show())},hide:function(c,d){if(d){var f=c.data(this.attr_name(!0)+"-init");f=f||this.settings;var g=e(f.animation);if(g.animate||(this.locked=!1),g.pop){var h={top:-a(b).scrollTop()-c.data("offset")+"px",opacity:0};return setTimeout(function(){return c.animate(h,f.animation_speed,"linear",function(){this.locked=!1,c.css(d).trigger("closed").trigger("closed.fndtn.reveal")}.bind(this)).removeClass("open")}.bind(this),f.animation_speed/2)}if(g.fade){var h={opacity:0};return setTimeout(function(){return c.animate(h,f.animation_speed,"linear",function(){this.locked=!1,c.css(d).trigger("closed").trigger("closed.fndtn.reveal")}.bind(this)).removeClass("open")}.bind(this),f.animation_speed/2)}return c.hide().css(d).removeClass("open").trigger("closed").trigger("closed.fndtn.reveal")}var f=this.settings;return e(f.animation).fade?c.fadeOut(f.animation_speed/2):c.hide()},close_video:function(b){var c=a(".flex-video",b.target),d=a("iframe",c);d.length>0&&(d.attr("data-src",d[0].src),d.attr("src",d.attr("src")),c.hide())},open_video:function(b){var c=a(".flex-video",b.target),e=c.find("iframe");if(e.length>0){var f=e.attr("data-src");if("string"==typeof f)e[0].src=e.attr("data-src");else{var g=e[0].src;e[0].src=d,e[0].src=g}c.show()}},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},cache_offset:function(a){var b=a.show().height()+parseInt(a.css("top"),10);return a.hide(),b},off:function(){a(this.scope).off(".fndtn.reveal")},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.slider={name:"slider",version:"5.3.3",settings:{start:0,end:100,step:1,initial:null,display_selector:"",vertical:!1,on_change:function(){}},cache:{},init:function(a,b,c){Foundation.inherit(this,"throttle"),this.bindings(b,c),this.reflow()},events:function(){var c=this;a(this.scope).off(".slider").on("mousedown.fndtn.slider touchstart.fndtn.slider pointerdown.fndtn.slider","["+c.attr_name()+"]:not(.disabled, [disabled]) .range-slider-handle",function(b){c.cache.active||(b.preventDefault(),c.set_active_slider(a(b.target)))}).on("mousemove.fndtn.slider touchmove.fndtn.slider pointermove.fndtn.slider",function(d){if(c.cache.active)if(d.preventDefault(),a.data(c.cache.active[0],"settings").vertical){var e=0;d.pageY||(e=b.scrollY),c.calculate_position(c.cache.active,(d.pageY||d.originalEvent.clientY||d.originalEvent.touches[0].clientY||d.currentPoint.y)+e)}else c.calculate_position(c.cache.active,d.pageX||d.originalEvent.clientX||d.originalEvent.touches[0].clientX||d.currentPoint.x)}).on("mouseup.fndtn.slider touchend.fndtn.slider pointerup.fndtn.slider",function(){c.remove_active_slider()}).on("change.fndtn.slider",function(){c.settings.on_change()}),c.S(b).on("resize.fndtn.slider",c.throttle(function(){c.reflow()},300))},set_active_slider:function(a){this.cache.active=a},remove_active_slider:function(){this.cache.active=null},calculate_position:function(b,c){var d=this,e=a.data(b[0],"settings"),f=(a.data(b[0],"handle_l"),a.data(b[0],"handle_o"),a.data(b[0],"bar_l")),g=a.data(b[0],"bar_o");requestAnimationFrame(function(){var a;a=Foundation.rtl&&!e.vertical?d.limit_to((g+f-c)/f,0,1):d.limit_to((c-g)/f,0,1),a=e.vertical?1-a:a;var h=d.normalized_value(a,e.start,e.end,e.step);d.set_ui(b,h)})},set_ui:function(b,c){var d=a.data(b[0],"settings"),e=a.data(b[0],"handle_l"),f=a.data(b[0],"bar_l"),g=this.normalized_percentage(c,d.start,d.end),h=g*(f-e)-1,i=100*g;Foundation.rtl&&!d.vertical&&(h=-h),h=d.vertical?-h+f-e+1:h,this.set_translate(b,h,d.vertical),d.vertical?b.siblings(".range-slider-active-segment").css("height",i+"%"):b.siblings(".range-slider-active-segment").css("width",i+"%"),b.parent().attr(this.attr_name(),c).trigger("change").trigger("change.fndtn.slider"),b.parent().children("input[type=hidden]").val(c),""!=d.input_id&&a(d.display_selector).each(function(){this.hasOwnProperty("value")?a(this).val(c):a(this).text(c)})},normalized_percentage:function(a,b,c){return(a-b)/(c-b)},normalized_value:function(a,b,c,d){var e=c-b,f=a*e,g=(f-f%d)/d,h=f%d,i=h>=.5*d?d:0;return g*d+i+b},set_translate:function(b,c,d){d?a(b).css("-webkit-transform","translateY("+c+"px)").css("-moz-transform","translateY("+c+"px)").css("-ms-transform","translateY("+c+"px)").css("-o-transform","translateY("+c+"px)").css("transform","translateY("+c+"px)"):a(b).css("-webkit-transform","translateX("+c+"px)").css("-moz-transform","translateX("+c+"px)").css("-ms-transform","translateX("+c+"px)").css("-o-transform","translateX("+c+"px)").css("transform","translateX("+c+"px)")},limit_to:function(a,b,c){return Math.min(Math.max(a,b),c)},initialize_settings:function(b){var c=a.extend({},this.settings,this.data_options(a(b).parent()));c.vertical?(a.data(b,"bar_o",a(b).parent().offset().top),a.data(b,"bar_l",a(b).parent().outerHeight()),a.data(b,"handle_o",a(b).offset().top),a.data(b,"handle_l",a(b).outerHeight())):(a.data(b,"bar_o",a(b).parent().offset().left),a.data(b,"bar_l",a(b).parent().outerWidth()),a.data(b,"handle_o",a(b).offset().left),a.data(b,"handle_l",a(b).outerWidth())),a.data(b,"bar",a(b).parent()),a.data(b,"settings",c)},set_initial_position:function(b){var c=a.data(b.children(".range-slider-handle")[0],"settings"),d=c.initial?c.initial:Math.floor(.5*(c.end-c.start)/c.step)*c.step+c.start,e=b.children(".range-slider-handle");this.set_ui(e,d)},set_value:function(b){var c=this;a("["+c.attr_name()+"]",this.scope).each(function(){a(this).attr(c.attr_name(),b)}),a(this.scope).attr(c.attr_name())&&a(this.scope).attr(c.attr_name(),b),c.reflow()},reflow:function(){var b=this;b.S("["+this.attr_name()+"]").each(function(){var c=a(this).children(".range-slider-handle")[0],d=a(this).attr(b.attr_name());b.initialize_settings(c),d?b.set_ui(a(c),parseFloat(d)):b.set_initial_position(a(this))})}}}(jQuery,window,window.document),function(a,b,c,d){"use strict";Foundation.libs.tab={name:"tab",version:"5.3.3",settings:{active_class:"active",callback:function(){},deep_linking:!1,scroll_to_content:!0,is_hover:!1},default_tab_hashes:[],init:function(a,b,c){var d=this,e=this.S;this.bindings(b,c),this.handle_location_hash_change(),e("["+this.attr_name()+"] > .active > a",this.scope).each(function(){d.default_tab_hashes.push(this.hash)})},events:function(){var a=this,c=this.S;c(this.scope).off(".tab").on("click.fndtn.tab","["+this.attr_name()+"] > * > a",function(b){var d=c(this).closest("["+a.attr_name()+"]").data(a.attr_name(!0)+"-init");(!d.is_hover||Modernizr.touch)&&(b.preventDefault(),b.stopPropagation(),a.toggle_active_tab(c(this).parent()))}).on("mouseenter.fndtn.tab","["+this.attr_name()+"] > * > a",function(){var b=c(this).closest("["+a.attr_name()+"]").data(a.attr_name(!0)+"-init");b.is_hover&&a.toggle_active_tab(c(this).parent())}),c(b).on("hashchange.fndtn.tab",function(b){b.preventDefault(),a.handle_location_hash_change()})},handle_location_hash_change:function(){var b=this,c=this.S;c("["+this.attr_name()+"]",this.scope).each(function(){var e=c(this).data(b.attr_name(!0)+"-init");if(e.deep_linking){var f=b.scope.location.hash;if(""!=f){var g=c(f);if(g.hasClass("content")&&g.parent().hasClass("tab-content"))b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href="+f+"]").parent());else{var h=g.closest(".content").attr("id");h!=d&&b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href=#"+h+"]").parent(),f)}}else for(var i in b.default_tab_hashes)b.toggle_active_tab(a("["+b.attr_name()+"] > * > a[href="+b.default_tab_hashes[i]+"]").parent())}})},toggle_active_tab:function(c,e){var f=this.S,g=c.closest("["+this.attr_name()+"]"),h=c.children("a").first(),i="#"+h.attr("href").split("#")[1],j=f(i),k=c.siblings(),l=g.data(this.attr_name(!0)+"-init");if(f(this).data(this.data_attr("tab-content"))&&(i="#"+f(this).data(this.data_attr("tab-content")).split("#")[1],j=f(i)),l.deep_linking){var m=a("body,html").scrollTop();b.location.hash=e!=d?e:i,l.scroll_to_content?e==d||e==i?c.parent()[0].scrollIntoView():f(i)[0].scrollIntoView():(e==d||e==i)&&a("body,html").scrollTop(m)}c.addClass(l.active_class).triggerHandler("opened"),k.removeClass(l.active_class),j.siblings().removeClass(l.active_class).end().addClass(l.active_class),l.callback(c),j.triggerHandler("toggled",[c]),g.triggerHandler("toggled",[j])},data_attr:function(a){return this.namespace.length>0?this.namespace+"-"+a:a},off:function(){},reflow:function(){}}}(jQuery,window,window.document),function(a,b){"use strict";Foundation.libs.tooltip={name:"tooltip",version:"5.3.3",settings:{additional_inheritable_classes:[],tooltip_class:".tooltip",append_to:"body",touch_close_text:"Tap To Close",disable_for_touch:!1,hover_delay:200,show_on:"all",tip_template:function(a,b){return'<span data-selector="'+a+'" class="'+Foundation.libs.tooltip.settings.tooltip_class.substring(1)+'">'+b+'<span class="nub"></span></span>'}},cache:{},init:function(a,b,c){Foundation.inherit(this,"random_str"),this.bindings(b,c)},should_show:function(b){var c=a.extend({},this.settings,this.data_options(b));return"all"===c.show_on?!0:this.small()&&"small"===c.show_on?!0:this.medium()&&"medium"===c.show_on?!0:this.large()&&"large"===c.show_on?!0:!1},medium:function(){return matchMedia(Foundation.media_queries.medium).matches},large:function(){return matchMedia(Foundation.media_queries.large).matches},events:function(b){var c=this,d=c.S;c.create(this.S(b)),a(this.scope).off(".tooltip").on("mouseenter.fndtn.tooltip mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip","["+this.attr_name()+"]",function(b){var e=d(this),f=a.extend({},c.settings,c.data_options(e)),g=!1;if(Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&d(b.target).is("a"))return!1;if(/mouse/i.test(b.type)&&c.ie_touch(b))return!1;if(e.hasClass("open"))Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&b.preventDefault(),c.hide(e);else{if(f.disable_for_touch&&Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type))return;!f.disable_for_touch&&Modernizr.touch&&/touchstart|MSPointerDown/i.test(b.type)&&(b.preventDefault(),d(f.tooltip_class+".open").hide(),g=!0),/enter|over/i.test(b.type)?this.timer=setTimeout(function(){c.showTip(e)}.bind(this),c.settings.hover_delay):"mouseout"===b.type||"mouseleave"===b.type?(clearTimeout(this.timer),c.hide(e)):c.showTip(e)}}).on("mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip","["+this.attr_name()+"].open",function(b){return/mouse/i.test(b.type)&&c.ie_touch(b)?!1:void(("touch"!=a(this).data("tooltip-open-event-type")||"mouseleave"!=b.type)&&("mouse"==a(this).data("tooltip-open-event-type")&&/MSPointerDown|touchstart/i.test(b.type)?c.convert_to_touch(a(this)):c.hide(a(this))))}).on("DOMNodeRemoved DOMAttrModified","["+this.attr_name()+"]:not(a)",function(){c.hide(d(this))})},ie_touch:function(){return!1},showTip:function(a){var b=this.getTip(a);return this.should_show(a,b)?this.show(a):void 0},getTip:function(b){var c=this.selector(b),d=a.extend({},this.settings,this.data_options(b)),e=null;return c&&(e=this.S('span[data-selector="'+c+'"]'+d.tooltip_class)),"object"==typeof e?e:!1},selector:function(a){var b=a.attr("id"),c=a.attr(this.attr_name())||a.attr("data-selector");return(b&&b.length<1||!b)&&"string"!=typeof c&&(c=this.random_str(6),a.attr("data-selector",c)),b&&b.length>0?b:c},create:function(c){var d=this,e=a.extend({},this.settings,this.data_options(c)),f=this.settings.tip_template;"string"==typeof e.tip_template&&b.hasOwnProperty(e.tip_template)&&(f=b[e.tip_template]);var g=a(f(this.selector(c),a("<div></div>").html(c.attr("title")).html())),h=this.inheritable_classes(c);g.addClass(h).appendTo(e.append_to),Modernizr.touch&&(g.append('<span class="tap-to-close">'+e.touch_close_text+"</span>"),g.on("touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip",function(){d.hide(c)})),c.removeAttr("title").attr("title","")},reposition:function(b,c,d){var e,f,g,h,i;if(c.css("visibility","hidden").show(),e=b.data("width"),f=c.children(".nub"),g=f.outerHeight(),h=f.outerHeight(),c.css(this.small()?{width:"100%"}:{width:e?e:"auto"}),i=function(a,b,c,d,e){return a.css({top:b?b:"auto",bottom:d?d:"auto",left:e?e:"auto",right:c?c:"auto"}).end()},i(c,b.offset().top+b.outerHeight()+10,"auto","auto",b.offset().left),this.small())i(c,b.offset().top+b.outerHeight()+10,"auto","auto",12.5,a(this.scope).width()),c.addClass("tip-override"),i(f,-g,"auto","auto",b.offset().left);else{var j=b.offset().left;Foundation.rtl&&(f.addClass("rtl"),j=b.offset().left+b.outerWidth()-c.outerWidth()),i(c,b.offset().top+b.outerHeight()+10,"auto","auto",j),c.removeClass("tip-override"),d&&d.indexOf("tip-top")>-1?(Foundation.rtl&&f.addClass("rtl"),i(c,b.offset().top-c.outerHeight(),"auto","auto",j).removeClass("tip-override")):d&&d.indexOf("tip-left")>-1?(i(c,b.offset().top+b.outerHeight()/2-c.outerHeight()/2,"auto","auto",b.offset().left-c.outerWidth()-g).removeClass("tip-override"),f.removeClass("rtl")):d&&d.indexOf("tip-right")>-1&&(i(c,b.offset().top+b.outerHeight()/2-c.outerHeight()/2,"auto","auto",b.offset().left+b.outerWidth()+g).removeClass("tip-override"),f.removeClass("rtl"))}c.css("visibility","visible").hide()},small:function(){return matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches},inheritable_classes:function(b){var c=a.extend({},this.settings,this.data_options(b)),d=["tip-top","tip-left","tip-bottom","tip-right","radius","round"].concat(c.additional_inheritable_classes),e=b.attr("class"),f=e?a.map(e.split(" "),function(b){return-1!==a.inArray(b,d)?b:void 0}).join(" "):"";return a.trim(f)},convert_to_touch:function(b){var c=this,d=c.getTip(b),e=a.extend({},c.settings,c.data_options(b));0===d.find(".tap-to-close").length&&(d.append('<span class="tap-to-close">'+e.touch_close_text+"</span>"),d.on("click.fndtn.tooltip.tapclose touchstart.fndtn.tooltip.tapclose MSPointerDown.fndtn.tooltip.tapclose",function(){c.hide(b)})),b.data("tooltip-open-event-type","touch")},show:function(a){var b=this.getTip(a);"touch"==a.data("tooltip-open-event-type")&&this.convert_to_touch(a),this.reposition(a,b,a.attr("class")),a.addClass("open"),b.fadeIn(150)},hide:function(a){var b=this.getTip(a);b.fadeOut(150,function(){b.find(".tap-to-close").remove(),b.off("click.fndtn.tooltip.tapclose MSPointerDown.fndtn.tapclose"),a.removeClass("open")})},off:function(){var b=this;this.S(this.scope).off(".fndtn.tooltip"),this.S(this.settings.tooltip_class).each(function(c){a("["+b.attr_name()+"]").eq(c).attr("title",a(this).text())}).remove()},reflow:function(){}}}(jQuery,window,window.document),function(a,b,c){"use strict";Foundation.libs.topbar={name:"topbar",version:"5.3.3",settings:{index:0,sticky_class:"sticky",custom_back_text:!0,back_text:"Back",mobile_show_parent_link:!0,is_hover:!0,scrolltop:!0,sticky_on:"all"},init:function(b,c,d){Foundation.inherit(this,"add_custom_rule register_media throttle");var e=this;e.register_media("topbar","foundation-mq-topbar"),this.bindings(c,d),e.S("["+this.attr_name()+"]",this.scope).each(function(){{var b=a(this),c=b.data(e.attr_name(!0)+"-init");e.S("section",this)}b.data("index",0);var d=b.parent();d.hasClass("fixed")||e.is_sticky(b,d,c)?(e.settings.sticky_class=c.sticky_class,e.settings.sticky_topbar=b,b.data("height",d.outerHeight()),b.data("stickyoffset",d.offset().top)):b.data("height",b.outerHeight()),c.assembled||e.assemble(b),c.is_hover?e.S(".has-dropdown",b).addClass("not-click"):e.S(".has-dropdown",b).removeClass("not-click"),e.add_custom_rule(".f-topbar-fixed { padding-top: "+b.data("height")+"px }"),d.hasClass("fixed")&&e.S("body").addClass("f-topbar-fixed")})},is_sticky:function(a,b,c){var d=b.hasClass(c.sticky_class);return d&&"all"===c.sticky_on?!0:d&&this.small()&&"small"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&!matchMedia(Foundation.media_queries.medium).matches&&!matchMedia(Foundation.media_queries.large).matches:d&&this.medium()&&"medium"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&matchMedia(Foundation.media_queries.medium).matches&&!matchMedia(Foundation.media_queries.large).matches:d&&this.large()&&"large"===c.sticky_on?matchMedia(Foundation.media_queries.small).matches&&matchMedia(Foundation.media_queries.medium).matches&&matchMedia(Foundation.media_queries.large).matches:!1},toggle:function(c){var d,e=this;d=c?e.S(c).closest("["+this.attr_name()+"]"):e.S("["+this.attr_name()+"]");var f=d.data(this.attr_name(!0)+"-init"),g=e.S("section, .section",d);e.breakpoint()&&(e.rtl?(g.css({right:"0%"}),a(">.name",g).css({right:"100%"})):(g.css({left:"0%"}),a(">.name",g).css({left:"100%"})),e.S("li.moved",g).removeClass("moved"),d.data("index",0),d.toggleClass("expanded").css("height","")),f.scrolltop?d.hasClass("expanded")?d.parent().hasClass("fixed")&&(f.scrolltop?(d.parent().removeClass("fixed"),d.addClass("fixed"),e.S("body").removeClass("f-topbar-fixed"),b.scrollTo(0,0)):d.parent().removeClass("expanded")):d.hasClass("fixed")&&(d.parent().addClass("fixed"),d.removeClass("fixed"),e.S("body").addClass("f-topbar-fixed")):(e.is_sticky(d,d.parent(),f)&&d.parent().addClass("fixed"),d.parent().hasClass("fixed")&&(d.hasClass("expanded")?(d.addClass("fixed"),d.parent().addClass("expanded"),e.S("body").addClass("f-topbar-fixed")):(d.removeClass("fixed"),d.parent().removeClass("expanded"),e.update_sticky_positioning())))},timer:null,events:function(){var c=this,d=this.S;d(this.scope).off(".topbar").on("click.fndtn.topbar","["+this.attr_name()+"] .toggle-topbar",function(a){a.preventDefault(),c.toggle(this)}).on("click.fndtn.topbar",'.top-bar .top-bar-section li a[href^="#"],['+this.attr_name()+'] .top-bar-section li a[href^="#"]',function(){var b=a(this).closest("li");!c.breakpoint()||b.hasClass("back")||b.hasClass("has-dropdown")||c.toggle()}).on("click.fndtn.topbar","["+this.attr_name()+"] li.has-dropdown",function(b){var e=d(this),f=d(b.target),g=e.closest("["+c.attr_name()+"]"),h=g.data(c.attr_name(!0)+"-init");return f.data("revealId")?void c.toggle():void(c.breakpoint()||(!h.is_hover||Modernizr.touch)&&(b.stopImmediatePropagation(),e.hasClass("hover")?(e.removeClass("hover").find("li").removeClass("hover"),e.parents("li.hover").removeClass("hover")):(e.addClass("hover"),a(e).siblings().removeClass("hover"),"A"===f[0].nodeName&&f.parent().hasClass("has-dropdown")&&b.preventDefault())))}).on("click.fndtn.topbar","["+this.attr_name()+"] .has-dropdown>a",function(a){if(c.breakpoint()){a.preventDefault();var b=d(this),e=b.closest("["+c.attr_name()+"]"),f=e.find("section, .section"),g=(b.next(".dropdown").outerHeight(),b.closest("li"));e.data("index",e.data("index")+1),g.addClass("moved"),c.rtl?(f.css({right:-(100*e.data("index"))+"%"}),f.find(">.name").css({right:100*e.data("index")+"%"})):(f.css({left:-(100*e.data("index"))+"%"}),f.find(">.name").css({left:100*e.data("index")+"%"})),e.css("height",b.siblings("ul").outerHeight(!0)+e.data("height"))}}),d(b).off(".topbar").on("resize.fndtn.topbar",c.throttle(function(){c.resize.call(c)},50)).trigger("resize").trigger("resize.fndtn.topbar"),d("body").off(".topbar").on("click.fndtn.topbar",function(a){var b=d(a.target).closest("li").closest("li.hover");b.length>0||d("["+c.attr_name()+"] li.hover").removeClass("hover")}),d(this.scope).on("click.fndtn.topbar","["+this.attr_name()+"] .has-dropdown .back",function(a){a.preventDefault();var b=d(this),e=b.closest("["+c.attr_name()+"]"),f=e.find("section, .section"),g=(e.data(c.attr_name(!0)+"-init"),b.closest("li.moved")),h=g.parent();e.data("index",e.data("index")-1),c.rtl?(f.css({right:-(100*e.data("index"))+"%"}),f.find(">.name").css({right:100*e.data("index")+"%"})):(f.css({left:-(100*e.data("index"))+"%"}),f.find(">.name").css({left:100*e.data("index")+"%"})),0===e.data("index")?e.css("height",""):e.css("height",h.outerHeight(!0)+e.data("height")),setTimeout(function(){g.removeClass("moved")},300)})},resize:function(){var a=this;a.S("["+this.attr_name()+"]").each(function(){var b,d=a.S(this),e=d.data(a.attr_name(!0)+"-init"),f=d.parent("."+a.settings.sticky_class);if(!a.breakpoint()){var g=d.hasClass("expanded");d.css("height","").removeClass("expanded").find("li").removeClass("hover"),g&&a.toggle(d)}a.is_sticky(d,f,e)&&(f.hasClass("fixed")?(f.removeClass("fixed"),b=f.offset().top,a.S(c.body).hasClass("f-topbar-fixed")&&(b-=d.data("height")),d.data("stickyoffset",b),f.addClass("fixed")):(b=f.offset().top,d.data("stickyoffset",b)))})},breakpoint:function(){return!matchMedia(Foundation.media_queries.topbar).matches},small:function(){return matchMedia(Foundation.media_queries.small).matches},medium:function(){return matchMedia(Foundation.media_queries.medium).matches},large:function(){return matchMedia(Foundation.media_queries.large).matches},assemble:function(b){var c=this,d=b.data(this.attr_name(!0)+"-init"),e=c.S("section",b);e.detach(),c.S(".has-dropdown>a",e).each(function(){var b,e=c.S(this),f=e.siblings(".dropdown"),g=e.attr("href");f.find(".title.back").length||(b=a(1==d.mobile_show_parent_link&&g?'<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5></li><li class="parent-link show-for-small"><a class="parent-link js-generated" href="'+g+'">'+e.html()+"</a></li>":'<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5>'),a("h5>a",b).html(1==d.custom_back_text?d.back_text:"&laquo; "+e.html()),f.prepend(b))}),e.appendTo(b),this.sticky(),this.assembled(b)},assembled:function(b){b.data(this.attr_name(!0),a.extend({},b.data(this.attr_name(!0)),{assembled:!0}))},height:function(b){var c=0,d=this;return a("> li",b).each(function(){c+=d.S(this).outerHeight(!0)}),c},sticky:function(){var a=this;this.S(b).on("scroll",function(){a.update_sticky_positioning()})},update_sticky_positioning:function(){var a="."+this.settings.sticky_class,c=this.S(b),d=this;if(d.settings.sticky_topbar&&d.is_sticky(this.settings.sticky_topbar,this.settings.sticky_topbar.parent(),this.settings)){var e=this.settings.sticky_topbar.data("stickyoffset");d.S(a).hasClass("expanded")||(c.scrollTop()>e?d.S(a).hasClass("fixed")||(d.S(a).addClass("fixed"),d.S("body").addClass("f-topbar-fixed")):c.scrollTop()<=e&&d.S(a).hasClass("fixed")&&(d.S(a).removeClass("fixed"),d.S("body").removeClass("f-topbar-fixed")))}},off:function(){this.S(this.scope).off(".fndtn.topbar"),this.S(b).off(".fndtn.topbar")},reflow:function(){}}}(jQuery,this,this.document);
/* 4    */ 
/* 5    */ /*!
/* 6    *|  * Modernizr v2.8.3
/* 7    *|  * www.modernizr.com
/* 8    *|  *
/* 9    *|  * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
/* 10   *|  * Available under the BSD and MIT licenses: www.modernizr.com/license/
/* 11   *|  */
/* 12   */ 
/* 13   */ /*
/* 14   *|  * Modernizr tests which native CSS3 and HTML5 features are available in
/* 15   *|  * the current UA and makes the results available to you in two ways:
/* 16   *|  * as properties on a global Modernizr object, and as classes on the
/* 17   *|  * <html> element. This information allows you to progressively enhance
/* 18   *|  * your pages with a granular level of control over the experience.
/* 19   *|  *
/* 20   *|  * Modernizr has an optional (not included) conditional resource loader
/* 21   *|  * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
/* 22   *|  * To get a build that includes Modernizr.load(), as well as choosing
/* 23   *|  * which tests to include, go to www.modernizr.com/download/
/* 24   *|  *
/* 25   *|  * Authors        Faruk Ates, Paul Irish, Alex Sexton
/* 26   *|  * Contributors   Ryan Seddon, Ben Alman
/* 27   *|  */
/* 28   */ 
/* 29   */ window.Modernizr = (function( window, document, undefined ) {
/* 30   */ 
/* 31   */     var version = '2.8.3',
/* 32   */ 
/* 33   */     Modernizr = {},
/* 34   */ 
/* 35   */     /*>>cssclasses*/
/* 36   */     // option for enabling the HTML classes to be added
/* 37   */     enableClasses = true,
/* 38   */     /*>>cssclasses*/
/* 39   */ 
/* 40   */     docElement = document.documentElement,
/* 41   */ 
/* 42   */     /**
/* 43   *|      * Create our "modernizr" element that we do most feature tests on.
/* 44   *|      */
/* 45   */     mod = 'modernizr',
/* 46   */     modElem = document.createElement(mod),
/* 47   */     mStyle = modElem.style,
/* 48   */ 
/* 49   */     /**
/* 50   *|      * Create the input element for various Web Forms feature tests.

/* foundation.min.js */

/* 51   *|      */
/* 52   */     inputElem /*>>inputelem*/ = document.createElement('input') /*>>inputelem*/ ,
/* 53   */ 
/* 54   */     /*>>smile*/
/* 55   */     smile = ':)',
/* 56   */     /*>>smile*/
/* 57   */ 
/* 58   */     toString = {}.toString,
/* 59   */ 
/* 60   */     // TODO :: make the prefixes more granular
/* 61   */     /*>>prefixes*/
/* 62   */     // List of property values to set for css tests. See ticket #21
/* 63   */     prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
/* 64   */     /*>>prefixes*/
/* 65   */ 
/* 66   */     /*>>domprefixes*/
/* 67   */     // Following spec is to expose vendor-specific style properties as:
/* 68   */     //   elem.style.WebkitBorderRadius
/* 69   */     // and the following would be incorrect:
/* 70   */     //   elem.style.webkitBorderRadius
/* 71   */ 
/* 72   */     // Webkit ghosts their properties in lowercase but Opera & Moz do not.
/* 73   */     // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
/* 74   */     //   erik.eae.net/archives/2008/03/10/21.48.10/
/* 75   */ 
/* 76   */     // More here: github.com/Modernizr/Modernizr/issues/issue/21
/* 77   */     omPrefixes = 'Webkit Moz O ms',
/* 78   */ 
/* 79   */     cssomPrefixes = omPrefixes.split(' '),
/* 80   */ 
/* 81   */     domPrefixes = omPrefixes.toLowerCase().split(' '),
/* 82   */     /*>>domprefixes*/
/* 83   */ 
/* 84   */     /*>>ns*/
/* 85   */     ns = {'svg': 'http://www.w3.org/2000/svg'},
/* 86   */     /*>>ns*/
/* 87   */ 
/* 88   */     tests = {},
/* 89   */     inputs = {},
/* 90   */     attrs = {},
/* 91   */ 
/* 92   */     classes = [],
/* 93   */ 
/* 94   */     slice = classes.slice,
/* 95   */ 
/* 96   */     featureName, // used in testing loop
/* 97   */ 
/* 98   */ 
/* 99   */     /*>>teststyles*/
/* 100  */     // Inject element with style element and some CSS rules

/* foundation.min.js */

/* 101  */     injectElementWithStyles = function( rule, callback, nodes, testnames ) {
/* 102  */ 
/* 103  */       var style, ret, node, docOverflow,
/* 104  */           div = document.createElement('div'),
/* 105  */           // After page load injecting a fake body doesn't work so check if body exists
/* 106  */           body = document.body,
/* 107  */           // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
/* 108  */           fakeBody = body || document.createElement('body');
/* 109  */ 
/* 110  */       if ( parseInt(nodes, 10) ) {
/* 111  */           // In order not to give false positives we create a node for each test
/* 112  */           // This also allows the method to scale for unspecified uses
/* 113  */           while ( nodes-- ) {
/* 114  */               node = document.createElement('div');
/* 115  */               node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
/* 116  */               div.appendChild(node);
/* 117  */           }
/* 118  */       }
/* 119  */ 
/* 120  */       // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
/* 121  */       // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
/* 122  */       // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
/* 123  */       // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
/* 124  */       // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
/* 125  */       style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
/* 126  */       div.id = mod;
/* 127  */       // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
/* 128  */       // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
/* 129  */       (body ? div : fakeBody).innerHTML += style;
/* 130  */       fakeBody.appendChild(div);
/* 131  */       if ( !body ) {
/* 132  */           //avoid crashing IE8, if background image is used
/* 133  */           fakeBody.style.background = '';
/* 134  */           //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
/* 135  */           fakeBody.style.overflow = 'hidden';
/* 136  */           docOverflow = docElement.style.overflow;
/* 137  */           docElement.style.overflow = 'hidden';
/* 138  */           docElement.appendChild(fakeBody);
/* 139  */       }
/* 140  */ 
/* 141  */       ret = callback(div, rule);
/* 142  */       // If this is done after page load we don't want to remove the body so check if body exists
/* 143  */       if ( !body ) {
/* 144  */           fakeBody.parentNode.removeChild(fakeBody);
/* 145  */           docElement.style.overflow = docOverflow;
/* 146  */       } else {
/* 147  */           div.parentNode.removeChild(div);
/* 148  */       }
/* 149  */ 
/* 150  */       return !!ret;

/* foundation.min.js */

/* 151  */ 
/* 152  */     },
/* 153  */     /*>>teststyles*/
/* 154  */ 
/* 155  */     /*>>mq*/
/* 156  */     // adapted from matchMedia polyfill
/* 157  */     // by Scott Jehl and Paul Irish
/* 158  */     // gist.github.com/786768
/* 159  */     testMediaQuery = function( mq ) {
/* 160  */ 
/* 161  */       var matchMedia = window.matchMedia || window.msMatchMedia;
/* 162  */       if ( matchMedia ) {
/* 163  */         return matchMedia(mq) && matchMedia(mq).matches || false;
/* 164  */       }
/* 165  */ 
/* 166  */       var bool;
/* 167  */ 
/* 168  */       injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function( node ) {
/* 169  */         bool = (window.getComputedStyle ?
/* 170  */                   getComputedStyle(node, null) :
/* 171  */                   node.currentStyle)['position'] == 'absolute';
/* 172  */       });
/* 173  */ 
/* 174  */       return bool;
/* 175  */ 
/* 176  */      },
/* 177  */      /*>>mq*/
/* 178  */ 
/* 179  */ 
/* 180  */     /*>>hasevent*/
/* 181  */     //
/* 182  */     // isEventSupported determines if a given element supports the given event
/* 183  */     // kangax.github.com/iseventsupported/
/* 184  */     //
/* 185  */     // The following results are known incorrects:
/* 186  */     //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
/* 187  */     //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
/* 188  */     //   ...
/* 189  */     isEventSupported = (function() {
/* 190  */ 
/* 191  */       var TAGNAMES = {
/* 192  */         'select': 'input', 'change': 'input',
/* 193  */         'submit': 'form', 'reset': 'form',
/* 194  */         'error': 'img', 'load': 'img', 'abort': 'img'
/* 195  */       };
/* 196  */ 
/* 197  */       function isEventSupported( eventName, element ) {
/* 198  */ 
/* 199  */         element = element || document.createElement(TAGNAMES[eventName] || 'div');
/* 200  */         eventName = 'on' + eventName;

/* foundation.min.js */

/* 201  */ 
/* 202  */         // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
/* 203  */         var isSupported = eventName in element;
/* 204  */ 
/* 205  */         if ( !isSupported ) {
/* 206  */           // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
/* 207  */           if ( !element.setAttribute ) {
/* 208  */             element = document.createElement('div');
/* 209  */           }
/* 210  */           if ( element.setAttribute && element.removeAttribute ) {
/* 211  */             element.setAttribute(eventName, '');
/* 212  */             isSupported = is(element[eventName], 'function');
/* 213  */ 
/* 214  */             // If property was created, "remove it" (by setting value to `undefined`)
/* 215  */             if ( !is(element[eventName], 'undefined') ) {
/* 216  */               element[eventName] = undefined;
/* 217  */             }
/* 218  */             element.removeAttribute(eventName);
/* 219  */           }
/* 220  */         }
/* 221  */ 
/* 222  */         element = null;
/* 223  */         return isSupported;
/* 224  */       }
/* 225  */       return isEventSupported;
/* 226  */     })(),
/* 227  */     /*>>hasevent*/
/* 228  */ 
/* 229  */     // TODO :: Add flag for hasownprop ? didn't last time
/* 230  */ 
/* 231  */     // hasOwnProperty shim by kangax needed for Safari 2.0 support
/* 232  */     _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;
/* 233  */ 
/* 234  */     if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
/* 235  */       hasOwnProp = function (object, property) {
/* 236  */         return _hasOwnProperty.call(object, property);
/* 237  */       };
/* 238  */     }
/* 239  */     else {
/* 240  */       hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
/* 241  */         return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
/* 242  */       };
/* 243  */     }
/* 244  */ 
/* 245  */     // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
/* 246  */     // es5.github.com/#x15.3.4.5
/* 247  */ 
/* 248  */     if (!Function.prototype.bind) {
/* 249  */       Function.prototype.bind = function bind(that) {
/* 250  */ 

/* foundation.min.js */

/* 251  */         var target = this;
/* 252  */ 
/* 253  */         if (typeof target != "function") {
/* 254  */             throw new TypeError();
/* 255  */         }
/* 256  */ 
/* 257  */         var args = slice.call(arguments, 1),
/* 258  */             bound = function () {
/* 259  */ 
/* 260  */             if (this instanceof bound) {
/* 261  */ 
/* 262  */               var F = function(){};
/* 263  */               F.prototype = target.prototype;
/* 264  */               var self = new F();
/* 265  */ 
/* 266  */               var result = target.apply(
/* 267  */                   self,
/* 268  */                   args.concat(slice.call(arguments))
/* 269  */               );
/* 270  */               if (Object(result) === result) {
/* 271  */                   return result;
/* 272  */               }
/* 273  */               return self;
/* 274  */ 
/* 275  */             } else {
/* 276  */ 
/* 277  */               return target.apply(
/* 278  */                   that,
/* 279  */                   args.concat(slice.call(arguments))
/* 280  */               );
/* 281  */ 
/* 282  */             }
/* 283  */ 
/* 284  */         };
/* 285  */ 
/* 286  */         return bound;
/* 287  */       };
/* 288  */     }
/* 289  */ 
/* 290  */     /**
/* 291  *|      * setCss applies given styles to the Modernizr DOM node.
/* 292  *|      */
/* 293  */     function setCss( str ) {
/* 294  */         mStyle.cssText = str;
/* 295  */     }
/* 296  */ 
/* 297  */     /**
/* 298  *|      * setCssAll extrapolates all vendor-specific css strings.
/* 299  *|      */
/* 300  */     function setCssAll( str1, str2 ) {

/* foundation.min.js */

/* 301  */         return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
/* 302  */     }
/* 303  */ 
/* 304  */     /**
/* 305  *|      * is returns a boolean for if typeof obj is exactly type.
/* 306  *|      */
/* 307  */     function is( obj, type ) {
/* 308  */         return typeof obj === type;
/* 309  */     }
/* 310  */ 
/* 311  */     /**
/* 312  *|      * contains returns a boolean for if substr is found within str.
/* 313  *|      */
/* 314  */     function contains( str, substr ) {
/* 315  */         return !!~('' + str).indexOf(substr);
/* 316  */     }
/* 317  */ 
/* 318  */     /*>>testprop*/
/* 319  */ 
/* 320  */     // testProps is a generic CSS / DOM property test.
/* 321  */ 
/* 322  */     // In testing support for a given CSS property, it's legit to test:
/* 323  */     //    `elem.style[styleName] !== undefined`
/* 324  */     // If the property is supported it will return an empty string,
/* 325  */     // if unsupported it will return undefined.
/* 326  */ 
/* 327  */     // We'll take advantage of this quick test and skip setting a style
/* 328  */     // on our modernizr element, but instead just testing undefined vs
/* 329  */     // empty string.
/* 330  */ 
/* 331  */     // Because the testing of the CSS property names (with "-", as
/* 332  */     // opposed to the camelCase DOM properties) is non-portable and
/* 333  */     // non-standard but works in WebKit and IE (but not Gecko or Opera),
/* 334  */     // we explicitly reject properties with dashes so that authors
/* 335  */     // developing in WebKit or IE first don't end up with
/* 336  */     // browser-specific content by accident.
/* 337  */ 
/* 338  */     function testProps( props, prefixed ) {
/* 339  */         for ( var i in props ) {
/* 340  */             var prop = props[i];
/* 341  */             if ( !contains(prop, "-") && mStyle[prop] !== undefined ) {
/* 342  */                 return prefixed == 'pfx' ? prop : true;
/* 343  */             }
/* 344  */         }
/* 345  */         return false;
/* 346  */     }
/* 347  */     /*>>testprop*/
/* 348  */ 
/* 349  */     // TODO :: add testDOMProps
/* 350  */     /**

/* foundation.min.js */

/* 351  *|      * testDOMProps is a generic DOM property test; if a browser supports
/* 352  *|      *   a certain property, it won't return undefined for it.
/* 353  *|      */
/* 354  */     function testDOMProps( props, obj, elem ) {
/* 355  */         for ( var i in props ) {
/* 356  */             var item = obj[props[i]];
/* 357  */             if ( item !== undefined) {
/* 358  */ 
/* 359  */                 // return the property name as a string
/* 360  */                 if (elem === false) return props[i];
/* 361  */ 
/* 362  */                 // let's bind a function
/* 363  */                 if (is(item, 'function')){
/* 364  */                   // default to autobind unless override
/* 365  */                   return item.bind(elem || obj);
/* 366  */                 }
/* 367  */ 
/* 368  */                 // return the unbound function or obj or value
/* 369  */                 return item;
/* 370  */             }
/* 371  */         }
/* 372  */         return false;
/* 373  */     }
/* 374  */ 
/* 375  */     /*>>testallprops*/
/* 376  */     /**
/* 377  *|      * testPropsAll tests a list of DOM properties we want to check against.
/* 378  *|      *   We specify literally ALL possible (known and/or likely) properties on
/* 379  *|      *   the element including the non-vendor prefixed one, for forward-
/* 380  *|      *   compatibility.
/* 381  *|      */
/* 382  */     function testPropsAll( prop, prefixed, elem ) {
/* 383  */ 
/* 384  */         var ucProp  = prop.charAt(0).toUpperCase() + prop.slice(1),
/* 385  */             props   = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');
/* 386  */ 
/* 387  */         // did they call .prefixed('boxSizing') or are we just testing a prop?
/* 388  */         if(is(prefixed, "string") || is(prefixed, "undefined")) {
/* 389  */           return testProps(props, prefixed);
/* 390  */ 
/* 391  */         // otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
/* 392  */         } else {
/* 393  */           props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
/* 394  */           return testDOMProps(props, prefixed, elem);
/* 395  */         }
/* 396  */     }
/* 397  */     /*>>testallprops*/
/* 398  */ 
/* 399  */ 
/* 400  */     /**

/* foundation.min.js */

/* 401  *|      * Tests
/* 402  *|      * -----
/* 403  *|      */
/* 404  */ 
/* 405  */     // The *new* flexbox
/* 406  */     // dev.w3.org/csswg/css3-flexbox
/* 407  */ 
/* 408  */     tests['flexbox'] = function() {
/* 409  */       return testPropsAll('flexWrap');
/* 410  */     };
/* 411  */ 
/* 412  */     // The *old* flexbox
/* 413  */     // www.w3.org/TR/2009/WD-css3-flexbox-20090723/
/* 414  */ 
/* 415  */     tests['flexboxlegacy'] = function() {
/* 416  */         return testPropsAll('boxDirection');
/* 417  */     };
/* 418  */ 
/* 419  */     // On the S60 and BB Storm, getContext exists, but always returns undefined
/* 420  */     // so we actually have to call getContext() to verify
/* 421  */     // github.com/Modernizr/Modernizr/issues/issue/97/
/* 422  */ 
/* 423  */     tests['canvas'] = function() {
/* 424  */         var elem = document.createElement('canvas');
/* 425  */         return !!(elem.getContext && elem.getContext('2d'));
/* 426  */     };
/* 427  */ 
/* 428  */     tests['canvastext'] = function() {
/* 429  */         return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
/* 430  */     };
/* 431  */ 
/* 432  */     // webk.it/70117 is tracking a legit WebGL feature detect proposal
/* 433  */ 
/* 434  */     // We do a soft detect which may false positive in order to avoid
/* 435  */     // an expensive context creation: bugzil.la/732441
/* 436  */ 
/* 437  */     tests['webgl'] = function() {
/* 438  */         return !!window.WebGLRenderingContext;
/* 439  */     };
/* 440  */ 
/* 441  */     /*
/* 442  *|      * The Modernizr.touch test only indicates if the browser supports
/* 443  *|      *    touch events, which does not necessarily reflect a touchscreen
/* 444  *|      *    device, as evidenced by tablets running Windows 7 or, alas,
/* 445  *|      *    the Palm Pre / WebOS (touch) phones.
/* 446  *|      *
/* 447  *|      * Additionally, Chrome (desktop) used to lie about its support on this,
/* 448  *|      *    but that has since been rectified: crbug.com/36415
/* 449  *|      *
/* 450  *|      * We also test for Firefox 4 Multitouch Support.

/* foundation.min.js */

/* 451  *|      *
/* 452  *|      * For more info, see: modernizr.github.com/Modernizr/touch.html
/* 453  *|      */
/* 454  */ 
/* 455  */     tests['touch'] = function() {
/* 456  */         var bool;
/* 457  */ 
/* 458  */         if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
/* 459  */           bool = true;
/* 460  */         } else {
/* 461  */           injectElementWithStyles(['@media (',prefixes.join('touch-enabled),('),mod,')','{#modernizr{top:9px;position:absolute}}'].join(''), function( node ) {
/* 462  */             bool = node.offsetTop === 9;
/* 463  */           });
/* 464  */         }
/* 465  */ 
/* 466  */         return bool;
/* 467  */     };
/* 468  */ 
/* 469  */ 
/* 470  */     // geolocation is often considered a trivial feature detect...
/* 471  */     // Turns out, it's quite tricky to get right:
/* 472  */     //
/* 473  */     // Using !!navigator.geolocation does two things we don't want. It:
/* 474  */     //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
/* 475  */     //   2. Disables page caching in WebKit: webk.it/43956
/* 476  */     //
/* 477  */     // Meanwhile, in Firefox < 8, an about:config setting could expose
/* 478  */     // a false positive that would throw an exception: bugzil.la/688158
/* 479  */ 
/* 480  */     tests['geolocation'] = function() {
/* 481  */         return 'geolocation' in navigator;
/* 482  */     };
/* 483  */ 
/* 484  */ 
/* 485  */     tests['postmessage'] = function() {
/* 486  */       return !!window.postMessage;
/* 487  */     };
/* 488  */ 
/* 489  */ 
/* 490  */     // Chrome incognito mode used to throw an exception when using openDatabase
/* 491  */     // It doesn't anymore.
/* 492  */     tests['websqldatabase'] = function() {
/* 493  */       return !!window.openDatabase;
/* 494  */     };
/* 495  */ 
/* 496  */     // Vendors had inconsistent prefixing with the experimental Indexed DB:
/* 497  */     // - Webkit's implementation is accessible through webkitIndexedDB
/* 498  */     // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
/* 499  */     // For speed, we don't test the legacy (and beta-only) indexedDB
/* 500  */     tests['indexedDB'] = function() {

/* foundation.min.js */

/* 501  */       return !!testPropsAll("indexedDB", window);
/* 502  */     };
/* 503  */ 
/* 504  */     // documentMode logic from YUI to filter out IE8 Compat Mode
/* 505  */     //   which false positives.
/* 506  */     tests['hashchange'] = function() {
/* 507  */       return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
/* 508  */     };
/* 509  */ 
/* 510  */     // Per 1.6:
/* 511  */     // This used to be Modernizr.historymanagement but the longer
/* 512  */     // name has been deprecated in favor of a shorter and property-matching one.
/* 513  */     // The old API is still available in 1.6, but as of 2.0 will throw a warning,
/* 514  */     // and in the first release thereafter disappear entirely.
/* 515  */     tests['history'] = function() {
/* 516  */       return !!(window.history && history.pushState);
/* 517  */     };
/* 518  */ 
/* 519  */     tests['draganddrop'] = function() {
/* 520  */         var div = document.createElement('div');
/* 521  */         return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
/* 522  */     };
/* 523  */ 
/* 524  */     // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
/* 525  */     // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
/* 526  */     // FF10 still uses prefixes, so check for it until then.
/* 527  */     // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
/* 528  */     tests['websockets'] = function() {
/* 529  */         return 'WebSocket' in window || 'MozWebSocket' in window;
/* 530  */     };
/* 531  */ 
/* 532  */ 
/* 533  */     // css-tricks.com/rgba-browser-support/
/* 534  */     tests['rgba'] = function() {
/* 535  */         // Set an rgba() color and check the returned value
/* 536  */ 
/* 537  */         setCss('background-color:rgba(150,255,150,.5)');
/* 538  */ 
/* 539  */         return contains(mStyle.backgroundColor, 'rgba');
/* 540  */     };
/* 541  */ 
/* 542  */     tests['hsla'] = function() {
/* 543  */         // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
/* 544  */         //   except IE9 who retains it as hsla
/* 545  */ 
/* 546  */         setCss('background-color:hsla(120,40%,100%,.5)');
/* 547  */ 
/* 548  */         return contains(mStyle.backgroundColor, 'rgba') || contains(mStyle.backgroundColor, 'hsla');
/* 549  */     };
/* 550  */ 

/* foundation.min.js */

/* 551  */     tests['multiplebgs'] = function() {
/* 552  */         // Setting multiple images AND a color on the background shorthand property
/* 553  */         //  and then querying the style.background property value for the number of
/* 554  */         //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!
/* 555  */ 
/* 556  */         setCss('background:url(https://),url(https://),red url(https://)');
/* 557  */ 
/* 558  */         // If the UA supports multiple backgrounds, there should be three occurrences
/* 559  */         //   of the string "url(" in the return value for elemStyle.background
/* 560  */ 
/* 561  */         return (/(url\s*\(.*?){3}/).test(mStyle.background);
/* 562  */     };
/* 563  */ 
/* 564  */ 
/* 565  */ 
/* 566  */     // this will false positive in Opera Mini
/* 567  */     //   github.com/Modernizr/Modernizr/issues/396
/* 568  */ 
/* 569  */     tests['backgroundsize'] = function() {
/* 570  */         return testPropsAll('backgroundSize');
/* 571  */     };
/* 572  */ 
/* 573  */     tests['borderimage'] = function() {
/* 574  */         return testPropsAll('borderImage');
/* 575  */     };
/* 576  */ 
/* 577  */ 
/* 578  */     // Super comprehensive table about all the unique implementations of
/* 579  */     // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance
/* 580  */ 
/* 581  */     tests['borderradius'] = function() {
/* 582  */         return testPropsAll('borderRadius');
/* 583  */     };
/* 584  */ 
/* 585  */     // WebOS unfortunately false positives on this test.
/* 586  */     tests['boxshadow'] = function() {
/* 587  */         return testPropsAll('boxShadow');
/* 588  */     };
/* 589  */ 
/* 590  */     // FF3.0 will false positive on this test
/* 591  */     tests['textshadow'] = function() {
/* 592  */         return document.createElement('div').style.textShadow === '';
/* 593  */     };
/* 594  */ 
/* 595  */ 
/* 596  */     tests['opacity'] = function() {
/* 597  */         // Browsers that actually have CSS Opacity implemented have done so
/* 598  */         //  according to spec, which means their return values are within the
/* 599  */         //  range of [0.0,1.0] - including the leading zero.
/* 600  */ 

/* foundation.min.js */

/* 601  */         setCssAll('opacity:.55');
/* 602  */ 
/* 603  */         // The non-literal . in this regex is intentional:
/* 604  */         //   German Chrome returns this value as 0,55
/* 605  */         // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
/* 606  */         return (/^0.55$/).test(mStyle.opacity);
/* 607  */     };
/* 608  */ 
/* 609  */ 
/* 610  */     // Note, Android < 4 will pass this test, but can only animate
/* 611  */     //   a single property at a time
/* 612  */     //   goo.gl/v3V4Gp
/* 613  */     tests['cssanimations'] = function() {
/* 614  */         return testPropsAll('animationName');
/* 615  */     };
/* 616  */ 
/* 617  */ 
/* 618  */     tests['csscolumns'] = function() {
/* 619  */         return testPropsAll('columnCount');
/* 620  */     };
/* 621  */ 
/* 622  */ 
/* 623  */     tests['cssgradients'] = function() {
/* 624  */         /**
/* 625  *|          * For CSS Gradients syntax, please see:
/* 626  *|          * webkit.org/blog/175/introducing-css-gradients/
/* 627  *|          * developer.mozilla.org/en/CSS/-moz-linear-gradient
/* 628  *|          * developer.mozilla.org/en/CSS/-moz-radial-gradient
/* 629  *|          * dev.w3.org/csswg/css3-images/#gradients-
/* 630  *|          */
/* 631  */ 
/* 632  */         var str1 = 'background-image:',
/* 633  */             str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
/* 634  */             str3 = 'linear-gradient(left top,#9f9, white);';
/* 635  */ 
/* 636  */         setCss(
/* 637  */              // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
/* 638  */               (str1 + '-webkit- '.split(' ').join(str2 + str1) +
/* 639  */              // standard syntax             // trailing 'background-image:'
/* 640  */               prefixes.join(str3 + str1)).slice(0, -str1.length)
/* 641  */         );
/* 642  */ 
/* 643  */         return contains(mStyle.backgroundImage, 'gradient');
/* 644  */     };
/* 645  */ 
/* 646  */ 
/* 647  */     tests['cssreflections'] = function() {
/* 648  */         return testPropsAll('boxReflect');
/* 649  */     };
/* 650  */ 

/* foundation.min.js */

/* 651  */ 
/* 652  */     tests['csstransforms'] = function() {
/* 653  */         return !!testPropsAll('transform');
/* 654  */     };
/* 655  */ 
/* 656  */ 
/* 657  */     tests['csstransforms3d'] = function() {
/* 658  */ 
/* 659  */         var ret = !!testPropsAll('perspective');
/* 660  */ 
/* 661  */         // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
/* 662  */         //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
/* 663  */         //   some conditions. As a result, Webkit typically recognizes the syntax but
/* 664  */         //   will sometimes throw a false positive, thus we must do a more thorough check:
/* 665  */         if ( ret && 'webkitPerspective' in docElement.style ) {
/* 666  */ 
/* 667  */           // Webkit allows this media query to succeed only if the feature is enabled.
/* 668  */           // `@media (transform-3d),(-webkit-transform-3d){ ... }`
/* 669  */           injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function( node, rule ) {
/* 670  */             ret = node.offsetLeft === 9 && node.offsetHeight === 3;
/* 671  */           });
/* 672  */         }
/* 673  */         return ret;
/* 674  */     };
/* 675  */ 
/* 676  */ 
/* 677  */     tests['csstransitions'] = function() {
/* 678  */         return testPropsAll('transition');
/* 679  */     };
/* 680  */ 
/* 681  */ 
/* 682  */     /*>>fontface*/
/* 683  */     // @font-face detection routine by Diego Perini
/* 684  */     // javascript.nwbox.com/CSSSupport/
/* 685  */ 
/* 686  */     // false positives:
/* 687  */     //   WebOS github.com/Modernizr/Modernizr/issues/342
/* 688  */     //   WP7   github.com/Modernizr/Modernizr/issues/538
/* 689  */     tests['fontface'] = function() {
/* 690  */         var bool;
/* 691  */ 
/* 692  */         injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function( node, rule ) {
/* 693  */           var style = document.getElementById('smodernizr'),
/* 694  */               sheet = style.sheet || style.styleSheet,
/* 695  */               cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';
/* 696  */ 
/* 697  */           bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
/* 698  */         });
/* 699  */ 
/* 700  */         return bool;

/* foundation.min.js */

/* 701  */     };
/* 702  */     /*>>fontface*/
/* 703  */ 
/* 704  */     // CSS generated content detection
/* 705  */     tests['generatedcontent'] = function() {
/* 706  */         var bool;
/* 707  */ 
/* 708  */         injectElementWithStyles(['#',mod,'{font:0/0 a}#',mod,':after{content:"',smile,'";visibility:hidden;font:3px/1 a}'].join(''), function( node ) {
/* 709  */           bool = node.offsetHeight >= 3;
/* 710  */         });
/* 711  */ 
/* 712  */         return bool;
/* 713  */     };
/* 714  */ 
/* 715  */ 
/* 716  */ 
/* 717  */     // These tests evaluate support of the video/audio elements, as well as
/* 718  */     // testing what types of content they support.
/* 719  */     //
/* 720  */     // We're using the Boolean constructor here, so that we can extend the value
/* 721  */     // e.g.  Modernizr.video     // true
/* 722  */     //       Modernizr.video.ogg // 'probably'
/* 723  */     //
/* 724  */     // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
/* 725  */     //                     thx to NielsLeenheer and zcorpan
/* 726  */ 
/* 727  */     // Note: in some older browsers, "no" was a return value instead of empty string.
/* 728  */     //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
/* 729  */     //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5
/* 730  */ 
/* 731  */     tests['video'] = function() {
/* 732  */         var elem = document.createElement('video'),
/* 733  */             bool = false;
/* 734  */ 
/* 735  */         // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
/* 736  */         try {
/* 737  */             if ( bool = !!elem.canPlayType ) {
/* 738  */                 bool      = new Boolean(bool);
/* 739  */                 bool.ogg  = elem.canPlayType('video/ogg; codecs="theora"')      .replace(/^no$/,'');
/* 740  */ 
/* 741  */                 // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
/* 742  */                 bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"') .replace(/^no$/,'');
/* 743  */ 
/* 744  */                 bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');
/* 745  */             }
/* 746  */ 
/* 747  */         } catch(e) { }
/* 748  */ 
/* 749  */         return bool;
/* 750  */     };

/* foundation.min.js */

/* 751  */ 
/* 752  */     tests['audio'] = function() {
/* 753  */         var elem = document.createElement('audio'),
/* 754  */             bool = false;
/* 755  */ 
/* 756  */         try {
/* 757  */             if ( bool = !!elem.canPlayType ) {
/* 758  */                 bool      = new Boolean(bool);
/* 759  */                 bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'');
/* 760  */                 bool.mp3  = elem.canPlayType('audio/mpeg;')               .replace(/^no$/,'');
/* 761  */ 
/* 762  */                 // Mimetypes accepted:
/* 763  */                 //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
/* 764  */                 //   bit.ly/iphoneoscodecs
/* 765  */                 bool.wav  = elem.canPlayType('audio/wav; codecs="1"')     .replace(/^no$/,'');
/* 766  */                 bool.m4a  = ( elem.canPlayType('audio/x-m4a;')            ||
/* 767  */                               elem.canPlayType('audio/aac;'))             .replace(/^no$/,'');
/* 768  */             }
/* 769  */         } catch(e) { }
/* 770  */ 
/* 771  */         return bool;
/* 772  */     };
/* 773  */ 
/* 774  */ 
/* 775  */     // In FF4, if disabled, window.localStorage should === null.
/* 776  */ 
/* 777  */     // Normally, we could not test that directly and need to do a
/* 778  */     //   `('localStorage' in window) && ` test first because otherwise Firefox will
/* 779  */     //   throw bugzil.la/365772 if cookies are disabled
/* 780  */ 
/* 781  */     // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
/* 782  */     // will throw the exception:
/* 783  */     //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
/* 784  */     // Peculiarly, getItem and removeItem calls do not throw.
/* 785  */ 
/* 786  */     // Because we are forced to try/catch this, we'll go aggressive.
/* 787  */ 
/* 788  */     // Just FWIW: IE8 Compat mode supports these features completely:
/* 789  */     //   www.quirksmode.org/dom/html5.html
/* 790  */     // But IE8 doesn't support either with local files
/* 791  */ 
/* 792  */     tests['localstorage'] = function() {
/* 793  */         try {
/* 794  */             localStorage.setItem(mod, mod);
/* 795  */             localStorage.removeItem(mod);
/* 796  */             return true;
/* 797  */         } catch(e) {
/* 798  */             return false;
/* 799  */         }
/* 800  */     };

/* foundation.min.js */

/* 801  */ 
/* 802  */     tests['sessionstorage'] = function() {
/* 803  */         try {
/* 804  */             sessionStorage.setItem(mod, mod);
/* 805  */             sessionStorage.removeItem(mod);
/* 806  */             return true;
/* 807  */         } catch(e) {
/* 808  */             return false;
/* 809  */         }
/* 810  */     };
/* 811  */ 
/* 812  */ 
/* 813  */     tests['webworkers'] = function() {
/* 814  */         return !!window.Worker;
/* 815  */     };
/* 816  */ 
/* 817  */ 
/* 818  */     tests['applicationcache'] = function() {
/* 819  */         return !!window.applicationCache;
/* 820  */     };
/* 821  */ 
/* 822  */ 
/* 823  */     // Thanks to Erik Dahlstrom
/* 824  */     tests['svg'] = function() {
/* 825  */         return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
/* 826  */     };
/* 827  */ 
/* 828  */     // specifically for SVG inline in HTML, not within XHTML
/* 829  */     // test page: paulirish.com/demo/inline-svg
/* 830  */     tests['inlinesvg'] = function() {
/* 831  */       var div = document.createElement('div');
/* 832  */       div.innerHTML = '<svg/>';
/* 833  */       return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
/* 834  */     };
/* 835  */ 
/* 836  */     // SVG SMIL animation
/* 837  */     tests['smil'] = function() {
/* 838  */         return !!document.createElementNS && /SVGAnimate/.test(toString.call(document.createElementNS(ns.svg, 'animate')));
/* 839  */     };
/* 840  */ 
/* 841  */     // This test is only for clip paths in SVG proper, not clip paths on HTML content
/* 842  */     // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg
/* 843  */ 
/* 844  */     // However read the comments to dig into applying SVG clippaths to HTML content here:
/* 845  */     //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
/* 846  */     tests['svgclippaths'] = function() {
/* 847  */         return !!document.createElementNS && /SVGClipPath/.test(toString.call(document.createElementNS(ns.svg, 'clipPath')));
/* 848  */     };
/* 849  */ 
/* 850  */     /*>>webforms*/

/* foundation.min.js */

/* 851  */     // input features and input types go directly onto the ret object, bypassing the tests loop.
/* 852  */     // Hold this guy to execute in a moment.
/* 853  */     function webforms() {
/* 854  */         /*>>input*/
/* 855  */         // Run through HTML5's new input attributes to see if the UA understands any.
/* 856  */         // We're using f which is the <input> element created early on
/* 857  */         // Mike Taylr has created a comprehensive resource for testing these attributes
/* 858  */         //   when applied to all input types:
/* 859  */         //   miketaylr.com/code/input-type-attr.html
/* 860  */         // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary
/* 861  */ 
/* 862  */         // Only input placeholder is tested while textarea's placeholder is not.
/* 863  */         // Currently Safari 4 and Opera 11 have support only for the input placeholder
/* 864  */         // Both tests are available in feature-detects/forms-placeholder.js
/* 865  */         Modernizr['input'] = (function( props ) {
/* 866  */             for ( var i = 0, len = props.length; i < len; i++ ) {
/* 867  */                 attrs[ props[i] ] = !!(props[i] in inputElem);
/* 868  */             }
/* 869  */             if (attrs.list){
/* 870  */               // safari false positive's on datalist: webk.it/74252
/* 871  */               // see also github.com/Modernizr/Modernizr/issues/146
/* 872  */               attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
/* 873  */             }
/* 874  */             return attrs;
/* 875  */         })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
/* 876  */         /*>>input*/
/* 877  */ 
/* 878  */         /*>>inputtypes*/
/* 879  */         // Run through HTML5's new input types to see if the UA understands any.
/* 880  */         //   This is put behind the tests runloop because it doesn't return a
/* 881  */         //   true/false like all the other tests; instead, it returns an object
/* 882  */         //   containing each input type with its corresponding true/false value
/* 883  */ 
/* 884  */         // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
/* 885  */         Modernizr['inputtypes'] = (function(props) {
/* 886  */ 
/* 887  */             for ( var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++ ) {
/* 888  */ 
/* 889  */                 inputElem.setAttribute('type', inputElemType = props[i]);
/* 890  */                 bool = inputElem.type !== 'text';
/* 891  */ 
/* 892  */                 // We first check to see if the type we give it sticks..
/* 893  */                 // If the type does, we feed it a textual value, which shouldn't be valid.
/* 894  */                 // If the value doesn't stick, we know there's input sanitization which infers a custom UI
/* 895  */                 if ( bool ) {
/* 896  */ 
/* 897  */                     inputElem.value         = smile;
/* 898  */                     inputElem.style.cssText = 'position:absolute;visibility:hidden;';
/* 899  */ 
/* 900  */                     if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {

/* foundation.min.js */

/* 901  */ 
/* 902  */                       docElement.appendChild(inputElem);
/* 903  */                       defaultView = document.defaultView;
/* 904  */ 
/* 905  */                       // Safari 2-4 allows the smiley as a value, despite making a slider
/* 906  */                       bool =  defaultView.getComputedStyle &&
/* 907  */                               defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
/* 908  */                               // Mobile android web browser has false positive, so must
/* 909  */                               // check the height to see if the widget is actually there.
/* 910  */                               (inputElem.offsetHeight !== 0);
/* 911  */ 
/* 912  */                       docElement.removeChild(inputElem);
/* 913  */ 
/* 914  */                     } else if ( /^(search|tel)$/.test(inputElemType) ){
/* 915  */                       // Spec doesn't define any special parsing or detectable UI
/* 916  */                       //   behaviors so we pass these through as true
/* 917  */ 
/* 918  */                       // Interestingly, opera fails the earlier test, so it doesn't
/* 919  */                       //  even make it here.
/* 920  */ 
/* 921  */                     } else if ( /^(url|email)$/.test(inputElemType) ) {
/* 922  */                       // Real url and email support comes with prebaked validation.
/* 923  */                       bool = inputElem.checkValidity && inputElem.checkValidity() === false;
/* 924  */ 
/* 925  */                     } else {
/* 926  */                       // If the upgraded input compontent rejects the :) text, we got a winner
/* 927  */                       bool = inputElem.value != smile;
/* 928  */                     }
/* 929  */                 }
/* 930  */ 
/* 931  */                 inputs[ props[i] ] = !!bool;
/* 932  */             }
/* 933  */             return inputs;
/* 934  */         })('search tel url email datetime date month week time datetime-local number range color'.split(' '));
/* 935  */         /*>>inputtypes*/
/* 936  */     }
/* 937  */     /*>>webforms*/
/* 938  */ 
/* 939  */ 
/* 940  */     // End of test definitions
/* 941  */     // -----------------------
/* 942  */ 
/* 943  */ 
/* 944  */ 
/* 945  */     // Run through all tests and detect their support in the current UA.
/* 946  */     // todo: hypothetically we could be doing an array of tests and use a basic loop here.
/* 947  */     for ( var feature in tests ) {
/* 948  */         if ( hasOwnProp(tests, feature) ) {
/* 949  */             // run the test, throw the return value into the Modernizr,
/* 950  */             //   then based on that boolean, define an appropriate className

/* foundation.min.js */

/* 951  */             //   and push it into an array of classes we'll join later.
/* 952  */             featureName  = feature.toLowerCase();
/* 953  */             Modernizr[featureName] = tests[feature]();
/* 954  */ 
/* 955  */             classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
/* 956  */         }
/* 957  */     }
/* 958  */ 
/* 959  */     /*>>webforms*/
/* 960  */     // input tests need to run.
/* 961  */     Modernizr.input || webforms();
/* 962  */     /*>>webforms*/
/* 963  */ 
/* 964  */ 
/* 965  */     /**
/* 966  *|      * addTest allows the user to define their own feature tests
/* 967  *|      * the result will be added onto the Modernizr object,
/* 968  *|      * as well as an appropriate className set on the html element
/* 969  *|      *
/* 970  *|      * @param feature - String naming the feature
/* 971  *|      * @param test - Function returning true if feature is supported, false if not
/* 972  *|      */
/* 973  */      Modernizr.addTest = function ( feature, test ) {
/* 974  */        if ( typeof feature == 'object' ) {
/* 975  */          for ( var key in feature ) {
/* 976  */            if ( hasOwnProp( feature, key ) ) {
/* 977  */              Modernizr.addTest( key, feature[ key ] );
/* 978  */            }
/* 979  */          }
/* 980  */        } else {
/* 981  */ 
/* 982  */          feature = feature.toLowerCase();
/* 983  */ 
/* 984  */          if ( Modernizr[feature] !== undefined ) {
/* 985  */            // we're going to quit if you're trying to overwrite an existing test
/* 986  */            // if we were to allow it, we'd do this:
/* 987  */            //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
/* 988  */            //   docElement.className = docElement.className.replace( re, '' );
/* 989  */            // but, no rly, stuff 'em.
/* 990  */            return Modernizr;
/* 991  */          }
/* 992  */ 
/* 993  */          test = typeof test == 'function' ? test() : test;
/* 994  */ 
/* 995  */          if (typeof enableClasses !== "undefined" && enableClasses) {
/* 996  */            docElement.className += ' ' + (test ? '' : 'no-') + feature;
/* 997  */          }
/* 998  */          Modernizr[feature] = test;
/* 999  */ 
/* 1000 */        }

/* foundation.min.js */

/* 1001 */ 
/* 1002 */        return Modernizr; // allow chaining.
/* 1003 */      };
/* 1004 */ 
/* 1005 */ 
/* 1006 */     // Reset modElem.cssText to nothing to reduce memory footprint.
/* 1007 */     setCss('');
/* 1008 */     modElem = inputElem = null;
/* 1009 */ 
/* 1010 */     /*>>shiv*/
/* 1011 */     /**
/* 1012 *|      * @preserve HTML5 Shiv prev3.7.1 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
/* 1013 *|      */
/* 1014 */     ;(function(window, document) {
/* 1015 */         /*jshint evil:true */
/* 1016 */         /** version */
/* 1017 */         var version = '3.7.0';
/* 1018 */ 
/* 1019 */         /** Preset options */
/* 1020 */         var options = window.html5 || {};
/* 1021 */ 
/* 1022 */         /** Used to skip problem elements */
/* 1023 */         var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;
/* 1024 */ 
/* 1025 */         /** Not all elements can be cloned in IE **/
/* 1026 */         var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;
/* 1027 */ 
/* 1028 */         /** Detect whether the browser supports default html5 styles */
/* 1029 */         var supportsHtml5Styles;
/* 1030 */ 
/* 1031 */         /** Name of the expando, to work with multiple documents or to re-shiv one document */
/* 1032 */         var expando = '_html5shiv';
/* 1033 */ 
/* 1034 */         /** The id for the the documents expando */
/* 1035 */         var expanID = 0;
/* 1036 */ 
/* 1037 */         /** Cached data for each document */
/* 1038 */         var expandoData = {};
/* 1039 */ 
/* 1040 */         /** Detect whether the browser supports unknown elements */
/* 1041 */         var supportsUnknownElements;
/* 1042 */ 
/* 1043 */         (function() {
/* 1044 */           try {
/* 1045 */             var a = document.createElement('a');
/* 1046 */             a.innerHTML = '<xyz></xyz>';
/* 1047 */             //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
/* 1048 */             supportsHtml5Styles = ('hidden' in a);
/* 1049 */ 
/* 1050 */             supportsUnknownElements = a.childNodes.length == 1 || (function() {

/* foundation.min.js */

/* 1051 */               // assign a false positive if unable to shiv
/* 1052 */               (document.createElement)('a');
/* 1053 */               var frag = document.createDocumentFragment();
/* 1054 */               return (
/* 1055 */                 typeof frag.cloneNode == 'undefined' ||
/* 1056 */                 typeof frag.createDocumentFragment == 'undefined' ||
/* 1057 */                 typeof frag.createElement == 'undefined'
/* 1058 */               );
/* 1059 */             }());
/* 1060 */           } catch(e) {
/* 1061 */             // assign a false positive if detection fails => unable to shiv
/* 1062 */             supportsHtml5Styles = true;
/* 1063 */             supportsUnknownElements = true;
/* 1064 */           }
/* 1065 */ 
/* 1066 */         }());
/* 1067 */ 
/* 1068 */         /*--------------------------------------------------------------------------*/
/* 1069 */ 
/* 1070 */         /**
/* 1071 *|          * Creates a style sheet with the given CSS text and adds it to the document.
/* 1072 *|          * @private
/* 1073 *|          * @param {Document} ownerDocument The document.
/* 1074 *|          * @param {String} cssText The CSS text.
/* 1075 *|          * @returns {StyleSheet} The style element.
/* 1076 *|          */
/* 1077 */         function addStyleSheet(ownerDocument, cssText) {
/* 1078 */           var p = ownerDocument.createElement('p'),
/* 1079 */           parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;
/* 1080 */ 
/* 1081 */           p.innerHTML = 'x<style>' + cssText + '</style>';
/* 1082 */           return parent.insertBefore(p.lastChild, parent.firstChild);
/* 1083 */         }
/* 1084 */ 
/* 1085 */         /**
/* 1086 *|          * Returns the value of `html5.elements` as an array.
/* 1087 *|          * @private
/* 1088 *|          * @returns {Array} An array of shived element node names.
/* 1089 *|          */
/* 1090 */         function getElements() {
/* 1091 */           var elements = html5.elements;
/* 1092 */           return typeof elements == 'string' ? elements.split(' ') : elements;
/* 1093 */         }
/* 1094 */ 
/* 1095 */         /**
/* 1096 *|          * Returns the data associated to the given document
/* 1097 *|          * @private
/* 1098 *|          * @param {Document} ownerDocument The document.
/* 1099 *|          * @returns {Object} An object of data.
/* 1100 *|          */

/* foundation.min.js */

/* 1101 */         function getExpandoData(ownerDocument) {
/* 1102 */           var data = expandoData[ownerDocument[expando]];
/* 1103 */           if (!data) {
/* 1104 */             data = {};
/* 1105 */             expanID++;
/* 1106 */             ownerDocument[expando] = expanID;
/* 1107 */             expandoData[expanID] = data;
/* 1108 */           }
/* 1109 */           return data;
/* 1110 */         }
/* 1111 */ 
/* 1112 */         /**
/* 1113 *|          * returns a shived element for the given nodeName and document
/* 1114 *|          * @memberOf html5
/* 1115 *|          * @param {String} nodeName name of the element
/* 1116 *|          * @param {Document} ownerDocument The context document.
/* 1117 *|          * @returns {Object} The shived element.
/* 1118 *|          */
/* 1119 */         function createElement(nodeName, ownerDocument, data){
/* 1120 */           if (!ownerDocument) {
/* 1121 */             ownerDocument = document;
/* 1122 */           }
/* 1123 */           if(supportsUnknownElements){
/* 1124 */             return ownerDocument.createElement(nodeName);
/* 1125 */           }
/* 1126 */           if (!data) {
/* 1127 */             data = getExpandoData(ownerDocument);
/* 1128 */           }
/* 1129 */           var node;
/* 1130 */ 
/* 1131 */           if (data.cache[nodeName]) {
/* 1132 */             node = data.cache[nodeName].cloneNode();
/* 1133 */           } else if (saveClones.test(nodeName)) {
/* 1134 */             node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
/* 1135 */           } else {
/* 1136 */             node = data.createElem(nodeName);
/* 1137 */           }
/* 1138 */ 
/* 1139 */           // Avoid adding some elements to fragments in IE < 9 because
/* 1140 */           // * Attributes like `name` or `type` cannot be set/changed once an element
/* 1141 */           //   is inserted into a document/fragment
/* 1142 */           // * Link elements with `src` attributes that are inaccessible, as with
/* 1143 */           //   a 403 response, will cause the tab/window to crash
/* 1144 */           // * Script elements appended to fragments will execute when their `src`
/* 1145 */           //   or `text` property is set
/* 1146 */           return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
/* 1147 */         }
/* 1148 */ 
/* 1149 */         /**
/* 1150 *|          * returns a shived DocumentFragment for the given document

/* foundation.min.js */

/* 1151 *|          * @memberOf html5
/* 1152 *|          * @param {Document} ownerDocument The context document.
/* 1153 *|          * @returns {Object} The shived DocumentFragment.
/* 1154 *|          */
/* 1155 */         function createDocumentFragment(ownerDocument, data){
/* 1156 */           if (!ownerDocument) {
/* 1157 */             ownerDocument = document;
/* 1158 */           }
/* 1159 */           if(supportsUnknownElements){
/* 1160 */             return ownerDocument.createDocumentFragment();
/* 1161 */           }
/* 1162 */           data = data || getExpandoData(ownerDocument);
/* 1163 */           var clone = data.frag.cloneNode(),
/* 1164 */           i = 0,
/* 1165 */           elems = getElements(),
/* 1166 */           l = elems.length;
/* 1167 */           for(;i<l;i++){
/* 1168 */             clone.createElement(elems[i]);
/* 1169 */           }
/* 1170 */           return clone;
/* 1171 */         }
/* 1172 */ 
/* 1173 */         /**
/* 1174 *|          * Shivs the `createElement` and `createDocumentFragment` methods of the document.
/* 1175 *|          * @private
/* 1176 *|          * @param {Document|DocumentFragment} ownerDocument The document.
/* 1177 *|          * @param {Object} data of the document.
/* 1178 *|          */
/* 1179 */         function shivMethods(ownerDocument, data) {
/* 1180 */           if (!data.cache) {
/* 1181 */             data.cache = {};
/* 1182 */             data.createElem = ownerDocument.createElement;
/* 1183 */             data.createFrag = ownerDocument.createDocumentFragment;
/* 1184 */             data.frag = data.createFrag();
/* 1185 */           }
/* 1186 */ 
/* 1187 */ 
/* 1188 */           ownerDocument.createElement = function(nodeName) {
/* 1189 */             //abort shiv
/* 1190 */             if (!html5.shivMethods) {
/* 1191 */               return data.createElem(nodeName);
/* 1192 */             }
/* 1193 */             return createElement(nodeName, ownerDocument, data);
/* 1194 */           };
/* 1195 */ 
/* 1196 */           ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
/* 1197 */                                                           'var n=f.cloneNode(),c=n.createElement;' +
/* 1198 */                                                           'h.shivMethods&&(' +
/* 1199 */                                                           // unroll the `createElement` calls
/* 1200 */                                                           getElements().join().replace(/[\w\-]+/g, function(nodeName) {

/* foundation.min.js */

/* 1201 */             data.createElem(nodeName);
/* 1202 */             data.frag.createElement(nodeName);
/* 1203 */             return 'c("' + nodeName + '")';
/* 1204 */           }) +
/* 1205 */             ');return n}'
/* 1206 */                                                          )(html5, data.frag);
/* 1207 */         }
/* 1208 */ 
/* 1209 */         /*--------------------------------------------------------------------------*/
/* 1210 */ 
/* 1211 */         /**
/* 1212 *|          * Shivs the given document.
/* 1213 *|          * @memberOf html5
/* 1214 *|          * @param {Document} ownerDocument The document to shiv.
/* 1215 *|          * @returns {Document} The shived document.
/* 1216 *|          */
/* 1217 */         function shivDocument(ownerDocument) {
/* 1218 */           if (!ownerDocument) {
/* 1219 */             ownerDocument = document;
/* 1220 */           }
/* 1221 */           var data = getExpandoData(ownerDocument);
/* 1222 */ 
/* 1223 */           if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
/* 1224 */             data.hasCSS = !!addStyleSheet(ownerDocument,
/* 1225 */                                           // corrects block display not defined in IE6/7/8/9
/* 1226 */                                           'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
/* 1227 */                                             // adds styling not present in IE6/7/8/9
/* 1228 */                                             'mark{background:#FF0;color:#000}' +
/* 1229 */                                             // hides non-rendered elements
/* 1230 */                                             'template{display:none}'
/* 1231 */                                          );
/* 1232 */           }
/* 1233 */           if (!supportsUnknownElements) {
/* 1234 */             shivMethods(ownerDocument, data);
/* 1235 */           }
/* 1236 */           return ownerDocument;
/* 1237 */         }
/* 1238 */ 
/* 1239 */         /*--------------------------------------------------------------------------*/
/* 1240 */ 
/* 1241 */         /**
/* 1242 *|          * The `html5` object is exposed so that more elements can be shived and
/* 1243 *|          * existing shiving can be detected on iframes.
/* 1244 *|          * @type Object
/* 1245 *|          * @example
/* 1246 *|          *
/* 1247 *|          * // options can be changed before the script is included
/* 1248 *|          * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
/* 1249 *|          */
/* 1250 */         var html5 = {

/* foundation.min.js */

/* 1251 */ 
/* 1252 */           /**
/* 1253 *|            * An array or space separated string of node names of the elements to shiv.
/* 1254 *|            * @memberOf html5
/* 1255 *|            * @type Array|String
/* 1256 *|            */
/* 1257 */           'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',
/* 1258 */ 
/* 1259 */           /**
/* 1260 *|            * current version of html5shiv
/* 1261 *|            */
/* 1262 */           'version': version,
/* 1263 */ 
/* 1264 */           /**
/* 1265 *|            * A flag to indicate that the HTML5 style sheet should be inserted.
/* 1266 *|            * @memberOf html5
/* 1267 *|            * @type Boolean
/* 1268 *|            */
/* 1269 */           'shivCSS': (options.shivCSS !== false),
/* 1270 */ 
/* 1271 */           /**
/* 1272 *|            * Is equal to true if a browser supports creating unknown/HTML5 elements
/* 1273 *|            * @memberOf html5
/* 1274 *|            * @type boolean
/* 1275 *|            */
/* 1276 */           'supportsUnknownElements': supportsUnknownElements,
/* 1277 */ 
/* 1278 */           /**
/* 1279 *|            * A flag to indicate that the document's `createElement` and `createDocumentFragment`
/* 1280 *|            * methods should be overwritten.
/* 1281 *|            * @memberOf html5
/* 1282 *|            * @type Boolean
/* 1283 *|            */
/* 1284 */           'shivMethods': (options.shivMethods !== false),
/* 1285 */ 
/* 1286 */           /**
/* 1287 *|            * A string to describe the type of `html5` object ("default" or "default print").
/* 1288 *|            * @memberOf html5
/* 1289 *|            * @type String
/* 1290 *|            */
/* 1291 */           'type': 'default',
/* 1292 */ 
/* 1293 */           // shivs the document according to the specified `html5` object options
/* 1294 */           'shivDocument': shivDocument,
/* 1295 */ 
/* 1296 */           //creates a shived element
/* 1297 */           createElement: createElement,
/* 1298 */ 
/* 1299 */           //creates a shived documentFragment
/* 1300 */           createDocumentFragment: createDocumentFragment

/* foundation.min.js */

/* 1301 */         };
/* 1302 */ 
/* 1303 */         /*--------------------------------------------------------------------------*/
/* 1304 */ 
/* 1305 */         // expose html5
/* 1306 */         window.html5 = html5;
/* 1307 */ 
/* 1308 */         // shiv the document
/* 1309 */         shivDocument(document);
/* 1310 */ 
/* 1311 */     }(this, document));
/* 1312 */     /*>>shiv*/
/* 1313 */ 
/* 1314 */     // Assign private properties to the return object with prefix
/* 1315 */     Modernizr._version      = version;
/* 1316 */ 
/* 1317 */     // expose these for the plugin API. Look in the source for how to join() them against your input
/* 1318 */     /*>>prefixes*/
/* 1319 */     Modernizr._prefixes     = prefixes;
/* 1320 */     /*>>prefixes*/
/* 1321 */     /*>>domprefixes*/
/* 1322 */     Modernizr._domPrefixes  = domPrefixes;
/* 1323 */     Modernizr._cssomPrefixes  = cssomPrefixes;
/* 1324 */     /*>>domprefixes*/
/* 1325 */ 
/* 1326 */     /*>>mq*/
/* 1327 */     // Modernizr.mq tests a given media query, live against the current state of the window
/* 1328 */     // A few important notes:
/* 1329 */     //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
/* 1330 */     //   * A max-width or orientation query will be evaluated against the current state, which may change later.
/* 1331 */     //   * You must specify values. Eg. If you are testing support for the min-width media query use:
/* 1332 */     //       Modernizr.mq('(min-width:0)')
/* 1333 */     // usage:
/* 1334 */     // Modernizr.mq('only screen and (max-width:768)')
/* 1335 */     Modernizr.mq            = testMediaQuery;
/* 1336 */     /*>>mq*/
/* 1337 */ 
/* 1338 */     /*>>hasevent*/
/* 1339 */     // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
/* 1340 */     // Modernizr.hasEvent('gesturestart', elem)
/* 1341 */     Modernizr.hasEvent      = isEventSupported;
/* 1342 */     /*>>hasevent*/
/* 1343 */ 
/* 1344 */     /*>>testprop*/
/* 1345 */     // Modernizr.testProp() investigates whether a given style property is recognized
/* 1346 */     // Note that the property names must be provided in the camelCase variant.
/* 1347 */     // Modernizr.testProp('pointerEvents')
/* 1348 */     Modernizr.testProp      = function(prop){
/* 1349 */         return testProps([prop]);
/* 1350 */     };

/* foundation.min.js */

/* 1351 */     /*>>testprop*/
/* 1352 */ 
/* 1353 */     /*>>testallprops*/
/* 1354 */     // Modernizr.testAllProps() investigates whether a given style property,
/* 1355 */     //   or any of its vendor-prefixed variants, is recognized
/* 1356 */     // Note that the property names must be provided in the camelCase variant.
/* 1357 */     // Modernizr.testAllProps('boxSizing')
/* 1358 */     Modernizr.testAllProps  = testPropsAll;
/* 1359 */     /*>>testallprops*/
/* 1360 */ 
/* 1361 */ 
/* 1362 */     /*>>teststyles*/
/* 1363 */     // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
/* 1364 */     // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
/* 1365 */     Modernizr.testStyles    = injectElementWithStyles;
/* 1366 */     /*>>teststyles*/
/* 1367 */ 
/* 1368 */ 
/* 1369 */     /*>>prefixed*/
/* 1370 */     // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
/* 1371 */     // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'
/* 1372 */ 
/* 1373 */     // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
/* 1374 */     // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
/* 1375 */     //
/* 1376 */     //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');
/* 1377 */ 
/* 1378 */     // If you're trying to ascertain which transition end event to bind to, you might do something like...
/* 1379 */     //
/* 1380 */     //     var transEndEventNames = {
/* 1381 */     //       'WebkitTransition' : 'webkitTransitionEnd',
/* 1382 */     //       'MozTransition'    : 'transitionend',
/* 1383 */     //       'OTransition'      : 'oTransitionEnd',
/* 1384 */     //       'msTransition'     : 'MSTransitionEnd',
/* 1385 */     //       'transition'       : 'transitionend'
/* 1386 */     //     },
/* 1387 */     //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];
/* 1388 */ 
/* 1389 */     Modernizr.prefixed      = function(prop, obj, elem){
/* 1390 */       if(!obj) {
/* 1391 */         return testPropsAll(prop, 'pfx');
/* 1392 */       } else {
/* 1393 */         // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
/* 1394 */         return testPropsAll(prop, obj, elem);
/* 1395 */       }
/* 1396 */     };
/* 1397 */     /*>>prefixed*/
/* 1398 */ 
/* 1399 */ 
/* 1400 */     /*>>cssclasses*/

/* foundation.min.js */

/* 1401 */     // Remove "no-js" class from <html> element, if it exists:
/* 1402 */     docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +
/* 1403 */ 
/* 1404 */                             // Add the new classes to the <html> element.
/* 1405 */                             (enableClasses ? ' js ' + classes.join(' ') : '');
/* 1406 */     /*>>cssclasses*/
/* 1407 */ 
/* 1408 */     return Modernizr;
/* 1409 */ 
/* 1410 */ })(this, this.document);
/* 1411 */ 
/* 1412 */ /*foundation script call */
/* 1413 */ jQuery(document).foundation();
