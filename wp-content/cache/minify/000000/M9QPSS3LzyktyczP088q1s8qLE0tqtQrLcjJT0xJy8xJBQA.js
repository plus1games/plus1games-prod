
/* jquery.uploadfile.js */

/* 1   */ /*!
/* 2   *|  * jQuery Upload File Plugin
/* 3   *|  * version: 3.1.0
/* 4   *|  * @requires jQuery v1.5 or later & form plugin
/* 5   *|  * Copyright (c) 2013 Ravishanker Kusuma
/* 6   *|  * http://hayageek.com/
/* 7   *|  */
/* 8   */ (function ($) {
/* 9   */     if ($.fn.ajaxForm == undefined) {
/* 10  */         $.getScript("//malsup.github.io/jquery.form.js");
/* 11  */     }
/* 12  */     var feature = {};
/* 13  */     feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
/* 14  */     feature.formdata = window.FormData !== undefined;
/* 15  */ 
/* 16  */     $.fn.uploadFile = function (options) {
/* 17  */         // This is the easiest way to have default options.
/* 18  */         var s = $.extend({
/* 19  */             // These are the defaults.
/* 20  */             url: "",
/* 21  */             method: "POST",
/* 22  */             enctype: "multipart/form-data",
/* 23  */             formData: null,
/* 24  */             returnType: null,
/* 25  */             allowedTypes: "*",
/* 26  */             fileName: "file",
/* 27  */             formData: {},
/* 28  */             dynamicFormData: function () {
/* 29  */                 return {};
/* 30  */             },
/* 31  */             maxFileSize: -1,
/* 32  */             multiple: false,
/* 33  */             dragDrop: true,
/* 34  */             autoSubmit: true,
/* 35  */             showCancel: true,
/* 36  */             showAbort: true,
/* 37  */             showDone: true,
/* 38  */             showDelete:false,
/* 39  */             showError: true,
/* 40  */             showStatusAfterSuccess: true,
/* 41  */             showStatusAfterError: true,
/* 42  */             showFileCounter:false,
/* 43  */             fileCounterStyle:"). ",
/* 44  */             showProgress:false,
/* 45  */             onSelect:function(files){ return true;},            
/* 46  */             onSubmit: function (files, xhr) {},
/* 47  */             onSuccess: function (files, response, xhr) {},
/* 48  */             onError: function (files, status, message) {},
/* 49  */             deleteCallback: false,
/* 50  */             afterUploadAll: false,

/* jquery.uploadfile.js */

/* 51  */             uploadButtonClass: "ajax-file-upload",
/* 52  */             dragDropStr: "",
/* 53  */             abortStr: "Abort",
/* 54  */             cancelStr: "Cancel",
/* 55  */             deletelStr: "Delete",
/* 56  */             doneStr: "Done",
/* 57  */             multiDragErrorStr: "Multiple File Drag &amp; Drop is not allowed.",
/* 58  */             extErrorStr: "is not allowed. Allowed extensions: ",
/* 59  */             sizeErrorStr: "is not allowed. Allowed Max size: ",
/* 60  */             uploadErrorStr: "Upload is not allowed"
/* 61  */         }, options);
/* 62  */ 
/* 63  */         this.fileCounter = 1;
/* 64  */         this.fCounter = 0; //failed uploads
/* 65  */         this.sCounter = 0; //success uploads
/* 66  */         this.tCounter = 0; //total uploads
/* 67  */         var formGroup = "ajax-file-upload-" + (new Date().getTime());
/* 68  */         this.formGroup = formGroup;
/* 69  */         this.hide();
/* 70  */         this.errorLog = $("<div></div>"); //Writing errors
/* 71  */         this.after(this.errorLog);
/* 72  */         this.responses = [];
/* 73  */         if (!feature.formdata) //check drag drop enabled.
/* 74  */         {
/* 75  */             s.dragDrop = false;
/* 76  */         }
/* 77  */ 
/* 78  */         
/* 79  */         var obj = this;
/* 80  */ 
/* 81  */         var uploadLabel = $('<div>' + $(this).html() + '</div>');
/* 82  */         $(uploadLabel).addClass(s.uploadButtonClass);
/* 83  */ 
/* 84  */         //wait form ajax Form plugin and initialize		
/* 85  */         (function checkAjaxFormLoaded() {
/* 86  */             if ($.fn.ajaxForm) {
/* 87  */ 
/* 88  */                 if (s.dragDrop) {
/* 89  */                     var dragDrop = $('<div class="ajax-upload-dragdrop" style="vertical-align:top;"></div>');
/* 90  */                     $(obj).before(dragDrop);
/* 91  */                     $(dragDrop).after(uploadLabel);
/* 92  */                     $(dragDrop).append($(s.dragDropStr));
/* 93  */                     setDragDropHandlers(obj, s, dragDrop);
/* 94  */ 
/* 95  */                 } else {
/* 96  */                     $(obj).before(uploadLabel);
/* 97  */                 }
/* 98  */ 
/* 99  */                 createCutomInputFile(obj, formGroup, s, uploadLabel);
/* 100 */ 

/* jquery.uploadfile.js */

/* 101 */             } else window.setTimeout(checkAjaxFormLoaded, 10);
/* 102 */         })();
/* 103 */ 
/* 104 */         this.startUpload = function () {
/* 105 */             $("." + this.formGroup).each(function (i, items) {
/* 106 */                 if ($(this).is('form')) $(this).submit();
/* 107 */             });
/* 108 */         }
/* 109 */         this.stopUpload = function () {
/* 110 */             $(".ajax-file-upload-red").each(function (i, items) {
/* 111 */                 if ($(this).hasClass(obj.formGroup)) $(this).click();
/* 112 */             });
/* 113 */         }
/* 114 */ 
/* 115 */         this.getResponses = function () {
/* 116 */             return this.responses;
/* 117 */         }
/* 118 */         var checking = false;
/* 119 */ 
/* 120 */         function checkPendingUploads() {
/* 121 */             if (s.afterUploadAll && !checking) {
/* 122 */                 checking = true;
/* 123 */                 (function checkPending() {
/* 124 */                     if (obj.sCounter != 0 && (obj.sCounter + obj.fCounter == obj.tCounter)) {
/* 125 */                         s.afterUploadAll(obj);
/* 126 */                         checking = false;
/* 127 */                     } else window.setTimeout(checkPending, 100);
/* 128 */                 })();
/* 129 */             }
/* 130 */ 
/* 131 */         }
/* 132 */ 
/* 133 */         function setDragDropHandlers(obj, s, ddObj) {
/* 134 */             ddObj.on('dragenter', function (e) {
/* 135 */                 e.stopPropagation();
/* 136 */                 e.preventDefault();
/* 137 */                 $(this).css({"border": "3px dashed #00b4da"});
/* 138 */             });
/* 139 */             ddObj.on('dragover', function (e) {
/* 140 */                 e.stopPropagation();
/* 141 */                 e.preventDefault();
/* 142 */ 				
/* 143 */             });
/* 144 */             ddObj.on('drop', function (e) {
/* 145 */                 $(this).css({"border": "3px dashed rgba(0,0,0,0.2)"});
/* 146 */                 e.preventDefault();
/* 147 */                 obj.errorLog.html("");
/* 148 */                 var files = e.originalEvent.dataTransfer.files;
/* 149 */                 if (!s.multiple && files.length > 1) {
/* 150 */ 					$(this).css({"border": "3px dashed #e40000"});

/* jquery.uploadfile.js */

/* 151 */                     if (s.showError) $("<div style='color:red;'>" + s.multiDragErrorStr + "</div>").appendTo(obj.errorLog);
/* 152 */                     return;
/* 153 */                 }
/* 154 */                 if(s.onSelect(files) == false)
/* 155 */                 	return;
/* 156 */                 serializeAndUploadFiles(s, obj, files);
/* 157 */             });
/* 158 */ 
/* 159 */             $(document).on('dragenter', function (e) {
/* 160 */                 e.stopPropagation();
/* 161 */                 e.preventDefault();
/* 162 */ 				$(this).css({"border": "3px dashed #00b4da"});
/* 163 */             });
/* 164 */             $(document).on('dragover', function (e) {
/* 165 */                 e.stopPropagation();
/* 166 */                 e.preventDefault();
/* 167 */ 				
/* 168 */             });
/* 169 */             $(document).on('drop', function (e) {
/* 170 */                 e.stopPropagation();
/* 171 */                 e.preventDefault();
/* 172 */                 $(this).css({"border": "3px dashed rgba(0,0,0,0.2)"});
/* 173 */             });
/* 174 */ 
/* 175 */         }
/* 176 */ 
/* 177 */         function getSizeStr(size) {
/* 178 */             var sizeStr = "";
/* 179 */             var sizeKB = size / 1024;
/* 180 */             if (parseInt(sizeKB) > 1024) {
/* 181 */                 var sizeMB = sizeKB / 1024;
/* 182 */                 sizeStr = sizeMB.toFixed(2) + " MB";
/* 183 */             } else {
/* 184 */                 sizeStr = sizeKB.toFixed(2) + " KB";
/* 185 */             }
/* 186 */             return sizeStr;
/* 187 */         }
/* 188 */ 
/* 189 */         function serializeData(extraData) {
/* 190 */             var serialized = [];
/* 191 */             if (jQuery.type(extraData) == "string") {
/* 192 */                 serialized = extraData.split('&');
/* 193 */             } else {
/* 194 */                 serialized = $.param(extraData).split('&');
/* 195 */             }
/* 196 */             var len = serialized.length;
/* 197 */             var result = [];
/* 198 */             var i, part;
/* 199 */             for (i = 0; i < len; i++) {
/* 200 */                 serialized[i] = serialized[i].replace(/\+/g, ' ');

/* jquery.uploadfile.js */

/* 201 */                 part = serialized[i].split('=');
/* 202 */                 result.push([decodeURIComponent(part[0]), decodeURIComponent(part[1])]);
/* 203 */             }
/* 204 */             return result;
/* 205 */         }
/* 206 */ 
/* 207 */         function serializeAndUploadFiles(s, obj, files) {
/* 208 */             for (var i = 0; i < files.length; i++) {
/* 209 */                 if (!isFileTypeAllowed(obj, s, files[i].name)) {
/* 210 */                     if (s.showError) $("<div style='color:red;'><b>" + files[i].name + "</b> " + s.extErrorStr + s.allowedTypes + "</div>").appendTo(obj.errorLog);
/* 211 */                     continue;
/* 212 */                 }
/* 213 */                 if (s.maxFileSize != -1 && files[i].size > s.maxFileSize) {
/* 214 */                     if (s.showError) $("<div style='color:red;'><b>" + files[i].name + "</b> " + s.sizeErrorStr + getSizeStr(s.maxFileSize) + "</div>").appendTo(obj.errorLog);
/* 215 */                     continue;
/* 216 */                 }
/* 217 */                 var ts = s;
/* 218 */                 var fd = new FormData();
/* 219 */                 var fileName = s.fileName.replace("[]", "");
/* 220 */                 fd.append(fileName, files[i]);
/* 221 */                 var extraData = s.formData;
/* 222 */                 if (extraData) {
/* 223 */                     var sData = serializeData(extraData);
/* 224 */                     for (var j = 0; j < sData.length; j++) {
/* 225 */                         if (sData[j]) {
/* 226 */                             fd.append(sData[j][0], sData[j][1]);
/* 227 */                         }
/* 228 */                     }
/* 229 */                 }
/* 230 */                 ts.fileData = fd;
/* 231 */ 
/* 232 */                 var pd = new createProgressDiv(obj, s);
/* 233 */                 var fileNameStr="";
/* 234 */             	if(s.showFileCounter)
/* 235 */             		fileNameStr = obj.fileCounter + s.fileCounterStyle + files[i].name
/* 236 */             	else
/* 237 */             		fileNameStr = files[i].name;
/* 238 */             		
/* 239 */                 pd.filename.html(fileNameStr);
/* 240 */                 var form = $("<form style='display:block; position:absolute;left: 150px;' class='" + obj.formGroup + "' method='" + s.method + "' action='" + s.url + "' enctype='" + s.enctype + "'></form>");
/* 241 */                 form.appendTo('body');
/* 242 */                 var fileArray = [];
/* 243 */                 fileArray.push(files[i].name);
/* 244 */                 ajaxFormSubmit(form, ts, pd, fileArray, obj);
/* 245 */                 obj.fileCounter++;
/* 246 */ 
/* 247 */ 
/* 248 */             }
/* 249 */         }
/* 250 */ 

/* jquery.uploadfile.js */

/* 251 */         function isFileTypeAllowed(obj, s, fileName) {
/* 252 */             var fileExtensions = s.allowedTypes.toLowerCase().split(",");
/* 253 */             var ext = fileName.split('.').pop().toLowerCase();
/* 254 */             if (s.allowedTypes != "*" && jQuery.inArray(ext, fileExtensions) < 0) {
/* 255 */                 return false;
/* 256 */             }
/* 257 */             return true;
/* 258 */         }
/* 259 */ 
/* 260 */         function createCutomInputFile(obj, group, s, uploadLabel) {
/* 261 */ 
/* 262 */             var fileUploadId = "ajax-upload-id-" + (new Date().getTime());
/* 263 */ 
/* 264 */             var form = $("<form method='" + s.method + "' action='" + s.url + "' enctype='" + s.enctype + "'></form>");
/* 265 */             var fileInputStr = "<input type='file' id='" + fileUploadId + "' name='" + s.fileName + "'/>";
/* 266 */             if (s.multiple) {
/* 267 */                 if (s.fileName.indexOf("[]") != s.fileName.length - 2) // if it does not endwith
/* 268 */                 {
/* 269 */                     s.fileName += "[]";
/* 270 */                 }
/* 271 */                 fileInputStr = "<input type='file' id='" + fileUploadId + "' name='" + s.fileName + "' multiple/>";
/* 272 */             }
/* 273 */             var fileInput = $(fileInputStr).appendTo(form);
/* 274 */ 
/* 275 */             fileInput.change(function () {
/* 276 */ 
/* 277 */                 obj.errorLog.html("");
/* 278 */                 var fileExtensions = s.allowedTypes.toLowerCase().split(",");
/* 279 */                 var fileArray = [];
/* 280 */                 if (this.files) //support reading files
/* 281 */                 {
/* 282 */                     for (i = 0; i < this.files.length; i++) 
/* 283 */                     {
/* 284 */                         fileArray.push(this.files[i].name);
/* 285 */                     }
/* 286 */                    
/* 287 */                     if(s.onSelect(this.files) == false)
/* 288 */ 	                	return;
/* 289 */                 } else {
/* 290 */                     var filenameStr = $(this).val();
/* 291 */                     var flist = [];
/* 292 */                     fileArray.push(filenameStr);
/* 293 */                     if (!isFileTypeAllowed(obj, s, filenameStr)) {
/* 294 */                         if (s.showError) $("<div style='color:red;'><b>" + filenameStr + "</b> " + s.extErrorStr + s.allowedTypes + "</div>").appendTo(obj.errorLog);
/* 295 */                         return;
/* 296 */                     }
/* 297 */                     //fallback for browser without FileAPI
/* 298 */                     flist.push({name:filenameStr,size:'NA'});
/* 299 */                     if(s.onSelect(flist) == false)
/* 300 */ 	                	return;

/* jquery.uploadfile.js */

/* 301 */ 
/* 302 */                 }
/* 303 */                 uploadLabel.unbind("click");
/* 304 */                 form.hide();
/* 305 */                 createCutomInputFile(obj, group, s, uploadLabel);
/* 306 */ 
/* 307 */                 form.addClass(group);
/* 308 */                 if (feature.fileapi && feature.formdata) //use HTML5 support and split file submission
/* 309 */                 {
/* 310 */                     form.removeClass(group); //Stop Submitting when.
/* 311 */                     var files = this.files;
/* 312 */                     serializeAndUploadFiles(s, obj, files);
/* 313 */                 } else {
/* 314 */                     var fileList = "";
/* 315 */                     for (var i = 0; i < fileArray.length; i++) {
/* 316 */ 		            	if(s.showFileCounter)
/* 317 */         		    		fileList += obj.fileCounter + s.fileCounterStyle + fileArray[i]+"<br>";
/* 318 */             			else
/* 319 */ 		            		fileList += fileArray[i]+"<br>";;
/* 320 */                         obj.fileCounter++;
/* 321 */                     }
/* 322 */                     var pd = new createProgressDiv(obj, s);
/* 323 */                     pd.filename.html(fileList);
/* 324 */                     ajaxFormSubmit(form, s, pd, fileArray, obj);
/* 325 */                 }
/* 326 */ 
/* 327 */ 
/* 328 */ 
/* 329 */             });
/* 330 */             
/* 331 */ 	         form.css({'margin':0,'padding':0});
/* 332 */             var uwidth=$(uploadLabel).width()+10;
/* 333 */             if(uwidth == 10)
/* 334 */             	uwidth =120;
/* 335 */             	
/* 336 */             var uheight=uploadLabel.height()+10;
/* 337 */             if(uheight == 10)
/* 338 */             	uheight = 35;
/* 339 */ 
/* 340 */ 			uploadLabel.css({position: 'relative',overflow:'hidden',cursor:'default'});
/* 341 */ 			fileInput.css({position: 'absolute','cursor':'pointer',  
/* 342 */ 							'top': '0px',
/* 343 */ 							'width': uwidth,  
/* 344 */ 							'height':uheight,
/* 345 */ 							'left': '0px',
/* 346 */ 							'z-index': '100',
/* 347 */ 							'opacity': '0.0',
/* 348 */ 							'filter':'alpha(opacity=0)',
/* 349 */ 							'-ms-filter':"alpha(opacity=0)",
/* 350 */ 							'-khtml-opacity':'0.0',

/* jquery.uploadfile.js */

/* 351 */ 							'-moz-opacity':'0.0'
/* 352 */ 							});
/* 353 */ 	         form.appendTo(uploadLabel);
/* 354 */ 
/* 355 */             //dont hide it, but move it to 
/* 356 */            /* form.css({
/* 357 *|                 margin: 0,
/* 358 *|                 padding: 0,
/* 359 *|                 display: 'block',
/* 360 *|                 position: 'absolute',
/* 361 *|                 left: '50px'
/* 362 *|             });
/* 363 *|            if (navigator.appVersion.indexOf("MSIE ") != -1) //IE Browser
/* 364 *|             {
/* 365 *|                 uploadLabel.attr('for', fileUploadId);
/* 366 *|             } else {
/* 367 *|                 uploadLabel.click(function () {
/* 368 *|                     fileInput.click();
/* 369 *|                 });
/* 370 *|             }*/
/* 371 */ 
/* 372 */ 
/* 373 */         }
/* 374 */ 
/* 375 */ 
/* 376 */         function createProgressDiv(obj, s) {
/* 377 */ 			$('.ajax-file-upload-statusbar').remove();
/* 378 */             this.statusbar = $("<div class='ajax-file-upload-statusbar'></div>");
/* 379 */             this.filename = $("<div class='ajax-file-upload-filename'></div>").appendTo(this.statusbar);
/* 380 */             this.progressDiv = $("<div class='ajax-file-upload-progress'>").appendTo(this.statusbar).hide();
/* 381 */             this.progressbar = $("<div class='ajax-file-upload-bar " + obj.formGroup + "'></div>").appendTo(this.progressDiv);
/* 382 */             this.abort = $("<div class='ajax-file-upload-red " + obj.formGroup + "'>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
/* 383 */             this.cancel = $("<div class='ajax-file-upload-red'>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
/* 384 */             this.done = $("<div class='ajax-file-upload-green'>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
/* 385 */             this.del = $("<div class='ajax-file-upload-red'>" + s.deletelStr + "</div>").appendTo(this.statusbar).hide();
/* 386 */             obj.errorLog.after(this.statusbar);
/* 387 */             return this;
/* 388 */         }
/* 389 */ 
/* 390 */ 
/* 391 */         function ajaxFormSubmit(form, s, pd, fileArray, obj) {
/* 392 */             var currentXHR = null;
/* 393 */             var options = {
/* 394 */                 cache: false,
/* 395 */                 contentType: false,
/* 396 */                 processData: false,
/* 397 */                 forceSync: false,
/* 398 */                 data: s.formData,
/* 399 */                 formData: s.fileData,
/* 400 */                 dataType: s.returnType,

/* jquery.uploadfile.js */

/* 401 */                 beforeSubmit: function (formData, $form, options) {
/* 402 */                     if (s.onSubmit.call(this, fileArray) != false) {
/* 403 */                         var dynData = s.dynamicFormData();
/* 404 */                         if (dynData) {
/* 405 */                             var sData = serializeData(dynData);
/* 406 */                             if (sData) {
/* 407 */                                 for (var j = 0; j < sData.length; j++) {
/* 408 */                                     if (sData[j]) {
/* 409 */                                         if (s.fileData != undefined) options.formData.append(sData[j][0], sData[j][1]);
/* 410 */                                         else options.data[sData[j][0]] = sData[j][1];
/* 411 */                                     }
/* 412 */                                 }
/* 413 */                             }
/* 414 */                         }
/* 415 */                         obj.tCounter += fileArray.length;
/* 416 */                         //window.setTimeout(checkPendingUploads, 1000); //not so critical
/* 417 */                         checkPendingUploads();
/* 418 */                         return true;
/* 419 */                     }
/* 420 */                     pd.statusbar.append("<div style='color:red;'>" + s.uploadErrorStr + "</div>");
/* 421 */                     pd.cancel.show()
/* 422 */                     form.remove();
/* 423 */                     pd.cancel.click(function () {
/* 424 */                         pd.statusbar.remove();
/* 425 */                     });
/* 426 */                     return false;
/* 427 */                 },
/* 428 */                 beforeSend: function (xhr, o) {
/* 429 */ 
/* 430 */                     pd.progressDiv.show();
/* 431 */                     pd.cancel.hide();
/* 432 */                     pd.done.hide();
/* 433 */                     if (s.showAbort) {
/* 434 */                         pd.abort.show();
/* 435 */                         pd.abort.click(function () {
/* 436 */                             xhr.abort();
/* 437 */                         });
/* 438 */                     }
/* 439 */                     if (!feature.formdata) //For iframe based push
/* 440 */                     {
/* 441 */                         pd.progressbar.width('5%');
/* 442 */                     } else pd.progressbar.width('1%'); //Fix for small files
/* 443 */                 },
/* 444 */                 uploadProgress: function (event, position, total, percentComplete) {
/* 445 */ 		            //Fix for smaller file uploads in MAC
/* 446 */                 	if(percentComplete > 98) percentComplete =98; 
/* 447 */                 	
/* 448 */                     var percentVal = percentComplete + '%';
/* 449 */                     if (percentComplete > 1) pd.progressbar.width(percentVal)
/* 450 */                     if(s.showProgress) 

/* jquery.uploadfile.js */

/* 451 */                     {
/* 452 */                     	pd.progressbar.html(percentVal);
/* 453 */                     	pd.progressbar.css('text-align', 'center');
/* 454 */                     }
/* 455 */ 	                
/* 456 */                 },
/* 457 */                 success: function (data, message, xhr) {
/* 458 */                     obj.responses.push(data);
/* 459 */                     pd.progressbar.width('100%')
/* 460 */                     if(s.showProgress)
/* 461 */                     { 
/* 462 */                     	pd.progressbar.html('100%');
/* 463 */                     	pd.progressbar.css('text-align', 'center');
/* 464 */                     }	
/* 465 */ 	                
/* 466 */                     pd.abort.hide();
/* 467 */                     s.onSuccess.call(this, fileArray, data, xhr);
/* 468 */                     if (s.showStatusAfterSuccess) {
/* 469 */                         if (s.showDone) {
/* 470 */                             pd.done.show();
/* 471 */                             pd.done.click(function () {
/* 472 */                                 pd.statusbar.hide("slow");
/* 473 */                                 pd.statusbar.remove();
/* 474 */                             });
/* 475 */                         } else {
/* 476 */                             pd.done.hide();
/* 477 */                         }
/* 478 */                         if(s.showDelete)
/* 479 */                         {
/* 480 */                         	pd.del.show();
/* 481 */                         	 pd.del.click(function () {
/* 482 */                         		if(s.deleteCallback) s.deleteCallback.call(this, data,pd);
/* 483 */                             });
/* 484 */                         }
/* 485 */                         else
/* 486 */                         {
/* 487 */ 	                        pd.del.hide();
/* 488 */ 	                    }
/* 489 */                     } else {
/* 490 */                         pd.statusbar.hide("slow");
/* 491 */                         pd.statusbar.remove();
/* 492 */ 
/* 493 */                     }
/* 494 */                     form.remove();
/* 495 */                     obj.sCounter += fileArray.length;
/* 496 */                 },
/* 497 */                 error: function (xhr, status, errMsg) {
/* 498 */                     pd.abort.hide();
/* 499 */                     if (xhr.statusText == "abort") //we aborted it
/* 500 */                     {

/* jquery.uploadfile.js */

/* 501 */                         pd.statusbar.hide("slow");
/* 502 */                     } else {
/* 503 */                         s.onError.call(this, fileArray, status, errMsg);
/* 504 */                         if (s.showStatusAfterError) {
/* 505 */                             pd.progressDiv.hide();
/* 506 */                             pd.statusbar.append("<span style='color:red;'>ERROR: " + errMsg + "</span>");
/* 507 */                         } else {
/* 508 */                             pd.statusbar.hide();
/* 509 */                             pd.statusbar.remove();
/* 510 */                         }
/* 511 */                     }
/* 512 */ 
/* 513 */                     form.remove();
/* 514 */                     obj.fCounter += fileArray.length;
/* 515 */ 
/* 516 */                 }
/* 517 */             };
/* 518 */             if (s.autoSubmit) {
/* 519 */                 form.ajaxSubmit(options);
/* 520 */             } else {
/* 521 */                 if (s.showCancel) {
/* 522 */                     pd.cancel.show();
/* 523 */                     pd.cancel.click(function () {
/* 524 */                         form.remove();
/* 525 */                         pd.statusbar.remove();
/* 526 */                     });
/* 527 */                 }
/* 528 */                 form.ajaxForm(options);
/* 529 */ 
/* 530 */             }
/* 531 */ 
/* 532 */         }
/* 533 */         return this;
/* 534 */     }
/* 535 */ }(jQuery));
