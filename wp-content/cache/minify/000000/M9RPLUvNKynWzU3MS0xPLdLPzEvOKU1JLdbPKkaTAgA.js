
/* events-manager.js */

/* 1    */ jQuery(document).ready( function($){
/* 2    */ 	var load_ui_css = false; //load jquery ui css?
/* 3    */ 	/* Time Entry */
/* 4    */ 	$('#start-time').each(function(i, el){
/* 5    */ 		$(el).addClass('em-time-input em-time-start').next('#end-time').addClass('em-time-input em-time-end').parent().addClass('em-time-range');
/* 6    */ 	});
/* 7    */ 	if( $(".em-time-input").length > 0 ){
/* 8    */ 		em_setup_timepicker('body');
/* 9    */ 	}
/* 10   */ 	/* Calendar AJAX */
/* 11   */ 	$('.em-calendar-wrapper a').unbind("click");
/* 12   */ 	$('.em-calendar-wrapper a').undelegate("click");
/* 13   */ 	$('.em-calendar-wrapper').delegate('a.em-calnav, a.em-calnav', 'click', function(e){
/* 14   */ 		e.preventDefault();
/* 15   */ 		$(this).closest('.em-calendar-wrapper').prepend('<div class="loading" id="em-loading"></div>');
/* 16   */ 		var url = em_ajaxify($(this).attr('href'));
/* 17   */ 		$(this).closest('.em-calendar-wrapper').load(url, function(){$(this).trigger('em_calendar_load');});
/* 18   */ 	} );
/* 19   */ 
/* 20   */ 	//Events Search
/* 21   */ 	$(document).delegate('.em-toggle', 'click change', function(e){
/* 22   */ 		e.preventDefault();
/* 23   */ 		//show or hide advanced tickets, hidden by default
/* 24   */ 		var el = $(this);
/* 25   */ 		var rel = el.attr('rel').split(':');
/* 26   */ 		if( el.hasClass('show-search') ){
/* 27   */ 			if( rel.length > 1 ){ el.closest(rel[1]).find(rel[0]).slideUp(); }
/* 28   */ 			else{ $(rel[0]).slideUp(); }
/* 29   */ 			el.find('.show, .show-advanced').show();
/* 30   */ 			el.find('.hide, .hide-advanced').hide();
/* 31   */ 			el.removeClass('show-search');
/* 32   */ 		}else{
/* 33   */ 			if( rel.length > 1 ){ el.closest(rel[1]).find(rel[0]).slideDown(); }
/* 34   */ 			else{ $(rel[0]).slideDown(); }
/* 35   */ 			el.find('.show, .show-advanced').hide();
/* 36   */ 			el.find('.hide, .hide-advanced').show();
/* 37   */ 			el.addClass('show-search');
/* 38   */ 		}
/* 39   */ 		
/* 40   */ 	});
/* 41   */ 	if( EM.search_term_placeholder ){
/* 42   */ 		if( 'placeholder' in document.createElement('input') ){
/* 43   */ 			$('input.em-events-search-text, input.em-search-text').attr('placeholder', EM.search_term_placeholder);
/* 44   */ 		}else{
/* 45   */ 			$('input.em-events-search-text, input.em-search-text').blur(function(){
/* 46   */ 				if( this.value=='' ) this.value = EM.search_term_placeholder;
/* 47   */ 			}).focus(function(){
/* 48   */ 				if( this.value == EM.search_term_placeholder ) this.value='';
/* 49   */ 			}).trigger('blur');
/* 50   */ 		}

/* events-manager.js */

/* 51   */ 	}
/* 52   */ 	$('.em-search-form select[name=country]').change( function(){
/* 53   */ 		var el = $(this);
/* 54   */ 		$('.em-search select[name=state]').html('<option value="">'+EM.txt_loading+'</option>');
/* 55   */ 		$('.em-search select[name=region]').html('<option value="">'+EM.txt_loading+'</option>');
/* 56   */ 		$('.em-search select[name=town]').html('<option value="">'+EM.txt_loading+'</option>');
/* 57   */ 		if( el.val() != '' ){
/* 58   */ 			el.closest('.em-search-location').find('.em-search-location-meta').slideDown();
/* 59   */ 			var data = {
/* 60   */ 				action : 'search_states',
/* 61   */ 				country : el.val(),
/* 62   */ 				return_html : true
/* 63   */ 			};
/* 64   */ 			$('.em-search select[name=state]').load( EM.ajaxurl, data );
/* 65   */ 			data.action = 'search_regions';
/* 66   */ 			$('.em-search select[name=region]').load( EM.ajaxurl, data );
/* 67   */ 			data.action = 'search_towns';
/* 68   */ 			$('.em-search select[name=town]').load( EM.ajaxurl, data );
/* 69   */ 		}else{
/* 70   */ 			el.closest('.em-search-location').find('.em-search-location-meta').slideUp();
/* 71   */ 		}
/* 72   */ 	});
/* 73   */ 
/* 74   */ 	$('.em-search-form select[name=region]').change( function(){
/* 75   */ 		$('.em-search select[name=state]').html('<option value="">'+EM.txt_loading+'</option>');
/* 76   */ 		$('.em-search select[name=town]').html('<option value="">'+EM.txt_loading+'</option>');
/* 77   */ 		var data = {
/* 78   */ 			action : 'search_states',
/* 79   */ 			region : $(this).val(),
/* 80   */ 			country : $('.em-search-form select[name=country]').val(),
/* 81   */ 			return_html : true
/* 82   */ 		};
/* 83   */ 		$('.em-search select[name=state]').load( EM.ajaxurl, data );
/* 84   */ 		data.action = 'search_towns';
/* 85   */ 		$('.em-search select[name=town]').load( EM.ajaxurl, data );
/* 86   */ 	});
/* 87   */ 
/* 88   */ 	$('.em-search-form select[name=state]').change( function(){
/* 89   */ 		$('.em-search select[name=town]').html('<option value="">'+EM.txt_loading+'</option>');
/* 90   */ 		var data = {
/* 91   */ 			action : 'search_towns',
/* 92   */ 			state : $(this).val(),
/* 93   */ 			region : $('.em-search-form select[name=region]').val(),
/* 94   */ 			country : $('.em-search-form select[name=country]').val(),
/* 95   */ 			return_html : true
/* 96   */ 		};
/* 97   */ 		$('.em-search select[name=town]').load( EM.ajaxurl, data );
/* 98   */ 	});
/* 99   */ 	
/* 100  */ 	//in order for this to work, you need the above classes to be present in your templates

/* events-manager.js */

/* 101  */ 	$(document).delegate('.em-search-form, .em-events-search-form', 'submit', function(e){
/* 102  */ 		var form = $(this);
/* 103  */     	if( this.em_search && this.em_search.value == EM.txt_search){ this.em_search.value = ''; }
/* 104  */     	var results_wrapper = form.closest('.em-search-wrapper').find('.em-search-ajax');
/* 105  */     	if( results_wrapper.length == 0 ) results_wrapper = $('.em-search-ajax');
/* 106  */     	if( results_wrapper.length > 0 ){
/* 107  */     		results_wrapper.append('<div class="loading" id="em-loading"></div>');
/* 108  */     		var submitButton = form.find('.em-search-submit');
/* 109  */     		submitButton.data('buttonText', submitButton.val()).val(EM.txt_searching);
/* 110  */     		var img = submitButton.children('img');
/* 111  */     		if( img.length > 0 ) img.attr('src', img.attr('src').replace('search-mag.png', 'search-loading.gif'));
/* 112  */     		var vars = form.serialize();
/* 113  */     		$.ajax( EM.ajaxurl, {
/* 114  */ 				type : 'POST',
/* 115  */ 	    		dataType : 'html',
/* 116  */ 	    		data : vars,
/* 117  */ 			    success : function(responseText){
/* 118  */ 			    	submitButton.val(submitButton.data('buttonText'));
/* 119  */ 			    	if( img.length > 0 ) img.attr('src', img.attr('src').replace('search-loading.gif', 'search-mag.png'));
/* 120  */ 		    		results_wrapper.replaceWith(responseText);
/* 121  */ 		        	if( form.find('input[name=em_search]').val() == '' ){ form.find('input[name=em_search]').val(EM.txt_search); }
/* 122  */ 		        	//reload results_wrapper
/* 123  */ 		        	results_wrapper = form.closest('.em-search-wrapper').find('.em-search-ajax');
/* 124  */ 		        	if( results_wrapper.length == 0 ) results_wrapper = $('.em-search-ajax');
/* 125  */ 			    	jQuery(document).triggerHandler('em_search_ajax', [vars, results_wrapper, e]); //ajax has loaded new results
/* 126  */ 			    }
/* 127  */ 	    	});
/* 128  */     		e.preventDefault();
/* 129  */ 			return false;
/* 130  */     	}
/* 131  */ 	});
/* 132  */ 	if( $('.em-search-ajax').length > 0 ){
/* 133  */ 		$(document).delegate('.em-search-ajax a.page-numbers', 'click', function(e){
/* 134  */ 			var a = $(this);
/* 135  */ 			var data = a.closest('.em-pagination').attr('data-em-ajax');
/* 136  */ 			var wrapper = a.closest('.em-search-ajax');
/* 137  */ 			var wrapper_parent = wrapper.parent();
/* 138  */ 		    var qvars = a.attr('href').split('?');
/* 139  */ 		    var vars = qvars[1];
/* 140  */ 		    //add data-em-ajax att if it exists
/* 141  */ 		    if( data != '' ){
/* 142  */ 		    	vars = vars != '' ? vars+'&'+data : data;
/* 143  */ 		    }
/* 144  */ 		    wrapper.append('<div class="loading" id="em-loading"></div>');
/* 145  */ 		    $.ajax( EM.ajaxurl, {
/* 146  */ 				type : 'POST',
/* 147  */ 	    		dataType : 'html',
/* 148  */ 	    		data : vars,
/* 149  */ 			    success : function(responseText) {
/* 150  */ 			    	wrapper.replaceWith(responseText);

/* events-manager.js */

/* 151  */ 			    	wrapper = wrapper_parent.find('.em-search-ajax');
/* 152  */ 			    	jQuery(document).triggerHandler('em_search_ajax', [vars, wrapper, e]); //ajax has loaded new results
/* 153  */ 			    }
/* 154  */ 	    	});
/* 155  */ 			e.preventDefault();
/* 156  */ 			return false;
/* 157  */ 		});
/* 158  */ 	}
/* 159  */ 		
/* 160  */ 	/*
/* 161  *| 	 * ADMIN AREA AND PUBLIC FORMS (Still polishing this section up, note that form ids and classes may change accordingly)
/* 162  *| 	 */
/* 163  */ 	//Events List
/* 164  */ 		//Approve/Reject Links
/* 165  */ 		$('.events-table').on('click', '.em-event-delete', function(){
/* 166  */ 			if( !confirm("Are you sure you want to delete?") ){ return false; }
/* 167  */ 			window.location.href = this.href;
/* 168  */ 		});
/* 169  */ 	//Forms
/* 170  */ 	$('#event-form #event-image-delete, #location-form #location-image-delete').on('click', function(){
/* 171  */ 		var el = $(this);
/* 172  */ 		if( el.is(':checked') ){
/* 173  */ 			el.closest('.event-form-image, .location-form-image').find('#event-image-img, #location-image-img').hide();
/* 174  */ 		}else{
/* 175  */ 			el.closest('.event-form-image, .location-form-image').find('#event-image-img, #location-image-img').show();
/* 176  */ 		}
/* 177  */ 	});
/* 178  */ 	//Tickets & Bookings
/* 179  */ 	if( $("#em-tickets-form").length > 0 ){
/* 180  */ 		//Enable/Disable Bookings
/* 181  */ 		$('#event-rsvp').click( function(event){
/* 182  */ 			if( !this.checked ){
/* 183  */ 				confirmation = confirm(EM.disable_bookings_warning);
/* 184  */ 				if( confirmation == false ){
/* 185  */ 					event.preventDefault();
/* 186  */ 				}else{
/* 187  */ 					$('#event-rsvp-options').hide();
/* 188  */ 				}
/* 189  */ 			}else{
/* 190  */ 				$('#event-rsvp-options').fadeIn();
/* 191  */ 			}
/* 192  */ 		});
/* 193  */ 		if($('input#event-rsvp').is(":checked")) {
/* 194  */ 			$("div#rsvp-data").fadeIn();
/* 195  */ 		} else {
/* 196  */ 			$("div#rsvp-data").hide();
/* 197  */ 		}
/* 198  */ 		//Ticket(s) UI
/* 199  */ 		var reset_ticket_forms = function(){
/* 200  */ 			$('#em-tickets-form table tbody tr.em-tickets-row').show();

/* events-manager.js */

/* 201  */ 			$('#em-tickets-form table tbody tr.em-tickets-row-form').hide();
/* 202  */ 		};
/* 203  */ 		//recurrences and cut-off logic for ticket availability
/* 204  */ 		if( $('#em-recurrence-checkbox').length > 0 ){
/* 205  */ 			$('#em-recurrence-checkbox').change(function(){
/* 206  */ 				if( $('#em-recurrence-checkbox').is(':checked') ){
/* 207  */ 					$('#em-tickets-form .ticket-dates-from-recurring, #em-tickets-form .ticket-dates-to-recurring, #event-rsvp-options .em-booking-date-recurring').show();
/* 208  */ 					$('#em-tickets-form .ticket-dates-from-normal, #em-tickets-form .ticket-dates-to-normal, #event-rsvp-options .em-booking-date-normal, #em-tickets-form .hidden').hide();
/* 209  */ 				}else{
/* 210  */ 					$('#em-tickets-form .ticket-dates-from-normal, #em-tickets-form .ticket-dates-to-normal, #event-rsvp-options .em-booking-date-normal').show();
/* 211  */ 					$('#em-tickets-form .ticket-dates-from-recurring, #em-tickets-form .ticket-dates-to-recurring, #event-rsvp-options .em-booking-date-recurring, #em-tickets-form .hidden').hide();
/* 212  */ 				}
/* 213  */ 			}).trigger('change');
/* 214  */ 		}else if( $('#em-form-recurrence').length > 0 ){
/* 215  */ 			$('#em-tickets-form .ticket-dates-from-recurring, #em-tickets-form .ticket-dates-to-recurring, #event-rsvp-options .em-booking-date-recurring').show();
/* 216  */ 			$('#em-tickets-form .ticket-dates-from-normal, #em-tickets-form .ticket-dates-to-normal, #event-rsvp-options .em-booking-date-normal, #em-tickets-form .hidden').hide();
/* 217  */ 		}else{
/* 218  */ 			$('#em-tickets-form .ticket-dates-from-recurring, #em-tickets-form .ticket-dates-to-recurring, #event-rsvp-options .em-booking-date-recurring, #em-tickets-form .hidden').hide();
/* 219  */ 		}
/* 220  */ 		//Add a new ticket
/* 221  */ 		$("#em-tickets-add").click(function(e){ 
/* 222  */ 			e.preventDefault();
/* 223  */ 			reset_ticket_forms();
/* 224  */ 			//create copy of template slot, insert so ready for population
/* 225  */ 			var tickets = $('#em-tickets-form table tbody');
/* 226  */ 			var rowNo = tickets.length+1;
/* 227  */ 			var slot = tickets.first().clone(true).attr('id','em-ticket-'+ rowNo).appendTo($('#em-tickets-form table'));
/* 228  */ 			//change the index of the form element names
/* 229  */ 			slot.find('*[name]').each( function(index,el){
/* 230  */ 				el = $(el);
/* 231  */ 				el.attr('name', el.attr('name').replace('em_tickets[0]','em_tickets['+rowNo+']'));
/* 232  */ 			});
/* 233  */ 			//show ticket and switch to editor
/* 234  */ 			slot.show().find('.ticket-actions-edit').trigger('click');
/* 235  */ 			//refresh datepicker and values
/* 236  */ 			slot.find('.em-date-input-loc').datepicker('destroy').removeAttr('id'); //clear all datepickers
/* 237  */ 			slot.find('.em-time-input').unbind().each(function(index, el){ this.timePicker = false; }); //clear all timepickers - consequently, also other click/blur/change events, recreate the further down
/* 238  */ 			em_setup_datepicker(slot);
/* 239  */ 			em_setup_timepicker(slot);
/* 240  */ 		    $('html, body').animate({ scrollTop: slot.offset().top - 30 }); //sends user to form
/* 241  */ 		});
/* 242  */ 		//Edit a Ticket
/* 243  */ 		$(document).delegate('.ticket-actions-edit', 'click', function(e){
/* 244  */ 			e.preventDefault();
/* 245  */ 			reset_ticket_forms();
/* 246  */ 			var tbody = $(this).closest('tbody');
/* 247  */ 			tbody.find('tr.em-tickets-row').hide();
/* 248  */ 			tbody.find('tr.em-tickets-row-form').fadeIn();
/* 249  */ 			return false;
/* 250  */ 		});

/* events-manager.js */

/* 251  */ 		$(document).delegate('.ticket-actions-edited', 'click', function(e){
/* 252  */ 			e.preventDefault();
/* 253  */ 			var tbody = $(this).closest('tbody');
/* 254  */ 			var rowNo = tbody.attr('id').replace('em-ticket-','');
/* 255  */ 			tbody.find('.em-tickets-row').fadeIn();
/* 256  */ 			tbody.find('.em-tickets-row-form').hide();
/* 257  */ 			tbody.find('*[name]').each(function(index,el){
/* 258  */ 				el = $(el);
/* 259  */ 				if( el.attr('name') == 'ticket_start_pub'){
/* 260  */ 					tbody.find('span.ticket_start').text(el.attr('value'));
/* 261  */ 				}else if( el.attr('name') == 'ticket_end_pub' ){
/* 262  */ 					tbody.find('span.ticket_end').text(el.attr('value'));
/* 263  */ 				}else if( el.attr('name') == 'em_tickets['+rowNo+'][ticket_type]' ){
/* 264  */ 					if( el.find(':selected').val() == 'members' ){
/* 265  */ 						tbody.find('span.ticket_name').prepend('* ');
/* 266  */ 					}
/* 267  */ 				}else if( el.attr('name') == 'em_tickets['+rowNo+'][ticket_start_recurring_days]' ){
/* 268  */ 					var text = tbody.find('select.ticket-dates-from-recurring-when').val() == 'before' ? '-'+el.attr('value'):el.attr('value');
/* 269  */ 					if( el.attr('value') != '' ){
/* 270  */ 						tbody.find('span.ticket_start_recurring_days').text(text);
/* 271  */ 						tbody.find('span.ticket_start_recurring_days_text, span.ticket_start_time').removeClass('hidden').show();
/* 272  */ 					}else{
/* 273  */ 						tbody.find('span.ticket_start_recurring_days').text(' - ');
/* 274  */ 						tbody.find('span.ticket_start_recurring_days_text, span.ticket_start_time').removeClass('hidden').hide();
/* 275  */ 					}
/* 276  */ 				}else if( el.attr('name') == 'em_tickets['+rowNo+'][ticket_end_recurring_days]' ){
/* 277  */ 					var text = tbody.find('select.ticket-dates-to-recurring-when').val() == 'before' ? '-'+el.attr('value'):el.attr('value');
/* 278  */ 					if( el.attr('value') != '' ){
/* 279  */ 						tbody.find('span.ticket_end_recurring_days').text(text);
/* 280  */ 						tbody.find('span.ticket_end_recurring_days_text, span.ticket_end_time').removeClass('hidden').show();
/* 281  */ 					}else{
/* 282  */ 						tbody.find('span.ticket_end_recurring_days').text(' - ');
/* 283  */ 						tbody.find('span.ticket_end_recurring_days_text, span.ticket_end_time').removeClass('hidden').hide();
/* 284  */ 					}
/* 285  */ 				}else{
/* 286  */ 					tbody.find('.'+el.attr('name').replace('em_tickets['+rowNo+'][','').replace(']','').replace('[]','')).text(el.attr('value'));
/* 287  */ 				}
/* 288  */ 			});
/* 289  */ 			//allow for others to hook into this
/* 290  */ 			$(document).triggerHandler('em_maps_tickets_edit', [tbody, rowNo, true]);
/* 291  */ 		    $('html, body').animate({ scrollTop: tbody.parent().offset().top - 30 }); //sends user back to top of form
/* 292  */ 			return false;
/* 293  */ 		});
/* 294  */ 		$(document).delegate('.em-ticket-form select.ticket_type','change', function(e){
/* 295  */ 			//check if ticket is for all users or members, if members, show roles to limit the ticket to
/* 296  */ 			var el = $(this);
/* 297  */ 			if( el.find('option:selected').val() == 'members' ){
/* 298  */ 				el.closest('.em-ticket-form').find('.ticket-roles').fadeIn();
/* 299  */ 			}else{
/* 300  */ 				el.closest('.em-ticket-form').find('.ticket-roles').hide();

/* events-manager.js */

/* 301  */ 			}
/* 302  */ 		});
/* 303  */ 		$(document).delegate('.em-ticket-form .ticket-options-advanced','click', function(e){
/* 304  */ 			//show or hide advanced tickets, hidden by default
/* 305  */ 			e.preventDefault();
/* 306  */ 			var el = $(this);
/* 307  */ 			if( el.hasClass('show') ){
/* 308  */ 				el.closest('.em-ticket-form').find('.em-ticket-form-advanced').fadeIn();
/* 309  */ 				el.find('.show,.show-advanced').hide();
/* 310  */ 				el.find('.hide,.hide-advanced').show();
/* 311  */ 			}else{
/* 312  */ 				el.closest('.em-ticket-form').find('.em-ticket-form-advanced').hide();
/* 313  */ 				el.find('.show,.show-advanced').show();
/* 314  */ 				el.find('.hide,.hide-advanced').hide();
/* 315  */ 			}
/* 316  */ 			el.toggleClass('show');
/* 317  */ 		});
/* 318  */ 		$('.em-ticket-form').each( function(){
/* 319  */ 			//check whether to show advanced options or not by default for each ticket
/* 320  */ 			var show_advanced = false;
/* 321  */ 			var el = $(this); 
/* 322  */ 			el.find('.em-ticket-form-advanced input[type="text"]').each(function(){ if(this.value != '') show_advanced = true; });
/* 323  */ 			if( el.find('.em-ticket-form-advanced input[type="checkbox"]:checked').length > 0 ){ show_advanced = true; }
/* 324  */ 			el.find('.em-ticket-form-advanced option:selected').each(function(){ if(this.value != '') show_advanced = true; });
/* 325  */ 			if( show_advanced ) el.find('.ticket-options-advanced').trigger('click');
/* 326  */ 		});
/* 327  */ 		//Delete a ticket
/* 328  */ 		$(document).delegate('.ticket-actions-delete', 'click', function(e){
/* 329  */ 			e.preventDefault();
/* 330  */ 			var el = $(this);
/* 331  */ 			var tbody = el.closest('tbody');
/* 332  */ 			if( tbody.find('input.ticket_id').val() > 0 ){
/* 333  */ 				//only will happen if no bookings made
/* 334  */ 				el.text('Deleting...');	
/* 335  */ 				$.getJSON( $(this).attr('href'), {'em_ajax_action':'delete_ticket', 'id':tbody.find('input.ticket_id').val()}, function(data){
/* 336  */ 					if(data.result){
/* 337  */ 						tbody.remove();
/* 338  */ 					}else{
/* 339  */ 						el.text('Delete');
/* 340  */ 						alert(data.error);
/* 341  */ 					}
/* 342  */ 				});
/* 343  */ 			}else{
/* 344  */ 				//not saved to db yet, so just remove
/* 345  */ 				tbody.remove();
/* 346  */ 			}
/* 347  */ 			return false;
/* 348  */ 		});
/* 349  */ 	}
/* 350  */ 	//Manageing Bookings

/* events-manager.js */

/* 351  */ 	if( $('#em-bookings-table').length > 0 ){
/* 352  */ 		//Pagination link clicks
/* 353  */ 		$(document).delegate('#em-bookings-table .tablenav-pages a', 'click', function(){
/* 354  */ 			var el = $(this);
/* 355  */ 			var form = el.parents('#em-bookings-table form.bookings-filter');
/* 356  */ 			//get page no from url, change page, submit form
/* 357  */ 			var match = el.attr('href').match(/#[0-9]+/);
/* 358  */ 			if( match != null && match.length > 0){
/* 359  */ 				var pno = match[0].replace('#','');
/* 360  */ 				form.find('input[name=pno]').val(pno);
/* 361  */ 			}else{
/* 362  */ 				form.find('input[name=pno]').val(1);
/* 363  */ 			}
/* 364  */ 			form.trigger('submit');
/* 365  */ 			return false;
/* 366  */ 		});
/* 367  */ 		//Overlay Options
/* 368  */ 		var em_bookings_settings_dialog = {
/* 369  */ 			modal : true,
/* 370  */ 			autoOpen: false,
/* 371  */ 			minWidth: 500,
/* 372  */ 			height: 'auto',
/* 373  */ 			buttons: [{
/* 374  */ 				text: EM.bookings_settings_save,
/* 375  */ 				click: function(e){
/* 376  */ 					e.preventDefault();
/* 377  */ 					//we know we'll deal with cols, so wipe hidden value from main
/* 378  */ 					var match = $("#em-bookings-table form.bookings-filter [name=cols]").val('');
/* 379  */ 					var booking_form_cols = $('form#em-bookings-table-settings-form input.em-bookings-col-item');
/* 380  */ 					$.each( booking_form_cols, function(i,item_match){
/* 381  */ 						//item_match = $(item_match);
/* 382  */ 						if( item_match.value == 1 ){
/* 383  */ 							if( match.val() != ''){
/* 384  */ 								match.val(match.val()+','+item_match.name);
/* 385  */ 							}else{
/* 386  */ 								match.val(item_match.name);
/* 387  */ 							}
/* 388  */ 						}
/* 389  */ 					});
/* 390  */ 					//submit main form
/* 391  */ 					$('#em-bookings-table-settings').trigger('submitted'); //hook into this with bind()
/* 392  */ 					$('#em-bookings-table form.bookings-filter').trigger('submit');					
/* 393  */ 					$(this).dialog('close');
/* 394  */ 				}
/* 395  */ 			}]
/* 396  */ 		};
/* 397  */ 		var em_bookings_export_dialog = {
/* 398  */ 			modal : true,
/* 399  */ 			autoOpen: false,
/* 400  */ 			minWidth: 500,

/* events-manager.js */

/* 401  */ 			height: 'auto',
/* 402  */ 			buttons: [{
/* 403  */ 				text: EM.bookings_export_save,
/* 404  */ 				click: function(e){
/* 405  */ 					$(this).children('form').submit();
/* 406  */ 					$(this).dialog('close');
/* 407  */ 				}
/* 408  */ 			}]
/* 409  */ 		};
/* 410  */ 		if( $("#em-bookings-table-settings").length > 0 ){
/* 411  */ 			//Settings Overlay
/* 412  */ 			$("#em-bookings-table-settings").dialog(em_bookings_settings_dialog);
/* 413  */ 			$(document).delegate('#em-bookings-table-settings-trigger','click', function(e){ e.preventDefault(); $("#em-bookings-table-settings").dialog('open'); });
/* 414  */ 			//Export Overlay
/* 415  */ 			$("#em-bookings-table-export").dialog(em_bookings_export_dialog);
/* 416  */ 			$(document).delegate('#em-bookings-table-export-trigger','click', function(e){ e.preventDefault(); $("#em-bookings-table-export").dialog('open'); });
/* 417  */ 			var export_overlay_show_tickets = function(){
/* 418  */ 				if( $('#em-bookings-table-export-form input[name=show_tickets]').is(':checked') ){
/* 419  */ 					$('#em-bookings-table-export-form .em-bookings-col-item-ticket').show();
/* 420  */ 					$('#em-bookings-table-export-form #em-bookings-export-cols-active .em-bookings-col-item-ticket input').val(1);
/* 421  */ 				}else{
/* 422  */ 					$('#em-bookings-table-export-form .em-bookings-col-item-ticket').hide().find('input').val(0);					
/* 423  */ 				}
/* 424  */ 			};
/* 425  */ 			//Sync export overlay with table search field changes
/* 426  */ 			$('#em-bookings-table form select').each(function(i, el){
/* 427  */ 				$(el).change(function(e){
/* 428  */ 					var select_el = $(this);
/* 429  */ 					var input_par = $('#em-bookings-table-export-form input[name='+select_el.attr('name')+']');
/* 430  */ 					var input_par_selected = select_el.find('option:selected');
/* 431  */ 					input_par.val(input_par_selected.val());
/* 432  */ 				});
/* 433  */ 			});
/* 434  */ 			
/* 435  */ 			export_overlay_show_tickets();
/* 436  */ 			$('#em-bookings-table-export-form input[name=show_tickets]').click(export_overlay_show_tickets);
/* 437  */ 			//Sortables
/* 438  */ 			$( ".em-bookings-cols-sortable" ).sortable({
/* 439  */ 				connectWith: ".em-bookings-cols-sortable",
/* 440  */ 				update: function(event, ui) {
/* 441  */ 					if( ui.item.parents('ul#em-bookings-cols-active, ul#em-bookings-export-cols-active').length > 0 ){							
/* 442  */ 						ui.item.addClass('ui-state-highlight').removeClass('ui-state-default').children('input').val(1);
/* 443  */ 					}else{
/* 444  */ 						ui.item.addClass('ui-state-default').removeClass('ui-state-highlight').children('input').val(0);
/* 445  */ 					}
/* 446  */ 				}
/* 447  */ 			}).disableSelection();
/* 448  */ 			load_ui_css = true;
/* 449  */ 		}
/* 450  */ 		//Widgets and filter submissions

/* events-manager.js */

/* 451  */ 		$(document).delegate('#em-bookings-table form.bookings-filter', 'submit', function(e){
/* 452  */ 			var el = $(this);			
/* 453  */ 			//append loading spinner
/* 454  */ 			el.parents('#em-bookings-table').find('.table-wrap').first().append('<div id="em-loading" />');
/* 455  */ 			//ajax call
/* 456  */ 			$.post( EM.ajaxurl, el.serializeArray(), function(data){
/* 457  */ 				var root = el.parents('#em-bookings-table').first();
/* 458  */ 				root.replaceWith(data);
/* 459  */ 				//recreate overlays
/* 460  */ 				$('#em-bookings-table-export input[name=scope]').val(root.find('select[name=scope]').val());
/* 461  */ 				$('#em-bookings-table-export input[name=status]').val(root.find('select[name=status]').val());
/* 462  */ 				jQuery(document).triggerHandler('em_bookings_filtered', [data, root, el]);
/* 463  */ 			});
/* 464  */ 			return false;
/* 465  */ 		});
/* 466  */ 		//Approve/Reject Links
/* 467  */ 		$(document).delegate('.em-bookings-approve,.em-bookings-reject,.em-bookings-unapprove,.em-bookings-delete', 'click', function(){
/* 468  */ 			var el = $(this); 
/* 469  */ 			if( el.hasClass('em-bookings-delete') ){
/* 470  */ 				if( !confirm(EM.booking_delete) ){ return false; }
/* 471  */ 			}
/* 472  */ 			var url = em_ajaxify( el.attr('href'));		
/* 473  */ 			var td = el.parents('td').first();
/* 474  */ 			td.html(EM.txt_loading);
/* 475  */ 			td.load( url );
/* 476  */ 			return false;
/* 477  */ 		});
/* 478  */ 	}
/* 479  */ 	//Old Bookings Table - depreciating soon
/* 480  */ 	if( $('.em_bookings_events_table').length > 0 ){
/* 481  */ 		//Widgets and filter submissions
/* 482  */ 		$(document).delegate('.em_bookings_events_table form', 'submit', function(e){
/* 483  */ 			var el = $(this);
/* 484  */ 			var url = em_ajaxify( el.attr('action') );		
/* 485  */ 			el.parents('.em_bookings_events_table').find('.table-wrap').first().append('<div id="em-loading" />');
/* 486  */ 			$.get( url, el.serializeArray(), function(data){
/* 487  */ 				el.parents('.em_bookings_events_table').first().replaceWith(data);
/* 488  */ 			});
/* 489  */ 			return false;
/* 490  */ 		});
/* 491  */ 		//Pagination link clicks
/* 492  */ 		$(document).delegate('.em_bookings_events_table .tablenav-pages a', 'click', function(){		
/* 493  */ 			var el = $(this);
/* 494  */ 			var url = em_ajaxify( el.attr('href') );	
/* 495  */ 			el.parents('.em_bookings_events_table').find('.table-wrap').first().append('<div id="em-loading" />');
/* 496  */ 			$.get( url, function(data){
/* 497  */ 				el.parents('.em_bookings_events_table').first().replaceWith(data);
/* 498  */ 			});
/* 499  */ 			return false;
/* 500  */ 		});

/* events-manager.js */

/* 501  */ 	}
/* 502  */ 	
/* 503  */ 	//Manual Booking
/* 504  */ 	$('a.em-booking-button').click(function(e){
/* 505  */ 		e.preventDefault();
/* 506  */ 		var button = $(this);
/* 507  */ 		if( button.text() != EM.bb_booked && $(this).text() != EM.bb_booking){
/* 508  */ 			button.text(EM.bb_booking);
/* 509  */ 			var button_data = button.attr('id').split('_'); 
/* 510  */ 			$.ajax({
/* 511  */ 				url: EM.ajaxurl,
/* 512  */ 				dataType: 'jsonp',
/* 513  */ 				data: {
/* 514  */ 					event_id : button_data[1],
/* 515  */ 					_wpnonce : button_data[2],
/* 516  */ 					action : 'booking_add_one'
/* 517  */ 				},
/* 518  */ 				success : function(response, statusText, xhr, $form) {
/* 519  */ 					if(response.result){
/* 520  */ 						button.text(EM.bb_booked);
/* 521  */ 					}else{
/* 522  */ 						button.text(EM.bb_error);					
/* 523  */ 					}
/* 524  */ 					if(response.message != '') alert(response.message);
/* 525  */ 				},
/* 526  */ 				error : function(){ button.text(EM.bb_error); }
/* 527  */ 			});
/* 528  */ 		}
/* 529  */ 		return false;
/* 530  */ 	});	
/* 531  */ 	$('a.em-cancel-button').click(function(e){
/* 532  */ 		e.preventDefault();
/* 533  */ 		var button = $(this);
/* 534  */ 		if( button.text() != EM.bb_cancelled && button.text() != EM.bb_canceling){
/* 535  */ 			button.text(EM.bb_canceling);
/* 536  */ 			var button_data = button.attr('id').split('_'); 
/* 537  */ 			$.ajax({
/* 538  */ 				url: EM.ajaxurl,
/* 539  */ 				dataType: 'jsonp',
/* 540  */ 				data: {
/* 541  */ 					booking_id : button_data[1],
/* 542  */ 					_wpnonce : button_data[2],
/* 543  */ 					action : 'booking_cancel'
/* 544  */ 				},
/* 545  */ 				success : function(response, statusText, xhr, $form) {
/* 546  */ 					if(response.result){
/* 547  */ 						button.text(EM.bb_cancelled);
/* 548  */ 					}else{
/* 549  */ 						button.text(EM.bb_cancel_error);
/* 550  */ 					}

/* events-manager.js */

/* 551  */ 				},
/* 552  */ 				error : function(){ button.text(EM.bb_cancel_error); }
/* 553  */ 			});
/* 554  */ 		}
/* 555  */ 		return false;
/* 556  */ 	});  
/* 557  */ 
/* 558  */ 	//Datepicker
/* 559  */ 	if( $('.em-date-single, .em-date-range, #em-date-start').length > 0 ){
/* 560  */ 		if( EM.locale != 'en' && EM.locale_data ){
/* 561  */ 			$.datepicker.setDefaults(EM.locale_data);
/* 562  */ 		}
/* 563  */ 		load_ui_css = true;
/* 564  */ 		em_setup_datepicker('body');
/* 565  */ 	}
/* 566  */ 	if( load_ui_css ) em_load_jquery_css();
/* 567  */ 	
/* 568  */ 	//previously in em-admin.php
/* 569  */ 	function updateIntervalDescriptor () { 
/* 570  */ 		$(".interval-desc").hide();
/* 571  */ 		var number = "-plural";
/* 572  */ 		if ($('input#recurrence-interval').val() == 1 || $('input#recurrence-interval').val() == "")
/* 573  */ 		number = "-singular";
/* 574  */ 		var descriptor = "span#interval-"+$("select#recurrence-frequency").val()+number;
/* 575  */ 		$(descriptor).show();
/* 576  */ 	}
/* 577  */ 	function updateIntervalSelectors () {
/* 578  */ 		$('p.alternate-selector').hide();   
/* 579  */ 		$('p#'+ $('select#recurrence-frequency').val() + "-selector").show();
/* 580  */ 	}
/* 581  */ 	function updateShowHideRecurrence () {
/* 582  */ 		if( $('input#event-recurrence').is(":checked")) {
/* 583  */ 			$("#event_recurrence_pattern").fadeIn();
/* 584  */ 			$("#event-date-explanation").hide();
/* 585  */ 			$("#recurrence-dates-explanation").show();
/* 586  */ 			$("h3#recurrence-dates-title").show();
/* 587  */ 			$("h3#event-date-title").hide();     
/* 588  */ 		} else {
/* 589  */ 			$("#event_recurrence_pattern").hide();
/* 590  */ 			$("#recurrence-dates-explanation").hide();
/* 591  */ 			$("#event-date-explanation").show();
/* 592  */ 			$("h3#recurrence-dates-title").hide();
/* 593  */ 			$("h3#event-date-title").show();   
/* 594  */ 		}
/* 595  */ 	}		 
/* 596  */ 	$("#recurrence-dates-explanation").hide();
/* 597  */ 	$("#date-to-submit").hide();
/* 598  */ 	$("#end-date-to-submit").hide();
/* 599  */ 	
/* 600  */ 	$("#localised-date").show();

/* events-manager.js */

/* 601  */ 	$("#localised-end-date").show();
/* 602  */ 	
/* 603  */ 	$('#em-wrapper input.select-all').change(function(){
/* 604  */ 	 	if($(this).is(':checked')){
/* 605  */ 			$('input.row-selector').prop('checked', true);
/* 606  */ 			$('input.select-all').prop('checked', true);
/* 607  */ 	 	}else{
/* 608  */ 			$('input.row-selector').prop('checked', false);
/* 609  */ 			$('input.select-all').prop('checked', false);
/* 610  */ 		}
/* 611  */ 	}); 
/* 612  */ 	
/* 613  */ 	updateIntervalDescriptor(); 
/* 614  */ 	updateIntervalSelectors();
/* 615  */ 	updateShowHideRecurrence();
/* 616  */ 	$('input#event-recurrence').change(updateShowHideRecurrence);
/* 617  */ 	   
/* 618  */ 	// recurrency elements   
/* 619  */ 	$('input#recurrence-interval').keyup(updateIntervalDescriptor);
/* 620  */ 	$('select#recurrence-frequency').change(updateIntervalDescriptor);
/* 621  */ 	$('select#recurrence-frequency').change(updateIntervalSelectors);
/* 622  */ 
/* 623  */ 	/* Load any maps */	
/* 624  */ 	if( $('.em-location-map').length > 0 || $('.em-locations-map').length > 0 || $('#em-map').length > 0 || $('.em-search-geo').length > 0 ){
/* 625  */ 		em_maps_load();
/* 626  */ 	}
/* 627  */ 	
/* 628  */ 	//Finally, add autocomplete here
/* 629  */ 	//Autocomplete
/* 630  */ 	if( jQuery( "div.em-location-data input#location-name" ).length > 0 ){
/* 631  */ 		jQuery( "div.em-location-data input#location-name" ).autocomplete({
/* 632  */ 			source: EM.locationajaxurl,
/* 633  */ 			minLength: 2,
/* 634  */ 			focus: function( event, ui ){
/* 635  */ 				jQuery("input#location-id" ).val( ui.item.value );
/* 636  */ 				return false;
/* 637  */ 			},			 
/* 638  */ 			select: function( event, ui ){
/* 639  */ 				var sellocation_id = ui.item.id;
/* 640  */ 				jQuery("input#location-id" ).val(ui.item.id).trigger('change');
/* 641  */ 				jQuery("input#location-name" ).val(ui.item.value);
/* 642  */ 				var locdetailurl = 'http://p1g.co/wp-content/themes/Directory/custom-ajax/location_details.php';
/* 643  */ 				$.ajax({
/* 644  */ 					url: locdetailurl,
/* 645  */ 					dataType : 'json',
/* 646  */ 					data :"action=locationdata_search & sellocation_id="+sellocation_id,
/* 647  */ 					success : function(results){
/* 648  */ 						jQuery('input#location-address').val(results.address);
/* 649  */ 						jQuery('input#location-town').val(results.city);
/* 650  */ 						jQuery('input#location-state').val(results.state);

/* events-manager.js */

/* 651  */ 						jQuery('input#location-postcode').val(results.zip);
/* 652  */ 						jQuery('input#location-latitude').val(results.geo_latitude);
/* 653  */ 						jQuery('input#location-longitude').val(results.geo_longitude);
/* 654  */ 					}
/* 655  */ 				});
/* 656  */ 				/*jQuery('input#location-address').val(ui.item.address);
/* 657  *| 				jQuery('input#location-town').val(ui.item.town);
/* 658  *| 				jQuery('input#location-state').val(ui.item.state);
/* 659  *| 				jQuery('input#location-region').val(ui.item.region);
/* 660  *| 				jQuery('input#location-postcode').val(ui.item.postcode);
/* 661  *| 				if( ui.item.country == '' ){
/* 662  *| 					jQuery('select#location-country option:selected').removeAttr('selected');
/* 663  *| 				}else{
/* 664  *| 					jQuery('select#location-country option[value="'+ui.item.country+'"]').attr('selected', 'selected');
/* 665  *| 				}*/
/* 666  */ 				jQuery('div.em-location-data input, div.em-location-data select').css('background-color','#ccc').attr('readonly','readonly');
/* 667  */ 				jQuery('div.em-location-data select').css('background-color','#ccc').attr('disabled','disabled');
/* 668  */ 				jQuery('#em-location-reset').show();
/* 669  */ 				jQuery('#em-location-search-tip').hide();
/* 670  */ 				jQuery(document).triggerHandler('em_locations_autocomplete_selected', [event, ui]);
/* 671  */ 				return false;
/* 672  */ 			}
/* 673  */ 		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
/* 674  */ 			html_val = "<a>" + item.label + '<br><span style="font-size:11px"><em>'+ item.fulladdress +"</em></span></a>";
/* 675  */ 			return jQuery( "<li></li>" ).data( "item.autocomplete", item ).append(html_val).appendTo( ul );
/* 676  */ 		};
/* 677  */ 		jQuery('#em-location-reset a').click( function(){
/* 678  */ 			jQuery('div.em-location-data input').css('background-color','#fff').val('').removeAttr('readonly');
/* 679  */ 			jQuery('div.em-location-data select').css('background-color','#fff');
/* 680  */ 			jQuery('div.em-location-data option:selected').removeAttr('selected');
/* 681  */ 			jQuery('input#location-id').val('');
/* 682  */ 			jQuery('#em-location-reset').hide();
/* 683  */ 			jQuery('#em-location-search-tip').show();
/* 684  */ 			jQuery('#em-map').hide();
/* 685  */ 			jQuery('#em-map-404').show();
/* 686  */ 			if(typeof(marker) !== 'undefined'){
/* 687  */ 				marker.setPosition(new google.maps.LatLng(0, 0));
/* 688  */ 				infoWindow.close();
/* 689  */ 				marker.setDraggable(true);
/* 690  */ 			}
/* 691  */ 			return false;
/* 692  */ 		});
/* 693  */ 		if( jQuery('input#location-id').val() != '0' && jQuery('input#location-id').val() != '' ){
/* 694  */ 			jQuery('div.em-location-data input, div.em-location-data select').css('background-color','#ccc').attr('readonly','readonly');
/* 695  */ 			jQuery('#em-location-reset').show();
/* 696  */ 			jQuery('#em-location-search-tip').hide();
/* 697  */ 		}
/* 698  */ 	}
/* 699  */ 	
/* 700  */ });

/* events-manager.js */

/* 701  */ 
/* 702  */ function em_load_jquery_css(){
/* 703  */ 	if( EM.ui_css && jQuery('link#jquery-ui-css').length == 0 ){
/* 704  */ 		var script = document.createElement("link");
/* 705  */ 		script.id = 'jquery-ui-css';
/* 706  */ 		script.rel = "stylesheet";
/* 707  */ 		script.href = EM.ui_css;
/* 708  */ 		document.body.appendChild(script);
/* 709  */ 	}
/* 710  */ }
/* 711  */ 
/* 712  */ function em_setup_datepicker(wrap){	
/* 713  */ 	wrap = jQuery(wrap);
/* 714  */ 	//default picker vals
/* 715  */ 	var datepicker_vals = { altFormat: "yy-mm-dd", changeMonth: true, changeYear: true, firstDay : EM.firstDay, yearRange:'-100:+10' };
/* 716  */ 	if( EM.dateFormat ) datepicker_vals.dateFormat = EM.dateFormat;
/* 717  */ 	if( EM.yearRange ) datepicker_vals.yearRange = EM.yearRange;
/* 718  */ 	jQuery(document).triggerHandler('em_datepicker', datepicker_vals);
/* 719  */ 	
/* 720  */ 	//apply datepickers
/* 721  */ 	dateDivs = wrap.find('.em-date-single, .em-date-range');
/* 722  */ 	if( dateDivs.length > 0 ){
/* 723  */ 		//apply datepickers to elements
/* 724  */ 		dateDivs.find('input.em-date-input-loc').each(function(i,dateInput){
/* 725  */ 			//init the datepicker
/* 726  */ 			var dateInput = jQuery(dateInput);
/* 727  */ 			var dateValue = dateInput.nextAll('input.em-date-input').first();
/* 728  */ 			var dateValue_value = dateValue.val();
/* 729  */ 			dateInput.datepicker(datepicker_vals);
/* 730  */ 			dateInput.datepicker('option', 'altField', dateValue);
/* 731  */ 			//now set the value
/* 732  */ 			if( dateValue_value ){
/* 733  */ 				var this_date_formatted = jQuery.datepicker.formatDate( EM.dateFormat, jQuery.datepicker.parseDate('yy-mm-dd', dateValue_value) );
/* 734  */ 				dateInput.val(this_date_formatted);
/* 735  */ 				dateValue.val(dateValue_value);
/* 736  */ 			}
/* 737  */ 			//add logic for texts
/* 738  */ 			dateInput.change(function(){
/* 739  */ 				if( jQuery(this).val() == '' ){
/* 740  */ 					jQuery(this).nextAll('.em-date-input').first().val('');
/* 741  */ 				}
/* 742  */ 			});
/* 743  */ 		});
/* 744  */ 		//deal with date ranges
/* 745  */ 		dateDivs.filter('.em-date-range').find('input.em-date-input-loc').each(function(i,dateInput){
/* 746  */ 			//finally, apply start/end logic to this field
/* 747  */ 			dateInput = jQuery(dateInput);
/* 748  */ 			if( dateInput.hasClass('em-date-start') ){
/* 749  */ 				dateInput.datepicker('option','onSelect', function( selectedDate ) {
/* 750  */ 					//get corresponding end date input, we expect ranges to be contained in .em-date-range with a start/end input element

/* events-manager.js */

/* 751  */ 					var startDate = jQuery(this);
/* 752  */ 					var endDate = startDate.parents('.em-date-range').find('.em-date-end').first();
/* 753  */ 					if( startDate.val() > endDate.val() && endDate.val() != '' ){
/* 754  */ 						endDate.datepicker( "setDate" , selectedDate );
/* 755  */ 						endDate.trigger('change');
/* 756  */ 					}
/* 757  */ 					endDate.datepicker( "option", 'minDate', selectedDate );
/* 758  */ 				});
/* 759  */ 			}else if( dateInput.hasClass('em-date-end') ){
/* 760  */ 				var startInput = dateInput.parents('.em-date-range').find('.em-date-start').first();
/* 761  */ 				if( startInput.val() != '' ){
/* 762  */ 					dateInput.datepicker('option', 'minDate', startInput.val());
/* 763  */ 				}
/* 764  */ 			}
/* 765  */ 		});
/* 766  */ 	}
/* 767  */ }
/* 768  */ 
/* 769  */ function em_setup_timepicker(wrap){
/* 770  */ 	wrap = jQuery(wrap);
/* 771  */ 	var timepicker_options = {
/* 772  */ 		show24Hours: EM.show24hours == 1,
/* 773  */ 		step:15
/* 774  */ 	}
/* 775  */ 	jQuery(document).triggerHandler('em_timepicker_options', timepicker_options);
/* 776  */ 	wrap.find(".em-time-input").timePicker(timepicker_options);
/* 777  */ 	
/* 778  */ 	// Keep the duration between the two inputs.
/* 779  */ 	wrap.find(".em-time-range input.em-time-start").each( function(i, el){
/* 780  */ 		jQuery(el).data('oldTime', jQuery.timePicker(el).getTime());
/* 781  */ 	}).change( function() {
/* 782  */ 		var start = jQuery(this);
/* 783  */ 		var end = start.nextAll('.em-time-end');
/* 784  */ 		if (end.val()) { // Only update when second input has a value.
/* 785  */ 		    // Calculate duration.
/* 786  */ 			var oldTime = start.data('oldTime');
/* 787  */ 		    var duration = (jQuery.timePicker(end).getTime() - oldTime);
/* 788  */ 		    var time = jQuery.timePicker(start).getTime();
/* 789  */ 		    if( jQuery.timePicker(end).getTime() >= oldTime ){
/* 790  */ 			    // Calculate and update the time in the second input.
/* 791  */ 			    jQuery.timePicker(end).setTime(new Date(new Date(time.getTime() + duration)));
/* 792  */ 			}
/* 793  */ 		    start.data('oldTime', time); 
/* 794  */ 		}
/* 795  */ 	});
/* 796  */ 	// Validate.
/* 797  */ 	wrap.find(".em-time-range input.em-time-end").change(function() {
/* 798  */ 		var end = jQuery(this);
/* 799  */ 		var start = end.prevAll('.em-time-start');
/* 800  */ 		if( start.val() ){

/* events-manager.js */

/* 801  */ 			if( jQuery.timePicker(start).getTime() > jQuery.timePicker(this).getTime() && ( jQuery('.em-date-end').val().length == 0 || jQuery('.em-date-start').val() == jQuery('.em-date-end').val() ) ) { end.addClass("error"); }
/* 802  */ 			else { end.removeClass("error"); }
/* 803  */ 		}
/* 804  */ 	});
/* 805  */ 	//Sort out all day checkbox
/* 806  */ 	wrap.find('.em-time-range input.em-time-all-day').change(function(){
/* 807  */ 		var allday = jQuery(this);
/* 808  */ 		if( allday.is(':checked') ){
/* 809  */ 			allday.siblings('.em-time-input').css('background-color','#ccc');
/* 810  */ 		}else{
/* 811  */ 			allday.siblings('.em-time-input').css('background-color','#fff');
/* 812  */ 		}
/* 813  */ 	}).trigger('change');
/* 814  */ }
/* 815  */ 
/* 816  */ /* Useful function for adding the em_ajax flag to a url, regardless of querystring format */
/* 817  */ var em_ajaxify = function(url){
/* 818  */ 	if ( url.search('em_ajax=0') != -1){
/* 819  */ 		url = url.replace('em_ajax=0','em_ajax=1');
/* 820  */ 	}else if( url.search(/\?/) != -1 ){
/* 821  */ 		url = url + "&em_ajax=1";
/* 822  */ 	}else{
/* 823  */ 		url = url + "?em_ajax=1";
/* 824  */ 	}
/* 825  */ 	return url;
/* 826  */ };
/* 827  */ 
/* 828  */ /*
/* 829  *|  * MAP FUNCTIONS
/* 830  *|  */
/* 831  */ var em_maps_loaded = false;
/* 832  */ var maps = {};
/* 833  */ var maps_markers = {};
/* 834  */ var infowindow;
/* 835  */ //loads maps script if not already loaded and executes EM maps script
/* 836  */ function em_maps_load(){
/* 837  */ 	if( !em_maps_loaded ){
/* 838  */ 		if ( jQuery('script#google-maps').length == 0 && ( typeof google !== 'object' || typeof google.maps !== 'object' ) ){ 
/* 839  */ 			var script = document.createElement("script");
/* 840  */ 			script.type = "text/javascript";
/* 841  */ 			script.id = "google-maps";
/* 842  */ 			var proto = (EM.is_ssl) ? 'https:' : 'http:';
/* 843  */ 			script.src = proto + '//maps.google.com/maps/api/js?v=3.12&sensor=false&libraries=places&callback=em_maps';
/* 844  */ 			document.body.appendChild(script);
/* 845  */ 		}else if( typeof google === 'object' && typeof google.maps === 'object' && !em_maps_loaded ){
/* 846  */ 			em_maps();
/* 847  */ 		}else if( jQuery('script#google-maps').length > 0 ){
/* 848  */ 			jQuery(window).load(function(){ if( !em_maps_loaded ) em_maps(); }); //google isn't loaded so wait for page to load resources
/* 849  */ 		}
/* 850  */ 	}

/* events-manager.js */

/* 851  */ }
/* 852  */ //re-usable function to load global location maps
/* 853  */ function em_maps_load_locations(el){
/* 854  */ 	var el = jQuery(el);
/* 855  */ 	var map_id = el.attr('id').replace('em-locations-map-','');
/* 856  */ 	var em_data = jQuery.parseJSON( el.nextAll('.em-locations-map-coords').first().text() );
/* 857  */ 	if( em_data == null ){
/* 858  */ 		var em_data = jQuery.parseJSON( jQuery('#em-locations-map-coords-'+map_id).text() );
/* 859  */ 	}
/* 860  */ 	jQuery.getJSON(document.URL, em_data , function(data){
/* 861  */ 		if(data.length > 0){
/* 862  */ 			//define default options and allow option for extension via event triggers
/* 863  */ 			  var map_options = { mapTypeId: google.maps.MapTypeId.ROADMAP };
/* 864  */ 			  jQuery(document).triggerHandler('em_maps_locations_map_options', map_options);
/* 865  */ 			  var marker_options = {};
/* 866  */ 			  jQuery(document).triggerHandler('em_maps_location_marker_options', marker_options);
/* 867  */ 			  
/* 868  */ 			  maps[map_id] = new google.maps.Map(el[0], map_options);
/* 869  */ 			  maps_markers[map_id] = [];
/* 870  */ 			  
/* 871  */ 			  var minLatLngArr = [0,0];
/* 872  */ 			  var maxLatLngArr = [0,0];
/* 873  */ 			  
/* 874  */ 			  for (var i = 0; i < data.length; i++) {
/* 875  */ 				  if( !(data[i].location_latitude == 0 && data[i].location_longitude == 0) ){
/* 876  */ 					var latitude = parseFloat( data[i].location_latitude );
/* 877  */ 					var longitude = parseFloat( data[i].location_longitude );
/* 878  */ 					var location = new google.maps.LatLng( latitude, longitude );
/* 879  */ 					//extend the default marker options
/* 880  */ 					jQuery.extend(marker_options, {
/* 881  */ 					    position: location, 
/* 882  */ 					    map: maps[map_id]
/* 883  */ 					})
/* 884  */ 					var marker = new google.maps.Marker(marker_options);
/* 885  */ 					maps_markers[map_id].push(marker);
/* 886  */ 					marker.setTitle(data[i].location_name);
/* 887  */ 					var myContent = '<div class="em-map-balloon"><div id="em-map-balloon-'+map_id+'" class="em-map-balloon-content">'+ data[i].location_balloon +'</div></div>';
/* 888  */ 					em_map_infobox(marker, myContent, maps[map_id]);
/* 889  */ 					
/* 890  */ 					//Get min and max long/lats
/* 891  */ 					minLatLngArr[0] = (latitude < minLatLngArr[0] || i == 0) ? latitude : minLatLngArr[0];
/* 892  */ 					minLatLngArr[1] = (longitude < minLatLngArr[1] || i == 0) ? longitude : minLatLngArr[1];
/* 893  */ 					maxLatLngArr[0] = (latitude > maxLatLngArr[0] || i == 0) ? latitude : maxLatLngArr[0];
/* 894  */ 					maxLatLngArr[1] = (longitude > maxLatLngArr[1] || i == 0) ? longitude : maxLatLngArr[1];
/* 895  */ 				  }
/* 896  */ 			  }
/* 897  */ 			  // Zoom in to the bounds
/* 898  */ 			  var minLatLng = new google.maps.LatLng(minLatLngArr[0],minLatLngArr[1]);
/* 899  */ 			  var maxLatLng = new google.maps.LatLng(maxLatLngArr[0],maxLatLngArr[1]);
/* 900  */ 			  var bounds = new google.maps.LatLngBounds(minLatLng,maxLatLng);

/* events-manager.js */

/* 901  */ 			  maps[map_id].fitBounds(bounds);
/* 902  */ 			  
/* 903  */ 			//Call a hook if exists
/* 904  */ 			jQuery(document).triggerHandler('em_maps_locations_hook', [maps[map_id], data, map_id]);
/* 905  */ 		}else{
/* 906  */ 			el.children().first().html('No locations found');
/* 907  */ 			jQuery(document).triggerHandler('em_maps_locations_hook_not_found', [el]);
/* 908  */ 		}
/* 909  */ 	});
/* 910  */ }
/* 911  */ function em_maps_load_location(el){
/* 912  */ 	el = jQuery(el);
/* 913  */ 	var map_id = el.attr('id').replace('em-location-map-','');
/* 914  */ 	em_LatLng = new google.maps.LatLng( jQuery('#em-location-map-coords-'+map_id+' .lat').text(), jQuery('#em-location-map-coords-'+map_id+' .lng').text());
/* 915  */ 	//extend map and markers via event triggers
/* 916  */ 	var map_options = {
/* 917  */ 	    zoom: 14,
/* 918  */ 	    center: em_LatLng,
/* 919  */ 	    mapTypeId: google.maps.MapTypeId.ROADMAP,
/* 920  */ 	    mapTypeControl: false
/* 921  */ 	};
/* 922  */ 	jQuery(document).triggerHandler('em_maps_location_map_options', map_options);
/* 923  */ 	maps[map_id] = new google.maps.Map( document.getElementById('em-location-map-'+map_id), map_options);
/* 924  */ 	var marker_options = {
/* 925  */ 	    position: em_LatLng,
/* 926  */ 	    map: maps[map_id]
/* 927  */ 	};
/* 928  */ 	jQuery(document).triggerHandler('em_maps_location_marker_options', marker_options);
/* 929  */ 	maps_markers[map_id] = new google.maps.Marker(marker_options);
/* 930  */ 	infowindow = new google.maps.InfoWindow({ content: jQuery('#em-location-map-info-'+map_id+' .em-map-balloon').get(0) });
/* 931  */ 	infowindow.open(maps[map_id],maps_markers[map_id]);
/* 932  */ 	maps[map_id].panBy(40,-70);
/* 933  */ 	
/* 934  */ 	//JS Hook for handling map after instantiation
/* 935  */ 	//Example hook, which you can add elsewhere in your theme's JS - jQuery(document).bind('em_maps_location_hook', function(){ alert('hi');} );
/* 936  */ 	jQuery(document).triggerHandler('em_maps_location_hook', [maps[map_id], infowindow, maps_markers[map_id], map_id]);
/* 937  */ 	//map resize listener
/* 938  */ 	jQuery(window).on('resize', function(e) {
/* 939  */ 		google.maps.event.trigger(maps[map_id], "resize");
/* 940  */ 		maps[map_id].setCenter(maps_markers[map_id].getPosition());
/* 941  */ 		maps[map_id].panBy(40,-70);
/* 942  */ 	});
/* 943  */ }
/* 944  */ jQuery(document).bind('em_search_ajax', function(e, vars, wrapper){
/* 945  */ 	if( em_maps_loaded ){
/* 946  */ 		wrapper.find('.em-location-map').each( function(index, el){ em_maps_load_location(el); } );
/* 947  */ 		wrapper.find('.em-locations-map').each( function(index, el){ em_maps_load_locations(el); });
/* 948  */ 	}
/* 949  */ });
/* 950  */ //Load single maps (each map is treated as a seperate map).

/* events-manager.js */

/* 951  */ function em_maps() {
/* 952  */ 	//Find all the maps on this page and load them
/* 953  */ 	jQuery('.em-location-map').each( function(index, el){ em_maps_load_location(el); } );	
/* 954  */ 	jQuery('.em-locations-map').each( function(index, el){ em_maps_load_locations(el); } );
/* 955  */ 	
/* 956  */ 	//Location stuff - only needed if inputs for location exist
/* 957  */ 	if( jQuery('select#location-select-id, input#location-address').length > 0 ){
/* 958  */ 		var map, marker;
/* 959  */ 		//load map info
/* 960  */ 		var refresh_map_location = function(){
/* 961  */ 			var location_latitude = jQuery('#location-latitude').val();
/* 962  */ 			var location_longitude = jQuery('#location-longitude').val();
/* 963  */ 			if( !(location_latitude == 0 && location_longitude == 0) ){
/* 964  */ 				var position = new google.maps.LatLng(location_latitude, location_longitude); //the location coords
/* 965  */ 				marker.setPosition(position);
/* 966  */ 				var mapTitle = (jQuery('input#location-name').length > 0) ? jQuery('input#location-name').val():jQuery('input#title').val();
/* 967  */ 				marker.setTitle( jQuery('input#location-name input#title, #location-select-id').first().val() );
/* 968  */ 				jQuery('#em-map').show();
/* 969  */ 				jQuery('#em-map-404').hide();
/* 970  */ 				google.maps.event.trigger(map, 'resize');
/* 971  */ 				map.setCenter(position);
/* 972  */ 				map.panBy(40,-55);
/* 973  */ 				infoWindow.setContent( 
/* 974  */ 					'<div id="location-balloon-content"><strong>' + 
/* 975  */ 					mapTitle + 
/* 976  */ 					'</strong><br/>' + 
/* 977  */ 					jQuery('#location-address').val() + 
/* 978  */ 					'<br/>' + jQuery('#location-town').val()+ 
/* 979  */ 					'</div>'
/* 980  */ 				);
/* 981  */ 				infoWindow.open(map, marker);
/* 982  */ 				jQuery(document).triggerHandler('em_maps_location_hook', [map, infowindow, marker, 0]);
/* 983  */ 			} else {
/* 984  */     			jQuery('#em-map').hide();
/* 985  */     			jQuery('#em-map-404').show();
/* 986  */ 			}
/* 987  */ 		};
/* 988  */ 		
/* 989  */ 		//Add listeners for changes to address
/* 990  */ 		var get_map_by_id = function(id){
/* 991  */ 			if(jQuery('#em-map').length > 0){
/* 992  */ 				jQuery.getJSON(document.URL,{ em_ajax_action:'get_location', id:id }, function(data){
/* 993  */ 					if( data.location_latitude!=0 && data.location_longitude!=0 ){
/* 994  */ 						loc_latlng = new google.maps.LatLng(data.location_latitude, data.location_longitude);
/* 995  */ 						marker.setPosition(loc_latlng);
/* 996  */ 						marker.setTitle( data.location_name );
/* 997  */ 						marker.setDraggable(false);
/* 998  */ 						jQuery('#em-map').show();
/* 999  */ 						jQuery('#em-map-404').hide();
/* 1000 */ 						map.setCenter(loc_latlng);

/* events-manager.js */

/* 1001 */ 						map.panBy(40,-55);
/* 1002 */ 						infoWindow.setContent( '<div id="location-balloon-content">'+ data.location_balloon +'</div>');
/* 1003 */ 						infoWindow.open(map, marker);
/* 1004 */ 						google.maps.event.trigger(map, 'resize');
/* 1005 */ 						jQuery(document).triggerHandler('em_maps_location_hook', [map, infowindow, marker, 0]);
/* 1006 */ 					}else{
/* 1007 */ 						jQuery('#em-map').hide();
/* 1008 */ 						jQuery('#em-map-404').show();
/* 1009 */ 					}
/* 1010 */ 				});
/* 1011 */ 			}
/* 1012 */ 		};
/* 1013 */ 		jQuery('#location-select-id, input#location-id').change( function(){get_map_by_id(jQuery(this).val());} );
/* 1014 */ 		jQuery('#location-name, #location-town, #location-address, #location-state, #location-postcode, #location-country').change( function(){
/* 1015 */ 			//build address
/* 1016 */ 			var addresses = [ jQuery('#location-address').val(), jQuery('#location-town').val(), jQuery('#location-state').val(), jQuery('#location-postcode').val() ];
/* 1017 */ 			var address = '';
/* 1018 */ 			jQuery.each( addresses, function(i, val){
/* 1019 */ 				if( val != '' ){
/* 1020 */ 					address = ( address == '' ) ? address+val:address+', '+val;
/* 1021 */ 				}
/* 1022 */ 			});
/* 1023 */ 			if( address == '' ){ //in case only name is entered, no address
/* 1024 */ 				jQuery('#em-map').hide();
/* 1025 */ 				jQuery('#em-map-404').show();
/* 1026 */ 				return false;
/* 1027 */ 			}
/* 1028 */ 			//do country last, as it's using the text version
/* 1029 */ 			if( jQuery('#location-country option:selected').val() != 0 ){
/* 1030 */ 				address = ( address == '' ) ? address+jQuery('#location-country option:selected').text():address+', '+jQuery('#location-country option:selected').text();
/* 1031 */ 			}
/* 1032 */ 			if( address != '' && jQuery('#em-map').length > 0 ){
/* 1033 */ 				geocoder.geocode( { 'address': address }, function(results, status) {
/* 1034 */ 				    if (status == google.maps.GeocoderStatus.OK) {
/* 1035 */ 						jQuery('#location-latitude').val(results[0].geometry.location.lat());
/* 1036 */ 						jQuery('#location-longitude').val(results[0].geometry.location.lng());
/* 1037 */ 					}
/* 1038 */ 				    refresh_map_location();
/* 1039 */ 				});
/* 1040 */ 			}
/* 1041 */ 		});
/* 1042 */ 		
/* 1043 */ 		//Load map
/* 1044 */ 		if(jQuery('#em-map').length > 0){
/* 1045 */ 			var em_LatLng = new google.maps.LatLng(0, 0);
/* 1046 */ 			map = new google.maps.Map( document.getElementById('em-map'), {
/* 1047 */ 			    zoom: 14,
/* 1048 */ 			    center: em_LatLng,
/* 1049 */ 			    mapTypeId: google.maps.MapTypeId.ROADMAP,
/* 1050 */ 			    mapTypeControl: false

/* events-manager.js */

/* 1051 */ 			});
/* 1052 */ 			var marker = new google.maps.Marker({
/* 1053 */ 			    position: em_LatLng,
/* 1054 */ 			    map: map,
/* 1055 */ 			    draggable: true
/* 1056 */ 			});
/* 1057 */ 			infoWindow = new google.maps.InfoWindow({
/* 1058 */ 			    content: ''
/* 1059 */ 			});
/* 1060 */ 			var geocoder = new google.maps.Geocoder();
/* 1061 */ 			google.maps.event.addListener(infoWindow, 'domready', function() { 
/* 1062 */ 				document.getElementById('location-balloon-content').parentNode.style.overflow=''; 
/* 1063 */ 				document.getElementById('location-balloon-content').parentNode.parentNode.style.overflow=''; 
/* 1064 */ 			});
/* 1065 */ 			google.maps.event.addListener(marker, 'dragend', function() {
/* 1066 */ 				var position = marker.getPosition();
/* 1067 */ 				jQuery('#location-latitude').val(position.lat());
/* 1068 */ 				jQuery('#location-longitude').val(position.lng());
/* 1069 */ 				map.setCenter(position);
/* 1070 */ 				map.panBy(40,-55);
/* 1071 */ 			});
/* 1072 */ 			if( jQuery('#location-select-id').length > 0 ){
/* 1073 */ 				jQuery('#location-select-id').trigger('change');
/* 1074 */ 			}else{
/* 1075 */ 				refresh_map_location();
/* 1076 */ 			}
/* 1077 */ 			jQuery(document).triggerHandler('em_map_loaded', [map, infowindow, marker]);
/* 1078 */ 		}
/* 1079 */ 		//map resize listener
/* 1080 */ 		jQuery(window).on('resize', function(e) {
/* 1081 */ 			google.maps.event.trigger(map, "resize");
/* 1082 */ 			map.setCenter(marker.getPosition());
/* 1083 */ 			map.panBy(40,-55);
/* 1084 */ 		});
/* 1085 */ 	}
/* 1086 */ 	em_maps_loaded = true; //maps have been loaded
/* 1087 */ 	jQuery(document).triggerHandler('em_maps_loaded');
/* 1088 */ }
/* 1089 */   
/* 1090 */ function em_map_infobox(marker, message, map) {
/* 1091 */   var iw = new google.maps.InfoWindow({ content: message });
/* 1092 */   google.maps.event.addListener(marker, 'click', function() {
/* 1093 */ 	if( infowindow ) infowindow.close();
/* 1094 */ 	infowindow = iw;
/* 1095 */     iw.open(map,marker);
/* 1096 */   });
/* 1097 */ }
/* 1098 */ 
/* 1099 */ /* jQuery timePicker - http://labs.perifer.se/timedatepicker/ @ http://github.com/perifer/timePicker commit 100644 */
/* 1100 */ (function(e){function t(t,n,r,i){t.value=e(n).text();e(t).change();if(!navigator.userAgent.match(/msie/i)){t.focus()}r.hide()}function n(e,t){var n=e.getHours();var i=t.show24Hours?n:(n+11)%12+1;var s=e.getMinutes();return r(i)+t.separator+r(s)+(t.show24Hours?"":n<12?" AM":" PM")}function r(e){return(e<10?"0":"")+e}function i(e,t){return typeof e=="object"?o(e):s(e,t)}function s(e,t){if(e){var n=e.split(t.separator);var r=parseFloat(n[0]);var i=parseFloat(n[1]);if(!t.show24Hours){if(r===12&&e.indexOf("AM")!==-1){r=0}else if(r!==12&&e.indexOf("PM")!==-1){r+=12}}var s=new Date(0,0,0,r,i,0);return o(s)}return null}function o(e){e.setFullYear(2001);e.setMonth(0);e.setDate(0);return e}e.fn.timePicker=function(t){var n=e.extend({},e.fn.timePicker.defaults,t);return this.each(function(){e.timePicker(this,n)})};e.timePicker=function(t,n){var r=e(t)[0];return r.timePicker||(r.timePicker=new jQuery._timePicker(r,n))};e.timePicker.version="0.3";e._timePicker=function(r,u){var a=false;var f=false;var l=i(u.startTime,u);var c=i(u.endTime,u);var h="selected";var p="li."+h;e(r).attr("autocomplete","OFF");var d=[];var v=new Date(l);while(v<=c){d[d.length]=n(v,u);v=new Date(v.setMinutes(v.getMinutes()+u.step))}var m=e('<div class="time-picker'+(u.show24Hours?"":" time-picker-12hours")+'"></div>');var g=e("<ul></ul>");for(var y=0;y<d.length;y++){g.append("<li>"+d[y]+"</li>")}m.append(g);m.appendTo("body").hide();m.mouseover(function(){a=true}).mouseout(function(){a=false});e("li",g).mouseover(function(){if(!f){e(p,m).removeClass(h);e(this).addClass(h)}}).mousedown(function(){a=true}).click(function(){t(r,this,m,u);a=false});var b=function(){if(m.is(":visible")){return false}e("li",m).removeClass(h);var t=e(r).offset();m.css({top:t.top+r.offsetHeight,left:t.left});m.show();var i=r.value?s(r.value,u):l;var a=l.getHours()*60+l.getMinutes();var f=i.getHours()*60+i.getMinutes()-a;var p=Math.round(f/u.step);var d=o(new Date(0,0,0,0,p*u.step+a,0));d=l<d&&d<=c?d:l;var v=e("li:contains("+n(d,u)+")",m);if(v.length){v.addClass(h);m[0].scrollTop=v[0].offsetTop}return true};e(r).focus(b).click(b);e(r).blur(function(){if(!a){m.hide()}});e(r)["keydown"](function(n){var i;f=true;var s=m[0].scrollTop;switch(n.keyCode){case 38:if(b()){return false}i=e(p,g);var o=i.prev().addClass(h)[0];if(o){i.removeClass(h);if(o.offsetTop<s){m[0].scrollTop=s-o.offsetHeight}}else{i.removeClass(h);o=e("li:last",g).addClass(h)[0];m[0].scrollTop=o.offsetTop-o.offsetHeight}return false;break;case 40:if(b()){return false}i=e(p,g);var a=i.next().addClass(h)[0];if(a){i.removeClass(h);if(a.offsetTop+a.offsetHeight>s+m[0].offsetHeight){m[0].scrollTop=s+a.offsetHeight}}else{i.removeClass(h);a=e("li:first",g).addClass(h)[0];m[0].scrollTop=0}return false;break;case 13:if(m.is(":visible")){var l=e(p,g)[0];t(r,l,m,u)}return false;break;case 27:m.hide();return false;break}return true});e(r).keyup(function(e){f=false});this.getTime=function(){return s(r.value,u)};this.setTime=function(t){r.value=n(i(t,u),u);e(r).change()}};e.fn.timePicker.defaults={step:30,startTime:new Date(0,0,0,0,0,0),endTime:new Date(0,0,0,23,30,0),separator:":",show24Hours:true}})(jQuery)
