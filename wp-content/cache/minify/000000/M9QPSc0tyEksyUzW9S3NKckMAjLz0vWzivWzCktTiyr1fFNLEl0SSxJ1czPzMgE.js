
/* jquery.MetaData-mini.js */

/* 1  */ /*
/* 2  *|  * Metadata - jQuery plugin for parsing metadata from elements
/* 3  *|  *
/* 4  *|  * Copyright (c) 2006 John Resig, Yehuda Katz, J�rn Zaefferer, Paul McLanahan
/* 5  *|  *
/* 6  *| 	* Licensed under http://en.wikipedia.org/wiki/MIT_License
/* 7  *|  *
/* 8  *|  *
/* 9  *|  */
/* 10 */ /**
/* 11 *|  * Sets the type of metadata to use. Metadata is encoded in JSON, and each property
/* 12 *|  * in the JSON will become a property of the element itself.
/* 13 *|  *
/* 14 *|  * There are three supported types of metadata storage:
/* 15 *|  *
/* 16 *|  *   attr:  Inside an attribute. The name parameter indicates *which* attribute.
/* 17 *|  *          
/* 18 *|  *   class: Inside the class attribute, wrapped in curly braces: { }
/* 19 *|  *   
/* 20 *|  *   elem:  Inside a child element (e.g. a script tag). The
/* 21 *|  *          name parameter indicates *which* element.
/* 22 *|  *          
/* 23 *|  * The metadata for an element is loaded the first time the element is accessed via jQuery.
/* 24 *|  *
/* 25 *|  * As a result, you can define the metadata type, use $(expr) to load the metadata into the elements
/* 26 *|  * matched by expr, then redefine the metadata type and run another $(expr) for other elements.
/* 27 *|  * 
/* 28 *|  * @name $.metadata.setType
/* 29 *|  *
/* 30 *|  * @example <p id="one" class="some_class {item_id: 1, item_label: 'Label'}">This is a p</p>
/* 31 *|  * @before $.metadata.setType("class")
/* 32 *|  * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
/* 33 *|  * @desc Reads metadata from the class attribute
/* 34 *|  * 
/* 35 *|  * @example <p id="one" class="some_class" data="{item_id: 1, item_label: 'Label'}">This is a p</p>
/* 36 *|  * @before $.metadata.setType("attr", "data")
/* 37 *|  * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
/* 38 *|  * @desc Reads metadata from a "data" attribute
/* 39 *|  * 
/* 40 *|  * @example <p id="one" class="some_class"><script>{item_id: 1, item_label: 'Label'}</script>This is a p</p>
/* 41 *|  * @before $.metadata.setType("elem", "script")
/* 42 *|  * @after $("#one").metadata().item_id == 1; $("#one").metadata().item_label == "Label"
/* 43 *|  * @desc Reads metadata from a nested script element
/* 44 *|  * 
/* 45 *|  * @param String type The encoding type
/* 46 *|  * @param String name The name of the attribute to be used to get metadata (optional)
/* 47 *|  * @cat Plugins/Metadata
/* 48 *|  * @descr Sets the type of encoding to be used when loading metadata for the first time
/* 49 *|  * @type undefined
/* 50 *|  * @see metadata()

/* jquery.MetaData-mini.js */

/* 51 *|  */
/* 52 */ (function($){$.extend({metadata:{defaults:{type:"class",name:"metadata",cre:/({.*})/,single:"metadata"},setType:function(e,t){this.defaults.type=e;this.defaults.name=t},get:function(elem,opts){var settings=$.extend({},this.defaults,opts);if(!settings.single.length)settings.single="metadata";var data=$.data(elem,settings.single);if(data)return data;data="{}";if(settings.type=="class"){var m=settings.cre.exec(elem.className);if(m)data=m[1]}else if(settings.type=="elem"){if(!elem.getElementsByTagName)return;var e=elem.getElementsByTagName(settings.name);if(e.length)data=$.trim(e[0].innerHTML)}else if(elem.getAttribute!=undefined){var attr=elem.getAttribute(settings.name);if(attr)data=attr}if(data.indexOf("{")<0)data="{"+data+"}";data=eval("("+data+")");$.data(elem,settings.single,data);return data}}});$.fn.metadata=function(e){return $.metadata.get(this[0],e)}})(jQuery)
/* 53 */ 
/* 54 */ jQuery.noConflict();
/* 55 */ function hide_rating(comment_id)
/* 56 */ {
/* 57 */    jQuery("#comment_rating_"+comment_id).hide();
/* 58 */ }
/* 59 */ function show_single_rating(comment_id)
/* 60 */ {
/* 61 */    jQuery("#single_comment_rating_"+comment_id).show();
/* 62 */ }
/* 63 */ function hide_single_rating(comment_id)
/* 64 */ {
/* 65 */    jQuery("#single_comment_rating_"+comment_id).hide();
/* 66 */ }
/* 67 */ function show_rating(comment_id)
/* 68 */ {
/* 69 */    jQuery("#comment_rating_"+comment_id).toggle();
/* 70 */ }
/* 71 */ function show_average_rating()
/* 72 */ {
/* 73 */ 	jQuery("#comment_rating_show_rating").toggle();
/* 74 */ }
