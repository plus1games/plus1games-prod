�7,W<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:7:"1970026";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-04-01 02:03:53";s:13:"post_date_gmt";s:19:"2016-04-01 07:03:53";s:12:"post_content";s:1587:"The Kentucky Derby is the second oldest horse race in America. The race track was built on farmland in Louisville, Kentucky. The first race was held on May 17, 1875, with 10 thousand people in attendance. An African-American named Oliver Lewis rode the winner, Aristides.

The race is held yearly in Louisville, Kentucky, United States during the first Saturday in May. It caps off the two-week Kentucky Derby Festival.

The race is a Grade I stakes race for three-year-old Thoroughbreds with a length of 2 kilometers at Churchill Downs.

Colts and Geldings carry 57 kilograms or 126 pounds and fillies 55 kilograms or 121 pounds.

In the United States, the race is known to be “The Most Exciting Two Minutes in Sports” or “The Fastest Two Minutes in Sports” because of its duration. It is also known to be “The Run for the Roses” because the winner will be draped with 554 red roses. This tradition started in 1883.

The Kentucky Derby is one event of the United States Triple Crown of Thoroughbred Racing, and is the most prestigious of these races. The other events are Preakness Stakes and the Belmont Stakes. The Kentucky Derby followed by the Preakness Stakes and then the Belmont Stakes.

Both Preakness and Belmont Stakes had gap years in 1891 until 1893 and 1911 and 1912, while the Kentucky Derby ran every year since 1875.

To win the Triple Crown, the horse must win the three races.

Audience for the Kentucky Derby usually surpasses the attendance of all other stakes races including the Preakness Stakes, Belmont Stakes and the Breeders’ Cup.";s:10:"post_title";s:14:"Kentucky Derby";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"kentucky-derby";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-04-01 02:03:53";s:17:"post_modified_gmt";s:19:"2016-04-01 07:03:53";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:58:"https://www.plus1games.com/?post_type=event&#038;p=1970026";s:10:"menu_order";s:1:"0";s:9:"post_type";s:5:"event";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}